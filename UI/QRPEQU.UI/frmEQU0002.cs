﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQU0002.cs                                         */
/* 프로그램명   : 점검계획등록(일괄)                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-24 : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0002 : Form, IToolbar
    {
        // 리소스호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 

        public frmEQU0002()
        {
            InitializeComponent();
        }

        private void frmEQU0002_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0002_Load(object sender, EventArgs e)
        {
            SetRunMode();
            SetToolAuth();
            // 초기화 함수
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            InitButton();
            InitTab();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            //MessageBox.Show(this.Tag.ToString());
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow3, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow4, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 초기값 설정 Method
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Title 제목
                titleArea.mfSetLabelText("점검계획등록(일괄)", m_resSys.GetString("SYS_FONTNAME"), 12);

                //GroupBox
                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBoxContentsArea, GroupBoxType.INFO, "계획정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxContentsArea.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBoxContentsArea.Appearance.FontData.SizeInPoints = 9;

                string strPlantcode = m_resSys.GetString("SYS_PLANTCODE");
                string strUserID = m_resSys.GetString("SYS_USERID");

                //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //brwChannel.mfCredentials(clsUser);

                //DataTable dtUser = clsUser.mfReadSYSUser(strPlantcode, strUserID, m_resSys.GetString("SYS_LANG"));

                string strUserName = m_resSys.GetString("SYS_USERNAME");

                // 정비사 ID 설정
                this.uTextUserID.Text = strUserID;
                this.uTextUserName.Text = strUserName;

                // 계획년도 TextBox 기본값 설정
                this.uNumSearchYear.Value = DateTime.Now.Year.ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "계획년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearch1, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStartDay, "계획적용시작일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.ultraLabel1, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // SearchArea 설비공정구분 ComboBox
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 점검항목상세(일간) 그리드 초기화
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMDay, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMDay, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 점검항목상세(주간)
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMWeek, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMWeek, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 점검항목상세(월간)
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMMonth, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMMonth, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 점검항목상세(분기, 반기, 년간)
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMYear, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMYear, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 점검그룹 설비리스트
                // 그리드 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼설정
                //wGrid.mfSetGridGroup(
                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false", 0, 0, 1, 2, null);

                //wGrid.mfSetGridColumn(this.uGridEquipList, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 2, null);

                //// 그룹헤더 처리
                //Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = null;
                //group1 = wGrid.mfSetGridGroup(this.ultraGrid6, 0, "주간점검", "주간점검", Infragistics.Win.DefaultableBoolean.True, Color.AliceBlue, true);

                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "주간점검", "주간점검", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 0, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMWeekDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 2, null);



                //// 그룹헤더 처리
                //Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = null;
                //group2 = wGrid.mfSetGridGroup(this.ultraGrid6, 0, "월간점검", "월간점검", Infragistics.Win.DefaultableBoolean.True, Color.AliceBlue, true);

                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "월간점검", "월간점검", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 2, 0, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 10, 0, 1, 2, null);


                // 그룹헤더 처리
                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "분기,반기,년간", "분기,반기,년간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 3, 0, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 1, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 12, 1, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 13, 1, 1, 2, null);

                //wGrid.mfSetGridGroup(this.ultraGrid6, 0, "분기,반기,년간", "분기,반기,년간", Infragistics.Win.DefaultableBoolean.True, Color.AliceBlue, true);

                this.uGridPMDay.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMDay.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMWeek.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMWeek.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMMonth.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMMonth.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMYear.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMYear.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab컨트롤초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinTabControl tab = new WinTabControl();

                tab.mfInitGeneralTabControl(this.uTabDay, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage,
                                                Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.Default
                                                , m_resSys.GetString("SYS_FONTNAME"));

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //--------필수 입력 사항 확인 ------//
                if (this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "계획년도를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                else if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
               

                //--------------값 저장 ----------------//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strProcGubunCode = this.uComboSearchProcGubun.Value.ToString();
                string strPlanYear = this.uNumSearchYear.Value.ToString();
                string strEquipType = "";
                string strEquipLoc = "";

                //-------------POPUP창 오픈----------------//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //------------BL호출-----------------//
                QRPEQU.BL.EQUMGM.PMPlan clsPMPlan;
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                if (m_bolDebugMode == false)
                {   
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                    clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                    brwChannel.mfCredentials(clsPMPlan);
                }
                else
                    clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan(m_strDBConn);

                //-----------매서드호출-------------// ==>SP호출이 없음 (2011.09.27)
                DataTable dtPlan = clsPMPlan.mfReadPMPlanBatch(strPlanYear, strPlantCode, strAreaCode, strStationCode, strProcGubunCode, m_resSys.GetString("SYS_LANG"));

                //--------정보가 있을 경우----------//
                if (dtPlan.Rows.Count > 0)
                {
                    this.uDateStartDay.Value = dtPlan.Rows[0]["PMApplyDate"].ToString();
                    this.uDateWrite.Value = dtPlan.Rows[0]["WriteDate"].ToString();
                    this.uTextUserID.Text = dtPlan.Rows[0]["WriteID"].ToString();
                    this.uTextUserName.Text = dtPlan.Rows[0]["UserName"].ToString();

                    this.uTabDay.Tabs[0].Selected = true;
                    //----------------------------예방점검상세정보-------------------------------//
                    QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD;
                    if (m_bolDebugMode == false)
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                        clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                        brwChannel.mfCredentials(clsPMPlanD);
                    }
                    else
                        clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD(m_strDBConn);

                    DataTable dtPlanD = clsPMPlanD.mfReadPMPlanD(strPlanYear, strPlantCode, strAreaCode, strStationCode, strProcGubunCode,strEquipLoc, strEquipType, m_resSys.GetString("SYS_LANG"));
                    //DataTable dtPlanD = clsPMPlanD.mfReadPMPlanD("", "", "", "", "", m_resSys.GetString("SYS_LANG"));
                    if (dtPlanD.Rows.Count != 0)
                    {
                        DataTable dtCopy = new DataTable();
                        DataRow[] drCopy;


                        #region  조건에 맞은 정보가 한 줄이상 있을 시 정보를 복사하여 저장 시킨 후 바인드
                        //일간
                        if (dtPlanD.Select("PMPeriodCode = 'DAY'").Count() != 0)
                        {

                            drCopy = dtPlanD.Select("PMPeriodCode = 'DAY'"); //검색된 값을 그리드 소스에 넣을 시 줄만 들어가서 컬럼인식을 못하여 화면에 안나옴
                            dtCopy = drCopy.CopyToDataTable();  //Row가 0 일 시 에러가 남

                            this.uGridPMDay.DataSource = dtCopy;
                            this.uGridPMDay.DataBind();
                            for (int i = 0; i < this.uGridPMDay.Rows.Count; i++)
                            {
                                if (this.uGridPMDay.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMDay.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                        }
                        //주간
                        if (dtPlanD.Select("PMPeriodCode = 'WEK'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'WEK'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMWeek.DataSource = dtCopy;
                            this.uGridPMWeek.DataBind();

                            for (int i = 0; i < this.uGridPMWeek.Rows.Count; i++)
                            {
                                if (this.uGridPMWeek.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMWeek.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                        }
                        //월간
                        if (dtPlanD.Select("PMPeriodCode = 'MON'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'MON'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMMonth.DataSource = dtCopy;
                            this.uGridPMMonth.DataBind();


                            for (int i = 0; i < this.uGridPMMonth.Rows.Count; i++)
                            {
                                if (this.uGridPMMonth.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMMonth.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                        }
                        //분기,반기,년간
                        string strExpr = "";
                        strExpr = "PMPeriodCode = 'QUA'";
                        strExpr = strExpr + " or PMPeriodCode = 'HAF'";
                        strExpr = strExpr + " or PMPeriodCode = 'YEA'";

                        if (dtPlanD.Select(strExpr).Count() != 0)
                        {
                            drCopy = dtPlanD.Select(strExpr);
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMYear.DataSource = dtCopy;
                            this.uGridPMYear.DataBind();


                            for (int i = 0; i < this.uGridPMYear.Rows.Count; i++)
                            {
                                if (this.uGridPMYear.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMYear.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                        }
                    }
                        #endregion
                    ////---설비리스트---//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    //QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    //brwChannel.mfCredentials(clsEquip);

                    //DataTable dtEquipList = clsEquip.mfReadEquipListBatch(strPlanYear, strPlantCode, strAreaCode, strStationCode, strProcGubunCode, m_resSys.GetString("SYS_LANG"));

                    //this.uGridEquipList.DataSource = dtEquipList;
                    //this.uGridEquipList.DataBind();
                }
                else
                {
                    InitDisplay();
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtPlan.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //--------필수 입력 사항 확인 ------//
                if (this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "계획년도를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                else if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                
                else if (this.uDateStartDay.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "계획적용 시작일을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateStartDay.DropDown();
                    return;
                }
                else if (this.uTextUserID.Text == "" || this.uTextUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                              , "확인창", "필수입력사항 확인", "작성자를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextUserID.Focus();
                    return;
                }

                //--------------값 저장 ----------------//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strProcGubunCode = this.uComboSearchProcGubun.Value.ToString();
                string strPlanYear = this.uNumSearchYear.Value.ToString();
                string strWriteID = this.uTextUserID.Text;
                string strStartDate = this.uDateStartDay.DateTime.Date.ToString("yyyy-MM-dd");
                string strWriteDate = this.uDateWrite.DateTime.Date.ToString("yyyy-MM-dd");

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", strPlanYear + "년도의 예방점검계획을 생성하겠습니까? (단, 처리시간이 5분이상 소요될 수 있으며, 점검진행이 된 예방점검계획은 유지됨)",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    //BL호출
                    QRPEQU.BL.EQUMGM.PMPlan clsPMPlan;
                    if (m_bolDebugMode == false)
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                        clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                        brwChannel.mfCredentials(clsPMPlan);
                    }
                    else
                        clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan(m_strDBConn);

                    string strErrRtn = clsPMPlan.mfSavePMPlanBatch(strPlantCode, strPlanYear, strAreaCode, strStationCode, strProcGubunCode, strWriteID, strWriteDate, strStartDate, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    TransErrRtn ErrRtn = new TransErrRtn();

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;


                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMDay.Rows.Count == 0 && this.uGridPMWeek.Rows.Count == 0 && this.uGridPMMonth.Rows.Count == 0 && this.uGridPMYear.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                //--------필수 입력 사항 확인 ------//
                if (this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "계획년도를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                else if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                else if (this.uDateStartDay.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "계획적용 시작일을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateStartDay.DropDown();
                    return;
                }
                else if (this.uTextUserID.Text == "" || this.uTextUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                              , "확인창", "필수입력사항 확인", "작성자를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextUserID.Focus();
                    return;
                }

                //--------------값 저장 ----------------//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strProcGubunCode = this.uComboSearchProcGubun.Value.ToString();
                string strPlanYear = this.uNumSearchYear.Value.ToString();
                string strWriteID = this.uTextUserID.Text;
                string strStartDate = this.uDateStartDay.DateTime.Date.ToString("yyyy-MM-dd");
                string strWriteDate = this.uDateWrite.DateTime.Date.ToString("yyyy-MM-dd");

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "확인창", "삭제확인", strPlanYear + "년도 예방점검계획을 삭제 하시겠습니까? (단, 점검진행이 된 예방점검계획은 삭제불가)",
                                     Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                    QRPEQU.BL.EQUMGM.PMPlan clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                    brwChannel.mfCredentials(clsPMPlan);

                    string strErrRtn = clsPMPlan.mfDeletePMPlanBatch(strPlanYear,strPlantCode,strAreaCode,strStationCode,strProcGubunCode);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;



                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                   Infragistics.Win.HAlign.Right);
                        InitDisplay();
                        this.uComboSearchPlant.Value = "";
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                                    Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        {
            
        }

        public void mfPrint()
        {
        }
        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridPMDay.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMDay);

                if (this.uGridPMMonth.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMMonth);

                if (this.uGridPMWeek.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMWeek);

                if (this.uGridPMYear.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMYear);

                if (this.uGridEquipList.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipList);


                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 이벤트
        //공장 선택 시 Area Station 설비공정구분 콤보박스 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinComboEditor wCom = new WinComboEditor();

                    //List클리어
                    this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    this.uComboSearchProcGubun.Items.Clear();



                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //------------Area콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);

                    DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                        "", "", "전체", "AreaCode", "AreaName", dtArea);

                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                        "", "", "전체", "StationCode", "StationName", dtStation);

                    //-----------설비공정구분콤보 변화 -------------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    brwChannel.mfCredentials(clsEquipProcGubun);

                    DataTable dtGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboSearchProcGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                        "", "", "전체", "EquipProcGubunCode", "EquipProcGubunName", dtGubun);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 행삭제 이벤트

        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteRow("DAY");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {

                DeleteRow("WEK");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uButtonDeleteRow3_Click(object sender, EventArgs e)
        {
            try
            {

                DeleteRow("MON");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uButtonDeleteRow4_Click(object sender, EventArgs e)
        {
            try
            {

                DeleteRow("YEA");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        //작성자 Id입력 후 엔터 누를 시 이름 자동입력
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    
                        this.uTextUserName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strWriteID = this.uTextUserID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strWriteID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextUserID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strWriteID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    //정보가 없는 경우 작성자명 클리어
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "확인창", "입력확인", "입력하신 ID의 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextUserName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //버튼 클릭 시 유저 정보 보여주기
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //SpinEditorButton 증감
        private void uNumSearchYear_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        private void uGridPMDay_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;


                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMDay, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMWeek_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;


                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMWeek, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMMonth_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;


                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMMonth, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMYear_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;


                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMYear, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uNumSearchYear_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 현재 년도로 초기화
                ed.Value = DateTime.Now.Year;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 행정보삭제
        /// </summary>
        /// <param name="strGubun">구분코드</param>
        private void DeleteRow(string strGubun)
        {
            try
            {
                //SystemReouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                brwChannel.mfCredentials(clsPMPlanD);

                DataTable dtPMDel = clsPMPlanD.mfSetPMPlanDColumn();
                //-------------------------그리드 종류에 따라 값 저장 ------------------------//
                switch (strGubun)
                {
                    //----------------------------일간-------------------------//
                    case "DAY":
                        //줄이 0이상 인지 판단
                        if (this.uGridPMDay.Rows.Count > 0)
                        {
                            for (int i = 0; i < this.uGridPMDay.Rows.Count; i++)
                            {
                                //체크 된 줄의 정보만 저장
                                if (Convert.ToBoolean(this.uGridPMDay.Rows[i].Cells["Check"].Value) == true)
                                {
                                    DataRow drDel;
                                    drDel = dtPMDel.NewRow();
                                    drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                    drDel["PlanYear"] = this.uGridPMDay.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                    drDel["EquipCode"] = this.uGridPMDay.Rows[i].Cells["EquipCode"].Value.ToString();
                                    drDel["Seq"] = this.uGridPMDay.Rows[i].Cells["Seq"].Value.ToString();
                                    dtPMDel.Rows.Add(drDel);
                                }
                            }
                        }
                        //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        break;

                    //-------------------------주간----------------------//
                    case "WEK":
                        //줄이 0이상 인지 판단
                        if (this.uGridPMWeek.Rows.Count > 0)
                        {
                            for (int i = 0; i < this.uGridPMWeek.Rows.Count; i++)
                            {
                                //체크 된 줄의 정보만 저장
                                if (Convert.ToBoolean(this.uGridPMWeek.Rows[i].Cells["Check"].Value) == true)
                                {
                                    DataRow drDel;
                                    drDel = dtPMDel.NewRow();
                                    drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                    drDel["PlanYear"] = this.uGridPMWeek.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                    drDel["EquipCode"] = this.uGridPMWeek.Rows[i].Cells["EquipCode"].Value.ToString();
                                    drDel["Seq"] = this.uGridPMWeek.Rows[i].Cells["Seq"].Value.ToString();
                                    dtPMDel.Rows.Add(drDel);
                                }
                            }
                        }
                        //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        break;
                    //----------------------월간---------------------//
                    case "MON":
                        //줄이 0이상 인지 판단
                        if (this.uGridPMMonth.Rows.Count > 0)
                        {
                            for (int i = 0; i < this.uGridPMMonth.Rows.Count; i++)
                            {
                                //체크 된 줄의 정보만 저장
                                if (Convert.ToBoolean(this.uGridPMMonth.Rows[i].Cells["Check"].Value) == true)
                                {
                                    DataRow drDel;
                                    drDel = dtPMDel.NewRow();
                                    drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                    drDel["PlanYear"] = this.uGridPMMonth.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                    drDel["EquipCode"] = this.uGridPMMonth.Rows[i].Cells["EquipCode"].Value.ToString();
                                    drDel["Seq"] = this.uGridPMMonth.Rows[i].Cells["Seq"].Value.ToString();
                                    dtPMDel.Rows.Add(drDel);
                                }
                            }
                        }
                        //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        break;
                    //---------------------분기,반기,년간-----------------------//
                    case "YEA":
                        //줄이 0이상 인지 판단
                        if (this.uGridPMYear.Rows.Count > 0)
                        {
                            for (int i = 0; i < this.uGridPMYear.Rows.Count; i++)
                            {
                                //체크 된 줄의 정보만 저장
                                if (Convert.ToBoolean(this.uGridPMYear.Rows[i].Cells["Check"].Value) == true)
                                {
                                    DataRow drDel;
                                    drDel = dtPMDel.NewRow();
                                    drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                    drDel["PlanYear"] = this.uGridPMYear.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                    drDel["EquipCode"] = this.uGridPMYear.Rows[i].Cells["EquipCode"].Value.ToString();
                                    drDel["Seq"] = this.uGridPMYear.Rows[i].Cells["Seq"].Value.ToString();
                                    dtPMDel.Rows.Add(drDel);
                                }
                            }
                        }
                        //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        break;
                }
                //------------------------------저장 끝------------------------------------//

                if (dtPMDel.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "선택사항 확인", "선택하신 행이 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "확인창", "삭제확인", "선택한 예방점검계획을 삭제하시겠습니까?",
                                     Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //처리로직//
                    string strRtn = clsPMPlanD.mfDeletePMPlanD(dtPMDel);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    //처리로직끝//

                    //처리결과값처리//
                    if (ErrRtn.ErrNum == 0)
                    {
                        InitDisplay();
                        SearchInfo();

                    }
                    
                    //else
                    //{
                    //    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                    //                Infragistics.Win.HAlign.Right);
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스 변화에 따라 자동조회
        /// </summary>
        private void DisplyChange()
        {
            try
            {
                SearchInfo();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자동조회,리로드
        /// </summary>
        private void SearchInfo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlanYear = this.uNumSearchYear.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strProcGubunCode = this.uComboSearchProcGubun.Value.ToString();
                string strEquipLoc = "";
                string strEquipType = "";
                

                //처리 로직//
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                QRPEQU.BL.EQUMGM.PMPlan clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                brwChannel.mfCredentials(clsPMPlan);

                DataTable dtPlan = clsPMPlan.mfReadPMPlan(strPlanYear,strPlantCode,strAreaCode,strStationCode,strProcGubunCode,m_resSys.GetString("SYS_LANG"));
                /////////////

                //---데이터 바인드? ---//
                if (dtPlan.Rows.Count != 0)
                {
                    this.uDateStartDay.Value = dtPlan.Rows[0]["PMApplyDate"].ToString();
                    this.uDateWrite.Value = dtPlan.Rows[0]["WriteDate"].ToString();
                    this.uTextUserID.Text = dtPlan.Rows[0]["WriteID"].ToString();
                    this.uTextUserName.Text = dtPlan.Rows[0]["UserName"].ToString();

                    this.uTabDay.Tabs[0].Selected = true;
                    //----------------------------예방점검상세정보-------------------------------//
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                    QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                    brwChannel.mfCredentials(clsPMPlanD);

                    DataTable dtPlanD = clsPMPlanD.mfReadPMPlanD(strPlanYear,strPlantCode,strAreaCode,strStationCode,strProcGubunCode,strEquipLoc,strEquipType, m_resSys.GetString("SYS_LANG"));

                    if (dtPlanD.Rows.Count != 0)
                    {

                        DataTable dtCopy = new DataTable();
                        DataRow[] drCopy;

                        //조건에 맞은 정보가 한 줄이상 있을 시 정보를 복사하여 저장 시킨 후 바인드

                        //일간
                        if (dtPlanD.Select("PMPeriodCode = 'DAY'").Count() != 0)
                        {

                            drCopy = dtPlanD.Select("PMPeriodCode = 'DAY'"); //검색된 값을 그리드 소스에 넣을 시 줄만 들어가서 컬럼인식을 못하여 화면에 안나옴
                            dtCopy = drCopy.CopyToDataTable();  //Row가 0 일 시 에러가 남

                            this.uGridPMDay.DataSource = dtCopy;
                            this.uGridPMDay.DataBind();
                            for (int i = 0; i < this.uGridPMDay.Rows.Count; i++)
                            {
                                if (this.uGridPMDay.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMDay.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                        }
                        //주간
                        if (dtPlanD.Select("PMPeriodCode = 'WEK'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'WEK'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMWeek.DataSource = dtCopy;
                            this.uGridPMWeek.DataBind();

                            for (int i = 0; i < this.uGridPMWeek.Rows.Count; i++)
                            {
                                if (this.uGridPMWeek.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMWeek.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                        }
                        //월간
                        if (dtPlanD.Select("PMPeriodCode = 'MON'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'MON'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMMonth.DataSource = dtCopy;
                            this.uGridPMMonth.DataBind();


                            for (int i = 0; i < this.uGridPMMonth.Rows.Count; i++)
                            {
                                if (this.uGridPMMonth.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMMonth.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                        }
                        //분기,반기,년간
                        string strExpr = "";
                        strExpr = "PMPeriodCode = 'QUA'";
                        strExpr = strExpr + " or PMPeriodCode = 'HAF'";
                        strExpr = strExpr + " or PMPeriodCode = 'YEA'";

                        if (dtPlanD.Select(strExpr).Count() != 0)
                        {
                            drCopy = dtPlanD.Select(strExpr);
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMYear.DataSource = dtCopy;
                            this.uGridPMYear.DataBind();


                            for (int i = 0; i < this.uGridPMYear.Rows.Count; i++)
                            {
                                if (this.uGridPMYear.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMYear.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                        }
                    }
                   
                }
                else
                {
                    InitDisplay();
                }
                //---설비리스트---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquipList = clsEquip.mfReadEquipListBatch(strPlanYear, strPlantCode, strAreaCode, strStationCode, strProcGubunCode, m_resSys.GetString("SYS_LANG"));

                this.uGridEquipList.DataSource = dtEquipList;
                this.uGridEquipList.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 기본값으로
        /// </summary>
        private void InitDisplay()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                //DataTable dtDefault = new DataTable();

                //dtDefault.Columns.Add("Seq", typeof(string));
                //dtDefault.Columns.Add("PMPeriodName", typeof(string));
                //dtDefault.Columns.Add("PMPlanDate", typeof(string));
                //dtDefault.Columns.Add("EquipCode", typeof(string));
                //dtDefault.Columns.Add("EquipName", typeof(string));
                //dtDefault.Columns.Add("PMInspectRegion", typeof(string));
                //dtDefault.Columns.Add("PMInspectName", typeof(string));
                //dtDefault.Columns.Add("PMInspectCriteria", typeof(string));
                //dtDefault.Columns.Add("PMMethod", typeof(string));
                //dtDefault.Columns.Add("FaultFixMethod", typeof(string));
                //dtDefault.Columns.Add("StandardManCount", typeof(string));
                //dtDefault.Columns.Add("StandardTime", typeof(string));
                //dtDefault.Columns.Add("LevelName", typeof(string));
                //dtDefault.Columns.Add("PMResultName", typeof(string));
                
                    

                //this.uGridPMDay.DataSource = dtDefault;
                //this.uGridPMDay.DataBind();

                //this.uGridPMWeek.DataSource = dtDefault;
                //this.uGridPMWeek.DataBind();

                //this.uGridPMMonth.DataSource = dtDefault;
                //this.uGridPMMonth.DataBind();

                //this.uGridPMYear.DataSource = dtDefault;
                //this.uGridPMYear.DataBind();

                //설비리스트
                while (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Rows[0].Delete(false);
                }
                //일간
                while (this.uGridPMDay.Rows.Count > 0)
                {
                    this.uGridPMDay.Rows[0].Delete(false);
                }
                //주간
                while (this.uGridPMWeek.Rows.Count > 0)
                {
                    this.uGridPMWeek.Rows[0].Delete(false);
                }
                //월간
                while (this.uGridPMMonth.Rows.Count > 0)
                {
                    this.uGridPMMonth.Rows[0].Delete(false);
                }
                //분기,반기,년간
                while (this.uGridPMYear.Rows.Count > 0)
                {
                    this.uGridPMYear.Rows[0].Delete(false);
                }

                this.uNumSearchYear.Value = DateTime.Now.Year.ToString();

                this.uTextUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //폼이 닫히기전에 그리드 설정값을 저장시킨다
        private void frmEQU0002_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0002_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    uTabDay.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    uTabDay.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        
        
    }
}
