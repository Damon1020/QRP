﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0012));
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRepairDay = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridRepairResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uLabelUseSparePart = new Infragistics.Win.Misc.UltraLabel();
            this.uDateUseSPChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextUseSPEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUseSPChgName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUseSPChgID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUsePerson = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGridRepairUseSP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRepairDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipStopTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairResultName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairEndDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCCSReqFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelStopType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairDay = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairReqDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectDay = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUseSPChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseSPEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseSPChgName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseSPChgID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairUseSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResultName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCCSReqFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRepairDay);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboEquipLargeType
            // 
            this.uComboEquipLargeType.Location = new System.Drawing.Point(672, 36);
            this.uComboEquipLargeType.MaxLength = 50;
            this.uComboEquipLargeType.Name = "uComboEquipLargeType";
            this.uComboEquipLargeType.Size = new System.Drawing.Size(130, 21);
            this.uComboEquipLargeType.TabIndex = 36;
            this.uComboEquipLargeType.ValueChanged += new System.EventHandler(this.uComboEquipLargeType_ValueChanged);
            // 
            // uLabelEquType
            // 
            this.uLabelEquType.Location = new System.Drawing.Point(568, 36);
            this.uLabelEquType.Name = "uLabelEquType";
            this.uLabelEquType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquType.TabIndex = 37;
            this.uLabelEquType.Text = "ultraLabel2";
            // 
            // uComboEquipLoc
            // 
            this.uComboEquipLoc.Location = new System.Drawing.Point(116, 36);
            this.uComboEquipLoc.MaxLength = 50;
            this.uComboEquipLoc.Name = "uComboEquipLoc";
            this.uComboEquipLoc.Size = new System.Drawing.Size(131, 21);
            this.uComboEquipLoc.TabIndex = 34;
            this.uComboEquipLoc.ValueChanged += new System.EventHandler(this.uComboLoc_ValueChanged);
            // 
            // uLabelLoc
            // 
            this.uLabelLoc.Location = new System.Drawing.Point(12, 36);
            this.uLabelLoc.Name = "uLabelLoc";
            this.uLabelLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelLoc.TabIndex = 35;
            this.uLabelLoc.Text = "ultraLabel1";
            // 
            // uComboSearchEquipProcess
            // 
            this.uComboSearchEquipProcess.Location = new System.Drawing.Point(1028, 4);
            this.uComboSearchEquipProcess.Name = "uComboSearchEquipProcess";
            this.uComboSearchEquipProcess.Size = new System.Drawing.Size(21, 21);
            this.uComboSearchEquipProcess.TabIndex = 18;
            this.uComboSearchEquipProcess.Text = "ultraComboEditor1";
            this.uComboSearchEquipProcess.Visible = false;
            // 
            // uLabelSearchEquipProcess
            // 
            this.uLabelSearchEquipProcess.Location = new System.Drawing.Point(1009, 4);
            this.uLabelSearchEquipProcess.Name = "uLabelSearchEquipProcess";
            this.uLabelSearchEquipProcess.Size = new System.Drawing.Size(11, 20);
            this.uLabelSearchEquipProcess.TabIndex = 17;
            this.uLabelSearchEquipProcess.Text = "ultraLabel5";
            this.uLabelSearchEquipProcess.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(672, 12);
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchStation.TabIndex = 16;
            this.uComboSearchStation.Text = "ultraComboEditor1";
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(568, 12);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 15;
            this.uLabelSearchStation.Text = "ultraLabel4";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1041, 8);
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(19, 21);
            this.uComboSearchArea.TabIndex = 14;
            this.uComboSearchArea.Text = "ultraComboEditor1";
            this.uComboSearchArea.Visible = false;
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(1025, 4);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(11, 20);
            this.uLabelSearchArea.TabIndex = 13;
            this.uLabelSearchArea.Text = "ultraLabel3";
            this.uLabelSearchArea.Visible = false;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(912, 36);
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipGroup.TabIndex = 12;
            this.uComboSearchEquipGroup.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(808, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 11;
            this.uLabelSearchEquipGroup.Text = "ultraLabel3";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(432, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchPlant.TabIndex = 10;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(328, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 9;
            // 
            // uDateSearchToDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDate.Appearance = appearance16;
            this.uDateSearchToDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateSearchToDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateSearchToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToDate.Location = new System.Drawing.Point(224, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 19);
            this.uDateSearchToDate.TabIndex = 8;
            // 
            // ultraLabel2
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(216, 15);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(8, 20);
            this.ultraLabel2.TabIndex = 6;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDate.Appearance = appearance39;
            this.uDateSearchFromDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateSearchFromDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateSearchFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 19);
            this.uDateSearchFromDate.TabIndex = 7;
            // 
            // uLabelSearchRepairDay
            // 
            this.uLabelSearchRepairDay.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchRepairDay.Name = "uLabelSearchRepairDay";
            this.uLabelSearchRepairDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchRepairDay.TabIndex = 0;
            this.uLabelSearchRepairDay.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridRepairResult
            // 
            this.uGridRepairResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRepairResult.DisplayLayout.Appearance = appearance6;
            this.uGridRepairResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRepairResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairResult.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridRepairResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairResult.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridRepairResult.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRepairResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRepairResult.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRepairResult.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridRepairResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRepairResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRepairResult.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRepairResult.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridRepairResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRepairResult.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairResult.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGridRepairResult.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridRepairResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRepairResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridRepairResult.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridRepairResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRepairResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGridRepairResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRepairResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRepairResult.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRepairResult.Location = new System.Drawing.Point(0, 100);
            this.uGridRepairResult.Name = "uGridRepairResult";
            this.uGridRepairResult.Size = new System.Drawing.Size(1070, 720);
            this.uGridRepairResult.TabIndex = 2;
            this.uGridRepairResult.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridRepairResult_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox4.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox4.Controls.Add(this.uLabelUseSparePart);
            this.uGroupBox4.Controls.Add(this.uDateUseSPChgDate);
            this.uGroupBox4.Controls.Add(this.uTextUseSPEtcDesc);
            this.uGroupBox4.Controls.Add(this.uLabelUnusual);
            this.uGroupBox4.Controls.Add(this.uTextUseSPChgName);
            this.uGroupBox4.Controls.Add(this.uTextUseSPChgID);
            this.uGroupBox4.Controls.Add(this.uLabelUsePerson);
            this.uGroupBox4.Controls.Add(this.uLabelUseDay);
            this.uGroupBox4.Controls.Add(this.uGridRepairUseSP);
            this.uGroupBox4.Location = new System.Drawing.Point(12, 152);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(1040, 492);
            this.uGroupBox4.TabIndex = 3;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(116, 88);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 39;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uLabelUseSparePart
            // 
            this.uLabelUseSparePart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uLabelUseSparePart.Location = new System.Drawing.Point(12, 92);
            this.uLabelUseSparePart.Name = "uLabelUseSparePart";
            this.uLabelUseSparePart.Size = new System.Drawing.Size(100, 20);
            this.uLabelUseSparePart.TabIndex = 38;
            this.uLabelUseSparePart.Text = "ultraLabel2";
            // 
            // uDateUseSPChgDate
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateUseSPChgDate.Appearance = appearance15;
            this.uDateUseSPChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateUseSPChgDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateUseSPChgDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateUseSPChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateUseSPChgDate.Location = new System.Drawing.Point(116, 28);
            this.uDateUseSPChgDate.Name = "uDateUseSPChgDate";
            this.uDateUseSPChgDate.Size = new System.Drawing.Size(100, 19);
            this.uDateUseSPChgDate.TabIndex = 47;
            // 
            // uTextUseSPEtcDesc
            // 
            this.uTextUseSPEtcDesc.Location = new System.Drawing.Point(116, 52);
            this.uTextUseSPEtcDesc.MaxLength = 100;
            this.uTextUseSPEtcDesc.Name = "uTextUseSPEtcDesc";
            this.uTextUseSPEtcDesc.Size = new System.Drawing.Size(676, 21);
            this.uTextUseSPEtcDesc.TabIndex = 46;
            // 
            // uLabelUnusual
            // 
            this.uLabelUnusual.Location = new System.Drawing.Point(12, 52);
            this.uLabelUnusual.Name = "uLabelUnusual";
            this.uLabelUnusual.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnusual.TabIndex = 45;
            this.uLabelUnusual.Text = "ultraLabel5";
            // 
            // uTextUseSPChgName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseSPChgName.Appearance = appearance22;
            this.uTextUseSPChgName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseSPChgName.Location = new System.Drawing.Point(484, 28);
            this.uTextUseSPChgName.Name = "uTextUseSPChgName";
            this.uTextUseSPChgName.ReadOnly = true;
            this.uTextUseSPChgName.Size = new System.Drawing.Size(100, 21);
            this.uTextUseSPChgName.TabIndex = 44;
            // 
            // uTextUseSPChgID
            // 
            appearance23.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUseSPChgID.Appearance = appearance23;
            this.uTextUseSPChgID.BackColor = System.Drawing.Color.PowderBlue;
            appearance24.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance24.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance24;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUseSPChgID.ButtonsRight.Add(editorButton1);
            this.uTextUseSPChgID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextUseSPChgID.Location = new System.Drawing.Point(380, 28);
            this.uTextUseSPChgID.MaxLength = 20;
            this.uTextUseSPChgID.Name = "uTextUseSPChgID";
            this.uTextUseSPChgID.Size = new System.Drawing.Size(100, 21);
            this.uTextUseSPChgID.TabIndex = 43;
            this.uTextUseSPChgID.ValueChanged += new System.EventHandler(this.uTextUseSPChgID_ValueChanged);
            this.uTextUseSPChgID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUseSPChgID_KeyDown);
            this.uTextUseSPChgID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUseSPChgID_EditorButtonClick);
            // 
            // uLabelUsePerson
            // 
            this.uLabelUsePerson.Location = new System.Drawing.Point(276, 28);
            this.uLabelUsePerson.Name = "uLabelUsePerson";
            this.uLabelUsePerson.Size = new System.Drawing.Size(100, 20);
            this.uLabelUsePerson.TabIndex = 42;
            this.uLabelUsePerson.Text = "ultraLabel2";
            // 
            // uLabelUseDay
            // 
            this.uLabelUseDay.Location = new System.Drawing.Point(12, 28);
            this.uLabelUseDay.Name = "uLabelUseDay";
            this.uLabelUseDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelUseDay.TabIndex = 41;
            this.uLabelUseDay.Text = "ultraLabel5";
            // 
            // uGridRepairUseSP
            // 
            this.uGridRepairUseSP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRepairUseSP.DisplayLayout.Appearance = appearance29;
            this.uGridRepairUseSP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRepairUseSP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairUseSP.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairUseSP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridRepairUseSP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairUseSP.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridRepairUseSP.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRepairUseSP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRepairUseSP.DisplayLayout.Override.ActiveCellAppearance = appearance37;
            appearance32.BackColor = System.Drawing.SystemColors.Highlight;
            appearance32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRepairUseSP.DisplayLayout.Override.ActiveRowAppearance = appearance32;
            this.uGridRepairUseSP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRepairUseSP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRepairUseSP.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRepairUseSP.DisplayLayout.Override.CellAppearance = appearance30;
            this.uGridRepairUseSP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRepairUseSP.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairUseSP.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance36.TextHAlignAsString = "Left";
            this.uGridRepairUseSP.DisplayLayout.Override.HeaderAppearance = appearance36;
            this.uGridRepairUseSP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRepairUseSP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGridRepairUseSP.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGridRepairUseSP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRepairUseSP.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridRepairUseSP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRepairUseSP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRepairUseSP.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRepairUseSP.Location = new System.Drawing.Point(12, 96);
            this.uGridRepairUseSP.Name = "uGridRepairUseSP";
            this.uGridRepairUseSP.Size = new System.Drawing.Size(1016, 356);
            this.uGridRepairUseSP.TabIndex = 40;
            this.uGridRepairUseSP.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridRepairUseSP_AfterCellUpdate);
            this.uGridRepairUseSP.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridRepairUseSP_CellListSelect);
            this.uGridRepairUseSP.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridRepairUseSP_AfterRowInsert);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextRepairDesc);
            this.uGroupBox1.Controls.Add(this.uTextEquipStopTypeName);
            this.uGroupBox1.Controls.Add(this.uTextRepairResultName);
            this.uGroupBox1.Controls.Add(this.uTextRepairID);
            this.uGroupBox1.Controls.Add(this.uTextRepairEndDate);
            this.uGroupBox1.Controls.Add(this.uLabelRepairUnusual);
            this.uGroupBox1.Controls.Add(this.uCheckCCSReqFlag);
            this.uGroupBox1.Controls.Add(this.uLabelStopType);
            this.uGroupBox1.Controls.Add(this.uLabelRepairResult);
            this.uGroupBox1.Controls.Add(this.uTextRepairName);
            this.uGroupBox1.Controls.Add(this.uLabelRepairUser);
            this.uGroupBox1.Controls.Add(this.uLabelRepairDay);
            this.uGroupBox1.Controls.Add(this.uTextRepairReqDate);
            this.uGroupBox1.Controls.Add(this.uLabelInspectDay);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1044, 132);
            this.uGroupBox1.TabIndex = 0;
            // 
            // uTextRepairDesc
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDesc.Appearance = appearance38;
            this.uTextRepairDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDesc.Location = new System.Drawing.Point(116, 100);
            this.uTextRepairDesc.Name = "uTextRepairDesc";
            this.uTextRepairDesc.ReadOnly = true;
            this.uTextRepairDesc.Size = new System.Drawing.Size(500, 21);
            this.uTextRepairDesc.TabIndex = 54;
            // 
            // uTextEquipStopTypeName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipStopTypeName.Appearance = appearance19;
            this.uTextEquipStopTypeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipStopTypeName.Location = new System.Drawing.Point(412, 76);
            this.uTextEquipStopTypeName.Name = "uTextEquipStopTypeName";
            this.uTextEquipStopTypeName.ReadOnly = true;
            this.uTextEquipStopTypeName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipStopTypeName.TabIndex = 53;
            // 
            // uTextRepairResultName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairResultName.Appearance = appearance40;
            this.uTextRepairResultName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairResultName.Location = new System.Drawing.Point(116, 76);
            this.uTextRepairResultName.Name = "uTextRepairResultName";
            this.uTextRepairResultName.ReadOnly = true;
            this.uTextRepairResultName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairResultName.TabIndex = 52;
            // 
            // uTextRepairID
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairID.Appearance = appearance20;
            this.uTextRepairID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairID.Location = new System.Drawing.Point(412, 52);
            this.uTextRepairID.Name = "uTextRepairID";
            this.uTextRepairID.ReadOnly = true;
            this.uTextRepairID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairID.TabIndex = 51;
            // 
            // uTextRepairEndDate
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairEndDate.Appearance = appearance41;
            this.uTextRepairEndDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairEndDate.Location = new System.Drawing.Point(116, 52);
            this.uTextRepairEndDate.Name = "uTextRepairEndDate";
            this.uTextRepairEndDate.ReadOnly = true;
            this.uTextRepairEndDate.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairEndDate.TabIndex = 50;
            // 
            // uLabelRepairUnusual
            // 
            this.uLabelRepairUnusual.Location = new System.Drawing.Point(12, 100);
            this.uLabelRepairUnusual.Name = "uLabelRepairUnusual";
            this.uLabelRepairUnusual.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairUnusual.TabIndex = 48;
            this.uLabelRepairUnusual.Text = "ultraLabel2";
            // 
            // uCheckCCSReqFlag
            // 
            this.uCheckCCSReqFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckCCSReqFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCCSReqFlag.Location = new System.Drawing.Point(516, 76);
            this.uCheckCCSReqFlag.Name = "uCheckCCSReqFlag";
            this.uCheckCCSReqFlag.Size = new System.Drawing.Size(100, 20);
            this.uCheckCCSReqFlag.TabIndex = 47;
            this.uCheckCCSReqFlag.Text = "CCS 의뢰";
            this.uCheckCCSReqFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelStopType
            // 
            this.uLabelStopType.Location = new System.Drawing.Point(308, 76);
            this.uLabelStopType.Name = "uLabelStopType";
            this.uLabelStopType.Size = new System.Drawing.Size(100, 20);
            this.uLabelStopType.TabIndex = 46;
            this.uLabelStopType.Text = "ultraLabel2";
            // 
            // uLabelRepairResult
            // 
            this.uLabelRepairResult.Location = new System.Drawing.Point(12, 76);
            this.uLabelRepairResult.Name = "uLabelRepairResult";
            this.uLabelRepairResult.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairResult.TabIndex = 44;
            this.uLabelRepairResult.Text = "ultraLabel3";
            // 
            // uTextRepairName
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairName.Appearance = appearance28;
            this.uTextRepairName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairName.Location = new System.Drawing.Point(516, 52);
            this.uTextRepairName.Name = "uTextRepairName";
            this.uTextRepairName.ReadOnly = true;
            this.uTextRepairName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairName.TabIndex = 42;
            // 
            // uLabelRepairUser
            // 
            this.uLabelRepairUser.Location = new System.Drawing.Point(308, 52);
            this.uLabelRepairUser.Name = "uLabelRepairUser";
            this.uLabelRepairUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairUser.TabIndex = 41;
            this.uLabelRepairUser.Text = "ultraLabel2";
            // 
            // uLabelRepairDay
            // 
            this.uLabelRepairDay.Location = new System.Drawing.Point(12, 52);
            this.uLabelRepairDay.Name = "uLabelRepairDay";
            this.uLabelRepairDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairDay.TabIndex = 39;
            this.uLabelRepairDay.Text = "ultraLabel5";
            // 
            // uTextRepairReqDate
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqDate.Appearance = appearance17;
            this.uTextRepairReqDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqDate.Location = new System.Drawing.Point(756, 28);
            this.uTextRepairReqDate.Name = "uTextRepairReqDate";
            this.uTextRepairReqDate.ReadOnly = true;
            this.uTextRepairReqDate.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairReqDate.TabIndex = 23;
            // 
            // uLabelInspectDay
            // 
            this.uLabelInspectDay.Location = new System.Drawing.Point(652, 28);
            this.uLabelInspectDay.Name = "uLabelInspectDay";
            this.uLabelInspectDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectDay.TabIndex = 22;
            this.uLabelInspectDay.Text = "ultraLabel5";
            // 
            // uTextEquipName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance18;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(412, 28);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(204, 21);
            this.uTextEquipName.TabIndex = 21;
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(308, 28);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipName.TabIndex = 20;
            this.uLabelEquipName.Text = "ultraLabel5";
            // 
            // uTextEquipCode
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance42;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(116, 28);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipCode.TabIndex = 19;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCode.TabIndex = 18;
            this.uLabelEquipCode.Text = "ultraLabel5";
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(328, 36);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 9;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(432, 36);
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboProcessGroup.TabIndex = 38;
            this.uComboProcessGroup.Text = "ultraComboEditor1";
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // frmEQUZ0012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridRepairResult);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0012";
            this.Load += new System.EventHandler(this.frmEQUZ0012_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0012_Activated);
            this.Resize += new System.EventHandler(this.frmEQUZ0012_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            this.uGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUseSPChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseSPEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseSPChgName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseSPChgID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairUseSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResultName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCCSReqFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRepairDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepairResult;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.Misc.UltraLabel uLabelUseSparePart;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepairUseSP;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipStopTypeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairResultName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairEndDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUnusual;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCCSReqFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelStopType;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairName;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUser;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateUseSPChgDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseSPEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseSPChgName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseSPChgID;
        private Infragistics.Win.Misc.UltraLabel uLabelUsePerson;
        private Infragistics.Win.Misc.UltraLabel uLabelUseDay;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
    }
}