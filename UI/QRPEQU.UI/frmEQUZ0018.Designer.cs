﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0018
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0018));
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboReturnSPInventoryCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelReturnSPInventoryCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReturnChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReturnChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateReturnDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSPTransferD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReturnSPInventoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReturnDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uComboReturnSPInventoryCode);
            this.uGroupBox1.Controls.Add(this.uLabelReturnSPInventoryCode);
            this.uGroupBox1.Controls.Add(this.uTextReturnChargeName);
            this.uGroupBox1.Controls.Add(this.uTextReturnChargeID);
            this.uGroupBox1.Controls.Add(this.uDateReturnDate);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uLabel3);
            this.uGroupBox1.Controls.Add(this.uGridSPTransferD);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1060, 800);
            this.uGroupBox1.TabIndex = 11;
            // 
            // uComboReturnSPInventoryCode
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboReturnSPInventoryCode.Appearance = appearance5;
            this.uComboReturnSPInventoryCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboReturnSPInventoryCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboReturnSPInventoryCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboReturnSPInventoryCode.Location = new System.Drawing.Point(724, 28);
            this.uComboReturnSPInventoryCode.MaxLength = 50;
            this.uComboReturnSPInventoryCode.Name = "uComboReturnSPInventoryCode";
            this.uComboReturnSPInventoryCode.Size = new System.Drawing.Size(120, 19);
            this.uComboReturnSPInventoryCode.TabIndex = 40;
            this.uComboReturnSPInventoryCode.Text = "ultraComboEditor1";
            // 
            // uLabelReturnSPInventoryCode
            // 
            this.uLabelReturnSPInventoryCode.Location = new System.Drawing.Point(620, 28);
            this.uLabelReturnSPInventoryCode.Name = "uLabelReturnSPInventoryCode";
            this.uLabelReturnSPInventoryCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelReturnSPInventoryCode.TabIndex = 39;
            this.uLabelReturnSPInventoryCode.Text = "ultraLabel1";
            // 
            // uTextReturnChargeName
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnChargeName.Appearance = appearance67;
            this.uTextReturnChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnChargeName.Location = new System.Drawing.Point(500, 28);
            this.uTextReturnChargeName.Name = "uTextReturnChargeName";
            this.uTextReturnChargeName.ReadOnly = true;
            this.uTextReturnChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextReturnChargeName.TabIndex = 37;
            // 
            // uTextReturnChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReturnChargeID.Appearance = appearance15;
            this.uTextReturnChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReturnChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReturnChargeID.ButtonsRight.Add(editorButton1);
            this.uTextReturnChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReturnChargeID.Location = new System.Drawing.Point(396, 28);
            this.uTextReturnChargeID.MaxLength = 20;
            this.uTextReturnChargeID.Name = "uTextReturnChargeID";
            this.uTextReturnChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextReturnChargeID.TabIndex = 36;
            this.uTextReturnChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReturnChargeID_KeyDown);
            this.uTextReturnChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReturnChargeID_EditorButtonClick);
            // 
            // uDateReturnDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReturnDate.Appearance = appearance16;
            this.uDateReturnDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReturnDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateReturnDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReturnDate.Location = new System.Drawing.Point(116, 28);
            this.uDateReturnDate.Name = "uDateReturnDate";
            this.uDateReturnDate.Size = new System.Drawing.Size(100, 19);
            this.uDateReturnDate.TabIndex = 35;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(292, 28);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(100, 20);
            this.uLabel4.TabIndex = 34;
            this.uLabel4.Text = "ultraLabel4";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(12, 28);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(100, 20);
            this.uLabel3.TabIndex = 33;
            this.uLabel3.Text = "ultraLabel3";
            // 
            // uGridSPTransferD
            // 
            this.uGridSPTransferD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSPTransferD.DisplayLayout.Appearance = appearance9;
            this.uGridSPTransferD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSPTransferD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridSPTransferD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSPTransferD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSPTransferD.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSPTransferD.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGridSPTransferD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSPTransferD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSPTransferD.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridSPTransferD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSPTransferD.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance18.TextHAlignAsString = "Left";
            this.uGridSPTransferD.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridSPTransferD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSPTransferD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridSPTransferD.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridSPTransferD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSPTransferD.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridSPTransferD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSPTransferD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSPTransferD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSPTransferD.Location = new System.Drawing.Point(12, 36);
            this.uGridSPTransferD.Name = "uGridSPTransferD";
            this.uGridSPTransferD.Size = new System.Drawing.Size(1040, 712);
            this.uGridSPTransferD.TabIndex = 38;
            this.uGridSPTransferD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToChgDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromChgDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 10;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(500, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel1.TabIndex = 5;
            this.ultraLabel1.Text = "~";
            // 
            // uDateToChgDate
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToChgDate.Appearance = appearance3;
            this.uDateToChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToChgDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateToChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToChgDate.Location = new System.Drawing.Point(516, 12);
            this.uDateToChgDate.MaskInput = "";
            this.uDateToChgDate.Name = "uDateToChgDate";
            this.uDateToChgDate.Size = new System.Drawing.Size(100, 19);
            this.uDateToChgDate.TabIndex = 4;
            // 
            // uDateFromChgDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromChgDate.Appearance = appearance4;
            this.uDateFromChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromChgDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateFromChgDate.DateTime = new System.DateTime(2011, 6, 1, 0, 0, 0, 0);
            this.uDateFromChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromChgDate.Location = new System.Drawing.Point(396, 12);
            this.uDateFromChgDate.MaskInput = "";
            this.uDateFromChgDate.Name = "uDateFromChgDate";
            this.uDateFromChgDate.Size = new System.Drawing.Size(100, 19);
            this.uDateFromChgDate.TabIndex = 3;
            this.uDateFromChgDate.Value = new System.DateTime(2011, 6, 1, 0, 0, 0, 0);
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(292, 12);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(100, 20);
            this.uLabel2.TabIndex = 2;
            this.uLabel2.Text = "ultraLabel2";
            // 
            // uComboPlant
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance20;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 9;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQUZ0018
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0018";
            this.Load += new System.EventHandler(this.frmEQUZ0018_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0018_Activated);
            this.Resize += new System.EventHandler(this.frmEQUZ0018_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReturnSPInventoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReturnDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSPTransferD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnChargeID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReturnDate;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToChgDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromChgDate;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboReturnSPInventoryCode;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnSPInventoryCode;
    }
}
