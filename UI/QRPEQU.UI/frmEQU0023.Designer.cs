﻿namespace QRPEQU.UI
{
    partial class frmEQU0023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0023));
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextPONumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPONumber = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInputPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInputPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uComboInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelGRSPInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uTextGRConfirmID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRConfirmName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateGRConfirmDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelGRConfirmID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelGRConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToGRDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromGRDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInputPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateGRConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextPONumber);
            this.uGroupBox1.Controls.Add(this.uLabelPONumber);
            this.uGroupBox1.Controls.Add(this.uComboInputPlant);
            this.uGroupBox1.Controls.Add(this.uLabelInputPlant);
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox1.Controls.Add(this.uComboInventory);
            this.uGroupBox1.Controls.Add(this.uLabelGRSPInventory);
            this.uGroupBox1.Controls.Add(this.uTextGRConfirmID);
            this.uGroupBox1.Controls.Add(this.uTextGRConfirmName);
            this.uGroupBox1.Controls.Add(this.uDateGRConfirmDate);
            this.uGroupBox1.Controls.Add(this.uLabelGRConfirmID);
            this.uGroupBox1.Controls.Add(this.uLabelGRConfirmDate);
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBox1.TabIndex = 11;
            // 
            // uTextPONumber
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPONumber.Appearance = appearance21;
            this.uTextPONumber.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPONumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPONumber.Location = new System.Drawing.Point(444, 56);
            this.uTextPONumber.MaxLength = 50;
            this.uTextPONumber.Name = "uTextPONumber";
            this.uTextPONumber.Size = new System.Drawing.Size(100, 21);
            this.uTextPONumber.TabIndex = 135;
            // 
            // uLabelPONumber
            // 
            this.uLabelPONumber.Location = new System.Drawing.Point(340, 56);
            this.uLabelPONumber.Name = "uLabelPONumber";
            this.uLabelPONumber.Size = new System.Drawing.Size(100, 20);
            this.uLabelPONumber.TabIndex = 134;
            this.uLabelPONumber.Text = "ultraLabel2";
            // 
            // uComboInputPlant
            // 
            this.uComboInputPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboInputPlant.MaxLength = 50;
            this.uComboInputPlant.Name = "uComboInputPlant";
            this.uComboInputPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboInputPlant.TabIndex = 133;
            this.uComboInputPlant.Text = "ultraComboEditor1";
            this.uComboInputPlant.AfterCloseUp += new System.EventHandler(this.uComboInputPlant_AfterCloseUp);
            this.uComboInputPlant.ValueChanged += new System.EventHandler(this.uComboInputPlant_ValueChanged);
            // 
            // uLabelInputPlant
            // 
            this.uLabelInputPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelInputPlant.Name = "uLabelInputPlant";
            this.uLabelInputPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelInputPlant.TabIndex = 132;
            this.uLabelInputPlant.Text = "ultraLabel1";
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 80);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 99;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uComboInventory
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboInventory.Appearance = appearance2;
            this.uComboInventory.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboInventory.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboInventory.Location = new System.Drawing.Point(444, 32);
            this.uComboInventory.MaxLength = 50;
            this.uComboInventory.Name = "uComboInventory";
            this.uComboInventory.Size = new System.Drawing.Size(120, 19);
            this.uComboInventory.TabIndex = 98;
            this.uComboInventory.Text = "ultraComboEditor1";
            // 
            // uLabelGRSPInventory
            // 
            this.uLabelGRSPInventory.Location = new System.Drawing.Point(340, 32);
            this.uLabelGRSPInventory.Name = "uLabelGRSPInventory";
            this.uLabelGRSPInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRSPInventory.TabIndex = 97;
            this.uLabelGRSPInventory.Text = "ultraLabel1";
            // 
            // uTextGRConfirmID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextGRConfirmID.Appearance = appearance15;
            this.uTextGRConfirmID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextGRConfirmID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextGRConfirmID.ButtonsRight.Add(editorButton1);
            this.uTextGRConfirmID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextGRConfirmID.Location = new System.Drawing.Point(116, 52);
            this.uTextGRConfirmID.MaxLength = 20;
            this.uTextGRConfirmID.Name = "uTextGRConfirmID";
            this.uTextGRConfirmID.Size = new System.Drawing.Size(100, 19);
            this.uTextGRConfirmID.TabIndex = 96;
            this.uTextGRConfirmID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextGRConfirmID_KeyDown);
            this.uTextGRConfirmID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextGRConfirmID_EditorButtonClick);
            this.uTextGRConfirmID.Click += new System.EventHandler(this.uTextGRConfirmID_Click);
            // 
            // uTextGRConfirmName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRConfirmName.Appearance = appearance3;
            this.uTextGRConfirmName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRConfirmName.Location = new System.Drawing.Point(220, 52);
            this.uTextGRConfirmName.Name = "uTextGRConfirmName";
            this.uTextGRConfirmName.ReadOnly = true;
            this.uTextGRConfirmName.Size = new System.Drawing.Size(100, 21);
            this.uTextGRConfirmName.TabIndex = 95;
            // 
            // uDateGRConfirmDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateGRConfirmDate.Appearance = appearance16;
            this.uDateGRConfirmDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateGRConfirmDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateGRConfirmDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateGRConfirmDate.Location = new System.Drawing.Point(684, 28);
            this.uDateGRConfirmDate.Name = "uDateGRConfirmDate";
            this.uDateGRConfirmDate.Size = new System.Drawing.Size(100, 19);
            this.uDateGRConfirmDate.TabIndex = 30;
            // 
            // uLabelGRConfirmID
            // 
            this.uLabelGRConfirmID.Location = new System.Drawing.Point(12, 52);
            this.uLabelGRConfirmID.Name = "uLabelGRConfirmID";
            this.uLabelGRConfirmID.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRConfirmID.TabIndex = 29;
            this.uLabelGRConfirmID.Text = "ultraLabel3";
            // 
            // uLabelGRConfirmDate
            // 
            this.uLabelGRConfirmDate.Location = new System.Drawing.Point(580, 28);
            this.uLabelGRConfirmDate.Name = "uLabelGRConfirmDate";
            this.uLabelGRConfirmDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRConfirmDate.TabIndex = 28;
            this.uLabelGRConfirmDate.Text = "ultraLabel4";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance8;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance17.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance17;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 88);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1040, 656);
            this.uGrid1.TabIndex = 33;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_ClickCellButton);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1080, 40);
            this.uGroupBoxSearchArea.TabIndex = 10;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(500, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 5;
            this.ultraLabel1.Text = "~";
            this.ultraLabel1.Visible = false;
            // 
            // uDateToGRDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToGRDate.Appearance = appearance4;
            this.uDateToGRDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToGRDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateToGRDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToGRDate.Location = new System.Drawing.Point(520, 12);
            this.uDateToGRDate.MaskInput = "";
            this.uDateToGRDate.Name = "uDateToGRDate";
            this.uDateToGRDate.Size = new System.Drawing.Size(100, 19);
            this.uDateToGRDate.TabIndex = 4;
            this.uDateToGRDate.Visible = false;
            // 
            // uDateFromGRDate
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromGRDate.Appearance = appearance20;
            this.uDateFromGRDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromGRDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateFromGRDate.DateTime = new System.DateTime(2011, 7, 1, 0, 0, 0, 0);
            this.uDateFromGRDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromGRDate.Location = new System.Drawing.Point(396, 12);
            this.uDateFromGRDate.MaskInput = "";
            this.uDateFromGRDate.Name = "uDateFromGRDate";
            this.uDateFromGRDate.Size = new System.Drawing.Size(100, 19);
            this.uDateFromGRDate.TabIndex = 3;
            this.uDateFromGRDate.Value = new System.DateTime(2011, 7, 1, 0, 0, 0, 0);
            this.uDateFromGRDate.Visible = false;
            // 
            // uLabelGRDate
            // 
            this.uLabelGRDate.Location = new System.Drawing.Point(292, 12);
            this.uLabelGRDate.Name = "uLabelGRDate";
            this.uLabelGRDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRDate.TabIndex = 2;
            this.uLabelGRDate.Text = "ultraLabel2";
            this.uLabelGRDate.Visible = false;
            // 
            // uComboPlant
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance19;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 9;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQU0023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0023";
            this.Load += new System.EventHandler(this.frmEQU0023_Load);
            this.Activated += new System.EventHandler(this.frmEQU0023_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0023_FormClosing);
            this.Resize += new System.EventHandler(this.frmEQU0023_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInputPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateGRConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateGRConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelGRConfirmID;
        private Infragistics.Win.Misc.UltraLabel uLabelGRConfirmDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToGRDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromGRDate;
        private Infragistics.Win.Misc.UltraLabel uLabelGRDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRConfirmID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRConfirmName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelGRSPInventory;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInputPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelInputPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPONumber;
        private Infragistics.Win.Misc.UltraLabel uLabelPONumber;
    }
}
