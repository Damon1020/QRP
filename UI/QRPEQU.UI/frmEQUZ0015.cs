﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQUZ0015.cs                                        */
/* 프로그램명   : 자재이동등록                                          */
/* 작성자       : 권종구, 코딩 : 남현식                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPEQU.UI
{
    public partial class frmEQUZ0015 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQUZ0015()
        {
            InitializeComponent();
        }

        private void frmEQUZ0015_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmEQUZ0015_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();
            InitText();
            
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }


        private void frmEQUZ0015_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("자재이동등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값지정
                uTextMoveChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextMoveChargeName.Text = m_resSys.GetString("SYS_USERNAME");


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                WinLabel lbl = new WinLabel();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMoveDate, "반납일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMoveChargeID, "반납담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정

                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid1, 0, "SPInventoryCode", "현재창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "SparePartCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "SparePartName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 50, Infragistics.Win.HAlign.Left
                    ,Infragistics.Win.VAlign.Middle,Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"","","");

                grd.mfSetGridColumn(this.uGrid1, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Qty", "현재고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 50, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ChgInventoryCode", "이동창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ChgQty", "이동수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "#,###", "nnnnnnnnnn", "0");
               

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄추가
                grd.mfAddRowGrid(this.uGrid1, 0);


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {


            }
            catch
            { }
            finally
            { }
        }
#endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;
                int intRowCheck = 0;

                // 필수입력사항 확인

                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;

                        if (this.uGrid1.Rows[i].Cells["SPInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGrid1.Rows[i].RowSelectorNumber + "번째 열의 현재창고를 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["SPInventoryCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGrid1.Rows[i].RowSelectorNumber + "번째 열의 SparePart코드를 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["SparePartCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["ChgInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGrid1.Rows[i].RowSelectorNumber + "번째 열의 이동창고를 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["ChgInventoryCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGrid1.Rows[i].RowSelectorNumber + "번째 열의 이동수량을 입력해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["ChgQty"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "하나 이상의 행을 입력하셔야 합니다", Infragistics.Win.HAlign.Center);
                    return;
                }


                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "선택한 정보를 저장하시겠습니까? "
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                

                //-------- 1.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 현재창고수량 (-) 처리 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                DataTable dtSPStock_Minus = clsSPStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtSPStock_Minus.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uGrid1.Rows[i].Cells["SPInventoryCode"].Value.ToString(); ;
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["Qty"] = "-" + this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStock_Minus.Rows.Add(row);
                    }
                }

                //-------- 2.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 이동창고수량 (+) 처리 -------//
                DataTable dtSPStock_Plus = clsSPStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtSPStock_Plus.NewRow();

                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["SPInventoryCode"] = this.uGrid1.Rows[i].Cells["ChgInventoryCode"].Value.ToString(); ;
                        row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                        row["Qty"] = this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtSPStock_Plus.Rows.Add(row);
                    }
                }


                //-------- 3.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 : 현재창고수량 (-) -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                DataTable dtSPStockMoveHist_Minus = clsSPStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                           row = dtSPStockMoveHist_Minus.NewRow();

                            row["MoveGubunCode"] = "M05";       //자재이동일 경우는 "M05"
                            row["DocCode"] = "";
                            row["MoveDate"] = this.uDateMoveDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["MoveChargeID"] = this.uTextMoveChargeID.Text;
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uGrid1.Rows[i].Cells["SPInventoryCode"].Value.ToString();
                            row["EquipCode"] = "";
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["MoveQty"] = "-" + this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStockMoveHist_Minus.Rows.Add(row);
                    }
                }

                //-------- 4.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 : 이동창고수량 (-) -------//

                DataTable dtSPStockMoveHist_Plus = clsSPStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtSPStockMoveHist_Plus.NewRow();

                        row["MoveGubunCode"] = "M05";       //자재이동일 경우는 "M05"
                        row["DocCode"] = "";
                        row["MoveDate"] = this.uDateMoveDate.DateTime.Date.ToString("yyyy-MM-dd");
                        row["MoveChargeID"] = this.uTextMoveChargeID.Text;
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["SPInventoryCode"] = this.uGrid1.Rows[i].Cells["ChgInventoryCode"].Value.ToString();
                        row["EquipCode"] = "";
                        row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                        row["MoveQty"] =  this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtSPStockMoveHist_Plus.Rows.Add(row);
                    }
                }




                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SAVESparePartStock), "SAVESparePartStock");
                QRPEQU.BL.EQUSPA.SAVESparePartStock clsSAVESparePartStock = new QRPEQU.BL.EQUSPA.SAVESparePartStock();
                brwChannel.mfCredentials(clsSAVESparePartStock);
                string rtMSG = clsSAVESparePartStock.mfSaveMove(dtSPStock_Minus, dtSPStock_Plus, dtSPStockMoveHist_Minus, dtSPStockMoveHist_Plus, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                InitText();
                InitCombo();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfExcel()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion


        private void uText2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid1.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();

                if (strPlantCode == "")
                    return;


                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);
                DataTable dtSPInventory = clsSPInventory.mfReadSPInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "SPInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtSPInventory);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "ChgInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtSPInventory);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);




                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                //컬럼
                String strColumn = e.Cell.Column.Key;
                int intIndex = e.Cell.Row.Index;

                string strPlantCode = uComboPlant.Value.ToString();
                //string strSPInventoryCode = uGrid1.ActiveCell.Row.Cells["SPInventoryCode"].Value.ToString();
                string strSPInventoryCode = e.Cell.Row.Cells["SPInventoryCode"].Value.ToString();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);
              

                if (strSPInventoryCode == "")
                    return;

                if (strColumn == "SPInventoryCode")
                {
                    
                    DataTable dtSPStock = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strSPInventoryCode, m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGrid1, intIndex, "SparePartCode", "", "", dtSPStock);
                    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                }

                
                //줄번호
                if (strColumn == "SparePartCode")
                {

                    string strSparePartCode = uGrid1.ActiveCell.Row.Cells["SparePartCode"].Value.ToString();

                    if (strSparePartCode == "")
                        return;

                    DataTable dtSPStockDetail = clsSPStock.mfReadSPStock_Detail(strPlantCode, strSPInventoryCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));

                    uGrid1.ActiveCell.Row.Cells["SparePartName"].Value = dtSPStockDetail.Rows[0]["SparePartName"].ToString();
                    uGrid1.ActiveCell.Row.Cells["Qty"].Value = dtSPStockDetail.Rows[0]["Qty"].ToString();
                    uGrid1.ActiveCell.Row.Cells["Spec"].Value = dtSPStockDetail.Rows[0]["Spec"].ToString();
                    uGrid1.ActiveCell.Row.Cells["UnitCode"].Value = dtSPStockDetail.Rows[0]["UnitCode"].ToString();
                    uGrid1.ActiveCell.Row.Cells["UnitName"].Value = dtSPStockDetail.Rows[0]["UnitName"].ToString();
                }


                if (strColumn == "ChgInventoryCode")
                {

                    string strChgInventoryCode = e.Cell.Row.Cells["ChgInventoryCode"].Value.ToString();

                    if (strSPInventoryCode == strChgInventoryCode)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "현재창고와 다른 창고를  선택하세요", Infragistics.Win.HAlign.Center);


                        this.uGrid1.ActiveCell.Row.Cells["ChgInventoryCode"].Value = "";
                        return;
                    }
                }

                if (strColumn == "ChgQty")
                {

                    int intChgQty = Convert.ToInt32(e.Cell.Row.Cells["ChgQty"].Value.ToString());
                    int intQty = Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value.ToString());

                    if (intChgQty > intQty)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "현재수량보다 더 많은 수량을 이동할 순 없습니다", Infragistics.Win.HAlign.Center);


                        this.uGrid1.ActiveCell.Row.Cells["ChgInventoryCode"].Value = "";
                        return;
                    }
                }
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


       

      
    }
}
