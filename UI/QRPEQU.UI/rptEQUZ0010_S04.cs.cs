﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Data;
namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S04.
    /// </summary>
    public partial class rptEQUZ0010_S04 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0010_S04()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }
        public rptEQUZ0010_S04(DataTable dtMainSubEquip)
        {
            //
            // Required for Windows Form Designer support
            //
            this.DataSource = dtMainSubEquip;
            InitializeComponent();
        }
    }
}
