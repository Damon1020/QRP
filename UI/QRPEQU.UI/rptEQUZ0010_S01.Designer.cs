﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S01.
    /// </summary>
    partial class rptEQUZ0010_S01
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptEQUZ0010_S01));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textProgressDesc = new DataDynamics.ActiveReports.TextBox();
            this.textSetupDate = new DataDynamics.ActiveReports.TextBox();
            this.grpHeader = new DataDynamics.ActiveReports.GroupHeader();
            this.label18 = new DataDynamics.ActiveReports.Label();
            this.label22 = new DataDynamics.ActiveReports.Label();
            this.label21 = new DataDynamics.ActiveReports.Label();
            this.grpFooter = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textProgressDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSetupDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textProgressDesc,
            this.textSetupDate});
            this.detail.Height = 0.3458333F;
            this.detail.Name = "detail";
            // 
            // textProgressDesc
            // 
            this.textProgressDesc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProgressDesc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProgressDesc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProgressDesc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProgressDesc.DataField = "ProgressDesc";
            this.textProgressDesc.Height = 0.35F;
            this.textProgressDesc.Left = 1.519F;
            this.textProgressDesc.Name = "textProgressDesc";
            this.textProgressDesc.Style = "font-size: 8pt; vertical-align: middle";
            this.textProgressDesc.Text = null;
            this.textProgressDesc.Top = 0F;
            this.textProgressDesc.Width = 5.636F;
            // 
            // textSetupDate
            // 
            this.textSetupDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.DataField = "SetupDate";
            this.textSetupDate.Height = 0.35F;
            this.textSetupDate.Left = 0.04F;
            this.textSetupDate.Name = "textSetupDate";
            this.textSetupDate.Style = "font-size: 8pt; text-align: center; vertical-align: middle";
            this.textSetupDate.Text = null;
            this.textSetupDate.Top = 0F;
            this.textSetupDate.Width = 1.479F;
            // 
            // grpHeader
            // 
            this.grpHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label18,
            this.label22,
            this.label21});
            this.grpHeader.Height = 0.542F;
            this.grpHeader.Name = "grpHeader";
            // 
            // label18
            // 
            this.label18.Height = 0.2F;
            this.label18.HyperLink = null;
            this.label18.Left = 0.04000024F;
            this.label18.Name = "label18";
            this.label18.Style = "font-weight: bold; vertical-align: middle";
            this.label18.Text = "3 . SET-UP 진행사항";
            this.label18.Top = 0.05900007F;
            this.label18.Width = 2.134F;
            // 
            // label22
            // 
            this.label22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Height = 0.2F;
            this.label22.HyperLink = null;
            this.label22.Left = 0.04000024F;
            this.label22.Name = "label22";
            this.label22.Style = "font-weight: bold; text-align: center; vertical-align: middle";
            this.label22.Text = "일자";
            this.label22.Top = 0.342F;
            this.label22.Width = 1.479F;
            // 
            // label21
            // 
            this.label21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Height = 0.2F;
            this.label21.HyperLink = null;
            this.label21.Left = 1.519F;
            this.label21.Name = "label21";
            this.label21.Style = "font-weight: bold; text-align: center; vertical-align: middle";
            this.label21.Text = "진행현황";
            this.label21.Top = 0.342F;
            this.label21.Width = 5.636F;
            // 
            // grpFooter
            // 
            this.grpFooter.Height = 0F;
            this.grpFooter.Name = "grpFooter";
            // 
            // rptEQUZ0010_S01
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.184F;
            this.Sections.Add(this.grpHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.grpFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textProgressDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSetupDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.GroupHeader grpHeader;
        private DataDynamics.ActiveReports.GroupFooter grpFooter;
        private DataDynamics.ActiveReports.Label label18;
        private DataDynamics.ActiveReports.Label label22;
        private DataDynamics.ActiveReports.Label label21;
        private DataDynamics.ActiveReports.TextBox textProgressDesc;
        private DataDynamics.ActiveReports.TextBox textSetupDate;
    }
}
