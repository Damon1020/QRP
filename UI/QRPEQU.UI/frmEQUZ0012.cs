﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0012.cs                                        */
/* 프로그램명   : 수리완료등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using Microsoft.VisualBasic;
namespace QRPEQU.UI
{
    public partial class frmEQUZ0012 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
        private string strRepairReqCode;
        private string strPlantCode;
        private string strFormName;

        public frmEQUZ0012()
        {
            InitializeComponent();
        }

        private string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }
        public string RepairReqCode
        {
            get { return strRepairReqCode; }
            set { strRepairReqCode = value; }
        }
        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }

        #region Initialize
        private void frmEQUZ0012_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("수리완료등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitGrid();
            InitComboBox();

            if (FormName != null)
            {
                Point point = new Point(0, 0);
                this.Location = point;
            }            
            InitButton();
            InitValue();

            if (RepairReqCode != null && !RepairReqCode.Equals(string.Empty))
            {
                mfSelectRepairReq();
            }


        }

        private void frmEQUZ0012_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "수리정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.Appearance.FontData.SizeInPoints = 9;


                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.DETAIL, "SparePart사용정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox4.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.Appearance.FontData.SizeInPoints = 9;

                // 그룹박스 접은상태로

                uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchRepairDay, "수리일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectDay, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelUseDay, "사용일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUsePerson, "사용담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUnusual, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelRepairDay, "수리일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairUser, "수리자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairResult, "수리결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStopType, "설비정지유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairUnusual, "수리특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelUseSparePart, "사용SparePart", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region Header
                // 수리 완료등록 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridRepairResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Extended, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairReqCode", "수리요청번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairReqDate", "점검일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                #endregion

                #region Detail
                // 사용 SparePart Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridRepairUseSP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default, Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default
                    , Infragistics.Win.UltraWinGrid.FilterUIType.Default, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "RepairReqCode", "수리요청번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "CurSparePartCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "CurSparePartName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "CurInputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "CurSpec", "구성품Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "CurMaker", "구성품Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgSPInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgSPInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgSparePartCode", "교체구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgSparePartName", "교체구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "AvailQty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgInputQty", "사용량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgSpec", "사용구성품Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgMaker", "사용구성품Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "ChgDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairUseSP, 0, "CancelFlag", "취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                // Add Row
                //wGrid.mfAddRowGrid(this.uGridRepairUseSP, 0);
                #endregion

                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                wGrid.mfSetGridColumnValueList(this.uGridRepairUseSP, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);

                // Set FontSize
                this.uGridRepairResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridRepairResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridRepairUseSP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridRepairUseSP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridRepairUseSP.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridRepairUseSP.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                wGrid.mfAddRowGrid(this.uGridRepairUseSP, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextRepairReqDate.Text = "";

                this.uTextUseSPChgID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextUseSPChgName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextUseSPEtcDesc.Text = "";

                this.uTextRepairName.Text = "";
                this.uCheckCCSReqFlag.CheckedValue = false;

                //RepairReqCode = string.Empty;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Common Event

        public void mfSearch()
        {
            this.uGroupBoxContentsArea.Expanded = false;

            for (int i = 0; i < uGridRepairUseSP.Rows.Count; i++)
            {
                uGridRepairUseSP.Rows[i].Delete(false);
            }
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            try
            {
                #region Indispensable
                if (uComboSearchPlant.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uDateSearchFromDate.Value == null || this.uDateSearchFromDate.Value.ToString().Equals(string.Empty))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M000209", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }
                if (this.uDateSearchToDate.Value == null || this.uDateSearchToDate.Value.ToString().Equals(string.Empty))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M000218", Infragistics.Win.HAlign.Right);

                    this.uDateSearchToDate.DropDown();
                    return;
                }
                if (this.uDateSearchFromDate.DateTime.Date > this.uDateSearchToDate.DateTime.Date)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M000211", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }

                #endregion

                #region Var
                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(uDateSearchFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(uDateSearchToDate.Value).ToString("yyyy-MM-dd");
                
                string strStation = uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboEquipLargeType.Value.ToString();
                string strEquipGroupCode = uComboSearchEquipGroup.Value.ToString();

                ////string strEquipProc = uComboSearchEquipProcess.Value.ToString();
                ////string strArea = uComboSearchArea.Value.ToString();
                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                #region BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairUseSP), "RepairUseSP");
                QRPEQU.BL.EQUREP.RepairUseSP clsEquip = new QRPEQU.BL.EQUREP.RepairUseSP();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipRepairEndH(strPlantCode,strFromDate,strToDate,
                                                                    strStation,strEquipLocCode,strProcessGroup,strEquipLargeTypeCode,strEquipGroupCode,
                                                                    m_resSys.GetString("SYS_LANG"));


                uGridRepairResult.Refresh();
                uGridRepairResult.DataSource = dtEquip;
                uGridRepairResult.DataBind();
                
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /////////////
                ///////////// 
                

                if (dtEquip.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridRepairResult, 0);
                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            titleArea.Focus();

            string strLang = m_resSys.GetString("SYS_LANG");

            #region Indispensable
            if (!uGroupBoxContentsArea.Expanded)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M001045", Infragistics.Win.HAlign.Right);
                uGridRepairResult.Focus();
                return;
            }

            if (RepairReqCode == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M001045", Infragistics.Win.HAlign.Right);
                uGridRepairResult.Focus();
                return;
            }

            if (uTextUseSPChgID.Text == string.Empty || uTextUseSPChgName.Text == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M000616", Infragistics.Win.HAlign.Right);
                uTextUseSPChgID.Focus();
                return;
            }
            if (uDateUseSPChgDate.Value.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M000620", Infragistics.Win.HAlign.Right);
                uDateUseSPChgDate.Focus();
                return;
            }

            //if (uGridDurableMatD.Rows.Count == 0)
            //{
            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //                , "확인창", "확인창", "저장할 수리출고구성 정보가 없습니다.", Infragistics.Win.HAlign.Right);
            //    uGridDurableMatD.Focus();
            //    return;
            //}

           
            
            #endregion

            try
            {
                

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairUseSP), "RepairUseSP");
                QRPEQU.BL.EQUREP.RepairUseSP clsRepairUseSP = new QRPEQU.BL.EQUREP.RepairUseSP();
                brwChannel.mfCredentials(clsRepairUseSP);

                string strUseSPChgDate = Convert.ToDateTime(uDateUseSPChgDate.Value).ToString("yyyy-MM-dd");
                string strUseSPChgID = uTextUseSPChgID.Text;
                string strEquipCode = this.uTextEquipCode.Text;
                

                #region Header
                DataTable dtUseSPH = clsRepairUseSP.mfDataSetInfoH();

                DataRow drUseSPH = dtUseSPH.NewRow();

                drUseSPH["PlantCode"] = PlantCode;
                drUseSPH["RepairReqCode"] = RepairReqCode;
                drUseSPH["UseSPChgDate"] = strUseSPChgDate;
                drUseSPH["UseSPChgID"] = strUseSPChgID;
                drUseSPH["UseSPEtcDesc"] = uTextUseSPEtcDesc.Text;

                dtUseSPH.Rows.Add(drUseSPH);
                #endregion

                #region Detail

                //  해당설비의 구성품
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);
                DataTable dtSPBOM = new DataTable();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                DataTable dtStock = new DataTable();

                DataTable dtUseSPD = clsRepairUseSP.mfDataSetInfoD();

                for (int i = 0; i < uGridRepairUseSP.Rows.Count; i++)
                {
                    if (!uGridRepairUseSP.Rows[i].Hidden)
                    {
                        if (Convert.ToBoolean(this.uGridRepairUseSP.Rows[i].GetCellValue("CancelFlag")).Equals(false) && this.uGridRepairUseSP.Rows[i].RowSelectorAppearance.Image != null
                            && this.uGridRepairUseSP.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            this.uGridRepairUseSP.Rows[i].RowSelectorAppearance.Image = null;
                        }

                        if (this.uGridRepairUseSP.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            #region 필수입력사항
                            int intIndex = this.uGridRepairUseSP.Rows[i].RowSelectorNumber;
                            if (uGridRepairUseSP.Rows[i].GetCellValue("ChgSPInventoryCode").ToString() == string.Empty)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M001133", Infragistics.Win.HAlign.Right);
                                uGridRepairUseSP.Focus();
                                return;
                            }
                            if (uGridRepairUseSP.Rows[i].GetCellValue("ChgSparePartCode").ToString() == string.Empty)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000295", Infragistics.Win.HAlign.Right);
                                uGridRepairUseSP.Focus();
                                return;
                            }
                            if (uGridRepairUseSP.Rows[i].GetCellValue("ChgInputQty").ToString() == string.Empty ||
                                uGridRepairUseSP.Rows[i].GetCellValue("ChgInputQty").ToString() == "0")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000307", Infragistics.Win.HAlign.Right);
                                uGridRepairUseSP.Focus();
                                return;
                            }

                            if (!Information.IsNumeric(uGridRepairUseSP.Rows[i].GetCellValue("ChgInputQty")))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M001083", Infragistics.Win.HAlign.Right);
                                uGridRepairUseSP.Focus();
                                return;
                            }
                            if (Convert.ToInt32(uGridRepairUseSP.Rows[i].GetCellValue("ChgInputQty")) <= 0)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M001083", Infragistics.Win.HAlign.Right);
                                uGridRepairUseSP.Focus();
                                return;
                            }

                            #region 수량체크
                            if (!Convert.ToBoolean(this.uGridRepairUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                            {
                                WinGrid wGrid = new WinGrid();

                                if (!this.uGridRepairUseSP.Rows[i].GetCellValue("CurSparePartCode").ToString().Equals(string.Empty))
                                {
                                    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridRepairUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepairUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                            , intIndex + msg.GetMessge_Text("M000471",strLang), Infragistics.Win.HAlign.Right);

                                        #region DropDown
                                        DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(PlantCode, uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
                                        this.uGridRepairUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();

                                        string strValue = "SparePartCode,SparePartName,InputQty,UnitName,Spec,Maker";
                                        string strText = "구성품코드,구성품명,Qty,단위,Spec,Maker";

                                        wGrid.mfSetGridColumnValueGridList(this.uGridRepairUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                                        , "SparePartCode", "SparePartName", dtEquipSPBOM);
                                        #endregion

                                        this.uGridRepairUseSP.Rows[i].Cells["CurInputQty"].Value = 0;
                                        this.uGridRepairUseSP.Rows[i].Cells["CurSparePartCode"].Value = string.Empty;
                                        this.uGridRepairUseSP.ActiveCell = this.uGridRepairUseSP.Rows[i].Cells["CurSparePartName"];
                                        this.uGridRepairUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                }

                                dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridRepairUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridRepairUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepairUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001006",strLang)
                                                        , intIndex + msg.GetMessge_Text("M000457",strLang), Infragistics.Win.HAlign.Right);

                                    #region DropDown
                                    DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, this.uGridRepairUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));
                                    string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                                    string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                                    //GridList 그리드에 삽입
                                    wGrid.mfSetGridCellValueGridList(this.uGridRepairUseSP, 0, i, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                                        , "SparePartCode", "SparePartName", dtChgSP);
                                    #endregion

                                    this.uGridRepairUseSP.Rows[i].Cells["AvailQty"].Value = 0;
                                    this.uGridRepairUseSP.Rows[i].Cells["ChgInputQty"].Value = 0;
                                    this.uGridRepairUseSP.Rows[i].Cells["ChgSparePartCode"].Value = string.Empty;
                                    this.uGridRepairUseSP.ActiveCell = this.uGridRepairUseSP.Rows[i].Cells["ChgSparePartName"];
                                    this.uGridRepairUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                            }
                            else
                            {
                                dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridRepairUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepairUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000746",strLang)
                                                        , intIndex + msg.GetMessge_Text("M000506",strLang), Infragistics.Win.HAlign.Right);
                                    this.uGridRepairUseSP.ActiveCell = this.uGridRepairUseSP.Rows[i].Cells["CancelFlag"];
                                    return;
                                }
                                if (!this.uGridRepairUseSP.Rows[i].GetCellValue("CurSparePartCode").ToString().Equals(string.Empty))
                                {
                                    dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridRepairUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridRepairUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepairUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Information,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            ,msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000746",strLang)
                                                            , intIndex + msg.GetMessge_Text("M000450",strLang), Infragistics.Win.HAlign.Right);

                                        this.uGridRepairUseSP.ActiveCell = this.uGridRepairUseSP.Rows[i].Cells["CancelFlag"];
                                        return;
                                    }
                                }
                            }
                            #endregion

                            #endregion

                            DataRow drRepairD = dtUseSPD.NewRow();

                            if (uGridRepairUseSP.Rows[i].GetCellValue("PlantCode").ToString() != string.Empty)
                            {
                                drRepairD["PlantCode"] = uGridRepairUseSP.Rows[i].GetCellValue("PlantCode");
                                drRepairD["RepairReqCode"] = uGridRepairUseSP.Rows[i].GetCellValue("RepairReqCode");
                                drRepairD["Seq"] = uGridRepairUseSP.Rows[i].GetCellValue("Seq");
                                drRepairD["CancelFlag"] = Convert.ToBoolean(uGridRepairUseSP.Rows[i].GetCellValue("CancelFlag")) == false ? "F" : "T";
                            }
                            else
                            {
                                drRepairD["PlantCode"] = PlantCode;
                                drRepairD["RepairReqCode"] = RepairReqCode;
                                drRepairD["Seq"] = 0;
                                drRepairD["CancelFlag"] = "F";
                            }
                            drRepairD["EquipCode"] = this.uTextEquipCode.Text;
                            drRepairD["CurSparePartCode"] = uGridRepairUseSP.Rows[i].GetCellValue("CurSparePartCode");
                            drRepairD["CurInputQty"] = uGridRepairUseSP.Rows[i].GetCellValue("CurInputQty");
                            drRepairD["ChgSPInventoryCode"] = uGridRepairUseSP.Rows[i].GetCellValue("ChgSPInventoryCode");
                            drRepairD["ChgSparePartCode"] = uGridRepairUseSP.Rows[i].GetCellValue("ChgSparePartCode");
                            drRepairD["ChgInputQty"] = uGridRepairUseSP.Rows[i].GetCellValue("ChgInputQty");
                            drRepairD["UnitCode"] = uGridRepairUseSP.Rows[i].GetCellValue("UnitCode");
                            drRepairD["ChgDesc"] = uGridRepairUseSP.Rows[i].GetCellValue("ChgDesc");

                            dtUseSPD.Rows.Add(drRepairD);

                        }
                    }
                }
                #endregion

                #region Save
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000914",
                                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                System.Windows.Forms.DialogResult result;


                string strRtn = clsRepairUseSP.mfSaveEquipRepairUseSP(dtUseSPH, dtUseSPD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);

                if (ErrRtn.ErrNum != 0)
                {
                    string strMeg = "";
                    if(ErrRtn.ErrMessage.Equals(string.Empty))
                        strMeg = msg.GetMessge_Text("M000953",strLang);
                    else
                        strMeg = ErrRtn.ErrMessage;

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMeg,
                                                 Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
                else
                {
                    InitValue();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridRepairResult.Rows.Count == 0 && (this.uGridRepairUseSP.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridRepairResult);

                if (this.uGridRepairUseSP.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridRepairUseSP);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region UI Event
        // ExpandableGroupBox 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 160);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridRepairResult.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridRepairResult.Height = 720;

                    for (int i = 0; i < uGridRepairResult.Rows.Count; i++)
                    {
                        uGridRepairResult.Rows[i].Fixed = false;
                    }

                    foreach (Control ctrl in uGroupBox1.Controls)
                    {
                        if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                        {
                            ctrl.Text = string.Empty;
                        }
                    }
                    foreach (Control ctrl in uGroupBox4.Controls)
                    {
                        if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                        {
                            ctrl.Text = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        // 행삭제 버튼 클릭 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridRepairUseSP.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridRepairUseSP.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridRepairUseSP.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Combo
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinComboEditor wCombo = new WinComboEditor();

            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                brwChannel.mfCredentials(clsArea);

                //DataTable dtArea = clsArea.mfReadAreaCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                //uComboSearchArea.ValueList.Reset();
                //wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                //    , "AreaCode", "AreaName", dtArea);

                brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);

                DataTable dtStation = clsStation.mfReadStationCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                uComboSearchStation.ValueList.Reset();
                wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "StationCode", "StationName", dtStation);

                //brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                //brwChannel.mfCredentials(clsEquipProcGubun);

                //DataTable dtEquipProcGugun = clsEquipProcGubun.mfReadProGubunCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                //uComboSearchEquipProcess.ValueList.Reset();
                //wCombo.mfSetComboEditor(this.uComboSearchEquipProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                //    , "EquipProcGubunCode", "EquipProcGubunName", dtEquipProcGugun);


            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //코드와 텍스트 저장
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }
                if (!strChk.Equals(string.Empty))
                {
                    //System resourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor com = new WinComboEditor();

                    QRPBrowser brwChannel = new QRPBrowser();

                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboEquipLoc.Items.Clear();

                    WinComboEditor wCombo = new WinComboEditor();

                    wCombo.mfSetComboEditor(this.uComboEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
                


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                string strCode = this.uComboEquipLoc.Value.ToString();

                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    //콤보아이템에 있는 경우
                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboEquipLargeType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLargeType.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    //콤보아이템에 해당하는 결과면
                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        #endregion
        

        #region Grid Event
        private void uGridRepairUseSP_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                WinMessageBox msg = new WinMessageBox();

                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridRepairUseSP, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                #region 기존구성품 수량
                if (e.Cell.Column.Key == "CurInputQty")
                {
                    if (e.Cell.Tag != null && Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M000121", "M000345", Infragistics.Win.HAlign.Right);

                       
                        e.Cell.Value = e.Cell.Tag;
                    }
                }

                #endregion

                #region 사용량

                if (e.Cell.Column.Key.ToString() == "ChgInputQty" 
                    && !e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString().Equals(string.Empty)
                    && Convert.ToInt32(e.Cell.Value) > 0)
                {
                    int intCurInputQty = Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Tag); //구성품수량
                    int intAvailQty = Convert.ToInt32(e.Cell.Row.Cells["AvailQty"].Value);       //재고수량
                    int intChgInputQty = Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value); //입력수량


                    if (!e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty) && intChgInputQty > intCurInputQty)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000121", "M000315", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                        
                     
                        return;
                    }

                    if (intChgInputQty > intAvailQty)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000121", "M000158", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                        

                        return;
                    }

                    #region 중복구성품재고구분

                    //현재 입력한교체수량
                    int intChgQty = 0;
                    int intQty = 0;

                    //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                    for (int i = 0; i < this.uGridRepairUseSP.Rows.Count; i++)
                    {
                        if (this.uGridRepairUseSP.Rows[i].RowSelectorAppearance.Image != null
                            && this.uGridRepairUseSP.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            && this.uGridRepairUseSP.Rows[i].Hidden.Equals(false))
                        {
                            //if (!i.Equals(e.Cell.Row.Index)
                            //    && e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value)
                            //    && e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value))
                            //{
                            if (e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridRepairUseSP.Rows[i].Cells["ChgSparePartCode"].Value) &&
                                e.Cell.Row.Cells["ChgSPInventoryName"].Value.Equals(this.uGridRepairUseSP.Rows[i].Cells["ChgSPInventoryName"].Value))
                            {
                                if (intQty.Equals(0))
                                {
                                    intQty = Convert.ToInt32(this.uGridRepairUseSP.Rows[i].Cells["AvailQty"].Value);
                                }
                                intChgQty = intChgQty + Convert.ToInt32(this.uGridRepairUseSP.Rows[i].Cells["ChgInputQty"].Value);


                            }
                        }
                    }

                    if (intChgQty > intQty)
                    {

                        string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;
                        string strLang = m_resSys.GetString("SYS_LANG");

                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000617",strLang)
                                               , msg.GetMessge_Text("M000892",strLang) + strQty + msg.GetMessge_Text("M000010",strLang), Infragistics.Win.HAlign.Right);

                        //e.Cell.Row.Cells["ChgSparePartCode"].Value = string.Empty;
                        //e.Cell.Row.Cells["ChgSparePartName"].Value = string.Empty;
                        //e.Cell.Row.Cells["Qty"].Value = 0;
                        //e.Cell.Row.Cells["Qty"].Tag = 0;
                        e.Cell.Value = 0;
                        //e.Cell.Row.Cells["UnitCode"].Value = string.Empty;

                        return;
                    }


                    #endregion
                }

                #endregion

                ////if (e.Cell.Column.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
                ////{
                ////    if (e.Cell.Value.ToString() != string.Empty)
                ////    {
                ////        uGridRepairUseSP.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Key.Replace("Name", "Code")].Value = e.Cell.Value;
                ////    }
                ////    else
                ////    {
                ////        uGridRepairUseSP.Rows[e.Cell.Row.Index].Cells[e.Cell.Column.Key.Replace("Name", "Code")].Value = "";
                ////    }

                ////    if (e.Cell.Column.Key.ToString() == "CurSparePartName")
                ////    {
                ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                ////        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                ////        QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                ////        brwChannel.mfCredentials(clsEquipSPBOM);

                ////        string strSparePartCode = uGridRepairUseSP.Rows[e.Cell.Row.Index].Cells["CurSparePartCode"].Value.ToString();
                ////        string strLang = m_resSys.GetString("SYS_LANG");
                ////        DataTable dtDurableMat = clsEquipSPBOM.mfReadEquipSTBOMDetail(PlantCode, uTextEquipCode.Text, strSparePartCode, strLang);

                ////        int intInputQty = Convert.ToInt32(dtDurableMat.Rows[0]["InputQty"].ToString());
                ////        int intChgStandbyQty = Convert.ToInt32(dtDurableMat.Rows[0]["ChgStandbyQty"].ToString());

                ////        if (dtDurableMat.Rows.Count != 0)
                ////        {
                ////            uGridRepairUseSP.Rows[e.Cell.Row.Index].Cells["CurInputQty"].Value = intInputQty - intChgStandbyQty;
                ////        }
                ////    }
                ////}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridRepairUseSP_AfterDataBinding()
        {
            try
            {
                for (int i = 0; i < uGridRepairUseSP.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridRepairUseSP.Rows[i].GetCellValue("CancelFlag")))
                    {
                        uGridRepairUseSP.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;
                        uGridRepairUseSP.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    else
                    {
                        for (int j = 0; j < uGridRepairUseSP.Rows[i].Cells.Count; j++)
                        {
                            if (uGridRepairUseSP.Rows[i].Cells["PlantCode"].Value.ToString() != string.Empty)
                            {
                                if (uGridRepairUseSP.Rows[i].Cells[j].Column.Key.ToString() == "CancelFlag")
                                {
                                    uGridRepairUseSP.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }
                                else
                                {
                                    uGridRepairUseSP.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridRepairUseSP_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            e.Row.Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }

        private void uGridRepairResult_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            WinGrid wGrid = new WinGrid();

            foreach (Control ctrl in uGroupBoxContentsArea.Controls)
            {
                if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                {
                    ctrl.Text = string.Empty;
                }
            }

            RepairReqCode = string.Empty;
            PlantCode = string.Empty;

            uDateUseSPChgDate.Value = DateTime.Now;
            uTextUseSPChgID.Text = m_resSys.GetString("SYS_USERID");
            uTextUseSPChgName.Text = m_resSys.GetString("SYS_USERNAME");

            uGridRepairUseSP.Refresh();
            wGrid.mfAddRowGrid(this.uGridRepairUseSP, 0);

            if (e.Cell.Row.GetCellValue("RepairReqCode").ToString() == string.Empty)
                return;

            RepairReqCode = e.Cell.Row.GetCellValue("RepairReqCode").ToString();
            PlantCode = e.Cell.Row.GetCellValue("PlantCode").ToString();

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                #region Read Header
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairUseSP), "RepairUseSP");
                QRPEQU.BL.EQUREP.RepairUseSP clsEquip = new QRPEQU.BL.EQUREP.RepairUseSP();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipRepairEndUseSPDetail(PlantCode, RepairReqCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    uGroupBoxContentsArea.Expanded = false;

                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000387", "M000382", Infragistics.Win.HAlign.Right);

                    return;
                }

                uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();
                uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                uTextRepairReqDate.Text = dtEquip.Rows[0]["RepairReqDate"].ToString();
                uTextRepairEndDate.Text = dtEquip.Rows[0]["RepairEndDate"].ToString();
                uTextRepairID.Text = dtEquip.Rows[0]["RepairID"].ToString();
                uTextRepairName.Text = dtEquip.Rows[0]["RepairName"].ToString();
                uTextRepairResultName.Text = dtEquip.Rows[0]["RepairResultName"].ToString();
                //uTextEquipStopTypeName.Text = dtEquip.Rows[0]["EquipStopTypeName"].ToString();
                uTextRepairDesc.Text = dtEquip.Rows[0]["RepairDesc"].ToString();

                if (dtEquip.Rows[0]["UseSPChgDate"].ToString() == string.Empty)
                    uDateUseSPChgDate.Value = DateTime.Now;
                else
                    uDateUseSPChgDate.Value = dtEquip.Rows[0]["UseSPChgDate"].ToString();

                if (dtEquip.Rows[0]["UseSPChgID"].ToString() == string.Empty)
                {
                    uTextUseSPChgID.Text = m_resSys.GetString("SYS_USERID");
                    uTextUseSPChgName.Text = m_resSys.GetString("SYS_USERNAME");
                }
                else
                {
                    uTextUseSPChgID.Text = dtEquip.Rows[0]["UseSPChgID"].ToString();
                    uTextUseSPChgName.Text = dtEquip.Rows[0]["UseSPChgName"].ToString();
                }

                uTextUseSPEtcDesc.Text = dtEquip.Rows[0]["UseSPEtcDesc"].ToString();

                PlantCode = dtEquip.Rows[0]["PlantCode"].ToString();
                RepairReqCode = dtEquip.Rows[0]["RepairReqCode"].ToString();
                #endregion

                #region Detail Grid DropDown Binding

                brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsInventory);
                DataTable dtInventory = clsInventory.mfReadMASSPInventoryCombo(PlantCode, m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(uGridRepairUseSP, 0, "ChgSPInventoryName", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInventory);

                brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);

                DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(PlantCode, uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
                //if (dtEquipSPBOM.Rows.Count == 0)
                //{
                //    this.MdiParent.Cursor = Cursors.Default;
                //    m_ProgressPopup.mfCloseProgressPopup(this);
                //    uGroupBoxContentsArea.Expanded = false;
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                            , "확인창", "설비정보 확인", "해당설비에 대한 SpareaPart BOM정보가 없습니다. 설비관리기준정보 - 설비구성품정보로 등록하세요.", Infragistics.Win.HAlign.Right);


                //    return;

                //}
                this.uGridRepairUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();

                string strValue = "SparePartCode,SparePartName,InputQty,UnitName,Spec,Maker";
                string strText = "구성품코드,구성품명,Qty,단위,Spec,Maker";

                wGrid.mfSetGridColumnValueGridList(this.uGridRepairUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                , "SparePartCode", "SparePartName", dtEquipSPBOM);


                #endregion

                #region Detail

                DataTable dtDetail = clsEquip.mfReadEquipRepairEndUseSP(PlantCode, RepairReqCode, m_resSys.GetString("SYS_LANG"));

                uGridRepairUseSP.Refresh();
                uGridRepairUseSP.DataSource = dtDetail;
                uGridRepairUseSP.DataBind();
                uGridRepairUseSP_AfterDataBinding();

                if (dtDetail.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridRepairUseSP, 0);
                }
                #endregion

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                uGroupBoxContentsArea.Expanded = true;
                uGridRepairResult.Rows[e.Cell.Row.Index].Fixed = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void mfSelectRepairReq()
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            WinGrid wGrid = new WinGrid();

            foreach (Control ctrl in uGroupBoxContentsArea.Controls)
            {
                if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                {
                    ctrl.Text = string.Empty;
                }
            }

            PlantCode = string.Empty;

            uDateUseSPChgDate.Value = DateTime.Now;
            uTextUseSPChgID.Text = m_resSys.GetString("SYS_USERID");
            uTextUseSPChgName.Text = m_resSys.GetString("SYS_USERNAME");

            uGridRepairUseSP.Refresh();
            wGrid.mfAddRowGrid(this.uGridRepairUseSP, 0);

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                #region Read Header
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairUseSP), "RepairUseSP");
                QRPEQU.BL.EQUREP.RepairUseSP clsEquip = new QRPEQU.BL.EQUREP.RepairUseSP();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipRepairEndUseSPDetail(PlantCode, RepairReqCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    uGroupBoxContentsArea.Expanded = false;
                    return;
                }

                PlantCode = dtEquip.Rows[0]["PlantCode"].ToString();
                uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();
                uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                uTextRepairReqDate.Text = dtEquip.Rows[0]["RepairReqDate"].ToString();
                uTextRepairEndDate.Text = dtEquip.Rows[0]["RepairEndDate"].ToString();
                uTextRepairID.Text = dtEquip.Rows[0]["RepairID"].ToString();
                uTextRepairName.Text = dtEquip.Rows[0]["RepairName"].ToString();
                uTextRepairResultName.Text = dtEquip.Rows[0]["RepairResultName"].ToString();
                //uTextEquipStopTypeName.Text = dtEquip.Rows[0]["EquipStopTypeName"].ToString();
                uTextRepairDesc.Text = dtEquip.Rows[0]["RepairDesc"].ToString();

                if (dtEquip.Rows[0]["UseSPChgDate"].ToString() == string.Empty)
                    uDateUseSPChgDate.Value = DateTime.Now;
                else
                    uDateUseSPChgDate.Value = dtEquip.Rows[0]["UseSPChgDate"].ToString();

                if (dtEquip.Rows[0]["UseSPChgID"].ToString() == string.Empty)
                {
                    uTextUseSPChgID.Text = m_resSys.GetString("SYS_USERID");
                    uTextUseSPChgName.Text = m_resSys.GetString("SYS_USERNAME");
                }
                else
                {
                    uTextUseSPChgID.Text = dtEquip.Rows[0]["UseSPChgID"].ToString();
                    uTextUseSPChgName.Text = dtEquip.Rows[0]["UseSPChgName"].ToString();
                }

                uTextUseSPEtcDesc.Text = dtEquip.Rows[0]["UseSPEtcDesc"].ToString();

                PlantCode = dtEquip.Rows[0]["PlantCode"].ToString();
                RepairReqCode = dtEquip.Rows[0]["RepairReqCode"].ToString();
                #endregion

                #region Detail Grid DropDown Binding

                brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsInventory);
                DataTable dtInventory = clsInventory.mfReadMASSPInventoryCombo(PlantCode, m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(uGridRepairUseSP, 0, "ChgSPInventoryName", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInventory);

                brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);

                DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(PlantCode, uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(uGridRepairUseSP, 0, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.Default, "", "선택", dtEquipSPBOM);

                wGrid.mfSetGridColumnValueList(uGridRepairUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.Default, "", "선택", dtEquipSPBOM);

                #endregion

                #region Detail

                DataTable dtDetail = clsEquip.mfReadEquipRepairEndUseSP(PlantCode, RepairReqCode, m_resSys.GetString("SYS_LANG"));

                uGridRepairUseSP.Refresh();
                uGridRepairUseSP.DataSource = dtDetail;
                uGridRepairUseSP.DataBind();
                uGridRepairUseSP_AfterDataBinding();

                #endregion

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                uGroupBoxContentsArea.Expanded = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridRepairUseSP_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);                
                QRPGlobal grdImg = new QRPGlobal();
                WinMessageBox msg = new WinMessageBox();
                WinGrid wGrid = new WinGrid();

                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                string strColumns = e.Cell.Column.Key;

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridRepairUseSP, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                #region 구성품명
                if (strColumns == "CurSparePartName")
                {
                    
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChannel.mfCredentials(clsEquipSPBOM);

                    string strSparePartCode = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    DataTable dtDurableMat = clsEquipSPBOM.mfReadEquipSTBOMDetail(PlantCode, uTextEquipCode.Text, strSparePartCode, strLang);

                    int intInputQty = Convert.ToInt32(dtDurableMat.Rows[0]["InputQty"].ToString());
                    int intChgStandbyQty = Convert.ToInt32(dtDurableMat.Rows[0]["ChgStandbyQty"].ToString());

                    if (dtDurableMat.Rows.Count != 0)
                    {
                        e.Cell.Row.Cells["CurSparePartCode"].Tag = dtDurableMat.Rows[0]["UnitCode"];
                        e.Cell.Row.Cells["CurSparePartCode"].Value = strSparePartCode;
                        
                        e.Cell.Row.Cells["CurInputQty"].Tag = intInputQty - intChgStandbyQty;
                        e.Cell.Row.Cells["CurInputQty"].Value = intInputQty - intChgStandbyQty;

                        e.Cell.Row.Cells["CurSpec"].Value = dtDurableMat.Rows[0]["Spec"];
                        e.Cell.Row.Cells["CurMaker"].Value = dtDurableMat.Rows[0]["Maker"];

                        if (!e.Cell.Row.Cells["AvailQty"].Value.Equals(0))
                        {
                            e.Cell.Row.Cells["AvailQty"].Value = 0;
                        }

                        //선택한 정보가 그리드내에 있으면 다시입력
                        for (int i = 0; i < this.uGridRepairUseSP.Rows.Count; i++)
                        {
                            if (this.uGridRepairUseSP.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridRepairUseSP.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridRepairUseSP.Rows[i].Hidden.Equals(false))
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    this.uGridRepairUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(strSparePartCode))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000311", "M000843" + this.uGridRepairUseSP.Rows[i].RowSelectorNumber + "M000459", Infragistics.Win.HAlign.Right);

                                    //단위저장 
                                    e.Cell.Row.Cells["CurSparePartCode"].Tag = string.Empty;

                                    e.Cell.Row.Cells["CurSparePartCode"].Value = string.Empty;
                                    e.Cell.Row.Cells["CurSparePartName"].Value = string.Empty;

                                    e.Cell.Row.Cells["CurInputQty"].Tag = 0;
                                    e.Cell.Row.Cells["CurInputQty"].Value = 0;
                                    e.Cell.Row.Cells["CurSpec"].Value = string.Empty;
                                    e.Cell.Row.Cells["CurMaker"].Value = string.Empty;
                                    return;
                                }
                            }
                        }
                        if (Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Tag))
                        {
                            e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                        }
                    }

                }
                #endregion


                #region SP창고
                if (strColumns.Equals("ChgSPInventoryName"))
                {

                    string strSPInventoryCode = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    //SP창고가 공백이 아닌경우
                    if (!strSPInventoryCode.Equals(string.Empty))
                    {
                        e.Cell.Row.Cells["ChgSparePartCode"].Value = ""; 
                        e.Cell.Row.Cells["ChgSparePartName"].Value = "";
                        e.Cell.Row.Cells["AvailQty"].Value = 0;
                        e.Cell.Row.Cells["ChgInputQty"].Value = 0;

                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                        DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strSPInventoryCode, m_resSys.GetString("SYS_LANG"));

                        if (dtChgSP.Rows.Count > 0)
                        {
                            string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                            string strDropDownName = "";
                            string strLang = m_resSys.GetString("SYS_LANG");

                            if (strLang.Equals("KOR"))
                                strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";
                            else if (strLang.Equals("CHN"))
                                strDropDownName = "使用构成品条码,使用构成品名,库存量,单位,Spec,Maker";
                            else
                                strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                            e.Cell.Row.Cells["ChgSPInventoryCode"].Value = strSPInventoryCode;
                            //GridList 그리드에 삽입
                            wGrid.mfSetGridCellValueGridList(this.uGridRepairUseSP, 0, e.Cell.Row.Index, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                                , "SparePartCode", "SparePartName", dtChgSP);
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000121", "M001252", Infragistics.Win.HAlign.Right);
                            return;
                        }

                    }
                }

                #endregion


                #region 사용구성품

                if (strColumns.Equals("ChgSparePartName"))
                {
                    string strChgSparePartCode = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    string strSPInventoryCode = e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString();

                    if (strSPInventoryCode =="")
                        return;

                    if (!strChgSparePartCode.Equals(string.Empty))
                    {
                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //SP현재고 조회
                        DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, strSPInventoryCode, strChgSparePartCode, m_resSys.GetString("SYS_LANG"));

                        if (dtSPStock.Rows.Count > 0)
                        {
                            if (!e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty) && !e.Cell.Row.Cells["CurSparePartCode"].Tag.Equals(dtSPStock.Rows[0]["UnitCode"]))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000360", "M000661", Infragistics.Win.HAlign.Right);
                                return;
                            }
                            e.Cell.Row.Cells["ChgSparePartCode"].Value = dtSPStock.Rows[0]["SparePartCode"];
                            e.Cell.Row.Cells["AvailQty"].Value = dtSPStock.Rows[0]["Qty"];
                            e.Cell.Row.Cells["AvailQty"].Tag = dtSPStock.Rows[0]["Qty"];

                            e.Cell.Row.Cells["UnitCode"].Value = dtSPStock.Rows[0]["UnitCode"];
                            e.Cell.Row.Cells["ChgSpec"].Value = dtSPStock.Rows[0]["Spec"];
                            e.Cell.Row.Cells["ChgMaker"].Value = dtSPStock.Rows[0]["Maker"];
                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQUZ0012_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region TextEvent

        //사용담당자 ID 입력 후 엔터 키를 누를 시 자동조회
        private void uTextUseSPChgID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보가 없으면 메세지 출력
                if (strPlantCode == null || strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000400", "M000399", Infragistics.Win.HAlign.Right);
                    if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                    {
                        this.uGroupBoxContentsArea.Expanded = false;
                    }
                    return;
                }
                
                //Delete,BackSpace 누를 경우 사용담당자 이름이 공백이 아니면 공백처리
                if (!this.uTextUseSPChgName.Text.Equals(string.Empty) && (e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back)))
                {
                    this.uTextUseSPChgName.Clear();
                }

                //사용담당자 저장
                string strUseSPChgID = this.uTextUseSPChgID.Text;

                if (!this.uTextUseSPChgID.Text.Equals(string.Empty) && e.KeyData.Equals(Keys.Enter))
                {
                    

                    //유저정보 BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //유저정보 조회매서드 호출
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUseSPChgID, m_resSys.GetString("SYS_LANG"));

                    // 조회 정보가 있는 경우 해당 ID의 명을 삽입 , 정보가 없을경우 메세지 출력
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUseSPChgName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001101", "M000883", Infragistics.Win.HAlign.Right);
                        
                        if (!this.uTextUseSPChgName.Text.Equals(string.Empty))
                        {
                            this.uTextUseSPChgName.Clear();
                        }
                        return;
                    }


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //사용담당자 버튼을 클릭하면 유저정보팝업창이 뜬다.
        private void uTextUseSPChgID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보가 없으면 메세지 출력
                if (strPlantCode == null  || strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000400", "M000399", Infragistics.Win.HAlign.Right);
                    if(this.uGroupBoxContentsArea.Expanded.Equals(true))
                    {
                        this.uGroupBoxContentsArea.Expanded = false;
                    }
                    return;
                }

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextUseSPChgID.Text = frmUser.UserID;
                this.uTextUseSPChgName.Text = frmUser.UserName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //사용담당자 ID가 바뀌면 이름이 공백이된다.
        private void uTextUseSPChgID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextUseSPChgName.Text.Equals(string.Empty))
                {
                    this.uTextUseSPChgName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #endregion


        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");

                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

                clsEquip.Dispose();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



    }
}
