﻿namespace QRPEQU.UI
{
    partial class frmEQU0013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0013));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextConfirmName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextConfirmID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelConfirm = new Infragistics.Win.Misc.UltraLabel();
            this.uDateConfirmDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCarryOutConfirm = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryOutConfirm)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextDocCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDocCode);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDay);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(472, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(368, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 6;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uTextDocCode
            // 
            this.uTextDocCode.Location = new System.Drawing.Point(728, 12);
            this.uTextDocCode.MaxLength = 20;
            this.uTextDocCode.Name = "uTextDocCode";
            this.uTextDocCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDocCode.TabIndex = 4;
            // 
            // uLabelDocCode
            // 
            this.uLabelDocCode.Location = new System.Drawing.Point(624, 12);
            this.uLabelDocCode.Name = "uLabelDocCode";
            this.uLabelDocCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelDocCode.TabIndex = 4;
            this.uLabelDocCode.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(216, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(15, 15);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "~";
            // 
            // uDateToDay
            // 
            this.uDateToDay.Location = new System.Drawing.Point(232, 12);
            this.uDateToDay.Name = "uDateToDay";
            this.uDateToDay.Size = new System.Drawing.Size(100, 21);
            this.uDateToDay.TabIndex = 2;
            // 
            // uDateFromDay
            // 
            this.uDateFromDay.Location = new System.Drawing.Point(116, 12);
            this.uDateFromDay.Name = "uDateFromDay";
            this.uDateFromDay.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDay.TabIndex = 1;
            // 
            // uLabelSearchDay
            // 
            this.uLabelSearchDay.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDay.Name = "uLabelSearchDay";
            this.uLabelSearchDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDay.TabIndex = 0;
            this.uLabelSearchDay.Text = "ultraLabel1";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextConfirmName);
            this.uGroupBox.Controls.Add(this.uTextConfirmID);
            this.uGroupBox.Controls.Add(this.uLabelConfirm);
            this.uGroupBox.Controls.Add(this.uDateConfirmDate);
            this.uGroupBox.Controls.Add(this.uLabelConfirmDate);
            this.uGroupBox.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 44);
            this.uGroupBox.TabIndex = 3;
            // 
            // uTextConfirmName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextConfirmName.Appearance = appearance3;
            this.uTextConfirmName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextConfirmName.Location = new System.Drawing.Point(476, 12);
            this.uTextConfirmName.Name = "uTextConfirmName";
            this.uTextConfirmName.ReadOnly = true;
            this.uTextConfirmName.Size = new System.Drawing.Size(100, 21);
            this.uTextConfirmName.TabIndex = 12;
            // 
            // uTextConfirmID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextConfirmID.Appearance = appearance2;
            this.uTextConfirmID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextConfirmID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance5;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextConfirmID.ButtonsRight.Add(editorButton1);
            this.uTextConfirmID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextConfirmID.Location = new System.Drawing.Point(372, 12);
            this.uTextConfirmID.MaxLength = 20;
            this.uTextConfirmID.Name = "uTextConfirmID";
            this.uTextConfirmID.Size = new System.Drawing.Size(100, 19);
            this.uTextConfirmID.TabIndex = 6;
            this.uTextConfirmID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextConfirmID_KeyDown);
            this.uTextConfirmID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextConfirmID_EditorButtonClick);
            // 
            // uLabelConfirm
            // 
            this.uLabelConfirm.Location = new System.Drawing.Point(268, 12);
            this.uLabelConfirm.Name = "uLabelConfirm";
            this.uLabelConfirm.Size = new System.Drawing.Size(100, 20);
            this.uLabelConfirm.TabIndex = 2;
            this.uLabelConfirm.Text = "ultraLabel2";
            // 
            // uDateConfirmDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateConfirmDate.Appearance = appearance4;
            this.uDateConfirmDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateConfirmDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateConfirmDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateConfirmDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateConfirmDate.Location = new System.Drawing.Point(116, 12);
            this.uDateConfirmDate.Name = "uDateConfirmDate";
            this.uDateConfirmDate.Size = new System.Drawing.Size(100, 19);
            this.uDateConfirmDate.TabIndex = 5;
            // 
            // uLabelConfirmDate
            // 
            this.uLabelConfirmDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelConfirmDate.Name = "uLabelConfirmDate";
            this.uLabelConfirmDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelConfirmDate.TabIndex = 0;
            this.uLabelConfirmDate.Text = "ultraLabel2";
            // 
            // uGridCarryOutConfirm
            // 
            this.uGridCarryOutConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCarryOutConfirm.DisplayLayout.Appearance = appearance6;
            this.uGridCarryOutConfirm.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCarryOutConfirm.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutConfirm.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryOutConfirm.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridCarryOutConfirm.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryOutConfirm.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridCarryOutConfirm.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCarryOutConfirm.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCarryOutConfirm.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCarryOutConfirm.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridCarryOutConfirm.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCarryOutConfirm.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutConfirm.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCarryOutConfirm.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridCarryOutConfirm.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCarryOutConfirm.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutConfirm.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGridCarryOutConfirm.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridCarryOutConfirm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCarryOutConfirm.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGridCarryOutConfirm.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGridCarryOutConfirm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCarryOutConfirm.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.uGridCarryOutConfirm.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCarryOutConfirm.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCarryOutConfirm.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCarryOutConfirm.Location = new System.Drawing.Point(0, 128);
            this.uGridCarryOutConfirm.Name = "uGridCarryOutConfirm";
            this.uGridCarryOutConfirm.Size = new System.Drawing.Size(1070, 700);
            this.uGridCarryOutConfirm.TabIndex = 4;
            this.uGridCarryOutConfirm.Text = "ultraGrid1";
            this.uGridCarryOutConfirm.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutConfirm_AfterCellUpdate);
            this.uGridCarryOutConfirm.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutConfirm_CellChange);
            // 
            // frmEQU0013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCarryOutConfirm);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0013";
            this.Load += new System.EventHandler(this.frnEQU0013_Load);
            this.Activated += new System.EventHandler(this.frnEQU0013_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0013_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryOutConfirm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDay;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDay;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextConfirmName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextConfirmID;
        private Infragistics.Win.Misc.UltraLabel uLabelConfirm;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelConfirmDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCarryOutConfirm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDocCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}