﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 전산SHEET관리                                         */
/* 프로그램ID   : frmEQUZ0035.cs                                        */
/* 프로그램명   : WHEEL CHANGE                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-03-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0035 : Form,IToolbar
    {
        //리소스호출을위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmEQUZ0035()
        {
            InitializeComponent();
        }

        private void frmEQUZ0035_Activated(object sender, EventArgs e)
        {
            QRPBrowser Toolbar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            Toolbar.mfActiveToolBar(this.MdiParent, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0035_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0035_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitGrid();
            InitLabel();
            InitCombo();

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }


        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("WHEEL CHANGE 등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchDate, "검색일", m_resSys.GetString("SYS_FONTNAME"), true, true);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_FONTNAME"));

                WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"),
                    "", "", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                this.uGridWheelChange.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;

                grd.mfInitGeneralGrid(this.uGridWheelChange, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false", 0, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "DocCode", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "", 1, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "WriteDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "", 2, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupAxis = grd.mfSetGridGroup(this.uGridWheelChange, 0, "AXIS", "AXIS", 3, 0, 2, 2, false);


                grd.mfSetGridColumn(this.uGridWheelChange, 0, "G1", "G1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupAxis);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "G2", "G2", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupAxis);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "InitialThickness", "Initial Thickness", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "WareMount", "WareMount(Before)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "SerialNo", "고유번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup = grd.mfSetGridGroup(this.uGridWheelChange, 0, "Result", "측정값", 8, 0, 5, 2, false);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "1P", "1P", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "2P", "2P", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "3P", "3P", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "4P", "4P", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroup);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "5P", "5P", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroup);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "InspectUserID", "점검자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 14, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridWheelChange, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 15, 0, 1, 2, null);

                //헤더,셀 FontSize 설정
                this.uGridWheelChange.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridWheelChange.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridWheelChange.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                //한줄추가
                grd.mfAddRowGrid(this.uGridWheelChange, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Toolbar

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                #region 필수 입력사항
                if (this.uDateFrom.Value == null || this.uDateFrom.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "검색시작일을 선택해주세요",
                                Infragistics.Win.HAlign.Right);
                    this.uDateFrom.DropDown();
                    return;
                }
                if (this.uDateTo.Value == null || this.uDateTo.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "검색종료일을 선택해주세요.",
                                Infragistics.Win.HAlign.Right);
                    this.uDateTo.DropDown();
                    return;
                }
                if (this.uDateFrom.DateTime.Date > this.uDateTo.DateTime.Date)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "검색시작일이 종료일보다 큽니다.",
                                Infragistics.Win.HAlign.Right);
                    this.uDateFrom.DropDown();
                    return;
                }
                #endregion

                string strFromDate = this.uDateFrom.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDate = this.uDateTo.DateTime.Date.ToString("yyyy-MM-dd");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY1), "ASSY1");
                QRPEQU.BL.EQUSHT.ASSY1 clsASSY1 = new QRPEQU.BL.EQUSHT.ASSY1();
                brwChannel.mfCredentials(clsASSY1);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                DataTable dtDataInfo = clsASSY1.mfReadWheelChange(strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));
                /////////////

                this.uGridWheelChange.DataSource = dtDataInfo;
                this.uGridWheelChange.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */

                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                                         Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            { 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGridWheelChange.Rows.Count > 0)
                    this.uGridWheelChange.ActiveCell = this.uGridWheelChange.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                
                
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY1), "ASSY1");
                QRPEQU.BL.EQUSHT.ASSY1 clsASSY1 = new QRPEQU.BL.EQUSHT.ASSY1();
                brwChannel.mfCredentials(clsASSY1);

                DataTable dtDataInfo = clsASSY1.mfSetDataInfo_Wheel();


                for (int i = 0; i < this.uGridWheelChange.Rows.Count; i++)
                {
                    if (this.uGridWheelChange.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (this.uGridWheelChange.Rows[i].Cells["InspectUserID"].Value.ToString().Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "점검자를 입력해주세요.",
                                Infragistics.Win.HAlign.Right);

                            this.uGridWheelChange.ActiveCell = this.uGridWheelChange.Rows[i].Cells["InspectUserID"];
                            this.uGridWheelChange.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        DataRow drRow = dtDataInfo.NewRow();

                        drRow["DocCode"] = this.uGridWheelChange.Rows[i].GetCellValue("DocCode");

                        if (this.uGridWheelChange.Rows[i].GetCellValue("WriteDate").ToString().Equals(string.Empty))
                            drRow["WriteDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        else
                            drRow["WriteDate"] = Convert.ToDateTime(this.uGridWheelChange.Rows[i].GetCellValue("WriteDate")).Date.ToString("yyyy-MM-dd");
                    
                        drRow["G1"] = this.uGridWheelChange.Rows[i].GetCellValue("G1");
                        drRow["G2"] = this.uGridWheelChange.Rows[i].GetCellValue("G2");
                        drRow["InitialThickness"] = this.uGridWheelChange.Rows[i].GetCellValue("InitialThickness");
                        drRow["WareMount"] = this.uGridWheelChange.Rows[i].GetCellValue("WareMount");
                        drRow["SerialNo"] = this.uGridWheelChange.Rows[i].GetCellValue("SerialNo");
                        drRow["1P"] = this.uGridWheelChange.Rows[i].GetCellValue("1P");
                        drRow["2P"] = this.uGridWheelChange.Rows[i].GetCellValue("2P");
                        drRow["3P"] = this.uGridWheelChange.Rows[i].GetCellValue("3P");
                        drRow["4P"] = this.uGridWheelChange.Rows[i].GetCellValue("4P");
                        drRow["5P"] = this.uGridWheelChange.Rows[i].GetCellValue("5P");
                        drRow["InspectUserID"] = this.uGridWheelChange.Rows[i].GetCellValue("InspectUserID");
                        drRow["EtcDesc"] = this.uGridWheelChange.Rows[i].GetCellValue("EtcDesc");


                        dtDataInfo.Rows.Add(drRow);
                    }
                }


                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "저장 할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }


                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsASSY1.mfSaveWheelChange(dtDataInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMeg = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMeg = "입력한 정보를 저장하지 못했습니다.";
                        else
                            strMeg = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", strMeg,
                                                     Infragistics.Win.HAlign.Right);
                    }

                }
            
            
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGridWheelChange.Rows.Count > 0)
                    this.uGridWheelChange.ActiveCell = this.uGridWheelChange.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "삭제 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY1), "ASSY1");
                QRPEQU.BL.EQUSHT.ASSY1 clsASSY1 = new QRPEQU.BL.EQUSHT.ASSY1();
                brwChannel.mfCredentials(clsASSY1);

                DataTable dtDataInfo = clsASSY1.mfSetDelDataInfo_Wheel();

                for (int i = 0; i < this.uGridWheelChange.Rows.Count; i++)
                {
                    if (this.uGridWheelChange.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (Convert.ToBoolean(this.uGridWheelChange.Rows[i].GetCellValue("Check"))) // 체크가 되어있는 정보
                        {
                            if (!this.uGridWheelChange.Rows[i].GetCellValue("DocCode").ToString().Equals(string.Empty)) //문서번호가 있는 정보
                            {
                                DataRow drRow = dtDataInfo.NewRow();

                                drRow["DocCode"] = this.uGridWheelChange.Rows[i].GetCellValue("DocCode");

                                dtDataInfo.Rows.Add(drRow);
                            }
                        }
                    }
                }

                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "삭제 할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = clsASSY1.mfDeleteWheelChange(dtDataInfo);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMes = "";
                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = "선택한 정보를 삭제하지 못했습니다.";
                        else
                            strMes = ErrRtn.ErrMessage;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", strMes,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridWheelChange.Rows.Count <= 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                        , "확인창", "출력정보확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridWheelChange);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
        }

        public void mfCreate()
        {
        }

        #endregion

        /// <summary>
        /// 그리드 셀에 신규나 수정이 발생할 경우 줄머리에 편집이미지를 넣어준다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridWheelChange_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridWheelChange, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 줄생성시 일자 수정불가
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridWheelChange_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            e.Row.Cells["WriteDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }
    }
}
