﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQU0027.cs                                         */
/* 프로그램명   : 치공구재고현황                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-13                                            */
/* 수정이력     : 1111-11-11 :                                          */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPEQU.UI
{
    public partial class frmEQU0037 : Form ,IToolbar
    {
        //리소스호출을위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQU0037()
        {
            InitializeComponent();
        }

        private void frmEQU0037_Activated(object sender, EventArgs e)
        {
            QRPBrowser ToolBar = new QRPBrowser();
            //System ResourceInfo
            ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

            ToolBar.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmEQU0037_Load(object sender, EventArgs e)
        {
            try
            {
                SetToolAuth();
                InitLabel();
                InitGrid();
                InitTitle();
                InitCombo();

                
                
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfLoadGridColumnProperty(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0037_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        #region 컨트롤 초기화 및 권한
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("SparePart이력조회", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSparePart, "SparePartCode", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //SparePart이력조회 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridSparePartMoveHist, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //SparePart이력조회 그리드 컬럼설정
                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "SparePartCode", "SparePartCode", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "SparePartName", "SparePart명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "Spec", "Spec", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "Maker", "Maker", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "MoveGubunCode", "구분", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "MoveGubunName", "구분", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "MoveDate", "일자", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 2, null);

                string strLang = m_resSys.GetString("SYS_LANG");
                string strIn = "";
                string strOut = "";
                string strIOQty = "";
                if (strLang.Equals("KOR"))
                {
                    strIn = "입고";
                    strOut = "출고";
                    strIOQty = "입출고수량";
                }
                else if (strLang.Equals("CHN"))
                {
                    strIn = "入库";
                    strOut = "出库";
                    strIOQty = "入出库";
                }
                else if (strLang.Equals("ENG"))
                {
                    strIn = "입고";
                    strOut = "출고";
                    strIOQty = "입출고수량";
                }

                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                //Infragistics.Win.UltraWinGrid.UltraGridGroup group0 = grd.mfSetGridGroup(this.uGridSparePartMoveHist, 0, "te", "입고", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = grd.mfSetGridGroup(this.uGridSparePartMoveHist, 0, "In", strIn, 8, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = grd.mfSetGridGroup(this.uGridSparePartMoveHist, 0, "Out", strOut, 11, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group3 = grd.mfSetGridGroup(this.uGridSparePartMoveHist, 0, "InOutQty", strIOQty, 13, 0, 3, 2, false);

                ////grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "test", "", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 1, false, false, 50
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, group0);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GRInventoryName", "창고", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",0, 0, 1, 1, group1);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GREquipCode", "교체설비", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, group1);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GREQuipName", "교체설비명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, group1);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GPInventoryName", "창고", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, group2);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GPEquipCode", "교체설비", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, group2);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GPEquipName", "교체설비명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, group2);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GRQty", "입고수량", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0", 0, 0, 1, 1, group3);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "GPQty", "출고수량", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0", 1, 0, 1, 1, group3);

                grd.mfSetGridColumn(this.uGridSparePartMoveHist, 0, "UnitName", "단위", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, group3);

                //Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridSparePartMoveHist.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSparePartMoveHist.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GRInventoryName"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GREquipCode"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GREQuipName"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GPInventoryName"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GPEquipCode"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GPEquipName"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GRQty"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["GPQty"].CellAppearance.ForeColor = Color.Black;
                this.uGridSparePartMoveHist.DisplayLayout.Bands[0].Columns["UnitName"].CellAppearance.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant),"Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                
                WinComboEditor com = new WinComboEditor();

                com.mfSetComboEditor(this.uComboSearchPlant,true,false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista,m_resSys.GetString("SYS_FONTNAME"),true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE")
                    , "", "선택", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Toolbar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uTextSparePartCode.Text.Equals(string.Empty) || this.uTextSparePartName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001230", "M000120", Infragistics.Win.HAlign.Right);

                    this.uTextSparePartCode.Focus();
                    return;
                }

                //검색정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strSparePartCode = this.uTextSparePartCode.Text;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                //SP재고이력 정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                //SP재고이력 정보조회 매서드 호출
                DataTable dtSpMoveHist = clsSPStockMoveHist.mfReadSPStockMoveHist(strPlantCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));


                //그리드에 바인드
                this.uGridSparePartMoveHist.DataSource = dtSpMoveHist;
                this.uGridSparePartMoveHist.DataBind();
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtSpMoveHist.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSparePartMoveHist, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSparePartMoveHist.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridSparePartMoveHist);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        public void mfPrint()
        {
        }

        #endregion

        #region 텍스트이벤트
        private void uTextSparePartCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //
                if (e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete))
                {
                    this.uTextSparePartName.Clear();
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (e.KeyData.Equals(Keys.Enter) && !this.uTextSparePartCode.Text.Equals(string.Empty))
                {
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strSparePart = this.uTextSparePartCode.Text;
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "");
                    QRPMAS.BL.MASEQU.SparePart clsSparePart = new QRPMAS.BL.MASEQU.SparePart();
                    brwChannel.mfCredentials(clsSparePart);

                    DataTable dtSparePart = clsSparePart.mfReadMASSparePartName(strPlantCode, strSparePart, m_resSys.GetString("SYS_LANG"));

                    if (dtSparePart.Rows.Count > 0)
                    {
                        this.uTextSparePartName.Text = dtSparePart.Rows[0]["SparePartName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001115", "M000886", Infragistics.Win.HAlign.Right);
                        this.uTextSparePartName.Clear();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSparePartCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }


                frmPOP0008 frmSpare = new frmPOP0008();
                frmSpare.PlantCode = strPlantCode;
                frmSpare.ShowDialog();

                this.uTextSparePartCode.Text = frmSpare.SparePartCode;
                this.uTextSparePartName.Text = frmSpare.SparePartName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSparePartCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextSparePartName.Text.Equals(string.Empty))
                {
                    this.uTextSparePartName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion
    }
}
