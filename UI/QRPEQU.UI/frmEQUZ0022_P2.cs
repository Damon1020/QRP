﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0022_P2.cs                                      */
/* 프로그램명   : 설비수리코드 팝업창                                   */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-12-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0022_P2 : Form
    {

        #region 전역변수
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strEquipCode;
        private string strRepairCode;
        private string strRepairName;
        private string strSuperCode;

        

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }
        
        public string EquipCode
        {
            get { return strEquipCode; }
            set { strEquipCode = value; }
        }
        
        public string RepairCode
        {
            get { return strRepairCode; }
            set { strRepairCode = value; }
        }
        
        public string RepairName
        {
            get { return strRepairName; }
            set { strRepairName = value; }
        }

        public string SuperCode
        {
            get { return strSuperCode; }
            set { strSuperCode = value; }
        }

        #endregion

        public frmEQUZ0022_P2()
        {
            InitializeComponent();
        }

        private void frmEQUZ0022D2_Load(object sender, EventArgs e)
        {
            InitTitle();
            InitGroupBox();
            InitButton();
            InitGrid();


            if (strSuperCode != null && !strSuperCode.Equals(string.Empty)
                && strPlantCode != null && !strPlantCode.Equals(string.Empty)
                && strEquipCode != null && !strEquipCode.Equals(string.Empty))
            {
                SearchInfo();
            }

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
            
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀 설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("설비수리코드", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grb = new WinGroupBox();

                grb.mfSetGroupBox(this.uGroupCauseInfo, GroupBoxType.INFO, "원인정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                uGroupCauseInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupCauseInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonCheck, "선택", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCancel, "취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGridInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridInfo, 0, "REASONCODE", "원인코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "DESCRIPTION", "설명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비정지유형 검색
        /// </summary>
        private void SearchInfo()
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                QRPMAS.BL.MASPRC.ReasonCode clsReasonCode = new QRPMAS.BL.MASPRC.ReasonCode();
                brwChannel.mfCredentials(clsReasonCode);

                DataTable dtEquipStopType = clsReasonCode.mfReadResonCode_EquipStopType(strPlantCode, strEquipCode, strSuperCode);

                this.uGridInfo.DataSource = dtEquipStopType;
                this.uGridInfo.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridInfo_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strRepairCode = e.Row.Cells["REASONCODE"].Text.ToString();
                strRepairName = e.Row.Cells["DESCRIPTION"].Text.ToString();
                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonCheck_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridInfo.ActiveRow == null) return;

                if (this.uGridInfo.ActiveRow.Index >= 0)
                {
                    strRepairCode = this.uGridInfo.ActiveRow.Cells["REASONCODE"].Text.ToString();
                    strRepairName = this.uGridInfo.ActiveRow.Cells["DESCRIPTION"].Text.ToString();

                    this.Close();
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


    }
}
