﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0011));
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextlStockedName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextlStockedUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockedUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDatelStockedDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelStockedDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextlStockedName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextlStockedUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDatelStockedDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(508, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchPlant.TabIndex = 7;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(404, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 6;
            this.uLabelSearchPlant.Text = "ultraLabel3";
            // 
            // uDateSearchToDate
            // 
            this.uDateSearchToDate.Location = new System.Drawing.Point(232, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDate.TabIndex = 5;
            // 
            // ultraLabel2
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(216, 15);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel2.TabIndex = 3;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            this.uDateSearchFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDate.TabIndex = 4;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 0;
            this.uLabelSearchDate.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextlStockedName);
            this.uGroupBox.Controls.Add(this.uTextlStockedUserID);
            this.uGroupBox.Controls.Add(this.uLabelStockedUser);
            this.uGroupBox.Controls.Add(this.uDatelStockedDay);
            this.uGroupBox.Controls.Add(this.uLabelStockedDay);
            this.uGroupBox.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBox.TabIndex = 2;
            // 
            // uTextlStockedName
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextlStockedName.Appearance = appearance28;
            this.uTextlStockedName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextlStockedName.Location = new System.Drawing.Point(484, 12);
            this.uTextlStockedName.Name = "uTextlStockedName";
            this.uTextlStockedName.ReadOnly = true;
            this.uTextlStockedName.Size = new System.Drawing.Size(100, 21);
            this.uTextlStockedName.TabIndex = 25;
            // 
            // uTextlStockedUserID
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextlStockedUserID.Appearance = appearance19;
            this.uTextlStockedUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance32.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance32.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance32;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextlStockedUserID.ButtonsRight.Add(editorButton1);
            this.uTextlStockedUserID.Location = new System.Drawing.Point(380, 12);
            this.uTextlStockedUserID.Name = "uTextlStockedUserID";
            this.uTextlStockedUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextlStockedUserID.TabIndex = 24;
            // 
            // uLabelStockedUser
            // 
            this.uLabelStockedUser.Location = new System.Drawing.Point(276, 12);
            this.uLabelStockedUser.Name = "uLabelStockedUser";
            this.uLabelStockedUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockedUser.TabIndex = 23;
            this.uLabelStockedUser.Text = "ultraLabel2";
            // 
            // uDatelStockedDay
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uDatelStockedDay.Appearance = appearance15;
            this.uDatelStockedDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDatelStockedDay.Location = new System.Drawing.Point(116, 12);
            this.uDatelStockedDay.Name = "uDatelStockedDay";
            this.uDatelStockedDay.Size = new System.Drawing.Size(100, 21);
            this.uDatelStockedDay.TabIndex = 22;
            // 
            // uLabelStockedDay
            // 
            this.uLabelStockedDay.Location = new System.Drawing.Point(12, 12);
            this.uLabelStockedDay.Name = "uLabelStockedDay";
            this.uLabelStockedDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockedDay.TabIndex = 21;
            this.uLabelStockedDay.Text = "ultraLabel2";
            // 
            // uGrid
            // 
            this.uGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance6;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid.Location = new System.Drawing.Point(0, 120);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1070, 720);
            this.uGrid.TabIndex = 3;
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            // 
            // frmEQUZ0011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0011";
            this.Load += new System.EventHandler(this.frmEQUZ0011_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0011_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextlStockedName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextlStockedUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDatelStockedDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextlStockedName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextlStockedUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelStockedUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDatelStockedDay;
        private Infragistics.Win.Misc.UltraLabel uLabelStockedDay;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
    }
}