﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0009.cs                                        */
/* 프로그램명   : 설비점검출고등록                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-01 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0009 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0009()
        {
            InitializeComponent();
        }

        //폼이닫히기 전에 그리드 속성 저장
        private void frmEQUZ0009_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0009_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0009_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("설비점검출고등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            InitButton();

            // ExtandableGroupBox 설정
            this.uGroupBoxContentsArea.Expanded = false;

            //this.uGridRelease.Height = 720;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "점검출고정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.Appearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "설비점검 출고등록상세", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3.Appearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 초기값 설정 Method
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);



                // 점검일 초기화
                this.uDateSearchFromDay.Value = DateTime.Now;//.Date.ToString().Substring(0,8)+"01";
                this.uDateSearchToDay.Value = DateTime.Now;
                
                //수리출고요청일
                this.uDateRepairReqDate.Value = DateTime.Now;


                // 수리출고요청자 ID 설정
                this.uTextRequestID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextRequestName.Text = m_resSys.GetString("SYS_USERNAME");

                // 정비사 ID 설정
                this.uTextTechnician.Text = m_resSys.GetString("SYS_USERID");
                this.uTextTechnicianName.Text = m_resSys.GetString("SYS_USERNAME");

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipMentGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcGubun, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchUser, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabel1, "점검출고코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel2, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel4, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabel5, "수리출고요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel6, "수리출고요청자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabel7, "수리출고구성품", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel8, "점검결과조회", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call Bl
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //////////////////////
                /// 나머지들 추가/////
                //////////////////////
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 설비점검출고
                // 설비점검출고 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridRelease, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridRelease, 0, "RepairGICode", "점검출고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "PMPlanDate", "점검일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "RepairGIReqDate", "수리출고요청일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "RepairGIReqID", "수리출고요청자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "RepairGIReqName", "수리출고요청자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRelease, 0, "ModelName", "ModelName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 수리출고 구성품
                // 수리출고구성품 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridRepair, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridRepair, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                wGrid.mfSetGridColumn(this.uGridRepair, 0, "RepairGICode", "수리출고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "RepairGIGubunCode", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "INN");

                //wGrid.mfSetGridColumn(this.uGridRepair, 0, "RepairGIGubunCode", "설비구성품구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CurDurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CurDurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CurLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridRepair, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgDurableInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgDurableMatCode", "교체구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgDurableMatName", "교체구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "Qty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgQty", "교체수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CancelFlag", "수리취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                #region DropDown

                //--헤더에있는 체크박스를 사용안함 --//
                this.uGridRepair.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //--수리출고구분--//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtRepairGICode = clsCommonCode.mfReadCommonCode("C0016", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "RepairGIGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtRepairGICode);

                //-----금형치공구창고정보-----//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                #endregion

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridRepair, 0);

                #endregion

                #region 점검결과조회
                // 점검결과조회 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMPeriodName", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultName", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultValue", "수치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMWorkDesc", "점검특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                //각 그리드 폰트설정
                this.uGridRelease.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridRelease.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridRepair.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridRepair.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                    ClearValue();

                }

                #region 필수입력사항 체크

                if (this.uDateSearchFromDay.Value.ToString() == "" || this.uDateSearchToDay.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "점검일을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDay.DropDown();
                    return;
                }
                if(this.uDateSearchFromDay.DateTime.Date > this.uDateSearchToDay.DateTime.Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "입력사항 확인", "범위가 올바르지 않습니다. 다시 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDay.DropDown();
                    return;

                }
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                 if (this.uTextTechnician.Text == "" || this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "정비사를 입력 또는 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextTechnician.Focus();
                    return;
                }

                #endregion

                //-------------------------값 저장--------------------------//
                string strDateFrom = this.uDateSearchFromDay.DateTime.Date.ToString("yyyy-MM-dd");          //검색시작일
                string strDateTo = this.uDateSearchToDay.DateTime.Date.ToString("yyyy-MM-dd");              //검색종료일
                string strPlantCode = this.uComboSearchPlant.Value.ToString();          //공장
                string strStation = this.uComboSearchStation.Value.ToString();          //Station
                string strTechnician = this.uTextTechnician.Text;                       //정비사
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();    //위치
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();      //설비대분류
                string strEquipLargeType = this.uComboEquipLargeType.Value.ToString();  //설비중분류
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();    //설비그룹

                //string strArea = this.uComboSearchArea.Value.ToString();
                //string strGubun = this.uComboSearchProcGubun.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);

                //처리 로직//

                DataTable dtRelease = new DataTable();

                dtRelease = clsDurableMatRepairH.mfReadDurableMatRepairGIH(strPlantCode, strStation, strEquipLocCode, strProcessGroup, strEquipLargeType, strEquipGroup,
                                                                            strTechnician, strDateFrom, strDateTo, m_resSys.GetString("SYS_LANG"));
                //----검색조건이 (설비점검그룹) 또는 (Area, Station, 설비공정구분) 인지 체크----//

                //if (strEquipGroup == "")
                //{
                //    dtRelease = clsDurableMatRepairH.mfReadDurableMatRepairHBatch(strPlantCode, strArea, strStation, strGubun, strEquipLocCode, strEquipType, strDateFrom, strDateTo, m_resSys.GetString("SYS_LANG"));
                //}
                //else
                //{
                //    dtRelease = clsDurableMatRepairH.mfReadDurableMatRepairH(strPlantCode, strEquipGroup, strDateFrom, strDateTo, m_resSys.GetString("SYS_LANG"));
                //}
                ///////////////

                this.uGridRelease.DataSource = dtRelease;
                this.uGridRelease.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtRelease.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridRelease, 0);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();



                #region 필수 입력확인 정보확인
                //-------------------------------필수 입력확인 , 정보 확인---------------------------------//
                if(this.uGridRelease.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uGroupBoxContentsArea.Expanded == false || this.uTextEquipCode.Text =="")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if(this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if(this.uTextRequestID.Text == "" || this.uTextRequestName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "수리출고요청자를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextRequestID.Focus();
                    return;
                }

                #endregion

                #region 저장 정보 저장

                #region 헤더저장
                //-----------------헤더 값저장-----------------//
                //---BL호출---//
                
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                //---헤더값을 넣을 데이터 테이블---//
                DataTable dtDurableRepairH = clsDurableMatRepairH.mfDataSetInfo();

                //-------------------헤더 값 저장-----------------//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;
                DataRow drRepairH;
                drRepairH = dtDurableRepairH.NewRow();
                drRepairH["PlantCode"] = strPlantCode;
                drRepairH["RepairGICode"] = this.uTextRepairGICode.Text;
                drRepairH["GITypeCode"] = "PM";
                drRepairH["PlanYear"] = this.uTextPMPlanDate.Text.Substring(0,4);
                drRepairH["EquipCode"] = strEquipCode;
                drRepairH["PMPlanDate"] = this.uTextPMPlanDate.Text;
                //drRepairH["RepairReqCode"] = 
                drRepairH["RepairGIReqDate"] = Convert.ToDateTime(this.uDateRepairReqDate.Value).ToString("yyyy-MM-dd");
                drRepairH["RepairGIReqID"] = this.uTextRequestID.Text;
                drRepairH["EtcDesc"] = this.uTextEtcDesc.Text;
                dtDurableRepairH.Rows.Add(drRepairH);

                #endregion

                #region 상세그리드 저장

                #region 상세그리드에 필요한BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsDurableMatRepairD);

                //--상세정보를 넣을 데이터 테이블--//
                DataTable dtDurableRepairD = clsDurableMatRepairD.mfsetDataInfo();
                //--행삭제된 정보를 넣을 데이터테이블--//
                //DataTable dtDurableRepairDel = clsDurableMatRepairD.mfsetDataInfo();
                //dtDurableRepairD.Columns.Add("UnitCode", typeof(string));
                dtDurableRepairD.Columns.Add("UnitName", typeof(string));
                dtDurableRepairD.Columns.Add("Qty", typeof(string));
                dtDurableRepairD.Columns.Add("ChgDurableMatName", typeof(string));
                dtDurableRepairD.Columns.Add("ItemGubunCode", typeof(string));


                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);
                #endregion

                if (this.uGridRepair.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridRepair.ActiveCell = this.uGridRepair.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                    {
                        //조회된 정보에서 수리취소가 false인데 편집이미지가 있으면 없앤다(수리출고구분 수정가능일 시 다른방법을..)
                        if (this.uGridRepair.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit 
                            && Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value) == false && this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            this.uGridRepair.Rows[i].RowSelectorAppearance.Image = null;
                        }

                        if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridRepair.Rows[i].Hidden == false)
                            {
                                if (this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() != "")
                                {
                                    #region 상세 필수입력사항 확인
                                    string strRowNum = this.uGridRepair.Rows[i].RowSelectorNumber.ToString();

                                    if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) && this.uGridRepair.Rows[i].Cells["InputQty"].Value.ToString().Equals("0"))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "수량입력 확인", strRowNum+ "번째 열의 재고량을 0 이상으로 입력해주세요.", Infragistics.Win.HAlign.Right);

                                        //PerFormAction
                                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["InputQty"];
                                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }

                                    if (this.uGridRepair.Rows[i].Cells["InputQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                        && this.uGridRepair.Rows[i].Cells["InputQty"].Tag != null 
                                        &&  Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value) > Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Tag))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "수량입력 확인", strRowNum + "번째 입력하신 수량이 해당구성품의 수량보다 많습니다.", Infragistics.Win.HAlign.Right);
                                        //PerFormAction
                                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["InputQty"];
                                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }
                                    if (this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString() == "")
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수입력사항 확인", strRowNum + "번째 창고를 선택해주세요.", Infragistics.Win.HAlign.Right);

                                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"];
                                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;

                                    }
                                    if (this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Value.ToString() == "" 
                                        || this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수입력사항 확인", strRowNum + "번째 교체구성품코드를 선택해주세요.", Infragistics.Win.HAlign.Right);

                                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"];
                                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    if (this.uGridRepair.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit 
                                        && this.uGridRepair.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수입력사항 확인", strRowNum +"번째 교체수량를 입력해주세요.", Infragistics.Win.HAlign.Right);

                                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgQty"];
                                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }
                                    if (this.uGridRepair.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                        && Convert.ToInt32(this.uGridRepair.Rows[i].Cells["Qty"].Value) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "입력사항 확인", strRowNum + "번째 교체수량이 가용수량보다 많습니다. 다시 입력해주세요", Infragistics.Win.HAlign.Right);

                                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgQty"];
                                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }

                                    #region 구성품 수량체크

                                    //구성품 교체나 투입 시
                                    if (!Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                    {
                                        // 교체 시
                                        if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            //BOM정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                            DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                        this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //BOM 정보와 비교하여 정보가 없거나 수량이 부족할 경우 콤보를 다시 뿌려줌
                                            if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "확인창", "구성품 정보확인", strRowNum + "번째 열의 기존구성품이 교체되거나 수량이 부족하여 등록 할 수 없습니다.", Infragistics.Win.HAlign.Right);

                                                #region DropDown

                                                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                                                //--해당설비의 BOM 이없을 경우 리턴 --
                                                if (dtDurable.Rows.Count == 0)
                                                {
                                                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "확인창", "설비정보 확인", "해당설비에 대한 금형치공구BOM정보가 없습니다. 설비관리기준정보 - 설비구성품정보로 등록하세요.", Infragistics.Win.HAlign.Right);

                                                    return;
                                                }
                                                //콤보 그리드 리스트 클러어
                                                this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                                                //콤보그리드 컬럼설정
                                                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                                                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                                                //--그리드에 추가 --
                                                WinGrid wGrid = new WinGrid();
                                                wGrid.mfSetGridColumnValueGridList(this.uGridRepair, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "DurableMatCode", "DurableMatName", dtDurable);
                                                #endregion

                                                this.uGridRepair.Rows[i].Cells["InputQty"].Value = 0;
                                                this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;

                                                this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CurDurableMatName"];
                                                this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }

                                        //Stock의 재고정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                        //재고정보와 비교하여 정보가 없거나 수량이 부족 할 경우 콤보를 다시뿌려줌
                                        if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "확인창", "구성품 정보확인", strRowNum + "번째 열의 교체구성품이 교체되거나 수량이 부족하여 등록 할 수 없습니다.", Infragistics.Win.HAlign.Right);

                                            ChgDurbleMat(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                            this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                            this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                            this.uGridRepair.Rows[i].Cells["Qty"].Value = 0;
                                            this.uGridRepair.Rows[i].Cells["ChgQty"].Value = 0;
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }

                                    }
                                    // 수리취소 할 경우
                                    else
                                    {
                                        DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                    this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        
                                        //BOM정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "확인창", "수리취소 정보확인", strRowNum + "번째 열의 기존구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                            return;
                                        }

                                        //교체된 내용을 취소할경우
                                        if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                    this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                    this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //재고정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                            if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "확인창", "수리취소 정보확인", strRowNum + "번째 열의 교체구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                                this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                                return;
                                            }
                                        }
                                    }
                                    
                                    #endregion

                                    #endregion


                                    DataRow drRepairD;
                                    drRepairD = dtDurableRepairD.NewRow();
                                    drRepairD["PlantCode"] = strPlantCode;                                                                  //공장코드
                                    drRepairD["Seq"] = this.uGridRepair.Rows[i].Cells["Seq"].Value.ToString();                           //순번
                                    drRepairD["ItemGubunCode"] = "TL";
                                    drRepairD["RepairGIGubunCode"] = this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Value.ToString();//수리출고구분
                                    drRepairD["CurDurableMatCode"] = this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(); //구성품
                                    drRepairD["CurQty"] = this.uGridRepair.Rows[i].Cells["InputQty"].Value.ToString();                  //기존수량
                                    drRepairD["CurLotNo"] = this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString();                //기존 LotNo
                                    drRepairD["ChgDurableInventoryCode"] = this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString();//교체창고
                                    drRepairD["ChgDurableMatCode"] = this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();//교체구성품
                                    drRepairD["ChgDurableMatName"] = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Text;
                                    drRepairD["ChgQty"] = this.uGridRepair.Rows[i].Cells["ChgQty"].Value.ToString();                //교체수량
                                    drRepairD["Qty"] = this.uGridRepair.Rows[i].Cells["Qty"].Value.ToString();                      //가용수량
                                    drRepairD["ChgLotNo"] = this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString();            //변경된LotNo
                                    drRepairD["UnitCode"] = this.uGridRepair.Rows[i].Cells["UnitCode"].Value.ToString();            //단위
                                    //drRepairD["UnitName"] = this.uGridRepair.Rows[i].Cells["UnitName"].Value.ToString();
                                    drRepairD["EtcDesc"] = this.uGridRepair.Rows[i].Cells["EtcDesc"].Value.ToString();              //특이사항
                                    drRepairD["CancelFlag"] = this.uGridRepair.Rows[i].Cells["CancelFlag"].Value.ToString().Substring(0,1);
                                    dtDurableRepairD.Rows.Add(drRepairD);
                                    //}
                                    //else
                                    //{
                                    //    DataRow drRepairDel;
                                    //    drRepairDel = dtDurableRepairDel.NewRow();
                                    //    drRepairDel["PlantCode"] = strPlantCode;
                                    //    drRepairDel["RepairGICode"] = this.uGridRepair.Rows[i].Cells["RepairGICode"].Value.ToString();
                                    //    drRepairDel["Seq"] = this.uGridRepair.Rows[i].Cells["Seq"].Value.ToString();
                                    //    dtDurableRepairDel.Rows.Add(drRepairDel);
                                    //}
                                }

                            }
                        }      
                    }
                }
                #endregion

                if (dtDurableRepairD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "저장정보 확인", "저장데이터가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                #endregion

                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsDurableMatRepairH.mfSaveDurableMatRepairH(dtDurableRepairH, dtDurableRepairD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    /////////drRepairD["RepairGICode"] = this.uTextRepairGICode.Text;

                    ///---Decoding---//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    

                    
                    //--커서변경--//
                    this.MdiParent.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        

                        mfSearch();
                        
                        #region 수리출고구분이 외부인정보만 찾아서 반출등록으로 보냄
                        //DataTable dtCarryOut = new DataTable();

                        //---수리출고 구분이 외부인 정보만 찾아서 저장 시킨다 ---//

                        //if (dtDurableRepairD.Select("RepairGIGubunCode = 'OUT' AND CancelFlag = 'F'").Count() != 0)
                        //{
                        //    string strRepairGICode = ErrRtn.mfGetReturnValue(0); //주석제거해야함
                        //    DataRow[] drCarryOut = dtDurableRepairD.Select("RepairGIGubunCode = 'OUT'");
                        //    dtCarryOut = drCarryOut.CopyToDataTable();

                        //    dtCarryOut.Columns["ChgDurableMatCode"].ColumnName = "ItemCode";
                        //    dtCarryOut.Columns["ChgDurableMatName"].ColumnName = "ItemName";
                        //    dtCarryOut.Columns["ChgDurableInventoryCode"].ColumnName = "InventoryCode";
                        //    dtCarryOut.Columns["ChgQty"].ColumnName = "CarryOutQty";
                        //    dtCarryOut.Columns["EtcDesc"].ColumnName = "CarryOutEtcDesc";

                        //    dtCarryOut.Rows[0]["RepairGICode"] = strRepairGICode;

                        //    QRPEQU.UI.frmEQU0012 frmCarryOut = new QRPEQU.UI.frmEQU0012(dtCarryOut);
                            
                        //    CommonControl cControl = new CommonControl();
                        //    Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        //    Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //    if (uTabMenu.Tabs.Exists("QRPEQU" + "," + "frmEQU0012"))
                        //    {

                        //        uTabMenu.Tabs["QRPEQU" + "," + "frmEQU0012"].Close();

                        //    }
                        //    uTabMenu.Tabs.Add("QRPEQU" + "," + "frmEQU0012", "반출등록");
                        //    uTabMenu.SelectedTab = uTabMenu.Tabs["QRPEQU,frmEQU0012"];

                        //    frmCarryOut.FormName = this.Name;
                        //    frmCarryOut.AutoScroll = true;
                        //    frmCarryOut.MdiParent = this.MdiParent;
                        //    frmCarryOut.ControlBox = false;
                        //    frmCarryOut.Dock = DockStyle.Fill;
                        //    frmCarryOut.FormBorderStyle = FormBorderStyle.None;
                        //    frmCarryOut.WindowState = FormWindowState.Normal;
                        //    frmCarryOut.Text = "반출등록";
                        //    frmCarryOut.Show();
                            

                        //}
                        #endregion

                    }
                    else
                    {
                        
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ////System ResourceInfo
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();



                //#region 필수입력사항 확인

                //if (this.uGridRelease.Rows.Count == 0)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                //    return;
                //}
                //else if (this.uGroupBoxContentsArea.Expanded == false || this.uTextEquipCode.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                //    return;
                //}
                //else if (this.uTextRepairGICode.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                //    return;
                //}
                //else if (this.uComboSearchPlant.Value.ToString() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    this.uComboSearchPlant.DropDown();
                //    return;
                //}
                //else if (this.uTextRequestID.Text == "" || this.uTextRequestName.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                               , "확인창", "필수입력사항 확인", "수리출고요청자를 입력해주세요", Infragistics.Win.HAlign.Right);

                //    this.uTextRequestID.Focus();
                //    return;
                //}
                //for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                //{
                //    if (!Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value))
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                    , "확인창", "확인창", "수리출고구성 정보가 있어 삭제할 수 없습니다.", Infragistics.Win.HAlign.Right);
                //        return;
                //    }
                //}

                //#endregion

                //string strPlantCode = this.uComboSearchPlant.Value.ToString();
                //string strRepairGICode = this.uTextRepairGICode.Text;
                ////------------------삭제여부 묻기 -----------------------//
                //if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                //                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                //{

                //    //----------POPUP창 보여줌----------//
                //    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //    Thread t1 = m_ProgressPopup.mfStartThread();
                //    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");

                //    //------커서변경-------//
                //    this.MdiParent.Cursor = Cursors.WaitCursor;

                //    //처리 로직//
                //    //설비점검출고등록 BL 호출
                //    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                //    QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                //    brwChannel.mfCredentials(clsDurableMatRepairH);

                //    //설비점검출고등록 삭제 매서드 호출
                //    string strErrRtn = clsDurableMatRepairH.mfDeleteDurableMatRepairH(strPlantCode,strRepairGICode);
                    
                //    //Decoding
                //    TransErrRtn ErrRtn = new TransErrRtn();
                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                //    //----커서변경------//
                //    this.MdiParent.Cursor = Cursors.Default;

                //    //-------------POPUP창 닫기---------------//
                //    m_ProgressPopup.mfCloseProgressPopup(this);

                //    //----------------처리결과에 따른 메세지 박스 -----------------//
                //    System.Windows.Forms.DialogResult result;
                //    if (ErrRtn.ErrNum == 0)
                //    {
                //        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                //                                    Infragistics.Win.HAlign.Right);
                //        //InitValue();
                //        mfSearch();
                //        ClearValue();
                //    }
                //    else
                //    {
                //        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                //                                     Infragistics.Win.HAlign.Right);
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
               
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridRelease.Rows.Count == 0 && ((this.uGridRepair.Rows.Count == 0 && this.uGridPMResult.Rows.Count == 0) || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "엑셀출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }


                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridRelease.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridRelease);

                if (this.uGridRepair.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridRepair);

                if (this.uGridPMResult.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridPMResult);

                /////////////

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region 이벤트

        // GroupContentsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridRelease.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;

                    this.uGridRelease.Height = 720;

                    for (int i = 0; i < uGridRelease.Rows.Count; i++)
                    {
                        uGridRelease.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {
            }
        }

            #region 검색창 이벤트

        //공장 선택 시 설비 그룹 Area Station 공정구분 콤보박스 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                 string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    //콤보 아이템 클리어
                    //this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();

                    if (this.uGroupBoxContentsArea.Expanded == true)
                    {
                        this.uGroupBoxContentsArea.Expanded = false;
                    }
                    ClearValue();

                    //----- 점검결과정보 클리어-----//
                    while (this.uGridRelease.Rows.Count > 0)
                    {
                        this.uGridRelease.Rows[0].Delete(false);
                    }

                    string strLang = m_resSys.GetString("SYS_LANG");

                    ////------------Area콤보 변화-----------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);

                    //DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, strLang);

                    //wCom.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체",
                    //    "AreaCode", "AreaName", dtArea);

                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체",
                        "StationCode", "StationName", dtStation);

                    ////-----------설비공정구분콤보 변화 -------------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    //brwChannel.mfCredentials(clsEquipProcGubun);

                    //DataTable dtGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, strLang);

                    //wCom.mfSetComboEditor(this.uComboSearchProcGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    //    "", "", "전체", "EquipProcGubunCode", "EquipProcGubunName", dtGubun);


                    //-----금형치공구창고정보-----//
                    WinGrid wGrid = new WinGrid();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    brwChannel.mfCredentials(clsDurableInventory);

                    DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, strLang);

                    wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);

                   
                    //if (strPlantCode != "")
                    //{
                    //    mfSearch();
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Area 선택 전 설비그룹 선택여부 체크
        private void uComboSearchArea_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uComboSearchArea.Value.ToString() != "")
                {
                    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                    {
                       
                        this.uComboSearchEquipGroup.Value = "";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Station 선택시 설비위치,설비대분류,설비중분류,설비그룹콤보 변경
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchEquipLoc.Items.Clear();

                    WinComboEditor wCom = new WinComboEditor();
                    wCom.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);


                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //공정구분 선택 전 설비그룹 선택여부 체크
        private void uComboSearchProcGubun_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uComboSearchProcGubun.Value.ToString() != "")
                {
                    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                    {
                      
                        this.uComboSearchEquipGroup.Value = "";
                        
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치콤보를 선택시 설비대분류,설비중분류,설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLoc.Value.ToString();

                if (!strCode.Equals(string.Empty))
                {
                    //현재 텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비대분류 선택시 설비중분류 설비그룹이바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    //현재 콤보텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비중분류를  선택하면 설비 그룹이 바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboEquipLargeType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLargeType.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    //현재 콤보텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string strText = this.uComboEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

            #endregion

            #region Text이벤트

        //정비사 버튼 클릭 시 유저 정보 보여주기
        private void uTextTechnician_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 보낸다.
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();


                this.uTextTechnician.Text = frmUser.UserID;
                this.uTextTechnicianName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 ID입력 후 엔터누를 시 이름 자동입력
        private void uTextTechnician_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                        this.uTextTechnicianName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextTechnician.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextTechnician.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextTechnicianName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "확인창", "입력확인", "입력하신 ID의 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextTechnicianName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTechnician_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextTechnicianName.Text.Equals(string.Empty))
                this.uTextTechnicianName.Clear();
        }

        //수리출고 요청자 ID입력 후 엔터누를 시 이름 자동입력
        private void uTextRequestID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    
                        this.uTextRequestName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextRequestID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextRequestID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextRequestName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "확인창", "입력확인", "입력하신 ID의 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextRequestName.Clear();
                    }

                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //수리출고요청자 버튼 클릭 시 유저 정보 보여주기
        private void uTextRequestID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 보낸다.
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                this.uTextRequestID.Text = frmUser.UserID;
                this.uTextRequestName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

            #endregion

            #region 그리드 이벤트
        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                ////System ResourceInfo
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;
                


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }


        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        private void uGridRepair_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;

                 // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridRepair, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                } 
              
                //--체크박스가 수정불가 일시 false로 되돌린다 
                if (strColumn == "Check")
                {
                    if ( e.Cell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    {
                        e.Cell.Value = false;
                        return;
                    }
                }
                //-- 수리취소 체크 박스를 체크 해제하면 편집이미지 X --//
                if (strColumn == "CancelFlag")
                {
                    if (e.Cell.Value.ToString() == "False")
                    {
                        e.Cell.Row.RowSelectorAppearance.Image = null;
                        return;
                    }
                }

                #region 수리출고 구분 셀업데이트시 창고 교체량 교체구성품코드 셀의 헤더 색이 필수로 변한다
                if (strColumn == "RepairGIGubunCode")
                {
                    if (e.Cell.Value.ToString() != "")
                    {
                        //-- 헤더 색변화 --//
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Red;
                    }
                    else
                    {
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Black;
                    }
                }
                #endregion

                #region 수량 비교
                //수량을 입력한 후 해당설비구성품의 수량과 비교함 (입력한 수량보다 설비구성품의수량이 크면 안됨)
                if (strColumn.Equals("InputQty") && !e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Value.ToString().Equals("0"))
                {
                    //기존구성품이 입력되어 있을 경우 수량 비교가 가능함.
                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;
                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridRepair.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                            {
                                if (
                                    e.Cell.Row.Cells["CurDurableMatCode"].Value.Equals(this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value))
                                {

                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value);

                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["CurLotNo"].Tag;

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "사용량 확인", "선택하신 구성품의 수량이 " + strQty + " 초과하였습니다.", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }

                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "수량입력 확인", "입력하신 수량이 해당구성품의 수량보다 많습니다.", Infragistics.Win.HAlign.Right);
                            //수량되돌림
                            e.Cell.Value = e.Cell.Tag;
                            //PerFormAction
                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[e.Cell.Row.Index].Cells["InputQty"];
                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                    }
                    else    //기존구성품이 입력되어 있지 않은 경우 수량 0으로 초기화
                        e.Cell.Value = 0;

                }
                #endregion

                #region 교체수량비교

                if(strColumn.Equals("ChgQty"))
                {
                    if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if ((!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) && (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag)))
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "수량입력 확인", "입력하신 교체수량이 가용수량이나 기존수량보다 많습니다.", Infragistics.Win.HAlign.Right);
                            
                            //교체하는 금형치공구 수량적용
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag))
                            {
                                e.Cell.Value = 0;//e.Cell.Row.Cells["InputQty"].Tag;
                            }
                            else
                            {
                                e.Cell.Value = 0;// e.Cell.Row.Cells["Qty"].Value;
                            }
                            
                            //PerFormAction
                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[e.Cell.Row.Index].Cells["ChgQty"];
                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        #region 중복구성품재고수량체크

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridRepair.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridRepair.Rows[i].Hidden.Equals(false)
                                && this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(string.Empty)
                                )
                            {
                                if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value) &&
                                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridRepair.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "교체수량 확인", "입력하신 구성품의 교체수량이 " + strQty + " 초과하였습니다.", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }


                        #endregion
                    }
                    
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //더블클릭시 클릭 한 행에 대해 상세정보 ExpandGroupBox 에서 보여줌
        private void uGridRelease_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                //선택한 셀의 줄에 정보가 있는 경우 ExpandGroupBox 목록 Display
                if (!wGrid.mfCheckCellDataInRow(this.uGridRepair, 0, e.Cell.Row.Index))
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                    //공장, 설비, 점검출고코드 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    string strRepairGICode = e.Cell.Row.Cells["RepairGICode"].Value.ToString();
                    //BL호출
                    

                    #region 저장된 수리출고 구성품정보가 있는경우 그리드에 바인드

                    //---수리출고 구성품---//
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                    QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                    brwChannel.mfCredentials(clsDurableMatRepairD);

                    DataTable dtPMDurable = clsDurableMatRepairD.mfReadDurableMatRepairD(strPlantCode, strEquipCode, strRepairGICode, m_resSys.GetString("SYS_LANG"));

                    WinGrid grd = new WinGrid();

                    //---데이터바인드---//
                    this.uGridRepair.DataSource = dtPMDurable;
                    this.uGridRepair.DataBind();

                    if (dtPMDurable.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            this.uGridRepair.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["CurDurableMatName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            //this.uGridRepair.Rows[i].Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                            //수리취소된 정보를 노랑줄로 바꾸고 수리취소체크박스 수정불가처리
                            if (Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value) == true)
                            {
                                this.uGridRepair.Rows[i].Appearance.BackColor = Color.Yellow;
                                this.uGridRepair.Rows[i].Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        }
                        grd.mfSetAutoResizeColWidth(this.uGridRepair, 0);
                    }
                    else
                    {
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                        return;
                    }

                    //-------------그리드 값 텍스트에 넣기 --------------//
                    this.uTextRepairGICode.Text = strRepairGICode;
                    this.uTextEquipCode.Text = strEquipCode;
                    this.uTextEquipCode.Tag = e.Cell.Row.Cells["ModelName"].Value.ToString();
                    this.uTextEquipName.Text = e.Cell.Row.Cells["EquipName"].Value.ToString();
                    this.uTextPMPlanDate.Text = e.Cell.Row.Cells["PMPlanDate"].Value.ToString();
                    this.uTextEtcDesc.Text = e.Cell.Row.Cells["EtcDesc"].Value.ToString();

                    if (e.Cell.Row.Cells["RepairGIReqDate"].Value.ToString() != "")
                    {
                        this.uDateRepairReqDate.Value = e.Cell.Row.Cells["RepairGIReqDate"].Value.ToString();

                    }

                    if (e.Cell.Row.Cells["RepairGIReqID"].Value.ToString() != "")
                    {
                        uTextRequestID.Text = e.Cell.Row.Cells["RepairGIReqID"].Value.ToString();
                        uTextRequestName.Text = e.Cell.Row.Cells["RepairGIReqName"].Value.ToString();
                    }

                    #endregion

                    #region 수리출고고성품그리드안의 구성품콤보그리드

                    //---구성품코드 정보 ---//


                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                    QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                    brwChannel.mfCredentials(clsEquipDurableBOM);

                    DataTable dtDurableMat = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //콤보 그리드 리스트 클러어
                    //this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.InitializeFrom(this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists);
                    this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                    //콤보그리드 컬럼설정
                    string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                    string strText = "구성품코드,구성품명,LotNo,수량,단위";

                    //--그리드에 추가 --
                    wGrid.mfSetGridColumnValueGridList(this.uGridRepair, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                        , "DurableMatCode", "DurableMatName", dtDurableMat);


                    #endregion

                    //---값 저장---//

                    string strPMPlanDate = this.uTextPMPlanDate.Text;
                    string strPlanYear = strPMPlanDate.Substring(0, 4);

                    #region 점검결과조회

                    //---점검결과조회---//
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMResult), "PMResult");
                    QRPEQU.BL.EQUMGM.PMResult clsPMResult = new QRPEQU.BL.EQUMGM.PMResult();
                    brwChannel.mfCredentials(clsPMResult);

                    DataTable dtPMResult = clsPMResult.mfReadPMResult(strPlantCode, strPlanYear, strEquipCode, strPMPlanDate, m_resSys.GetString("SYS_LANG"));

                    //---데이터바인드---//
                    this.uGridPMResult.DataSource = dtPMResult;
                    this.uGridPMResult.DataBind();

                    if (dtPMResult.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGridPMResult, 0);
                    #endregion

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //수리출고구성품그리드에서 창고 콤보를 선택하기 전 수리출고구분 체크
        private void uGridRepair_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //---창고 컬럼일때 발생--//
                if (e.Cell.Column.Key == "ChgDurableInventoryCode")
                {
                    //--- 수리출고구분이 "" 일경우 메세지 박스 ---//
                    if (e.Cell.Row.Cells["RepairGIGubunCode"].Value.ToString() == "")
                    {
                        
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "입력사항 확인", "수리출고구분을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[e.Cell.Row.Index].Cells["RepairGIGubunCode"];
                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }

                    if (this.uTextEquipCode.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString(SysRes.SystemInfoRes), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "정보확인", "저장데이터가 없습니다. 그리드리스트의 정보를 더블클릭 해주십시요", Infragistics.Win.HAlign.Right);

                        if (this.uGroupBoxContentsArea.Expanded == true)
                        {
                            this.uGroupBoxContentsArea.Expanded = false;
                        }

                        return;
                    }
                }

               
                //수리출고구분 선택전 헤더에 정보 유무확인
                if (e.Cell.Column.Key.Equals("RepairGIGubunCode"))
                {
                    if (this.uTextEquipCode.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString(SysRes.SystemInfoRes), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "정보확인", "저장데이터가 없습니다. 그리드리스트의 정보를 더블클릭 해주십시요", Infragistics.Win.HAlign.Right);

                        if (this.uGroupBoxContentsArea.Expanded == true)
                        {
                            this.uGroupBoxContentsArea.Expanded = false;
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        //-- 그리드 컬럼중 리스트 컬럼의 아이템을 선택하였을 때 발생함 --//
        private void uGridRepair_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                
                 string strColumn =  e.Cell.Column.Key;
                 int intSelectRow;
                //공장 정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //선택한 리스트의 Key값과 Value값 저장(컬럼그리드콤보가 없으면 셀그리드 콤보 정보로
                string strKey = "";
                string strValue = "";

                if(e.Cell.Column.ValueList != null)
                {
                    strKey = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }
                

                 #region 구성품명 의 리스트 선택시 선택한 이름과 코드를 각 셀에 넣는다
                 
                 if (strColumn == "CurDurableMatName")
                 {
                 
                     //공백일 경우
                     if (strKey.Equals(string.Empty))
                     {
                         //수량,구성품코드 공백처리
                         e.Cell.Row.Cells["InputQty"].Value = 0;
                         e.Cell.Row.Cells["CurDurableMatCode"].Value = "";
                         
                     }
                     else
                     {
                         intSelectRow = e.Cell.Column.ValueList.SelectedItemIndex;

                         //설비구성품정보 BL 호출
                         
                         //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                         //QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                         //brwChannel.mfCredentials(clsEquipDurableBOM);

                         //DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOM_DurableMatInputQty(strPlantCode, this.uTextEquipCode.Text, strKey, m_resSys.GetString("SYS_LANG"));

                         brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                         QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                         brwChannel.mfCredentials(clsEquipDurableBOM);

                         DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                         e.Cell.Row.Cells["CurDurableMatCode"].Value = strKey;

                         //LotNo가 없으면 수량을 직접입력할수있고(100개중 50개만 교체대상이 될수도있음) LotNo가 있으면 할수없다.(LotNo가있는 구성품은 무조건1개라서)
                         if (dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                         {
                             //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                             this.uGridRepair.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                             e.Cell.Row.Cells["InputQty"].Tag = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                             e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                             e.Cell.Row.Cells["CurLotNo"].Value = "";
                         }
                         else
                         {
                             //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                             this.uGridRepair.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                             e.Cell.Row.Cells["InputQty"].Tag = 1;
                             e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                             e.Cell.Row.Cells["CurLotNo"].Value = dtDurableMatInfo.Rows[intSelectRow]["LotNo"];
                         }

                         //단위저장
                         e.Cell.Row.Cells["CurLotNo"].Tag = dtDurableMatInfo.Rows[intSelectRow]["UnitCode"];

                         //Lot정보가 있는 구성품을 선택하였을 경우 리스트내에 똑같은 구성품이 올수없다.
                         for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                         {
                             if (!i.Equals(e.Cell.Row.Index) &&
                                 this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString()) &&
                                 !dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                 !this.uGridRepair.Rows[i].Appearance.BackColor.Equals(Color.Yellow) &&
                                 this.uGridRepair.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                             {
                                 msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                             , "확인창", "LotID확인", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

                                 e.Cell.Row.Cells["InputQty"].Value = 0;
                                 e.Cell.Row.Cells["InputQty"].Tag = 0;
                                 e.Cell.Row.Cells["CurLotNo"].Value = string.Empty;
                                 e.Cell.Row.Cells["CurLotNo"].Tag = string.Empty;
                                 e.Cell.Row.Cells["CurDurableMatCode"].Value = string.Empty;
                                 e.Cell.Row.Cells["CurDurableMatName"].Value = string.Empty;
                                 e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                 return;
                             }

                             
                         }

                         //창고 콤보가 선택되어있다면,
                         if (!e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString().Equals(string.Empty))
                         {
                             if (!e.Cell.Row.Cells["CurLotNo"].Value.Equals(e.Cell.Row.Cells["ChgLotNo"].Value))
                             {
                                 ChgDurbleMat(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(), e.Cell.Row.Index);
                                 e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                 e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                 e.Cell.Row.Cells["Qty"].Value = 0;
                                 e.Cell.Row.Cells["ChgQty"].Value = 0;
                             }
                             else
                             {
                                 if (e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty) || Convert.ToInt32(e.Cell.Row.Cells["ChgQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value))
                                     e.Cell.Row.Cells["ChgQty"].Value = 0;
                             }
                         }
                         
                     }

                 }
                 #endregion

                 #region 교체구성품창고의 구성품코드 리스트를 가져온다
                 //선택한 교체구성품창고의 구성품코드리스트를 가져온다.
                 if (strColumn.Equals("ChgDurableInventoryCode"))
                 {
                     if (!strKey.Equals(string.Empty))
                     {
                         ChgDurbleMat(strPlantCode, strKey, e.Cell.Row.Index);
                     }
                     
                 }
                 #endregion

                 #region 교체구성품코드 선택시 가용량 Lot정보 삽입

                 if (strColumn.Equals("ChgDurableMatName"))
                 {
                     if (!strKey.Equals(string.Empty))
                     {
                         brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                         QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                         brwChannel.mfCredentials(clsRepairReq);

                         DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                         if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                             return;


                         //금형치공구정보 BL 호출
                         brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                         QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                         brwChannel.mfCredentials(clsDurableStock);

                         //금형치공구정보 Detail매서드 호출
                         DataTable dtStock = new DataTable();


                         //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                         string strLotChk = "";

                         if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                             strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                         dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                 dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                 strLotChk, m_resSys.GetString("SYS_LANG"));


                         if (dtStock.Rows.Count > 0)
                         {
                             intSelectRow = e.Cell.ValueList.SelectedItemIndex;

                             ////구성품끼리의 단위가 다를경우 경고
                             //if (!e.Cell.Row.Cells["CurLotNo"].Tag.Equals(dtStock.Rows[intSelectRow]["UnitCode"]))
                             //{
                             //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                             //                       , "확인창", "구성품 단위 확인", "기존구성품의 단위와 교체구성품의 단위가 다릅니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                             //    return;
                             //}
                             //그리드콤보에 선택한 정보를 삽입
                             e.Cell.Row.Cells["ChgLotNo"].Value = dtStock.Rows[intSelectRow]["LotNo"];
                             e.Cell.Row.Cells["Qty"].Value = dtStock.Rows[intSelectRow]["Qty"];
                             e.Cell.Row.Cells["ChgDurableMatCode"].Value = strKey;
                             //e.Cell.Row.Cells["UnitCode"].Value = dtStock.Rows[intSelectRow]["UnitCode"];

                             //Lot정보가 있을경우 교체수량1개 수정불가로 변경
                             if (!dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                             {
                                 e.Cell.Row.Cells["ChgQty"].Value = 1;

                                 e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                             }
                             else
                             {
                                 e.Cell.Row.Cells["ChgQty"].Value = 0;
                                 e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                             }

                             //선택한 정보가 그리드내에 있으면 다시입력
                             for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                             {
                                 if (!i.Equals(e.Cell.Row.Index) &&
                                     uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtStock.Rows[intSelectRow]["LotNo"].ToString()) &&
                                     !dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                     this.uGridRepair.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit &&
                                    !this.uGridRepair.Rows[i].Appearance.BackColor.Equals(Color.Yellow))
                                 {
                                     msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                 , "확인창", "LotID확인", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

                                     e.Cell.Row.Cells["CurQty"].Value = 0;
                                     e.Cell.Row.Cells["ChgQty"].Value = 0;

                                     e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                     e.Cell.Row.Cells["ChgDurableMatCode"].Value = string.Empty;
                                     e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;   
                                     e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                     return;
                                 }
                             }
                         }
                     }
                 }

                 #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //-- 새로운 줄 생성 시 CancelFlag 수정불가
        private void uGridRepair_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            this.uGridRepair.Rows[e.Row.Index].Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }
            #endregion

        // 행삭제 이벤트 
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                //-- 체크 박스에 체크된 행을 숨김 처리한다 --//
                for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["Check"].Value) == true)
                    {
                        //if (this.uGridRepair.Rows[i].Cells["RepairGICode"].Value.ToString() == "" && this.uGridRepair.Rows[i].Cells["Seq"].Value.ToString() == "0")
                        //{
                            //QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                            //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            //msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                            //                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                         "확인창", "입력확인", "신규로 등록하신 정보만 삭제가 됩니다.",
                            //                         Infragistics.Win.HAlign.Right);

                            this.uGridRepair.Rows[i].Hidden = true;
                        //}
                        //else
                        //{
                            //this.uGridRepair.Rows[i].Cells["Check"].Value = false;
                        //}

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion



        /// <summary>
        /// ExpandGroupBox Value초기화
        /// </summary>
        private void ClearValue()
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //수리출고구성품
                while (this.uGridRepair.Rows.Count > 0)
                {
                    this.uGridRepair.Rows[0].Delete(false);
                }
                //점검결과조회
                while (this.uGridPMResult.Rows.Count > 0)
                {
                    this.uGridPMResult.Rows[0].Delete(false);
                }
                //텍스트 박스들
                this.uTextRepairGICode.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextPMPlanDate.Text = "";

                //수리출고요청일
                this.uDateRepairReqDate.Value = DateTime.Now;


                string strPlantcode = m_resSys.GetString("SYS_PLANTCODE");
                string strUserID = m_resSys.GetString("SYS_USERID");

                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //brwChannel.mfCredentials(clsUser);

                //DataTable dtUser = clsUser.mfReadSYSUser(strPlantcode, strUserID, m_resSys.GetString("SYS_LANG"));

                string strUserName = m_resSys.GetString("SYS_USERNAME");


                // 수리출고요청자 ID 설정
                this.uTextRequestID.Text = strUserID;
                this.uTextRequestName.Text = strUserName;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 교체구성품그리드콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInven">창고정보</param>
        /// <param name="e">Infragistics.Win.UltraWinGrid.CellEventArgs</param>
        private void ChgDurbleMat(string strPlantCode, string strInven, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }

                //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                string strLotChk = "";

                if (!this.uGridRepair.Rows[intIndex].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    strLotChk = this.uGridRepair.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //금형치공구창고에 해당하는 금형치공구 재고 조회
                DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, strInven, dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                strLotChk, m_resSys.GetString("SYS_LANG"));

                //금형치공구정보에 검색조건 적용
                WinGrid wGrid = new WinGrid();

//                this.uGridRepair.Rows[intIndex].Cells["ChgDurableMatName"].Column.Layout.ValueLists.Clear();
                string strDropDownValue = "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName";
                string strDropDownText = "치공구코드,치공구,LOTNO,수량,단위코드,단위";

                //콤보그리드 초기화.
                //e.Cell.Column.Band.Layout.ValueLists.Clear();

                wGrid.mfSetGridCellValueGridList(this.uGridRepair, 0, intIndex, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                   , strDropDownValue, strDropDownText, "DurableMatCode", "DurableMatName", dtStock);

                if (dtStock.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "확인창", "구성품조회 결과확인", "교체가능한 구성품이 없습니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQUZ0009_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
                /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");

                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }







    }
}
