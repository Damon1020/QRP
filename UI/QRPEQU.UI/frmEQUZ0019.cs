﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQUZ0019.cs                                        */
/* 프로그램명   : 자재출고등록                                          */
/* 작성자       : 권종구 , 코딩 : 남현식                                */
/* 작성일자     : 2011-06-30                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0019 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQUZ0019()
        {
            InitializeComponent();
        }

        private void frmEQUZ0019_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0019_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitText();
            InitLabel();
            InitGrid();
            InitCombo();

            this.uGroupBoxContentsArea.Expanded = false;
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmEQUZ0019_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("자재출고등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                this.uTextTransferChargeID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextTransferChargeName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextEtcDesc.Text = "";
                this.uTextTransferCode.Text = "";

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchTransferDate, "출고일", m_resSys.GetString("SYS_FONTNAME"), true, true);

                lbl.mfSetLabel(this.uLabelPlant1, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTransferCode, "출고번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTransferDate, "출고일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTransferChargeID, "출고담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);                
                lbl.mfSetLabel(this.uLabel6, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel7, "출고내역", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                //1
                grd.mfInitGeneralGrid(this.uGridSPTransferH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //1
                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false,true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "TransferCode", "출고문서번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "TransferDate", "출고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "TransferChargeID", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "TransferChargeName", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferH, 0, "StockFlag", "재고반영여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridSPTransferH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSPTransferH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd.mfAddRowGrid(this.uGridSPTransferH, 0);

                //2
                WinGrid grd2 = new WinGrid();
                grd2.mfInitGeneralGrid(this.uGridSPTransferD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FOTNNAME"));
                //2
                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgEquipCode", "교체설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgEquipName", "교체설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 100, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgSparePartCode", "교체SparePart", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "InputQty", "구성품수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgSpec", "구성품Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgMaker", "구성품Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPInventoryCode", "출고창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");


                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "Qty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferQty", "출고량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnn", "0");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferEtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgFlag", "교체여부플래그", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgDate", "교체일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 15, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridSPTransferD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSPTransferD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd2.mfAddRowGrid(this.uGridSPTransferD, 0);


                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGridSPTransferD, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }


        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

#endregion

     

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferH), "SPTransferH");
                QRPEQU.BL.EQUSPA.SPTransferH clsSPTransferH = new QRPEQU.BL.EQUSPA.SPTransferH();
                brwChannel.mfCredentials(clsSPTransferH);
                
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromTransferDate = this.uDateFromTransferDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strToTransferDate = this.uDateToTransferDate.DateTime.Date.ToString("yyyy-MM-dd");

                // Call Method
                DataTable dtSPTransferH = clsSPTransferH.mfReadEQUSPTransferH(strPlantCode, strFromTransferDate, strToTransferDate, "F", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridSPTransferH.DataSource = dtSPTransferH;
                uGridSPTransferH.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtSPTransferH.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001115", "M001102",
                                                                           Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSPTransferH, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                string strStockFlag = "F";
                int intInputQty = 0;
                int intQty = 0;
                int intTransferQty = 0;


                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextTransferChargeID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001164", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextTransferChargeID.Focus();
                    return;
                }
                if (this.uTextTransferChargeName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001164", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextTransferChargeID.Focus();
                    return;
                }
                
                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                string strLang = m_resSys.GetString("SYS_LANG");

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];

                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;

                        if (this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000489",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000486",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000533",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }


                        if (this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000476", strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000499", strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["UnitCode"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        intQty = Convert.ToInt32(this.uGridSPTransferD.Rows[i].Cells["Qty"].Value.ToString());
                        intTransferQty = Convert.ToInt32(this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString());
                        intInputQty = Convert.ToInt32(this.uGridSPTransferD.Rows[i].Cells["InputQty"].Value.ToString());

                        if (intTransferQty > intQty)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000532",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["TransferQty"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (intTransferQty > intInputQty)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000531", strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["TransferQty"];
                            this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001237", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                DialogResult dirStockFlag = new DialogResult();
                dirStockFlag = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M001017"
                                            , Infragistics.Win.HAlign.Right);

                

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferH), "SPTransferH");
                QRPEQU.BL.EQUSPA.SPTransferH clsSPTransferH = new QRPEQU.BL.EQUSPA.SPTransferH();
                brwChannel.mfCredentials(clsSPTransferH);


                //--------- 1. EQUSPTransferH 테이블에 저장 ----------------------------------------------//
                DataTable dtSPTransferH = clsSPTransferH.mfSetDatainfo();
                row = dtSPTransferH.NewRow();
                row["PlantCode"] = this.uComboPlant.Value.ToString();
                row["TransferCode"] = this.uTextTransferCode.Text;
                row["TransferDate"] = this.uDateTransferDate.DateTime.Date.ToString("yyyy-MM-dd");
                row["TransferChargeID"] = this.uTextTransferChargeID.Text;
                row["EtcDesc"] = this.uTextEtcDesc.Text;

                if (dirStockFlag == DialogResult.No)
                {
                    row["StockFlag"] = "F";
                    strStockFlag = "F";
                }
                else
                {
                    row["StockFlag"] = "T";
                    strStockFlag = "T";
                }
                dtSPTransferH.Rows.Add(row);


                //-------- 2.  EQUSPTransferD 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);

                DataTable dtSPTransferD = clsSPTransferD.mfSetDatainfo();

                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        row = dtSPTransferD.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["TransferCode"] = this.uTextTransferCode.Text;
                        row["TransferSeq"] = i + 1;
                        row["TransferSPInventoryCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"].Value.ToString();
                        row["TransferSPCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                        row["TransferQty"] = this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                        row["ChgEquipCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                        row["ChgSparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                        row["InputQty"] = this.uGridSPTransferD.Rows[i].Cells["InputQty"].Value.ToString();
                        row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                        row["TransferEtcDesc"] = this.uGridSPTransferD.Rows[i].Cells["TransferEtcDesc"].Value.ToString();
                        row["ChgFlag"] = "";
                        row["ChgDate"] = "";
                        row["ChgChargeID"] = "";
                        row["ChgEtcDesc"] = "";
                        row["ReturnFlag"] = "";
                        row["ReturnDate"] = "";
                        row["ReturnChargeID"] = "";
                        row["ReturnSPInventoryCode"] ="";
                        row["ReturnEtcDesc"] = "";
                        dtSPTransferD.Rows.Add(row);                        
                    }
                }


                //-------- 3.  EQUSPChgStandby 테이블에 저장 (수량 + 처리)----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPChgStandby), "SPChgStandby");
                QRPEQU.BL.EQUSPA.SPChgStandby clsSPChgStandby = new QRPEQU.BL.EQUSPA.SPChgStandby();
                brwChannel.mfCredentials(clsSPChgStandby);

                DataTable dtSPChgStandby = clsSPChgStandby.mfSetDatainfo();

                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        row = dtSPChgStandby.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["SparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                        row["ChgQty"] = this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                        row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                        dtSPChgStandby.Rows.Add(row);
                    }
                }


                //-------- 4.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 수량 (-) 처리 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                DataTable dtSPStock = clsSPStock.mfSetDatainfo();

                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];

                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                            row = dtSPStock.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"].Value.ToString();
                            row["SparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                            row["Qty"] =  "-" + this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStock.Rows.Add(row);
                    }
                }


                //-------- 5.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                DataTable dtSPStockMoveHist = clsSPStockMoveHist.mfSetDatainfo();
                
                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];

                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                            row = dtSPStockMoveHist.NewRow();

                            row["MoveGubunCode"] = "M03";       //자재출고일 경우는 "M03"
                            row["DocCode"] = "";
                            row["MoveDate"] = this.uDateTransferDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["MoveChargeID"] = this.uTextTransferChargeID.Text;
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"].Value.ToString();
                            row["EquipCode"] =  this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            row["SparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                            row["MoveQty"] = "-" + this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStockMoveHist.Rows.Add(row);
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SAVESparePartStock), "SAVESparePartStock");
                QRPEQU.BL.EQUSPA.SAVESparePartStock clsSAVESparePartStock = new QRPEQU.BL.EQUSPA.SAVESparePartStock();
                brwChannel.mfCredentials(clsSAVESparePartStock);
                string rtMSG = clsSAVESparePartStock.mfSaveTransfer(dtSPTransferH, dtSPTransferD, dtSPChgStandby, dtSPStock, dtSPStockMoveHist, strStockFlag, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    if (this.uGridSPTransferD.Rows.Count > 0)
                    {
                        this.uGridSPTransferD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridSPTransferD.Rows.All);
                        this.uGridSPTransferD.DeleteSelectedRows(false);
                    }
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {

                    while (this.uGridSPTransferD.Rows.Count > 0)
                    {
                        this.uGridSPTransferD.Rows[0].Delete(false);
                    }
                    uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    WinGrid grd = new WinGrid();

                    while (this.uGridSPTransferD.Rows.Count > 0)
                    {
                        this.uGridSPTransferD.Rows[0].Delete(false);
                    }
                    //while (this.uGridSPTransferH.Rows.Count > 0)
                    //{
                    //    //this.uGridSPTransferH.Rows[0].Delete(false);
                    //}

                }

                InitText();
                
               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSPTransferH.Rows.Count == 0 && (this.uGridSPTransferD.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                WinGrid grd = new WinGrid();
                if (this.uGridSPTransferH.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridSPTransferH);

                if (this.uGridSPTransferD.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    grd.mfDownLoadGridToExcel(this.uGridSPTransferD);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion
        
        #region 이벤트

        //그룹박스 펼침상태
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridSPTransferH.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridSPTransferH.Height = 720;
                    for (int i = 0; i < uGridSPTransferH.Rows.Count; i++)
                    {
                        uGridSPTransferH.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                mfCreate();
                string strPlantCode = uComboPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    GridComboList(strPlantCode, 0);
                    GridCombo(strPlantCode);




                }                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQUZ0019_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        this.uGridSPTransferD.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTransferChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextTransferChargeID;
                uTextName = this.uTextTransferChargeName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTransferChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextTransferChargeID.Text = frmPOP.UserID;
                this.uTextTransferChargeName.Text = frmPOP.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region Grid

        private void uGridSPTransferH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);                


                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strTransferCode = e.Cell.Row.Cells["TransferCode"].Value.ToString();

                
                QRPBrowser brwChannel = new QRPBrowser();

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferH), "SPTransferH");
                QRPEQU.BL.EQUSPA.SPTransferH clsSPTransferH = new QRPEQU.BL.EQUSPA.SPTransferH();
                brwChannel.mfCredentials(clsSPTransferH);

                // Call Method
                DataTable dtSPTransferH = clsSPTransferH.mfReadEQUSPTransferH_Detail(strPlantCode,  strTransferCode, m_resSys.GetString("SYS_LANG"));


                for (int i = 0; i < dtSPTransferH.Rows.Count; i++)
                {
                    this.uComboPlant.Value = dtSPTransferH.Rows[i]["PlantCode"].ToString();
                    this.uTextTransferCode.Text = dtSPTransferH.Rows[i]["TransferCode"].ToString();
                    this.uDateTransferDate.Value = dtSPTransferH.Rows[i]["TransferDate"].ToString();
                    this.uTextTransferChargeID.Text = dtSPTransferH.Rows[i]["TransferChargeID"].ToString();
                    this.uTextTransferChargeName.Text = dtSPTransferH.Rows[i]["TransferChargeName"].ToString();
                    this.uTextEtcDesc.Text = dtSPTransferH.Rows[i]["EtcDesc"].ToString();
                }


                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);

                // Call Method
                DataTable dtSPTransferD = clsSPTransferD.mfReadEQUSPTransferD(strPlantCode, strTransferCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridSPTransferD.DataSource = dtSPTransferD;
                uGridSPTransferD.DataBind();

                if (dtSPTransferD.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSPTransferD, 0);
                }
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }      
  
        private void uGridSPTransferD_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                //brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                //QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                //brwChnnel.mfCredentials(clsEquip);

                //Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;

                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                if (grd.mfCheckCellDataInRow(this.uGridSPTransferD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                string strColumn = e.Cell.Column.Key;

                if (strColumn == "TransferSPName")
                {
                    // SparePartCode 열에서 엔터키를 칠 경우 SparePartName 및 현 재고 수량을 가지고 오는 이벤트 

                    string strSparePartCode = e.Cell.Row.Cells["TransferSPCode"].Value.ToString();
                    string strSPInventoryCode = e.Cell.Row.Cells["TransferSPInventoryCode"].Value.ToString();
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    if (e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Cells["Qty"].Value = string.Empty;
                        e.Cell.Row.Cells["TransferSPCode"].Value = string.Empty;
                        //e.Cell.Row.Cells["TransferSPName"].Value = "";
                        return;
                    }


                    // 필수입력사항 확인
                    if (strSPInventoryCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M001163", Infragistics.Win.HAlign.Center);

                        // Focus
                        return;
                    }


                }

                #region 교체수량

                if (strColumn.Equals("TransferQty"))
                {
                    if (!e.Cell.Value.Equals(0) && !e.Cell.Row.Cells["TransferSPCode"].Value.ToString().Equals(string.Empty))
                    {
                        //교체수량이 기존수량보다 많거나 사용량이 재고량보다 많을 경우 메세지 박스
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value) && !e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString().Equals(string.Empty)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000302", "M000875", Infragistics.Win.HAlign.Right);

                            //재고량이 기존수량보다 크고 기존수량이 있는 경우
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value)
                                && !e.Cell.Row.Cells["InputQty"].Value.ToString().Equals(string.Empty))
                            {
                                e.Cell.Value = e.Cell.Row.Cells["InputQty"].Value;
                            }
                            else
                            {
                                e.Cell.Value = e.Cell.Row.Cells["Qty"].Value;
                            }
                            return;
                        }

                        #region 중복구성품재고구분

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;


                        for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                        {
                            if (this.uGridSPTransferD.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridSPTransferD.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridSPTransferD.Rows[i].Hidden.Equals(false))
                            {
                                //if (!i.Equals(e.Cell.Row.Index)
                                //    && e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value)
                                //    && e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value))
                                //{
                                if (e.Cell.Row.Cells["TransferSPCode"].Value.Equals(this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value) &&
                                    e.Cell.Row.Cells["TransferSPInventoryCode"].Value.Equals(this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridSPTransferD.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value.ToString();
                            string strLang = m_resSys.GetString("SYS_LANG");

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000617",strLang)
                                                   , msg.GetMessge_Text("M000892", strLang) + strQty + msg.GetMessge_Text("M000010", strLang), Infragistics.Win.HAlign.Right);

                            //e.Cell.Row.Cells["ChgSparePartCode"].Value = string.Empty;
                            //e.Cell.Row.Cells["ChgSparePartName"].Value = string.Empty;
                            //e.Cell.Row.Cells["Qty"].Value = 0;
                            //e.Cell.Row.Cells["Qty"].Tag = 0;
                            e.Cell.Value = 0;
                            //e.Cell.Row.Cells["UnitCode"].Value = string.Empty;

                            return;
                        }


                        #endregion

                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridSPTransferD_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChnnel.mfCredentials(clsEquip);

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;


                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;
                string strPlantCode = uComboPlant.Value.ToString();

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                // 교체설비명 가져오기//
                string strChgEquipCode = uGrid.ActiveCell.Row.Cells["ChgEquipCode"].Text;

                

                if (strColumn == "ChgEquipCode")
                {
                    if (strChgEquipCode == "")
                        return;

                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strChgEquipCode, m_resSys.GetString("SYS_LANG"));
                    uGrid.ActiveCell.Row.Cells["ChgEquipName"].Value = dtEquip.Rows[0]["EquipName"].ToString();

                    // 설비에 해당하는 SparePart 코드 가져오기
                    brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChnnel.mfCredentials(clsEquipSPBOM);
                    DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(strPlantCode, strChgEquipCode, m_resSys.GetString("SYS_LANG"));

                    //grd.mfSetGridCellValueList(this.uGridSPTransferD, e.Cell.Row.Index, "ChgSparePartCode", "", "", dtEquipSPBOM);

                    string strValue = "SparePartCode,SparePartName,InputQty,Spec,Maker,UnitName";
                    string strText = "구성품코드,구성품명,Qty,Spec,Maker,단위";

                    //grd.mfSetGridColumnValueGridList(this.uGridSPTransferD, 0, "ChgSparePartCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                    //                                , "SparePartCode", "SparePartName", dtEquipSPBOM);
                    grd.mfSetGridCellValueGridList(this.uGridSPTransferD, 0,e.Cell.Row.Index, "ChgSparePartCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "SparePartCode", "SparePartName", dtEquipSPBOM);
                    // SparePart 선택시 SparePart 기준수량 가져오기
                    //this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    
                }
                

                
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridSPTransferD_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPBrowser brwChannel = new QRPBrowser();

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs uCell = sender as Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs;

                if (e.KeyCode == Keys.Enter)
                {
                    // SparePartCode 열에서 엔터키를 칠 경우 SparePartName 및 현 재고 수량을 가지고 오는 이벤트 
                    if (uGrid.ActiveCell.Column.Key == "TransferSPCode")
                    {
                        string strSparePartCode = uGrid.ActiveCell.Row.Cells["TransferSPCode"].Text;
                        string strSPInventoryCode = uGrid.ActiveCell.Row.Cells["TransferSPInventoryCode"].Value.ToString();
                        string strPlantCode = this.uComboPlant.Value.ToString();

                        // 필수입력사항 확인
                        if (strPlantCode == "")
                        {
                            DialogResult dir = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                            // Focus
                            return;
                        }

                        // 필수입력사항 확인
                        if (strSPInventoryCode == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", "M001476", Infragistics.Win.HAlign.Center);

                            // Focus
                            return;
                        }

                        // 필수입력사항 확인
                        if (strSparePartCode == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", "M000120", Infragistics.Win.HAlign.Center);

                            // Focus
                            return;
                        }


                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, strSPInventoryCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));

                        if (dtSPStock.Rows.Count > 0)
                        {
                            uGrid.ActiveCell.Row.Cells["TransferSPName"].Value = dtSPStock.Rows[0]["SparePartName"].ToString();
                            uGrid.ActiveCell.Row.Cells["Qty"].Value = Convert.ToInt32(dtSPStock.Rows[0]["Qty"].ToString());
                            uGrid.ActiveCell.Row.Cells["Spec"].Value = dtSPStock.Rows[0]["Spec"].ToString();
                            uGrid.ActiveCell.Row.Cells["Maker"].Value = dtSPStock.Rows[0]["Maker"].ToString();

                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        ,"M001264", "M000962", "M001477",
                                        Infragistics.Win.HAlign.Right);
                            //uGrid.ActiveCell.Row.Cells["Qty"].Value = "0";
                            uGrid.ActiveCell.Row.Cells["TransferSPName"].Value = "";
                            uGrid.ActiveCell.Row.Cells["TransferSPCode"].Value = "";
                            uGrid.ActiveCell.Row.Cells["Spec"].Value = string.Empty;
                            uGrid.ActiveCell.Row.Cells["Maker"].Value = string.Empty;
                        }



                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridSPTransferD_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");
                //string strPlantCode = this.uTextPlantCode.Text;
                //string strSPInventoryCode = e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString();

                //if (strPlantCode == "")
                //    return;

                //if (strSPInventoryCode == "")
                //    return;

                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (e.Cell.Row.Cells["TransferSPInventoryCode"].Value.ToString() == "")
                {
                     DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[e.Cell.Row.Index].RowSelectorNumber + msg.GetMessge_Text("M000533", strLang), Infragistics.Win.HAlign.Center);

                    // Focus Cell
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["TransferSPInventoryCode"];
                    this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    return;
                }

                frmPOP0008 frm = new frmPOP0008();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                if (e.Cell.Row.Cells["UnitCode"].Tag != null && !frm.UnitCode.Equals(string.Empty) && !e.Cell.Row.Cells["UnitCode"].Tag.Equals(frm.UnitCode))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000360", "M000306", Infragistics.Win.HAlign.Right);
                    e.Cell.Row.Cells["TransferSPCode"].Value = string.Empty;
                    e.Cell.Row.Cells["TransferSPName"].Value = string.Empty;
                    e.Cell.Row.Cells["Qty"].Value = 0;
                    e.Cell.Row.Cells["Spec"].Value = string.Empty;
                    return;
                }
                e.Cell.Row.Cells["TransferSPCode"].Value = frm.SparePartCode;
                e.Cell.Row.Cells["TransferSPName"].Value = frm.SparePartName;

                //if (e.Cell.Row.Cells["ChgSparePartCode"].Value != "")
                //{
                //    e.Cell.Row.Cells["ChgSparePartName"].Value = frm.SparePartName;
                //}
        
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridSPTransferD_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //선택한 리스트의 Value값과 Text값 저장
                string strValue = "";
                string strText = "";

                //ColumValueList 인지 CellValueList인지에 따라 저장
                if (e.Cell.Column.ValueList != null)
                {
                    strValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strText = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strValue = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strText = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }

                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString());
                

                ///System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;


                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;
                string strPlantCode = uComboPlant.Value.ToString();

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                // 교체설비명 가져오기//
                string strChgEquipCode = uGrid.ActiveCell.Row.Cells["ChgEquipCode"].Text;

                

                if (strColumn == "ChgSparePartCode")
                {
                    string strChgSparePartCode = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    //string strChgSparePartCode = e.Cell.Value.ToString();
                    if (!strChgSparePartCode.Equals(string.Empty))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                        QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                        brwChannel.mfCredentials(clsEquipSPBOM);
                        DataTable dtEquipSPBOMQty = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strChgEquipCode, strChgSparePartCode, m_resSys.GetString("SYS_LANG"));

                        int intInputQty = Convert.ToInt32(dtEquipSPBOMQty.Rows[0]["InputQty"].ToString());
                        int intChgStandbyQty = Convert.ToInt32(dtEquipSPBOMQty.Rows[0]["ChgStandbyQty"].ToString());

                        uGrid.ActiveCell.Row.Cells["InputQty"].Value = intInputQty - intChgStandbyQty;
                        e.Cell.Row.Cells["UnitCode"].Tag = dtEquipSPBOMQty.Rows[0]["UnitCode"];
                        e.Cell.Row.Cells["ChgSpec"].Value = dtEquipSPBOMQty.Rows[0]["Spec"];
                        e.Cell.Row.Cells["ChgMaker"].Value = dtEquipSPBOMQty.Rows[0]["Maker"];

                        for (int i = 0; i < uGridSPTransferD.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString().Equals(dtEquipSPBOMQty.Rows[0]["SparePartCode"].ToString()) &&
                                uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString().Equals(dtEquipSPBOMQty.Rows[0]["EquipCode"].ToString()) &&
                                !dtEquipSPBOMQty.Rows[0]["SparePartCode"].ToString().Equals(string.Empty))
                            {
                                //WinMessageBox msg = new WinMessageBox();
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000854", Infragistics.Win.HAlign.Right);

                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["ChgSparePartCode"].Value = "";
                                //uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["ChgSparePartName"].Value = string.Empty;

                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = 0;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;
                                //uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Tag = 0;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Value = 0;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["Qty"].Tag = 0;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["Qty"].Value = 0;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["TransferSPCode"].Value = string.Empty;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["TransferSPName"].Value = string.Empty;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["ChgSpec"].Value = string.Empty;
                                uGridSPTransferD.Rows[e.Cell.Row.Index].Cells["ChgMaker"].Value = string.Empty;
                                e.Cell.Row.Cells["TransferQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                return;
                            }
                        }

                        if (!e.Cell.Row.Cells["TransferSPCode"].Value.ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["Qty"].Value = 0;
                            e.Cell.Row.Cells["TransferSPCode"].Value = string.Empty;
                            e.Cell.Row.Cells["TransferSPName"].Value = string.Empty;
                            e.Cell.Row.Cells["Spec"].Value = string.Empty;
                            e.Cell.Row.Cells["Maker"].Value = string.Empty;
                            e.Cell.Row.Cells["TransferQty"].Value = 0;
                        }
                    }
                }

                #region SP창고

                if (strColumn.Equals("TransferSPInventoryCode"))
                {
                    //SP창고가 공백이 아닌경우
                    if (!strValue.Equals(string.Empty))
                    {

                        SearchSPStock(strPlantCode, strValue, e.Cell.Row.Index);

                        if (!e.Cell.Row.Cells["TransferSPCode"].Value.ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["Qty"].Value = 0;
                            e.Cell.Row.Cells["TransferQty"].Value = 0;
                            e.Cell.Row.Cells["TransferSPCode"].Value = string.Empty;
                            e.Cell.Row.Cells["TransferSPName"].Value = string.Empty;
                            e.Cell.Row.Cells["Spec"].Value = string.Empty;
                            e.Cell.Row.Cells["Maker"].Value = string.Empty;
                        }
                    }
                }

                #endregion

                #region 사용구성품

                if (strColumn.Equals("TransferSPName"))
                {
                    if (!strValue.Equals(string.Empty))
                    {
                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //SP현재고 조회
                        DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, e.Cell.Row.Cells["TransferSPInventoryCode"].Value.ToString(), strValue, m_resSys.GetString("SYS_LANG"));

                        if (dtSPStock.Rows.Count > 0)
                        {
                            //기존구성품코드가 있는 상태에서 선택한 SP의 단위와 기존SP의 단위가 틀리면 리턴
                            if (e.Cell.Row.Cells["UnitCode"].Tag != null && !e.Cell.Row.Cells["UnitCode"].Tag.ToString().Equals(string.Empty) && !e.Cell.Row.Cells["UnitCode"].Tag.Equals(dtSPStock.Rows[0]["UnitCode"]))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000360", "M000661", Infragistics.Win.HAlign.Right);
                                return;
                            }

                            //선택 구성품이 바뀔 때 교체수량이 있을 경우 초기화
                            if (!e.Cell.Row.Cells["TransferQty"].Value.ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["TransferQty"].Value = 0;
                            }

                            e.Cell.Row.Cells["TransferSPCode"].Value = dtSPStock.Rows[0]["SparePartCode"];

                            e.Cell.Row.Cells["Qty"].Value = dtSPStock.Rows[0]["Qty"];
                            e.Cell.Row.Cells["Qty"].Tag = dtSPStock.Rows[0]["Qty"];

                            e.Cell.Row.Cells["UnitCode"].Value = dtSPStock.Rows[0]["UnitCode"];

                            e.Cell.Row.Cells["Spec"].Value = dtSPStock.Rows[0]["Spec"];
                            e.Cell.Row.Cells["Maker"].Value = dtSPStock.Rows[0]["Maker"];

                        }
                       
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// 그리드안 창고재고 콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strInventoryCode">창고</param>
        /// <param name="intIndex">줄번호</param>
        /// <param name="brwChannel">QRPBrowser</param>
        private void SearchSPStock(string strPlantCode, string strInventoryCode, int intIndex)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //SP현재고 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                string strDropDownName = "";
                string strLang = m_resSys.GetString("SYS_LANG");

                if (strLang.Equals("KOR"))
                    strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";
                else if (strLang.Equals("CHN"))
                    strDropDownName = "使用构成品条码,使用构成品名,库存量,单位,Spec,Maker";
                else 
                    strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";



                WinGrid wGrid = new WinGrid();

                //GridList 그리드에 삽입
                wGrid.mfSetGridCellValueGridList(this.uGridSPTransferD, 0, intIndex, "TransferSPName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                    , "SparePartCode", "SparePartName", dtChgSP);

                if (dtChgSP.Rows.Count == 0)
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000121", "M001252", Infragistics.Win.HAlign.Right);
                    
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void GridComboList(string strPlantCode, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChnnel.mfCredentials(clsEquip);

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                //매서드호출
                DataTable dtEquipList = clsEquip.mfReadEquipListCombo(strPlantCode, "", "", "", "", m_resSys.GetString("SYS_LANG"));



                if (dtEquipList.Rows.Count != 0)
                {

                    WinGrid grd = new WinGrid();

                    string strDropDownGridHKey = "EquipCode,EquipName,SuperEquipCode,ModelName,SerialNo,EquipTypeName,EquipGroupName,VendorName";
                    string strDropDownGridHName = "";
                    string strLang = m_resSys.GetString("SYS_LANG");

                    if (strLang.Equals("KOR"))
                        strDropDownGridHName = "설비코드,설비명,Super설비,모델,SerialNo,설비유형,설비그룹명,Vendor";
                    else if (strLang.Equals("CHN"))
                        strDropDownGridHName = "设备号码,设备名,Super设备,型号,SerialNo,设备类型,设备组名,Vendor";
                    else
                        strDropDownGridHName = "설비코드,설비명,Super설비,모델,SerialNo,설비유형,설비그룹명,Vendor";

                    grd.mfSetGridColumnValueGridList(this.uGridSPTransferD, intIndex, "ChgEquipCode", Infragistics.Win.ValueListDisplayStyle.DataValue
                        , strDropDownGridHKey, strDropDownGridHName, "EquipCode", "EquipCode", dtEquipList);

                }
                else
                {
                    ///* 검색결과 Record수 = 0이면 메시지 띄움 */
                    //System.Windows.Forms.DialogResult result;
                    //result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                            "처리결과", "처리결과", "검색처리결과가 없습니다.", Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void GridCombo(string strPlantCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();



                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);
                DataTable dtSPInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridSPTransferD, 0, "TransferSPInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtSPInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }

}

