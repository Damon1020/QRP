﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0026.cs                                         */
/* 프로그램명   : PM체크결과조회                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-01-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*              : ~~~~~ 추가 ()                                         */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;
using Microsoft.VisualBasic;
namespace QRPEQU.UI
{
    public partial class frmEQUZ0026 : Form , IToolbar
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0026()
        {
            InitializeComponent();
        }

        private void frmEQUZ0026_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화

            ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

            brwChannel.mfActiveToolBar(this.MdiParent, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmEQUZ0026_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0026_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGrid();
            InitComboBox();

            this.uOptionSelect.Value = "E";

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("PM체크 결과조회", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region 그리드기본설정

                grd.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                   false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                   Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                   Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfInitGeneralGrid(this.uGridUser, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                   false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                   Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                   Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                #endregion

                #region 컬럼설정

                //루프를 돌며 컬럼20개 생성
                for (int i = 0; i < 15; i++)
                {
                    string strKey = "";
                    string strKeyName = "";
                    string strValue = "";
                    string strText = "";
                    if (i < 9)
                    {
                        strKey = "E0" + (i + 1).ToString();
                        strValue = "U0" + (i + 1).ToString();
                    }
                    else
                    {
                        strKey = "E" + (i + 1).ToString();
                        strValue = "U" + (i + 1).ToString();
                    }
                    grd.mfSetGridColumn(this.uGridEquip, 0, strKey, strKeyName, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    grd.mfSetGridColumn(this.uGridUser, 0, strValue, strText, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                }

                this.uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridUser.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridUser.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridEquip.DisplayLayout.Override.CellAppearance.BackColor = Color.Gold;
                this.uGridUser.DisplayLayout.Override.CellAppearance.BackColor = Color.Gold;

                //해당그리드의 선택한 줄이나 셀의 속성을 비활성화한다.
                this.uGridEquip.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridEquip.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;

                this.uGridUser.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridUser.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDate, "기간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUesr, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelAll, "전체건수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelWait, "미완료 건수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel, "실시율", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelOk, "완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelWaitEquip, "미완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDefalut, "계획미등록", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                //기본값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");


                #region Date

                //검색조건의 기간 콤보 설정
                ArrayList MonthKey = new ArrayList();
                ArrayList MonthValue = new ArrayList();
                ArrayList YearKey = new ArrayList();
                ArrayList YearValue = new ArrayList();
                int intCnt = 2011;

                for (int i = 1; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        MonthKey.Add("0" + i);
                        MonthValue.Add(i.ToString() + "月");
                    }
                    else
                    {
                        MonthKey.Add(i);
                        MonthValue.Add(i.ToString() + "月");
                    }
                    if (intCnt < 2021)
                    {
                        YearKey.Add(intCnt);
                        YearValue.Add(intCnt.ToString() + "年");
                        intCnt++;
                    }
                }

                wCombo.mfSetComboEditor(this.uComboSearchYear, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", YearKey, YearValue);

                wCombo.mfSetComboEditor(this.uComboSearchFromMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", MonthKey, MonthValue);

               
                #endregion

                // Date ComboBox 기본값 설정
                DateTime DateNow = DateTime.Now;
                this.uComboSearchYear.Value = DateNow.Year.ToString();
                this.uComboSearchFromMonth.Value = DateNow.Month.ToString("D2");
                this.uComboSearchFromDay.Value = DateNow.Day.ToString("D2");
                this.uComboSearchToDay.Value = DateNow.Day.ToString("D2");


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            mfSearchData(this.uOptionSelect.Value.ToString());
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGridEquip.Rows.Count == 0 && this.uGridUser.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                if(this.uGridEquip.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquip);

                if (this.uGridUser.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridUser);
                /////////////
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfPrint()
        {
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        #endregion

        #region Event

        #region Text

        //정비사정보가 변경시 이름클리어
        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextUserName.Text.Equals(string.Empty))
                    this.uTextUserName.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //정비사정보입력조회
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //출고요청자ID, 공장 저장
                string strUserID = this.uTextUserID.Text;
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //키 데이터가 엔터키 고 내용이 있는 경우 조회한다.
                if (e.KeyData.Equals(Keys.Enter) && !strUserID.Equals(string.Empty))
                {
                    //공장정보조회
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //유저정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //유저정보조회매서드 호출
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    //정보가 있을경우 등록 없을 경우 명초기화
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                             , "M001264", "M001116", "M000883", Infragistics.Win.HAlign.Right);

                        if (!this.uTextUserName.Text.Equals(string.Empty))
                            this.uTextUserName.Clear();

                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //정비사텍스트 유저정보팝업창조회
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //공장정보조회
                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Combo

        //공장 선택 시 Station 콤보박스 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitData();

                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {

                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    //콤보 아이템 클리어
                    //this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();

                    //언어저장
                    string strLang = m_resSys.GetString("SYS_LANG");


                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택",
                        "StationCode", "StationName", dtStation);

                    

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station 선택 시위치,설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchEquipLoc.Items.Clear();
                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치변경시 설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류 콤보선택시 설비 중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비중분류 선택시 설비 그룹이변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검색년도를 선택 시 해당월의 일이 콤보에 박힌다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchYear_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboSearchYear.Value != null
                    && !this.uComboSearchYear.Value.ToString().Equals(string.Empty)
                    && Information.IsNumeric(this.uComboSearchYear.Value)
                    && this.uComboSearchFromMonth.Value != null
                    && !this.uComboSearchFromMonth.Value.ToString().Equals(string.Empty)
                    && Information.IsNumeric(this.uComboSearchFromMonth.Value))
                {
                    DataTable dtDay = mfSearchDay(Convert.ToInt32(this.uComboSearchYear.Value), Convert.ToInt32(this.uComboSearchFromMonth.Value));

                    WinComboEditor Com = new WinComboEditor();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    this.uComboSearchFromDay.Items.Clear();
                    this.uComboSearchToDay.Items.Clear();
                    Com.mfSetComboEditor(this.uComboSearchFromDay, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Key", "Value", dtDay);

                    Com.mfSetComboEditor(this.uComboSearchToDay, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Key", "Value", dtDay);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 월이 변경될 경우 해당월의 일이 콤보에 박힌다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchFromMonth_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboSearchYear.Value != null
                    && !this.uComboSearchYear.Value.ToString().Equals(string.Empty)
                    && Information.IsNumeric(this.uComboSearchYear.Value)
                    && this.uComboSearchFromMonth.Value != null
                    && !this.uComboSearchFromMonth.Value.ToString().Equals(string.Empty)
                    && Information.IsNumeric(this.uComboSearchFromMonth.Value))
                {
                    DataTable dtDay = mfSearchDay(Convert.ToInt32(this.uComboSearchYear.Value), Convert.ToInt32(this.uComboSearchFromMonth.Value));

                    WinComboEditor Com = new WinComboEditor();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    this.uComboSearchFromDay.Items.Clear();
                    this.uComboSearchToDay.Items.Clear();

                    Com.mfSetComboEditor(this.uComboSearchFromDay, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Key", "Value", dtDay);

                    Com.mfSetComboEditor(this.uComboSearchToDay, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Key", "Value", dtDay);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Grid

        /// <summary>
        /// 설비별검색으로 나온 설비를 더블클릭시 PM상세정보가나오는 팝업창을 띄운다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquip_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (!e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Appearance.BackColor.Equals(Color.Gold))
                {
                    mfPMDetailPOP("E", e.Cell.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 정비사별검색을 나온 정비사에대한 PM상세정보를 보여주는 팝업창을 띄운다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridUser_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            if (!e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Appearance.BackColor.Equals(Color.Gold))
            {
                mfPMDetailPOP("U", e.Cell.Value.ToString());
            }
        }
        #endregion


        #endregion

        #region Search

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();
                
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 해당 년월에 해당하는 일수 구하기
        /// </summary>
        /// <param name="intYear">년도</param>
        /// <param name="intMonth">월</param>
        /// <returns></returns>
        private DataTable mfSearchDay(int intYear, int intMonth)
        {
            DataTable dtDay = new DataTable();
            try
            {
                dtDay.Columns.Add("Key", typeof(string));
                dtDay.Columns.Add("Value", typeof(string));
                
                int intMax = 0;

                //월이 4,6,9,11 인경우 날짜는 30일까지
                if (intMonth.Equals(2)
                    || intMonth.Equals(4)
                    || intMonth.Equals(6)
                    || intMonth.Equals(9)
                    || intMonth.Equals(11))
                {
                    //월이 2월인경우 날짜는 28일 혹은 29일까지 있다.
                    if (intMonth.Equals(2))
                    {
                        // 년도 / 4 하여 몫이 있으면 28일 
                        if (!(intYear % 4).Equals(0))
                        {
                            intMax = 28;
                        }
                        //없으면 29일
                        else
                        {
                            intMax = 29;
                        }
                    }
                    else
                    {
                        intMax = 30;
                    }
                }
                else
                {
                    intMax = 31;
                }
                DataRow drDay;
                for (int i = 1; i <= intMax; i++)
                {
                    drDay = dtDay.NewRow();
                    if (i < 10)
                    {
                        drDay["Key"] = "0"+i.ToString();
                        drDay["Value"] = "0" + i.ToString()+"日";
                    }
                    else
                    {
                        drDay["Key"] = i;
                        drDay["Value"] = i.ToString() + "日";
                    }
                    dtDay.Rows.Add(drDay);
                }


                return dtDay;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDay;
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 정보를 검색한다.
        /// </summary>
        /// <param name="strSearch">E:설비별검색,U:정비사별검색</param>
        private void mfSearchData(string strSearch)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                InitData();

                #region 필수입력확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboSearchYear.Value == null
                    || this.uComboSearchYear.Value.ToString().Equals(string.Empty)
                    || !Information.IsNumeric(this.uComboSearchYear.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000208", Infragistics.Win.HAlign.Right);

                    this.uComboSearchYear.DropDown();
                    return;
                }

                if (this.uComboSearchFromMonth.Value == null
                   || this.uComboSearchFromMonth.Value.ToString().Equals(string.Empty)
                   || !Information.IsNumeric(this.uComboSearchFromMonth.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000201", Infragistics.Win.HAlign.Right);

                    this.uComboSearchFromMonth.DropDown();
                    return;
                }
                if (this.uComboSearchFromDay.Value.ToString().Equals(string.Empty) || !Information.IsNumeric(this.uComboSearchFromDay.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000209", Infragistics.Win.HAlign.Right);

                    this.uComboSearchFromDay.DropDown();
                    return;
                }
                if (this.uComboSearchToDay.Value.ToString().Equals(string.Empty) || !Information.IsNumeric(this.uComboSearchToDay.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000218", Infragistics.Win.HAlign.Right);

                    this.uComboSearchToDay.DropDown();
                    return;
                }
                if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000128", Infragistics.Win.HAlign.Right);

                    this.uComboSearchStation.DropDown();
                    return;
                }
                

                #endregion

                //검색조건 정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strYear = this.uComboSearchYear.Value.ToString();
                string strMonth = this.uComboSearchFromMonth.Value.ToString();
                //검색시작일
                string strFromDate = strYear + "-" + strMonth + "-" + this.uComboSearchFromDay.Value.ToString();
                //검색종료일
                string strToDate = strYear + "-" + strMonth + "-" + this.uComboSearchToDay.Value.ToString();

                //string strFromMonth = ;

                //string strToMonth = this.uComboSearchToMonth.Value.ToString();


                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboSearchEquipLargeType.Value.ToString();
                string strEquipGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strUserID = "";
                if (!this.uTextUserID.Text.Equals(string.Empty) && !this.uTextUserName.Text.Equals(string.Empty))
                    strUserID = this.uTextUserID.Text;


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                brwChannel.mfCredentials(clsPMPlanD);

                DataTable dtTotalInfo = new DataTable();
                DataTable dtGridInfo = new DataTable();
                DataTable dtPMChk = new DataTable();
                int intCnt =0;
                int intAll = 0;
                //dtTotalInfo = clsPMPlanD.mfReadPMPlanD_TotalCount
                //    (strPlantCode
                //    , strStationCode
                //    , strEquipLocCode
                //    , strProcessGroup
                //    , strEquipLargeTypeCode
                //    , strEquipGroupCode
                //    , strUserID
                //    , strYear
                //    , strFromDate
                //    , strToDate);

                //if (dtTotalInfo.Rows.Count > 0)
                //{
                //    this.uTextAll.Text = dtTotalInfo.Rows[0]["TotalCount"].ToString();
                //    this.uTextWait.Text = dtTotalInfo.Rows[0]["PMNotResultCount"].ToString();
                //    this.uText.Text = dtTotalInfo.Rows[0]["PMResultRate"].ToString() + "%";
                    
                //}


                #region 설비별 검색
                if (strSearch.Equals("E"))  // 설비별검색
                {
                    //정비사그리드 비활성화
                    if (this.uGridUser.Visible)
                        this.uGridUser.Visible = false;

                    //설비별 검색시 설비그리드 활성화
                    if (this.uGridEquip.Visible.Equals(false))
                        this.uGridEquip.Visible = true;



                    dtGridInfo = clsPMPlanD.mfReadPMPlanD_Equip(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strEquipGroupCode, strUserID);


                    if (dtGridInfo.Rows.Count > 0)
                    {
                        this.uGridEquip.DataSource = dtGridInfo;
                        this.uGridEquip.DataBind();

                        dtPMChk = clsPMPlanD.mfReadPMPlanD_PMChk(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strEquipGroupCode, strUserID, strYear, strFromDate, strToDate, strSearch);

                        if (dtPMChk.Rows.Count > 0)
                        {
                            

                            //PM계획정보가 있는데 계획이남아있다면 Salmon , 등록이 다되어있다면 White
                            for (int i = 0; i < dtPMChk.Rows.Count; i++)
                            {
                                for (int j = 0; j < this.uGridEquip.Rows.Count; j++)
                                {
                                    for (int k = 0; k < this.uGridEquip.DisplayLayout.Bands[0].Columns.Count; k++)
                                    {
                                        if(this.uGridEquip.Rows[j].Cells[k].Value.ToString().Equals(string.Empty))
                                        {
                                            this.uGridEquip.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                                            //break;
                                        }
                                        else
                                        {
                                            if (this.uGridEquip.Rows[j].Cells[k].Value.ToString().Equals(dtPMChk.Rows[i]["EquipCode"].ToString()))
                                            {
                                                if (dtPMChk.Rows[i]["NPMCount"].ToString().Equals("0"))
                                                {
                                                    this.uGridEquip.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                                                    intCnt++;
                                                }
                                                else
                                                    this.uGridEquip.Rows[j].Cells[k].Appearance.BackColor = Color.Salmon;
                                                

                                                intAll++;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region 정비사별 검색
                if(strSearch.Equals("U"))    // 정비사별 검색
                {
                    //설비그리드 비활성화
                    if (this.uGridEquip.Visible)
                        this.uGridEquip.Visible = false;

                    //정비사별 검색인 경우 정비사 그리드 활성화 처리
                    if (this.uGridUser.Visible.Equals(false))
                        this.uGridUser.Visible = true;

                    dtGridInfo = clsPMPlanD.mfReadPMPlanD_Worker(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strEquipGroupCode, strUserID);


                    if (dtGridInfo.Rows.Count > 0)
                    {
                        this.uGridUser.DataSource = dtGridInfo;
                        this.uGridUser.DataBind();

                        dtPMChk = clsPMPlanD.mfReadPMPlanD_PMChk(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strEquipGroupCode, strUserID, strYear, strFromDate, strToDate, strSearch);

                        if (dtPMChk.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtPMChk.Rows.Count; i++)
                            {
                                for (int j = 0; j < this.uGridUser.Rows.Count; j++)
                                {
                                    for (int k = 0; k < this.uGridUser.DisplayLayout.Bands[0].Columns.Count; k++)
                                    {
                                        if (this.uGridUser.Rows[j].Cells[k].Value.ToString().Equals(string.Empty))
                                        {
                                            this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                                            
                                        }
                                        else
                                        {
                                           
                                            if (this.uGridUser.Rows[j].Cells[k].Value.ToString().Equals(dtPMChk.Rows[i]["UserName"].ToString()))
                                            {
                                                if (dtPMChk.Rows[i]["NPMWorker"].ToString().Equals("0"))
                                                {
                                                    this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                                                    intCnt++;
                                                }
                                                else
                                                    this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.Salmon;
                                                    
                                                intAll++;
                                                break;
                                            }

                                            
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }

               
                #endregion

                if (dtPMChk.Rows.Count > 0)
                {
                    this.uTextAll.Text = intAll.ToString();
                    this.uTextWait.Text = (intAll - intCnt).ToString();
                    double duto;
                    duto = Convert.ToDouble(intCnt) / Convert.ToDouble(intAll);
                    duto = 100 * duto;
                    this.uText.Text = duto.ToString("N3") + "%";
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtGridInfo.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);

                    
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        /// <summary>
        /// 그리드 줄삭제
        /// </summary>
        private void InitData()
        {
            if (this.uGridEquip.Rows.Count > 0)
            {
                this.uGridEquip.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquip.Rows.All);
                this.uGridEquip.DeleteSelectedRows(false);
            }
            if (this.uGridUser.Rows.Count > 0)
            {
                this.uGridUser.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridUser.Rows.All);
                this.uGridUser.DeleteSelectedRows(false);
            }

            this.uTextAll.Clear();
            this.uTextWait.Clear();
            this.uText.Clear();
        }

        /// <summary>
        /// PM상세정보 POPUP 창을 띄운다
        /// </summary>
        /// <param name="strTaget">구분자 (E:설비,U : 정비사)</param>
        /// <param name="strTagetCode">설비or정비사</param>
        private void mfPMDetailPOP(string strTaget, string strTagetCode)
        {
            try
            {
                #region 필수 입력사항
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboSearchYear.Value == null
                    || this.uComboSearchYear.Value.ToString().Equals(string.Empty)
                    || !Information.IsNumeric(this.uComboSearchYear.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000208", Infragistics.Win.HAlign.Right);

                    this.uComboSearchYear.DropDown();
                    return;
                }

                if (this.uComboSearchFromMonth.Value == null
                   || this.uComboSearchFromMonth.Value.ToString().Equals(string.Empty)
                   || !Information.IsNumeric(this.uComboSearchFromMonth.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000214", Infragistics.Win.HAlign.Right);

                    this.uComboSearchFromMonth.DropDown();
                    return;
                }

                if (this.uComboSearchFromDay.Value.ToString().Equals(string.Empty) || !Information.IsNumeric(this.uComboSearchFromDay.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000209", Infragistics.Win.HAlign.Right);

                    this.uComboSearchFromDay.DropDown();
                    return;
                }
                if (this.uComboSearchToDay.Value.ToString().Equals(string.Empty) || !Information.IsNumeric(this.uComboSearchToDay.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000218", Infragistics.Win.HAlign.Right);

                    this.uComboSearchToDay.DropDown();
                    return;
                }

                #endregion

                //검색조건 정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strYear = this.uComboSearchYear.Value.ToString();
                string strMonth = this.uComboSearchFromMonth.Value.ToString();

                //검색시작일
                string strFromDate = strYear + "-" + strMonth + "-" + this.uComboSearchFromDay.Value.ToString();
                //검색종료일
                string strToDate = strYear + "-" + strMonth + "-" + this.uComboSearchToDay.Value.ToString();

                DataTable dtPMList = new DataTable();
                dtPMList.Columns.Add("PlantCode");  //공장
                dtPMList.Columns.Add("PlanYear");   //계획년도
                dtPMList.Columns.Add("PMFromDate"); //검색시작일
                dtPMList.Columns.Add("PMToDate");   //검색종료일
                dtPMList.Columns.Add("PMTaget");    //대상
                dtPMList.Columns.Add("PMTagetCode");//대상코드


                DataRow drPM = dtPMList.NewRow();

                drPM["PlantCode"] = strPlantCode;
                drPM["PlanYear"] = strYear;
                drPM["PMFromDate"] = strFromDate;
                drPM["PMToDate"] = strToDate;
                drPM["PMTaget"] = strTaget;
                drPM["PMTagetCode"] = strTagetCode;

                dtPMList.Rows.Add(drPM);



                frmEQUZ0026_P1 frmPMD = new frmEQUZ0026_P1(dtPMList);
                frmPMD.ShowDialog();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

    }
}
