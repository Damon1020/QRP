﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0010.cs                                        */
/* 프로그램명   : 설비수리 출고등록                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-30                                            */
/* 수정이력     : 2022-09-06 : 이종민 수정                              */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.VisualBasic;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0010 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        //BL호출을위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        private string strRepairReqCode;
        private string strPlantCode;
        private string strRepairGICode;
        private string strFormName;

        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }
        public string RepairReqCode
        {
            get { return strRepairReqCode; }
            set { strRepairReqCode = value; }
        }
        private string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }
        private string RepairGICode
        {
            get { return strRepairGICode; }
            set { strRepairGICode = value; }
        }

        public frmEQUZ0010()
        {
            InitializeComponent();
        }

        #region Initialize
        private void frmEQUZ0010_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("설비수리 출고등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 초기화 Method
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            Initbutton();
            InitValue();
            if (FormName != null)
            {
                Point point = new Point(0, 0);
                this.Location = point;
            }    
            if (RepairReqCode != null && !RepairReqCode.Equals(string.Empty))
            {
                mfSelectRepairReq();
            }
        }

        private void frmEQUZ0010_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "수리정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "수리출고상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                // ExpandableGroupBox 상태 설정
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchDate, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);


                wLabel.mfSetLabel(this.uLabelReleaseCode, "수리출고코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                wLabel.mfSetLabel(this.uLabelRepairDay, "수리일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairUser, "수리자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairResult, "수리결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStopType, "설비정지유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairUnusual, "수리특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelReleaseRequestDay, "수리출고요청일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReleaseRequestUser, "수리출고요청자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUnusual, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReleaseComponent, "수리출고구성품", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelGIFlag, "등록여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                if (uComboGIFlag.Items.Count == 0)
                {
                    uComboGIFlag.Clear();
                    uComboGIFlag.Items.Add("F", "N");
                    uComboGIFlag.Items.Add("T", "Y");
                    uComboGIFlag.SelectedText = "N";
                }

                //this.uComboSearchArea.Items.Clear();
                //this.uComboSearchStation.Items.Clear();
                //this.uComboSearchEquipProcess.Items.Clear();
                //this.uComboSearchEquipGroup.Items.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region Header
                // 설비수리출고 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableMatRepairH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "RepairReqCode", "수리요청코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "RepairGICode", "수리출고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "RepairEndDate", "수리일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "RepairGIReqDate", "수리출고요청일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "RepairGIReqName", "수리출고요청자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatRepairH, 0, "ModelName", "ModelName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 빈줄추가
                //wGrid.mfAddRowGrid(this.uGridRepair, 0);
                #endregion

                #region Detail
                // 수리출고구성품 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableMatD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default, Infragistics.Win.UltraWinGrid.SelectType.Default
                    , Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // SET Column
                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "RepairGICode", "수리출고번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "RepairGIGubunCode", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "RepairGIGubunName", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "INN");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "CurDurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "CurDurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "CurLotNo", "LOTNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "ChgDurableInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "ChgDurableInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "ChgDurableMatCode", "교체구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "ChgDurableMatName", "교체구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "ChgLotNo", "LOTNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "CurQty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "ChgQty", "교체수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMatD, 0, "CancelFlag", "취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfAddRowGrid(this.uGridDurableMatD, 0);
                #endregion

                this.uGridDurableMatRepairH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMatRepairH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDurableMatD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMatD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDurableMatD.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridDurableMatD.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #region Grid DropDown Binding

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommon = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommon);

                DataTable dtCommon = clsCommon.mfReadCommonCode("C0016", m_resSys.GetString("SYS_LANG"));
                // 수리출고구분
                wGrid.mfSetGridColumnValueList(this.uGridDurableMatD, 0, "RepairGIGubunName", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCommon);

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                wGrid.mfSetGridColumnValueList(this.uGridDurableMatD, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);


                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Initbutton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //this.uDateSearchFromDate.Value = DateTime.Now;
                //this.uDateSearchToDate.Value = DateTime.Now;
                //this.uComboSearchPlant.Value = "";
                //this.uComboSearchEquipGroup.Value = "";
                //this.uComboSearchArea.Value = "";
                //this.uComboSearchStation.Value = "";
                //this.uComboSearchEquipProcess.Value = "";

                this.uTextRepairGICode.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                
                this.uDateRepairGIReqDate.Value = DateTime.Now;
                this.uTextRepairGIReqID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextRepairGIReqName.Text = m_resSys.GetString("SYS_USERNAME");

                RepairGICode = string.Empty;
                //RepairReqCode = string.Empty;

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Common Event
        public void mfSearch()
        {
            this.uGroupBoxContentsArea.Expanded = false;
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            try
            {
                #region Indispensable
                if (uComboSearchPlant.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uDateSearchFromDate.Value == null || this.uDateSearchFromDate.Value.ToString().Equals(string.Empty))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M000209", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }
                if (this.uDateSearchToDate.Value == null || this.uDateSearchToDate.Value.ToString().Equals(string.Empty))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M000218", Infragistics.Win.HAlign.Right);

                    this.uDateSearchToDate.DropDown();
                    return;
                }

                if (this.uDateSearchFromDate.DateTime.Date > this.uDateSearchToDate.DateTime.Date)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M000211", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }
                #endregion

                #region Var
                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(uDateSearchFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(uDateSearchToDate.Value).ToString("yyyy-MM-dd");
                string strStation = uComboSearchStation.Value.ToString();
                string strEquipLocCode = uComboEquipLoc.Value.ToString();
                string strProcessGroup = uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = uComboEquipLargeType.Value.ToString();
                string strEquipGroupCode = uComboSearchEquipGroup.Value.ToString();

                //string strArea = uComboSearchArea.Value.ToString();
                //string strEquipProc = uComboSearchEquipProcess.Value.ToString();
                //string strGIFlag = uComboGIFlag.Value.ToString();

                #endregion
                WinGrid grd = new WinGrid();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                #region BL
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsEquip = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadDurableMatRepairGIH_Repair(strPlantCode,strFromDate,strToDate,strStation,strEquipLocCode,strProcessGroup,strEquipLargeTypeCode,
                                                                                strEquipGroupCode
                                                                                , m_resSys.GetString("SYS_LANG"));

                //DataTable dtPlan = clsPMPlan.mfReadPMPlan(strPlantCode, strYear, strGroupCode);
                /////////////
                ///////////// 
                uGridDurableMatRepairH.Refresh();
                uGridDurableMatRepairH.DataSource = dtEquip;
                uGridDurableMatRepairH.DataBind();
                #endregion

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtEquip.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                    grd.mfSetAutoResizeColWidth(this.uGridDurableMatRepairH, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            titleArea.Focus();

            #region Indispensable
            if (!uGroupBoxContentsArea.Expanded)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M001046", Infragistics.Win.HAlign.Right);
                
                return;
            }

            if (RepairReqCode == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M001046", Infragistics.Win.HAlign.Right);
                
                return;
            }

            if (uTextRepairGIReqID.Text == string.Empty || uTextRepairGIReqName.Text == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M001168", Infragistics.Win.HAlign.Right);
                uTextRepairGIReqID.Focus();
                return;
            }
            if (uDateRepairGIReqDate.Value.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M001169", Infragistics.Win.HAlign.Right);
                uDateRepairGIReqDate.DropDown();
                return;
            }

            //if (uGridDurableMatD.Rows.Count == 0)
            //{
            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //                , "확인창", "확인창", "저장할 수리출고구성 정보가 없습니다.", Infragistics.Win.HAlign.Right);
            //    uGridDurableMatD.Focus();
            //    return;
            //}


            try
            {
                string strEquipCode = uTextEquipCode.Text;
                string strLang = m_resSys.GetString("SYS_LANG");
                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);


                if (this.uGridDurableMatD.Rows.Count > 0)
                {
                    this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[0].Cells[0];
                    for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
                    {
                        if (!uGridDurableMatD.Rows[i].Hidden)
                        {
                            //조회된 정보에서 수리취소가 false인데 편집이미지가 있으면 없앤다(수리출고구분 수정가능일 시 다른방법을..)
                            if (this.uGridDurableMatD.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit
                                && Convert.ToBoolean(this.uGridDurableMatD.Rows[i].Cells["CancelFlag"].Value) == false
                                && this.uGridDurableMatD.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                this.uGridDurableMatD.Rows[i].RowSelectorAppearance.Image = null;
                            }

                            if (this.uGridDurableMatD.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                int intRowNum = this.uGridDurableMatD.Rows[i].RowSelectorNumber;
                                
                                if (uGridDurableMatD.Rows[i].GetCellValue("RepairGIGubunCode").ToString() != string.Empty || uGridDurableMatD.Rows[i].GetCellValue("RepairGIGubunName").ToString() != string.Empty)
                                {
                                    #region 필수 입력사항
                                    ////if (uGridDurableMatD.Rows[i].GetCellValue("CurDurableMatCode").ToString() == string.Empty)
                                    ////{
                                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    ////                , "확인창", "확인창", intRowNum + "번째 열의 구성품을 입력하세요.", Infragistics.Win.HAlign.Right);

                                    ////    this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["CurDurableMatName"];
                                    ////    this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    ////    return;
                                    ////}
                                    if (uGridDurableMatD.Rows[i].GetCellValue("ChgDurableInventoryName").ToString() == string.Empty)
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001264",strLang)
                                                    , intRowNum + msg.GetMessge_Text("M000528",strLang), Infragistics.Win.HAlign.Right);

                                        this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryName"];
                                        this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    if (uGridDurableMatD.Rows[i].GetCellValue("ChgDurableMatCode").ToString() == string.Empty)
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001264",strLang)
                                                    , intRowNum + msg.GetMessge_Text("M000485",strLang), Infragistics.Win.HAlign.Right);

                                        this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["ChgDurableMatName"];
                                        this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    if (!this.uGridDurableMatD.Rows[i].GetCellValue("CurDurableMatCode").ToString().Equals(string.Empty) 
                                        && (uGridDurableMatD.Rows[i].GetCellValue("InputQty").ToString() == string.Empty 
                                        || Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["InputQty"].Value).Equals(0)))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001264",strLang)
                                                    , intRowNum + msg.GetMessge_Text("M000526",strLang), Infragistics.Win.HAlign.Right);

                                        this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["InputQty"];
                                        this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }

                                    //if (!Information.IsNumeric(uGridDurableMatD.Rows[i].GetCellValue("ChgQty")))
                                    //{
                                    //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    //                , "확인창", "확인창", "정확한 수량을 입력하세요.", Infragistics.Win.HAlign.Right);
                                    //    uGridDurableMatD.Focus();
                                    //    return;
                                    //}

                                    if (Convert.ToInt32(uGridDurableMatD.Rows[i].GetCellValue("ChgQty")) <= 0 || this.uGridDurableMatD.Rows[i].Cells["ChgQty"].Value.ToString().Equals(string.Empty))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001264",strLang)
                                                    , intRowNum + msg.GetMessge_Text("M000525",strLang), Infragistics.Win.HAlign.Right);

                                        this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["ChgQty"];
                                        this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }
                                    #endregion

                                    #region 테이블 수량 확인

                                    //교체,투입일 경우
                                    if (!Convert.ToBoolean(this.uGridDurableMatD.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                    {
                                        //교체의 경우
                                        if (!this.uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            //설비BOM 정보 호출 하여 비교한다.
                                            DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(PlantCode, strEquipCode, this.uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                            this.uGridDurableMatD.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //BOM에 정보가 없거나 BOM수량보다 많으면 수량부족 혹은 정보없음 콤보를 다시 뿌려준다.
                                            if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000746",strLang)
                                                                    , intRowNum + msg.GetMessge_Text("M000494",strLang), Infragistics.Win.HAlign.Right);

                                                #region DropDown

                                                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                                                //////--해당설비의 BOM 이없을 경우 리턴 --
                                                ////if (dtDurable.Rows.Count == 0)
                                                ////{
                                                ////    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                ////                            , "확인창", "설비정보 확인", "해당설비에 대한 금형치공구BOM정보가 없습니다. 설비관리기준정보 - 설비구성품정보로 등록하세요.", Infragistics.Win.HAlign.Right);

                                                ////    return;
                                                ////}
                                                //콤보 그리드 리스트 클러어
                                                this.uGridDurableMatD.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                                                //콤보그리드 컬럼설정
                                                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                                                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                                                //--그리드에 추가 --
                                                WinGrid wGrid = new WinGrid();
                                                wGrid.mfSetGridColumnValueGridList(this.uGridDurableMatD, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "DurableMatCode", "DurableMatName", dtDurable);
                                                #endregion

                                                this.uGridDurableMatD.Rows[i].Cells["InputQty"].Value = 0;
                                                this.uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;

                                                this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["CurDurableMatName"];
                                                this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }

                                        }

                                        //치공구 재고 정보를 호출하여 비교한다.
                                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(PlantCode, this.uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryName"].Value.ToString(),
                                                                                                    this.uGridDurableMatD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                    this.uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                        //Stock 정보가 없거나 재고수량 부족할 경우 콤보를 다시 뿌려준다.
                                        if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000746",strLang)
                                                                , intRowNum + msg.GetMessge_Text("M000495",strLang), Infragistics.Win.HAlign.Right);
                                            
                                            ChgDurbleMat(strPlantCode, this.uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                            
                                            this.uGridDurableMatD.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                            this.uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                            this.uGridDurableMatD.Rows[i].Cells["CurQty"].Value = 0;
                                            this.uGridDurableMatD.Rows[i].Cells["ChgQty"].Value = 0;
                                            this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }

                                    }
                                    // 수리취소 할 경우
                                    else
                                    {
                                        DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridDurableMatD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                    this.uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                       
                                        //BOM에 정보가 없거나 BOM수량보다 많으면 수량부족 혹은 정보없음 수리취소 불가
                                        if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000746",strLang)
                                                                , intRowNum + msg.GetMessge_Text("M000498",strLang), Infragistics.Win.HAlign.Right);
                                            this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["CancelFlag"];
                                            return;
                                        }

                                        //교체를 한정보 인경우
                                        if (!this.uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                   this.uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                   this.uGridDurableMatD.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //Stock 정보가 없거나 재고수량 부족할 경우 수리취소 불가
                                            if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) 
                                                < Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000746",strLang)
                                                                    , intRowNum + msg.GetMessge_Text("M000488",strLang), Infragistics.Win.HAlign.Right);
                                                this.uGridDurableMatD.ActiveCell = this.uGridDurableMatD.Rows[i].Cells["CancelFlag"];
                                                return;
                                            }
                                        }
                                    }

                                    #endregion

                                }
                            }
                        }
                    }
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000915",
                                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");


                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH DurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(DurableMatRepairH);

                

                #region Header
                DataTable dtRepairH = DurableMatRepairH.mfDataSetInfo();

                DataRow drRepairH = dtRepairH.NewRow();

                drRepairH["PlantCode"] = PlantCode;
                drRepairH["RepairGICode"] = uTextRepairGICode.Text;
                drRepairH["GITypeCode"] = "RE";
                drRepairH["EquipCode"] = strEquipCode;
                drRepairH["RepairReqCode"] = RepairReqCode;
                drRepairH["RepairGIReqDate"] = Convert.ToDateTime(uDateRepairGIReqDate.Value).ToString("yyyy-MM-dd");
                drRepairH["RepairGIReqID"] = uTextRepairGIReqID.Text;
                drRepairH["EtcDesc"] = uTextEtcDesc.Text;

                dtRepairH.Rows.Add(drRepairH);
                #endregion

                #region Detail

                QRPEQU.BL.EQUMGM.DurableMatRepairD DurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();

                DataTable dtMatRepairD = DurableMatRepairD.mfsetDataInfo();


                if (this.uGridDurableMatD.Rows.Count > 0)
                {
                    for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
                    {
                        if (!uGridDurableMatD.Rows[i].Hidden)
                        {
                            

                            if (this.uGridDurableMatD.Rows[i].RowSelectorAppearance.Image != null)
                            {



                                DataRow drRepairD = dtMatRepairD.NewRow();

                                if (uGridDurableMatD.Rows[i].Cells["PlantCode"].Value.ToString() != string.Empty)
                                {
                                    drRepairD["PlantCode"] = uGridDurableMatD.Rows[i].Cells["PlantCode"].Value;
                                    drRepairD["RepairGICode"] = uGridDurableMatD.Rows[i].Cells["RepairGICode"].Value;
                                    drRepairD["Seq"] = uGridDurableMatD.Rows[i].Cells["Seq"].Value;
                                    drRepairD["CancelFlag"] = Convert.ToBoolean(uGridDurableMatD.Rows[i].Cells["CancelFlag"].Value) == false ? "F" : "T";
                                }
                                else
                                {
                                    drRepairD["PlantCode"] = PlantCode;
                                    drRepairD["RepairGICode"] = RepairGICode;
                                    drRepairD["Seq"] = 0;
                                    drRepairD["CancelFlag"] = "F";
                                }

                                if(uGridDurableMatD.Rows[i].Cells["RepairGIGubunCode"].Value.ToString().Equals(string.Empty))
                                    drRepairD["RepairGIGubunCode"] = uGridDurableMatD.Rows[i].Cells["RepairGIGubunName"].Value;
                                else
                                    drRepairD["RepairGIGubunCode"] = uGridDurableMatD.Rows[i].Cells["RepairGIGubunCode"].Value;

                                drRepairD["CurDurableMatCode"] = uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value;
                                drRepairD["CurLotNo"] = uGridDurableMatD.Rows[i].Cells["CurLotNo"].Value;
                                drRepairD["CurQty"] = uGridDurableMatD.Rows[i].Cells["InputQty"].Value;

                                if (!uGridDurableMatD.Rows[i].GetCellValue("ChgDurableInventoryCode").ToString().Equals(string.Empty))
                                {
                                    drRepairD["ChgDurableInventoryCode"] = uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryCode"].Value;
                                }
                                else
                                {
                                    drRepairD["ChgDurableInventoryCode"] = uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryName"].Value;
                                }

                                drRepairD["ChgDurableMatCode"] = uGridDurableMatD.Rows[i].Cells["ChgDurableMatCode"].Value;
                                drRepairD["ChgLotNo"] = uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value;
                                drRepairD["UnitCode"] = uGridDurableMatD.Rows[i].Cells["UnitCode"].Value;
                                drRepairD["ChgQty"] = uGridDurableMatD.Rows[i].Cells["ChgQty"].Value;
                                drRepairD["EtcDesc"] = uGridDurableMatD.Rows[i].Cells["EtcDesc"].Value;

                                dtMatRepairD.Rows.Add(drRepairD);
                            }
                        }
                    }
                }
                #endregion

                #region Save

                TransErrRtn ErrRtn = new TransErrRtn();

                string strRtn = DurableMatRepairH.mfSaveDurableMatRepairH(dtRepairH, dtMatRepairD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                //Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);

                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum != 0)
                {
                    string strMsg = "";
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = msg.GetMessge_Text("M000953",strLang);
                    else
                        strMsg = ErrRtn.ErrMessage;

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                 ,Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                 ,msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang)
                                                 , strMsg,Infragistics.Win.HAlign.Right);

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            titleArea.Focus();

            if (!uGroupBoxContentsArea.Expanded)
                return;

            #region Indispensable
            if (uTextRepairGICode.Text == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001264", "M000642", Infragistics.Win.HAlign.Right);
                uGridDurableMatRepairH.Focus();
                return;
            }

            for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
            {
                if (!Convert.ToBoolean(uGridDurableMatD.Rows[i].Cells["CancelFlag"].Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001264", "M000744", Infragistics.Win.HAlign.Right);
                    return; 
                }
            }

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000672",
                                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }
            #endregion

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                System.Windows.Forms.DialogResult result;

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH DurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(DurableMatRepairH);

                string strRtn = DurableMatRepairH.mfDeleteDurableMatRepairH(PlantCode, RepairGICode);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);

                if (ErrRtn.ErrNum != 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M000638", "M000676",
                                                 Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M000638", "M000677",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
           
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridDurableMatRepairH.Rows.Count == 0 && (this.uGridDurableMatD.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                if(this.uGridDurableMatRepairH.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridDurableMatRepairH);

                if (this.uGridDurableMatD.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridDurableMatD);
               
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Grid Event
        // 행삭제 버튼 클릭 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDurableMatD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDurableMatD.Rows[i].Cells["Check"].Value) == true)
                    {
                        if (uGridDurableMatD.Rows[i].Cells["PlantCode"].Value.ToString() == string.Empty
                            && uGridDurableMatD.Rows[i].Cells["RepairGICode"].Value.ToString() == string.Empty
                            && Convert.ToInt32(uGridDurableMatD.Rows[i].Cells["Seq"].Value) == 0
                            && uGridDurableMatD.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() == string.Empty)
                        {
                            this.uGridDurableMatD.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();

            }
            finally
            {
            }
        }

        //컬럼리스트 클릭 전에 필수 입력사항 확인
        private void uGridDurableMatD_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //컬럼이름 저장
                string strCloumns = e.Cell.Column.Key;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (strCloumns.Equals("ChgDurableInventoryName"))
                {
                    if (e.Cell.Row.GetCellValue("RepairGIGubunName").ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000742", "M000743", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uGridDurableMatD.ActiveCell = e.Cell.Row.Cells["RepairGIGubunName"];
                        this.uGridDurableMatD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }

                    if (this.uTextEquipCode.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001075", "M001026", Infragistics.Win.HAlign.Right);

                        if (this.uGroupBoxContentsArea.Expanded == true)
                        {
                            this.uGroupBoxContentsArea.Expanded = false;
                        }

                        return;
                    }
                }

               

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableMatD_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            // Cell 수정시 RowSelector 이미지 변경

            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        private void uGridDurableMatD_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            System.Windows.Forms.DialogResult result;

            try
            {
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableMatD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                

                #region 수량
                if (e.Cell.Column.Key.Equals("InputQty") && Convert.ToInt32(e.Cell.Value) > 0)
                {

                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        // 같은정보의 수량을 비교하여 총수량이 기존 수량보다 많으면 메세지 출력을 한다.
                        for (int i = 0; i < this.uGridDurableMatD.Rows.Count; i++)
                        {
                            if (this.uGridDurableMatD.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridDurableMatD.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridDurableMatD.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                            {
                                if (
                                    e.Cell.Row.Cells["CurDurableMatCode"].Value.Equals(this.uGridDurableMatD.Rows[i].Cells["CurDurableMatCode"].Value))
                                {

                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["InputQty"].Value);

                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["CurLotNo"].Tag;
                            string strLang = m_resSys.GetString("SYS_LANG");

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000617",strLang)
                                                   , msg.GetMessge_Text("M000659",strLang) + strQty + msg.GetMessge_Text("M000010",strLang)
                                                   , Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }

                        if (e.Cell.Tag != null && !e.Cell.Value.ToString().Equals(string.Empty))
                        {
                            if (Convert.ToInt32(e.Cell.Tag) < Convert.ToInt32(e.Cell.Value))
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "M001264", "M000731", "M000317",
                                                             Infragistics.Win.HAlign.Right);
                                e.Cell.Value = e.Cell.Tag;
                                return;
                            }
                        }

                    }
                    else
                        e.Cell.Value = 0;
                }
                #endregion

                #region 교체량
                if (e.Cell.Column.Key.Equals("ChgQty"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {

                        if (Convert.ToInt32(e.Cell.Row.Cells["CurQty"].Value) < Convert.ToInt32(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M000731", "M000157",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = 0;
                            return;
                        }
                        if (e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) 
                            && e.Cell.Row.Cells["InputQty"].Tag != null 
                            && Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag) < Convert.ToInt32(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M000731", "M000344",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = 0;
                            return;
                        }

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                        for (int i = 0; i < this.uGridDurableMatD.Rows.Count; i++)
                        {
                            if (this.uGridDurableMatD.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridDurableMatD.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridDurableMatD.Rows[i].Hidden.Equals(false)
                                && this.uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(string.Empty)
                                )
                            {
                                if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(this.uGridDurableMatD.Rows[i].Cells["ChgDurableMatCode"].Value) &&
                                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(this.uGridDurableMatD.Rows[i].Cells["ChgDurableInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["CurQty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridDurableMatD.Rows[i].Cells["ChgQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;
                            string strLang = m_resSys.GetString("SYS_LANG");

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000302",strLang)
                                                   , msg.GetMessge_Text("M000891",strLang) + strQty + msg.GetMessge_Text("M000010",strLang)
                                                   , Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }
                        
                    }
                }
                #endregion

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableMatRepairH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            WinGrid wGrid = new WinGrid();

            string strRepairReqCode;
            string strRepairGICode;

            foreach (Control ctrl in uGroupBoxContentsArea.Controls)
            {
                if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                {
                    ctrl.Text = string.Empty;
                }
            }

            RepairGICode = string.Empty;
            RepairReqCode = string.Empty;
            PlantCode = string.Empty;

            uDateRepairGIReqDate.Value = DateTime.Now;
            uTextRepairGIReqID.Text = m_resSys.GetString("SYS_USERID");
            uTextRepairGIReqName.Text = m_resSys.GetString("SYS_USERNAME");

            uGridDurableMatD.Refresh();
            wGrid.mfAddRowGrid(this.uGridDurableMatD, 0);

            if (e.Cell.Row.GetCellValue("RepairReqCode").ToString() == string.Empty)
                return;

            strRepairReqCode = e.Cell.Row.GetCellValue("RepairReqCode").ToString();
            strRepairGICode = e.Cell.Row.GetCellValue("RepairGICode").ToString();

            try
            {

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                #region Read Header
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsEquip = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadDurableMatRepairH_Detail_Repair(strRepairReqCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    uGroupBoxContentsArea.Expanded = false;
                    return;
                }

                PlantCode = dtEquip.Rows[0]["PlantCode"].ToString();
                RepairGICode = dtEquip.Rows[0]["RepairGICode"].ToString();
                RepairReqCode = dtEquip.Rows[0]["RepairReqCode"].ToString();

                #region DropDown

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtclsDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_Lot(PlantCode, dtEquip.Rows[0]["EquipCode"].ToString(), m_resSys.GetString("SYS_LANG"));

                //if (dtclsDurableMat.Rows.Count == 0)
                //{
                    
                //    this.MdiParent.Cursor = Cursors.Default;
                //    m_ProgressPopup.mfCloseProgressPopup(this);

                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                     , "확인창", "설비정보 확인", "해당설비에 대한 금형치공구BOM정보가 없습니다. 설비관리기준정보 - 설비구성품정보로 등록하세요.", Infragistics.Win.HAlign.Right);


                //    if (this.uGroupBoxContentsArea.Expanded == true)
                //    {
                //        this.uGroupBoxContentsArea.Expanded = false;
                //    }
                //    return;
                //}

                //콤보 그리드 리스트 클러어
                this.uGridDurableMatD.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();


                wGrid.mfSetGridColumnValueGridList(this.uGridDurableMatD, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName", "치공구코드,치공구,LOTNO,수량,단위", "DurableMatCode", "DurableMatName", dtclsDurableMat);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(PlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurableMatD, 0, "ChgDurableInventoryName", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);


                #endregion

                uTextRepairGICode.Text = dtEquip.Rows[0]["RepairGICode"].ToString();
                uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();
                uTextEquipCode.Tag = dtEquip.Rows[0]["ModelName"].ToString();
                uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                uTextRepairEndDate.Text = dtEquip.Rows[0]["RepairEndDate"].ToString();
                uTextRepairID.Text = dtEquip.Rows[0]["RepairID"].ToString();
                uTextRepairName.Text = dtEquip.Rows[0]["RepairName"].ToString();
                uTextRepairResultName.Text = dtEquip.Rows[0]["RepairResultName"].ToString();
                uTextEquipStopTypeName.Text = dtEquip.Rows[0]["EquipStopTypeName"].ToString();
                uTextRepairDesc.Text = dtEquip.Rows[0]["RepairDesc"].ToString();

                if (dtEquip.Rows[0]["RepairGIReqDate"].ToString() == string.Empty)
                    uDateRepairGIReqDate.Value = DateTime.Now;
                else
                    uDateRepairGIReqDate.Value = dtEquip.Rows[0]["RepairGIReqDate"].ToString();

                if (dtEquip.Rows[0]["RepairGIReqID"].ToString() == string.Empty)
                {
                    uTextRepairGIReqID.Text = m_resSys.GetString("SYS_USERID");
                    uTextRepairGIReqName.Text = m_resSys.GetString("SYS_USERNAME");
                }
                else
                {
                    uTextRepairGIReqID.Text = dtEquip.Rows[0]["RepairGIReqID"].ToString();
                    uTextRepairGIReqName.Text = dtEquip.Rows[0]["RepairGIReqName"].ToString();
                }                

                uTextEtcDesc.Text = dtEquip.Rows[0]["EtcDesc"].ToString();

                
                #endregion

                #region Detail

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsEquipD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsEquipD);

                DataTable dtDetail = clsEquipD.mfReadDurableMatRepairD(PlantCode, uTextEquipCode.Text, RepairGICode, m_resSys.GetString("SYS_LANG"));

                uGridDurableMatD.Refresh();
                uGridDurableMatD.DataSource = dtDetail;
                uGridDurableMatD.DataBind();
                uGridDurableMatD_AfterDataBinding();

                if (dtDetail.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurableMatD, 0);
                }
                #endregion

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                uGroupBoxContentsArea.Expanded = true;
                uGridDurableMatRepairH.Rows[e.Cell.Row.Index].Fixed = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableMatD_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString());

                titleArea.Focus();
                
                 ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                 WinMessageBox msg = new WinMessageBox();

                #region CurDurableMatCode
                if (e.Cell.Column.Key.ToString() == "CurDurableMatName")
                {

                    //e.Cell.Row.Cells["CurDurableMatCode"].Value = "";
                    //e.Cell.Row.Cells["CurDurableMatName"].Value = "";
                    //e.Cell.Row.Cells["CurLotNo"].Value = "";
                    //e.Cell.Row.Cells["InputQty"].Value = 0;
                    //e.Cell.Row.Cells["ChgDurableInventoryCode"].Value = "";
                    //e.Cell.Row.Cells["ChgDurableInventoryName"].Value = "";
                    //e.Cell.Row.Cells["ChgDurableMatCode"].Value = "";
                    //e.Cell.Row.Cells["ChgDurableMatName"].Value = "";

                    //e.Cell.Row.Cells["ChgLotNo"].Value = "";
                    //e.Cell.Row.Cells["CurQty"].Value = 0;
                    //e.Cell.Row.Cells["ChgQty"].Value = 0;
                    //e.Cell.Row.Cells["EtcDesc"].Value = "";


                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                    QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                    brwChannel.mfCredentials(clsDurableMat);

                    string strDurableMatCode = uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"].Value.ToString();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    DataTable dtDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_Lot(PlantCode, uTextEquipCode.Text, strLang);

                    if (dtDurableMat.Rows.Count != 0)
                    {
                        //단위저장
                        e.Cell.Row.Cells["CurLotNo"].Tag = dtDurableMat.Rows[intSelectItem]["UnitCode"].ToString();

                        uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = dtDurableMat.Rows[intSelectItem]["LotNo"].ToString();
                        uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = dtDurableMat.Rows[intSelectItem]["DurableMatCode"].ToString();

                        uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = dtDurableMat.Rows[intSelectItem]["InputQty"].ToString();
                        uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = dtDurableMat.Rows[intSelectItem]["InputQty"].ToString();

                        
                        //e.Cell.Row.Cells["ChgDurableInventoryName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        
                        if (!dtDurableMat.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                        {
                            uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }
                        else
                        {
                            uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        }

                        for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                uGridDurableMatD.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(dtDurableMat.Rows[intSelectItem]["LotNo"].ToString()) &&
                                !dtDurableMat.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty) &&
                                this.uGridDurableMatD.Rows[i].Cells["Check"].Activation== Infragistics.Win.UltraWinGrid.Activation.AllowEdit &&
                                !this.uGridDurableMatD.Rows[i].Appearance.BackColor.Equals(Color.Yellow))
                            {
                                
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;

                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"].Value = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                e.Cell.Row.RowSelectorAppearance.Image = null;
                                e.Cell.Row.Cells["ChgDurableInventoryName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                return;
                            }
                        }

                        if (!e.Cell.Row.Cells["ChgDurableInventoryName"].Value.Equals(string.Empty))
                        {
                            ChgDurbleMat(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryName"].Value.ToString(), e.Cell.Row.Index);
                            
                            if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgDurableMatCode"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["CurQty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                        }
                    }
                }
                #endregion

                #region ChgDurableInventory
                if (e.Cell.Column.Key.ToString().Equals("ChgDurableInventoryName"))
                {
                    //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryName"].Value.ToString();

                    //brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    //QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    //brwChannel.mfCredentials(clsDurableStock);

                    //DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(PlantCode, _strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

                    //string strCode = "";
                    //if (e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                    //{
                    //    strCode = "LotNo = ''";
                    //}
                    //else
                    //{
                    //    strCode = "LotNo <> ''";
                    //}

                    //DataRow[] dr = dtStock.Select(strCode);

                    //DataTable dtBind = dtStock.Copy();
                    //dtBind.Clear();

                    //for (int i = 0; i < dr.Length; i++)
                    //{
                    //    DataRow _dr = dtBind.NewRow();
                    //    for (int j = 0; j < _dr.ItemArray.Length; j++)
                    //    {
                    //        _dr[j] = dr[i][j];
                    //    }
                    //    dtBind.Rows.Add(_dr);
                    //}

                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    brwChannel.mfCredentials(clsRepairReq);

                    DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                          , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                    string strLotChk = "";

                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                        strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                    //금형치공구 현재고 정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //금형치공구창고에 해당하는 금형치공구 재고 조회
                    DataTable dtBind = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, _strDurableInventoryCode, 
                                                                                    dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                    strLotChk, m_resSys.GetString("SYS_LANG"));

                    if (dtBind.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000300", "M000663", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    //WinGrid wGrid = new WinGrid();
                    mfSetGridCellValueGridList(uGridDurableMatD, 0, e.Cell.Row.Index, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DataValue,
                        "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName", "치공구코드,치공구,LOTNO,수량,단위코드,단위", "DurableMatCode", "DurableMatName", dtBind);

                }
                #endregion

                #region ChgDurableMatCode
                if (e.Cell.Column.Key.ToString().Equals("ChgDurableMatName"))
                {
                    //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryName"].Value.ToString();
                    string _strDurableMatCode = e.Cell.Value.ToString();

                    ////brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    ////QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    ////brwChannel.mfCredentials(clsDurableStock);

                    ////DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(PlantCode, _strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

                    ////string strCode = "";
                    ////if (e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                    ////{
                    ////    strCode = "LotNo = ''";
                    ////}
                    ////else
                    ////{
                    ////    strCode = "LotNo <> ''";
                    ////}

                    ////DataRow[] dr = dtStock.Select(strCode);

                    ////DataTable dtBind = dtStock.Copy();
                    ////dtBind.Clear();

                    ////for (int i = 0; i < dr.Length; i++)
                    ////{
                    ////    DataRow _dr = dtBind.NewRow();
                    ////    for (int j = 0; j < _dr.ItemArray.Length; j++)
                    ////    {
                    ////        _dr[j] = dr[i][j];
                    ////    }
                    ////    dtBind.Rows.Add(_dr);
                    ////}

                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    brwChannel.mfCredentials(clsRepairReq);

                    DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                        return;
                    
                    //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                    string strLotChk = "";

                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                        strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                    //금형치공구 현재고 정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //금형치공구창고에 해당하는 금형치공구 재고 조회
                    DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, _strDurableInventoryCode, 
                                                                                    dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                    strLotChk, m_resSys.GetString("SYS_LANG"));

                    if (!dtStock.Rows.Count.Equals(0))
                    {
                        //if (!e.Cell.Row.Cells["CurLotNo"].Tag.Equals(dtBind.Rows[intSelectItem]["UnitCode"]))
                        //{
                        //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //                        "확인창", "구성품 단위확인", "기존구성품 단위와 교체구성품의 단위가 다릅니다.", Infragistics.Win.HAlign.Right);

                        //    return;
                        //}
                        e.Cell.Row.Cells["ChgLotNo"].Value = dtStock.Rows[intSelectItem]["LotNo"].ToString();
                        e.Cell.Row.Cells["CurQty"].Tag = dtStock.Rows[intSelectItem]["Qty"];
                        e.Cell.Row.Cells["CurQty"].Value = dtStock.Rows[intSelectItem]["Qty"];

                        //e.Cell.Row.Cells["UnitCode"].Value = dtBind.Rows[intSelectItem]["UnitCode"].ToString();

                        e.Cell.Row.Cells["ChgDurableMatCode"].Value = dtStock.Rows[intSelectItem]["DurableMatCode"].ToString();

                        if (!e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["ChgQty"].Tag = dtStock.Rows[intSelectItem]["Qty"];
                            e.Cell.Row.Cells["ChgQty"].Value = dtStock.Rows[intSelectItem]["Qty"];

                            e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }
                        else
                        {
                            e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        }

                        for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtStock.Rows[intSelectItem]["LotNo"].ToString()) &&
                                !dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty) &&
                                this.uGridDurableMatD.Rows[i].Cells["Check"].Activation== Infragistics.Win.UltraWinGrid.Activation.AllowEdit &&
                                !this.uGridDurableMatD.Rows[i].Appearance.BackColor.Equals(Color.Yellow))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = 0;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Value = 0;

                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgLotNo"].Value = string.Empty;
                                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgDurableMatCode"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                return;
                            }
                        }
                    }

                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region UI Event
        // ExpandableGroupBox 상태변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDurableMatRepairH.Height = 65;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDurableMatRepairH.Height = 740;

                    for (int i = 0; i < uGridDurableMatRepairH.Rows.Count; i++)
                    {
                        uGridDurableMatRepairH.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        #region Text
        private void uTextRepairGIReqID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
               if(!this.uTextRepairGIReqName.Text.Equals(string.Empty))
               {
                   this.uTextRepairGIReqName.Clear();
               }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //수리출고요정자 ID입력후 엔터시 자동조회
        private void uTextRepairGIReqID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //수리출고요청자ID저장
                string strGIReqID = this.uTextRepairGIReqID.Text;

                if (!strGIReqID.Equals(string.Empty))
                {

                    if (strPlantCode == null || strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000400", "M000391", Infragistics.Win.HAlign.Right);

                        if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                        {
                            this.uGroupBoxContentsArea.Expanded = false;
                        }
                        return;
                    }

                    //유저정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //유정정보조회매서드 호출
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strGIReqID, m_resSys.GetString("SYS_LANG"));

                    //정보가 없을 경우 메세지 있을 경우 텍스트에 삽입
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextRepairGIReqName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001114", "M000883", Infragistics.Win.HAlign.Right);

                        if (!this.uTextRepairGIReqName.Text.Equals(string.Empty))
                        {
                            this.uTextRepairGIReqName.Clear();
                        }
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //수리출고요청자 버틀 누를 시 유저정보팝업창을 띄운다.
        private void uTextRepairGIReqID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if(strPlantCode == null || strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000400", "M000391", Infragistics.Win.HAlign.Right);

                    if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                    {
                        this.uGroupBoxContentsArea.Expanded = false;
                    }
                    return;
                }


                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();


                this.uTextRepairGIReqID.Text = frmUser.UserID;
                this.uTextRepairGIReqName.Text = frmUser.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Combo

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinComboEditor wCombo = new WinComboEditor();

            try
            {
                if (uComboSearchPlant.Value.ToString() != string.Empty)
                {
                    
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);

                    //DataTable dtArea = clsArea.mfReadAreaCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    //uComboSearchArea.ValueList.Reset();
                    //wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "AreaCode", "AreaName", dtArea);
                    
                    this.uComboSearchStation.Items.Clear();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    
                    wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                        , "StationCode", "StationName", dtStation);

                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    //brwChannel.mfCredentials(clsEquipProcGubun);

                    //DataTable dtEquipProcGugun = clsEquipProcGubun.mfReadProGubunCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    //uComboSearchEquipProcess.ValueList.Reset();
                    //wCombo.mfSetComboEditor(this.uComboSearchEquipProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "EquipProcGubunCode", "EquipProcGubunName", dtEquipProcGugun);

                }
                else
                {
                    //uComboSearchArea.ValueList.Reset();
                    //uComboSearchEquipProcess.ValueList.Reset();
                    uComboSearchStation.ValueList.Reset();
                    uComboSearchEquipGroup.ValueList.Reset();
                    uComboEquipLargeType.ValueList.Reset();
                    uComboEquipLoc.ValueList.Reset();
                    uComboProcessGroup.ValueList.Reset();

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }

        }

        private void uDateRepairGIReqDate_AfterExitEditMode(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            System.Windows.Forms.DialogResult result;
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (Convert.ToDateTime(uDateRepairGIReqDate.Value) > DateTime.Now)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001264", "M001264", "M001260",
                                                Infragistics.Win.HAlign.Right);
                    uDateRepairGIReqDate.Value = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboEquipLoc.Items.Clear();
                    WinComboEditor wCombo = new WinComboEditor();

                    wCombo.mfSetComboEditor(this.uComboEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);
                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    //해당콤보의 텍스트정보와 코드정보를 저장하여 아이템정보에 존재여부확인후 검색
                    string strText = this.uComboEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), "", "", 3);

                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    //해당콤보의 텍스트와 코드값을 저장하여 아이템정보에 존재여부확인후 검색
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboEquType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), this.uComboEquipLargeType.Value.ToString(), 1);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion


        #region 콤보박스 설정

        /// <summary>
        /// 그리드 Cell에 콤보박스로 설정하는 함수
        /// </summary>
        /// <param name="Grid">그리드 Object</param>
        /// <param name="intBandIndex"></param>
        /// <param name="strColKey"></param>
        /// <param name="ValueListStyle"></param>
        /// <param name="strTopKey"></param>
        /// <param name="strTopValue"></param>
        /// <param name="dtValueList"></param>
        public void mfSetGridCellValueGridList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                             int intBandIndex,
                                             int intRowIndex,
                                             string strColKey,
                                             Infragistics.Win.ValueListDisplayStyle ValueListStyle,
                                             string strDropDownGridHKey,
                                             string strDropDownGridHName,
                                             string strKeyName,
                                             string strValueName,
                                             System.Data.DataTable dtValueList)
        {
            try
            {
                #region 수정부분 %% ValueList 이름이 동일해서 바인딩이 안된 문제 수정
                string strValueListColKey = "";
                string strcol = "_List";
                Boolean blCheck = false;
                int z = 0;
                while (blCheck == false)
                {
                    if (!Grid.DisplayLayout.ValueLists.Exists(strColKey + strcol))
                    {
                        strValueListColKey = strColKey + strcol;
                        blCheck = true;
                    }
                    z += 1;
                    strcol = "_List" + z.ToString();
                }
                #endregion

                //그리드에 ValueList Collection 추가
                Grid.DisplayLayout.ValueLists.Add(strValueListColKey);

                //ValueList의 Style 지정
                Grid.DisplayLayout.ValueLists[strValueListColKey].DisplayStyle = ValueListStyle;

                //Value List에 상단값 설정
                //if (strTopKey != "" || strTopValue != "")
                //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(strTopKey, strTopValue);

                Infragistics.Win.UltraWinGrid.UltraDropDown uDropDown = new Infragistics.Win.UltraWinGrid.UltraDropDown();

                #region Grid DropDown 속성 지정
                uDropDown.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsVista;
                //헤더 정렬 지정
                uDropDown.DisplayLayout.Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                uDropDown.DisplayLayout.Override.HeaderAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                //헤더 배경색 및 글짜색 지정
                uDropDown.DisplayLayout.Override.HeaderAppearance.BackColor = System.Drawing.Color.FromArgb(106, 160, 206);
                uDropDown.DisplayLayout.Override.HeaderAppearance.ForeColor = System.Drawing.Color.FromArgb(50, 50, 50);
                uDropDown.DisplayLayout.Override.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //선택할 셀의 컬럼헤더 글자색 지정
                uDropDown.DisplayLayout.Override.ActiveCellColumnHeaderAppearance.ForeColor = Color.DimGray;

                //홀수줄 짝수줄 색깔 지정
                uDropDown.DisplayLayout.Override.RowAppearance.BackColor = System.Drawing.Color.White;
                uDropDown.DisplayLayout.Override.RowAlternateAppearance.BackColor = System.Drawing.Color.MintCream;

                //그리드 행에 MouseOver시 색깔 지정 (요기 처리해야 함)
                uDropDown.DisplayLayout.Override.HotTrackRowAppearance.BackColor = System.Drawing.Color.WhiteSmoke;
                uDropDown.DisplayLayout.Override.HotTrackRowAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
                uDropDown.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);

                //그리드 내부선 색 지정
                uDropDown.DisplayLayout.Override.CellAppearance.BorderColor = System.Drawing.Color.FromArgb(125, 138, 158);
                uDropDown.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Default;
                uDropDown.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Default;
                
                //스크롤이 최하단으로 내려갔을 때 빈공간이 없도록 설정 
                uDropDown.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;

                //그리드 컬럼 자동생성 해지 (기본값 = Hide)
                uDropDown.DisplayLayout.NewColumnLoadStyle = Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide;
                #endregion

                //컬럼설정
                char[] strSeparator = { ',' };
                string[] arrKey = strDropDownGridHKey.Split(strSeparator);
                string[] arrName = strDropDownGridHName.Split(strSeparator);
                for (int i = 0; i < arrKey.Count(); i++)
                {
                    uDropDown.DisplayLayout.Bands[0].Columns.Add(arrKey[i], arrName[i]);
                }

                uDropDown.Visible = false;
                uDropDown.DataSource = dtValueList;
                uDropDown.ValueMember = strKeyName;
                uDropDown.DisplayMember = strValueName;

                Grid.Rows[intRowIndex].Cells[strColKey].ValueList = uDropDown;
                
                //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = uDropDown;

                //추가적인 Value List 값 설정
                //foreach (DataRow dr in dtValueList.Rows)
                //{
                //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(dr[0].ToString(), dr[1].ToString());
                //}

                ////그리드 컬럼에 ValueList 지정            
                //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = Grid.DisplayLayout.ValueLists[strValueListColKey];
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 교체구성품그리드콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInven">창고정보</param>
        /// <param name="e">Infragistics.Win.UltraWinGrid.CellEventArgs</param>
        private void ChgDurbleMat(string strPlantCode, string strInven, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //금형치공구창고에 해당하는 금형치공구 재고 조회
                DataTable dtStock = new DataTable();

                //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                if (this.uGridDurableMatD.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                {
                    //strLotChk = "LotNo = ''";
                    dtStock = clsDurableStock.mfReadDurableStock_SPCombo_OnlyDurable(strPlantCode, strInven, m_resSys.GetString("SYS_LANG"));
                }
                else
                {
                    //strLotChk = "LotNo <> ''";
                    dtStock = clsDurableStock.mfReadDurableStock_SPCombo_OnlyLot(strPlantCode, strInven, m_resSys.GetString("SYS_LANG"));
                }

                //금형치공구정보에 검색조건 적용
                WinGrid wGrid = new WinGrid();

                //                this.uGridRepair.Rows[intIndex].Cells["ChgDurableMatName"].Column.Layout.ValueLists.Clear();
                string strDropDownValue = "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName";
                string strDropDownText = "치공구코드,치공구,LOTNO,수량,단위코드,단위";

                //콤보그리드 초기화.
                //e.Cell.Column.Band.Layout.ValueLists.Clear();

                wGrid.mfSetGridCellValueGridList(this.uGridDurableMatD, 0, intIndex, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                   , strDropDownValue, strDropDownText, "DurableMatCode", "DurableMatName", dtStock);

                if (dtStock.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 그리드 바인드후 수정불가처리
        /// </summary>
        private void uGridDurableMatD_AfterDataBinding()
        {
            try
            {
                for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDurableMatD.Rows[i].GetCellValue("CancelFlag")))
                    {
                        uGridDurableMatD.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;
                        uGridDurableMatD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    else
                    {
                        for (int j = 0; j < uGridDurableMatD.Rows[i].Cells.Count; j++)
                        {
                            if (uGridDurableMatD.Rows[i].Cells["PlantCode"].Value.ToString() != string.Empty)
                            {
                                if (uGridDurableMatD.Rows[i].Cells[j].Column.Key.ToString() == "CancelFlag")// || uGridDurableMatD.Rows[i].Cells[j].Column.Key.ToString() == "RepairGIGubunName")
                                    
                                {
                                    uGridDurableMatD.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }
                                else
                                {
                                    uGridDurableMatD.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                        }
                    }

                    uGridDurableMatD.Rows[i].Cells["InputQty"].Tag = uGridDurableMatD.Rows[i].Cells["InputQty"].Value;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void mfSelectRepairReq()
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            WinGrid wGrid = new WinGrid();

            foreach (Control ctrl in uGroupBoxContentsArea.Controls)
            {
                if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                {
                    ctrl.Text = string.Empty;
                }
            }

            RepairGICode = string.Empty;
            PlantCode = string.Empty;

            uDateRepairGIReqDate.Value = DateTime.Now;
            uTextRepairGIReqID.Text = m_resSys.GetString("SYS_USERID");
            uTextRepairGIReqName.Text = m_resSys.GetString("SYS_USERNAME");

            uGridDurableMatD.Refresh();
            wGrid.mfAddRowGrid(this.uGridDurableMatD, 0);

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                #region Read Header
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsEquip = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadDurableMatRepairH_Detail_Repair(RepairReqCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    uGroupBoxContentsArea.Expanded = false;
                    return;
                }

                uTextRepairGICode.Text = dtEquip.Rows[0]["RepairGICode"].ToString();
                uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();
                uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                uTextRepairEndDate.Text = dtEquip.Rows[0]["RepairEndDate"].ToString();
                uTextRepairID.Text = dtEquip.Rows[0]["RepairID"].ToString();
                uTextRepairName.Text = dtEquip.Rows[0]["RepairName"].ToString();
                uTextRepairResultName.Text = dtEquip.Rows[0]["RepairResultName"].ToString();
                uTextEquipStopTypeName.Text = dtEquip.Rows[0]["EquipStopTypeName"].ToString();
                uTextRepairDesc.Text = dtEquip.Rows[0]["RepairDesc"].ToString();

                if (dtEquip.Rows[0]["RepairGIReqDate"].ToString() == string.Empty)
                    uDateRepairGIReqDate.Value = DateTime.Now;
                else
                    uDateRepairGIReqDate.Value = dtEquip.Rows[0]["RepairGIReqDate"].ToString();

                if (dtEquip.Rows[0]["RepairGIReqID"].ToString() == string.Empty)
                {
                    uTextRepairGIReqID.Text = m_resSys.GetString("SYS_USERID");
                    uTextRepairGIReqName.Text = m_resSys.GetString("SYS_USERNAME");
                }
                else
                {
                    uTextRepairGIReqID.Text = dtEquip.Rows[0]["RepairGIReqID"].ToString();
                    uTextRepairGIReqName.Text = dtEquip.Rows[0]["RepairGIReqName"].ToString();
                }

                uTextEtcDesc.Text = dtEquip.Rows[0]["EtcDesc"].ToString();

                PlantCode = dtEquip.Rows[0]["PlantCode"].ToString();
                RepairGICode = dtEquip.Rows[0]["RepairGICode"].ToString();
                RepairReqCode = dtEquip.Rows[0]["RepairReqCode"].ToString();
                #endregion

                #region Detail Grid DropDown Binding

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsInventory);
                DataTable dtInventory = clsInventory.mfReadDurableInventoryCombo(PlantCode, m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(uGridDurableMatD, 0, "ChgDurableInventoryName", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInventory);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtclsDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_Lot(PlantCode, uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                //콤보 그리드 리스트 클러어
                this.uGridDurableMatD.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                wGrid.mfSetGridColumnValueGridList(this.uGridDurableMatD, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "DurableMatCode,DurableMatName,LotNo,InputQty", "치공구코드,치공구,LOTNO,수량", "DurableMatCode", "DurableMatName", dtclsDurableMat);

                #endregion

                #region Detail

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsEquipD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsEquipD);

                DataTable dtDetail = clsEquipD.mfReadDurableMatRepairD_Repair(PlantCode, uTextEquipCode.Text, RepairGICode, RepairReqCode, m_resSys.GetString("SYS_LANG"));

                uGridDurableMatD.Refresh();
                uGridDurableMatD.DataSource = dtDetail;
                uGridDurableMatD.DataBind();
                uGridDurableMatD_AfterDataBinding();

                #endregion

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                uGroupBoxContentsArea.Expanded = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uGridDurableMatD_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            

            ////try
            ////{
            ////    titleArea.Focus();
            ////    int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString()); 

            ////    #region CurDurableMatCode
            ////    if (e.Cell.Column.Key.ToString() == "CurDurableMatName")
            ////    {
            ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
            ////        QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
            ////        brwChannel.mfCredentials(clsDurableMat);

            ////        string strDurableMatCode = uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"].Value.ToString();
            ////        string strLang = m_resSys.GetString("SYS_LANG");
            ////        DataTable dtDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_Lot(PlantCode, uTextEquipCode.Text, strLang);

            ////        if (dtDurableMat.Rows.Count != 0)
            ////        {
            ////            DataRow[] drDurable = dtDurableMat.Select("DurableMatCode = '" + strDurableMatCode + "'");
            ////            uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = drDurable[0]["InputQty"];
            ////            uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = drDurable[0]["InputQty"];
                        
            ////            uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = drDurable[0]["LotNo"];
            ////            uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = drDurable[0]["DurableMatCode"];
            ////            e.Cell.Row.Cells["ChgDurableInventoryName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////            if (!drDurable[0]["LotNo"].ToString().Equals(string.Empty))
            ////            {
            ////                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ////            }
            ////            else
            ////            {
            ////                uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////            }

            ////            for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
            ////            {
            ////                if (!i.Equals(e.Cell.Row.Index) &&
            ////                    uGridDurableMatD.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(drDurable[0]["LotNo"].ToString()) &&
            ////                    !drDurable[0]["LotNo"].ToString().Equals(string.Empty))
            ////                {
            ////                    WinMessageBox msg = new WinMessageBox();
            ////                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "확인창", "확인창", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = string.Empty;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;
                                
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = string.Empty;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = string.Empty;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////                    e.Cell.Row.Cells["ChgDurableInventoryName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ////                    return;
            ////                }
            ////            }
            ////        }
            ////    }
            ////    #endregion

            ////    #region ChgDurableInventory
            ////    if (e.Cell.Column.Key.ToString().Equals("ChgDurableInventoryName"))
            ////    {
            ////        //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
            ////        string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryName"].Value.ToString();

            ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            ////        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
            ////        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
            ////        brwChannel.mfCredentials(clsDurableStock);

            ////        DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(PlantCode, _strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

            ////        string strCode = "";
            ////        if (e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
            ////        {
            ////            strCode = "LotNo = ''";
            ////        }
            ////        else
            ////        {
            ////            strCode = "LotNo <> ''";
            ////        }

            ////        DataRow[] dr = dtStock.Select(strCode);

            ////        DataTable dtBind = dtStock.Copy();
            ////        dtBind.Clear();

            ////        for (int i = 0; i < dr.Length; i++)
            ////        {
            ////            DataRow _dr = dtBind.NewRow();
            ////            for (int j = 0; j < _dr.ItemArray.Length; j++)
            ////            {
            ////                _dr[j] = dr[i][j];
            ////            }
            ////            dtBind.Rows.Add(_dr);
            ////        }
                    
            ////        //WinGrid wGrid = new WinGrid();
            ////        mfSetGridCellValueGridList(uGridDurableMatD, 0, e.Cell.Row.Index, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DataValue,
            ////            "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName", "치공구코드,치공구,LOTNO,수량,단위코드,단위", "DurableMatCode", "DurableMatName", dtBind);

            ////    }
            ////    #endregion

            ////    #region ChgDurableMatCode
            ////    if (e.Cell.Column.Key.ToString().Equals("ChgDurableMatName"))
            ////    {
            ////        //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
            ////        string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryName"].Value.ToString();
            ////        string _strDurableMatCode = e.Cell.Value.ToString();

            ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            ////        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
            ////        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
            ////        brwChannel.mfCredentials(clsDurableStock);

            ////        DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(PlantCode, _strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

            ////        if (!dtStock.Rows.Count.Equals(0))
            ////        {

            ////            DataRow[] dr = dtStock.Select("DurableMatCode = '" + _strDurableMatCode + "'");

            ////            e.Cell.Row.Cells["ChgLotNo"].Value = dr[0]["LotNo"].ToString();
            ////            e.Cell.Row.Cells["CurQty"].Tag = dr[0]["Qty"].ToString();
            ////            e.Cell.Row.Cells["CurQty"].Value = dr[0]["Qty"].ToString();
                        
            ////            e.Cell.Row.Cells["ChgDurableMatCode"].Value = dr[0]["DurableMatCode"];

            ////            if (!dr[0]["LotNo"].ToString().Equals(string.Empty))
            ////            {
            ////                e.Cell.Row.Cells["ChgQty"].Tag = dr[0]["Qty"].ToString();
            ////                e.Cell.Row.Cells["ChgQty"].Value = dr[0]["Qty"].ToString();
                            
            ////                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ////            }

            ////            for (int i = 0; i < uGridDurableMatD.Rows.Count; i++)
            ////            {
            ////                if (!i.Equals(e.Cell.Row.Index) &&
            ////                    uGridDurableMatD.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dr[0]["LotNo"].ToString()) &&
            ////                    !dr[0]["LotNo"].ToString().Equals(string.Empty))
            ////                {
            ////                    WinMessageBox msg = new WinMessageBox();
            ////                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "확인창", "확인창", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = 0;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Value = 0;
                                
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgLotNo"].Value = string.Empty;
            ////                    uGridDurableMatD.Rows[e.Cell.Row.Index].Cells["ChgDurableMatCode"].Value = string.Empty;
            ////                    e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////                    return;
            ////                }
            ////            }
            ////        }

            ////    }
            ////    #endregion

            ////}
            ////catch(Exception ex)
            ////{
            ////}
            ////finally
            ////{
            ////}
        }

        private void frmEQUZ0010_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
