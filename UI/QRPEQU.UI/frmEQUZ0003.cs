﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmEQUZ0003.cs                                        */
/* 프로그램명   : 설비변경등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;

using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0003 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQUZ0003()
        {
            InitializeComponent();
        }



        private void frmEQUZ0003_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
            //검색툴
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, true, false, m_rssSys.GetString("SYS_USERID"), this.Name);

        }
        private void frmEQUZ0003_Load(object sender, EventArgs e)
        {
            ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정 
            titleArea.mfSetLabelText("설비변경등록", m_rssSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();

            //컨트롤 초기화
            InitLable();
            InitGrid();
            InitGroupbox();
            InitButton();
            InitText();
            InitCombo();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        #region 컨트롤초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLable()
        {
            //Syste ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchCCSDate, "변경일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSysCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSysName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSuperSys, "Super설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLocation, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysGroupName, "설비그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSTSStock, "STS입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelYear, "제작년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysGrade, "설비등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelEquipCerti, "설비인증구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipCCSType, "설비변경점유형", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSetupDocNum, "Setup문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSubject, "목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelWorkGubun, "작업조", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelCCSChargeID, "변경담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelCCSDate, "변경일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCCSFinish, "변경완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInspectStandard, "검사기준", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInspectQty, "검사수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelResult, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEtcDesc, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel27, "변경진행사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 텍스트
        /// </summary>
        private void InitText()
        {
            try
            {
                //Syste ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                // 각해당 컨트롤에 삽입 //
                this.uTextCCSChargeID.Text = m_resSys.GetString("SYS_USERID");

                this.uTextCCSChargeName.Text = m_resSys.GetString("SYS_USERNAME");
 
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPBrowser brwChannel = new QRPBrowser();

                //-- 공장 콤보 --//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();
                

                // - 검색조건의 공장 코드 -//
                wCom.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                //-- Expand 안 공장콤보 --//
                wCom.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

                //-- 작업조 콤보 --//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtWork = clsCommonCode.mfReadCommonCode("C0025", m_resSys.GetString("SYS_LANG"));

                wCom.mfSetComboEditor(this.uComboWorkGubun, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtWork);

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
               
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSyes = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                // 설비변경점 헤더 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridEquipCCSH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.Yes, 0, false, m_resSyes.GetString("SYS_FONTNAME"));

                //설비변경점 헤더 컬럼설정 ///
                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSCode", "변경점코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSDate", "변경일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EquipCertiDocCode", "설비인증문서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 140, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EquipCertiDocName", "설비인증문서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 140, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EquipCCSTypeCode", "설비변경점유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 140, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EquipCCSTypeName", "설비변경점유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 140, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSFlag", "CCS대상", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 140, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSFlagName", "CCS대상", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 140, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1000
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "SetupDocNum", "Setup문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "WorkGubunCode", "작업조코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSFinishDate", "변경완료일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSChargeID", "변경담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "CCSChargeName", "변경담당자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "InspectStandard", "검사기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "InspectQty", "검사수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "Object", "목적", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSH, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridEquipCCSH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipCCSH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipCCSD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single,Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSyes.GetString("SYS_FONTNAME"));
               
                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipCCSD, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipCCSD, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "0");

                grd.mfSetGridColumn(this.uGridEquipCCSD, 0, "CCSDate", "일자", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");
                
                grd.mfSetGridColumn(this.uGridEquipCCSD, 0, "PreCCS", "변경전", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 800, false, false, 1000,Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSD, 0, "PostCCS", "변경후", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 800, false, false, 1000, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridEquipCCSD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipCCSD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                grd.mfAddRowGrid(this.uGridEquipCCSD, 0);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupbox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();

                grpbox.mfSetGroupBox(this.uGroupBox, GroupBoxType.INFO, "설비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox.HeaderAppearance.FontData.SizeInPoints = 9;
                
                grpbox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "설비인증정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바관련

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                // 검색조건에 있는 변경일의 시작일이 종료일 보다 크면 메세지박스를 보여준다.
                if (Convert.ToDateTime(this.uDateFromDate.Value) > Convert.ToDateTime(this.uDateToDate.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "시작일이 종료일보다 작아야합니다.", Infragistics.Win.HAlign.Right);

                    this.uDateFromDate.DropDown();
                    return;
                }

                //-- 설비변경 등록 검색 조건 정보 저장 
                string strPlantCode = this.uComboSearchPlant.Value.ToString();                      //공장
                string strEquipCode = "";                                                           //설비
                // 설비가 입력되어 있을 시 설비코드를 저장한다.
                if (this.uTextSearchEquipCode.Text != "" && this.uTextSearchEquipName.Text != "")
                {
                    strEquipCode = this.uTextSearchEquipCode.Text;

                }
                string strFromDate = Convert.ToDateTime(this.uDateFromDate.Value).ToString("yyyy-MM-dd");//변경일(시작일)
                string strToDate = Convert.ToDateTime(this.uDateToDate.Value).ToString("yyyy-MM-dd");//변경일(종료일)

                // 팝업창을 띄우고 커서를 변경한다.
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 설비변경등록 헤더 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSH), "EquipCCSH");
                QRPEQU.BL.EQUCCS.EquipCCSH clsEquipCCSH = new QRPEQU.BL.EQUCCS.EquipCCSH();
                brwChannel.mfCredentials(clsEquipCCSH);

                // 설비변경등록 헤더 조회 매서드 호출 
                DataTable dtEquipCCSH = clsEquipCCSH.mfReadEquipCCSH(strPlantCode, strEquipCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridEquipCCSH.DataSource = dtEquipCCSH;
                this.uGridEquipCCSH.DataBind();

                //커서를 기본값으로 변경 후 팝업창을 닫는다.
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtEquipCCSH.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.", Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }
                //----------------------- 필 수 입 력 사 항 확 인 ----------------------//
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    
                    //DropDown
                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextEquipCode.Text == "" || this.uTextEquipName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "설비코드를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    
                    // Focus 후 리턴
                    this.uTextEquipCode.Focus();
                    return;
                }
                else if (this.uComboProcess.Value.ToString() == "")
                {

                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "공정을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    //DropDown
                    this.uComboProcess.DropDown();
                    return;
                }
                else if (this.uComboEquipCCSType.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "설비변경점유형을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    // DropDown
                    this.uComboEquipCCSType.DropDown();
                    return;
                }
                else if (this.uTextCCSChargeID.Text == "" || this.uTextCCSChargeName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "변경담당자를 입력해주세요.", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextCCSChargeID.Focus();
                    return;
                }
                else if (this.uComboWorkGubun.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "작업조를 선택해주세요.", Infragistics.Win.HAlign.Right);

                    //DropDown
                    this.uComboWorkGubun.DropDown();
                    return;
                }

                //-------------- 필 수 입 력 사 항 끝 ----------------//

                
                QRPBrowser brwChannel = new QRPBrowser();

                // 설비변경등록 헤더 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSH), "EquipCCSH");
                QRPEQU.BL.EQUCCS.EquipCCSH clsEquipCCSH = new QRPEQU.BL.EQUCCS.EquipCCSH();
                brwChannel.mfCredentials(clsEquipCCSH);

                // 설비변경등록 헤더 저장에 필요한 컬럼 정보 매서드 호출 
                DataTable dtEquipCCSH = clsEquipCCSH.mfSetDataInfo();

                //설비변경등록 상세 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSD), "EquipCCSD");
                QRPEQU.BL.EQUCCS.EquipCCSD clsEquipCCSD = new QRPEQU.BL.EQUCCS.EquipCCSD();
                brwChannel.mfCredentials(clsEquipCCSH);
                //설비변경등록 상세 저장에 필요한 컬럼 정보 매서드 호출
                DataTable dtEquipCCSD = clsEquipCCSD.mfSetDateaInfo();

                // 헤더 저장 
                DataRow drHeader;
                drHeader = dtEquipCCSH.NewRow();
                drHeader["PlantCode"] = this.uComboPlant.Value.ToString();                                  //공장
                drHeader["CCSCode"] = this.uTextCCSCode.Text;                                               //변경점코드
                drHeader["EquipCode"] = this.uTextEquipCode.Text;                                           //설비코드
                drHeader["CCSDate"] = Convert.ToDateTime(this.uDateCCSDate.Value).ToString("yyyy-MM-dd");   //변경일
                drHeader["ProcessCode"] = this.uComboProcess.Value.ToString();                              //공정
                drHeader["EquipCCSTypeCode"] = this.uComboEquipCCSType.Value.ToString();                    //설비변경점유형

                if (this.uTextCCSFlag.Text == "")
                    drHeader["CCSFlag"] = false;                                           //CCS대상여부
                else
                    drHeader["CCSFlag"] = this.uTextCCSFlag.Text;  

                drHeader["SetupDocNum"] = this.uTextSetupDocNum.Text;                                       //검사수량
                drHeader["WorkGubunCode"] = this.uComboWorkGubun.Value.ToString();                          //작업조
                drHeader["CCSFinishDate"] = Convert.ToDateTime(this.uDateCCSFinishDate.Value).ToString("yyyy-MM-dd");//변경완료일
                drHeader["CCSChargeID"] = this.uTextCCSChargeID.Text;                                       //변경담당자
                drHeader["InspectStandard"] = this.uTextInspectStandard.Text;                               //검사기준
                drHeader["InspectResult"] = this.uTextInspectResult.Text;                                   //검사결과
                drHeader["InspectQty"] = this.uTextInspectQty.Value.ToString();                             //검사수량
                drHeader["Object"] = this.uTextObject.Text;                                                 //목적
                drHeader["EtcDesc"] = this.uTextEtcDesc.Text;                                               //특이사항
                drHeader["EquipCertiDocCode"] = "";                                                         //설비인증문서코드
                dtEquipCCSH.Rows.Add(drHeader);

                //  설비 변경점 정보 상세 정보 저장 
                if (this.uGridEquipCCSD.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridEquipCCSD.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridEquipCCSD.ActiveCell = this.uGridEquipCCSD.Rows[0].Cells[0];

                        if (this.uGridEquipCCSD.Rows[i].Hidden == false)
                        {
                            DataRow drDetail;
                            drDetail = dtEquipCCSD.NewRow();
                            drDetail["Seq"]= this.uGridEquipCCSD.Rows[i].RowSelectorNumber;                     //순번
                            drDetail["CCSDate"] = this.uGridEquipCCSD.Rows[i].Cells["CCSDate"].Value.ToString();//변경일
                            drDetail["PreCCS"] = this.uGridEquipCCSD.Rows[i].Cells["PreCCS"].Value.ToString();  //변경전
                            drDetail["PostCCS"] = this.uGridEquipCCSD.Rows[i].Cells["PostCCS"].Value.ToString();//변경후
                            dtEquipCCSD.Rows.Add(drDetail);
                        }
                    }
                }
                // 저장 여부 메세지 박스를 띄운다 
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // Yes를 눌렀을 때 팝업창이 뜨고 커서가 변경된다 ..
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 설비변경점정보 저장 매서드 호출
                    string strErrRtn = clsEquipCCSH.mfSaveEquipCCSH(dtEquipCCSH, dtEquipCCSD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //  Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    // 커서가 기본값으로 변경되고 팝업창이 닫힌다 
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    // 처리결과에 따라 메세지 박스를 띄운다 //
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }

        }
        
        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextCCSCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                // 공장 , 변경점코드 저장
                string strPlantcode = this.uComboPlant.Value.ToString();
                string strCCSCode = this.uTextCCSCode.Text;

                //   삭제 여부 메세지 박스를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // Yes 누를 시 팝업창이 뜨고 커서가 바뀐다.
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //설비변경등록 BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSH), "EquipCCSH");
                    QRPEQU.BL.EQUCCS.EquipCCSH clsEquipCCSH = new QRPEQU.BL.EQUCCS.EquipCCSH();
                    brwChannel.mfCredentials(clsEquipCCSH);
                    //설비변경등록 삭제 매서드 호출
                    string strErrRtn = clsEquipCCSH.mfDeleteEquipCCSH(strPlantcode, strCCSCode);

                    //  Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //커서를 기본값으로 변경뒤 팝업창을 닫는다.
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    //처리결과에 따라서 메세지박스를 띄운다.
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "출력중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipCCSH.Rows.Count == 0 && (this.uGridEquipCCSD.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "엑셀출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }


                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridEquipCCSH.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipCCSH);

                if (this.uGridEquipCCSD.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridEquipCCSD);

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }

                InitExpandControl();

                //공정 설비변경점유형 초기화
                this.uComboEquipCCSType.Value = "";
                this.uComboProcess.Value = "";

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                if (this.uComboPlant.ReadOnly == true)
                    this.uComboPlant.ReadOnly = false;

                if (this.uTextEquipCode.ReadOnly == true)
                    this.uTextEquipCode.ReadOnly = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        public void mfExcel()
        {
            try
            {
                //SystmeResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipCCSH.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀출력정보가 없습니다", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridEquipCCSH);

                if (this.uGridEquipCCSD.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    grd.mfDownLoadGridToExcel(this.uGridEquipCCSD);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        //ExpandGroupBox 펼침숨김상태
        private void uGroupContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipCCSH.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipCCSH.Height = 740;

                    for (int i = 0; i < uGridEquipCCSH.Rows.Count; i++)
                    {
                        uGridEquipCCSH.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 콤보
        // 공장의 정보가 바뀔 때 마다 영향받는 콤보들이 바뀐다. //
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (!this.uTextEquipName.Text.Equals(string.Empty))
                {
                    InitExpandControl();
                }
                else
                {
                    InitText();
                }

                this.uComboProcess.Items.Clear();
                this.uComboEquipCCSType.Items.Clear();

                // 공정 정보 콤보 //
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                string strPlantCode = this.uComboPlant.Value.ToString();

                DataTable dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboProcess, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ProcessCode", "ProcessName", dtProcess);

                //설비변경점유형 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCCSType), "EquipCCSType");
                QRPMAS.BL.MASEQU.EquipCCSType clsEquipCCSType = new QRPMAS.BL.MASEQU.EquipCCSType();
                brwChannel.mfCredentials(clsEquipCCSType);
               
                //설비변경점유형 콤보조회 매서드호출
                DataTable dtEquipCCSType = clsEquipCCSType.mfReadEquipCCSTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCom.mfSetComboEditor(this.uComboEquipCCSType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "EquipCCSTypeCode", "EquipCCSTypeName", dtEquipCCSType);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비변경점유형의 정보가 바뀔 때마다 해당CCS대상여부가 나타난다.
        private void uComboEquipCCSType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                

                // 설비변경점유형이 공백이 아닐 떄 조회를 한다
                if (this.uComboEquipCCSType.Value.ToString() != "")
                {

                    // 공장콤보 값이 공백일 경우 메세지 박스를 띄운다.
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //공장 콤보DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    // 공장, 설비변경점유형 저장
                    QRPBrowser brwChannel = new QRPBrowser();
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strEquipCCSType = this.uComboEquipCCSType.Value.ToString();
                    //설비변경점유형 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCCSType), "EquipCCSType");
                    QRPMAS.BL.MASEQU.EquipCCSType clsEquipCCSType = new QRPMAS.BL.MASEQU.EquipCCSType();
                    brwChannel.mfCredentials(clsEquipCCSType);
                    //설비변경점유형 CCSFlag조회 매서드 호출
                    DataTable dtEquipCCSFlag = clsEquipCCSType.mfReadEquipCCSTypeCCSFlag(strPlantCode, strEquipCCSType);

                    // 정보가 있다면 텍스트에 삽입한다.
                    if (dtEquipCCSFlag.Rows.Count > 0)
                    {
                        this.uTextCCSFlag.Text = dtEquipCCSFlag.Rows[0]["CCSFlag"].ToString();
                        this.uTextCCSFlagName.Text = dtEquipCCSFlag.Rows[0]["CCSFlagName"].ToString();
                    }
                    else
                    {
                        this.uTextCCSFlag.Text = "";
                        this.uTextCCSFlagName.Text = "";
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region TextBox
        //검사수량 왼쪽 에디터버튼 클릭시 검사수량이 초기값(0)으로 돌아간다.
        private void uTextInspectQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 현재 년도로 초기화
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //검사수량 오른쪽 스핀버튼 클릭 시 수량이 증가 하거나 감소한다. //
        private void uTextInspectQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //--검색조건에 있는 설비코드 텍스트 에디터버튼을 클릭하였을 때 설비정보 팝업창을 띄운다 ..
        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                

                string strPlantCode = "";

                if (this.uComboSearchPlant.Value.ToString() != "")
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                frmPOP0005 frmEquip = new frmPOP0005();
                //공장정보를 보낸다.
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                // 선택한 정보 각 컨트롤에 삽입 //
                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextSearchEquipName.Text = frmEquip.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //변경담당자텍스트 엔터키를 누를 시 유저정보를 조회한다.
        private void uTextCCSChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextCCSChargeName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //교체점검자 공장코드 저장
                    string strCCSChargeID = this.uTextCCSChargeID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //----- 아이디 공백 확인
                    if (strCCSChargeID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCCSChargeID.Focus();
                        return;
                    }
                    //-- 공장 확인
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    //-- 유저정보 가져오기 
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCCSChargeID, m_resSys.GetString("SYS_LANG"));
                    if (dtUser.Rows.Count > 0)
                    {
                        // 이름 텍스트에 유저 이름을 삽입한다.
                        this.uTextCCSChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "입력확인", "입력하신 ID의 유저정보가 없습니다. 다시 확인해주세요.", Infragistics.Win.HAlign.Right);
                        this.uTextCCSChargeName.Clear();
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //변경담당자텍스트 에디터버튼을 클릭 시 유저정보창을 띄운다.
        private void uTextCCSChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //-- 공장 이 공백일 경우 메세지박스를 띄운다. --//
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    //공장 콤보DropDown
                    this.uComboPlant.DropDown();
                    return;
                }
                //-- 공장정보 저장 --//
                string strPlantCode = this.uComboPlant.Value.ToString();

                frmPOP0011 frmUser = new frmPOP0011();
                //공장정보 상속된폼에 정보보냄
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                //--각 해당컨트롤에 아이디 명 을 삽입시킨다.
                this.uTextCCSChargeID.Text = frmUser.UserID;
                this.uTextCCSChargeName.Text = frmUser.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //ExpandGroupBox 안의 설비코드 텍스트 에디터버튼을 클릭시 설비정보창을 띄운다 //
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlant.ReadOnly == false)
                {
                    // 공장콤보 값이 공백일 경우 메세지 박스를 띄운다.
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //공장 콤보DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    // 공장 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    frmPOP0005 frmEquip = new frmPOP0005();
                    //공장정보를 보낸다.
                    frmEquip.PlantCode = strPlantCode;
                    frmEquip.ShowDialog();

                    
                    // 설비코드 저장
                    string strEquipCode = frmEquip.EquipCode;

                    if (!strEquipCode.Equals(string.Empty))
                    {
                        QRPBrowser brwChannel = new QRPBrowser();

                        // 설비변경등록 헤더 BL호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSH), "EquipCCSH");
                        QRPEQU.BL.EQUCCS.EquipCCSH clsEquipCCSH = new QRPEQU.BL.EQUCCS.EquipCCSH();
                        brwChannel.mfCredentials(clsEquipCCSH);

                        // 설비변경등록 헤더조회 매서드 호출
                        DataTable dtEquipCCSH = clsEquipCCSH.mfReadEquipCCSH(strPlantCode, strEquipCode, "", "", m_resSys.GetString("SYS_LANG"));

                        //정보가 있을경우 각 해당 컨트롤에 정보를 삽입한다.
                        if (dtEquipCCSH.Rows.Count > 0)
                        {
                            this.uComboProcess.Value = dtEquipCCSH.Rows[0]["ProcessCode"].ToString();
                            this.uComboEquipCCSType.Value = dtEquipCCSH.Rows[0]["EquipCCSTypeCode"].ToString();
                            this.uTextCCSFlagName.Text = dtEquipCCSH.Rows[0]["CCSFlagName"].ToString();
                            this.uDateCCSDate.Value = dtEquipCCSH.Rows[0]["CCSDate"].ToString();
                            this.uTextSetupDocNum.Text = dtEquipCCSH.Rows[0]["SetupDocNum"].ToString();
                            this.uComboWorkGubun.Value = dtEquipCCSH.Rows[0]["WorkGubunCode"].ToString();
                            this.uDateCCSFinishDate.Value = dtEquipCCSH.Rows[0]["CCSFinishDate"].ToString();
                            this.uTextCCSChargeID.Text = dtEquipCCSH.Rows[0]["CCSChargeID"].ToString();
                            this.uTextCCSChargeName.Text = dtEquipCCSH.Rows[0]["CCSChargeName"].ToString();
                            this.uTextInspectQty.Value = dtEquipCCSH.Rows[0]["InspectQty"].ToString();
                            this.uTextInspectStandard.Text = dtEquipCCSH.Rows[0]["InspectStandard"].ToString();
                            this.uTextInspectResult.Text = dtEquipCCSH.Rows[0]["InspectResult"].ToString();
                            this.uTextObject.Text = dtEquipCCSH.Rows[0]["Object"].ToString();
                            this.uTextEtcDesc.Text = dtEquipCCSH.Rows[0]["EtcDesc"].ToString();
                            // 변경점 코드 저장
                            string strCCSCode = dtEquipCCSH.Rows[0]["CCSCode"].ToString();
                            //숨겨져있는 컨트롤
                            this.uTextCCSFlag.Text = dtEquipCCSH.Rows[0]["CCSFlag"].ToString();
                            this.uTextCCSCode.Text = strCCSCode;

                            //설비변경등록 상세 BL 호출
                            brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSD), "EquipCCSD");
                            QRPEQU.BL.EQUCCS.EquipCCSD clsEquipCCSD = new QRPEQU.BL.EQUCCS.EquipCCSD();
                            brwChannel.mfCredentials(clsEquipCCSD);

                            //설비변경등록 상세 조회 매서드 호출
                            DataTable dtEquipCCSD = clsEquipCCSD.mfReadEquipCCSD(strPlantCode, strCCSCode, m_resSys.GetString("SYS_LANG"));

                            //그리드에 바인드
                            this.uGridEquipCCSD.DataSource = dtEquipCCSD;
                            this.uGridEquipCCSD.DataBind();

                        }
                        // 정보가 없는데 변경점코드가 남아있다면 헤더,상세 컨트롤 초기화 //
                        else
                        {
                            if (this.uTextCCSCode.Text != "")
                            {
                                InitExpandControl();
                                this.uComboProcess.Value = "";
                                this.uComboEquipCCSType.Value = "";

                            }
                        }


                        // 팝업창에서 선택한 설비의 정보를 각 해당 컨트롤에 삽입한다.
                        this.uTextEquipCode.Text = frmEquip.EquipCode;
                        this.uTextArea.Text = frmEquip.AreaName;
                        this.uTextEquipGroupName.Text = frmEquip.EquipGroupName;
                        this.uTextEquipLevel.Text = frmEquip.EquipLevelCode;
                        this.uTextLocation.Text = frmEquip.EquipLocName;
                        this.uTextEquipProcess.Text = frmEquip.EquipProcGubunName;
                        this.uTextEquipType.Text = frmEquip.EquipTypeName;
                        this.uTextMakerYear.Text = frmEquip.MakeYear;
                        this.uTextModel.Text = frmEquip.ModelName;
                        this.uTextSerialNo.Text = frmEquip.SerialNo;
                        this.uTextStation.Text = frmEquip.StationName;
                        this.uTextSTSStock.Text = frmEquip.GRDate;
                        this.uTextSuperEquip.Text = frmEquip.SuperEquipCode;
                        this.uTextEquipName.Text = frmEquip.EquipName;
                        this.uTextVendor.Text = frmEquip.VendorName;


                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //ExpandGroupBox안의 설비코드 키를 누를 경우 생김
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                #region 백스페이스orDelete
                if (this.uTextEquipCode.ReadOnly.Equals(false) && (e.KeyData == Keys.Delete || e.KeyData == Keys.Back))
                {
                    if (!this.uTextEquipName.Text.Equals(string.Empty))
                    {
                        this.uTextEquipCode.Text = "";      //설비코드
                        this.uTextEquipName.Text = "";      //설비명
                        this.uTextSuperEquip.Text = "";     //Super설비
                        this.uTextStation.Text = "";        //Station
                        this.uTextArea.Text = "";           //Area
                        this.uTextEquipProcess.Text = "";   //공정구분
                        this.uTextLocation.Text = "";       //위치
                        this.uTextModel.Text = "";          //모델
                        this.uTextEquipType.Text = "";      //설비유형
                        this.uTextSerialNo.Text = "";       //SerialNo
                        this.uTextVendor.Text = "";         //거래처
                        this.uTextEquipGroupName.Text = ""; //그룹명
                        this.uTextSTSStock.Text = "";       //입고일
                        this.uTextMakerYear.Text = "";      //제작년도
                        this.uTextEquipLevel.Text = "";     //설비등급

                        this.uComboProcess.Value = "";
                        this.uComboEquipCCSType.Value = "";

                        this.uDateCCSDate.Value = DateTime.Now;
                        this.uDateCCSFinishDate.Value = DateTime.Now;

                        this.uTextCCSFlagName.Text = "";

                        this.uTextEtcDesc.Text = "";
                        this.uTextInspectQty.Value = 0;
                        this.uTextInspectResult.Text = "";
                        this.uTextInspectStandard.Text = "";
                        this.uTextObject.Text = "";
                        this.uTextSetupDocNum.Text = "";
                        this.uComboWorkGubun.Value = "";

                        this.uTextCCSFlag.Text = "";
                        this.uTextCCSCode.Text = "";

                        InitText();
                        //-- 전체 행을 선택 하여 삭제한다 --//
                        if (this.uGridEquipCCSD.Rows.Count > 0)
                        {
                            this.uGridEquipCCSD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipCCSD.Rows.All);
                            this.uGridEquipCCSD.DeleteSelectedRows(false);
                        }
                    }

                }
                #endregion


                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                #region Enter
                //엔터키를 누를시
                if (e.KeyData.Equals(Keys.Enter))
                {
                    if (!this.uTextEquipCode.Text.Equals(string.Empty))
                    {
                        //공장정보 체크
                        if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공장정보확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                            //DropDown
                            this.uComboPlant.DropDown();
                            return;
                        }

                        //공장,설비 정보 저장
                        string strPlant = this.uComboPlant.Value.ToString();
                        string strEquip = this.uTextEquipCode.Text;

                        QRPBrowser brwChannel = new QRPBrowser();
                        //설비정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEquip);
                        //설비정보 조회 매서드호출
                        DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlant, strEquip, m_resSys.GetString("SYS_LANG"));

                        //정보가 있는경우
                        if (dtEquip.Rows.Count > 0)
                        {
                            this.uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();             //설비코드
                            this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();             //설비명
                            this.uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();       //Super설비
                            this.uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();             //Station
                            this.uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();                   //Area
                            this.uTextEquipProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString(); //공정구분
                            this.uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();           //위치
                            this.uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();                 //모델
                            this.uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();         //설비유형
                            this.uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();               //SerialNo
                            this.uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();               //거래처
                            this.uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();   //그룹명
                            this.uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();                 //입고일
                            this.uTextMakerYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();              //제작년도
                            this.uTextEquipLevel.Text = dtEquip.Rows[0]["EquipLevelCode"].ToString();       //설비등급
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "조회처리결과 확인", "입력하신 설비코드를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);

                            if (!this.uTextEquipName.Text.Equals(string.Empty))
                            {
                                this.uTextEquipName.Text = "";      //설비명
                                this.uTextSuperEquip.Text = "";     //Super설비
                                this.uTextStation.Text = "";        //Station
                                this.uTextArea.Text = "";           //Area
                                this.uTextEquipProcess.Text = "";   //공정구분
                                this.uTextLocation.Text = "";       //위치
                                this.uTextModel.Text = "";          //모델
                                this.uTextEquipType.Text = "";      //설비유형
                                this.uTextSerialNo.Text = "";       //SerialNo
                                this.uTextVendor.Text = "";         //거래처
                                this.uTextEquipGroupName.Text = ""; //그룹명
                                this.uTextSTSStock.Text = "";       //입고일
                                this.uTextMakerYear.Text = "";      //제작년도
                                this.uTextEquipLevel.Text = "";     //설비등급
                            }

                            return;
                        }


                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //SearchArea에 있는 설비코드를 키를 누를 경우
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
            //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //Delete키나 Backspace키를 누르면 설비명 공백처리
                if (e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back))
                {
                    this.uTextSearchEquipName.Clear();
                }

                //Enter키를 누를 시 해당설비의 설비명을 가져온다.
                if (e.KeyData.Equals(Keys.Enter) && !this.uTextSearchEquipCode.Text.Equals(string.Empty))
                {
                    //공장정보가 없는 경우
                    if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "공장정보확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //공장 설비 정보 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strEquipCode = this.uTextSearchEquipCode.Text;

                    //설비정보BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보 상세조회매서드 호출
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //정보가 있으면 설비명을 보여준다.
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "조회결과 확인", "입력하신 설비의 정보를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Grid

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridEquipCCSD_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridEquipCCSD, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
            
        }

        private void uGridEquipCCSD_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        }

        //헤더 그리드를 더블클릭 시 해당 줄의 정보가 ExpandGroupBox 안에 상세히 나타난다.
        private void uGridEquipCCSH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                WinGrid grd = new WinGrid();

                if (grd.mfCheckCellDataInRow(this.uGridEquipCCSD, 0, e.Cell.Row.Index) == false)
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //ExpandGroupBox 숨김상태라면 펼친다 .
                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                    }
                    e.Cell.Row.Fixed = true;

                    // 공장 설비 저장
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    this.uComboPlant.Value = strPlantCode;

                    this.uComboPlant.ReadOnly = true;
                    this.uTextEquipCode.ReadOnly = true;
                    //-- 설비정보 조회 --//
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    
                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //정보가 있을경우 텍스트박스에 삽입
                    if (dtEquip.Rows.Count > 0)
                    {
                        
                        this.uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();   //Super설비
                        this.uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();         //Station
                        this.uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();               //Area
                        this.uTextEquipProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString(); //공정구분
                        this.uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();       //위치
                        this.uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();             //모델
                        this.uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();     //설비유형
                        this.uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();           //SerialNo
                        this.uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();           //거래처
                        this.uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();//그룹명
                        this.uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();             //입고일
                        this.uTextMakerYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();           //제작년도
                        this.uTextEquipLevel.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();     //설비등급
                    }

                    
                    this.uTextEquipCode.Text = strEquipCode;
                    this.uTextEquipName.Text = e.Cell.Row.Cells["EquipName"].Value.ToString();
                    this.uComboProcess.Value = e.Cell.Row.Cells["ProcessCode"].Value.ToString();
                    this.uComboEquipCCSType.Value = e.Cell.Row.Cells["EquipCCSTypeCode"].Value.ToString();
                    this.uTextCCSFlagName.Text = e.Cell.Row.Cells["CCSFlagName"].Value.ToString();
                    this.uDateCCSDate.Value = e.Cell.Row.Cells["CCSDate"].Value.ToString();
                    this.uTextSetupDocNum.Text = e.Cell.Row.Cells["SetupDocNum"].Value.ToString();
                    this.uComboWorkGubun.Value = e.Cell.Row.Cells["WorkGubunCode"].Value.ToString();
                    this.uDateCCSFinishDate.Value = e.Cell.Row.Cells["CCSFinishDate"].Value.ToString();
                    this.uTextCCSChargeID.Text = e.Cell.Row.Cells["CCSChargeID"].Value.ToString();
                    this.uTextCCSChargeName.Text = e.Cell.Row.Cells["CCSChargeName"].Value.ToString();
                    this.uTextInspectQty.Value = e.Cell.Row.Cells["InspectQty"].Value.ToString();
                    this.uTextInspectStandard.Text = e.Cell.Row.Cells["InspectStandard"].Value.ToString();
                    this.uTextInspectResult.Text = e.Cell.Row.Cells["InspectResult"].Value.ToString();
                    this.uTextObject.Text = e.Cell.Row.Cells["Object"].Value.ToString();
                    this.uTextEtcDesc.Text = e.Cell.Row.Cells["EtcDesc"].Value.ToString();
                    // 변경점 코드 저장
                    string strCCSCode = e.Cell.Row.Cells["CCSCode"].Value.ToString();
                    //숨겨져있는 컨트롤
                    this.uTextCCSFlag.Text = e.Cell.Row.Cells["CCSFlag"].Value.ToString();
                    this.uTextCCSCode.Text = strCCSCode;

                    //설비변경등록 상세 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipCCSD), "EquipCCSD");
                    QRPEQU.BL.EQUCCS.EquipCCSD clsEquipCCSD = new QRPEQU.BL.EQUCCS.EquipCCSD();
                    brwChannel.mfCredentials(clsEquipCCSD);

                    //설비변경등록 상세 조회 매서드 호출
                    DataTable dtEquipCCSD = clsEquipCCSD.mfReadEquipCCSD(strPlantCode, strCCSCode, m_resSys.GetString("SYS_LANG"));

                    //그리드에 바인드
                    this.uGridEquipCCSD.DataSource = dtEquipCCSD;
                    this.uGridEquipCCSD.DataBind();

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        #endregion


        #endregion

        /// <summary>
        /// ExpandGroupBox안 컨트롤 값 초기화
        /// </summary>
        private void InitExpandControl()
        {
            try
            {
                this.uTextEquipCode.Text = "";      //설비코드
                this.uTextEquipName.Text = "";      //설비명
                this.uTextSuperEquip.Text = "";     //Super설비
                this.uTextStation.Text = "";        //Station
                this.uTextArea.Text = "";           //Area
                this.uTextEquipProcess.Text = "";   //공정구분
                this.uTextLocation.Text = "";       //위치
                this.uTextModel.Text = "";          //모델
                this.uTextEquipType.Text = "";      //설비유형
                this.uTextSerialNo.Text = "";       //SerialNo
                this.uTextVendor.Text = "";         //거래처
                this.uTextEquipGroupName.Text = ""; //그룹명
                this.uTextSTSStock.Text = "";       //입고일
                this.uTextMakerYear.Text = "";      //제작년도
                this.uTextEquipLevel.Text = "";     //설비등급

                this.uDateCCSDate.Value = DateTime.Now;
                this.uDateCCSFinishDate.Value = DateTime.Now;

                this.uTextCCSFlagName.Text = "";
                
                this.uTextEtcDesc.Text = "";
                this.uTextInspectQty.Value = 0;
                this.uTextInspectResult.Text = "";
                this.uTextInspectStandard.Text = "";
                this.uTextObject.Text = "";
                this.uTextSetupDocNum.Text = "";
                this.uComboWorkGubun.Value = "";

                this.uTextCCSFlag.Text = "";
                this.uTextCCSCode.Text = "";

                InitText();
                //-- 전체 행을 선택 하여 삭제한다 --//
                if (this.uGridEquipCCSD.Rows.Count > 0)
                {
                    this.uGridEquipCCSD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipCCSD.Rows.All);
                    this.uGridEquipCCSD.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }


        private void frmEQUZ0003_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }




    }

}
