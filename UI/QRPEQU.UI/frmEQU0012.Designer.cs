﻿namespace QRPEQU.UI
{
    partial class frmEQU0012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0012));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextCarryOutNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridCarryOutList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextCarryOutAdmitID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCarryOutAdmitName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCarryOutAdmit = new Infragistics.Win.Misc.UltraLabel();
            this.uComboCarryOutUser = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uLabelCarryOut = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCarryOutReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCarryOutReason = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCreateDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateCarryInEstDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCarryInEstDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboCarryOut = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboECInGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelCOutChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelECInGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCarryOutEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCarryOutList = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCarryOutEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboECOutGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ulabelECOutGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUserDept = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUserDept = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCOutChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCOutChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCOutCharge = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryOutList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutAdmitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutAdmitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCarryOutUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCarryInEstDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCarryOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboECInGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboECOutGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCOutChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCOutChargeName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextCarryOutNum);
            this.uGroupBox.Controls.Add(this.uComboPlant);
            this.uGroupBox.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox.Controls.Add(this.uGridCarryOutList);
            this.uGroupBox.Controls.Add(this.uTextCarryOutAdmitID);
            this.uGroupBox.Controls.Add(this.uTextCarryOutAdmitName);
            this.uGroupBox.Controls.Add(this.uLabelCarryOutAdmit);
            this.uGroupBox.Controls.Add(this.uComboCarryOutUser);
            this.uGroupBox.Controls.Add(this.uLabelCarryOut);
            this.uGroupBox.Controls.Add(this.uTextCarryOutReason);
            this.uGroupBox.Controls.Add(this.uLabelCarryOutReason);
            this.uGroupBox.Controls.Add(this.uDateCreateDate);
            this.uGroupBox.Controls.Add(this.uDateCarryInEstDate);
            this.uGroupBox.Controls.Add(this.uLabelCarryInEstDate);
            this.uGroupBox.Controls.Add(this.uComboCarryOut);
            this.uGroupBox.Controls.Add(this.uComboECInGubun);
            this.uGroupBox.Controls.Add(this.uLabelCOutChargeID);
            this.uGroupBox.Controls.Add(this.uLabelECInGubun);
            this.uGroupBox.Controls.Add(this.uTextCarryOutEtcDesc);
            this.uGroupBox.Controls.Add(this.uLabelCarryOutList);
            this.uGroupBox.Controls.Add(this.uLabelCarryOutEtcDesc);
            this.uGroupBox.Controls.Add(this.uComboECOutGubun);
            this.uGroupBox.Controls.Add(this.ulabelECOutGubun);
            this.uGroupBox.Controls.Add(this.uTextUserDept);
            this.uGroupBox.Controls.Add(this.uLabelUserDept);
            this.uGroupBox.Controls.Add(this.uTextCOutChargeID);
            this.uGroupBox.Controls.Add(this.uTextCOutChargeName);
            this.uGroupBox.Controls.Add(this.uLabelCOutCharge);
            this.uGroupBox.Controls.Add(this.uLabelPlant);
            this.uGroupBox.Controls.Add(this.uLabelCreateDate);
            this.uGroupBox.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 796);
            this.uGroupBox.TabIndex = 4;
            // 
            // uTextCarryOutNum
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryOutNum.Appearance = appearance5;
            this.uTextCarryOutNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryOutNum.Location = new System.Drawing.Point(896, 100);
            this.uTextCarryOutNum.MaxLength = 13;
            this.uTextCarryOutNum.Name = "uTextCarryOutNum";
            this.uTextCarryOutNum.ReadOnly = true;
            this.uTextCarryOutNum.Size = new System.Drawing.Size(120, 21);
            this.uTextCarryOutNum.TabIndex = 38;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(116, 192);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 35;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridCarryOutList
            // 
            this.uGridCarryOutList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCarryOutList.DisplayLayout.Appearance = appearance6;
            this.uGridCarryOutList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCarryOutList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutList.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryOutList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridCarryOutList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryOutList.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridCarryOutList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCarryOutList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCarryOutList.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCarryOutList.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridCarryOutList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCarryOutList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutList.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCarryOutList.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridCarryOutList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCarryOutList.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutList.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGridCarryOutList.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridCarryOutList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCarryOutList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGridCarryOutList.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGridCarryOutList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCarryOutList.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.uGridCarryOutList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCarryOutList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCarryOutList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCarryOutList.Location = new System.Drawing.Point(12, 200);
            this.uGridCarryOutList.Name = "uGridCarryOutList";
            this.uGridCarryOutList.Size = new System.Drawing.Size(1044, 584);
            this.uGridCarryOutList.TabIndex = 36;
            this.uGridCarryOutList.Text = "ultraGrid1";
            this.uGridCarryOutList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutList_AfterCellUpdate);
            this.uGridCarryOutList.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutList_CellListSelect);
            this.uGridCarryOutList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutList_ClickCellButton);
            this.uGridCarryOutList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutList_CellChange);
            // 
            // uTextCarryOutAdmitID
            // 
            appearance32.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCarryOutAdmitID.Appearance = appearance32;
            this.uTextCarryOutAdmitID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCarryOutAdmitID.ButtonsRight.Add(editorButton1);
            this.uTextCarryOutAdmitID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCarryOutAdmitID.Location = new System.Drawing.Point(444, 100);
            this.uTextCarryOutAdmitID.MaxLength = 20;
            this.uTextCarryOutAdmitID.Name = "uTextCarryOutAdmitID";
            this.uTextCarryOutAdmitID.Size = new System.Drawing.Size(100, 21);
            this.uTextCarryOutAdmitID.TabIndex = 9;
            this.uTextCarryOutAdmitID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCarryOutAdmitID_KeyDown);
            this.uTextCarryOutAdmitID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCarryOutAdmitID_EditorButtonClick);
            // 
            // uTextCarryOutAdmitName
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryOutAdmitName.Appearance = appearance1;
            this.uTextCarryOutAdmitName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryOutAdmitName.Location = new System.Drawing.Point(548, 100);
            this.uTextCarryOutAdmitName.Name = "uTextCarryOutAdmitName";
            this.uTextCarryOutAdmitName.ReadOnly = true;
            this.uTextCarryOutAdmitName.Size = new System.Drawing.Size(100, 21);
            this.uTextCarryOutAdmitName.TabIndex = 33;
            // 
            // uLabelCarryOutAdmit
            // 
            this.uLabelCarryOutAdmit.Location = new System.Drawing.Point(340, 100);
            this.uLabelCarryOutAdmit.Name = "uLabelCarryOutAdmit";
            this.uLabelCarryOutAdmit.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryOutAdmit.TabIndex = 31;
            this.uLabelCarryOutAdmit.Text = "ultraLabel2";
            // 
            // uComboCarryOutUser
            // 
            this.uComboCarryOutUser.CheckedListSettings.CheckStateMember = "";
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboCarryOutUser.DisplayLayout.Appearance = appearance34;
            this.uComboCarryOutUser.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboCarryOutUser.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboCarryOutUser.DisplayLayout.GroupByBox.Appearance = appearance35;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboCarryOutUser.DisplayLayout.GroupByBox.BandLabelAppearance = appearance36;
            this.uComboCarryOutUser.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance37.BackColor2 = System.Drawing.SystemColors.Control;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboCarryOutUser.DisplayLayout.GroupByBox.PromptAppearance = appearance37;
            this.uComboCarryOutUser.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboCarryOutUser.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboCarryOutUser.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboCarryOutUser.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.uComboCarryOutUser.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboCarryOutUser.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            this.uComboCarryOutUser.DisplayLayout.Override.CardAreaAppearance = appearance40;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            appearance41.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboCarryOutUser.DisplayLayout.Override.CellAppearance = appearance41;
            this.uComboCarryOutUser.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboCarryOutUser.DisplayLayout.Override.CellPadding = 0;
            appearance42.BackColor = System.Drawing.SystemColors.Control;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboCarryOutUser.DisplayLayout.Override.GroupByRowAppearance = appearance42;
            appearance43.TextHAlignAsString = "Left";
            this.uComboCarryOutUser.DisplayLayout.Override.HeaderAppearance = appearance43;
            this.uComboCarryOutUser.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboCarryOutUser.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            this.uComboCarryOutUser.DisplayLayout.Override.RowAppearance = appearance44;
            this.uComboCarryOutUser.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboCarryOutUser.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.uComboCarryOutUser.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboCarryOutUser.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboCarryOutUser.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboCarryOutUser.Location = new System.Drawing.Point(792, 100);
            this.uComboCarryOutUser.MaxLength = 20;
            this.uComboCarryOutUser.Name = "uComboCarryOutUser";
            this.uComboCarryOutUser.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboCarryOutUser.Size = new System.Drawing.Size(100, 22);
            this.uComboCarryOutUser.TabIndex = 7;
            this.uComboCarryOutUser.Text = "ultraCombo1";
            this.uComboCarryOutUser.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboCarryOutUser_RowSelected);
            this.uComboCarryOutUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uComboCarryOutUser_KeyDown);
            this.uComboCarryOutUser.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboCarryOutUser_BeforeDropDown);
            this.uComboCarryOutUser.ValueChanged += new System.EventHandler(this.uComboCarryOutUser_ValueChanged);
            // 
            // uLabelCarryOut
            // 
            this.uLabelCarryOut.Location = new System.Drawing.Point(688, 76);
            this.uLabelCarryOut.Name = "uLabelCarryOut";
            this.uLabelCarryOut.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryOut.TabIndex = 26;
            this.uLabelCarryOut.Text = "ultraLabel2";
            // 
            // uTextCarryOutReason
            // 
            this.uTextCarryOutReason.Location = new System.Drawing.Point(116, 124);
            this.uTextCarryOutReason.MaxLength = 100;
            this.uTextCarryOutReason.Name = "uTextCarryOutReason";
            this.uTextCarryOutReason.Size = new System.Drawing.Size(532, 21);
            this.uTextCarryOutReason.TabIndex = 10;
            // 
            // uLabelCarryOutReason
            // 
            this.uLabelCarryOutReason.Location = new System.Drawing.Point(12, 124);
            this.uLabelCarryOutReason.Name = "uLabelCarryOutReason";
            this.uLabelCarryOutReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryOutReason.TabIndex = 24;
            this.uLabelCarryOutReason.Text = "ultraLabel2";
            // 
            // uDateCreateDate
            // 
            this.uDateCreateDate.Location = new System.Drawing.Point(116, 52);
            this.uDateCreateDate.Name = "uDateCreateDate";
            this.uDateCreateDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCreateDate.TabIndex = 2;
            // 
            // uDateCarryInEstDate
            // 
            appearance46.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCarryInEstDate.Appearance = appearance46;
            this.uDateCarryInEstDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCarryInEstDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateCarryInEstDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateCarryInEstDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCarryInEstDate.Location = new System.Drawing.Point(116, 100);
            this.uDateCarryInEstDate.Name = "uDateCarryInEstDate";
            this.uDateCarryInEstDate.Size = new System.Drawing.Size(100, 19);
            this.uDateCarryInEstDate.TabIndex = 8;
            // 
            // uLabelCarryInEstDate
            // 
            this.uLabelCarryInEstDate.Location = new System.Drawing.Point(12, 100);
            this.uLabelCarryInEstDate.Name = "uLabelCarryInEstDate";
            this.uLabelCarryInEstDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryInEstDate.TabIndex = 21;
            this.uLabelCarryInEstDate.Text = "ultraLabel2";
            // 
            // uComboCarryOut
            // 
            this.uComboCarryOut.Location = new System.Drawing.Point(792, 76);
            this.uComboCarryOut.MaxLength = 50;
            this.uComboCarryOut.Name = "uComboCarryOut";
            this.uComboCarryOut.Size = new System.Drawing.Size(104, 21);
            this.uComboCarryOut.TabIndex = 6;
            this.uComboCarryOut.Text = "ultraComboEditor1";
            this.uComboCarryOut.ValueChanged += new System.EventHandler(this.uComboCarryOut_ValueChanged);
            // 
            // uComboECInGubun
            // 
            this.uComboECInGubun.Location = new System.Drawing.Point(444, 76);
            this.uComboECInGubun.MaxLength = 50;
            this.uComboECInGubun.Name = "uComboECInGubun";
            this.uComboECInGubun.Size = new System.Drawing.Size(104, 21);
            this.uComboECInGubun.TabIndex = 5;
            this.uComboECInGubun.Text = "ultraComboEditor1";
            // 
            // uLabelCOutChargeID
            // 
            this.uLabelCOutChargeID.Location = new System.Drawing.Point(688, 100);
            this.uLabelCOutChargeID.Name = "uLabelCOutChargeID";
            this.uLabelCOutChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelCOutChargeID.TabIndex = 19;
            this.uLabelCOutChargeID.Text = "ultraLabel2";
            // 
            // uLabelECInGubun
            // 
            this.uLabelECInGubun.Location = new System.Drawing.Point(340, 76);
            this.uLabelECInGubun.Name = "uLabelECInGubun";
            this.uLabelECInGubun.Size = new System.Drawing.Size(100, 20);
            this.uLabelECInGubun.TabIndex = 19;
            this.uLabelECInGubun.Text = "ultraLabel2";
            // 
            // uTextCarryOutEtcDesc
            // 
            this.uTextCarryOutEtcDesc.Location = new System.Drawing.Point(116, 148);
            this.uTextCarryOutEtcDesc.MaxLength = 100;
            this.uTextCarryOutEtcDesc.Name = "uTextCarryOutEtcDesc";
            this.uTextCarryOutEtcDesc.Size = new System.Drawing.Size(532, 21);
            this.uTextCarryOutEtcDesc.TabIndex = 11;
            // 
            // uLabelCarryOutList
            // 
            this.uLabelCarryOutList.Location = new System.Drawing.Point(12, 196);
            this.uLabelCarryOutList.Name = "uLabelCarryOutList";
            this.uLabelCarryOutList.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryOutList.TabIndex = 17;
            this.uLabelCarryOutList.Text = "ultraLabel2";
            // 
            // uLabelCarryOutEtcDesc
            // 
            this.uLabelCarryOutEtcDesc.Location = new System.Drawing.Point(12, 148);
            this.uLabelCarryOutEtcDesc.Name = "uLabelCarryOutEtcDesc";
            this.uLabelCarryOutEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryOutEtcDesc.TabIndex = 17;
            this.uLabelCarryOutEtcDesc.Text = "ultraLabel2";
            // 
            // uComboECOutGubun
            // 
            this.uComboECOutGubun.Location = new System.Drawing.Point(116, 76);
            this.uComboECOutGubun.MaxLength = 50;
            this.uComboECOutGubun.Name = "uComboECOutGubun";
            this.uComboECOutGubun.Size = new System.Drawing.Size(100, 21);
            this.uComboECOutGubun.TabIndex = 4;
            this.uComboECOutGubun.Text = "ultraComboEditor1";
            // 
            // ulabelECOutGubun
            // 
            this.ulabelECOutGubun.Location = new System.Drawing.Point(12, 76);
            this.ulabelECOutGubun.Name = "ulabelECOutGubun";
            this.ulabelECOutGubun.Size = new System.Drawing.Size(100, 20);
            this.ulabelECOutGubun.TabIndex = 15;
            this.ulabelECOutGubun.Text = "ultraLabel2";
            // 
            // uTextUserDept
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserDept.Appearance = appearance47;
            this.uTextUserDept.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserDept.Location = new System.Drawing.Point(792, 52);
            this.uTextUserDept.Name = "uTextUserDept";
            this.uTextUserDept.ReadOnly = true;
            this.uTextUserDept.Size = new System.Drawing.Size(144, 21);
            this.uTextUserDept.TabIndex = 14;
            // 
            // uLabelUserDept
            // 
            this.uLabelUserDept.Location = new System.Drawing.Point(688, 52);
            this.uLabelUserDept.Name = "uLabelUserDept";
            this.uLabelUserDept.Size = new System.Drawing.Size(100, 20);
            this.uLabelUserDept.TabIndex = 13;
            this.uLabelUserDept.Text = "ultraLabel2";
            // 
            // uTextCOutChargeID
            // 
            appearance48.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCOutChargeID.Appearance = appearance48;
            this.uTextCOutChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance2;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCOutChargeID.ButtonsRight.Add(editorButton2);
            this.uTextCOutChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCOutChargeID.Location = new System.Drawing.Point(444, 52);
            this.uTextCOutChargeID.MaxLength = 20;
            this.uTextCOutChargeID.Name = "uTextCOutChargeID";
            this.uTextCOutChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextCOutChargeID.TabIndex = 3;
            this.uTextCOutChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCOutChargeID_KeyDown);
            this.uTextCOutChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCOutChargeID_EditorButtonClick);
            // 
            // uTextCOutChargeName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCOutChargeName.Appearance = appearance3;
            this.uTextCOutChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCOutChargeName.Location = new System.Drawing.Point(548, 52);
            this.uTextCOutChargeName.Name = "uTextCOutChargeName";
            this.uTextCOutChargeName.ReadOnly = true;
            this.uTextCOutChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextCOutChargeName.TabIndex = 12;
            // 
            // uLabelCOutCharge
            // 
            this.uLabelCOutCharge.Location = new System.Drawing.Point(340, 52);
            this.uLabelCOutCharge.Name = "uLabelCOutCharge";
            this.uLabelCOutCharge.Size = new System.Drawing.Size(100, 20);
            this.uLabelCOutCharge.TabIndex = 2;
            this.uLabelCOutCharge.Text = "ultraLabel2";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel2";
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(12, 52);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateDate.TabIndex = 0;
            this.uLabelCreateDate.Text = "ultraLabel2";
            // 
            // frmEQU0012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0012";
            this.Load += new System.EventHandler(this.frnEQU0012_Load);
            this.Activated += new System.EventHandler(this.frnEQU0012_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0012_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryOutList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutAdmitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutAdmitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCarryOutUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCarryInEstDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCarryOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboECInGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryOutEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboECOutGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCOutChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCOutChargeName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCOutChargeName;
        private Infragistics.Win.Misc.UltraLabel uLabelCOutCharge;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.Misc.UltraLabel ulabelECOutGubun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserDept;
        private Infragistics.Win.Misc.UltraLabel uLabelUserDept;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboECInGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelECInGubun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryOutEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryOutEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboECOutGubun;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCarryInEstDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryInEstDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryOutReason;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryOutReason;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboCarryOutUser;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryOut;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryOutAdmitName;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryOutAdmit;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCarryOutList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryOutAdmitID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCOutChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryOutList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCarryOut;
        private Infragistics.Win.Misc.UltraLabel uLabelCOutChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryOutNum;
    }
}