﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//using 추가
using System.Data;

namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S02.
    /// </summary>
    public partial class rptEQUZ0010_S02 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0010_S02()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }
        public rptEQUZ0010_S02(DataTable dtJobPk,int intRow)
        {
            int intCnt;
            DataRow dr;
            if (intRow == 1)
            {
                if (dtJobPk.Rows.Count.Equals(0) || dtJobPk.Rows.Count < 4)
                {
                    intCnt = 4 - dtJobPk.Rows.Count;

                    for (int i = 0; i < intCnt; i++)
                    {
                        DataRow drNew = dtJobPk.NewRow();
                        dtJobPk.Rows.Add(drNew);
                    }

                }

            }
            else
            {
                if (dtJobPk.Rows.Count == 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        dr = dtJobPk.NewRow();
                        dtJobPk.Rows.Add(dr);
                    }
                }

                if (4 - intRow > 0)
                {
                    // 10 - 8 - 5 = -1
                    intCnt = 4 - intRow - dtJobPk.Rows.Count;
                    if (intCnt > 0)
                    {
                        for (int i = 0; i < intCnt; i++)
                        {
                            dr = dtJobPk.NewRow();
                            dtJobPk.Rows.Add(dr);
                        }
                    }
                }

            }

            this.DataSource = dtJobPk;

            InitializeComponent();
        }
    }
}
