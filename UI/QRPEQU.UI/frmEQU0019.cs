﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmEQU0019.cs                                         */
/* 프로그램명   : 설비관리대장조회                                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-11-05 : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;
using System.Threading;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQU0019 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQU0019()
        {
            InitializeComponent();
        }


        private void frmEQU0019_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser toolbutton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolbutton.mfActiveToolBar(this.ParentForm, true, false, false, false,false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        private void frmEQU0019_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀설정
            titleArea.mfSetLabelText("설비관리대장조회", m_resSys.GetString("SYS_FONTNAME"), 12);
            //컨트롤초기화
            SetToolAuth();
            InitLable();
            //InitTree();
            InitGrid();
            InitButton();
            InitCombo();

            uGroupBoxContentsArea.Expanded = false;
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLable()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcGubun, "공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDate, "검색기간", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchCheck, "조회항목선택", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVedor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSTSDate, "STS입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelERPASSETSNAME, "자산코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region Header

                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None 
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipNameCh", "설비명_중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipNameEn", "설비명_영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "SuperEquipCode", "Super설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "ModelName", "모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipTypeName", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "VendorName", "Vendor", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "GRDate", "STS입고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "MakeYear", "제작년도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipLevelCode", "설비등급", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridEquipList, 0, "On-Line적용여부", "On-Line적용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 170, false, false, 10
                //   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "ERPASSETSNAME", "자산코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipIMageFile", "설비이미지", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                
                //폰트설정
                uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Detail

                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipDetail, 0, "DisplayCode", "유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipDetail, 0, "Date", "일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipDetail, 0, "TypeGubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipDetail, 0, "EquipStateInfo", "설비현황 및 특기사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 510, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipDetail, 0, "ChargeID", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 190, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridEquipDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridEquipDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                //공장정보 매서드 실행
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false
                                        , "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                                        m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                //this.uDateFromDate.Value = Convert.ToDateTime(this.uDateToDate.Value).AddDays(-7);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSelect, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        ///// <summary>
        ///// 트리뷰초기화
        ///// </summary>
        //private void InitTree()
        //{
        //    try
        //    {
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinTree tree = new WinTree();
        //        //초기설정
        //        tree.mfInitTree(this.uTree, Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle.WindowsVista, Infragistics.Win.UltraWinTree.ViewStyle.Grid
        //            , Infragistics.Win.UltraWinTree.ScrollBounds.Default, Infragistics.Win.DefaultableBoolean.False, m_resSys.GetString("SYS_FONTNAME"));
        //        //노드설정
        //        Infragistics.Win.UltraWinTree.UltraTreeNode uTreeNodePlant = tree.mfAddNodeToTree(this.uTree, "Plant", "공장", null, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
        //        Infragistics.Win.UltraWinTree.UltraTreeNode uTreeNodeArea = tree.mfAddNodeToTree(this.uTree, "Area", "Area", uTreeNodePlant, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
        //        Infragistics.Win.UltraWinTree.UltraTreeNode uTreeNodeStation = tree.mfAddNodeToTree(this.uTree, "Station", "Station", uTreeNodeArea, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
        //        Infragistics.Win.UltraWinTree.UltraTreeNode uTreeNodeProcess = tree.mfAddNodeToTree(this.uTree, "설비공정구분", "설비공정부분", uTreeNodeStation, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
        //        //tree.mfAddGridToTree(this.uTree,
        //    }
        //    catch (Exception ex)
        //    { }
        //    finally
        //    { }
        //}
        #endregion

        #region 툴바관련

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //검색 정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipGroup = this.uComboEquipGroup.Value.ToString();
                string strArea = this.uComboArea.Value.ToString();
                string strStation = this.uComboStation.Value.ToString();
                string strProcGubun = this.uComboProcGubun.Value.ToString();
                string strEquipLocCode = this.uComboSearchLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchType.Value.ToString();


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //설비정보조회 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //설비정보조회 매서드 실행
                DataTable dtEquipList = clsEquip.mfReadEquipMerge(strPlantCode, strArea, strStation, strProcGubun, strEquipGroup,strEquipLocCode, strEquipTypeCode
                                                                    , m_resSys.GetString("SYS_LANG"));

                //그리드에 삽입
                this.uGridEquipList.DataSource = dtEquipList;
                this.uGridEquipList.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtEquipList.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001115", "M001102",
                                                                           Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            // 펼침상태가 false 인경우
            if (this.uGroupBoxContentsArea.Expanded == false)
            {
                this.uGroupBoxContentsArea.Expanded = true;
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipList.Rows.Count.Equals(0) && (this.uGridEquipDetail.Rows.Count.Equals(0) || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid wGrid = new WinGrid();
                if (this.uGridEquipList.Rows.Count > 0)
                    wGrid.mfDownLoadGridToExcel(this.uGridEquipList);

                if (this.uGridEquipDetail.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    wGrid.mfDownLoadGridToExcel(this.uGridEquipDetail);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트


        //uGroupBoxContentsArea펼치거나 닫았을때의 위치설정
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipList.Height = 720;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //선택한 공장에 해당하는 설비그룹,Area,Station,공정구분 콤보를 설정한다.
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                
                //공장정보체크
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                //사용자가 입력할때 위의 조건이 성립할시 공장의 Area,Station,공정을 가져온다.
                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    this.uComboEquipGroup.Items.Clear();
                    this.uComboArea.Items.Clear();
                    this.uComboStation.Items.Clear();
                    this.uComboProcGubun.Items.Clear();
                    this.uComboSearchType.Items.Clear();

                    string strLang = m_resSys.GetString("SYS_LANG");

                    //BL
                    QRPBrowser brwChannel = new QRPBrowser();
                    WinComboEditor wCom = new WinComboEditor();

                    //설비그룹정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    brwChannel.mfCredentials(clsEquipGroup);

                    //설비그룹조회 콤보박스용 매서드 호출
                    DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false
                                            , "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                            , "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                    //--------------------- Area정보BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);

                    //Area정보조회 콤보박스용 매서드 호출
                    DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false
                        , "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                        , "", "", "전체", "AreaCode", "AreaName", dtArea);

                    //----------------------Station정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    //Station정보 조회 콤보박스용 매서드 호출
                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false
                        , "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                        , "", "", "전체", "StationCode", "StationName", dtStation);

                    //-----------------------공정구분정보BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    brwChannel.mfCredentials(clsEquipProcGubun);

                    //공정구분정보 조회 콤보박스용 매서드 호출
                    DataTable dtEquipProcGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboProcGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false
                        , "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "EquipProcGubunCode", "EquipProcGubunName", dtEquipProcGubun);


                    //--------설비유형 콤보 변화--------//

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                    QRPMAS.BL.MASEQU.EquipType clsEqutype = new QRPMAS.BL.MASEQU.EquipType();
                    brwChannel.mfCredentials(clsEqutype);

                    DataTable dtType = clsEqutype.mfReadEquipTypeCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboSearchType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                                            "", "", "전체", "EquipTypeCode", "EquipTypeName", dtType);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Station에 따라 설비위치콤보가 바뀜
        private void uComboStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                //-----위치정보 콤보 변화--------//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsLoc);

                DataTable dtLoc = clsLoc.mfReadLocation_Combo(uComboSearchPlant.Value.ToString(), this.uComboStation.Value.ToString(),m_resSys.GetString("SYS_LANG"));

                this.uComboSearchLoc.Items.Clear();

                WinComboEditor wCom = new WinComboEditor();
                wCom.mfSetComboEditor(this.uComboSearchLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                                        "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //선택한 설비의 상세정보를 보여준다.
        private void uGridEquipList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //선택한 줄 고정
                e.Row.Fixed = true;

                //ExpandGroupBox가 숨김상태면 펼친다.
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }

                if (this.uGridEquipDetail.Rows.Count > 0)
                {
                    this.uGridEquipDetail.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipDetail.Rows.All);
                    this.uGridEquipDetail.DeleteSelectedRows(false);
                }

                // 체크박스 컨트롤을 찾아 체크해제를 한다.
                foreach (Control Ctrl in this.uGroupBoxContentsArea.Controls)
                {
                    if(Ctrl.GetType().Equals(typeof(Infragistics.Win.UltraWinEditors.UltraCheckEditor)))
                    {
                        ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)Ctrl).Checked = false;
                    }
                }
                
                this.uCheckPMResult.Checked = true;

                //정보 저장
                string strEquipCode = e.Row.GetCellValue("EquipCode").ToString();
                string strPlantCode = e.Row.GetCellValue("PlantCode").ToString();
                string strFromDate = this.uDateFromDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDate = this.uDateToDate.DateTime.Date.ToString("yyyy-MM-dd");

                //텍스트 박스에 설비정보를 삽입
                this.uTextEquipCode.Text = strEquipCode;
                this.uTextEquipCode.Tag = strPlantCode;
                this.uTextEquipName.Text = e.Row.GetCellValue("EquipName").ToString();
                this.uTextModel.Text = e.Row.GetCellValue("ModelName").ToString();
                this.uTextSerialNo.Text = e.Row.GetCellValue("SerialNo").ToString();
                this.uTextVendor.Text = e.Row.GetCellValue("VendorName").ToString();
                this.uTextSTSDate.Text = e.Row.GetCellValue("GRDate").ToString();
                this.uTextEquipImageFile.Text = e.Row.GetCellValue("EquipIMageFile").ToString();
                this.uTextERPASSETSNAME.Text = e.Row.GetCellValue("ERPASSETSNAME").ToString();
               
                QRPBrowser brwChannel = new QRPBrowser();
                //설비정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //설비정보 조회 매서드 실행
                DataTable dtEquipInfo = clsEquip.mfReadEquipMergeInfo(strPlantCode, strEquipCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //점검결과만보여준다.
                if (!dtEquipInfo.Select("DisplayCode='P' OR DisplayCode='F'").Count().Equals(0))
                {

                    DataTable dtCopy = new DataTable();
                    DataRow[] drCopy;

                    drCopy = dtEquipInfo.Select("DisplayCode='P' OR DisplayCode='F'");
                    dtCopy = drCopy.CopyToDataTable();

                    //검색에 해당하는 리스트를 그리드에 바인드
                    this.uGridEquipDetail.DataSource = dtCopy;
                    this.uGridEquipDetail.DataBind();

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipDetail, 0);
                }

                #region 설비이미지 출력
                //설비이미지 Download받아 보여주기
                uPicEquipImage.Image = null;
                if (uTextEquipImageFile.Text != "")
                {

                    // 서버 연결 체크 //
                    if (brwChannel.mfCheckQRPServer() == false) return;

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0001");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(uTextEquipImageFile.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();

                    //Download받은 화일을 이미지에 보여줌
                    string strImageFile = strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + uTextEquipImageFile.Text;
                    Bitmap img = (Bitmap)Bitmap.FromFile(strImageFile);
                    uPicEquipImage.AutoSize = false;
                    uPicEquipImage.Image = img;

                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //ExpandGroupBox안 검색조건에 해당하는 정보를 조회해준다.
        private void uButtonSelect_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //정보 저장
                string strEquipCode = this.uTextEquipCode.Text;
                string strPlantCode = this.uTextEquipCode.Tag.ToString();
                string strFromDate = this.uDateFromDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDate = this.uDateToDate.DateTime.Date.ToString("yyyy-MM-dd");

                //검색조건 저장
                string strSearchInfo = "";
                string[] strSearch = new string[6];

                strSearch[0] = this.uCheckCerti.Checked.Equals(true) ? "E" : "";                    //== false ? "F" : "T";
                strSearch[1] = this.uCheckDeviceChg.Checked.Equals(true) ? "C" : "";
                strSearch[2] = this.uCheckPMResult.Checked.Equals(true) ? "P" : "";
                strSearch[3] = this.uCheckReapairReq.Checked.Equals(true) ? "R" : "";
                strSearch[4] = this.uCheckDurableTransfe.Checked.Equals(true) ? "D" : "";
                strSearch[5] = this.uCheckSPChg.Checked.Equals(true) ? "S" : "";

                //체크박스 체크를 확인하여 검색조건을 만든다.
                for (int i = 0; i < strSearch.Length; i++)
                {
                    if (!strSearch[i].Equals(string.Empty))
                    {
                        if (strSearchInfo.Equals(string.Empty))
                        {
                            strSearchInfo = "DisplayCode='" + strSearch[i] + "'";
                        }
                        else
                        {
                            strSearchInfo = strSearchInfo + " OR DisplayCode='" + strSearch[i] + "'";
                        }
                    }
                }
                if (!strSearchInfo.Equals(string.Empty))
                {
                    strSearchInfo = strSearchInfo + " OR DisplayCode='F'";
                }

                QRPBrowser brwChannel = new QRPBrowser();
                //설비정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //설비정보 조회 매서드 실행
                DataTable dtEquipInfo = clsEquip.mfReadEquipMergeInfo(strPlantCode, strEquipCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //검색조건에 해당하는 정보가 있을 경우 바인드 없으면 메세지
                if (!dtEquipInfo.Select(strSearchInfo).Count().Equals(0))
                {

                    DataTable dtCopy = new DataTable();
                    DataRow[] drCopy;

                    drCopy = dtEquipInfo.Select(strSearchInfo);
                    dtCopy = drCopy.CopyToDataTable();

                    //검색에 해당하는 리스트를 그리드에 바인드
                    this.uGridEquipDetail.DataSource = dtCopy;
                    this.uGridEquipDetail.DataBind();

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipDetail, 0);
                }
                else
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    WinMessageBox msg = new WinMessageBox();
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001099", "M001115", "M000662", Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmEQU0019_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }









    }
}
