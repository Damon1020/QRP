﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//using 추가
using System.Data;

namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S03.
    /// </summary>
    public partial class rptEQUZ0010_S03 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0010_S03()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }
        public rptEQUZ0010_S03(DataTable dtCheck,int intRow)
        {
            int intCnt;
            DataRow dr;
            if (intRow == 1)
            {
                if (dtCheck.Rows.Count.Equals(0) || dtCheck.Rows.Count < 4)
                {
                    intCnt = 4 - dtCheck.Rows.Count;

                    for (int i = 0; i < intCnt; i++)
                    {
                        DataRow drNew = dtCheck.NewRow();
                        dtCheck.Rows.Add(drNew);
                    }


                }

            }
            else
            {
                if (dtCheck.Rows.Count == 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        dr = dtCheck.NewRow();
                        dtCheck.Rows.Add(dr);
                    }
                }

                if (4 - intRow > 0)
                {
                    // 10 - 8 - 5 = -1
                    intCnt = 4 - intRow - dtCheck.Rows.Count;
                    if (intCnt > 0)
                    {
                        for (int i = 0; i < intCnt; i++)
                        {
                            dr = dtCheck.NewRow();
                            dtCheck.Rows.Add(dr);
                        }
                    }
                }

            }

            this.DataSource = dtCheck;

           InitializeComponent();
        }
    }
}
