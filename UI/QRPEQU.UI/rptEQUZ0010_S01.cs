﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//using추가
using System.Data;

namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S01.
    /// </summary>
    public partial class rptEQUZ0010_S01 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0010_S01()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        public rptEQUZ0010_S01(DataTable dtState,int intRow)
        {
                        // 화면 비율 맞추기.
            int intCnt;
            DataRow dr;
            if (intRow == 1)
            {

                //받은정보의 줄수가 8미만일경우 8줄이 될때까지 줄을 더한다.
                if (dtState.Rows.Count.Equals(0) || dtState.Rows.Count < 3)
                {
                    intCnt = 3 - dtState.Rows.Count;

                    for (int i = 0; i < intCnt; i++)
                    {
                        DataRow drNew = dtState.NewRow();
                        dtState.Rows.Add(drNew);
                    }

                }
                //줄이 5줄 이상일시 그대로 출력
            }
            else
            {
                if (dtState.Rows.Count == 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        dr = dtState.NewRow();
                        dtState.Rows.Add(dr);
                    }
                }

                if (3 - intRow > 0)
                {
                    // 10 - 8 - 5 = -1
                    intCnt = 3 - intRow - dtState.Rows.Count;
                    if (intCnt > 0)
                    {
                        for (int i = 0; i < intCnt; i++)
                        {
                            dr = dtState.NewRow();
                            dtState.Rows.Add(dr);
                        }
                    }
                }

            }

            this.DataSource = dtState;

            InitializeComponent();
        }

    }
}
