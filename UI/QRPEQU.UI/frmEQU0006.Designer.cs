﻿namespace QRPEQU.UI
{
    partial class frmEQU0006
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0006));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLoc = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextTechnicianName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTechnician = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipMentGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uGridPMEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateUsingDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPMPlanDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridPMResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridUseSP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUsingDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMPlanDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUseSP)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnicianName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnician);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipMentGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 7;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 56);
            this.uComboProcessGroup.MaxLength = 40;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(132, 21);
            this.uComboProcessGroup.TabIndex = 31;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 56);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 30;
            // 
            // uComboEquipLargeType
            // 
            this.uComboEquipLargeType.Location = new System.Drawing.Point(448, 56);
            this.uComboEquipLargeType.MaxLength = 40;
            this.uComboEquipLargeType.Name = "uComboEquipLargeType";
            this.uComboEquipLargeType.Size = new System.Drawing.Size(132, 21);
            this.uComboEquipLargeType.TabIndex = 28;
            this.uComboEquipLargeType.ValueChanged += new System.EventHandler(this.uComboEquipLargeType_ValueChanged);
            // 
            // uLabelEquType
            // 
            this.uLabelEquType.Location = new System.Drawing.Point(344, 56);
            this.uLabelEquType.Name = "uLabelEquType";
            this.uLabelEquType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquType.TabIndex = 29;
            this.uLabelEquType.Text = "ultraLabel2";
            // 
            // uComboEquipLoc
            // 
            this.uComboEquipLoc.Location = new System.Drawing.Point(732, 32);
            this.uComboEquipLoc.MaxLength = 50;
            this.uComboEquipLoc.Name = "uComboEquipLoc";
            this.uComboEquipLoc.Size = new System.Drawing.Size(131, 21);
            this.uComboEquipLoc.TabIndex = 26;
            this.uComboEquipLoc.ValueChanged += new System.EventHandler(this.uComboEquipLoc_ValueChanged);
            // 
            // uLabelLoc
            // 
            this.uLabelLoc.Location = new System.Drawing.Point(628, 32);
            this.uLabelLoc.Name = "uLabelLoc";
            this.uLabelLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelLoc.TabIndex = 27;
            this.uLabelLoc.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(15, 15);
            this.ultraLabel1.TabIndex = 19;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchToDay
            // 
            appearance48.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDay.Appearance = appearance48;
            this.uDateSearchToDay.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToDay.Location = new System.Drawing.Point(232, 8);
            this.uDateSearchToDay.Name = "uDateSearchToDay";
            this.uDateSearchToDay.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDay.TabIndex = 2;
            // 
            // uDateSearchFromDay
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDay.Appearance = appearance18;
            this.uDateSearchFromDay.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromDay.Location = new System.Drawing.Point(116, 8);
            this.uDateSearchFromDay.Name = "uDateSearchFromDay";
            this.uDateSearchFromDay.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDay.TabIndex = 1;
            // 
            // uTextTechnicianName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Appearance = appearance21;
            this.uTextTechnicianName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Location = new System.Drawing.Point(220, 32);
            this.uTextTechnicianName.Name = "uTextTechnicianName";
            this.uTextTechnicianName.ReadOnly = true;
            this.uTextTechnicianName.Size = new System.Drawing.Size(100, 21);
            this.uTextTechnicianName.TabIndex = 16;
            // 
            // uTextTechnician
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTechnician.Appearance = appearance22;
            this.uTextTechnician.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTechnician.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance23.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance23;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTechnician.ButtonsRight.Add(editorButton1);
            this.uTextTechnician.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTechnician.Location = new System.Drawing.Point(116, 32);
            this.uTextTechnician.MaxLength = 20;
            this.uTextTechnician.Name = "uTextTechnician";
            this.uTextTechnician.Size = new System.Drawing.Size(100, 19);
            this.uTextTechnician.TabIndex = 8;
            this.uTextTechnician.ValueChanged += new System.EventHandler(this.uTextTechnician_ValueChanged);
            this.uTextTechnician.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextTechnician_KeyDown);
            this.uTextTechnician.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextTechnician_EditorButtonClick);
            // 
            // uLabelSearchUser
            // 
            this.uLabelSearchUser.Location = new System.Drawing.Point(12, 32);
            this.uLabelSearchUser.Name = "uLabelSearchUser";
            this.uLabelSearchUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchUser.TabIndex = 14;
            this.uLabelSearchUser.Text = "ultraLabel3";
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(732, 56);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchEquipGroup.TabIndex = 4;
            // 
            // uLabelSearchEquipMentGroup
            // 
            this.uLabelSearchEquipMentGroup.Location = new System.Drawing.Point(628, 56);
            this.uLabelSearchEquipMentGroup.Name = "uLabelSearchEquipMentGroup";
            this.uLabelSearchEquipMentGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipMentGroup.TabIndex = 12;
            this.uLabelSearchEquipMentGroup.Text = "ultraLabel2";
            // 
            // uComboSearchProcGubun
            // 
            this.uComboSearchProcGubun.Location = new System.Drawing.Point(1040, 8);
            this.uComboSearchProcGubun.MaxLength = 50;
            this.uComboSearchProcGubun.Name = "uComboSearchProcGubun";
            this.uComboSearchProcGubun.Size = new System.Drawing.Size(24, 21);
            this.uComboSearchProcGubun.TabIndex = 7;
            this.uComboSearchProcGubun.Visible = false;
            this.uComboSearchProcGubun.ValueChanged += new System.EventHandler(this.uComboSearchProcGubun_ValueChanged);
            // 
            // uLabelSearchProcGubun
            // 
            this.uLabelSearchProcGubun.Location = new System.Drawing.Point(1012, 8);
            this.uLabelSearchProcGubun.Name = "uLabelSearchProcGubun";
            this.uLabelSearchProcGubun.Size = new System.Drawing.Size(4, 20);
            this.uLabelSearchProcGubun.TabIndex = 10;
            this.uLabelSearchProcGubun.Text = "ultraLabel2";
            this.uLabelSearchProcGubun.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(448, 32);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchStation.TabIndex = 6;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(344, 32);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 8;
            this.uLabelSearchStation.Text = "ultraLabel1";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1024, 8);
            this.uComboSearchArea.MaxLength = 50;
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(16, 21);
            this.uComboSearchArea.TabIndex = 5;
            this.uComboSearchArea.Visible = false;
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(1016, 8);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(4, 20);
            this.uLabelSearchArea.TabIndex = 6;
            this.uLabelSearchArea.Text = "ultraLabel2";
            this.uLabelSearchArea.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(448, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(344, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 2;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uGridPMEquip
            // 
            this.uGridPMEquip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMEquip.DisplayLayout.Appearance = appearance14;
            this.uGridPMEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMEquip.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridPMEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPMEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMEquip.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMEquip.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridPMEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMEquip.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMEquip.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridPMEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMEquip.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMEquip.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridPMEquip.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridPMEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMEquip.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPMEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridPMEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMEquip.Location = new System.Drawing.Point(0, 120);
            this.uGridPMEquip.Name = "uGridPMEquip";
            this.uGridPMEquip.Size = new System.Drawing.Size(1070, 700);
            this.uGridPMEquip.TabIndex = 8;
            this.uGridPMEquip.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            this.uGridPMEquip.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridPMEquip_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 9;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabelUnusual);
            this.uGroupBox1.Controls.Add(this.uTextUserName);
            this.uGroupBox1.Controls.Add(this.uTextUserID);
            this.uGroupBox1.Controls.Add(this.ultraLabel2);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.uDateUsingDate);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uTextPMPlanDate);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 108);
            this.uGroupBox1.TabIndex = 12;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(124, 76);
            this.uTextEtcDesc.MaxLength = 100;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(544, 21);
            this.uTextEtcDesc.TabIndex = 11;
            // 
            // uLabelUnusual
            // 
            this.uLabelUnusual.Location = new System.Drawing.Point(12, 75);
            this.uLabelUnusual.Name = "uLabelUnusual";
            this.uLabelUnusual.Size = new System.Drawing.Size(108, 20);
            this.uLabelUnusual.TabIndex = 26;
            this.uLabelUnusual.Text = "ultraLabel2";
            // 
            // uTextUserName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance3;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(568, 52);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 25;
            // 
            // uTextUserID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.Appearance = appearance2;
            this.uTextUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance5;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton2);
            this.uTextUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextUserID.Location = new System.Drawing.Point(464, 52);
            this.uTextUserID.MaxLength = 20;
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 19);
            this.uTextUserID.TabIndex = 10;
            this.uTextUserID.ValueChanged += new System.EventHandler(this.uTextUserID_ValueChanged);
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(352, 28);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel2.TabIndex = 23;
            this.ultraLabel2.Text = "ultraLabel3";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(352, 52);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(108, 20);
            this.uLabel6.TabIndex = 23;
            this.uLabel6.Text = "ultraLabel3";
            // 
            // uDateUsingDate
            // 
            this.uDateUsingDate.Location = new System.Drawing.Point(124, 52);
            this.uDateUsingDate.Name = "uDateUsingDate";
            this.uDateUsingDate.Size = new System.Drawing.Size(100, 21);
            this.uDateUsingDate.TabIndex = 9;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 52);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(108, 20);
            this.uLabel5.TabIndex = 21;
            this.uLabel5.Text = "ultraLabel2";
            // 
            // uTextPMPlanDate
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMPlanDate.Appearance = appearance17;
            this.uTextPMPlanDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMPlanDate.Location = new System.Drawing.Point(864, 28);
            this.uTextPMPlanDate.Name = "uTextPMPlanDate";
            this.uTextPMPlanDate.ReadOnly = true;
            this.uTextPMPlanDate.Size = new System.Drawing.Size(100, 21);
            this.uTextPMPlanDate.TabIndex = 15;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(752, 28);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(108, 20);
            this.uLabel4.TabIndex = 14;
            this.uLabel4.Text = "ultraLabel2";
            // 
            // uTextEquipName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance19;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(464, 28);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(180, 21);
            this.uTextEquipName.TabIndex = 13;
            // 
            // uTextEquipCode
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance20;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(124, 28);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 11;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(12, 28);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(108, 20);
            this.uLabel2.TabIndex = 10;
            this.uLabel2.Text = "ultraLabel2";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox3.Controls.Add(this.uGridPMResult);
            this.uGroupBox3.Controls.Add(this.uLabel8);
            this.uGroupBox3.Controls.Add(this.uGridUseSP);
            this.uGroupBox3.Controls.Add(this.uLabel7);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 124);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 540);
            this.uGroupBox3.TabIndex = 11;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(124, 24);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 5;
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridPMResult
            // 
            this.uGridPMResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMResult.DisplayLayout.Appearance = appearance24;
            this.uGridPMResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridPMResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResult.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridPMResult.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMResult.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMResult.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.uGridPMResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMResult.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridPMResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMResult.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.uGridPMResult.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGridPMResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMResult.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridPMResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridPMResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMResult.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMResult.Location = new System.Drawing.Point(12, 280);
            this.uGridPMResult.Name = "uGridPMResult";
            this.uGridPMResult.Size = new System.Drawing.Size(1020, 228);
            this.uGridPMResult.TabIndex = 4;
            this.uGridPMResult.Text = "ultraGrid1";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 280);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(100, 20);
            this.uLabel8.TabIndex = 3;
            this.uLabel8.Text = "ultraLabel2";
            // 
            // uGridUseSP
            // 
            this.uGridUseSP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridUseSP.DisplayLayout.Appearance = appearance36;
            this.uGridUseSP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridUseSP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUseSP.DisplayLayout.GroupByBox.Appearance = appearance37;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUseSP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance38;
            this.uGridUseSP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUseSP.DisplayLayout.GroupByBox.PromptAppearance = appearance39;
            this.uGridUseSP.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridUseSP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridUseSP.DisplayLayout.Override.ActiveCellAppearance = appearance40;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridUseSP.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.uGridUseSP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridUseSP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            this.uGridUseSP.DisplayLayout.Override.CardAreaAppearance = appearance42;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridUseSP.DisplayLayout.Override.CellAppearance = appearance43;
            this.uGridUseSP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridUseSP.DisplayLayout.Override.CellPadding = 0;
            appearance44.BackColor = System.Drawing.SystemColors.Control;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUseSP.DisplayLayout.Override.GroupByRowAppearance = appearance44;
            appearance45.TextHAlignAsString = "Left";
            this.uGridUseSP.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.uGridUseSP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridUseSP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            this.uGridUseSP.DisplayLayout.Override.RowAppearance = appearance46;
            this.uGridUseSP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridUseSP.DisplayLayout.Override.TemplateAddRowAppearance = appearance47;
            this.uGridUseSP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridUseSP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridUseSP.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridUseSP.Location = new System.Drawing.Point(12, 32);
            this.uGridUseSP.Name = "uGridUseSP";
            this.uGridUseSP.Size = new System.Drawing.Size(1020, 212);
            this.uGridUseSP.TabIndex = 2;
            this.uGridUseSP.Text = "ultraGrid1";
            this.uGridUseSP.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUseSP_AfterCellUpdate);
            this.uGridUseSP.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUseSP_CellListSelect);
            this.uGridUseSP.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridUseSP_BeforeCellListDropDown);
            this.uGridUseSP.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridUseSP_AfterRowInsert);
            this.uGridUseSP.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 28);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(105, 20);
            this.uLabel7.TabIndex = 0;
            this.uLabel7.Text = "ultraLabel2";
            // 
            // frmEQU0006
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridPMEquip);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0006";
            this.Load += new System.EventHandler(this.frmEQU0006_Load);
            this.Activated += new System.EventHandler(this.frmEQU0006_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0006_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUsingDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMPlanDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUseSP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnicianName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnician;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipMentGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMEquip;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMResult;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUseSP;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateUsingDate;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMPlanDate;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelUnusual;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
    }
}