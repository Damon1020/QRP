﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0003
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0003));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSTSStock = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSTSStock = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipLevel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakerYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLocation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextArea = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSuperEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSysGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLocation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSuperSys = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextInspectQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextCCSChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextCCSChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridEquipCCSD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextInspectResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCCSFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCCSCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectStandard = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectStandard = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCCSFlagName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboEquipCCSType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquipCCSType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCCSDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCCSFinish = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCCSDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateCCSFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCCSChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uComboWorkGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelWorkGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSubject = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSetupDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSetupDocNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcess = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquipCerti = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCCSDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridEquipCCSH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipCCSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectStandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSFlagName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipCCSType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCCSDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCCSFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWorkGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSetupDocNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipCCSH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextSTSStock);
            this.uGroupBox.Controls.Add(this.uLabelSTSStock);
            this.uGroupBox.Controls.Add(this.uTextEquipLevel);
            this.uGroupBox.Controls.Add(this.uTextMakerYear);
            this.uGroupBox.Controls.Add(this.uTextVendor);
            this.uGroupBox.Controls.Add(this.uTextEquipGroupName);
            this.uGroupBox.Controls.Add(this.uTextSerialNo);
            this.uGroupBox.Controls.Add(this.uTextEquipType);
            this.uGroupBox.Controls.Add(this.uTextModel);
            this.uGroupBox.Controls.Add(this.uTextEquipProcess);
            this.uGroupBox.Controls.Add(this.uTextLocation);
            this.uGroupBox.Controls.Add(this.uTextStation);
            this.uGroupBox.Controls.Add(this.uTextArea);
            this.uGroupBox.Controls.Add(this.uTextSuperEquip);
            this.uGroupBox.Controls.Add(this.uTextEquipName);
            this.uGroupBox.Controls.Add(this.uTextEquipCode);
            this.uGroupBox.Controls.Add(this.uComboPlant);
            this.uGroupBox.Controls.Add(this.uLabelSysGrade);
            this.uGroupBox.Controls.Add(this.uLabelYear);
            this.uGroupBox.Controls.Add(this.uLabelVendor);
            this.uGroupBox.Controls.Add(this.uLabelSysGroupName);
            this.uGroupBox.Controls.Add(this.uLabelSerialNo);
            this.uGroupBox.Controls.Add(this.uLabelSysType);
            this.uGroupBox.Controls.Add(this.uLabelModel);
            this.uGroupBox.Controls.Add(this.uLabelSysProcess);
            this.uGroupBox.Controls.Add(this.uLabelLocation);
            this.uGroupBox.Controls.Add(this.uLabelStation);
            this.uGroupBox.Controls.Add(this.uLabelArea);
            this.uGroupBox.Controls.Add(this.uLabelSuperSys);
            this.uGroupBox.Controls.Add(this.uLabelSysName);
            this.uGroupBox.Controls.Add(this.uLabelSysCode);
            this.uGroupBox.Controls.Add(this.uLabelPlant);
            this.uGroupBox.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1040, 180);
            this.uGroupBox.TabIndex = 1;
            // 
            // uTextSTSStock
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Appearance = appearance5;
            this.uTextSTSStock.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Location = new System.Drawing.Point(688, 124);
            this.uTextSTSStock.Name = "uTextSTSStock";
            this.uTextSTSStock.ReadOnly = true;
            this.uTextSTSStock.Size = new System.Drawing.Size(108, 21);
            this.uTextSTSStock.TabIndex = 69;
            // 
            // uLabelSTSStock
            // 
            this.uLabelSTSStock.Location = new System.Drawing.Point(584, 124);
            this.uLabelSTSStock.Name = "uLabelSTSStock";
            this.uLabelSTSStock.Size = new System.Drawing.Size(100, 20);
            this.uLabelSTSStock.TabIndex = 68;
            this.uLabelSTSStock.Text = "STS입고일";
            // 
            // uTextEquipLevel
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Appearance = appearance22;
            this.uTextEquipLevel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Location = new System.Drawing.Point(400, 148);
            this.uTextEquipLevel.Name = "uTextEquipLevel";
            this.uTextEquipLevel.ReadOnly = true;
            this.uTextEquipLevel.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipLevel.TabIndex = 67;
            // 
            // uTextMakerYear
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerYear.Appearance = appearance4;
            this.uTextMakerYear.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerYear.Location = new System.Drawing.Point(116, 148);
            this.uTextMakerYear.Name = "uTextMakerYear";
            this.uTextMakerYear.ReadOnly = true;
            this.uTextMakerYear.Size = new System.Drawing.Size(108, 21);
            this.uTextMakerYear.TabIndex = 66;
            // 
            // uTextVendor
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance6;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 124);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(108, 21);
            this.uTextVendor.TabIndex = 65;
            // 
            // uTextEquipGroupName
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Appearance = appearance7;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(400, 124);
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.ReadOnly = true;
            this.uTextEquipGroupName.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipGroupName.TabIndex = 64;
            // 
            // uTextSerialNo
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance8;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(688, 100);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(228, 21);
            this.uTextSerialNo.TabIndex = 63;
            // 
            // uTextEquipType
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Appearance = appearance9;
            this.uTextEquipType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Location = new System.Drawing.Point(400, 100);
            this.uTextEquipType.Name = "uTextEquipType";
            this.uTextEquipType.ReadOnly = true;
            this.uTextEquipType.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipType.TabIndex = 62;
            // 
            // uTextModel
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance10;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(116, 100);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(108, 21);
            this.uTextModel.TabIndex = 61;
            // 
            // uTextEquipProcess
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcess.Appearance = appearance11;
            this.uTextEquipProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcess.Location = new System.Drawing.Point(400, 76);
            this.uTextEquipProcess.Name = "uTextEquipProcess";
            this.uTextEquipProcess.ReadOnly = true;
            this.uTextEquipProcess.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipProcess.TabIndex = 60;
            // 
            // uTextLocation
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Appearance = appearance12;
            this.uTextLocation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Location = new System.Drawing.Point(688, 76);
            this.uTextLocation.Name = "uTextLocation";
            this.uTextLocation.ReadOnly = true;
            this.uTextLocation.Size = new System.Drawing.Size(228, 21);
            this.uTextLocation.TabIndex = 59;
            // 
            // uTextStation
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Appearance = appearance13;
            this.uTextStation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Location = new System.Drawing.Point(688, 52);
            this.uTextStation.Name = "uTextStation";
            this.uTextStation.ReadOnly = true;
            this.uTextStation.Size = new System.Drawing.Size(228, 21);
            this.uTextStation.TabIndex = 58;
            // 
            // uTextArea
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Appearance = appearance16;
            this.uTextArea.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextArea.Location = new System.Drawing.Point(116, 76);
            this.uTextArea.Name = "uTextArea";
            this.uTextArea.ReadOnly = true;
            this.uTextArea.Size = new System.Drawing.Size(108, 21);
            this.uTextArea.TabIndex = 57;
            // 
            // uTextSuperEquip
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Appearance = appearance17;
            this.uTextSuperEquip.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Location = new System.Drawing.Point(400, 52);
            this.uTextSuperEquip.Name = "uTextSuperEquip";
            this.uTextSuperEquip.ReadOnly = true;
            this.uTextSuperEquip.Size = new System.Drawing.Size(108, 21);
            this.uTextSuperEquip.TabIndex = 56;
            // 
            // uTextEquipName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance37;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextEquipName.Location = new System.Drawing.Point(116, 52);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipName.TabIndex = 55;
            // 
            // uTextEquipCode
            // 
            appearance38.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.Appearance = appearance38;
            this.uTextEquipCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance42.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance42;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(400, 28);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(108, 19);
            this.uTextEquipCode.TabIndex = 6;
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uComboPlant
            // 
            appearance43.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance43;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 5;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelSysGrade
            // 
            this.uLabelSysGrade.Location = new System.Drawing.Point(296, 148);
            this.uLabelSysGrade.Name = "uLabelSysGrade";
            this.uLabelSysGrade.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysGrade.TabIndex = 52;
            this.uLabelSysGrade.Text = "설비등급";
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(12, 148);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 51;
            this.uLabelYear.Text = "제작년도";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 124);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelVendor.TabIndex = 50;
            this.uLabelVendor.Text = "Vendor";
            // 
            // uLabelSysGroupName
            // 
            this.uLabelSysGroupName.Location = new System.Drawing.Point(296, 124);
            this.uLabelSysGroupName.Name = "uLabelSysGroupName";
            this.uLabelSysGroupName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysGroupName.TabIndex = 49;
            this.uLabelSysGroupName.Text = "설비그룹명";
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(584, 100);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSerialNo.TabIndex = 48;
            this.uLabelSerialNo.Text = "SerialNo";
            // 
            // uLabelSysType
            // 
            this.uLabelSysType.Location = new System.Drawing.Point(296, 100);
            this.uLabelSysType.Name = "uLabelSysType";
            this.uLabelSysType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysType.TabIndex = 47;
            this.uLabelSysType.Text = "설비유형";
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(12, 100);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(100, 20);
            this.uLabelModel.TabIndex = 46;
            this.uLabelModel.Text = "모델";
            // 
            // uLabelSysProcess
            // 
            this.uLabelSysProcess.Location = new System.Drawing.Point(296, 76);
            this.uLabelSysProcess.Name = "uLabelSysProcess";
            this.uLabelSysProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysProcess.TabIndex = 45;
            this.uLabelSysProcess.Text = "설비공정부분";
            // 
            // uLabelLocation
            // 
            this.uLabelLocation.Location = new System.Drawing.Point(584, 76);
            this.uLabelLocation.Name = "uLabelLocation";
            this.uLabelLocation.Size = new System.Drawing.Size(100, 20);
            this.uLabelLocation.TabIndex = 44;
            this.uLabelLocation.Text = "위치";
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(584, 52);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 43;
            this.uLabelStation.Text = "Station";
            // 
            // uLabelArea
            // 
            this.uLabelArea.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelArea.Location = new System.Drawing.Point(12, 76);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelArea.TabIndex = 42;
            this.uLabelArea.Text = "Area";
            // 
            // uLabelSuperSys
            // 
            this.uLabelSuperSys.Location = new System.Drawing.Point(296, 52);
            this.uLabelSuperSys.Name = "uLabelSuperSys";
            this.uLabelSuperSys.Size = new System.Drawing.Size(100, 20);
            this.uLabelSuperSys.TabIndex = 41;
            this.uLabelSuperSys.Text = "Super설비";
            // 
            // uLabelSysName
            // 
            this.uLabelSysName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelSysName.Location = new System.Drawing.Point(12, 52);
            this.uLabelSysName.Name = "uLabelSysName";
            this.uLabelSysName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysName.TabIndex = 40;
            this.uLabelSysName.Text = "설비명";
            // 
            // uLabelSysCode
            // 
            this.uLabelSysCode.Location = new System.Drawing.Point(296, 28);
            this.uLabelSysCode.Name = "uLabelSysCode";
            this.uLabelSysCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysCode.TabIndex = 39;
            this.uLabelSysCode.Text = "설비코드";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 38;
            this.uLabelPlant.Text = "공장";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextInspectQty);
            this.uGroupBox1.Controls.Add(this.uTextCCSChargeID);
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox1.Controls.Add(this.uTextCCSChargeName);
            this.uGroupBox1.Controls.Add(this.uGridEquipCCSD);
            this.uGroupBox1.Controls.Add(this.uTextInspectResult);
            this.uGroupBox1.Controls.Add(this.uLabelResult);
            this.uGroupBox1.Controls.Add(this.uTextCCSFlag);
            this.uGroupBox1.Controls.Add(this.uTextCCSCode);
            this.uGroupBox1.Controls.Add(this.uLabelInspectQty);
            this.uGroupBox1.Controls.Add(this.uTextInspectStandard);
            this.uGroupBox1.Controls.Add(this.uLabelInspectStandard);
            this.uGroupBox1.Controls.Add(this.uTextCCSFlagName);
            this.uGroupBox1.Controls.Add(this.uComboEquipCCSType);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCCSType);
            this.uGroupBox1.Controls.Add(this.uLabel27);
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabelCCSDate);
            this.uGroupBox1.Controls.Add(this.uLabelCCSFinish);
            this.uGroupBox1.Controls.Add(this.uDateCCSDate);
            this.uGroupBox1.Controls.Add(this.uDateCCSFinishDate);
            this.uGroupBox1.Controls.Add(this.uLabelCCSChargeID);
            this.uGroupBox1.Controls.Add(this.uComboWorkGubun);
            this.uGroupBox1.Controls.Add(this.uLabelWorkGubun);
            this.uGroupBox1.Controls.Add(this.uTextObject);
            this.uGroupBox1.Controls.Add(this.uLabelSubject);
            this.uGroupBox1.Controls.Add(this.uTextSetupDocNum);
            this.uGroupBox1.Controls.Add(this.uLabelSetupDocNum);
            this.uGroupBox1.Controls.Add(this.uComboProcess);
            this.uGroupBox1.Controls.Add(this.uLabelProcess);
            this.uGroupBox1.Controls.Add(this.ultraComboEditor1);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCerti);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 196);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 496);
            this.uGroupBox1.TabIndex = 2;
            // 
            // uTextInspectQty
            // 
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Text = "0";
            this.uTextInspectQty.ButtonsLeft.Add(editorButton2);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectQty.ButtonsRight.Add(spinEditorButton1);
            this.uTextInspectQty.Location = new System.Drawing.Point(760, 76);
            this.uTextInspectQty.MaxValue = 9999;
            this.uTextInspectQty.MinValue = 0;
            this.uTextInspectQty.Name = "uTextInspectQty";
            this.uTextInspectQty.PromptChar = ' ';
            this.uTextInspectQty.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectQty.TabIndex = 81;
            this.uTextInspectQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uTextInspectQty_EditorSpinButtonClick);
            this.uTextInspectQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectQty_EditorButtonClick);
            // 
            // uTextCCSChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCCSChargeID.Appearance = appearance15;
            this.uTextCCSChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCCSChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance35;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCCSChargeID.ButtonsRight.Add(editorButton3);
            this.uTextCCSChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCCSChargeID.Location = new System.Drawing.Point(408, 76);
            this.uTextCCSChargeID.MaxLength = 20;
            this.uTextCCSChargeID.Name = "uTextCCSChargeID";
            this.uTextCCSChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextCCSChargeID.TabIndex = 14;
            this.uTextCCSChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCCSChargeID_KeyDown);
            this.uTextCCSChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCCSChargeID_EditorButtonClick);
            // 
            // uButtonDeleteRow
            // 
            appearance3.FontData.BoldAsString = "True";
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_delTable;
            this.uButtonDeleteRow.Appearance = appearance3;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(116, 196);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 80;
            this.uButtonDeleteRow.Text = "행삭제";
            // 
            // uTextCCSChargeName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCCSChargeName.Appearance = appearance36;
            this.uTextCCSChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCCSChargeName.Location = new System.Drawing.Point(512, 76);
            this.uTextCCSChargeName.Name = "uTextCCSChargeName";
            this.uTextCCSChargeName.ReadOnly = true;
            this.uTextCCSChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextCCSChargeName.TabIndex = 79;
            // 
            // uGridEquipCCSD
            // 
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipCCSD.DisplayLayout.Appearance = appearance23;
            this.uGridEquipCCSD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipCCSD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipCCSD.DisplayLayout.GroupByBox.Appearance = appearance24;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipCCSD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance25;
            this.uGridEquipCCSD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance26.BackColor2 = System.Drawing.SystemColors.Control;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipCCSD.DisplayLayout.GroupByBox.PromptAppearance = appearance26;
            this.uGridEquipCCSD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipCCSD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipCCSD.DisplayLayout.Override.ActiveCellAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.Highlight;
            appearance28.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipCCSD.DisplayLayout.Override.ActiveRowAppearance = appearance28;
            this.uGridEquipCCSD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipCCSD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipCCSD.DisplayLayout.Override.CardAreaAppearance = appearance29;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipCCSD.DisplayLayout.Override.CellAppearance = appearance30;
            this.uGridEquipCCSD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipCCSD.DisplayLayout.Override.CellPadding = 0;
            appearance31.BackColor = System.Drawing.SystemColors.Control;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipCCSD.DisplayLayout.Override.GroupByRowAppearance = appearance31;
            appearance32.TextHAlignAsString = "Left";
            this.uGridEquipCCSD.DisplayLayout.Override.HeaderAppearance = appearance32;
            this.uGridEquipCCSD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipCCSD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipCCSD.DisplayLayout.Override.RowAppearance = appearance33;
            this.uGridEquipCCSD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipCCSD.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
            this.uGridEquipCCSD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipCCSD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipCCSD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipCCSD.Location = new System.Drawing.Point(12, 224);
            this.uGridEquipCCSD.Name = "uGridEquipCCSD";
            this.uGridEquipCCSD.Size = new System.Drawing.Size(1016, 260);
            this.uGridEquipCCSD.TabIndex = 78;
            this.uGridEquipCCSD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipCCSD_AfterCellUpdate);
            this.uGridEquipCCSD.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipCCSD_CellChange);
            // 
            // uTextInspectResult
            // 
            this.uTextInspectResult.Location = new System.Drawing.Point(116, 124);
            this.uTextInspectResult.MaxLength = 1000;
            this.uTextInspectResult.Name = "uTextInspectResult";
            this.uTextInspectResult.Size = new System.Drawing.Size(496, 21);
            this.uTextInspectResult.TabIndex = 17;
            // 
            // uLabelResult
            // 
            this.uLabelResult.Location = new System.Drawing.Point(12, 124);
            this.uLabelResult.Name = "uLabelResult";
            this.uLabelResult.Size = new System.Drawing.Size(100, 20);
            this.uLabelResult.TabIndex = 75;
            this.uLabelResult.Text = "검사결과";
            // 
            // uTextCCSFlag
            // 
            this.uTextCCSFlag.Location = new System.Drawing.Point(520, 52);
            this.uTextCCSFlag.Name = "uTextCCSFlag";
            this.uTextCCSFlag.ReadOnly = true;
            this.uTextCCSFlag.Size = new System.Drawing.Size(108, 21);
            this.uTextCCSFlag.TabIndex = 16;
            this.uTextCCSFlag.Visible = false;
            // 
            // uTextCCSCode
            // 
            this.uTextCCSCode.Location = new System.Drawing.Point(760, 100);
            this.uTextCCSCode.Name = "uTextCCSCode";
            this.uTextCCSCode.ReadOnly = true;
            this.uTextCCSCode.Size = new System.Drawing.Size(108, 21);
            this.uTextCCSCode.TabIndex = 16;
            this.uTextCCSCode.Visible = false;
            // 
            // uLabelInspectQty
            // 
            this.uLabelInspectQty.Location = new System.Drawing.Point(656, 76);
            this.uLabelInspectQty.Name = "uLabelInspectQty";
            this.uLabelInspectQty.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectQty.TabIndex = 73;
            this.uLabelInspectQty.Text = "검사수량";
            // 
            // uTextInspectStandard
            // 
            this.uTextInspectStandard.Location = new System.Drawing.Point(116, 100);
            this.uTextInspectStandard.MaxLength = 1000;
            this.uTextInspectStandard.Name = "uTextInspectStandard";
            this.uTextInspectStandard.Size = new System.Drawing.Size(496, 21);
            this.uTextInspectStandard.TabIndex = 15;
            // 
            // uLabelInspectStandard
            // 
            this.uLabelInspectStandard.Location = new System.Drawing.Point(12, 100);
            this.uLabelInspectStandard.Name = "uLabelInspectStandard";
            this.uLabelInspectStandard.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectStandard.TabIndex = 71;
            this.uLabelInspectStandard.Text = "검사기준";
            // 
            // uTextCCSFlagName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCCSFlagName.Appearance = appearance2;
            this.uTextCCSFlagName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCCSFlagName.Location = new System.Drawing.Point(520, 28);
            this.uTextCCSFlagName.Name = "uTextCCSFlagName";
            this.uTextCCSFlagName.ReadOnly = true;
            this.uTextCCSFlagName.Size = new System.Drawing.Size(108, 21);
            this.uTextCCSFlagName.TabIndex = 70;
            // 
            // uComboEquipCCSType
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboEquipCCSType.Appearance = appearance14;
            this.uComboEquipCCSType.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboEquipCCSType.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboEquipCCSType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboEquipCCSType.Location = new System.Drawing.Point(408, 28);
            this.uComboEquipCCSType.MaxLength = 50;
            this.uComboEquipCCSType.Name = "uComboEquipCCSType";
            this.uComboEquipCCSType.Size = new System.Drawing.Size(108, 19);
            this.uComboEquipCCSType.TabIndex = 8;
            this.uComboEquipCCSType.Text = "ultraComboEditor2";
            this.uComboEquipCCSType.ValueChanged += new System.EventHandler(this.uComboEquipCCSType_ValueChanged);
            // 
            // uLabelEquipCCSType
            // 
            this.uLabelEquipCCSType.Location = new System.Drawing.Point(292, 28);
            this.uLabelEquipCCSType.Name = "uLabelEquipCCSType";
            this.uLabelEquipCCSType.Size = new System.Drawing.Size(112, 20);
            this.uLabelEquipCCSType.TabIndex = 67;
            this.uLabelEquipCCSType.Text = "설비변경점유형";
            // 
            // uLabel27
            // 
            this.uLabel27.Location = new System.Drawing.Point(12, 200);
            this.uLabel27.Name = "uLabel27";
            this.uLabel27.Size = new System.Drawing.Size(100, 20);
            this.uLabel27.TabIndex = 65;
            this.uLabel27.Text = "변경진행사항";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 172);
            this.uTextEtcDesc.MaxLength = 1000;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(512, 21);
            this.uTextEtcDesc.TabIndex = 19;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 172);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcDesc.TabIndex = 63;
            this.uLabelEtcDesc.Text = "특이사항";
            // 
            // uLabelCCSDate
            // 
            this.uLabelCCSDate.Location = new System.Drawing.Point(12, 52);
            this.uLabelCCSDate.Name = "uLabelCCSDate";
            this.uLabelCCSDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCCSDate.TabIndex = 62;
            this.uLabelCCSDate.Text = "변경일";
            // 
            // uLabelCCSFinish
            // 
            this.uLabelCCSFinish.Location = new System.Drawing.Point(12, 76);
            this.uLabelCCSFinish.Name = "uLabelCCSFinish";
            this.uLabelCCSFinish.Size = new System.Drawing.Size(100, 20);
            this.uLabelCCSFinish.TabIndex = 62;
            this.uLabelCCSFinish.Text = "변경완료일";
            // 
            // uDateCCSDate
            // 
            this.uDateCCSDate.Location = new System.Drawing.Point(116, 52);
            this.uDateCCSDate.Name = "uDateCCSDate";
            this.uDateCCSDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCCSDate.TabIndex = 10;
            // 
            // uDateCCSFinishDate
            // 
            this.uDateCCSFinishDate.Location = new System.Drawing.Point(116, 76);
            this.uDateCCSFinishDate.Name = "uDateCCSFinishDate";
            this.uDateCCSFinishDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCCSFinishDate.TabIndex = 13;
            // 
            // uLabelCCSChargeID
            // 
            this.uLabelCCSChargeID.Location = new System.Drawing.Point(292, 76);
            this.uLabelCCSChargeID.Name = "uLabelCCSChargeID";
            this.uLabelCCSChargeID.Size = new System.Drawing.Size(112, 20);
            this.uLabelCCSChargeID.TabIndex = 59;
            this.uLabelCCSChargeID.Text = "변경담당자";
            // 
            // uComboWorkGubun
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboWorkGubun.Appearance = appearance39;
            this.uComboWorkGubun.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboWorkGubun.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboWorkGubun.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboWorkGubun.Location = new System.Drawing.Point(760, 52);
            this.uComboWorkGubun.Name = "uComboWorkGubun";
            this.uComboWorkGubun.Size = new System.Drawing.Size(108, 19);
            this.uComboWorkGubun.TabIndex = 12;
            this.uComboWorkGubun.Text = "ultraComboEditor1";
            // 
            // uLabelWorkGubun
            // 
            this.uLabelWorkGubun.Location = new System.Drawing.Point(656, 52);
            this.uLabelWorkGubun.Name = "uLabelWorkGubun";
            this.uLabelWorkGubun.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkGubun.TabIndex = 57;
            this.uLabelWorkGubun.Text = "작업조";
            // 
            // uTextObject
            // 
            this.uTextObject.Location = new System.Drawing.Point(116, 148);
            this.uTextObject.MaxLength = 1000;
            this.uTextObject.Name = "uTextObject";
            this.uTextObject.Size = new System.Drawing.Size(512, 21);
            this.uTextObject.TabIndex = 18;
            // 
            // uLabelSubject
            // 
            this.uLabelSubject.Location = new System.Drawing.Point(12, 148);
            this.uLabelSubject.Name = "uLabelSubject";
            this.uLabelSubject.Size = new System.Drawing.Size(100, 20);
            this.uLabelSubject.TabIndex = 55;
            this.uLabelSubject.Text = "목적";
            // 
            // uTextSetupDocNum
            // 
            this.uTextSetupDocNum.Location = new System.Drawing.Point(408, 52);
            this.uTextSetupDocNum.Name = "uTextSetupDocNum";
            this.uTextSetupDocNum.Size = new System.Drawing.Size(108, 21);
            this.uTextSetupDocNum.TabIndex = 11;
            // 
            // uLabelSetupDocNum
            // 
            this.uLabelSetupDocNum.Location = new System.Drawing.Point(292, 52);
            this.uLabelSetupDocNum.Name = "uLabelSetupDocNum";
            this.uLabelSetupDocNum.Size = new System.Drawing.Size(112, 20);
            this.uLabelSetupDocNum.TabIndex = 53;
            this.uLabelSetupDocNum.Text = "Setup문서번호";
            // 
            // uComboProcess
            // 
            appearance40.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcess.Appearance = appearance40;
            this.uComboProcess.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcess.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboProcess.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboProcess.Location = new System.Drawing.Point(116, 28);
            this.uComboProcess.Name = "uComboProcess";
            this.uComboProcess.Size = new System.Drawing.Size(108, 19);
            this.uComboProcess.TabIndex = 7;
            this.uComboProcess.Text = "ultraComboEditor1";
            // 
            // uLabelProcess
            // 
            this.uLabelProcess.Location = new System.Drawing.Point(12, 28);
            this.uLabelProcess.Name = "uLabelProcess";
            this.uLabelProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcess.TabIndex = 51;
            this.uLabelProcess.Text = "공정";
            // 
            // ultraComboEditor1
            // 
            appearance41.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor1.Appearance = appearance41;
            this.ultraComboEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraComboEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraComboEditor1.Location = new System.Drawing.Point(760, 28);
            this.ultraComboEditor1.MaxLength = 50;
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(108, 19);
            this.ultraComboEditor1.TabIndex = 9;
            this.ultraComboEditor1.Text = "ultraComboEditor1";
            this.ultraComboEditor1.Visible = false;
            // 
            // uLabelEquipCerti
            // 
            this.uLabelEquipCerti.Location = new System.Drawing.Point(656, 28);
            this.uLabelEquipCerti.Name = "uLabelEquipCerti";
            this.uLabelEquipCerti.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCerti.TabIndex = 49;
            this.uLabelEquipCerti.Text = "설비인증구분";
            this.uLabelEquipCerti.Visible = false;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel8);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCCSDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 38;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(300, 12);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquip.TabIndex = 39;
            this.uLabelSearchEquip.Text = "설비코드";
            // 
            // uTextSearchEquipCode
            // 
            appearance21.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance21.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance21;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton4);
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(404, 12);
            this.uTextSearchEquipCode.MaxLength = 20;
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(108, 21);
            this.uTextSearchEquipCode.TabIndex = 2;
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uTextSearchEquipName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance18;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextSearchEquipName.Location = new System.Drawing.Point(516, 12);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(108, 21);
            this.uTextSearchEquipName.TabIndex = 55;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(888, 16);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel8.TabIndex = 62;
            this.ultraLabel8.Text = "~";
            // 
            // uLabelSearchCCSDate
            // 
            this.uLabelSearchCCSDate.Location = new System.Drawing.Point(684, 12);
            this.uLabelSearchCCSDate.Name = "uLabelSearchCCSDate";
            this.uLabelSearchCCSDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCCSDate.TabIndex = 62;
            this.uLabelSearchCCSDate.Text = "변경일";
            // 
            // uDateToDate
            // 
            this.uDateToDate.Location = new System.Drawing.Point(904, 12);
            this.uDateToDate.Name = "uDateToDate";
            this.uDateToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToDate.TabIndex = 4;
            // 
            // uDateFromDate
            // 
            this.uDateFromDate.Location = new System.Drawing.Point(788, 12);
            this.uDateFromDate.Name = "uDateFromDate";
            this.uDateFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDate.TabIndex = 3;
            // 
            // uGridEquipCCSH
            // 
            this.uGridEquipCCSH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipCCSH.DisplayLayout.Appearance = appearance47;
            this.uGridEquipCCSH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipCCSH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipCCSH.DisplayLayout.GroupByBox.Appearance = appearance44;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipCCSH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance45;
            this.uGridEquipCCSH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance46.BackColor2 = System.Drawing.SystemColors.Control;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance46.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipCCSH.DisplayLayout.GroupByBox.PromptAppearance = appearance46;
            this.uGridEquipCCSH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipCCSH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipCCSH.DisplayLayout.Override.ActiveCellAppearance = appearance55;
            appearance50.BackColor = System.Drawing.SystemColors.Highlight;
            appearance50.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipCCSH.DisplayLayout.Override.ActiveRowAppearance = appearance50;
            this.uGridEquipCCSH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipCCSH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipCCSH.DisplayLayout.Override.CardAreaAppearance = appearance49;
            appearance48.BorderColor = System.Drawing.Color.Silver;
            appearance48.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipCCSH.DisplayLayout.Override.CellAppearance = appearance48;
            this.uGridEquipCCSH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipCCSH.DisplayLayout.Override.CellPadding = 0;
            appearance52.BackColor = System.Drawing.SystemColors.Control;
            appearance52.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance52.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipCCSH.DisplayLayout.Override.GroupByRowAppearance = appearance52;
            appearance54.TextHAlignAsString = "Left";
            this.uGridEquipCCSH.DisplayLayout.Override.HeaderAppearance = appearance54;
            this.uGridEquipCCSH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipCCSH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipCCSH.DisplayLayout.Override.RowAppearance = appearance53;
            this.uGridEquipCCSH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipCCSH.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGridEquipCCSH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipCCSH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipCCSH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipCCSH.Location = new System.Drawing.Point(0, 80);
            this.uGridEquipCCSH.Name = "uGridEquipCCSH";
            this.uGridEquipCCSH.Size = new System.Drawing.Size(1070, 760);
            this.uGridEquipCCSH.TabIndex = 4;
            this.uGridEquipCCSH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEquipCCSH_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 5;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // frmEQUZ0003
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquipCCSH);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0003";
            this.Load += new System.EventHandler(this.frmEQUZ0003_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0003_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0003_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipCCSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectStandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSFlagName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipCCSType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCCSDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCCSFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWorkGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSetupDocNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipCCSH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipCCSD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectResult;
        private Infragistics.Win.Misc.UltraLabel uLabelResult;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectStandard;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectStandard;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCCSFlagName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipCCSType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCCSType;
        private Infragistics.Win.Misc.UltraLabel uLabel27;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelCCSFinish;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCCSFinishDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCCSChargeID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboWorkGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkGubun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextObject;
        private Infragistics.Win.Misc.UltraLabel uLabelSubject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSetupDocNum;
        private Infragistics.Win.Misc.UltraLabel uLabelSetupDocNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCerti;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCCSChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSTSStock;
        private Infragistics.Win.Misc.UltraLabel uLabelSTSStock;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipLevel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakerYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLocation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSuperEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSysGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSysGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSysType;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.Misc.UltraLabel uLabelSysProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelLocation;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSuperSys;
        private Infragistics.Win.Misc.UltraLabel uLabelSysName;
        private Infragistics.Win.Misc.UltraLabel uLabelSysCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCCSChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelCCSDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCCSDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCCSDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipCCSH;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCCSCode;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uTextInspectQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCCSFlag;
    }
}