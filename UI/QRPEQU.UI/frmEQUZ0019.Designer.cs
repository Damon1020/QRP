﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0019
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0019));
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextTransferCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTransferCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextTransferChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSPTransferD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTransferChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelTransferChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTransferDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSPTransferH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchTransferDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 22;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTransferCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTransferCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDeleteRow);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTransferChargeID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridSPTransferD);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel6);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTransferChargeName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateTransferDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTransferChargeID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTransferDate);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextTransferCode
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferCode.Appearance = appearance67;
            this.uTextTransferCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferCode.Location = new System.Drawing.Point(356, 12);
            this.uTextTransferCode.Name = "uTextTransferCode";
            this.uTextTransferCode.ReadOnly = true;
            this.uTextTransferCode.Size = new System.Drawing.Size(100, 21);
            this.uTextTransferCode.TabIndex = 101;
            // 
            // uLabelTransferCode
            // 
            this.uLabelTransferCode.Location = new System.Drawing.Point(252, 12);
            this.uLabelTransferCode.Name = "uLabelTransferCode";
            this.uLabelTransferCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelTransferCode.TabIndex = 100;
            this.uLabelTransferCode.Text = "ultraLabel3";
            // 
            // uComboPlant
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance31;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 99;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uButtonDeleteRow
            // 
            appearance3.FontData.BoldAsString = "True";
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_delTable;
            this.uButtonDeleteRow.Appearance = appearance3;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(116, 92);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 98;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uTextTransferChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTransferChargeID.Appearance = appearance15;
            this.uTextTransferChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTransferChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTransferChargeID.ButtonsRight.Add(editorButton1);
            this.uTextTransferChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTransferChargeID.Location = new System.Drawing.Point(356, 36);
            this.uTextTransferChargeID.MaxLength = 20;
            this.uTextTransferChargeID.Name = "uTextTransferChargeID";
            this.uTextTransferChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextTransferChargeID.TabIndex = 97;
            this.uTextTransferChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextTransferChargeID_KeyDown);
            this.uTextTransferChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextTransferChargeID_EditorButtonClick);
            // 
            // uLabelPlant1
            // 
            this.uLabelPlant1.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant1.Name = "uLabelPlant1";
            this.uLabelPlant1.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant1.TabIndex = 52;
            this.uLabelPlant1.Text = "ultraLabel9";
            // 
            // uGridSPTransferD
            // 
            this.uGridSPTransferD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSPTransferD.DisplayLayout.Appearance = appearance22;
            this.uGridSPTransferD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSPTransferD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.uGridSPTransferD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSPTransferD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSPTransferD.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSPTransferD.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGridSPTransferD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSPTransferD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSPTransferD.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGridSPTransferD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSPTransferD.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance29.TextHAlignAsString = "Left";
            this.uGridSPTransferD.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGridSPTransferD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSPTransferD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.uGridSPTransferD.DisplayLayout.Override.RowAppearance = appearance28;
            this.uGridSPTransferD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSPTransferD.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridSPTransferD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSPTransferD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSPTransferD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSPTransferD.Location = new System.Drawing.Point(12, 120);
            this.uGridSPTransferD.Name = "uGridSPTransferD";
            this.uGridSPTransferD.Size = new System.Drawing.Size(1036, 550);
            this.uGridSPTransferD.TabIndex = 49;
            this.uGridSPTransferD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSPTransferD_AfterCellUpdate);
            this.uGridSPTransferD.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSPTransferD_CellListSelect);
            this.uGridSPTransferD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridSPTransferD_KeyDown);
            this.uGridSPTransferD.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSPTransferD_AfterCellListCloseUp);
            this.uGridSPTransferD.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSPTransferD_ClickCellButton);
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 96);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(100, 20);
            this.uLabel7.TabIndex = 47;
            this.uLabel7.Text = "ultraLabel7";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextEtcDesc.MaxLength = 1000;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(444, 21);
            this.uTextEtcDesc.TabIndex = 45;
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(12, 60);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(100, 20);
            this.uLabel6.TabIndex = 43;
            this.uLabel6.Text = "ultraLabel6";
            // 
            // uTextTransferChargeName
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferChargeName.Appearance = appearance34;
            this.uTextTransferChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferChargeName.Location = new System.Drawing.Point(460, 36);
            this.uTextTransferChargeName.Name = "uTextTransferChargeName";
            this.uTextTransferChargeName.ReadOnly = true;
            this.uTextTransferChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextTransferChargeName.TabIndex = 42;
            // 
            // uDateTransferDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTransferDate.Appearance = appearance16;
            this.uDateTransferDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTransferDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateTransferDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTransferDate.Location = new System.Drawing.Point(116, 36);
            this.uDateTransferDate.Name = "uDateTransferDate";
            this.uDateTransferDate.Size = new System.Drawing.Size(100, 19);
            this.uDateTransferDate.TabIndex = 40;
            // 
            // uLabelTransferChargeID
            // 
            this.uLabelTransferChargeID.Location = new System.Drawing.Point(252, 36);
            this.uLabelTransferChargeID.Name = "uLabelTransferChargeID";
            this.uLabelTransferChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelTransferChargeID.TabIndex = 39;
            this.uLabelTransferChargeID.Text = "ultraLabel3";
            // 
            // uLabelTransferDate
            // 
            this.uLabelTransferDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelTransferDate.Name = "uLabelTransferDate";
            this.uLabelTransferDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelTransferDate.TabIndex = 38;
            this.uLabelTransferDate.Text = "ultraLabel4";
            // 
            // uGridSPTransferH
            // 
            this.uGridSPTransferH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSPTransferH.DisplayLayout.Appearance = appearance5;
            this.uGridSPTransferH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSPTransferH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferH.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridSPTransferH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferH.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridSPTransferH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSPTransferH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSPTransferH.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSPTransferH.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridSPTransferH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSPTransferH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferH.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSPTransferH.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridSPTransferH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSPTransferH.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferH.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridSPTransferH.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridSPTransferH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSPTransferH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridSPTransferH.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridSPTransferH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSPTransferH.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.uGridSPTransferH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSPTransferH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSPTransferH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSPTransferH.Location = new System.Drawing.Point(0, 80);
            this.uGridSPTransferH.Name = "uGridSPTransferH";
            this.uGridSPTransferH.Size = new System.Drawing.Size(1070, 760);
            this.uGridSPTransferH.TabIndex = 21;
            this.uGridSPTransferH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridSPTransferH_DoubleClickCell);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToTransferDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromTransferDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTransferDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 20;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(464, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel1.TabIndex = 6;
            this.ultraLabel1.Text = "~";
            // 
            // uDateToTransferDate
            // 
            appearance32.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToTransferDate.Appearance = appearance32;
            this.uDateToTransferDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToTransferDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateToTransferDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToTransferDate.Location = new System.Drawing.Point(480, 12);
            this.uDateToTransferDate.MaskInput = "";
            this.uDateToTransferDate.Name = "uDateToTransferDate";
            this.uDateToTransferDate.Size = new System.Drawing.Size(100, 19);
            this.uDateToTransferDate.TabIndex = 4;
            // 
            // uDateFromTransferDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromTransferDate.Appearance = appearance4;
            this.uDateFromTransferDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromTransferDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateFromTransferDate.DateTime = new System.DateTime(2011, 6, 1, 0, 0, 0, 0);
            this.uDateFromTransferDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromTransferDate.Location = new System.Drawing.Point(360, 12);
            this.uDateFromTransferDate.MaskInput = "";
            this.uDateFromTransferDate.Name = "uDateFromTransferDate";
            this.uDateFromTransferDate.Size = new System.Drawing.Size(100, 19);
            this.uDateFromTransferDate.TabIndex = 3;
            this.uDateFromTransferDate.Value = new System.DateTime(2011, 6, 1, 0, 0, 0, 0);
            // 
            // uLabelSearchTransferDate
            // 
            this.uLabelSearchTransferDate.Location = new System.Drawing.Point(256, 12);
            this.uLabelSearchTransferDate.Name = "uLabelSearchTransferDate";
            this.uLabelSearchTransferDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTransferDate.TabIndex = 2;
            this.uLabelSearchTransferDate.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            appearance33.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Appearance = appearance33;
            this.uComboSearchPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 19;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQUZ0019
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridSPTransferH);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0019";
            this.Load += new System.EventHandler(this.frmEQUZ0019_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0019_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0019_FormClosing);
            this.Resize += new System.EventHandler(this.frmEQUZ0019_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSPTransferD;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTransferChargeName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTransferDate;
        private Infragistics.Win.Misc.UltraLabel uLabelTransferChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelTransferDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSPTransferH;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToTransferDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromTransferDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTransferDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTransferChargeID;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTransferCode;
        private Infragistics.Win.Misc.UltraLabel uLabelTransferCode;

    }
}