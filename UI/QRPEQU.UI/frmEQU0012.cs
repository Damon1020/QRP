﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQU0012.cs                                         */
/* 프로그램명   : 반출등록                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQU0012 : Form, IToolbar
    {

        #region 전역변수
        
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
        private string strFormName;


        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }


        #endregion

        public frmEQU0012()
        {
            InitializeComponent();
        }

        public frmEQU0012(DataTable dtCarryOut)
        {
            InitializeComponent();
            InitGrid();
            InitComboBox();
            CarryOutDisPlay(dtCarryOut);
        }

        // 툴바 활성화 처리
        private void frnEQU0012_Activated(object sender, EventArgs e)
        {
            

            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frnEQU0012_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            this.titleArea.mfSetLabelText("반출등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitGroupBox();
            InitLabel();

            if (FormName == null)
            {
                InitGrid();
                InitComboBox();
            }
            else
            {
                Point point = new Point(0, 0);
                this.Location = point;
            }
            InitButton();
            InitText();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 컨트롤초기화

        /// <summary>
        /// 반출담당자,승인자 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //-- 기본값 입력 --//

                // 반출담당자 아이디 설정
                this.uTextCOutChargeID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCOutChargeName.Text = m_resSys.GetString("SYS_USERNAME");

                // 승인자 아이디 설정
                this.uTextCarryOutAdmitID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCarryOutAdmitName.Text = m_resSys.GetString("SYS_USERNAME");

                //반출부서
                this.uTextUserDept.Text = m_resSys.GetString("SYS_DEPTNAME");

                //--등록일 반입예정일 초기화 --//
                this.uDateCreateDate.Value = DateTime.Now;
                this.uDateCarryInEstDate.Value = DateTime.Now;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox, GroupBoxType.INFO, "반출등록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCreateDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCOutCharge, "반출담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUserDept, "반출부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelECOutGubun, "반출구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCarryOutEtcDesc, "기타", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelECInGubun, "반입구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCarryInEstDate, "반입예정일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCarryOutReason, "반출사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCarryOut, "반출처", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCOutChargeID, "반출처담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelCarryOutAdmit, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCarryOutList, "반출내역", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinComboEditor wCom = new QRPCOM.QRPUI.WinComboEditor();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                //---공장 콤보 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCom.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                //---반출처 콤보 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                DataTable dtVendor = clsVendor.mfReadVendorPopup(m_resSys.GetString("SYS_LANG"));

                wCom.mfSetComboEditor(this.uComboCarryOut, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "VendorCode", "VendorName", dtVendor);

                //기본값 설정
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCarryOutList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Extended, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "DocCode", "등록번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "RepairGICode", "수리출고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "ItemGubunCode", "설비구성품구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "InventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "ItemCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "ItemName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");
                
                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "Qty", "재고수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "CarryOutQty", "반출수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutList, 0, "CarryOutEtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //설비구성품구분 콤보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsSYS = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsSYS);

                DataTable dtCommon = clsSYS.mfReadCommonCode("C0015", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridCarryOutList, 0, "ItemGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "선택", dtCommon);

                // Check열 DataType 설정
                this.uGridCarryOutList.DisplayLayout.Bands[0].Columns["Check"].DataType = typeof(Boolean);

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridCarryOutList, 0);

                // Set FontSize
                this.uGridCarryOutList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCarryOutList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region 툴바

        public void mfSearch()
        {

        }
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                int intRowCheck = 0;

                #region 필수입력사항확인

                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                if (this.uTextCOutChargeID.Text == "" || this.uTextCOutChargeName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "반출담당자를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextCOutChargeID.Focus();
                    return;
                }
                if (this.uComboECOutGubun.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "반출구분을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboECOutGubun.DropDown();
                    return;
                }
                if (this.uComboECInGubun.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "반입구분을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboECInGubun.DropDown();
                    return;
                }
                if (this.uComboCarryOut.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "반출처를 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboCarryOut.DropDown();
                    return;
                }
                if (this.uDateCarryInEstDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "반입예정일을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateCarryInEstDate.DropDown();
                    return;
                }
                if (this.uTextCarryOutAdmitID.Text == "" || this.uTextCarryOutAdmitName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "승인자를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextCarryOutAdmitID.Focus();
                    return;
                }
                //if (this.uComboCarryOutUser.Value.ToString() == "" || this.uTextCarryOutNum.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                               , "확인창", "필수입력사항 확인", "반출처담당자를 입력해주세요", Infragistics.Win.HAlign.Right);


                //    return;
                //}


                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridCarryOutList.Rows.Count; i++)
                {
                    this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[0].Cells[0];

                    if (this.uGridCarryOutList.Rows[i].Hidden == false)
                    {
                        if (this.uGridCarryOutList.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            intRowCheck = intRowCheck + 1;

                            if (this.uGridCarryOutList.Rows[i].Cells["ItemGubunCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", this.uGridCarryOutList.Rows[i].RowSelectorNumber + "번째 열의 설비구성품구분을 선택해주세요", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[i].Cells["ItemGubunCode"];
                                this.uGridCarryOutList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGridCarryOutList.Rows[i].Cells["InventoryCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", this.uGridCarryOutList.Rows[i].RowSelectorNumber + "번째 열의 창고를 선택해주세요", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[i].Cells["InventoryCode"];
                                this.uGridCarryOutList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            

                            if (this.uGridCarryOutList.Rows[i].Cells["ItemCode"].Value.ToString() == ""||
                                this.uGridCarryOutList.Rows[i].Cells["ItemName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", this.uGridCarryOutList.Rows[i].RowSelectorNumber + "번째 열의 자재를 선택해주세요", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[i].Cells["ItemName"];
                                this.uGridCarryOutList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGridCarryOutList.Rows[i].Cells["CarryOutQty"].Value.ToString() == "0" ||
                                this.uGridCarryOutList.Rows[i].Cells["CarryOutQty"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", this.uGridCarryOutList.Rows[i].RowSelectorNumber + "번째 열의 반출수량을 입력해주세요", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[i].Cells["CarryOutQty"];
                                this.uGridCarryOutList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }                            
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "하나 이상의 행을 입력하셔야 합니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                #endregion

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                #region 헤더저장

                //----- 헤더 값 저장 -----//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryOutH), "CarryOutH");
                QRPEQU.BL.EQUCAR.CarryOutH clsCarryOutH = new QRPEQU.BL.EQUCAR.CarryOutH();
                brwChannel.mfCredentials(clsCarryOutH);

                DataTable dtCarryOutH = clsCarryOutH.mfSetDataInfo();

                DataRow drOutH;

                drOutH = dtCarryOutH.NewRow();
                drOutH["PlantCode"] = this.uComboPlant.Value.ToString(); //공장코드
                drOutH["DocCode"] = this.uGridCarryOutList.Rows[0].Cells["DocCode"].Value.ToString();//문서코드
                drOutH["RepairGICode"] = this.uGridCarryOutList.Rows[0].Cells["RepairGICode"].Value.ToString();//수리출고코드
                drOutH["CarryOutWriteDate"] = this.uDateCreateDate.DateTime.Date.ToString("yyyy-MM-dd");
                drOutH["COutChargeID"] = this.uTextCOutChargeID.Value.ToString();       //반출담당자id
                drOutH["AssetChargeID"] = "";
                drOutH["ECOutGubunCode"] = this.uComboECOutGubun.Value.ToString();      //반출구분코드
                drOutH["CarryOutEtcDesc"] = this.uTextCarryOutEtcDesc.Text;             //반출기타
                drOutH["CarryOutReason"] = this.uTextCarryOutReason.Text;               //반출사유
                drOutH["VendorCode"] = this.uComboCarryOut.Value.ToString();            //거래선코드 
                drOutH["VendorChargeName"] = this.uComboCarryOutUser.Value.ToString(); //반출처담당자명
                drOutH["COutVendorTel"] = this.uTextCarryOutNum.Text;                   //반출처전화번호
                drOutH["ECInGubunCode"] = this.uComboECInGubun.Value.ToString();        //반입구분코드
                drOutH["CarryInEstDate"] = Convert.ToDateTime(this.uDateCarryInEstDate.Value).ToString("yyyy-MM-dd");   //반입예정일

                drOutH["CarryOutAdmitID"] = this.uTextCarryOutAdmitID.Text;             //반출승인자
                dtCarryOutH.Rows.Add(drOutH);

                #endregion

                #region 상세정보 저장
                //--------------------- 상세정보 저장 및 확인------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryOutD), "CarryOutD");
                QRPEQU.BL.EQUCAR.CarryOutD clsCarryOutD = new QRPEQU.BL.EQUCAR.CarryOutD();
                brwChannel.mfCredentials(clsCarryOutD);

                DataTable dtCarryOutD = clsCarryOutD.mfDataSetInfo();

                if (this.uGridCarryOutList.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridCarryOutList.Rows.Count; i++)
                    {
                        if (this.uGridCarryOutList.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridCarryOutList.Rows[i].Hidden == false)
                            {
                                
                                DataRow drOutD;
                                drOutD = dtCarryOutD.NewRow();
                                drOutD["Seq"] = this.uGridCarryOutList.Rows[i].RowSelectorNumber;
                                drOutD["ItemGubunCode"] = this.uGridCarryOutList.Rows[i].Cells["ItemGubunCode"].Value.ToString(); //품목구분코드
                                drOutD["ItemCode"] = this.uGridCarryOutList.Rows[i].Cells["ItemCode"].Value.ToString();             //품목코드
                                drOutD["ItemName"] = this.uGridCarryOutList.Rows[i].Cells["ItemName"].Value.ToString();             //품목명
                                drOutD["LotNo"] = this.uGridCarryOutList.Rows[i].Cells["LotNo"].Value.ToString();             //LotNo
                                drOutD["ItemSpec"] = "";                   //품목규격
                                drOutD["CarryOutQty"] = this.uGridCarryOutList.Rows[i].Cells["CarryOutQty"].Value.ToString();       //반출수량
                                drOutD["UnitCode"] = this.uGridCarryOutList.Rows[i].Cells["UnitCode"].Value.ToString();             // 단위코드
                                drOutD["CarryOutEtcDesc"] = this.uGridCarryOutList.Rows[i].Cells["CarryOutEtcDesc"].Value.ToString(); //특이사항
                                drOutD["InventoryCode"] = this.uGridCarryOutList.Rows[i].Cells["InventoryCode"].Value.ToString();             //품목명
                                dtCarryOutD.Rows.Add(drOutD);

                            }
                        }
                    }
                }
                dtCarryOutH.Rows[0]["RepairGICode"] = this.uGridCarryOutList.Rows[0].Cells["RepairGICode"].Value.ToString();//수리출고코드

                #endregion

                //-------------------------------------필 수 확 인 끝 ---------------------------------//
                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    
                    //반출등록 저장 매서드
                    string strErrRtn = clsCarryOutH.mfSaveCarryOutH(dtCarryOutH, dtCarryOutD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));                    

                    ///---Decoding---//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--커서변경--//
                    this.MdiParent.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        
                        InitControl();

                        if (FormName != null)
                        {
                            FormName = null;
                            CommonControl cControl = new CommonControl();
                            Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                            Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                            uTabMenu.Tabs["QRPEQU" + "," + "frmEQU0012"].Close();
                        }
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfDelete()
        {

        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                InitText();
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Value = "";

                this.uTextCarryOutReason.Text = "";
                this.uTextCarryOutEtcDesc.Text = "";

                while (this.uGridCarryOutList.Rows.Count > 0)
                {
                    this.uGridCarryOutList.Rows[0].Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }
        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridCarryOutList.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridCarryOutList);

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        // 행삭제 버튼 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            DeleteRow();
        }

        #region 헤더이벤트

        //공장에 따라 반출구분,반입구분,콤보바뀜
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinComboEditor wCom = new QRPCOM.QRPUI.WinComboEditor();
                WinGrid grd = new WinGrid();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                //공장저장//
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strPlant = this.uComboPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                while (this.uGridCarryOutList.Rows.Count > 0)
                {
                    this.uGridCarryOutList.Rows[0].Delete(false);
                }

                if (!strChk.Equals(string.Empty))
                {

                    
                    

                    this.uComboECOutGubun.Items.Clear();
                    this.uComboECInGubun.Items.Clear();
                    //thus.uComboCarryOut

                    

                    //--------반입구분정보 ---////
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCarryInGubun), "EquipCarryInGubun");
                    QRPMAS.BL.MASEQU.EquipCarryInGubun clsEquipCarryInGubun = new QRPMAS.BL.MASEQU.EquipCarryInGubun();
                    brwChannel.mfCredentials(clsEquipCarryInGubun);

                    DataTable dtInGubun = clsEquipCarryInGubun.mfReadMASEquipCarryInGubunCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboECInGubun, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                        , "", "", "선택", "EquipCarryInGubunCode", "EquipCarryInGubunName", dtInGubun);

                    //--------반출 구분 정보 -----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCarryOutGubun), "EquipCarryOutGubun");
                    QRPMAS.BL.MASEQU.EquipCarryOutGubun clsEquipCarryOutGubun = new QRPMAS.BL.MASEQU.EquipCarryOutGubun();
                    brwChannel.mfCredentials(clsEquipCarryOutGubun);

                    DataTable dtOutGubun = clsEquipCarryOutGubun.mfReadMASEquipCarryOutGubunCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboECOutGubun, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                        , "", "", "선택", "EquipCarryOutGubunCode", "EquipCarryOutGubunName", dtOutGubun);


                

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //반출처 가바뀌면 반출처에 해당하는 담당자명이바뀜
        private void uComboCarryOut_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //반출처 그리드 콤보박스 설정
                QRPCOM.QRPUI.WinComboGrid wComboGrid = new QRPCOM.QRPUI.WinComboGrid();
                // 그리드 콤보 일반설정
                wComboGrid.mfInitGeneralComboGrid(this.uComboCarryOutUser, true, false, true, true, "PersonName", "PersonName", "선택", Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                    , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default, Infragistics.Win.UltraWinGrid.SelectType.Default
                    , Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 콤보 컬럼설정
                wComboGrid.mfSetComboGridColumn(this.uComboCarryOutUser, 0, "PersonName", "담당자", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboGrid.mfSetComboGridColumn(this.uComboCarryOutUser, 0, "Tel", "연락처", false, 120, false, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                DataTable dtVendorP = clsVendor.mfReadVendorP(this.uComboCarryOut.Value.ToString());

                this.uComboCarryOutUser.DataSource = dtVendorP;
                this.uComboCarryOutUser.DataBind();
                //this.uComboCarryOutUser
                if (this.uTextCarryOutNum.Text != "")
                {
                    this.uTextCarryOutNum.Text = "";
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //---반출처담당자 선택에 따라 연락처텍스트 값이 변화 ---//
        private void uComboCarryOutUser_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                this.uTextCarryOutNum.Text = e.Row.Cells["Tel"].Value.ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //--반출처담당자 공백일시 연락처텍스트 텍스트 공백처리 ---//
        private void uComboCarryOutUser_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //---Value 가 공백일경우 연락처텍스트가 공백이아니면 공백처리 ---//
                if (this.uComboCarryOutUser.Value.ToString() == "" || this.uComboCarryOutUser.Value.ToString() == "선택")
                {
                    if (this.uTextCarryOutNum.Text != "")
                    {
                        this.uTextCarryOutNum.Text = "";
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //-----반출처 먼저 선택하지않고 담당자 선택시 메세지박스 ---//
        private void uComboCarryOutUser_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                if (this.uComboCarryOut.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "반출처를 선택해주세요", Infragistics.Win.HAlign.Right);
                    e.Cancel = true;

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //---반출담당자 엔터 키누를 시 아이디 검색 ---//
        private void uTextCOutChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextCOutChargeName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //반출담당자 공장코드 저장
                    string strCOutChargeID = this.uTextCOutChargeID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strCOutChargeID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCOutChargeID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCOutChargeID, m_resSys.GetString("SYS_LANG"));
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextCOutChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextUserDept.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "조회결과 확인", "입력하신 ID가 존재하지않습니다.", Infragistics.Win.HAlign.Right);

                        this.uTextCOutChargeName.Clear();
                        this.uTextUserDept.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //--승인자 엔터 키누를시 아이디 검색 ---//
        private void uTextCarryOutAdmitID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextCarryOutAdmitName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //승인자자 공장코드 저장
                    string strCarryOutAdmitID = this.uTextCarryOutAdmitID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strCarryOutAdmitID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCarryOutAdmitID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCarryOutAdmitID, m_resSys.GetString("SYS_LANG"));
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextCarryOutAdmitName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "조회 결과확인", "입력하신 ID가 존재하지않습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextCarryOutAdmitName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //---반출담당자 EditorButton클릭시 아이디 검색창 띄어준다 ---//
        private void uTextCOutChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                this.uTextCOutChargeID.Text = frmUser.UserID;
                this.uTextCOutChargeName.Text = frmUser.UserName;
                this.uTextUserDept.Text = frmUser.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //---승인자 EditorButton클릭시 아이디 검색창 띄어준다 ---//
        private void uTextCarryOutAdmitID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

               

                this.uTextCarryOutAdmitID.Text = frmUser.UserID;
                this.uTextCarryOutAdmitName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //---반출처담당자 텍스트 텍스트 삭제 시 연락처 텍스트 공백처리 ---///
        private void uComboCarryOutUser_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //이름지울시 연락처도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextCarryOutNum.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 그리드이벤트

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGridCarryOutList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        private void uGridCarryOutList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                KeyEventArgs KeyEvent = new KeyEventArgs(Keys.Enter);
                QRPBrowser brwChannel = new QRPBrowser();

                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;
                // Cell 수정시 RowSelector 이미지 변경
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridCarryOutList, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }


                if (strColumn == "CarryOutQty")
                {
                    if (e.Cell.Row.Cells["CarryOutQty"].Value.ToString() == "")
                        return;

                    if (e.Cell.Row.Cells["LotNo"].Value.ToString() != "")
                        return;

                    int intCarryOutQty = Convert.ToInt32(e.Cell.Row.Cells["CarryOutQty"].Value.ToString()); //입력수량
                    int intQty = Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value.ToString());     //재고수량

                    if (intCarryOutQty > intQty)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "교체수량 확인", "입력 하신 사용량이 재고량보다 많습니다.", Infragistics.Win.HAlign.Right);

                        e.Cell.Row.Cells["CarryOutQty"].Value = 0;

                        this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["CarryOutQty"];
                        this.uGridCarryOutList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }                    
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //---자재코드 셀의 버튼 클릭 시 발생---//
        private void uGridCarryOutList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            ////try
            ////{
            ////    //SystemResourceInfo
            ////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////    string strPlantCode = this.uComboPlant.Value.ToString();
            ////    if (strFormName == null)
            ////    {
            ////        //---설비구성품구분이 공백이아닌경우 시행--//
            ////        if (e.Cell.Row.Cells["ItemGubunCode"].Value.ToString() != "")
            ////        {
            ////            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

            ////            //--설비구성품구분이 금형치공구일경우 금형치공구 팝업창을 띄운다.--//
            ////            if (e.Cell.Row.Cells["ItemGubunCode"].Value.ToString() == "TL")
            ////            {
            ////                QRPEQU.UI.frmPOP0007 frmDurableMat = new QRPEQU.UI.frmPOP0007();
            ////                frmDurableMat.PlantCode = strPlantCode;
            ////                frmDurableMat.ShowDialog();


            ////                //선택된 공장코드와 현화면의 공장이 틀릴 시 메세지 박스
            ////                if (frmDurableMat.PlantCode != "" && strPlantCode != frmDurableMat.PlantCode)
            ////                {
            ////                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                                       "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);
            ////                    return;
            ////                }

            ////                //--팝업창에서 자재코드와 자재명을 가져와 그리드안의 자재코드,자재명에 삽입 --//
            ////                e.Cell.Row.Cells["ItemCode"].Value = frmDurableMat.DurableMatName;
            ////                e.Cell.Row.Cells["ItemName"].Value = frmDurableMat.DurableMatName;

            ////            }
            ////            //--설비구성품구분이 SparePart일경우 SparePart팝업창을 띄운다--//
            ////            else if (e.Cell.Row.Cells["ItemGubunCode"].Value.ToString() == "SP")
            ////            {
            ////                QRPEQU.UI.frmPOP0008 frmSparePart = new QRPEQU.UI.frmPOP0008();
            ////                frmSparePart.PlantCode = strPlantCode;
            ////                frmSparePart.ShowDialog();

            ////                //선택된 공장코드와 현화면의 공장이 틀릴 시 메세지 박스
            ////                if (strPlantCode != frmSparePart.PlantCode)
            ////                {
            ////                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                                       "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);
            ////                    return;
            ////                }

            ////                //--팝업창에서 자재코드와 자재명을 가져와 그리드안의 자재코드,자재명에 삽입 --//
            ////                e.Cell.Row.Cells["ItemCode"].Value = frmSparePart.SparePartCode;
            ////                e.Cell.Row.Cells["ItemName"].Value = frmSparePart.SparePartName;

            ////            }
            ////        }
            ////        //-- 공백일 경우 메세지 박스 보여준다 --//
            ////        else
            ////        {
            ////            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

            ////            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                                       "확인창", "입력사항확인", "설비구성품구분을 선택해주세요.", Infragistics.Win.HAlign.Right);

            ////            this.uGridCarryOutList.ActiveCell = this.uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["ItemGubunCode"];
            ////            this.uGridCarryOutList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
            ////            return;
            ////        }

            ////    }

            ////}
            ////catch (Exception ex)
            ////{
            ////}
            ////finally
            ////{
            ////}
        }

        #endregion

        /// <summary>
        /// 체크된 행 삭제 Method
        /// </summary>
        private void DeleteRow()
        {
            try
            {
                for (int i = 0; i < this.uGridCarryOutList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridCarryOutList.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridCarryOutList.Rows[i].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// Display 클리어
        /// </summary>
        private void InitControl()
        {
            try
            {
                InitText();
                this.uComboPlant.Value = "";
                this.uComboCarryOut.Value = "";
                this.uTextCarryOutEtcDesc.Text = "";
                this.uTextCarryOutReason.Text = "";

                this.uGridCarryOutList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridCarryOutList.Rows.All);
                this.uGridCarryOutList.DeleteSelectedRows(false);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 반출등록하려는 폼에서 정보를 받아 Display
        /// </summary>
        /// <param name="dtCarryOut">반출등록정보</param>
        private void CarryOutDisPlay(DataTable dtCarryOut)
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //--- 그리드 새로운 줄 생성 안함 , 선택헤더의 체크박스 사용안함
                this.uGridCarryOutList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGridCarryOutList.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //--공장 콤보에 공장코드 저장 --//
                string strPlantCode = dtCarryOut.Rows[0]["PlantCode"].ToString();
                this.uComboPlant.Value = strPlantCode;
                this.uComboPlant.ReadOnly = true;

                //-----금형치공구창고정보 콤보-----//
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //--외부 선택한 정보 그리드에 바인드 --//
                this.uGridCarryOutList.DataSource = dtCarryOut;
                this.uGridCarryOutList.DataBind();
                QRPGlobal grdImg = new QRPGlobal();


                for (int i = 0; i < this.uGridCarryOutList.Rows.Count; i++)
                {
                    //--바인드된 모든 줄 편집이미지 처리 --//
                    this.uGridCarryOutList.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                    this.uGridCarryOutList.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //-----금형치공구창고정보-----//
                    wGrid.mfSetGridCellValueList(this.uGridCarryOutList, i, "InventoryCode", "", "", dtDurableInven);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //화면을 닫기전 그리드의 속성을 저장, 외부정보가 있는 상태에서는 취소여부를 묻는다.
        private void frmEQU0012_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (strFormName != null)
                {
                    //Mdi폼에 있는 컨틀롤중 탭컨트롤을 찾아 저장
                    CommonControl cControl = new CommonControl();
                    Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                    Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;

                    //지금 선택되어있는 탭이 반출등록일시 
                    //if (uTabMenu.SelectedTab == uTabMenu.Tabs["QRPEQU,frmEQU0012"])
                    //{
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "확인창", "저장 확인", "반출등록을 취소 하시겠습니까?(취소시 등록되지않습니다.)", Infragistics.Win.HAlign.Right) == DialogResult.No)
                        {

                            Form MdiParentName = this.MdiParent;
                            FormName = null;
                            DataTable dtCarryOutGrid = (DataTable)this.uGridCarryOutList.DataSource;

                            QRPEQU.UI.frmEQU0012 frmCarryOut = new QRPEQU.UI.frmEQU0012(dtCarryOutGrid);


                            //현재 탭의 키를 임시로 키를 변경한뒤 변경한 탭이 닫히고 지금정보의 폼을 띄우며 탭을 생성
                            if (uTabMenu.Tabs.Exists("QRPEQU" + "," + "frmEQU0012"))
                            {

                                uTabMenu.Tabs["QRPEQU" + "," + "frmEQU0012"].Key = "QRPEQU" + "," + "frmEQU00122";

                            }

                            //탭에 추가
                            uTabMenu.Tabs.Add("QRPEQU" + "," + "frmEQU0012", "반출등록");
                            uTabMenu.SelectedTab = uTabMenu.Tabs["QRPEQU,frmEQU0012"];
                            frmCarryOut.FormName = this.Name;
                            frmCarryOut.AutoScroll = true;
                            frmCarryOut.MdiParent = MdiParentName;
                            frmCarryOut.ControlBox = false;
                            frmCarryOut.Dock = DockStyle.Fill;
                            frmCarryOut.FormBorderStyle = FormBorderStyle.None;
                            frmCarryOut.WindowState = FormWindowState.Normal;
                            frmCarryOut.Text = "반출등록";

                            frmCarryOut.Show();


                        }
                    //}
                }
                else
                {
                    QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                    grd.mfSaveGridColumnProperty(this);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        private void uGridCarryOutList_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinComboEditor wCom = new QRPCOM.QRPUI.WinComboEditor();
                WinGrid grd = new WinGrid();
                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();                
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString()); 

                //--컬럼 키값 저장 --//
                string strcolumn = e.Cell.Column.Key;
                string strPlantCode = this.uComboPlant.Value.ToString();

                //--- 공장 미선택 시 메세지 박스 ---//
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    e.Cell.Value = "";
                    this.uComboPlant.DropDown();
                    return;
                }

                #region 구성품구분
                //--- 컬럼키값이 ItemGubunCode 경우 실행 --//
                if (strcolumn == "ItemGubunCode" )
                {
                    
                    e.Cell.Row.Cells["InventoryCode"].Value = "";
                    e.Cell.Row.Cells["RepairGICode"].Value = "";
                    e.Cell.Row.Cells["ItemCode"].Value = "";
                    e.Cell.Row.Cells["ItemName"].Value = "";
                    e.Cell.Row.Cells["LotNo"].Value = "";
                    e.Cell.Row.Cells["Qty"].Value = "";
                    e.Cell.Row.Cells["CarryOutQty"].Value = "";
                    e.Cell.Row.Cells["UnitCode"].Value = "";
                    e.Cell.Row.Cells["UnitName"].Value = "";
                    e.Cell.Row.Cells["CarryOutEtcDesc"].Value = "";



                    string strItemGubunCode = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();

                    if (strItemGubunCode == "")
                        return;

                    
                    //--금형치공구 일시--//
                    if (strItemGubunCode == "TL")
                    {
                        //-----금형치공구창고정보-----//


                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                        QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                        brwChannel.mfCredentials(clsDurableInventory);

                        DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                        //wGrid.mfSetGridColumnValueList(this.uGridCarryOutList, 0, "InventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);
                        wGrid.mfSetGridCellValueList(this.uGridCarryOutList, e.Cell.Row.Index, "InventoryCode", "", "", dtDurableInven);
                    }
                    //-- SparePart일시 --//
                    else if (strItemGubunCode == "SP")
                    {

                        //--- 창고 그리드 콤보 변화 ---//
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                        QRPMAS.BL.MASEQU.SPInventory clsPMUseSP = new QRPMAS.BL.MASEQU.SPInventory();
                        brwChannel.mfCredentials(clsPMUseSP);

                        DataTable dtInven = clsPMUseSP.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                        //wGrid.mfSetGridColumnValueList(this.uGridCarryOutList, 0, "", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", );
                        wGrid.mfSetGridCellValueList(this.uGridCarryOutList, e.Cell.Row.Index, "InventoryCode", "", "", dtInven);

                    }
                }
                #endregion

                #region 창고
                if (strcolumn == "InventoryCode")
                {
                    e.Cell.Row.Cells["RepairGICode"].Value = "";
                    e.Cell.Row.Cells["ItemCode"].Value = "";
                    e.Cell.Row.Cells["ItemName"].Value = "";
                    e.Cell.Row.Cells["LotNo"].Value = "";
                    e.Cell.Row.Cells["Qty"].Value = "0";
                    e.Cell.Row.Cells["CarryOutQty"].Value = "0";
                    e.Cell.Row.Cells["UnitCode"].Value = "";
                    e.Cell.Row.Cells["UnitName"].Value = "";
                    e.Cell.Row.Cells["CarryOutEtcDesc"].Value = "";

                    string strItemGubunCode = e.Cell.Row.Cells["ItemGubunCode"].Value.ToString();
                    string strInventoryCode = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();

                    if (strItemGubunCode == "" || strInventoryCode == "")
                        return;

                    //--금형치공구 일시--//
                    if (strItemGubunCode == "TL")
                    {
                        //치공구현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                        brwChannel.mfCredentials(clsDurableStock);

                        //공장, 창고코드 선택시 해당되는 치공구정보 가져옴
                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                        if (dtDurableStock.Rows.Count > 0)
                        {
                            string strDropDownKey = "DurableMatCode,DurableMatName,LotNo,Qty";
                            string strDropDownName = "치공구코드,치공구,LOTNO,수량";

                            //GridList 그리드에 삽입
                            wGrid.mfSetGridCellValueGridList(this.uGridCarryOutList, 0, e.Cell.Row.Index, "ItemName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                                , "DurableMatCode", "DurableMatName", dtDurableStock);
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, "", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "치공구정보 확인", "해당창고의 치공구정보가 없습니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    //-- SparePart일시 --//
                    else if (strItemGubunCode == "SP")
                    {

                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                        DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                        if (dtChgSP.Rows.Count > 0)
                        {
                            string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName";
                            string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위";

                            //GridList 그리드에 삽입
                            wGrid.mfSetGridCellValueGridList(this.uGridCarryOutList, 0, e.Cell.Row.Index, "ItemName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                                , "SparePartCode", "SparePartName", dtChgSP);
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, "", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "SparePart정보 확인", "해당창고의 SparePart가 없습니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }

                }
                #endregion

                #region ItemName
                if (strcolumn == "ItemName")
                {
                    e.Cell.Row.Cells["LotNo"].Value = "";
                    e.Cell.Row.Cells["Qty"].Value = "0";
                    e.Cell.Row.Cells["CarryOutQty"].Value = "0";
                    e.Cell.Row.Cells["UnitCode"].Value = "";
                    e.Cell.Row.Cells["UnitName"].Value = "";
                    e.Cell.Row.Cells["CarryOutEtcDesc"].Value = "";

                    string strItemGubunCode = e.Cell.Row.Cells["ItemGubunCode"].Value.ToString();
                    string strInventoryCode = e.Cell.Row.Cells["InventoryCode"].Value.ToString();
                    string strItemCode = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();

                    if (strItemGubunCode == "" || strInventoryCode == "" || strItemCode == "")
                        return;

                    //--금형치공구 일시--//
                    if (strItemGubunCode == "TL")
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                        brwChannel.mfCredentials(clsDurableStock);

                        DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                        if (!dtStock.Rows.Count.Equals(0))
                        {
                            e.Cell.Row.Cells["ItemCode"].Value = strItemCode;

                            e.Cell.Row.Cells["LotNo"].Value = dtStock.Rows[intSelectItem]["LotNo"].ToString();
                            e.Cell.Row.Cells["Qty"].Tag = dtStock.Rows[intSelectItem]["Qty"].ToString();
                            e.Cell.Row.Cells["Qty"].Value = dtStock.Rows[intSelectItem]["Qty"].ToString();

                            e.Cell.Row.Cells["UnitCode"].Value = dtStock.Rows[intSelectItem]["UnitCode"].ToString();
                            e.Cell.Row.Cells["UnitName"].Value = dtStock.Rows[intSelectItem]["UnitName"].ToString();

                            if (!dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["CarryOutQty"].Tag = dtStock.Rows[intSelectItem]["Qty"].ToString();
                                e.Cell.Row.Cells["CarryOutQty"].Value = dtStock.Rows[intSelectItem]["Qty"].ToString();

                                e.Cell.Row.Cells["CarryOutQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                            

                            for (int i = 0; i < uGridCarryOutList.Rows.Count; i++)
                            {
                                if (uGridCarryOutList.Rows[i].Cells["ItemGubunCode"].Value.ToString() == "TL")
                                {
                                    if (!i.Equals(e.Cell.Row.Index) &&
                                        uGridCarryOutList.Rows[i].Cells["LotNo"].Value.ToString().Equals(dtStock.Rows[intSelectItem]["LotNo"].ToString()) &&
                                        !dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "확인창", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

                                        //uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
                                        uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["Qty"].Tag = 0;
                                        uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["Qty"].Value = 0;
                                        //uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                                        uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["CarryOutQty"].Tag = 0;
                                        uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["CarryOutQty"].Value = 0;

                                        e.Cell.Row.Cells["RepairGICode"].Value = "";
                                        e.Cell.Row.Cells["ItemCode"].Value = "";
                                        e.Cell.Row.Cells["ItemName"].Value = "";
                                        e.Cell.Row.Cells["LotNo"].Value = "";
                                        e.Cell.Row.Cells["Qty"].Value = "0";
                                        e.Cell.Row.Cells["CarryOutQty"].Value = "0";
                                        e.Cell.Row.Cells["UnitCode"].Value = "";
                                        e.Cell.Row.Cells["UnitName"].Value = "";
                                        e.Cell.Row.Cells["CarryOutEtcDesc"].Value = "";
                                        e.Cell.Row.Cells["CarryOutQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                        return;
                                    }
                                }
                            }
                        }

                    }
                    //-- SparePart일시 --//
                    else if (strItemGubunCode == "SP")
                    {

                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, strInventoryCode, strItemCode, m_resSys.GetString("SYS_LANG"));

                        if (dtSPStock.Rows.Count > 0)
                        {
                            e.Cell.Row.Cells["ItemCode"].Value = dtSPStock.Rows[0]["SparePartCode"];
                            e.Cell.Row.Cells["Qty"].Value = dtSPStock.Rows[0]["Qty"];
                            e.Cell.Row.Cells["Qty"].Tag = dtSPStock.Rows[0]["Qty"];

                            e.Cell.Row.Cells["UnitCode"].Value = dtSPStock.Rows[0]["UnitCode"];
                            e.Cell.Row.Cells["UnitName"].Value = dtSPStock.Rows[0]["UnitName"];
                        }

                        for (int i = 0; i < uGridCarryOutList.Rows.Count; i++)
                        {
                            if (uGridCarryOutList.Rows[i].Cells["ItemGubunCode"].Value.ToString() == "SP")
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    uGridCarryOutList.Rows[i].Cells["ItemCode"].Value.ToString().Equals(dtSPStock.Rows[intSelectItem]["SparePartCode"].ToString()) &&
                                    !dtSPStock.Rows[intSelectItem]["SparePartCode"].ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "확인창", "이미 입력한 SparePart 입니다.", Infragistics.Win.HAlign.Right);

                                    //uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
                                    uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["Qty"].Tag = 0;
                                    uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["Qty"].Value = 0;
                                    //uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                                    uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["CarryOutQty"].Tag = 0;
                                    uGridCarryOutList.Rows[e.Cell.Row.Index].Cells["CarryOutQty"].Value = 0;

                                    e.Cell.Row.Cells["RepairGICode"].Value = "";
                                    e.Cell.Row.Cells["ItemCode"].Value = "";
                                    e.Cell.Row.Cells["ItemName"].Value = "";
                                    e.Cell.Row.Cells["LotNo"].Value = "";
                                    e.Cell.Row.Cells["Qty"].Value = "0";
                                    e.Cell.Row.Cells["CarryOutQty"].Value = "0";
                                    e.Cell.Row.Cells["UnitCode"].Value = "";
                                    e.Cell.Row.Cells["UnitName"].Value = "";
                                    e.Cell.Row.Cells["CarryOutEtcDesc"].Value = "";
                                    e.Cell.Row.Cells["CarryOutQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                    return;
                                }
                            }
                        }
                    }

                }

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
