﻿namespace QRPEQU.UI
{
    partial class frmEQU0010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0010));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchEquipProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridRepairResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateRepairEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextRepairDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProblem = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCCSRequest = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelStopType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboRepairResult = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRepairResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFaultTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairEndTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairStartTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateRepairStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRepairDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextReceiptDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStageDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStage = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckHeadFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextReceiptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReceiptID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReceiptDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRepairReqCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairReqReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPKGType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPKGType = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckUrgentFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextBreakDownTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFailTime = new Infragistics.Win.Misc.UltraLabel();
            this.uTextBreakDownDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFailDay = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairReqDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCCSRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStageDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckHeadFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPKGType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUrgentFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBreakDownTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBreakDownDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqDate)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchEquipProcess
            // 
            this.uComboSearchEquipProcess.Location = new System.Drawing.Point(744, 37);
            this.uComboSearchEquipProcess.Name = "uComboSearchEquipProcess";
            this.uComboSearchEquipProcess.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipProcess.TabIndex = 6;
            this.uComboSearchEquipProcess.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquipProcess
            // 
            this.uLabelSearchEquipProcess.Location = new System.Drawing.Point(640, 36);
            this.uLabelSearchEquipProcess.Name = "uLabelSearchEquipProcess";
            this.uLabelSearchEquipProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipProcess.TabIndex = 0;
            this.uLabelSearchEquipProcess.Text = "ultraLabel1";
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(388, 36);
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchStation.TabIndex = 5;
            this.uComboSearchStation.Text = "ultraComboEditor1";
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(284, 36);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 0;
            this.uLabelSearchStation.Text = "ultraLabel1";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchArea.TabIndex = 4;
            this.uComboSearchArea.Text = "ultraComboEditor1";
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchArea.TabIndex = 0;
            this.uLabelSearchArea.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uDateSearchToDate
            // 
            this.uDateSearchToDate.Location = new System.Drawing.Point(496, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDate.TabIndex = 3;
            // 
            // ultraLabel1
            // 
            appearance30.TextHAlignAsString = "Center";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance30;
            this.ultraLabel1.Location = new System.Drawing.Point(488, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(8, 12);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            this.uDateSearchFromDate.Location = new System.Drawing.Point(388, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDate.TabIndex = 2;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(284, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 0;
            this.uLabelSearchDate.Text = "ultraLabel1";
            // 
            // uGridRepairResult
            // 
            this.uGridRepairResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRepairResult.DisplayLayout.Appearance = appearance6;
            this.uGridRepairResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRepairResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairResult.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridRepairResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairResult.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridRepairResult.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRepairResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRepairResult.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRepairResult.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridRepairResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRepairResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRepairResult.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRepairResult.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridRepairResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRepairResult.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairResult.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGridRepairResult.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridRepairResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRepairResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridRepairResult.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridRepairResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRepairResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGridRepairResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRepairResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRepairResult.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRepairResult.Location = new System.Drawing.Point(0, 100);
            this.uGridRepairResult.Name = "uGridRepairResult";
            this.uGridRepairResult.Size = new System.Drawing.Size(1070, 650);
            this.uGridRepairResult.TabIndex = 3;
            this.uGridRepairResult.Text = "ultraGrid1";
            this.uGridRepairResult.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridRepairResult_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uCheckChangeFlag);
            this.uGroupBox3.Controls.Add(this.uDateRepairEndDate);
            this.uGroupBox3.Controls.Add(this.uTextRepairDesc);
            this.uGroupBox3.Controls.Add(this.uLabelProblem);
            this.uGroupBox3.Controls.Add(this.uCheckCCSRequest);
            this.uGroupBox3.Controls.Add(this.uLabelStopType);
            this.uGroupBox3.Controls.Add(this.uComboRepairResult);
            this.uGroupBox3.Controls.Add(this.uLabelRepairResult);
            this.uGroupBox3.Controls.Add(this.uTextFaultTypeName);
            this.uGroupBox3.Controls.Add(this.uTextFaultTypeCode);
            this.uGroupBox3.Controls.Add(this.uTextRepairUserName);
            this.uGroupBox3.Controls.Add(this.uTextRepairUserID);
            this.uGroupBox3.Controls.Add(this.uLabelRepairUser);
            this.uGroupBox3.Controls.Add(this.uDateRepairEndTime);
            this.uGroupBox3.Controls.Add(this.ultraLabel3);
            this.uGroupBox3.Controls.Add(this.uDateRepairStartTime);
            this.uGroupBox3.Controls.Add(this.uDateRepairStartDate);
            this.uGroupBox3.Controls.Add(this.uLabelRepairDay);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 292);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 160);
            this.uGroupBox3.TabIndex = 5;
            // 
            // uCheckChangeFlag
            // 
            this.uCheckChangeFlag.Enabled = false;
            this.uCheckChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckChangeFlag.Location = new System.Drawing.Point(220, 76);
            this.uCheckChangeFlag.Name = "uCheckChangeFlag";
            this.uCheckChangeFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckChangeFlag.TabIndex = 27;
            this.uCheckChangeFlag.Text = "교체의뢰";
            this.uCheckChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateRepairEndDate
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairEndDate.Appearance = appearance31;
            this.uDateRepairEndDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairEndDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateRepairEndDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairEndDate.Location = new System.Drawing.Point(324, 28);
            this.uDateRepairEndDate.Name = "uDateRepairEndDate";
            this.uDateRepairEndDate.Size = new System.Drawing.Size(100, 19);
            this.uDateRepairEndDate.TabIndex = 9;
            // 
            // uTextRepairDesc
            // 
            this.uTextRepairDesc.Location = new System.Drawing.Point(148, 132);
            this.uTextRepairDesc.Name = "uTextRepairDesc";
            this.uTextRepairDesc.Size = new System.Drawing.Size(576, 21);
            this.uTextRepairDesc.TabIndex = 24;
            // 
            // uLabelProblem
            // 
            this.uLabelProblem.Location = new System.Drawing.Point(12, 132);
            this.uLabelProblem.Name = "uLabelProblem";
            this.uLabelProblem.Size = new System.Drawing.Size(132, 20);
            this.uLabelProblem.TabIndex = 23;
            this.uLabelProblem.Text = "ultraLabel2";
            // 
            // uCheckCCSRequest
            // 
            this.uCheckCCSRequest.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckCCSRequest.Enabled = false;
            this.uCheckCCSRequest.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCCSRequest.Location = new System.Drawing.Point(428, 100);
            this.uCheckCCSRequest.Name = "uCheckCCSRequest";
            this.uCheckCCSRequest.Size = new System.Drawing.Size(72, 20);
            this.uCheckCCSRequest.TabIndex = 22;
            this.uCheckCCSRequest.Text = "CCS의뢰";
            this.uCheckCCSRequest.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCCSRequest.Visible = false;
            // 
            // uLabelStopType
            // 
            this.uLabelStopType.Location = new System.Drawing.Point(12, 100);
            this.uLabelStopType.Name = "uLabelStopType";
            this.uLabelStopType.Size = new System.Drawing.Size(100, 20);
            this.uLabelStopType.TabIndex = 21;
            this.uLabelStopType.Text = "ultraLabel2";
            // 
            // uComboRepairResult
            // 
            this.uComboRepairResult.Location = new System.Drawing.Point(116, 76);
            this.uComboRepairResult.MaxLength = 50;
            this.uComboRepairResult.Name = "uComboRepairResult";
            this.uComboRepairResult.Size = new System.Drawing.Size(100, 21);
            this.uComboRepairResult.TabIndex = 20;
            this.uComboRepairResult.ValueChanged += new System.EventHandler(this.uComboRepairResult_ValueChanged);
            // 
            // uLabelRepairResult
            // 
            this.uLabelRepairResult.Location = new System.Drawing.Point(12, 76);
            this.uLabelRepairResult.Name = "uLabelRepairResult";
            this.uLabelRepairResult.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairResult.TabIndex = 19;
            this.uLabelRepairResult.Text = "ultraLabel2";
            // 
            // uTextFaultTypeName
            // 
            this.uTextFaultTypeName.Location = new System.Drawing.Point(220, 100);
            this.uTextFaultTypeName.MaxLength = 100;
            this.uTextFaultTypeName.Name = "uTextFaultTypeName";
            this.uTextFaultTypeName.Size = new System.Drawing.Size(204, 21);
            this.uTextFaultTypeName.TabIndex = 18;
            // 
            // uTextFaultTypeCode
            // 
            appearance28.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance28;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextFaultTypeCode.ButtonsRight.Add(editorButton1);
            this.uTextFaultTypeCode.Location = new System.Drawing.Point(116, 100);
            this.uTextFaultTypeCode.Name = "uTextFaultTypeCode";
            this.uTextFaultTypeCode.Size = new System.Drawing.Size(100, 21);
            this.uTextFaultTypeCode.TabIndex = 18;
            // 
            // uTextRepairUserName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Appearance = appearance36;
            this.uTextRepairUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Location = new System.Drawing.Point(220, 52);
            this.uTextRepairUserName.Name = "uTextRepairUserName";
            this.uTextRepairUserName.ReadOnly = true;
            this.uTextRepairUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserName.TabIndex = 18;
            // 
            // uTextRepairUserID
            // 
            appearance32.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance32.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance32;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairUserID.ButtonsRight.Add(editorButton2);
            this.uTextRepairUserID.Location = new System.Drawing.Point(116, 52);
            this.uTextRepairUserID.MaxLength = 20;
            this.uTextRepairUserID.Name = "uTextRepairUserID";
            this.uTextRepairUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserID.TabIndex = 11;
            // 
            // uLabelRepairUser
            // 
            this.uLabelRepairUser.Location = new System.Drawing.Point(12, 52);
            this.uLabelRepairUser.Name = "uLabelRepairUser";
            this.uLabelRepairUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairUser.TabIndex = 6;
            this.uLabelRepairUser.Text = "ultraLabel2";
            // 
            // uDateRepairEndTime
            // 
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateRepairEndTime.ButtonsRight.Add(spinEditorButton1);
            this.uDateRepairEndTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairEndTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateRepairEndTime.Location = new System.Drawing.Point(428, 28);
            this.uDateRepairEndTime.MaskInput = "{LOC}hh:mm";
            this.uDateRepairEndTime.Name = "uDateRepairEndTime";
            this.uDateRepairEndTime.Size = new System.Drawing.Size(80, 21);
            this.uDateRepairEndTime.TabIndex = 10;
            this.uDateRepairEndTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uDateTime_EditorSpinButtonClick);
            // 
            // ultraLabel3
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance2;
            this.ultraLabel3.Location = new System.Drawing.Point(304, 32);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 17);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "~";
            // 
            // uDateRepairStartTime
            // 
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateRepairStartTime.ButtonsRight.Add(spinEditorButton2);
            this.uDateRepairStartTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairStartTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateRepairStartTime.Location = new System.Drawing.Point(220, 28);
            this.uDateRepairStartTime.MaskInput = "{LOC}hh:mm";
            this.uDateRepairStartTime.Name = "uDateRepairStartTime";
            this.uDateRepairStartTime.Size = new System.Drawing.Size(80, 21);
            this.uDateRepairStartTime.TabIndex = 8;
            this.uDateRepairStartTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uDateTime_EditorSpinButtonClick);
            // 
            // uDateRepairStartDate
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairStartDate.Appearance = appearance20;
            this.uDateRepairStartDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairStartDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateRepairStartDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairStartDate.Location = new System.Drawing.Point(116, 28);
            this.uDateRepairStartDate.Name = "uDateRepairStartDate";
            this.uDateRepairStartDate.Size = new System.Drawing.Size(100, 19);
            this.uDateRepairStartDate.TabIndex = 7;
            // 
            // uLabelRepairDay
            // 
            this.uLabelRepairDay.Location = new System.Drawing.Point(12, 28);
            this.uLabelRepairDay.Name = "uLabelRepairDay";
            this.uLabelRepairDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairDay.TabIndex = 0;
            this.uLabelRepairDay.Text = "ultraLabel2";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uTextReceiptDesc);
            this.uGroupBox2.Controls.Add(this.uLabelUnusual);
            this.uGroupBox2.Controls.Add(this.uTextStageDesc);
            this.uGroupBox2.Controls.Add(this.uLabelStage);
            this.uGroupBox2.Controls.Add(this.uCheckHeadFlag);
            this.uGroupBox2.Controls.Add(this.uTextReceiptName);
            this.uGroupBox2.Controls.Add(this.uTextReceiptID);
            this.uGroupBox2.Controls.Add(this.uLabelAcceptUser);
            this.uGroupBox2.Controls.Add(this.uTextReceiptDate);
            this.uGroupBox2.Controls.Add(this.uLabelAcceptDate);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 180);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBox2.TabIndex = 4;
            // 
            // uTextReceiptDesc
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptDesc.Appearance = appearance17;
            this.uTextReceiptDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptDesc.Location = new System.Drawing.Point(116, 76);
            this.uTextReceiptDesc.Name = "uTextReceiptDesc";
            this.uTextReceiptDesc.ReadOnly = true;
            this.uTextReceiptDesc.Size = new System.Drawing.Size(604, 21);
            this.uTextReceiptDesc.TabIndex = 19;
            // 
            // uLabelUnusual
            // 
            this.uLabelUnusual.Location = new System.Drawing.Point(12, 76);
            this.uLabelUnusual.Name = "uLabelUnusual";
            this.uLabelUnusual.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnusual.TabIndex = 18;
            this.uLabelUnusual.Text = "ultraLabel3";
            // 
            // uTextStageDesc
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStageDesc.Appearance = appearance33;
            this.uTextStageDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStageDesc.Location = new System.Drawing.Point(116, 52);
            this.uTextStageDesc.Name = "uTextStageDesc";
            this.uTextStageDesc.ReadOnly = true;
            this.uTextStageDesc.Size = new System.Drawing.Size(100, 21);
            this.uTextStageDesc.TabIndex = 17;
            // 
            // uLabelStage
            // 
            this.uLabelStage.Location = new System.Drawing.Point(12, 52);
            this.uLabelStage.Name = "uLabelStage";
            this.uLabelStage.Size = new System.Drawing.Size(100, 20);
            this.uLabelStage.TabIndex = 16;
            this.uLabelStage.Text = "ultraLabel2";
            // 
            // uCheckHeadFlag
            // 
            this.uCheckHeadFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckHeadFlag.Enabled = false;
            this.uCheckHeadFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckHeadFlag.Location = new System.Drawing.Point(276, 52);
            this.uCheckHeadFlag.Name = "uCheckHeadFlag";
            this.uCheckHeadFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckHeadFlag.TabIndex = 15;
            this.uCheckHeadFlag.Text = "Head";
            this.uCheckHeadFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextReceiptName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptName.Appearance = appearance22;
            this.uTextReceiptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptName.Location = new System.Drawing.Point(484, 28);
            this.uTextReceiptName.Name = "uTextReceiptName";
            this.uTextReceiptName.ReadOnly = true;
            this.uTextReceiptName.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptName.TabIndex = 9;
            // 
            // uTextReceiptID
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptID.Appearance = appearance29;
            this.uTextReceiptID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptID.Location = new System.Drawing.Point(380, 28);
            this.uTextReceiptID.Name = "uTextReceiptID";
            this.uTextReceiptID.ReadOnly = true;
            this.uTextReceiptID.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptID.TabIndex = 8;
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(276, 28);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelAcceptUser.TabIndex = 7;
            this.uLabelAcceptUser.Text = "ultraLabel2";
            // 
            // uTextReceiptDate
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptDate.Appearance = appearance19;
            this.uTextReceiptDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptDate.Location = new System.Drawing.Point(116, 28);
            this.uTextReceiptDate.Name = "uTextReceiptDate";
            this.uTextReceiptDate.ReadOnly = true;
            this.uTextReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptDate.TabIndex = 6;
            // 
            // uLabelAcceptDate
            // 
            this.uLabelAcceptDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelAcceptDate.Name = "uLabelAcceptDate";
            this.uLabelAcceptDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelAcceptDate.TabIndex = 5;
            this.uLabelAcceptDate.Text = "ultraLabel2";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextRepairReqCode);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Controls.Add(this.uTextRepairReqReason);
            this.uGroupBox1.Controls.Add(this.uLabelRequestReason);
            this.uGroupBox1.Controls.Add(this.uTextPKGType);
            this.uGroupBox1.Controls.Add(this.uLabelPKGType);
            this.uGroupBox1.Controls.Add(this.uCheckUrgentFlag);
            this.uGroupBox1.Controls.Add(this.uTextProductName);
            this.uGroupBox1.Controls.Add(this.uTextProductCode);
            this.uGroupBox1.Controls.Add(this.uLabelProduct);
            this.uGroupBox1.Controls.Add(this.uTextBreakDownTime);
            this.uGroupBox1.Controls.Add(this.uLabelFailTime);
            this.uGroupBox1.Controls.Add(this.uTextBreakDownDate);
            this.uGroupBox1.Controls.Add(this.uLabelFailDay);
            this.uGroupBox1.Controls.Add(this.uTextRepairReqName);
            this.uGroupBox1.Controls.Add(this.uTextRepairReqID);
            this.uGroupBox1.Controls.Add(this.uLabelRequestUser);
            this.uGroupBox1.Controls.Add(this.uTextRepairReqDate);
            this.uGroupBox1.Controls.Add(this.uLabelRequestDate);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 160);
            this.uGroupBox1.TabIndex = 3;
            // 
            // uTextRepairReqCode
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqCode.Appearance = appearance27;
            this.uTextRepairReqCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqCode.Location = new System.Drawing.Point(116, 28);
            this.uTextRepairReqCode.Name = "uTextRepairReqCode";
            this.uTextRepairReqCode.ReadOnly = true;
            this.uTextRepairReqCode.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqCode.TabIndex = 20;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(12, 28);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(100, 20);
            this.uLabel2.TabIndex = 19;
            this.uLabel2.Text = "ultraLabel2";
            // 
            // uTextRepairReqReason
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqReason.Appearance = appearance15;
            this.uTextRepairReqReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqReason.Location = new System.Drawing.Point(116, 124);
            this.uTextRepairReqReason.Name = "uTextRepairReqReason";
            this.uTextRepairReqReason.ReadOnly = true;
            this.uTextRepairReqReason.Size = new System.Drawing.Size(608, 21);
            this.uTextRepairReqReason.TabIndex = 18;
            // 
            // uLabelRequestReason
            // 
            this.uLabelRequestReason.Location = new System.Drawing.Point(12, 124);
            this.uLabelRequestReason.Name = "uLabelRequestReason";
            this.uLabelRequestReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestReason.TabIndex = 17;
            this.uLabelRequestReason.Text = "ultraLabel2";
            // 
            // uTextPKGType
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPKGType.Appearance = appearance23;
            this.uTextPKGType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPKGType.Location = new System.Drawing.Point(520, 100);
            this.uTextPKGType.Name = "uTextPKGType";
            this.uTextPKGType.ReadOnly = true;
            this.uTextPKGType.Size = new System.Drawing.Size(204, 21);
            this.uTextPKGType.TabIndex = 16;
            // 
            // uLabelPKGType
            // 
            this.uLabelPKGType.Location = new System.Drawing.Point(416, 100);
            this.uLabelPKGType.Name = "uLabelPKGType";
            this.uLabelPKGType.Size = new System.Drawing.Size(100, 20);
            this.uLabelPKGType.TabIndex = 15;
            this.uLabelPKGType.Text = "ultraLabel2";
            // 
            // uCheckUrgentFlag
            // 
            this.uCheckUrgentFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckUrgentFlag.Enabled = false;
            this.uCheckUrgentFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckUrgentFlag.Location = new System.Drawing.Point(628, 76);
            this.uCheckUrgentFlag.Name = "uCheckUrgentFlag";
            this.uCheckUrgentFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckUrgentFlag.TabIndex = 14;
            this.uCheckUrgentFlag.Text = "긴급처리";
            this.uCheckUrgentFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextProductName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance18;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(220, 100);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(150, 21);
            this.uTextProductName.TabIndex = 13;
            // 
            // uTextProductCode
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance24;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(116, 100);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(100, 21);
            this.uTextProductCode.TabIndex = 12;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(12, 100);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelProduct.TabIndex = 11;
            this.uLabelProduct.Text = "ultraLabel2";
            // 
            // uTextBreakDownTime
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBreakDownTime.Appearance = appearance16;
            this.uTextBreakDownTime.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBreakDownTime.Location = new System.Drawing.Point(520, 76);
            this.uTextBreakDownTime.Name = "uTextBreakDownTime";
            this.uTextBreakDownTime.ReadOnly = true;
            this.uTextBreakDownTime.Size = new System.Drawing.Size(100, 21);
            this.uTextBreakDownTime.TabIndex = 8;
            // 
            // uLabelFailTime
            // 
            this.uLabelFailTime.Location = new System.Drawing.Point(416, 76);
            this.uLabelFailTime.Name = "uLabelFailTime";
            this.uLabelFailTime.Size = new System.Drawing.Size(100, 20);
            this.uLabelFailTime.TabIndex = 7;
            this.uLabelFailTime.Text = "ultraLabel2";
            // 
            // uTextBreakDownDate
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBreakDownDate.Appearance = appearance21;
            this.uTextBreakDownDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBreakDownDate.Location = new System.Drawing.Point(116, 76);
            this.uTextBreakDownDate.Name = "uTextBreakDownDate";
            this.uTextBreakDownDate.ReadOnly = true;
            this.uTextBreakDownDate.Size = new System.Drawing.Size(100, 21);
            this.uTextBreakDownDate.TabIndex = 6;
            // 
            // uLabelFailDay
            // 
            this.uLabelFailDay.Location = new System.Drawing.Point(12, 76);
            this.uLabelFailDay.Name = "uLabelFailDay";
            this.uLabelFailDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelFailDay.TabIndex = 5;
            this.uLabelFailDay.Text = "ultraLabel2";
            // 
            // uTextRepairReqName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqName.Appearance = appearance25;
            this.uTextRepairReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqName.Location = new System.Drawing.Point(624, 52);
            this.uTextRepairReqName.Name = "uTextRepairReqName";
            this.uTextRepairReqName.ReadOnly = true;
            this.uTextRepairReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqName.TabIndex = 4;
            // 
            // uTextRepairReqID
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqID.Appearance = appearance26;
            this.uTextRepairReqID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqID.Location = new System.Drawing.Point(520, 52);
            this.uTextRepairReqID.Name = "uTextRepairReqID";
            this.uTextRepairReqID.ReadOnly = true;
            this.uTextRepairReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqID.TabIndex = 3;
            // 
            // uLabelRequestUser
            // 
            this.uLabelRequestUser.Location = new System.Drawing.Point(416, 52);
            this.uLabelRequestUser.Name = "uLabelRequestUser";
            this.uLabelRequestUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestUser.TabIndex = 2;
            this.uLabelRequestUser.Text = "ultraLabel2";
            // 
            // uTextRepairReqDate
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqDate.Appearance = appearance34;
            this.uTextRepairReqDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqDate.Location = new System.Drawing.Point(116, 52);
            this.uTextRepairReqDate.Name = "uTextRepairReqDate";
            this.uTextRepairReqDate.ReadOnly = true;
            this.uTextRepairReqDate.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqDate.TabIndex = 1;
            // 
            // uLabelRequestDate
            // 
            this.uLabelRequestDate.Location = new System.Drawing.Point(12, 52);
            this.uLabelRequestDate.Name = "uLabelRequestDate";
            this.uLabelRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestDate.TabIndex = 0;
            this.uLabelRequestDate.Text = "ultraLabel2";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQU0010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridRepairResult);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0010";
            this.Load += new System.EventHandler(this.frmEQU0010_Load);
            this.Activated += new System.EventHandler(this.frmEQU0010_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCCSRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStageDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckHeadFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPKGType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUrgentFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBreakDownTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBreakDownDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepairResult;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraLabel uLabelProblem;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCCSRequest;
        private Infragistics.Win.Misc.UltraLabel uLabelStopType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairResult;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairEndTime;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairStartTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairStartDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDay;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStageDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelStage;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckHeadFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptID;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPKGType;
        private Infragistics.Win.Misc.UltraLabel uLabelPKGType;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUrgentFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBreakDownTime;
        private Infragistics.Win.Misc.UltraLabel uLabelFailTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBreakDownDate;
        private Infragistics.Win.Misc.UltraLabel uLabelFailDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqID;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairEndDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqCode;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckChangeFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultTypeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultTypeCode;
    }
}