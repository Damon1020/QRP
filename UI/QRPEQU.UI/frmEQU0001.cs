﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQU0001.cs                                         */
/* 프로그램명   : 점검계획등록(그룹별)                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-19 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Data.SqlClient;

namespace QRPEQU.UI
{
    public partial class frmEQU0001 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 

        public frmEQU0001()
        {
            InitializeComponent();
        }

        //폼이 닫히기전에 그리드 설정값을 저장시킨다
        private void frmEQU0001_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQU0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    uTabControlDay.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    uTabControlDay.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0001_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0001_Load(object sender, EventArgs e)
        {
            SetRunMode();
            SetToolAuth();
            // 초기화 함수
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            InitButton();
            InitTab();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            //this.uDateStartDay.Value = Convert.ToDateTime("2012-12-01");
            //this.uDateStartDay.ReadOnly = true;
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            //MessageBox.Show(this.Tag.ToString());
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow3, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow4, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonDeleteRow1.Hide();
                this.uButtonDeleteRow2.Hide();
                this.uButtonDeleteRow3.Hide();
                this.uButtonDeleteRow4.Hide();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 초기값 설정 Method
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Title 제목
                titleArea.mfSetLabelText("점검계획등록(그룹별)", m_resSys.GetString("SYS_FONTNAME"), 12);

                //GroupBox
                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBoxContentsArea, GroupBoxType.INFO, "계획정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                this.uGroupBoxContentsArea.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxContentsArea.Appearance.FontData.SizeInPoints = 9;

                string strPlantcode = m_resSys.GetString("SYS_PLANTCODE");
                string strUserID = m_resSys.GetString("SYS_USERID");
                

                // 계획년도 TextBox 기본값 설정
                this.uNumSearchYear.Value = DateTime.Now.Year.ToString();
                
                this.uTextUserID.Text = strUserID;
                this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "계획년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipMentGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelStartDay, "계획적용시작일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.ultraLabel1, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTechnician, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 점검항목상세(일간) 그리드 초기화
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMPlanDDay, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDDay, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfAddRowGrid(this.uGridPMPlanDDay, 0);

                // 점검항목상세(주간)
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMPlanDWeek, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDWeek, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfAddRowGrid(this.uGridPMPlanDWeek, 0);

                // 점검항목상세(월간)
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMPlanDMonth, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDMonth, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfAddRowGrid(this.uGridPMPlanDMonth, 0);

                // 점검항목상세(분기, 반기, 년간)
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMPlanDYear, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMPeriodName", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMPlanDate", "점검계획일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlanDYear, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfAddRowGrid(this.uGridPMPlanDYear, 0);

                // 점검그룹 설비리스트
                // 그리드 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uGridEquipList.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "주간점검", "주간점검", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "월간점검", "월간점검", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group3 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "분기,반기,년간", "분기,반기,년간", false);

                // 그리드 컬럼설정
                //wGrid.mfSetGridGroup(
                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false", 0, 0, 1, 2, null);

                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 2, null);

                // 그룹헤더 처리

                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "주간점검", "주간점검", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 0, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMWeekDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, group1);



                //// 그룹헤더 처리
                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "월간점검", "월간점검", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 2, 0, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 10, 0, 1, 1, group2);


                // 그룹헤더 처리
                //wGrid.mfSetGridColumn(this.ultraGrid6, 0, "분기,반기,년간", "분기,반기,년간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 3, 0, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 1, 1, group3);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 12, 0, 1, 1, group3);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 13, 0, 1, 1, group3);

                //wGrid.mfSetGridGroup(this.ultraGrid6, 0, "분기,반기,년간", "분기,반기,년간", Infragistics.Win.DefaultableBoolean.True, Color.AliceBlue, true);

                wGrid.mfAddRowGrid(this.uGridEquipList, 0);

                this.uGridPMPlanDDay.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMPlanDDay.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMPlanDWeek.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMPlanDWeek.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMPlanDMonth.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMPlanDMonth.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMPlanDYear.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMPlanDYear.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab컨트롤초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinTabControl tab = new WinTabControl();

                tab.mfInitGeneralTabControl(this.uTabControlDay, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage,
                                                Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.Default
                                                , m_resSys.GetString("SYS_FONTNAME"));

                

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                WinGrid grd = new WinGrid();

                #region 필수입력사항 확인
                //--------필수 입력 사항 확인 ------//
                if (this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000250", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uComboSearchEquipGroup.Value.ToString() == "" && this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000705", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uTextTechnicianName.Text == "")
                {


                    if (this.uComboSearchStation.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000128", Infragistics.Win.HAlign.Right);
                        this.uComboSearchStation.DropDown();
                        return;
                    }

                    if (this.uComboSearchEquipLoc.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000831", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipLoc.DropDown();
                        return;
                    }
                    if (this.uComboProcessGroup.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                  , "M001264", "M001228", "M000698", Infragistics.Win.HAlign.Right);
                        this.uComboProcessGroup.DropDown();
                        return;

                    }

                    if (this.uComboSearchEquipType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000720", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipType.DropDown();
                        return;
                    }
                    if (this.uComboSearchEquipGroup.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000695", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipGroup.DropDown();
                        return;
                    }
                }

                //-------필수 입력 사항 확인 끝 --------//
                #endregion

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strGroupCode = this.uComboSearchEquipGroup.Value.ToString();

                string strUserID = this.uTextTechnicianID.Text;
                string strYear = this.uNumSearchYear.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 예방점검계획조회
                //처리 로직//
                //BL호출
                QRPEQU.BL.EQUMGM.PMPlan clsPMPlan;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                    clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                    brwChannel.mfCredentials(clsPMPlan);
                }
                else
                    clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan(m_strDBConn);

                DataTable dtPlan = null;
                if (strGroupCode != "")
                    dtPlan = clsPMPlan.mfReadPMPlanEquipGroup
                        (strPlantCode
                        , strYear
                        , strStationCode
                        , strEquipLocCode
                        , strProcessGroup
                        , strEquipTypeCode
                        , strGroupCode);
                else
                {
                    if (strUserID != "")
                        dtPlan = clsPMPlan.mfReadPMPlanEquipWorker(strPlantCode, strYear, strUserID);
                }
                /////////////
                
                //---데이터 바인드? ---//
                if (dtPlan.Rows.Count != 0)
                {
                    this.uDateStartDay.Value = dtPlan.Rows[0]["PMApplyDate"].ToString();
                    this.uDateWrite.Value = dtPlan.Rows[0]["WriteDate"].ToString();
                    this.uTextUserID.Text = dtPlan.Rows[0]["WriteID"].ToString();
                    this.uTextUserName.Text = dtPlan.Rows[0]["UserName"].ToString();



                    //----------------------------예방점검상세정보-------------------------------//
                    QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD;
                    if (m_bolDebugMode == false)
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                        clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                        brwChannel.mfCredentials(clsPMPlanD);
                    }
                    else
                        clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD(m_strDBConn);

                    DataTable dtPlanD = null;
                    if (strGroupCode != "")
                        dtPlanD = clsPMPlanD.mfReadPMPlanDEquipGroup
                            (strPlantCode
                            , strYear
                            , strStationCode
                            , strEquipLocCode
                            , strProcessGroup
                            , strEquipTypeCode
                            , strGroupCode
                            , m_resSys.GetString("SYS_LANG"));
                    else
                    {
                        if (strUserID != "")
                            dtPlanD = clsPMPlanD.mfReadPMPlanDEquipWorker(strPlantCode, strUserID, strYear, m_resSys.GetString("SYS_LANG"));
                    }

                    if (dtPlanD.Rows.Count != 0)
                    {
                        DataTable dtCopy = new DataTable();
                        DataRow[] drCopy;


                        #region 조건에 맞은 정보가 한 줄이상 있을 시 정보를 복사하여 저장 시킨 후 바인드
                        //일간
                        if (dtPlanD.Select("PMPeriodCode = 'DAY'").Count() != 0)
                        {

                            drCopy = dtPlanD.Select("PMPeriodCode = 'DAY'"); //검색된 값을 그리드 소스에 넣을 시 줄만 들어가서 컬럼인식을 못하여 화면에 안나옴
                            dtCopy = drCopy.CopyToDataTable();  //Row가 0 일 시 에러가 남

                            this.uGridPMPlanDDay.DataSource = dtCopy;
                            this.uGridPMPlanDDay.DataBind();

                            for (int i = 0; i < this.uGridPMPlanDDay.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDDay.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDDay.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                            if (dtCopy.Rows.Count > 0)
                            {
                                grd.mfSetAutoResizeColWidth(this.uGridPMPlanDDay, 0);
                                this.uButtonDeleteRow1.Show();
                            }
                        }
                        //주간
                        if (dtPlanD.Select("PMPeriodCode = 'WEK'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'WEK'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMPlanDWeek.DataSource = dtCopy;
                            this.uGridPMPlanDWeek.DataBind();

                            for (int i = 0; i < this.uGridPMPlanDWeek.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDWeek.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDWeek.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                            if (dtCopy.Rows.Count > 0)
                            {
                                grd.mfSetAutoResizeColWidth(this.uGridPMPlanDDay, 0);
                                this.uButtonDeleteRow2.Show();
                            }
                        }
                        //월간
                        if (dtPlanD.Select("PMPeriodCode = 'MON'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'MON'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMPlanDMonth.DataSource = dtCopy;
                            this.uGridPMPlanDMonth.DataBind();


                            for (int i = 0; i < this.uGridPMPlanDMonth.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDMonth.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDMonth.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                            if (dtCopy.Rows.Count > 0)
                            {
                                grd.mfSetAutoResizeColWidth(this.uGridPMPlanDDay, 0);
                                this.uButtonDeleteRow3.Show();
                            }
                        }
                        //분기,반기,년간
                        string strExpr = "";
                        strExpr = "PMPeriodCode = 'QUA'";
                        strExpr = strExpr + " or PMPeriodCode = 'HAF'";
                        strExpr = strExpr + " or PMPeriodCode = 'YEA'";

                        if (dtPlanD.Select(strExpr).Count() != 0)
                        {
                            drCopy = dtPlanD.Select(strExpr);
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMPlanDYear.DataSource = dtCopy;
                            this.uGridPMPlanDYear.DataBind();


                            for (int i = 0; i < this.uGridPMPlanDYear.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDYear.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDYear.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                            if (dtCopy.Rows.Count > 0)
                            {
                                grd.mfSetAutoResizeColWidth(this.uGridPMPlanDDay, 0);
                                this.uButtonDeleteRow4.Show();
                            }
                        }

                    }
                    else
                    {
                        this.uGridPMPlanDDay.DataSource = dtPlanD;
                        this.uGridPMPlanDDay.DataBind();
                        this.uGridPMPlanDWeek.DataSource = dtPlanD;
                        this.uGridPMPlanDWeek.DataBind();
                        this.uGridPMPlanDMonth.DataSource = dtPlanD;
                        this.uGridPMPlanDMonth.DataBind();
                        this.uGridPMPlanDYear.DataSource = dtPlanD;
                        this.uGridPMPlanDYear.DataBind();
                    }
                    //---설비리스트---//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    //QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    //brwChannel.mfCredentials(clsEquipGroup);

                    //DataTable dtEquipList = clsEquipGroup.mfReadEquipGroupList(strPlantCode, strGroupCode, m_resSys.GetString("SYS_LANG"));

                    //this.uGridEquipList.DataSource = dtEquipList;
                    //this.uGridEquipList.DataBind();
                        #endregion

                }
                #endregion
                else
                {
                    InitDisplay();
                }
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtPlan.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            bool bolchk = false;
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();


                #region 필수입력사항 확인

                if (this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000250", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }

                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboSearchEquipGroup.Value.ToString() == "" && this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000705", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipGroup.DropDown();
                    return;
                }

                if (this.uTextTechnicianName.Text == "")
                {
                    

                    if (this.uComboSearchStation.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000128", Infragistics.Win.HAlign.Right);
                        this.uComboSearchStation.DropDown();
                        return;
                    }

                    if (this.uComboSearchEquipLoc.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000831", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipLoc.DropDown();
                        return;
                    }

                    if (this.uComboProcessGroup.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000698", Infragistics.Win.HAlign.Right);
                        this.uComboProcessGroup.DropDown();
                        return;
                    }

                    if (this.uComboSearchEquipType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000720", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipType.DropDown();
                        return;
                    }
                    if (this.uComboSearchEquipGroup.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000695", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipGroup.DropDown();
                        return;
                    }
                }

                if (this.uDateStartDay.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000249", Infragistics.Win.HAlign.Right);

                    this.uDateStartDay.DropDown();
                    return;
                }

                if (this.uTextUserID.Text == "" || this.uTextUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M001000", Infragistics.Win.HAlign.Right);

                    this.uTextUserID.Focus();
                    return;
                }
                
                if (this.uNumSearchYear.Value.ToString().Trim() != this.uDateStartDay.Value.ToString().Substring(0,4))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000251", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                //-------필수 입력 사항 확인 끝 --------//
                #endregion


                //BL보낼 값 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strUserID = this.uTextTechnicianID.Text;

                string strProcessGroupCode = this.uComboProcessGroup.Value.ToString();


                string strYear = this.uNumSearchYear.Value.ToString();
                string strStart = this.uDateStartDay.Value.ToString();
                string strWriteDate = this.uDateWrite.Value.ToString();
                string strWriteID = this.uTextUserID.Text;
                string strLang = m_resSys.GetString("SYS_LANG");

          if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001053", strLang), strYear + msg.GetMessge_Text("M000352", strLang),
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직/
                    //BL호출
                    QRPEQU.BL.EQUMGM.PMPlan clsPMPlan;
                    if (m_bolDebugMode == false)
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                        clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                        brwChannel.mfCredentials(clsPMPlan);
                    }
                    else
                        clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan(m_strDBConn);

                    ////string strTime = "";
                    ////for (int i = 0; i < 2000; i++)
                    ////    //{
                    ////    //strTime = i.ToString("D7"); //string.Format("{0:00000#}", i);

                    ////    strTime = DateTime.Now.ToString("yyyyMMddHHMMss");
                    ////    MessageBox.Show(strTime);
                    //////}   


                    System.Windows.Forms.DialogResult result;

                    DataTable dtTest = new DataTable();
                    string strErrRtn = "";
                    //설비그룹 중심으로 예방점검계획을 세운다. --> 2011-11-30 설비그룹, Station, 위치, 설비유형 추가
                    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                    //string strErrRtn = clsPMPlan.mfSavePMPlan(strPlantCode, strYear, strGroupCode, strWriteID, strWriteDate, strStart, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    {
                        #region 주석
                        ////string strConn = "data source=10.60.24.173;user id=QRPUSR;password=jinjujin;Initial Catalog=QRP_STS_DEV;persist security info=true";
                        ////SqlConnection sqlcon = new SqlConnection(strConn);
                        ////sqlcon.Open();

                        ////SqlCommand sqlcmd = new SqlCommand();
                        ////sqlcmd.Connection = sqlcon;
                        ////sqlcmd.CommandType = CommandType.StoredProcedure;

                        ////SqlParameter param = new SqlParameter();

                        ////param.ParameterName = "@i_strPlantCode";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strPlantCode;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@i_strEquipGroupCode";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strGroupCode;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@i_strStationCode";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strStationCode;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@i_strEquipLocCode";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strEquipLocCode;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@i_strProcessGroupCode";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strProcessGroupCode;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@i_strEquipLargeTypeCode";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strEquipTypeCode;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@i_strPlanYear";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Input;
                        ////param.Value = strYear;
                        ////sqlcmd.Parameters.Add(param);

                        ////param = new SqlParameter();
                        ////param.ParameterName = "@ErrorMessage";
                        ////param.SqlDbType = SqlDbType.VarChar;
                        ////param.Size = 10;
                        ////param.Direction = ParameterDirection.Output;
                        ////param.Value = "";
                        ////sqlcmd.Parameters.Add(param);


                        //////계획추가// 
                        ////sqlcmd.CommandText = "up_Delete_EQUPMPlanEquipGroup";
                        ////string strRtn = Convert.ToString(sqlcmd.ExecuteScalar());
                        ////string strResult = Convert.ToString(sqlcmd.Parameters["@ErrorMessage"].Value);
                        ////sqlcon.Close();

                        ////string[] strArr = strResult.Split(':');

                        ////if (strArr[0] != "")
                        ////{
                        ////    this.MdiParent.Cursor = Cursors.Default;
                        ////    m_ProgressPopup.mfCloseProgressPopup(this);
                        ////    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        ////                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        ////                             "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                        ////                             Infragistics.Win.HAlign.Right);
                        ////    return;
                        ////}
                        #endregion

                        strErrRtn = clsPMPlan.mfSavePMPlanEquipGroup
                            (strPlantCode 
                            , strYear
                            , strGroupCode
                            , strStationCode
                            , strEquipLocCode
                            , strProcessGroupCode
                            , strEquipTypeCode
                            , strWriteID
                            , strWriteDate
                            , strStart
                            , m_resSys.GetString("SYS_USERIP")
                            , m_resSys.GetString("SYS_USERID"));


                    }

                    //정비사 중심으로 예방점검계획을 세운다.
                    else if (this.uTextUserName.Text != "")
                        //string strErrRtn = clsPMPlan.mfSavePMPlanEquipWorker(strPlantCode, strYear, strUserID, strWriteID, strWriteDate, strStart, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                        strErrRtn = clsPMPlan.mfSavePMPlanEquipWorker
                            (strPlantCode
                            , strYear
                            , strUserID
                            , strWriteID
                            , strWriteDate
                            , strStart
                            , m_resSys.GetString("SYS_USERIP")
                            , m_resSys.GetString("SYS_USERID"));

                    TransErrRtn ErrRtn = new TransErrRtn();

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    bolchk = true;

                    if (strErrRtn.Equals(string.Empty))
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M001032", "M001492",
                                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    
                    if (ErrRtn.ErrNum == 0)
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        //mfSearch();
                    else
                    {
                        string strMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M001135", strLang),
                                    msg.GetMessge_Text("M001037", strLang), strMes,
                                    Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                if (!bolchk)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMPlanDDay.Rows.Count == 0 && this.uGridPMPlanDWeek.Rows.Count == 0 && this.uGridPMPlanDMonth.Rows.Count == 0 && this.uGridPMPlanDYear.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                    return;
                }


                #region 필수입력사항 확인

                if (this.uNumSearchYear.Value == null || this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000250", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }

                if (this.uComboSearchPlant.Value == null || this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if ((this.uComboSearchEquipGroup.Value == null || this.uComboSearchEquipGroup.Value.ToString() == "") 
                    && this.uTextTechnicianName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000705", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipGroup.DropDown();
                    return;
                }

                if (this.uTextTechnicianName.Text.Equals(string.Empty))
                {


                    if (this.uComboSearchStation.Value == null || this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000128", Infragistics.Win.HAlign.Right);
                        this.uComboSearchStation.DropDown();
                        return;
                    }

                    if (this.uComboSearchEquipLoc.Value == null || this.uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000831", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipLoc.DropDown();
                        return;
                    }

                    if (this.uComboProcessGroup.Value == null || this.uComboProcessGroup.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000698", Infragistics.Win.HAlign.Right);
                        this.uComboProcessGroup.DropDown();
                        return;
                    }

                    if (this.uComboSearchEquipType.Value == null || this.uComboSearchEquipType.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000720", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipType.DropDown();
                        return;
                    }
                    if (this.uComboSearchEquipGroup.Value == null || this.uComboSearchEquipGroup.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000695", Infragistics.Win.HAlign.Right);
                        this.uComboSearchEquipGroup.DropDown();
                        return;
                    }
                }

                if (this.uDateStartDay.Value == null || this.uDateStartDay.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000249", Infragistics.Win.HAlign.Right);

                    this.uDateStartDay.DropDown();
                    return;
                }

                if (this.uTextUserID.Text.Equals(string.Empty) || this.uTextUserName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M001000", Infragistics.Win.HAlign.Right);

                    this.uTextUserID.Focus();
                    return;
                }

                if (this.uNumSearchYear.Value.ToString().Trim() != this.uDateStartDay.Value.ToString().Substring(0, 4))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001228", "M000251", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                
                #endregion


                //BL보낼 값 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strUserID = this.uTextTechnicianID.Text;

                string strProcessGroupCode = this.uComboProcessGroup.Value.ToString();
                string strYear = this.uNumSearchYear.Value.ToString();
                string strAppDate = this.uDateStartDay.Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");

                if (msg.mfSetMessageBox(MessageBoxType.YesNo,m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000650", strLang), strYear + msg.GetMessge_Text("M000351", strLang),
                                     Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//

                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                    QRPEQU.BL.EQUMGM.PMPlan clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                    brwChannel.mfCredentials(clsPMPlan);

                    string strErrRtn = clsPMPlan.mfDeletePMPlan_All(strPlantCode,strStationCode,strEquipLocCode,strProcessGroupCode,strEquipTypeCode,strGroupCode,strYear,strAppDate);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;



                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001135", "M000638", "M000677",
                                   Infragistics.Win.HAlign.Right);
                        InitDisplay();
                        this.uComboSearchPlant.Value = "";
                    }
                    else
                    {
                        string strMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000676", strLang);
                        else
                            strMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M001135",strLang),
                                    msg.GetMessge_Text("M000638",strLang), strMes,
                                    Infragistics.Win.HAlign.Right);
                    }
                }
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀 출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ReosurceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //계획정보에 정보가 없는 경우 메세지 박스를 띄운다.
                if (this.uGridPMPlanDDay.Rows.Count == 0 && this.uGridPMPlanDMonth.Rows.Count == 0 && this.uGridPMPlanDWeek.Rows.Count == 0 && this.uGridPMPlanDYear.Rows.Count == 0 && this.uGridEquipList.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);

                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridPMPlanDDay.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMPlanDDay);

                if (this.uGridPMPlanDMonth.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMPlanDMonth);

                if (this.uGridPMPlanDWeek.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMPlanDWeek);

                if (this.uGridPMPlanDYear.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMPlanDYear);

                if (this.uGridEquipList.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipList);


                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        #region 편집이미지이벤트

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMPlanDDay_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMPlanDDay, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMPlanDWeek_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMPlanDWeek, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMPlanDMonth_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMPlanDMonth, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMPlanDYear_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMPlanDYear, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 행삭제 이벤트

        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteRow("DAY");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {

                DeleteRow("WEK");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uButtonDeleteRow3_Click(object sender, EventArgs e)
        {
            try
            {

                DeleteRow("MON");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uButtonDeleteRow4_Click(object sender, EventArgs e)
        {
            try
            {

                DeleteRow("YEA");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Combo
        //공장선택에 따라 설비그룹이 바뀜
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string strLang = m_resSys.GetString("SYS_LANG");

                    WinComboEditor com = new WinComboEditor();
                   

                    //Station정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    //Station콤보조회 BL호출
                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    com.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "StationCode", "StationName", dtStation);

                    ////////////////////////////////////////////////////////////


                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), "", "", "", "", 3);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station선택시 위치,설비대분류,설비중분류,설비그룹이바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                
                //코드와 텍스트 저장
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                //Station 코드가 공백이아닌 경우
                if (!strStationCode.Equals(string.Empty))
                {
                    this.uTextTechnicianID.Clear();
                    this.uTextTechnicianName.Clear();
                }

                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //설비위치정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLocation);

                    //설비위치정보콤보조회 매서드 실행
                    DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    WinComboEditor com = new WinComboEditor();

                    this.uComboSearchEquipLoc.Items.Clear();

                    com.mfSetComboEditor(this.uComboSearchEquipLoc, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치선택시 설비대분류,설비중분류,설비그룹이 바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                
                string strCode = this.uComboSearchEquipLoc.Value.ToString();

                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    //콤보아이템에 있는 경우
                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비대분류선택시 설비중분류 설비그룹이바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcess_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비중분류선택시 설비그룹이 바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipType.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    //콤보아이템에 해당하는 결과면
                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        #endregion

        //버튼 클릭 시 유저 정보 보여주기
        private void uTextTechnician_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보 frmUser에 보냄
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();
                
                //텍스트에 삽입
                this.uTextTechnicianID.Text = frmUser.UserID;
                this.uTextTechnicianName.Text = frmUser.UserName;

                if (!frmUser.UserID.Equals(string.Empty))
                {
                    if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                    {
                        this.uComboSearchEquipLoc.Value = "";
                        this.uComboProcessGroup.Value = "";
                        this.uComboSearchEquipType.Value = "";
                        this.uComboSearchEquipGroup.Value = "";
                    }
                    else
                        this.uComboSearchStation.Value = "";
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 ID입력 후 엔터누를 시 이름 자동입력
        private void uTextTechnician_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    if (this.uTextTechnicianID.Text == "")
                        this.uTextTechnicianName.Text = "";
                }
                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextTechnicianID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextTechnicianID.Focus();
                        this.uTextTechnicianName.Text = "";
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    //정비사를 선택한 경우 설비그룹선택은 초기화한다.
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextTechnicianName.Text = dtUser.Rows[0]["UserName"].ToString();
                        if(this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                        {
                            this.uComboSearchEquipLoc.Value = "";
                            this.uComboProcessGroup.Value = "";
                            this.uComboSearchEquipType.Value = "";
                            this.uComboSearchEquipGroup.Value = "";
                        }
                        else
                            this.uComboSearchStation.Value = "";

                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextTechnicianName.Clear();
                        this.uTextTechnicianID.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //작성자 Id입력 후 엔터 누를 시 이름 자동입력
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디를 지울경우 이름공백처리
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    if (this.uTextUserID.Text == "")
                        this.uTextUserName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strWriteID = this.uTextUserID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strWriteID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextUserID.Focus();
                        this.uTextUserName.Text = "";
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strWriteID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                        

                    }
                    //정보가 없는 경우 텍스트 박스 클리어
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextUserID.Clear();
                        this.uTextUserName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //버튼 클릭 시 유저 정보 보여주기
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 frmUser폼에 보내어 해당공장을 검색한다.
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                //텍스트에 삽입
                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTechnicianID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextTechnicianName.Text.Equals(string.Empty))
                this.uTextTechnicianName.Clear();
        }

        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextUserName.Text.Equals(string.Empty))
                this.uTextUserName.Clear();
        }

        //SpinEditorButton 증감
        private void uNumSearchYear_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //누를시 현재 년도로 바뀜
        private void uNumSearchYear_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 현재 년도로 초기화
                ed.Value = DateTime.Now.Year;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 컨트롤 기본값으로
        /// </summary>
        private void InitDisplay()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //설비리스트
                this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipList.Rows.All);
                this.uGridEquipList.DeleteSelectedRows(false);

                //일간
                this.uGridPMPlanDDay.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridPMPlanDDay.Rows.All);
                this.uGridPMPlanDDay.DeleteSelectedRows(false);

                //주간
                while (this.uGridPMPlanDWeek.Rows.Count > 0)
                {
                    this.uGridPMPlanDWeek.Rows[0].Delete(false);
                }
                //월간
                while (this.uGridPMPlanDMonth.Rows.Count > 0)
                {
                    this.uGridPMPlanDMonth.Rows[0].Delete(false);
                }
                //분기,반기,년간
                while (this.uGridPMPlanDYear.Rows.Count > 0)
                {
                    this.uGridPMPlanDYear.Rows[0].Delete(false);
                }

                //this.uNumSearchYear.Value = DateTime.Now.Year.ToString();
                
                //this.uTextTechnicianID.Text = m_resSys.GetString("SYS_USERID");
                //this.uTextTechnicianName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uButtonDeleteRow1.Hide();
                this.uButtonDeleteRow2.Hide();
                this.uButtonDeleteRow3.Hide();
                this.uButtonDeleteRow4.Hide();
                

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 행정보삭제
        /// </summary>
        /// <param name="strGubun">구분코드</param>
        private void DeleteRow(string strGubun)
        {
            try
            {
                //SystemReouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                     "M001264", "M000650", "M000673",
                     Infragistics.Win.HAlign.Right) == DialogResult.No)
                    return;
                

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                    QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                    brwChannel.mfCredentials(clsPMPlanD);

                    DataTable dtPMDel = clsPMPlanD.mfSetPMPlanDColumn();
                    //-------------------------그리드 종류에 따라 값 저장 ------------------------//
                    switch (strGubun)
                    {
                        //----------------------------일간-------------------------//
                        case "DAY":
                            //줄이 0이상 인지 판단
                            if (this.uGridPMPlanDDay.Rows.Count > 0)
                            {
                                for (int i = 0; i < this.uGridPMPlanDDay.Rows.Count; i++)
                                {
                                    //체크 된 줄의 정보만 저장
                                    if (Convert.ToBoolean(this.uGridPMPlanDDay.Rows[i].Cells["Check"].Value) == true)
                                    {
                                        DataRow drDel;
                                        drDel = dtPMDel.NewRow();
                                        drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                        drDel["PlanYear"] = this.uGridPMPlanDDay.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                        drDel["EquipCode"] = this.uGridPMPlanDDay.Rows[i].Cells["EquipCode"].Value.ToString();
                                        drDel["Seq"] = this.uGridPMPlanDDay.Rows[i].Cells["Seq"].Value.ToString();
                                        dtPMDel.Rows.Add(drDel);
                                    }
                                }
                            }
                            //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                                return;
                            }
                            break;

                        //-------------------------주간----------------------//
                        case "WEK":
                            //줄이 0이상 인지 판단
                            if (this.uGridPMPlanDWeek.Rows.Count > 0)
                            {
                                for (int i = 0; i < this.uGridPMPlanDWeek.Rows.Count; i++)
                                {
                                    //체크 된 줄의 정보만 저장
                                    if (Convert.ToBoolean(this.uGridPMPlanDWeek.Rows[i].Cells["Check"].Value) == true)
                                    {
                                        DataRow drDel;
                                        drDel = dtPMDel.NewRow();
                                        drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                        drDel["PlanYear"] = this.uGridPMPlanDWeek.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                        drDel["EquipCode"] = this.uGridPMPlanDWeek.Rows[i].Cells["EquipCode"].Value.ToString();
                                        drDel["Seq"] = this.uGridPMPlanDWeek.Rows[i].Cells["Seq"].Value.ToString();
                                        dtPMDel.Rows.Add(drDel);
                                    }
                                }
                            }
                            //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                                return;
                            }
                            break;
                        //----------------------월간---------------------//
                        case "MON":
                            //줄이 0이상 인지 판단
                            if (this.uGridPMPlanDMonth.Rows.Count > 0)
                            {
                                for (int i = 0; i < this.uGridPMPlanDMonth.Rows.Count; i++)
                                {
                                    //체크 된 줄의 정보만 저장
                                    if (Convert.ToBoolean(this.uGridPMPlanDMonth.Rows[i].Cells["Check"].Value) == true)
                                    {
                                        DataRow drDel;
                                        drDel = dtPMDel.NewRow();
                                        drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                        drDel["PlanYear"] = this.uGridPMPlanDMonth.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                        drDel["EquipCode"] = this.uGridPMPlanDMonth.Rows[i].Cells["EquipCode"].Value.ToString();
                                        drDel["Seq"] = this.uGridPMPlanDMonth.Rows[i].Cells["Seq"].Value.ToString();
                                        dtPMDel.Rows.Add(drDel);
                                    }
                                }
                            }
                            //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                                return;
                            }
                            break;
                        //---------------------분기,반기,년간-----------------------//
                        case "YEA":
                            //줄이 0이상 인지 판단
                            if (this.uGridPMPlanDYear.Rows.Count > 0)
                            {
                                for (int i = 0; i < this.uGridPMPlanDYear.Rows.Count; i++)
                                {
                                    //체크 된 줄의 정보만 저장
                                    if (Convert.ToBoolean(this.uGridPMPlanDYear.Rows[i].Cells["Check"].Value) == true)
                                    {
                                        DataRow drDel;
                                        drDel = dtPMDel.NewRow();
                                        drDel["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                        drDel["PlanYear"] = this.uGridPMPlanDYear.Rows[i].Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                                        drDel["EquipCode"] = this.uGridPMPlanDYear.Rows[i].Cells["EquipCode"].Value.ToString();
                                        drDel["Seq"] = this.uGridPMPlanDYear.Rows[i].Cells["Seq"].Value.ToString();
                                        dtPMDel.Rows.Add(drDel);
                                    }
                                }
                            }
                            //줄이 0 일시 메세지 박스를 보여주고 빠져나간다.
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                                return;
                            }
                            break;
                    }
                    //------------------------------저장 끝------------------------------------//

                    if (dtPMDel.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000882", "M000649", Infragistics.Win.HAlign.Right);

                        return;
                    }


                    //처리로직//
                    string strRtn = clsPMPlanD.mfDeletePMPlanD(dtPMDel);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    //처리로직끝//

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과값처리//
                    if (ErrRtn.ErrNum == 0)
                    {
                        InitDisplay();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        SearchInfo();

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                    }
                    //else
                    //{
                    //    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                    //                Infragistics.Win.HAlign.Right);
                    //}
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자동조회,리로드
        /// </summary>
        private void SearchInfo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strUserID = this.uTextTechnicianID.Text;
                string strYear = this.uNumSearchYear.Value.ToString();

                #region 예방점검계획조회
                //처리 로직//
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                QRPEQU.BL.EQUMGM.PMPlan clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                brwChannel.mfCredentials(clsPMPlan);

                DataTable dtPlan = null;
                if (strGroupCode != "")
                    dtPlan = clsPMPlan.mfReadPMPlanEquipGroup
                        (strPlantCode
                        , strYear
                        , strStationCode
                        , strEquipLocCode
                        , strProcessGroup
                        , strEquipTypeCode
                        , strGroupCode);
                else
                {
                    if (strUserID != "")
                        dtPlan = clsPMPlan.mfReadPMPlanEquipWorker(strPlantCode, strYear, strUserID);
                }
                /////////////

                //---데이터 바인드? ---//
                if (dtPlan.Rows.Count != 0)
                {
                    this.uDateStartDay.Value = dtPlan.Rows[0]["PMApplyDate"].ToString();
                    this.uDateWrite.Value = dtPlan.Rows[0]["WriteDate"].ToString();
                    this.uTextUserID.Text = dtPlan.Rows[0]["WriteID"].ToString();
                    this.uTextUserName.Text = dtPlan.Rows[0]["UserName"].ToString();

                    this.uTabControlDay.Tabs[0].Selected = true;
                    //----------------------------예방점검상세정보-------------------------------//
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                    QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                    brwChannel.mfCredentials(clsPMPlanD);

                    DataTable dtPlanD = null;
                    if (strGroupCode != "")
                        dtPlanD = clsPMPlanD.mfReadPMPlanDEquipGroup
                            (strPlantCode
                            ,strYear 
                            , strStationCode
                            , strEquipLocCode
                            , strProcessGroup
                            , strEquipTypeCode
                            , strGroupCode
                            , m_resSys.GetString("SYS_LANG"));
                    else
                    {
                        if (strUserID != "")
                            dtPlanD = clsPMPlanD.mfReadPMPlanDEquipWorker(strPlantCode, strGroupCode, strYear, m_resSys.GetString("SYS_LANG"));
                    }

                

                    if (dtPlanD.Rows.Count != 0)
                    {

                        DataTable dtCopy = new DataTable();
                        DataRow[] drCopy;

                        #region 각탭의 조건에 맞게 바인드
                        //조건에 맞은 정보가 한 줄이상 있을 시 정보를 복사하여 저장 시킨 후 바인드

                        //일간
                        if (dtPlanD.Select("PMPeriodCode = 'DAY'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'DAY'"); //검색된 값을 그리드 소스에 넣을 시 줄만 들어가서 컬럼인식을 못하여 화면에 안나옴
                            dtCopy = drCopy.CopyToDataTable();  //Row가 0 일 시 에러가 남

                            this.uGridPMPlanDDay.DataSource = dtCopy;
                            this.uGridPMPlanDDay.DataBind();
                            for (int i = 0; i < this.uGridPMPlanDDay.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDDay.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDDay.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                            this.uButtonDeleteRow1.Show();
                        }
                        //주간
                        if (dtPlanD.Select("PMPeriodCode = 'WEK'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'WEK'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMPlanDWeek.DataSource = dtCopy;
                            this.uGridPMPlanDWeek.DataBind();

                            for (int i = 0; i < this.uGridPMPlanDWeek.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDWeek.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDWeek.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                            this.uButtonDeleteRow2.Show();
                        }
                        //월간
                        if (dtPlanD.Select("PMPeriodCode = 'MON'").Count() != 0)
                        {
                            drCopy = dtPlanD.Select("PMPeriodCode = 'MON'");
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMPlanDMonth.DataSource = dtCopy;
                            this.uGridPMPlanDMonth.DataBind();


                            for (int i = 0; i < this.uGridPMPlanDMonth.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDMonth.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDMonth.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                            this.uButtonDeleteRow3.Show();
                        }
                        //분기,반기,년간
                        string strExpr = "";
                        strExpr = "PMPeriodCode = 'QUA'";
                        strExpr = strExpr + " or PMPeriodCode = 'HAF'";
                        strExpr = strExpr + " or PMPeriodCode = 'YEA'";

                        if (dtPlanD.Select(strExpr).Count() != 0)
                        {
                            drCopy = dtPlanD.Select(strExpr);
                            dtCopy = drCopy.CopyToDataTable();

                            this.uGridPMPlanDYear.DataSource = dtCopy;
                            this.uGridPMPlanDYear.DataBind();

                            for (int i = 0; i < this.uGridPMPlanDYear.Rows.Count; i++)
                            {
                                if (this.uGridPMPlanDYear.Rows[i].Cells["PMResultCode"].Value.ToString() != "")
                                {
                                    this.uGridPMPlanDYear.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }

                            this.uButtonDeleteRow4.Show();
                        }

                        #endregion
                    }
                }

                #endregion

                else
                {
                    //InitDisplay();
                }
                //---설비리스트---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquipList = clsEquipGroup.mfReadEquipGroupList(strPlantCode, strGroupCode, m_resSys.GetString("SYS_LANG"));

                this.uGridEquipList.DataSource = dtEquipList;
                this.uGridEquipList.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }




        
    }
}
        

