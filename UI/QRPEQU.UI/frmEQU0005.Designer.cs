﻿namespace QRPEQU.UI
{
    partial class frmEQU0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0005));
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchPlanYear = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextTechnicianName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTechnician = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipMentGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uGridPMResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uLabelPMWoker = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPMWorkName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPMWorkerID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonFileDwon = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchPlanYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMWorkName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMWorkerID)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchPlanYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnicianName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnician);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipMentGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 32);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboProcessGroup.TabIndex = 26;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboSearchEquipLargeType
            // 
            this.uComboSearchEquipLargeType.Location = new System.Drawing.Point(360, 32);
            this.uComboSearchEquipLargeType.MaxLength = 50;
            this.uComboSearchEquipLargeType.Name = "uComboSearchEquipLargeType";
            this.uComboSearchEquipLargeType.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipLargeType.TabIndex = 24;
            this.uComboSearchEquipLargeType.ValueChanged += new System.EventHandler(this.uComboSearchEquType_ValueChanged);
            // 
            // uLabelSearchProcessGroup
            // 
            this.uLabelSearchProcessGroup.Location = new System.Drawing.Point(12, 32);
            this.uLabelSearchProcessGroup.Name = "uLabelSearchProcessGroup";
            this.uLabelSearchProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcessGroup.TabIndex = 25;
            this.uLabelSearchProcessGroup.Text = "ultraLabel2";
            // 
            // uLabelEquType
            // 
            this.uLabelEquType.Location = new System.Drawing.Point(256, 32);
            this.uLabelEquType.Name = "uLabelEquType";
            this.uLabelEquType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquType.TabIndex = 25;
            this.uLabelEquType.Text = "ultraLabel2";
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(604, 8);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipLoc.TabIndex = 22;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uLabelLoc
            // 
            this.uLabelLoc.Location = new System.Drawing.Point(500, 7);
            this.uLabelLoc.Name = "uLabelLoc";
            this.uLabelLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelLoc.TabIndex = 23;
            this.uLabelLoc.Text = "ultraLabel1";
            // 
            // uDateSearchPlanYear
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchPlanYear.Appearance = appearance17;
            this.uDateSearchPlanYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchPlanYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchPlanYear.Location = new System.Drawing.Point(116, 8);
            this.uDateSearchPlanYear.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.uDateSearchPlanYear.Name = "uDateSearchPlanYear";
            this.uDateSearchPlanYear.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchPlanYear.TabIndex = 1;
            this.uDateSearchPlanYear.ValueChanged += new System.EventHandler(this.uDateSearchPlanYear_ValueChanged);
            // 
            // uTextTechnicianName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Appearance = appearance18;
            this.uTextTechnicianName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Location = new System.Drawing.Point(220, 56);
            this.uTextTechnicianName.Name = "uTextTechnicianName";
            this.uTextTechnicianName.ReadOnly = true;
            this.uTextTechnicianName.Size = new System.Drawing.Size(100, 21);
            this.uTextTechnicianName.TabIndex = 16;
            // 
            // uTextTechnician
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTechnician.Appearance = appearance19;
            this.uTextTechnician.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTechnician.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance20.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance20;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTechnician.ButtonsRight.Add(editorButton1);
            this.uTextTechnician.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTechnician.Location = new System.Drawing.Point(116, 56);
            this.uTextTechnician.MaxLength = 20;
            this.uTextTechnician.Name = "uTextTechnician";
            this.uTextTechnician.Size = new System.Drawing.Size(100, 19);
            this.uTextTechnician.TabIndex = 7;
            this.uTextTechnician.ValueChanged += new System.EventHandler(this.uTextTechnician_ValueChanged);
            this.uTextTechnician.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextTechnician_KeyDown);
            this.uTextTechnician.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextTechnician_EditorButtonClick);
            // 
            // uLabelSearchUser
            // 
            this.uLabelSearchUser.Location = new System.Drawing.Point(12, 56);
            this.uLabelSearchUser.Name = "uLabelSearchUser";
            this.uLabelSearchUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchUser.TabIndex = 14;
            this.uLabelSearchUser.Text = "ultraLabel3";
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(604, 32);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipGroup.TabIndex = 3;
            // 
            // uLabelSearchEquipMentGroup
            // 
            this.uLabelSearchEquipMentGroup.Location = new System.Drawing.Point(500, 32);
            this.uLabelSearchEquipMentGroup.Name = "uLabelSearchEquipMentGroup";
            this.uLabelSearchEquipMentGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipMentGroup.TabIndex = 12;
            this.uLabelSearchEquipMentGroup.Text = "ultraLabel2";
            // 
            // uComboSearchProcGubun
            // 
            this.uComboSearchProcGubun.Location = new System.Drawing.Point(1056, 13);
            this.uComboSearchProcGubun.MaxLength = 50;
            this.uComboSearchProcGubun.Name = "uComboSearchProcGubun";
            this.uComboSearchProcGubun.Size = new System.Drawing.Size(12, 21);
            this.uComboSearchProcGubun.TabIndex = 6;
            this.uComboSearchProcGubun.Text = "ultraComboEditor2";
            this.uComboSearchProcGubun.Visible = false;
            this.uComboSearchProcGubun.ValueChanged += new System.EventHandler(this.uComboSearchProcGubun_ValueChanged);
            // 
            // uLabelSearchProcGubun
            // 
            this.uLabelSearchProcGubun.Location = new System.Drawing.Point(1032, 12);
            this.uLabelSearchProcGubun.Name = "uLabelSearchProcGubun";
            this.uLabelSearchProcGubun.Size = new System.Drawing.Size(12, 20);
            this.uLabelSearchProcGubun.TabIndex = 10;
            this.uLabelSearchProcGubun.Text = "ultraLabel2";
            this.uLabelSearchProcGubun.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(360, 8);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchStation.TabIndex = 5;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(256, 8);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 8;
            this.uLabelSearchStation.Text = "ultraLabel1";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1048, 9);
            this.uComboSearchArea.MaxLength = 50;
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(12, 21);
            this.uComboSearchArea.TabIndex = 4;
            this.uComboSearchArea.Text = "ultraComboEditor2";
            this.uComboSearchArea.Visible = false;
            this.uComboSearchArea.ValueChanged += new System.EventHandler(this.uComboSearchArea_ValueChanged);
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(1032, 9);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(12, 20);
            this.uLabelSearchArea.TabIndex = 6;
            this.uLabelSearchArea.Text = "ultraLabel2";
            this.uLabelSearchArea.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(996, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(36, 21);
            this.uComboSearchPlant.TabIndex = 2;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(984, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(8, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 2;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uGridPMResult
            // 
            this.uGridPMResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMResult.DisplayLayout.Appearance = appearance8;
            this.uGridPMResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridPMResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResult.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridPMResult.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMResult.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMResult.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridPMResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMResult.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridPMResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMResult.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance15.TextHAlignAsString = "Left";
            this.uGridPMResult.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridPMResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMResult.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridPMResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPMResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMResult.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMResult.Location = new System.Drawing.Point(0, 152);
            this.uGridPMResult.Name = "uGridPMResult";
            this.uGridPMResult.Size = new System.Drawing.Size(1070, 686);
            this.uGridPMResult.TabIndex = 6;
            this.uGridPMResult.Text = "ultraGrid1";
            this.uGridPMResult.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPMResult_AfterCellUpdate);
            this.uGridPMResult.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPMResult_CellListSelect);
            this.uGridPMResult.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridPMResult_KeyDown);
            this.uGridPMResult.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPMResult_ClickCellButton);
            this.uGridPMResult.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPMResult_CellChange);
            this.uGridPMResult.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridPMResult_DoubleClickCell);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uLabelPMWoker
            // 
            this.uLabelPMWoker.Location = new System.Drawing.Point(12, 128);
            this.uLabelPMWoker.Name = "uLabelPMWoker";
            this.uLabelPMWoker.Size = new System.Drawing.Size(100, 20);
            this.uLabelPMWoker.TabIndex = 25;
            this.uLabelPMWoker.Text = "ultraLabel2";
            // 
            // uTextPMWorkName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMWorkName.Appearance = appearance3;
            this.uTextPMWorkName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMWorkName.Location = new System.Drawing.Point(220, 128);
            this.uTextPMWorkName.Name = "uTextPMWorkName";
            this.uTextPMWorkName.ReadOnly = true;
            this.uTextPMWorkName.Size = new System.Drawing.Size(100, 21);
            this.uTextPMWorkName.TabIndex = 16;
            // 
            // uTextPMWorkerID
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPMWorkerID.Appearance = appearance21;
            this.uTextPMWorkerID.BackColor = System.Drawing.Color.PowderBlue;
            appearance22.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance22;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextPMWorkerID.ButtonsRight.Add(editorButton2);
            this.uTextPMWorkerID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPMWorkerID.Location = new System.Drawing.Point(116, 128);
            this.uTextPMWorkerID.MaxLength = 20;
            this.uTextPMWorkerID.Name = "uTextPMWorkerID";
            this.uTextPMWorkerID.Size = new System.Drawing.Size(100, 21);
            this.uTextPMWorkerID.TabIndex = 26;
            this.uTextPMWorkerID.ValueChanged += new System.EventHandler(this.uTextPMWorkerID_ValueChanged);
            this.uTextPMWorkerID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextPMWorkerID_KeyDown);
            this.uTextPMWorkerID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextPMWorkerID_EditorButtonClick);
            // 
            // uButtonFileDwon
            // 
            this.uButtonFileDwon.Location = new System.Drawing.Point(972, 124);
            this.uButtonFileDwon.Name = "uButtonFileDwon";
            this.uButtonFileDwon.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDwon.TabIndex = 27;
            this.uButtonFileDwon.Text = "다운";
            this.uButtonFileDwon.Visible = false;
            this.uButtonFileDwon.Click += new System.EventHandler(this.uButtonFileDwon_Click);
            // 
            // frmEQU0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uButtonFileDwon);
            this.Controls.Add(this.uTextPMWorkerID);
            this.Controls.Add(this.uGridPMResult);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uLabelPMWoker);
            this.Controls.Add(this.uTextPMWorkName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0005";
            this.Load += new System.EventHandler(this.frmEQU0005_Load);
            this.Activated += new System.EventHandler(this.frmEQU0005_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0005_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchPlanYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMWorkName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMWorkerID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchPlanYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnicianName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnician;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipMentGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMResult;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelPMWoker;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMWorkName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMWorkerID;
        private Infragistics.Win.Misc.UltraButton uButtonFileDwon;

    }
}