﻿namespace QRPEQU.UI
{
    partial class frmEQU0005_P1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0005_P1));
            this.uButton1 = new Infragistics.Win.Misc.UltraButton();
            this.uButton2 = new Infragistics.Win.Misc.UltraButton();
            this.uButton3 = new Infragistics.Win.Misc.UltraButton();
            this.uButton4 = new Infragistics.Win.Misc.UltraButton();
            this.uButton5 = new Infragistics.Win.Misc.UltraButton();
            this.uButton6 = new Infragistics.Win.Misc.UltraButton();
            this.uButton7 = new Infragistics.Win.Misc.UltraButton();
            this.uButton8 = new Infragistics.Win.Misc.UltraButton();
            this.uButton9 = new Infragistics.Win.Misc.UltraButton();
            this.uTextReturnValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButton00 = new Infragistics.Win.Misc.UltraButton();
            this.uButton0 = new Infragistics.Win.Misc.UltraButton();
            this.uButtonBack = new Infragistics.Win.Misc.UltraButton();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonCancel = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClear = new Infragistics.Win.Misc.UltraButton();
            this.uButton000 = new Infragistics.Win.Misc.UltraButton();
            this.uButtonPoint = new Infragistics.Win.Misc.UltraButton();
            this.uButtonPlus = new Infragistics.Win.Misc.UltraButton();
            this.uButtonM = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnValue)).BeginInit();
            this.SuspendLayout();
            // 
            // uButton1
            // 
            this.uButton1.Location = new System.Drawing.Point(12, 164);
            this.uButton1.Name = "uButton1";
            this.uButton1.Size = new System.Drawing.Size(84, 36);
            this.uButton1.TabIndex = 0;
            this.uButton1.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton2
            // 
            this.uButton2.Location = new System.Drawing.Point(100, 164);
            this.uButton2.Name = "uButton2";
            this.uButton2.Size = new System.Drawing.Size(84, 36);
            this.uButton2.TabIndex = 0;
            this.uButton2.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton3
            // 
            this.uButton3.Location = new System.Drawing.Point(188, 164);
            this.uButton3.Name = "uButton3";
            this.uButton3.Size = new System.Drawing.Size(84, 36);
            this.uButton3.TabIndex = 0;
            this.uButton3.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton4
            // 
            this.uButton4.Location = new System.Drawing.Point(12, 124);
            this.uButton4.Name = "uButton4";
            this.uButton4.Size = new System.Drawing.Size(84, 36);
            this.uButton4.TabIndex = 0;
            this.uButton4.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton5
            // 
            this.uButton5.Location = new System.Drawing.Point(100, 124);
            this.uButton5.Name = "uButton5";
            this.uButton5.Size = new System.Drawing.Size(84, 36);
            this.uButton5.TabIndex = 0;
            this.uButton5.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton6
            // 
            this.uButton6.Location = new System.Drawing.Point(188, 124);
            this.uButton6.Name = "uButton6";
            this.uButton6.Size = new System.Drawing.Size(84, 36);
            this.uButton6.TabIndex = 0;
            this.uButton6.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton7
            // 
            this.uButton7.Location = new System.Drawing.Point(12, 84);
            this.uButton7.Name = "uButton7";
            this.uButton7.Size = new System.Drawing.Size(84, 36);
            this.uButton7.TabIndex = 0;
            this.uButton7.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton8
            // 
            this.uButton8.Location = new System.Drawing.Point(100, 84);
            this.uButton8.Name = "uButton8";
            this.uButton8.Size = new System.Drawing.Size(84, 36);
            this.uButton8.TabIndex = 0;
            this.uButton8.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton9
            // 
            this.uButton9.Location = new System.Drawing.Point(188, 84);
            this.uButton9.Name = "uButton9";
            this.uButton9.Size = new System.Drawing.Size(84, 36);
            this.uButton9.TabIndex = 0;
            this.uButton9.Click += new System.EventHandler(this.bt_num);
            // 
            // uTextReturnValue
            // 
            this.uTextReturnValue.AlwaysInEditMode = true;
            appearance1.TextHAlignAsString = "Right";
            this.uTextReturnValue.Appearance = appearance1;
            this.uTextReturnValue.Location = new System.Drawing.Point(12, 12);
            this.uTextReturnValue.MaxLength = 100;
            this.uTextReturnValue.Name = "uTextReturnValue";
            this.uTextReturnValue.Size = new System.Drawing.Size(260, 21);
            this.uTextReturnValue.TabIndex = 1;
            this.uTextReturnValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uTextReturnValue_KeyPress);
            // 
            // uButton00
            // 
            this.uButton00.Location = new System.Drawing.Point(12, 204);
            this.uButton00.Name = "uButton00";
            this.uButton00.Size = new System.Drawing.Size(84, 36);
            this.uButton00.TabIndex = 0;
            this.uButton00.Click += new System.EventHandler(this.bt_num);
            // 
            // uButton0
            // 
            this.uButton0.Location = new System.Drawing.Point(100, 204);
            this.uButton0.Name = "uButton0";
            this.uButton0.Size = new System.Drawing.Size(84, 36);
            this.uButton0.TabIndex = 0;
            this.uButton0.Click += new System.EventHandler(this.bt_num);
            // 
            // uButtonBack
            // 
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 20F;
            this.uButtonBack.Appearance = appearance2;
            this.uButtonBack.Location = new System.Drawing.Point(276, 8);
            this.uButtonBack.Name = "uButtonBack";
            this.uButtonBack.Size = new System.Drawing.Size(64, 28);
            this.uButtonBack.TabIndex = 0;
            this.uButtonBack.Text = "←";
            this.uButtonBack.Click += new System.EventHandler(this.uButtonBack_Click);
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(276, 124);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(64, 116);
            this.uButtonOK.TabIndex = 0;
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonCancel
            // 
            this.uButtonCancel.Location = new System.Drawing.Point(276, 84);
            this.uButtonCancel.Name = "uButtonCancel";
            this.uButtonCancel.Size = new System.Drawing.Size(64, 36);
            this.uButtonCancel.TabIndex = 0;
            this.uButtonCancel.Click += new System.EventHandler(this.uButtonCancel_Click);
            // 
            // uButtonClear
            // 
            this.uButtonClear.Location = new System.Drawing.Point(276, 44);
            this.uButtonClear.Name = "uButtonClear";
            this.uButtonClear.Size = new System.Drawing.Size(64, 36);
            this.uButtonClear.TabIndex = 0;
            this.uButtonClear.Click += new System.EventHandler(this.uButtonClear_Click);
            // 
            // uButton000
            // 
            this.uButton000.Location = new System.Drawing.Point(188, 204);
            this.uButton000.Name = "uButton000";
            this.uButton000.Size = new System.Drawing.Size(84, 36);
            this.uButton000.TabIndex = 0;
            this.uButton000.Click += new System.EventHandler(this.bt_num);
            // 
            // uButtonPoint
            // 
            this.uButtonPoint.Location = new System.Drawing.Point(12, 44);
            this.uButtonPoint.Name = "uButtonPoint";
            this.uButtonPoint.Size = new System.Drawing.Size(84, 36);
            this.uButtonPoint.TabIndex = 2;
            this.uButtonPoint.Click += new System.EventHandler(this.bt_num);
            // 
            // uButtonPlus
            // 
            this.uButtonPlus.Location = new System.Drawing.Point(100, 44);
            this.uButtonPlus.Name = "uButtonPlus";
            this.uButtonPlus.Size = new System.Drawing.Size(84, 36);
            this.uButtonPlus.TabIndex = 0;
            this.uButtonPlus.Click += new System.EventHandler(this.bt_num);
            // 
            // uButtonM
            // 
            this.uButtonM.Location = new System.Drawing.Point(188, 44);
            this.uButtonM.Name = "uButtonM";
            this.uButtonM.Size = new System.Drawing.Size(84, 36);
            this.uButtonM.TabIndex = 0;
            this.uButtonM.Click += new System.EventHandler(this.bt_num);
            // 
            // frmEQU0005_P1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(350, 249);
            this.ControlBox = false;
            this.Controls.Add(this.uButtonPoint);
            this.Controls.Add(this.uTextReturnValue);
            this.Controls.Add(this.uButtonClear);
            this.Controls.Add(this.uButtonCancel);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uButtonBack);
            this.Controls.Add(this.uButton000);
            this.Controls.Add(this.uButton0);
            this.Controls.Add(this.uButton00);
            this.Controls.Add(this.uButtonM);
            this.Controls.Add(this.uButtonPlus);
            this.Controls.Add(this.uButton9);
            this.Controls.Add(this.uButton8);
            this.Controls.Add(this.uButton7);
            this.Controls.Add(this.uButton6);
            this.Controls.Add(this.uButton5);
            this.Controls.Add(this.uButton4);
            this.Controls.Add(this.uButton3);
            this.Controls.Add(this.uButton2);
            this.Controls.Add(this.uButton1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0005_P1";
            this.Text = "수치입력창";
            this.Load += new System.EventHandler(this.frmEQU0005_P1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton uButton1;
        private Infragistics.Win.Misc.UltraButton uButton2;
        private Infragistics.Win.Misc.UltraButton uButton3;
        private Infragistics.Win.Misc.UltraButton uButton4;
        private Infragistics.Win.Misc.UltraButton uButton5;
        private Infragistics.Win.Misc.UltraButton uButton6;
        private Infragistics.Win.Misc.UltraButton uButton7;
        private Infragistics.Win.Misc.UltraButton uButton8;
        private Infragistics.Win.Misc.UltraButton uButton9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnValue;
        private Infragistics.Win.Misc.UltraButton uButton00;
        private Infragistics.Win.Misc.UltraButton uButton0;
        private Infragistics.Win.Misc.UltraButton uButtonBack;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonCancel;
        private Infragistics.Win.Misc.UltraButton uButtonClear;
        private Infragistics.Win.Misc.UltraButton uButton000;
        private Infragistics.Win.Misc.UltraButton uButtonPoint;
        private Infragistics.Win.Misc.UltraButton uButtonPlus;
        private Infragistics.Win.Misc.UltraButton uButtonM;
    }
}