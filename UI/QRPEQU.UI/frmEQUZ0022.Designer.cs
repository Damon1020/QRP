﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0022));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchDown = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchEquipProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquipmentProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipState = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDown = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectEquipment = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipRepair = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxRequestRepair = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEquipState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairReqReason = new System.Windows.Forms.RichTextBox();
            this.uTextDownName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonRepairCancel = new Infragistics.Win.Misc.UltraButton();
            this.uButtonRepair = new Infragistics.Win.Misc.UltraButton();
            this.uTextDownCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDown = new Infragistics.Win.Misc.UltraLabel();
            this.uDateBreakDownTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateBreakDownDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckAutoAccept = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckUrgentFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateRepairReqDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPKType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextContainer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRepairRequestDay = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTroubleDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRequestReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPKType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCarrier = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipmentName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelGoods = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairRequestUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipmentCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxAcceptRepair = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextReceiptDesc = new System.Windows.Forms.RichTextBox();
            this.uButtonCancelAccept = new Infragistics.Win.Misc.UltraButton();
            this.uButtonAccept = new Infragistics.Win.Misc.UltraButton();
            this.uCheckHeadFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelNote = new Infragistics.Win.Misc.UltraLabel();
            this.uComboRepairResult = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStage = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonCancelResult = new Infragistics.Win.Misc.UltraButton();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonUseSP = new Infragistics.Win.Misc.UltraButton();
            this.uLabelAcceptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairResult = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonChangeMold = new Infragistics.Win.Misc.UltraButton();
            this.uDateReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextReceiptID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReceiptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckCCSReqFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextStageDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairTime = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxRepairResult = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckImportant = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uButtonChange = new Infragistics.Win.Misc.UltraButton();
            this.uButtonCCS = new Infragistics.Win.Misc.UltraButton();
            this.uLabelNotice = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonChg = new Infragistics.Win.Misc.UltraButton();
            this.uTextRepairDesc = new System.Windows.Forms.RichTextBox();
            this.uTextResultEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairEndTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uButtonSaveResult = new Infragistics.Win.Misc.UltraButton();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSCV = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextRepairUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFaultTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipRepair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRequestRepair)).BeginInit();
            this.uGroupBoxRequestRepair.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDownName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateBreakDownTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateBreakDownDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAutoAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUrgentFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPKType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAcceptRepair)).BeginInit();
            this.uGroupBoxAcceptRepair.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckHeadFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCCSReqFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStageDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRepairResult)).BeginInit();
            this.uGroupBoxRepairResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckImportant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDown);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipState);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipmentProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipState);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDown);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelInspectEquipment);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 36);
            this.uComboProcessGroup.MaxLength = 40;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(140, 21);
            this.uComboProcessGroup.TabIndex = 2;
            // 
            // uComboSearchDown
            // 
            this.uComboSearchDown.Location = new System.Drawing.Point(392, 36);
            this.uComboSearchDown.MaxLength = 10;
            this.uComboSearchDown.Name = "uComboSearchDown";
            this.uComboSearchDown.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchDown.TabIndex = 5;
            // 
            // uTextSearchEquipName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance2;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Location = new System.Drawing.Point(772, 36);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipName.TabIndex = 8;
            // 
            // uTextSearchEquipCode
            // 
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(668, 36);
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipCode.TabIndex = 6;
            this.uTextSearchEquipCode.ValueChanged += new System.EventHandler(this.uTextSearchEquipCode_ValueChanged);
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uComboSearchEquipProcess
            // 
            this.uComboSearchEquipProcess.Location = new System.Drawing.Point(1028, 32);
            this.uComboSearchEquipProcess.MaxLength = 50;
            this.uComboSearchEquipProcess.Name = "uComboSearchEquipProcess";
            this.uComboSearchEquipProcess.Size = new System.Drawing.Size(32, 21);
            this.uComboSearchEquipProcess.TabIndex = 5;
            this.uComboSearchEquipProcess.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(972, 8);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(32, 21);
            this.uComboSearchStation.TabIndex = 4;
            this.uComboSearchStation.Visible = false;
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1028, 8);
            this.uComboSearchArea.MaxLength = 50;
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(28, 21);
            this.uComboSearchArea.TabIndex = 3;
            this.uComboSearchArea.Visible = false;
            // 
            // uComboEquipGroup
            // 
            this.uComboEquipGroup.Location = new System.Drawing.Point(972, 32);
            this.uComboEquipGroup.MaxLength = 50;
            this.uComboEquipGroup.Name = "uComboEquipGroup";
            this.uComboEquipGroup.Size = new System.Drawing.Size(32, 21);
            this.uComboEquipGroup.TabIndex = 2;
            this.uComboEquipGroup.Visible = false;
            // 
            // uComboSearchEquipState
            // 
            this.uComboSearchEquipState.Location = new System.Drawing.Point(668, 12);
            this.uComboSearchEquipState.MaxLength = 50;
            this.uComboSearchEquipState.Name = "uComboSearchEquipState";
            this.uComboSearchEquipState.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchEquipState.TabIndex = 4;
            // 
            // uComboSearchEquipType
            // 
            this.uComboSearchEquipType.Location = new System.Drawing.Point(392, 12);
            this.uComboSearchEquipType.MaxLength = 50;
            this.uComboSearchEquipType.Name = "uComboSearchEquipType";
            this.uComboSearchEquipType.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchEquipType.TabIndex = 3;
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchEquipLoc.TabIndex = 1;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchPlant
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Appearance = appearance4;
            this.uComboSearchPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboSearchPlant.Location = new System.Drawing.Point(916, 36);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(24, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelEquipmentProcess
            // 
            this.uLabelEquipmentProcess.Location = new System.Drawing.Point(1008, 32);
            this.uLabelEquipmentProcess.Name = "uLabelEquipmentProcess";
            this.uLabelEquipmentProcess.Size = new System.Drawing.Size(16, 20);
            this.uLabelEquipmentProcess.TabIndex = 0;
            this.uLabelEquipmentProcess.Text = "ultraLabel1";
            this.uLabelEquipmentProcess.Visible = false;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(952, 8);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(16, 20);
            this.uLabelStation.TabIndex = 0;
            this.uLabelStation.Text = "ultraLabel1";
            this.uLabelStation.Visible = false;
            // 
            // uLabelArea
            // 
            this.uLabelArea.Location = new System.Drawing.Point(1012, 8);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(8, 20);
            this.uLabelArea.TabIndex = 0;
            this.uLabelArea.Text = "ultraLabel1";
            this.uLabelArea.Visible = false;
            // 
            // uLabelSearchEquipState
            // 
            this.uLabelSearchEquipState.Location = new System.Drawing.Point(564, 12);
            this.uLabelSearchEquipState.Name = "uLabelSearchEquipState";
            this.uLabelSearchEquipState.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipState.TabIndex = 0;
            this.uLabelSearchEquipState.Text = "ultraLabel1";
            // 
            // uLabelSearchEquipType
            // 
            this.uLabelSearchEquipType.Location = new System.Drawing.Point(288, 12);
            this.uLabelSearchEquipType.Name = "uLabelSearchEquipType";
            this.uLabelSearchEquipType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipType.TabIndex = 0;
            this.uLabelSearchEquipType.Text = "ultraLabel1";
            // 
            // uLabelSearchDown
            // 
            this.uLabelSearchDown.Location = new System.Drawing.Point(288, 36);
            this.uLabelSearchDown.Name = "uLabelSearchDown";
            this.uLabelSearchDown.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDown.TabIndex = 0;
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(564, 36);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquip.TabIndex = 0;
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 0;
            this.uLabelProcessGroup.Text = "ultraLabel1";
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 0;
            this.uLabelSearchEquipLoc.Text = "ultraLabel1";
            // 
            // uLabelInspectEquipment
            // 
            this.uLabelInspectEquipment.Location = new System.Drawing.Point(948, 32);
            this.uLabelInspectEquipment.Name = "uLabelInspectEquipment";
            this.uLabelInspectEquipment.Size = new System.Drawing.Size(20, 20);
            this.uLabelInspectEquipment.TabIndex = 0;
            this.uLabelInspectEquipment.Text = "ultraLabel1";
            this.uLabelInspectEquipment.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(888, 36);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(24, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridEquipRepair
            // 
            this.uGridEquipRepair.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipRepair.DisplayLayout.Appearance = appearance5;
            this.uGridEquipRepair.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipRepair.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipRepair.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipRepair.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridEquipRepair.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipRepair.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridEquipRepair.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipRepair.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipRepair.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipRepair.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridEquipRepair.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipRepair.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipRepair.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipRepair.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridEquipRepair.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipRepair.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipRepair.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridEquipRepair.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridEquipRepair.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipRepair.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipRepair.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridEquipRepair.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipRepair.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridEquipRepair.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipRepair.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipRepair.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipRepair.Location = new System.Drawing.Point(0, 100);
            this.uGridEquipRepair.Name = "uGridEquipRepair";
            this.uGridEquipRepair.Size = new System.Drawing.Size(1070, 396);
            this.uGridEquipRepair.TabIndex = 2;
            this.uGridEquipRepair.Text = "ultraGrid1";
            this.uGridEquipRepair.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEquipRepair_DoubleClickCell);
            // 
            // uGroupBoxRequestRepair
            // 
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextEquipState);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextRepairReqReason);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextDownName);
            this.uGroupBoxRequestRepair.Controls.Add(this.uButtonRepairCancel);
            this.uGroupBoxRequestRepair.Controls.Add(this.uButtonRepair);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextDownCode);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelDown);
            this.uGroupBoxRequestRepair.Controls.Add(this.uDateBreakDownTime);
            this.uGroupBoxRequestRepair.Controls.Add(this.uDateBreakDownDate);
            this.uGroupBoxRequestRepair.Controls.Add(this.uCheckAutoAccept);
            this.uGroupBoxRequestRepair.Controls.Add(this.uCheckUrgentFlag);
            this.uGroupBoxRequestRepair.Controls.Add(this.uDateRepairReqDate);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextProductName);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextEquipName);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextRepairReqName);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextPackage);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextPKType);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextContainer);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextProductCode);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextRepairReqID);
            this.uGroupBoxRequestRepair.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxRequestRepair.Controls.Add(this.uComboPlant);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelRepairRequestDay);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelTroubleDate);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelRequestReason);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelPackage);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelPKType);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelCarrier);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelEquipmentName);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelGoods);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelRepairRequestUser);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelEquipmentCode);
            this.uGroupBoxRequestRepair.Controls.Add(this.uLabelPlant);
            this.uGroupBoxRequestRepair.Location = new System.Drawing.Point(0, 496);
            this.uGroupBoxRequestRepair.Name = "uGroupBoxRequestRepair";
            this.uGroupBoxRequestRepair.Size = new System.Drawing.Size(348, 345);
            this.uGroupBoxRequestRepair.TabIndex = 5;
            // 
            // uTextEquipState
            // 
            this.uTextEquipState.Location = new System.Drawing.Point(12, 300);
            this.uTextEquipState.Name = "uTextEquipState";
            this.uTextEquipState.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipState.TabIndex = 39;
            this.uTextEquipState.Visible = false;
            // 
            // uTextRepairReqReason
            // 
            this.uTextRepairReqReason.Location = new System.Drawing.Point(116, 244);
            this.uTextRepairReqReason.Name = "uTextRepairReqReason";
            this.uTextRepairReqReason.Size = new System.Drawing.Size(204, 52);
            this.uTextRepairReqReason.TabIndex = 16;
            this.uTextRepairReqReason.Text = "";
            // 
            // uTextDownName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDownName.Appearance = appearance17;
            this.uTextDownName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDownName.Location = new System.Drawing.Point(196, 100);
            this.uTextDownName.Name = "uTextDownName";
            this.uTextDownName.ReadOnly = true;
            this.uTextDownName.Size = new System.Drawing.Size(124, 21);
            this.uTextDownName.TabIndex = 9;
            // 
            // uButtonRepairCancel
            // 
            this.uButtonRepairCancel.Location = new System.Drawing.Point(140, 304);
            this.uButtonRepairCancel.Name = "uButtonRepairCancel";
            this.uButtonRepairCancel.Size = new System.Drawing.Size(88, 28);
            this.uButtonRepairCancel.TabIndex = 7;
            this.uButtonRepairCancel.Visible = false;
            this.uButtonRepairCancel.Click += new System.EventHandler(this.uButtonRepairCancel_Click);
            // 
            // uButtonRepair
            // 
            this.uButtonRepair.Location = new System.Drawing.Point(232, 304);
            this.uButtonRepair.Name = "uButtonRepair";
            this.uButtonRepair.Size = new System.Drawing.Size(88, 28);
            this.uButtonRepair.TabIndex = 17;
            this.uButtonRepair.Text = "ultraButton1";
            this.uButtonRepair.Click += new System.EventHandler(this.uButtonRepair_Click);
            // 
            // uTextDownCode
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDownCode.Appearance = appearance18;
            this.uTextDownCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance19.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance19;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDownCode.ButtonsRight.Add(editorButton2);
            this.uTextDownCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDownCode.Location = new System.Drawing.Point(116, 100);
            this.uTextDownCode.MaxLength = 20;
            this.uTextDownCode.Name = "uTextDownCode";
            this.uTextDownCode.ReadOnly = true;
            this.uTextDownCode.Size = new System.Drawing.Size(76, 21);
            this.uTextDownCode.TabIndex = 12;
            this.uTextDownCode.ValueChanged += new System.EventHandler(this.uTextDownCode_ValueChanged);
            this.uTextDownCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDownCode_EditorButtonClick);
            // 
            // uLabelDown
            // 
            this.uLabelDown.Location = new System.Drawing.Point(12, 100);
            this.uLabelDown.Name = "uLabelDown";
            this.uLabelDown.Size = new System.Drawing.Size(100, 20);
            this.uLabelDown.TabIndex = 7;
            this.uLabelDown.Text = "ultraLabel1";
            // 
            // uDateBreakDownTime
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateBreakDownTime.Appearance = appearance20;
            this.uDateBreakDownTime.BackColor = System.Drawing.Color.PowderBlue;
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateBreakDownTime.ButtonsRight.Add(spinEditorButton1);
            this.uDateBreakDownTime.DateTime = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateBreakDownTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateBreakDownTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateBreakDownTime.Location = new System.Drawing.Point(220, 124);
            this.uDateBreakDownTime.MaskInput = "hh:mm:ss";
            this.uDateBreakDownTime.Name = "uDateBreakDownTime";
            this.uDateBreakDownTime.Size = new System.Drawing.Size(76, 21);
            this.uDateBreakDownTime.TabIndex = 14;
            this.uDateBreakDownTime.Value = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateBreakDownTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.EditorSpinButtonClick);
            // 
            // uDateBreakDownDate
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateBreakDownDate.Appearance = appearance21;
            this.uDateBreakDownDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateBreakDownDate.DateTime = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateBreakDownDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateBreakDownDate.Location = new System.Drawing.Point(116, 124);
            this.uDateBreakDownDate.Name = "uDateBreakDownDate";
            this.uDateBreakDownDate.Size = new System.Drawing.Size(100, 21);
            this.uDateBreakDownDate.TabIndex = 13;
            this.uDateBreakDownDate.Value = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            // 
            // uCheckAutoAccept
            // 
            this.uCheckAutoAccept.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckAutoAccept.Location = new System.Drawing.Point(220, 4);
            this.uCheckAutoAccept.Name = "uCheckAutoAccept";
            this.uCheckAutoAccept.Size = new System.Drawing.Size(100, 20);
            this.uCheckAutoAccept.TabIndex = 8;
            this.uCheckAutoAccept.Text = "자동접수처리";
            this.uCheckAutoAccept.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckUrgentFlag
            // 
            this.uCheckUrgentFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckUrgentFlag.Location = new System.Drawing.Point(116, 4);
            this.uCheckUrgentFlag.Name = "uCheckUrgentFlag";
            this.uCheckUrgentFlag.Size = new System.Drawing.Size(100, 20);
            this.uCheckUrgentFlag.TabIndex = 8;
            this.uCheckUrgentFlag.Text = "긴급처리";
            this.uCheckUrgentFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateRepairReqDate
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairReqDate.Appearance = appearance22;
            this.uDateRepairReqDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairReqDate.DateTime = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateRepairReqDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairReqDate.Location = new System.Drawing.Point(48, 320);
            this.uDateRepairReqDate.Name = "uDateRepairReqDate";
            this.uDateRepairReqDate.Size = new System.Drawing.Size(32, 21);
            this.uDateRepairReqDate.TabIndex = 4;
            this.uDateRepairReqDate.Value = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            // 
            // uTextProductName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance23;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(220, 148);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(100, 21);
            this.uTextProductName.TabIndex = 3;
            // 
            // uTextEquipName
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance24;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(220, 76);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 3;
            this.uTextEquipName.Visible = false;
            // 
            // uTextRepairReqName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqName.Appearance = appearance25;
            this.uTextRepairReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqName.Location = new System.Drawing.Point(220, 52);
            this.uTextRepairReqName.Name = "uTextRepairReqName";
            this.uTextRepairReqName.ReadOnly = true;
            this.uTextRepairReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqName.TabIndex = 3;
            // 
            // uTextPackage
            // 
            appearance26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextPackage.Appearance = appearance26;
            this.uTextPackage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextPackage.Enabled = false;
            this.uTextPackage.Location = new System.Drawing.Point(116, 220);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.Size = new System.Drawing.Size(110, 21);
            this.uTextPackage.TabIndex = 3;
            // 
            // uTextPKType
            // 
            appearance27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextPKType.Appearance = appearance27;
            this.uTextPKType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextPKType.Enabled = false;
            this.uTextPKType.Location = new System.Drawing.Point(116, 196);
            this.uTextPKType.Name = "uTextPKType";
            this.uTextPKType.Size = new System.Drawing.Size(110, 21);
            this.uTextPKType.TabIndex = 3;
            // 
            // uTextContainer
            // 
            this.uTextContainer.Location = new System.Drawing.Point(116, 172);
            this.uTextContainer.Name = "uTextContainer";
            this.uTextContainer.Size = new System.Drawing.Size(110, 21);
            this.uTextContainer.TabIndex = 3;
            // 
            // uTextProductCode
            // 
            appearance28.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance28;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextProductCode.ButtonsRight.Add(editorButton3);
            this.uTextProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProductCode.Location = new System.Drawing.Point(116, 148);
            this.uTextProductCode.MaxLength = 20;
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(100, 21);
            this.uTextProductCode.TabIndex = 15;
            this.uTextProductCode.ValueChanged += new System.EventHandler(this.uTextProductCode_ValueChanged);
            this.uTextProductCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.uTextProductCode_KeyUp);
            this.uTextProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextProductCode_EditorButtonClick);
            // 
            // uTextRepairReqID
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairReqID.Appearance = appearance29;
            this.uTextRepairReqID.BackColor = System.Drawing.Color.PowderBlue;
            appearance30.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance30.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance30;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairReqID.ButtonsRight.Add(editorButton4);
            this.uTextRepairReqID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepairReqID.Location = new System.Drawing.Point(116, 52);
            this.uTextRepairReqID.MaxLength = 20;
            this.uTextRepairReqID.Name = "uTextRepairReqID";
            this.uTextRepairReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqID.TabIndex = 10;
            this.uTextRepairReqID.ValueChanged += new System.EventHandler(this.uTextRepairReqID_ValueChanged);
            this.uTextRepairReqID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRepairReqID_KeyDown);
            this.uTextRepairReqID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRepairReqID_EditorButtonClick);
            // 
            // uTextEquipCode
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance31;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(116, 76);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 11;
            this.uTextEquipCode.ValueChanged += new System.EventHandler(this.uTextEquipCode_ValueChanged);
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uComboPlant
            // 
            appearance32.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance32;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboPlant.TabIndex = 9;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelRepairRequestDay
            // 
            this.uLabelRepairRequestDay.Location = new System.Drawing.Point(16, 316);
            this.uLabelRepairRequestDay.Name = "uLabelRepairRequestDay";
            this.uLabelRepairRequestDay.Size = new System.Drawing.Size(28, 20);
            this.uLabelRepairRequestDay.TabIndex = 0;
            this.uLabelRepairRequestDay.Text = "ultraLabel1";
            // 
            // uLabelTroubleDate
            // 
            this.uLabelTroubleDate.Location = new System.Drawing.Point(12, 124);
            this.uLabelTroubleDate.Name = "uLabelTroubleDate";
            this.uLabelTroubleDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelTroubleDate.TabIndex = 0;
            this.uLabelTroubleDate.Text = "ultraLabel1";
            // 
            // uLabelRequestReason
            // 
            this.uLabelRequestReason.Location = new System.Drawing.Point(12, 244);
            this.uLabelRequestReason.Name = "uLabelRequestReason";
            this.uLabelRequestReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestReason.TabIndex = 0;
            this.uLabelRequestReason.Text = "ultraLabel1";
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(12, 220);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackage.TabIndex = 0;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uLabelPKType
            // 
            this.uLabelPKType.Location = new System.Drawing.Point(12, 196);
            this.uLabelPKType.Name = "uLabelPKType";
            this.uLabelPKType.Size = new System.Drawing.Size(100, 20);
            this.uLabelPKType.TabIndex = 0;
            this.uLabelPKType.Text = "ultraLabel1";
            // 
            // uLabelCarrier
            // 
            this.uLabelCarrier.Location = new System.Drawing.Point(12, 172);
            this.uLabelCarrier.Name = "uLabelCarrier";
            this.uLabelCarrier.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarrier.TabIndex = 0;
            this.uLabelCarrier.Text = "ultraLabel1";
            // 
            // uLabelEquipmentName
            // 
            this.uLabelEquipmentName.Location = new System.Drawing.Point(12, 276);
            this.uLabelEquipmentName.Name = "uLabelEquipmentName";
            this.uLabelEquipmentName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipmentName.TabIndex = 0;
            this.uLabelEquipmentName.Text = "ultraLabel1";
            // 
            // uLabelGoods
            // 
            this.uLabelGoods.Location = new System.Drawing.Point(12, 148);
            this.uLabelGoods.Name = "uLabelGoods";
            this.uLabelGoods.Size = new System.Drawing.Size(100, 20);
            this.uLabelGoods.TabIndex = 0;
            this.uLabelGoods.Text = "ultraLabel1";
            // 
            // uLabelRepairRequestUser
            // 
            this.uLabelRepairRequestUser.Location = new System.Drawing.Point(12, 52);
            this.uLabelRepairRequestUser.Name = "uLabelRepairRequestUser";
            this.uLabelRepairRequestUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairRequestUser.TabIndex = 0;
            this.uLabelRepairRequestUser.Text = "ultraLabel1";
            // 
            // uLabelEquipmentCode
            // 
            this.uLabelEquipmentCode.Location = new System.Drawing.Point(12, 76);
            this.uLabelEquipmentCode.Name = "uLabelEquipmentCode";
            this.uLabelEquipmentCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipmentCode.TabIndex = 0;
            this.uLabelEquipmentCode.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBoxAcceptRepair
            // 
            this.uGroupBoxAcceptRepair.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxAcceptRepair.Controls.Add(this.uTextReceiptDesc);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uButtonCancelAccept);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uButtonAccept);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uCheckHeadFlag);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uCheckChangeFlag);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uLabelNote);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uComboRepairResult);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uLabelStage);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uButtonCancelResult);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uLabelAcceptUser);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uButtonUseSP);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uLabelAcceptDate);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uLabelRepairResult);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uButtonChangeMold);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uDateReceiptDate);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uTextReceiptID);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uTextReceiptName);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uCheckCCSReqFlag);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uTextStageDesc);
            this.uGroupBoxAcceptRepair.Controls.Add(this.uLabelRepairTime);
            this.uGroupBoxAcceptRepair.Location = new System.Drawing.Point(352, 496);
            this.uGroupBoxAcceptRepair.Name = "uGroupBoxAcceptRepair";
            this.uGroupBoxAcceptRepair.Size = new System.Drawing.Size(328, 345);
            this.uGroupBoxAcceptRepair.TabIndex = 6;
            // 
            // uTextReceiptDesc
            // 
            this.uTextReceiptDesc.Location = new System.Drawing.Point(112, 124);
            this.uTextReceiptDesc.Name = "uTextReceiptDesc";
            this.uTextReceiptDesc.Size = new System.Drawing.Size(204, 52);
            this.uTextReceiptDesc.TabIndex = 22;
            this.uTextReceiptDesc.Text = "";
            // 
            // uButtonCancelAccept
            // 
            this.uButtonCancelAccept.Location = new System.Drawing.Point(164, 304);
            this.uButtonCancelAccept.Name = "uButtonCancelAccept";
            this.uButtonCancelAccept.Size = new System.Drawing.Size(88, 28);
            this.uButtonCancelAccept.TabIndex = 24;
            this.uButtonCancelAccept.Text = "ultraButton1";
            this.uButtonCancelAccept.Click += new System.EventHandler(this.uButtonCancelAccept_Click);
            // 
            // uButtonAccept
            // 
            this.uButtonAccept.Location = new System.Drawing.Point(72, 304);
            this.uButtonAccept.Name = "uButtonAccept";
            this.uButtonAccept.Size = new System.Drawing.Size(88, 28);
            this.uButtonAccept.TabIndex = 23;
            this.uButtonAccept.Text = "ultraButton1";
            this.uButtonAccept.Click += new System.EventHandler(this.uButtonAccept_Click_1);
            // 
            // uCheckHeadFlag
            // 
            appearance33.BackColor = System.Drawing.Color.White;
            this.uCheckHeadFlag.Appearance = appearance33;
            this.uCheckHeadFlag.BackColor = System.Drawing.Color.White;
            this.uCheckHeadFlag.BackColorInternal = System.Drawing.Color.White;
            this.uCheckHeadFlag.Enabled = false;
            this.uCheckHeadFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckHeadFlag.Location = new System.Drawing.Point(112, 76);
            this.uCheckHeadFlag.Name = "uCheckHeadFlag";
            this.uCheckHeadFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckHeadFlag.TabIndex = 20;
            this.uCheckHeadFlag.Text = "Head";
            this.uCheckHeadFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckChangeFlag
            // 
            this.uCheckChangeFlag.Enabled = false;
            this.uCheckChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckChangeFlag.Location = new System.Drawing.Point(232, 180);
            this.uCheckChangeFlag.Name = "uCheckChangeFlag";
            this.uCheckChangeFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckChangeFlag.TabIndex = 33;
            this.uCheckChangeFlag.Text = "교체의뢰";
            this.uCheckChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckChangeFlag.Visible = false;
            // 
            // uLabelNote
            // 
            this.uLabelNote.Location = new System.Drawing.Point(8, 124);
            this.uLabelNote.Name = "uLabelNote";
            this.uLabelNote.Size = new System.Drawing.Size(100, 20);
            this.uLabelNote.TabIndex = 0;
            this.uLabelNote.Text = "ultraLabel2";
            // 
            // uComboRepairResult
            // 
            this.uComboRepairResult.Location = new System.Drawing.Point(128, 180);
            this.uComboRepairResult.MaxLength = 10;
            this.uComboRepairResult.Name = "uComboRepairResult";
            this.uComboRepairResult.Size = new System.Drawing.Size(100, 21);
            this.uComboRepairResult.TabIndex = 32;
            this.uComboRepairResult.Text = "ultraComboEditor6";
            this.uComboRepairResult.Visible = false;
            this.uComboRepairResult.ValueChanged += new System.EventHandler(this.uComboRepairResult_ValueChanged);
            // 
            // uLabelStage
            // 
            this.uLabelStage.Location = new System.Drawing.Point(8, 100);
            this.uLabelStage.Name = "uLabelStage";
            this.uLabelStage.Size = new System.Drawing.Size(100, 20);
            this.uLabelStage.TabIndex = 0;
            this.uLabelStage.Text = "ultraLabel2";
            // 
            // uButtonCancelResult
            // 
            this.uButtonCancelResult.Location = new System.Drawing.Point(72, 268);
            this.uButtonCancelResult.Name = "uButtonCancelResult";
            this.uButtonCancelResult.Size = new System.Drawing.Size(120, 28);
            this.uButtonCancelResult.TabIndex = 37;
            this.uButtonCancelResult.Text = "ultraButton1";
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(8, 24);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelAcceptUser.TabIndex = 0;
            this.uLabelAcceptUser.Text = "ultraLabel2";
            // 
            // uButtonUseSP
            // 
            this.uButtonUseSP.Location = new System.Drawing.Point(196, 236);
            this.uButtonUseSP.Name = "uButtonUseSP";
            this.uButtonUseSP.Size = new System.Drawing.Size(120, 28);
            this.uButtonUseSP.TabIndex = 36;
            this.uButtonUseSP.Text = "ultraButton3";
            this.uButtonUseSP.Visible = false;
            this.uButtonUseSP.Click += new System.EventHandler(this.uButtonUseSP_Click);
            // 
            // uLabelAcceptDate
            // 
            this.uLabelAcceptDate.Location = new System.Drawing.Point(8, 48);
            this.uLabelAcceptDate.Name = "uLabelAcceptDate";
            this.uLabelAcceptDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelAcceptDate.TabIndex = 0;
            this.uLabelAcceptDate.Text = "ultraLabel2";
            // 
            // uLabelRepairResult
            // 
            this.uLabelRepairResult.Location = new System.Drawing.Point(24, 180);
            this.uLabelRepairResult.Name = "uLabelRepairResult";
            this.uLabelRepairResult.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairResult.TabIndex = 0;
            this.uLabelRepairResult.Text = "ultraLabel3";
            this.uLabelRepairResult.Visible = false;
            // 
            // uButtonChangeMold
            // 
            this.uButtonChangeMold.Location = new System.Drawing.Point(72, 236);
            this.uButtonChangeMold.Name = "uButtonChangeMold";
            this.uButtonChangeMold.Size = new System.Drawing.Size(120, 28);
            this.uButtonChangeMold.TabIndex = 35;
            this.uButtonChangeMold.Text = "ultraButton3";
            this.uButtonChangeMold.Visible = false;
            this.uButtonChangeMold.Click += new System.EventHandler(this.uButtonChangeMold_Click);
            // 
            // uDateReceiptDate
            // 
            appearance34.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReceiptDate.Appearance = appearance34;
            this.uDateReceiptDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReceiptDate.DateTime = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateReceiptDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReceiptDate.Location = new System.Drawing.Point(112, 48);
            this.uDateReceiptDate.Name = "uDateReceiptDate";
            this.uDateReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReceiptDate.TabIndex = 19;
            this.uDateReceiptDate.Value = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            // 
            // uTextReceiptID
            // 
            appearance35.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReceiptID.Appearance = appearance35;
            this.uTextReceiptID.BackColor = System.Drawing.Color.PowderBlue;
            appearance36.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance36;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReceiptID.ButtonsRight.Add(editorButton5);
            this.uTextReceiptID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReceiptID.Location = new System.Drawing.Point(112, 24);
            this.uTextReceiptID.MaxLength = 20;
            this.uTextReceiptID.Name = "uTextReceiptID";
            this.uTextReceiptID.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptID.TabIndex = 18;
            this.uTextReceiptID.ValueChanged += new System.EventHandler(this.uTextReceiptID_ValueChanged);
            this.uTextReceiptID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReceiptID_KeyDown);
            this.uTextReceiptID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReceiptID_EditorButtonClick);
            // 
            // uTextReceiptName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptName.Appearance = appearance37;
            this.uTextReceiptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptName.Location = new System.Drawing.Point(216, 24);
            this.uTextReceiptName.Name = "uTextReceiptName";
            this.uTextReceiptName.ReadOnly = true;
            this.uTextReceiptName.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptName.TabIndex = 3;
            // 
            // uCheckCCSReqFlag
            // 
            this.uCheckCCSReqFlag.Enabled = false;
            this.uCheckCCSReqFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCCSReqFlag.Location = new System.Drawing.Point(200, 268);
            this.uCheckCCSReqFlag.Name = "uCheckCCSReqFlag";
            this.uCheckCCSReqFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckCCSReqFlag.TabIndex = 38;
            this.uCheckCCSReqFlag.Text = "CCS의뢰";
            this.uCheckCCSReqFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCCSReqFlag.Visible = false;
            // 
            // uTextStageDesc
            // 
            appearance38.BackColor = System.Drawing.Color.White;
            this.uTextStageDesc.Appearance = appearance38;
            this.uTextStageDesc.BackColor = System.Drawing.Color.White;
            this.uTextStageDesc.Enabled = false;
            this.uTextStageDesc.Location = new System.Drawing.Point(112, 100);
            this.uTextStageDesc.Name = "uTextStageDesc";
            this.uTextStageDesc.Size = new System.Drawing.Size(110, 21);
            this.uTextStageDesc.TabIndex = 21;
            // 
            // uLabelRepairTime
            // 
            this.uLabelRepairTime.Location = new System.Drawing.Point(216, 208);
            this.uLabelRepairTime.Name = "uLabelRepairTime";
            this.uLabelRepairTime.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairTime.TabIndex = 0;
            this.uLabelRepairTime.Text = "ultraLabel3";
            // 
            // uGroupBoxRepairResult
            // 
            this.uGroupBoxRepairResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxRepairResult.Controls.Add(this.uCheckImportant);
            this.uGroupBoxRepairResult.Controls.Add(this.uButtonChange);
            this.uGroupBoxRepairResult.Controls.Add(this.uButtonCCS);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelNotice);
            this.uGroupBoxRepairResult.Controls.Add(this.uButtonChg);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairDesc);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextResultEquip);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelEquip);
            this.uGroupBoxRepairResult.Controls.Add(this.uDateRepairEndTime);
            this.uGroupBoxRepairResult.Controls.Add(this.uButtonSaveResult);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabel1);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabel);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelSCV);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelRepairDate);
            this.uGroupBoxRepairResult.Controls.Add(this.uDateRepairDate);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairUserID);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextFaultTypeName);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextFaultTypeCode);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairUserName);
            this.uGroupBoxRepairResult.Location = new System.Drawing.Point(684, 496);
            this.uGroupBoxRepairResult.Name = "uGroupBoxRepairResult";
            this.uGroupBoxRepairResult.Size = new System.Drawing.Size(384, 345);
            this.uGroupBoxRepairResult.TabIndex = 7;
            // 
            // uCheckImportant
            // 
            appearance47.FontData.BoldAsString = "True";
            appearance47.FontData.SizeInPoints = 9F;
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            this.uCheckImportant.Appearance = appearance47;
            this.uCheckImportant.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckImportant.Location = new System.Drawing.Point(220, 56);
            this.uCheckImportant.Name = "uCheckImportant";
            this.uCheckImportant.Size = new System.Drawing.Size(100, 20);
            this.uCheckImportant.TabIndex = 43;
            this.uCheckImportant.Text = "중요수리여부";
            this.uCheckImportant.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uButtonChange
            // 
            this.uButtonChange.Location = new System.Drawing.Point(52, 304);
            this.uButtonChange.Name = "uButtonChange";
            this.uButtonChange.Size = new System.Drawing.Size(120, 28);
            this.uButtonChange.TabIndex = 42;
            this.uButtonChange.Click += new System.EventHandler(this.uButtonChange_Click);
            // 
            // uButtonCCS
            // 
            this.uButtonCCS.Location = new System.Drawing.Point(124, 200);
            this.uButtonCCS.Name = "uButtonCCS";
            this.uButtonCCS.Size = new System.Drawing.Size(130, 50);
            this.uButtonCCS.TabIndex = 41;
            this.uButtonCCS.Visible = false;
            this.uButtonCCS.Click += new System.EventHandler(this.uButtonCCS_Click);
            // 
            // uLabelNotice
            // 
            appearance39.TextHAlignAsString = "Center";
            this.uLabelNotice.Appearance = appearance39;
            this.uLabelNotice.Location = new System.Drawing.Point(24, 108);
            this.uLabelNotice.Name = "uLabelNotice";
            this.uLabelNotice.Size = new System.Drawing.Size(332, 76);
            this.uLabelNotice.TabIndex = 40;
            this.uLabelNotice.Text = "24, 108";
            this.uLabelNotice.Visible = false;
            // 
            // uButtonChg
            // 
            this.uButtonChg.Location = new System.Drawing.Point(124, 200);
            this.uButtonChg.Name = "uButtonChg";
            this.uButtonChg.Size = new System.Drawing.Size(130, 50);
            this.uButtonChg.TabIndex = 39;
            this.uButtonChg.Visible = false;
            this.uButtonChg.Click += new System.EventHandler(this.uButtonChg_Click);
            // 
            // uTextRepairDesc
            // 
            this.uTextRepairDesc.Location = new System.Drawing.Point(116, 104);
            this.uTextRepairDesc.Name = "uTextRepairDesc";
            this.uTextRepairDesc.Size = new System.Drawing.Size(204, 52);
            this.uTextRepairDesc.TabIndex = 28;
            this.uTextRepairDesc.Text = "";
            // 
            // uTextResultEquip
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextResultEquip.Appearance = appearance40;
            this.uTextResultEquip.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextResultEquip.Location = new System.Drawing.Point(116, 56);
            this.uTextResultEquip.Name = "uTextResultEquip";
            this.uTextResultEquip.ReadOnly = true;
            this.uTextResultEquip.Size = new System.Drawing.Size(100, 21);
            this.uTextResultEquip.TabIndex = 13;
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(12, 56);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquip.TabIndex = 12;
            this.uLabelEquip.Text = "ultraLabel1";
            // 
            // uDateRepairEndTime
            // 
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateRepairEndTime.ButtonsRight.Add(spinEditorButton2);
            this.uDateRepairEndTime.DateTime = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateRepairEndTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateRepairEndTime.Location = new System.Drawing.Point(220, 164);
            this.uDateRepairEndTime.MaskInput = "hh:mm";
            this.uDateRepairEndTime.Name = "uDateRepairEndTime";
            this.uDateRepairEndTime.Size = new System.Drawing.Size(70, 21);
            this.uDateRepairEndTime.TabIndex = 31;
            this.uDateRepairEndTime.Value = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateRepairEndTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.EditorSpinButtonClick_1);
            // 
            // uButtonSaveResult
            // 
            this.uButtonSaveResult.Location = new System.Drawing.Point(196, 304);
            this.uButtonSaveResult.Name = "uButtonSaveResult";
            this.uButtonSaveResult.Size = new System.Drawing.Size(120, 28);
            this.uButtonSaveResult.TabIndex = 34;
            this.uButtonSaveResult.Text = "ultraButton3";
            this.uButtonSaveResult.Click += new System.EventHandler(this.uButtonResultSave_Click);
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(12, 104);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(100, 20);
            this.uLabel1.TabIndex = 0;
            this.uLabel1.Text = "ultraLabel3";
            // 
            // uLabel
            // 
            this.uLabel.Location = new System.Drawing.Point(12, 80);
            this.uLabel.Name = "uLabel";
            this.uLabel.Size = new System.Drawing.Size(100, 20);
            this.uLabel.TabIndex = 0;
            this.uLabel.Text = "ultraLabel3";
            // 
            // uLabelSCV
            // 
            this.uLabelSCV.Location = new System.Drawing.Point(12, 32);
            this.uLabelSCV.Name = "uLabelSCV";
            this.uLabelSCV.Size = new System.Drawing.Size(100, 20);
            this.uLabelSCV.TabIndex = 0;
            this.uLabelSCV.Text = "ultraLabel3";
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(12, 164);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairDate.TabIndex = 0;
            this.uLabelRepairDate.Text = "ultraLabel3";
            // 
            // uDateRepairDate
            // 
            appearance41.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairDate.Appearance = appearance41;
            this.uDateRepairDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairDate.DateTime = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            this.uDateRepairDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairDate.Location = new System.Drawing.Point(116, 164);
            this.uDateRepairDate.Name = "uDateRepairDate";
            this.uDateRepairDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairDate.TabIndex = 29;
            this.uDateRepairDate.Value = new System.DateTime(2012, 1, 5, 0, 0, 0, 0);
            // 
            // uTextRepairUserID
            // 
            appearance42.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairUserID.Appearance = appearance42;
            this.uTextRepairUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance43.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance43.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance43;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairUserID.ButtonsRight.Add(editorButton6);
            this.uTextRepairUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepairUserID.Location = new System.Drawing.Point(116, 32);
            this.uTextRepairUserID.MaxLength = 20;
            this.uTextRepairUserID.Name = "uTextRepairUserID";
            this.uTextRepairUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserID.TabIndex = 25;
            this.uTextRepairUserID.ValueChanged += new System.EventHandler(this.uTextRepairUserID_ValueChanged);
            this.uTextRepairUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRepairUserID_KeyDown);
            this.uTextRepairUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRepairUserID_EditorButtonClick);
            // 
            // uTextFaultTypeName
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFaultTypeName.Appearance = appearance48;
            this.uTextFaultTypeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFaultTypeName.Location = new System.Drawing.Point(220, 80);
            this.uTextFaultTypeName.Name = "uTextFaultTypeName";
            this.uTextFaultTypeName.ReadOnly = true;
            this.uTextFaultTypeName.Size = new System.Drawing.Size(100, 21);
            this.uTextFaultTypeName.TabIndex = 27;
            // 
            // uTextFaultTypeCode
            // 
            appearance44.BackColor = System.Drawing.Color.White;
            this.uTextFaultTypeCode.Appearance = appearance44;
            this.uTextFaultTypeCode.BackColor = System.Drawing.Color.White;
            appearance45.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance45.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance45;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextFaultTypeCode.ButtonsRight.Add(editorButton7);
            this.uTextFaultTypeCode.Location = new System.Drawing.Point(116, 80);
            this.uTextFaultTypeCode.Name = "uTextFaultTypeCode";
            this.uTextFaultTypeCode.ReadOnly = true;
            this.uTextFaultTypeCode.Size = new System.Drawing.Size(100, 21);
            this.uTextFaultTypeCode.TabIndex = 26;
            this.uTextFaultTypeCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFaultTypeCode_EditorButtonClick);
            // 
            // uTextRepairUserName
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Appearance = appearance46;
            this.uTextRepairUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Location = new System.Drawing.Point(220, 32);
            this.uTextRepairUserName.Name = "uTextRepairUserName";
            this.uTextRepairUserName.ReadOnly = true;
            this.uTextRepairUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserName.TabIndex = 3;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQUZ0022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxRepairResult);
            this.Controls.Add(this.uGroupBoxAcceptRepair);
            this.Controls.Add(this.uGroupBoxRequestRepair);
            this.Controls.Add(this.uGridEquipRepair);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0022";
            this.Load += new System.EventHandler(this.frmEQUZ0022_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0022_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipRepair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRequestRepair)).EndInit();
            this.uGroupBoxRequestRepair.ResumeLayout(false);
            this.uGroupBoxRequestRepair.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDownName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateBreakDownTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateBreakDownDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAutoAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUrgentFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPKType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAcceptRepair)).EndInit();
            this.uGroupBoxAcceptRepair.ResumeLayout(false);
            this.uGroupBoxAcceptRepair.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckHeadFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCCSReqFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStageDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRepairResult)).EndInit();
            this.uGroupBoxRepairResult.ResumeLayout(false);
            this.uGroupBoxRepairResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckImportant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipmentProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectEquipment;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipRepair;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxRequestRepair;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateBreakDownTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateBreakDownDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUrgentFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPKType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextContainer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairRequestDay;
        private Infragistics.Win.Misc.UltraLabel uLabelTroubleDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestReason;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPKType;
        private Infragistics.Win.Misc.UltraLabel uLabelCarrier;
        private Infragistics.Win.Misc.UltraLabel uLabelGoods;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairRequestUser;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipmentCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDownName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDownCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDown;
        private System.Windows.Forms.RichTextBox uTextRepairReqReason;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxAcceptRepair;
        private Infragistics.Win.Misc.UltraButton uButtonCancelAccept;
        private Infragistics.Win.Misc.UltraButton uButtonAccept;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckHeadFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelNote;
        private Infragistics.Win.Misc.UltraLabel uLabelStage;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStageDesc;
        private System.Windows.Forms.RichTextBox uTextReceiptDesc;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckChangeFlag;
        private Infragistics.Win.Misc.UltraButton uButtonCancelResult;
        private Infragistics.Win.Misc.UltraButton uButtonSaveResult;
        private Infragistics.Win.Misc.UltraButton uButtonUseSP;
        private Infragistics.Win.Misc.UltraButton uButtonChangeMold;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCCSReqFlag;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabel;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResult;
        private Infragistics.Win.Misc.UltraLabel uLabelSCV;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairTime;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultTypeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultTypeCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextResultEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private System.Windows.Forms.RichTextBox uTextRepairDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipmentName;
        private Infragistics.Win.Misc.UltraButton uButtonRepair;
        private Infragistics.Win.Misc.UltraButton uButtonRepairCancel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipState;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipState;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.Misc.UltraButton uButtonChg;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAutoAccept;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipState;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairEndTime;
        private Infragistics.Win.Misc.UltraLabel uLabelNotice;
        private Infragistics.Win.Misc.UltraButton uButtonCCS;
        private Infragistics.Win.Misc.UltraButton uButtonChange;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckImportant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDown;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDown;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
    }
}