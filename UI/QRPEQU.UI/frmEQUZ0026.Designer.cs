﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0026
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0026));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uOptionSelect = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchToDay = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchFromDay = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchFromMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchYear = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUesr = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridUser = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxResult = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextDefalut = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWaitEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextOk = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDefalut = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWaitEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelOk = new Infragistics.Win.Misc.UltraLabel();
            this.uText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWait = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAll = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWait = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAll = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).BeginInit();
            this.uGroupSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchToDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchFromDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchFromMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).BeginInit();
            this.uGroupBoxResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDefalut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWaitEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAll)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupSearchArea
            // 
            this.uGroupSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupSearchArea.Appearance = appearance1;
            this.uGroupSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupSearchArea.Controls.Add(this.uOptionSelect);
            this.uGroupSearchArea.Controls.Add(this.uTextUserName);
            this.uGroupSearchArea.Controls.Add(this.uTextUserID);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchToDay);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchFromDay);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchFromMonth);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipLargeType);
            this.uGroupSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchYear);
            this.uGroupSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelUesr);
            this.uGroupSearchArea.Controls.Add(this.uLabelEquipLoc);
            this.uGroupSearchArea.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupSearchArea.Controls.Add(this.uLabelEquipGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupSearchArea.Name = "uGroupSearchArea";
            this.uGroupSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupSearchArea.TabIndex = 1;
            // 
            // uOptionSelect
            // 
            this.uOptionSelect.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSelect.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "U";
            valueListItem1.DisplayText = "정비사";
            valueListItem2.DataValue = "E";
            valueListItem2.DisplayText = "설비";
            this.uOptionSelect.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionSelect.Location = new System.Drawing.Point(672, 12);
            this.uOptionSelect.Name = "uOptionSelect";
            this.uOptionSelect.Size = new System.Drawing.Size(140, 16);
            this.uOptionSelect.TabIndex = 29;
            this.uOptionSelect.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance2;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(772, 56);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 2;
            // 
            // uTextUserID
            // 
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton1);
            this.uTextUserID.Location = new System.Drawing.Point(668, 56);
            this.uTextUserID.MaxLength = 20;
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 12;
            this.uTextUserID.ValueChanged += new System.EventHandler(this.uTextUserID_ValueChanged);
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uComboSearchToDay
            // 
            this.uComboSearchToDay.Location = new System.Drawing.Point(608, 8);
            this.uComboSearchToDay.MaxLength = 4;
            this.uComboSearchToDay.Name = "uComboSearchToDay";
            this.uComboSearchToDay.Size = new System.Drawing.Size(56, 21);
            this.uComboSearchToDay.TabIndex = 3;
            // 
            // uComboSearchFromDay
            // 
            this.uComboSearchFromDay.Location = new System.Drawing.Point(540, 8);
            this.uComboSearchFromDay.MaxLength = 4;
            this.uComboSearchFromDay.Name = "uComboSearchFromDay";
            this.uComboSearchFromDay.Size = new System.Drawing.Size(56, 21);
            this.uComboSearchFromDay.TabIndex = 3;
            // 
            // uComboSearchFromMonth
            // 
            this.uComboSearchFromMonth.Location = new System.Drawing.Point(484, 8);
            this.uComboSearchFromMonth.MaxLength = 4;
            this.uComboSearchFromMonth.Name = "uComboSearchFromMonth";
            this.uComboSearchFromMonth.Size = new System.Drawing.Size(56, 21);
            this.uComboSearchFromMonth.TabIndex = 3;
            this.uComboSearchFromMonth.ValueChanged += new System.EventHandler(this.uComboSearchFromMonth_ValueChanged);
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(380, 56);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipGroup.TabIndex = 11;
            // 
            // uComboSearchEquipLargeType
            // 
            this.uComboSearchEquipLargeType.Location = new System.Drawing.Point(116, 56);
            this.uComboSearchEquipLargeType.MaxLength = 50;
            this.uComboSearchEquipLargeType.Name = "uComboSearchEquipLargeType";
            this.uComboSearchEquipLargeType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLargeType.TabIndex = 10;
            this.uComboSearchEquipLargeType.ValueChanged += new System.EventHandler(this.uComboSearchEquType_ValueChanged);
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(668, 32);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboProcessGroup.TabIndex = 9;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(380, 32);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLoc.TabIndex = 8;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(116, 32);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchStation.TabIndex = 7;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uComboSearchYear
            // 
            this.uComboSearchYear.Location = new System.Drawing.Point(380, 8);
            this.uComboSearchYear.MaxLength = 5;
            this.uComboSearchYear.Name = "uComboSearchYear";
            this.uComboSearchYear.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchYear.TabIndex = 2;
            this.uComboSearchYear.ValueChanged += new System.EventHandler(this.uComboSearchYear_ValueChanged);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(596, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "~";
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(564, 32);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 0;
            // 
            // uLabelUesr
            // 
            this.uLabelUesr.Location = new System.Drawing.Point(564, 56);
            this.uLabelUesr.Name = "uLabelUesr";
            this.uLabelUesr.Size = new System.Drawing.Size(100, 20);
            this.uLabelUesr.TabIndex = 0;
            // 
            // uLabelEquipLoc
            // 
            this.uLabelEquipLoc.Location = new System.Drawing.Point(276, 32);
            this.uLabelEquipLoc.Name = "uLabelEquipLoc";
            this.uLabelEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLoc.TabIndex = 0;
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(12, 56);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 0;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(276, 56);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroup.TabIndex = 0;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(12, 32);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 0;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(276, 8);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 0;
            // 
            // uGridEquip
            // 
            this.uGridEquip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquip.DisplayLayout.Appearance = appearance4;
            this.uGridEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquip.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquip.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquip.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGridEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquip.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGridEquip.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquip.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquip.Location = new System.Drawing.Point(0, 160);
            this.uGridEquip.Name = "uGridEquip";
            this.uGridEquip.Size = new System.Drawing.Size(1070, 672);
            this.uGridEquip.TabIndex = 2;
            this.uGridEquip.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEquip_DoubleClickCell);
            // 
            // uGridUser
            // 
            this.uGridUser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridUser.DisplayLayout.Appearance = appearance16;
            this.uGridUser.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridUser.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUser.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUser.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uGridUser.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUser.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uGridUser.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridUser.DisplayLayout.MaxRowScrollRegions = 1;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridUser.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridUser.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uGridUser.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridUser.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGridUser.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridUser.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGridUser.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridUser.DisplayLayout.Override.CellPadding = 0;
            appearance24.BackColor = System.Drawing.SystemColors.Control;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUser.DisplayLayout.Override.GroupByRowAppearance = appearance24;
            appearance25.TextHAlignAsString = "Left";
            this.uGridUser.DisplayLayout.Override.HeaderAppearance = appearance25;
            this.uGridUser.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridUser.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGridUser.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGridUser.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridUser.DisplayLayout.Override.TemplateAddRowAppearance = appearance27;
            this.uGridUser.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridUser.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridUser.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridUser.Location = new System.Drawing.Point(0, 160);
            this.uGridUser.Name = "uGridUser";
            this.uGridUser.Size = new System.Drawing.Size(1070, 676);
            this.uGridUser.TabIndex = 2;
            this.uGridUser.Visible = false;
            this.uGridUser.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridUser_DoubleClickCell);
            // 
            // uGroupBoxResult
            // 
            this.uGroupBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxResult.Controls.Add(this.uTextDefalut);
            this.uGroupBoxResult.Controls.Add(this.uTextWaitEquip);
            this.uGroupBoxResult.Controls.Add(this.uTextOk);
            this.uGroupBoxResult.Controls.Add(this.uLabelDefalut);
            this.uGroupBoxResult.Controls.Add(this.uLabelWaitEquip);
            this.uGroupBoxResult.Controls.Add(this.uLabelOk);
            this.uGroupBoxResult.Controls.Add(this.uText);
            this.uGroupBoxResult.Controls.Add(this.uTextWait);
            this.uGroupBoxResult.Controls.Add(this.uTextAll);
            this.uGroupBoxResult.Controls.Add(this.uLabel);
            this.uGroupBoxResult.Controls.Add(this.uLabelWait);
            this.uGroupBoxResult.Controls.Add(this.uLabelAll);
            this.uGroupBoxResult.Location = new System.Drawing.Point(0, 120);
            this.uGroupBoxResult.Name = "uGroupBoxResult";
            this.uGroupBoxResult.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxResult.TabIndex = 3;
            // 
            // uTextDefalut
            // 
            appearance33.BackColor = System.Drawing.Color.Gold;
            this.uTextDefalut.Appearance = appearance33;
            this.uTextDefalut.BackColor = System.Drawing.Color.Gold;
            this.uTextDefalut.Location = new System.Drawing.Point(1004, 16);
            this.uTextDefalut.Multiline = true;
            this.uTextDefalut.Name = "uTextDefalut";
            this.uTextDefalut.ReadOnly = true;
            this.uTextDefalut.Size = new System.Drawing.Size(20, 12);
            this.uTextDefalut.TabIndex = 3;
            // 
            // uTextWaitEquip
            // 
            appearance32.BackColor = System.Drawing.Color.Salmon;
            this.uTextWaitEquip.Appearance = appearance32;
            this.uTextWaitEquip.BackColor = System.Drawing.Color.Salmon;
            this.uTextWaitEquip.Location = new System.Drawing.Point(880, 16);
            this.uTextWaitEquip.Multiline = true;
            this.uTextWaitEquip.Name = "uTextWaitEquip";
            this.uTextWaitEquip.ReadOnly = true;
            this.uTextWaitEquip.Size = new System.Drawing.Size(20, 12);
            this.uTextWaitEquip.TabIndex = 3;
            // 
            // uTextOk
            // 
            appearance31.BackColor = System.Drawing.Color.White;
            this.uTextOk.Appearance = appearance31;
            this.uTextOk.BackColor = System.Drawing.Color.White;
            this.uTextOk.Location = new System.Drawing.Point(780, 16);
            this.uTextOk.Multiline = true;
            this.uTextOk.Name = "uTextOk";
            this.uTextOk.ReadOnly = true;
            this.uTextOk.Size = new System.Drawing.Size(20, 12);
            this.uTextOk.TabIndex = 3;
            // 
            // uLabelDefalut
            // 
            this.uLabelDefalut.Location = new System.Drawing.Point(908, 12);
            this.uLabelDefalut.Name = "uLabelDefalut";
            this.uLabelDefalut.Size = new System.Drawing.Size(96, 20);
            this.uLabelDefalut.TabIndex = 2;
            // 
            // uLabelWaitEquip
            // 
            this.uLabelWaitEquip.Location = new System.Drawing.Point(808, 12);
            this.uLabelWaitEquip.Name = "uLabelWaitEquip";
            this.uLabelWaitEquip.Size = new System.Drawing.Size(70, 20);
            this.uLabelWaitEquip.TabIndex = 2;
            // 
            // uLabelOk
            // 
            this.uLabelOk.Location = new System.Drawing.Point(708, 12);
            this.uLabelOk.Name = "uLabelOk";
            this.uLabelOk.Size = new System.Drawing.Size(70, 20);
            this.uLabelOk.TabIndex = 2;
            // 
            // uText
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uText.Appearance = appearance28;
            this.uText.BackColor = System.Drawing.Color.Gainsboro;
            this.uText.Location = new System.Drawing.Point(556, 12);
            this.uText.Name = "uText";
            this.uText.ReadOnly = true;
            this.uText.Size = new System.Drawing.Size(100, 21);
            this.uText.TabIndex = 1;
            // 
            // uTextWait
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWait.Appearance = appearance29;
            this.uTextWait.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWait.Location = new System.Drawing.Point(336, 12);
            this.uTextWait.Name = "uTextWait";
            this.uTextWait.ReadOnly = true;
            this.uTextWait.Size = new System.Drawing.Size(100, 21);
            this.uTextWait.TabIndex = 1;
            // 
            // uTextAll
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAll.Appearance = appearance30;
            this.uTextAll.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAll.Location = new System.Drawing.Point(116, 12);
            this.uTextAll.Name = "uTextAll";
            this.uTextAll.ReadOnly = true;
            this.uTextAll.Size = new System.Drawing.Size(100, 21);
            this.uTextAll.TabIndex = 1;
            // 
            // uLabel
            // 
            this.uLabel.Location = new System.Drawing.Point(452, 12);
            this.uLabel.Name = "uLabel";
            this.uLabel.Size = new System.Drawing.Size(100, 20);
            this.uLabel.TabIndex = 0;
            // 
            // uLabelWait
            // 
            this.uLabelWait.Location = new System.Drawing.Point(232, 12);
            this.uLabelWait.Name = "uLabelWait";
            this.uLabelWait.Size = new System.Drawing.Size(100, 20);
            this.uLabelWait.TabIndex = 0;
            // 
            // uLabelAll
            // 
            this.uLabelAll.Location = new System.Drawing.Point(12, 12);
            this.uLabelAll.Name = "uLabelAll";
            this.uLabelAll.Size = new System.Drawing.Size(100, 20);
            this.uLabelAll.TabIndex = 0;
            // 
            // frmEQUZ0026
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxResult);
            this.Controls.Add(this.uGridUser);
            this.Controls.Add(this.uGridEquip);
            this.Controls.Add(this.uGroupSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0026";
            this.Load += new System.EventHandler(this.frmEQUZ0026_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0026_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0026_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).EndInit();
            this.uGroupSearchArea.ResumeLayout(false);
            this.uGroupSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchToDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchFromDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchFromMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).EndInit();
            this.uGroupBoxResult.ResumeLayout(false);
            this.uGroupBoxResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDefalut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWaitEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAll)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchFromMonth;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelUesr;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquip;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUser;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxResult;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWait;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAll;
        private Infragistics.Win.Misc.UltraLabel uLabel;
        private Infragistics.Win.Misc.UltraLabel uLabelWait;
        private Infragistics.Win.Misc.UltraLabel uLabelAll;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSelect;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchFromDay;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchToDay;
        private Infragistics.Win.Misc.UltraLabel uLabelOk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDefalut;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWaitEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOk;
        private Infragistics.Win.Misc.UltraLabel uLabelDefalut;
        private Infragistics.Win.Misc.UltraLabel uLabelWaitEquip;
    }
}