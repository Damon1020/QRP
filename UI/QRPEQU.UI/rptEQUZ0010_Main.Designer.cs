﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_Main.
    /// </summary>
    partial class rptEQUZ0010_Main
    {
        private DataDynamics.ActiveReports.PageHeader pageHeader;
        private DataDynamics.ActiveReports.Detail detail;
        private DataDynamics.ActiveReports.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptEQUZ0010_Main));
            this.pageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.textPlantCode = new DataDynamics.ActiveReports.TextBox();
            this.label5 = new DataDynamics.ActiveReports.Label();
            this.textObject = new DataDynamics.ActiveReports.TextBox();
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.subReportState = new DataDynamics.ActiveReports.SubReport();
            this.subReportJobPK = new DataDynamics.ActiveReports.SubReport();
            this.subReportCheck = new DataDynamics.ActiveReports.SubReport();
            this.subReportEquip = new DataDynamics.ActiveReports.SubReport();
            this.pageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.rptHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.labelTitle = new DataDynamics.ActiveReports.Label();
            this.crossSectionLine1 = new DataDynamics.ActiveReports.CrossSectionLine();
            this.crossSectionLine2 = new DataDynamics.ActiveReports.CrossSectionLine();
            this.line1 = new DataDynamics.ActiveReports.Line();
            this.rptFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.line3 = new DataDynamics.ActiveReports.Line();
            this.groupHeader = new DataDynamics.ActiveReports.GroupHeader();
            this.label4 = new DataDynamics.ActiveReports.Label();
            this.label3 = new DataDynamics.ActiveReports.Label();
            this.textDocCode = new DataDynamics.ActiveReports.TextBox();
            this.textCertiReqID = new DataDynamics.ActiveReports.TextBox();
            this.textCertiReqDate = new DataDynamics.ActiveReports.TextBox();
            this.label7 = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.textBox3 = new DataDynamics.ActiveReports.TextBox();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.textSubject = new DataDynamics.ActiveReports.TextBox();
            this.label6 = new DataDynamics.ActiveReports.Label();
            this.label8 = new DataDynamics.ActiveReports.Label();
            this.label9 = new DataDynamics.ActiveReports.Label();
            this.label10 = new DataDynamics.ActiveReports.Label();
            this.label11 = new DataDynamics.ActiveReports.Label();
            this.label12 = new DataDynamics.ActiveReports.Label();
            this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            this.textBox10 = new DataDynamics.ActiveReports.TextBox();
            this.textBox7 = new DataDynamics.ActiveReports.TextBox();
            this.textBox4 = new DataDynamics.ActiveReports.TextBox();
            this.textBox6 = new DataDynamics.ActiveReports.TextBox();
            this.textBox5 = new DataDynamics.ActiveReports.TextBox();
            this.textBox1 = new DataDynamics.ActiveReports.TextBox();
            this.txtDRAFT = new DataDynamics.ActiveReports.TextBox();
            this.textBox8 = new DataDynamics.ActiveReports.TextBox();
            this.textBox9 = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.textPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCertiReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCertiReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSubject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRAFT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // textPlantCode
            // 
            this.textPlantCode.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPlantCode.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPlantCode.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPlantCode.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPlantCode.DataField = "PlantCode";
            this.textPlantCode.Height = 0.148F;
            this.textPlantCode.Left = 7.008F;
            this.textPlantCode.Name = "textPlantCode";
            this.textPlantCode.Style = "font-size: 9pt";
            this.textPlantCode.Text = null;
            this.textPlantCode.Top = 0.678F;
            this.textPlantCode.Visible = false;
            this.textPlantCode.Width = 0.217F;
            // 
            // label5
            // 
            this.label5.Height = 0.2F;
            this.label5.HyperLink = null;
            this.label5.Left = 0.04000001F;
            this.label5.Name = "label5";
            this.label5.Style = "";
            this.label5.Text = "1 . 목         적 : ";
            this.label5.Top = 0.678F;
            this.label5.Width = 1F;
            // 
            // textObject
            // 
            this.textObject.DataField = "Object";
            this.textObject.Height = 0.311F;
            this.textObject.Left = 0.238F;
            this.textObject.Name = "textObject";
            this.textObject.Style = "font-size: 9pt; vertical-align: middle";
            this.textObject.Text = null;
            this.textObject.Top = 0.878F;
            this.textObject.Width = 6.827001F;
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.subReportState,
            this.subReportJobPK,
            this.subReportCheck,
            this.subReportEquip});
            this.detail.Height = 3.292166F;
            this.detail.Name = "detail";
            // 
            // subReportState
            // 
            this.subReportState.CloseBorder = false;
            this.subReportState.Height = 0.9065F;
            this.subReportState.Left = 0.021F;
            this.subReportState.Name = "subReportState";
            this.subReportState.Report = null;
            this.subReportState.ReportName = "subReport1";
            this.subReportState.Top = 0.583F;
            this.subReportState.Width = 7.204F;
            // 
            // subReportJobPK
            // 
            this.subReportJobPK.CloseBorder = false;
            this.subReportJobPK.Height = 0.8839999F;
            this.subReportJobPK.Left = 0.021F;
            this.subReportJobPK.Name = "subReportJobPK";
            this.subReportJobPK.Report = null;
            this.subReportJobPK.ReportName = "subReport2";
            this.subReportJobPK.Top = 1.531F;
            this.subReportJobPK.Width = 7.204F;
            // 
            // subReportCheck
            // 
            this.subReportCheck.CloseBorder = false;
            this.subReportCheck.Height = 0.835F;
            this.subReportCheck.Left = 0.021F;
            this.subReportCheck.Name = "subReportCheck";
            this.subReportCheck.Report = null;
            this.subReportCheck.ReportName = "subReport3";
            this.subReportCheck.Top = 2.468F;
            this.subReportCheck.Width = 7.204F;
            // 
            // subReportEquip
            // 
            this.subReportEquip.CloseBorder = false;
            this.subReportEquip.Height = 0.583F;
            this.subReportEquip.Left = 0.021F;
            this.subReportEquip.Name = "subReportEquip";
            this.subReportEquip.Report = null;
            this.subReportEquip.ReportName = "subReport1";
            this.subReportEquip.Top = 0F;
            this.subReportEquip.Width = 7.204F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // rptHeader
            // 
            this.rptHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.labelTitle,
            this.crossSectionLine1,
            this.crossSectionLine2,
            this.line1,
            this.textBox5,
            this.textBox6,
            this.textBox4,
            this.textBox7,
            this.textBox10,
            this.textBox1,
            this.txtDRAFT,
            this.textBox8,
            this.textBox9});
            this.rptHeader.Height = 1.416667F;
            this.rptHeader.Name = "rptHeader";
            // 
            // labelTitle
            // 
            this.labelTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.labelTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.labelTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.labelTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.labelTitle.Height = 1.009F;
            this.labelTitle.HyperLink = null;
            this.labelTitle.Left = 0.144F;
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Style = "color: Blue; font-family: 바탕; font-size: 20pt; font-weight: bold; text-align: cen" +
                "ter; text-decoration: none";
            this.labelTitle.Text = "MACHINE SET-UP REPORT\r\n[]";
            this.labelTitle.Top = 0.187F;
            this.labelTitle.Width = 3.975F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0F;
            this.crossSectionLine1.Left = 0F;
            this.crossSectionLine1.LineWeight = 1F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0F;
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0F;
            this.crossSectionLine2.Left = 7.3F;
            this.crossSectionLine2.LineWeight = 1F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 0F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0F;
            this.line1.Width = 7.3F;
            this.line1.X1 = 0F;
            this.line1.X2 = 7.3F;
            this.line1.Y1 = 0F;
            this.line1.Y2 = 0F;
            // 
            // rptFooter
            // 
            this.rptFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.line3});
            this.rptFooter.Height = 0F;
            this.rptFooter.Name = "rptFooter";
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 7.3F;
            this.line3.X1 = 0F;
            this.line3.X2 = 7.3F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0F;
            // 
            // groupHeader
            // 
            this.groupHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textObject,
            this.label5,
            this.textPlantCode,
            this.label4,
            this.label3,
            this.textDocCode,
            this.textCertiReqID,
            this.textCertiReqDate,
            this.label7,
            this.label2,
            this.textBox3,
            this.label1,
            this.textSubject,
            this.label6,
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12});
            this.groupHeader.Height = 1.661417F;
            this.groupHeader.Name = "groupHeader";
            this.groupHeader.Format += new System.EventHandler(this.groupHeader_Format);
            // 
            // label4
            // 
            this.label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Height = 0.2F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.04000008F;
            this.label4.Name = "label4";
            this.label4.Style = "font-weight: bold; text-align: center";
            this.label4.Text = "제목";
            this.label4.Top = 0.4F;
            this.label4.Width = 1.096F;
            // 
            // label3
            // 
            this.label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.974002F;
            this.label3.Name = "label3";
            this.label3.Style = "font-weight: bold; text-align: center";
            this.label3.Text = "폐기일자";
            this.label3.Top = 0.2F;
            this.label3.Width = 1.096F;
            // 
            // textDocCode
            // 
            this.textDocCode.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textDocCode.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textDocCode.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textDocCode.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textDocCode.DataField = "DocCode";
            this.textDocCode.Height = 0.2F;
            this.textDocCode.Left = 1.136F;
            this.textDocCode.Name = "textDocCode";
            this.textDocCode.Style = "font-size: 9pt; vertical-align: middle";
            this.textDocCode.Text = null;
            this.textDocCode.Top = 0F;
            this.textDocCode.Width = 3.838F;
            // 
            // textCertiReqID
            // 
            this.textCertiReqID.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqID.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqID.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqID.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqID.DataField = "CertiReqName";
            this.textCertiReqID.Height = 0.2F;
            this.textCertiReqID.Left = 1.136F;
            this.textCertiReqID.Name = "textCertiReqID";
            this.textCertiReqID.Style = "font-size: 9pt; vertical-align: middle";
            this.textCertiReqID.Text = null;
            this.textCertiReqID.Top = 0.2F;
            this.textCertiReqID.Width = 3.838F;
            // 
            // textCertiReqDate
            // 
            this.textCertiReqDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textCertiReqDate.DataField = "CertiReqDate";
            this.textCertiReqDate.Height = 0.2F;
            this.textCertiReqDate.Left = 6.070001F;
            this.textCertiReqDate.Name = "textCertiReqDate";
            this.textCertiReqDate.Style = "font-size: 9pt; text-align: center; vertical-align: middle";
            this.textCertiReqDate.Text = null;
            this.textCertiReqDate.Top = 0F;
            this.textCertiReqDate.Width = 1.155F;
            // 
            // label7
            // 
            this.label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Height = 0.2F;
            this.label7.HyperLink = null;
            this.label7.Left = 0.04000008F;
            this.label7.Name = "label7";
            this.label7.Style = "font-weight: bold; text-align: center";
            this.label7.Text = "문서번호";
            this.label7.Top = 0F;
            this.label7.Width = 1.096F;
            // 
            // label2
            // 
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.04000008F;
            this.label2.Name = "label2";
            this.label2.Style = "font-weight: bold; text-align: center";
            this.label2.Text = "보고자";
            this.label2.Top = 0.2F;
            this.label2.Width = 1.096F;
            // 
            // textBox3
            // 
            this.textBox3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Height = 0.2F;
            this.textBox3.Left = 6.070001F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-size: 9pt; text-align: center; vertical-align: middle";
            this.textBox3.Text = "작성 후 1년";
            this.textBox3.Top = 0.2F;
            this.textBox3.Width = 1.155F;
            // 
            // label1
            // 
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 4.974001F;
            this.label1.Name = "label1";
            this.label1.Style = "font-weight: bold; text-align: center";
            this.label1.Text = "보고일자";
            this.label1.Top = 0F;
            this.label1.Width = 1.096F;
            // 
            // textSubject
            // 
            this.textSubject.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSubject.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSubject.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSubject.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSubject.DataField = "Subject";
            this.textSubject.Height = 0.2F;
            this.textSubject.Left = 1.136F;
            this.textSubject.Name = "textSubject";
            this.textSubject.Style = "font-size: 9pt; vertical-align: middle";
            this.textSubject.Text = null;
            this.textSubject.Top = 0.4F;
            this.textSubject.Width = 6.089F;
            // 
            // label6
            // 
            this.label6.Height = 0.2F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.04000044F;
            this.label6.Name = "label6";
            this.label6.Style = "";
            this.label6.Text = "2. SET-UP 설비";
            this.label6.Top = 1.241F;
            this.label6.Width = 1.096F;
            // 
            // label8
            // 
            this.label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Height = 0.2F;
            this.label8.HyperLink = null;
            this.label8.Left = 0.04000044F;
            this.label8.Name = "label8";
            this.label8.Style = "font-weight: bold; text-align: center";
            this.label8.Text = "MODEL";
            this.label8.Top = 1.461F;
            this.label8.Width = 1.3F;
            // 
            // label9
            // 
            this.label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label9.Height = 0.2F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.34F;
            this.label9.Name = "label9";
            this.label9.Style = "font-weight: bold; text-align: center";
            this.label9.Text = "MAKER";
            this.label9.Top = 1.461F;
            this.label9.Width = 1.35F;
            // 
            // label10
            // 
            this.label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Height = 0.2F;
            this.label10.HyperLink = null;
            this.label10.Left = 2.69F;
            this.label10.Name = "label10";
            this.label10.Style = "font-weight: bold; text-align: center";
            this.label10.Text = "S/N";
            this.label10.Top = 1.461F;
            this.label10.Width = 1.429F;
            // 
            // label11
            // 
            this.label11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label11.Height = 0.2F;
            this.label11.HyperLink = null;
            this.label11.Left = 4.119F;
            this.label11.Name = "label11";
            this.label11.Style = "font-weight: bold; text-align: center";
            this.label11.Text = "적용 PKG";
            this.label11.Top = 1.461F;
            this.label11.Width = 1.637001F;
            // 
            // label12
            // 
            this.label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label12.Height = 0.2F;
            this.label12.HyperLink = null;
            this.label12.Left = 5.756001F;
            this.label12.Name = "label12";
            this.label12.Style = "font-weight: bold; text-align: center";
            this.label12.Text = "비고";
            this.label12.Top = 1.461F;
            this.label12.Width = 1.469F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // textBox10
            // 
            this.textBox10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Height = 0.218F;
            this.textBox10.Left = 6.302001F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "font-size: 9pt";
            this.textBox10.Text = null;
            this.textBox10.Top = 0.978F;
            this.textBox10.Width = 0.7630005F;
            // 
            // textBox7
            // 
            this.textBox7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Height = 0.573F;
            this.textBox7.Left = 6.302001F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-size: 9pt";
            this.textBox7.Text = null;
            this.textBox7.Top = 0.4050001F;
            this.textBox7.Width = 0.7630007F;
            // 
            // textBox4
            // 
            this.textBox4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Height = 0.573F;
            this.textBox4.Left = 5.539F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-size: 9pt";
            this.textBox4.Text = null;
            this.textBox4.Top = 0.4050001F;
            this.textBox4.Width = 0.7630007F;
            // 
            // textBox6
            // 
            this.textBox6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Height = 0.218F;
            this.textBox6.Left = 6.302001F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.textBox6.Text = "APPROVAL";
            this.textBox6.Top = 0.187F;
            this.textBox6.Width = 0.7630007F;
            // 
            // textBox5
            // 
            this.textBox5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Height = 0.218F;
            this.textBox5.Left = 5.539F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.textBox5.Text = "CHECKED";
            this.textBox5.Top = 0.187F;
            this.textBox5.Width = 0.7630007F;
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Height = 0.573F;
            this.textBox1.Left = 4.776F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-size: 9pt";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.405F;
            this.textBox1.Width = 0.7630007F;
            // 
            // txtDRAFT
            // 
            this.txtDRAFT.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDRAFT.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDRAFT.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDRAFT.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDRAFT.Height = 0.218F;
            this.txtDRAFT.Left = 4.776F;
            this.txtDRAFT.Name = "txtDRAFT";
            this.txtDRAFT.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.txtDRAFT.Text = "DRAFT";
            this.txtDRAFT.Top = 0.1869999F;
            this.txtDRAFT.Width = 0.7630007F;
            // 
            // textBox8
            // 
            this.textBox8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Height = 0.218F;
            this.textBox8.Left = 5.539F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "font-size: 9pt";
            this.textBox8.Text = null;
            this.textBox8.Top = 0.978F;
            this.textBox8.Width = 0.7630007F;
            // 
            // textBox9
            // 
            this.textBox9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Height = 0.218F;
            this.textBox9.Left = 4.776F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-size: 9pt";
            this.textBox9.Text = null;
            this.textBox9.Top = 0.978F;
            this.textBox9.Width = 0.7630007F;
            // 
            // rptEQUZ0010_Main
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.3F;
            this.Sections.Add(this.rptHeader);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.rptFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCertiReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCertiReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSubject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRAFT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.ReportHeader rptHeader;
        private DataDynamics.ActiveReports.ReportFooter rptFooter;
        private DataDynamics.ActiveReports.Label label5;
        private DataDynamics.ActiveReports.TextBox textObject;
        private DataDynamics.ActiveReports.TextBox textPlantCode;
        private DataDynamics.ActiveReports.SubReport subReportState;
        private DataDynamics.ActiveReports.SubReport subReportJobPK;
        private DataDynamics.ActiveReports.SubReport subReportCheck;
        private DataDynamics.ActiveReports.Label labelTitle;
        private DataDynamics.ActiveReports.CrossSectionLine crossSectionLine1;
        private DataDynamics.ActiveReports.Line line1;
        private DataDynamics.ActiveReports.GroupHeader groupHeader;
        private DataDynamics.ActiveReports.GroupFooter groupFooter1;
        private DataDynamics.ActiveReports.Line line3;
        private DataDynamics.ActiveReports.CrossSectionLine crossSectionLine2;
        private DataDynamics.ActiveReports.SubReport subReportEquip;
        private DataDynamics.ActiveReports.Label label4;
        private DataDynamics.ActiveReports.Label label3;
        private DataDynamics.ActiveReports.TextBox textDocCode;
        private DataDynamics.ActiveReports.TextBox textCertiReqID;
        private DataDynamics.ActiveReports.TextBox textCertiReqDate;
        private DataDynamics.ActiveReports.Label label7;
        private DataDynamics.ActiveReports.Label label2;
        private DataDynamics.ActiveReports.TextBox textBox3;
        private DataDynamics.ActiveReports.Label label1;
        private DataDynamics.ActiveReports.TextBox textSubject;
        private DataDynamics.ActiveReports.Label label6;
        private DataDynamics.ActiveReports.Label label8;
        private DataDynamics.ActiveReports.Label label9;
        private DataDynamics.ActiveReports.Label label10;
        private DataDynamics.ActiveReports.Label label11;
        private DataDynamics.ActiveReports.Label label12;
        private DataDynamics.ActiveReports.TextBox textBox5;
        private DataDynamics.ActiveReports.TextBox textBox6;
        private DataDynamics.ActiveReports.TextBox textBox4;
        private DataDynamics.ActiveReports.TextBox textBox7;
        private DataDynamics.ActiveReports.TextBox textBox10;
        private DataDynamics.ActiveReports.TextBox textBox1;
        private DataDynamics.ActiveReports.TextBox txtDRAFT;
        private DataDynamics.ActiveReports.TextBox textBox8;
        private DataDynamics.ActiveReports.TextBox textBox9;
    }
}
