﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmEQUZ0005.cs                                        */
/* 프로그램명   : 설비폐기등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;


namespace QRPEQU.UI
{
    public partial class frmEQUZ0005 : Form,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //수정여부를위한 전역변수
        string strEdit = "";


        public frmEQUZ0005()
        {
            InitializeComponent();
        }

        private void frmEQUZ0005_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //검색툴
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0005_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정 
            titleArea.mfSetLabelText("설비폐기등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            //컨트롤초기화
            InitLable();
            InitGrid();
            InitGroupbox();
            IninText();
            InitCombo();
            InitButton();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);


                wCom.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {

            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinButton wBtn = new WinButton();
                wBtn.mfSetButton(this.uButtonDownDocFile, "기존양식다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
                wBtn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wBtn.mfSetButton(this.uButtonDownLoad, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLable()
        {
            //Syste ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSuperSys, "Super설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLocation, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGroupName, "설비그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSTSStock, "STS입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelYear, "제작년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGrade, "설비등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelDiscardDate, "폐기일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDiscardUser, "폐기담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelPurchasePrice, "설비도입가", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRemainPrice, "설비잔존가", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelReason, "폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFile, "첨부문서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트
        /// </summary>
        private void IninText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextDiscardUserID.Text = m_resSys.GetString("SYS_USERID");
                uTextDiscardUserName.Text = m_resSys.GetString("SYS_USERNAME");
                //this.uTextEquipCode.Tag = null;

                if (this.uCheckGWResultState.Checked.Equals(true))
                    this.uCheckGWResultState.Checked = false;

                if (this.uCheckGWTFlag.Checked.Equals(true))
                    this.uCheckGWTFlag.Checked = false;

                if (!this.uNumPurchasePrice.Value.ToString().Equals("0"))
                    this.uNumPurchasePrice.Value = 0;

                if (!this.uNumRemainPrice.Value.ToString().Equals("0"))
                    this.uNumRemainPrice.Value = 0;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region 설비정보
                //설비정보 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //설비정보 그리드 컬럼설정
                grd.mfSetGridColumn(this.uGridEquip, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "","","");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "DiscardDate", "폐기일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GWTFlagDiscard", "그룹웨어전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GWTFlagDiscardName", "그룹웨어전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GWTDateDiscard", "그룹웨어전송일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GWResultStateDiscard", "그룹웨어승인상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GWResultStateDiscardName", "그룹웨어승인상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GWResultDateDiscard", "그룹웨어승인일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "MDMIFDiscardFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "MDMIFDiscardFlagName", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                #endregion

                #region 설비폐기첨부파일
                
                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipDiscardFile, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipDiscardFile, 0, "Check", "", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false,0,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipDiscardFile, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 350, true, true, 10,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridEquipDiscardFile, 0, "Subject", "문서제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 350, true, false, 1000,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                grd.mfSetGridColumn(this.uGridEquipDiscardFile, 0, "FileName", "파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 400, true, false, 1000,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");
                
                grd.mfSetGridColumn(this.uGridEquipDiscardFile, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 500, false, false, 1000,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion
                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridEquipDiscardFile.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridEquipDiscardFile.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //this.uGrid1.DisplayLayout.Bands[0].Columns["Check"].DataType = typeof(Boolean);
                grd.mfAddRowGrid(this.uGridEquipDiscardFile, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupbox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();
                
                
                grpbox.mfSetGroupBox(this.uGroupBox, GroupBoxType.INFO, "설비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                
                //폰트설정
                uGroupBox.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox.HeaderAppearance.FontData.SizeInPoints = 9;
                
                grpbox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "설비폐기등록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                
                //폰트설정
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 툴바
        
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipDiscardFile), "EquipDiscardFile");
                QRPEQU.BL.EQUCCS.EquipDiscardFile clsEquipDiscardFile = new QRPEQU.BL.EQUCCS.EquipDiscardFile();
                brwChannel.mfCredentials(clsEquipDiscardFile);

                //설비폐기헤더정보조회매서드 호출
                DataTable dtEquip = clsEquipDiscardFile.mfReadEquipDiscardH(strPlantCode,m_resSys.GetString("SYS_LANG"));
                
                //그리드에바인드
                this.uGridEquip.DataSource = dtEquip;
                this.uGridEquip.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtEquip.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquip, 0);

                    for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                    {
                        //그룹웨어승인여부가 승인상태고 MDM전송여부가 F 인경우 줄에 색을 준다.
                        if (this.uGridEquip.Rows[i].Cells["GWResultStateDiscard"].Value.ToString().Equals("T") &&
                            this.uGridEquip.Rows[i].Cells["MDMIFDiscardFlag"].Value.ToString().Equals("F"))
                        {
                            this.uGridEquip.Rows[i].Appearance.BackColor = Color.Salmon;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                System.Windows.Forms.DialogResult result;
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //// 결재선 팝업창
                QRPCOM.UI.frmCOM0013 frm = new QRPCOM.UI.frmCOM0013();
                DialogResult _dr = new DialogResult();

                #region 필수입력확인

                if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    //공장콤보DropDown
                    this.uComboPlant.DropDown();
                    return;

                }
                if (this.uTextEquipCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000699", Infragistics.Win.HAlign.Right);

                    // Focus.
                    this.uTextEquipCode.Focus();
                    return;
                }

                if (this.uCheckGWResultState.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000237", "M000236", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uCheckGWTFlag.Checked && !uCheckGWTFlag.Enabled)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000239", "M000238", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (uDateDiscardTime.Value == null)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M000239", "M001195", Infragistics.Win.HAlign.Right);

                    // Focus.
                    this.uDateDiscardTime.DropDown();
                    return;
                }

                if (this.uTextDiscardUserID.Text == "" || this.uTextDiscardUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001194", Infragistics.Win.HAlign.Right);
                    this.uTextDiscardUserID.Focus();
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                #endregion

                #region 설비폐기등록 헤더저장
                // -----  설비폐기등록 헤더부분 저장 ----//
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;
                DataTable dtDiscardH = new DataTable();

                dtDiscardH.Columns.Add("PlantCode", typeof(string));
                dtDiscardH.Columns.Add("EquipCode", typeof(string));
                dtDiscardH.Columns.Add("DiscardDate", typeof(string));
                dtDiscardH.Columns.Add("DiscardChargeID", typeof(string));
                dtDiscardH.Columns.Add("DiscardReason", typeof(string));
                dtDiscardH.Columns.Add("PurchasePrice", typeof(string));
                dtDiscardH.Columns.Add("RemainPrice", typeof(string));

                dtDiscardH.Columns.Add("StationName", typeof(string));
                dtDiscardH.Columns.Add("GRDate", typeof(string));
                dtDiscardH.Columns.Add("SerialNo", typeof(string));

                DataRow drH;
                drH = dtDiscardH.NewRow();
                drH["PlantCode"] = strPlantCode;
                drH["EquipCode"] = strEquipCode;
                drH["DiscardDate"] = Convert.ToDateTime(this.uDateDiscardTime.Value).ToString("yyyy-MM-dd");
                drH["DiscardChargeID"] = this.uTextDiscardUserID.Text;
                drH["DiscardReason"] = this.uTextDiscardReason.Text;
                if (this.uNumPurchasePrice.Value.Equals(double.NaN))
                {
                    drH["PurchasePrice"] = 0;
                }
                else
                {
                    drH["PurchasePrice"] = this.uNumPurchasePrice.Value;
                }
                if (this.uNumRemainPrice.Value.Equals(double.NaN))
                {
                    drH["RemainPrice"] = 0;
                }
                else
                {
                    drH["RemainPrice"] = this.uNumRemainPrice.Value;
                }
                drH["StationName"] = uTextStation.Text;
                drH["GRDate"] = uTextSTSStock.Text;
                drH["SErialNo"] = uTextSerialNo.Text;

                dtDiscardH.Rows.Add(drH);

                #endregion

                #region 설비폐기등록첨부파일저장
                //----- 설비폐기등록 첨부파일 저장 시 필요한 컬럼 셋 -----//
                DataTable dtDiscardD = new DataTable();

                dtDiscardD.Columns.Add("Seq", typeof(string));
                dtDiscardD.Columns.Add("Subject", typeof(string));
                dtDiscardD.Columns.Add("FileName", typeof(string));
                dtDiscardD.Columns.Add("EtcDesc", typeof(string));

                // 그리드 편집여부
                string strChk = "";

                string strUpLoadFile = "";
                //파일이름 저장할 변수
                ArrayList arrFil = new ArrayList();

                //----- 첨부파일이 있으면 그리드안의 정보를 저장한다 -----//
                if (this.uGridEquipDiscardFile.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridEquipDiscardFile.ActiveCell = this.uGridEquipDiscardFile.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridEquipDiscardFile.Rows.Count; i++)
                    {

                        // 편집이미지가 있는 줄중에서 Hidden이 false 인 것을 찾는다.
                        if (this.uGridEquipDiscardFile.Rows[i].Hidden == false)
                        {
                            if (this.uGridEquipDiscardFile.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                strChk = "Image";
                            }

                            string strRowNum = this.uGridEquipDiscardFile.Rows[i].RowSelectorNumber.ToString();

                            //---------------그리드 안의 필수 입력 사항 체크 ----------------//
                            if (this.uGridEquipDiscardFile.Rows[i].Cells["Subject"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001230", strRowNum + "M000502", Infragistics.Win.HAlign.Right);


                                this.uGridEquipDiscardFile.ActiveCell = this.uGridEquipDiscardFile.Rows[i].Cells["Subject"];
                                this.uGridEquipDiscardFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else if (this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M001230", strRowNum + "M000535", Infragistics.Win.HAlign.Right);

                                this.uGridEquipDiscardFile.ActiveCell = this.uGridEquipDiscardFile.Rows[i].Cells["FileName"];
                                this.uGridEquipDiscardFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            #region 파일저장
                            // 첨부파일이 수정되거나 신규 를 확인한다 .
                            if (this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo filDoc = new FileInfo(this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString());


                                strUpLoadFile = filDoc.Name;

                                // 파일이름변경하여 복사하기
                                string strFileName = filDoc.DirectoryName + "\\" + strEquipCode + "-" +
                                                      this.uGridEquipDiscardFile.Rows[i].RowSelectorNumber + "-" + filDoc.Name;

                                //변경한 화일이 있으면 삭제하기
                                if (File.Exists(strFileName))
                                    File.Delete(strFileName);

                                //변경된파일이름으로 복사하기
                                File.Copy(this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString(), strFileName);
                                arrFil.Add(strFileName);

                                // 첨부파일 정보를 저장한다 /
                                DataRow drFile;
                                drFile = dtDiscardD.NewRow();
                                drFile["Seq"] = this.uGridEquipDiscardFile.Rows[i].RowSelectorNumber;
                                drFile["Subject"] = this.uGridEquipDiscardFile.Rows[i].Cells["Subject"].Value.ToString();
                                drFile["FileName"] = strEquipCode + "-" + this.uGridEquipDiscardFile.Rows[i].RowSelectorNumber + "-" + filDoc.Name;
                                drFile["EtcDesc"] = this.uGridEquipDiscardFile.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtDiscardD.Rows.Add(drFile);



                            }
                            // 경로가 없으면 바로 저장.
                            else
                            {
                                // 첨부파일 정보를 저장한다 /
                                DataRow drFile;
                                drFile = dtDiscardD.NewRow();
                                drFile["Seq"] = this.uGridEquipDiscardFile.Rows[i].RowSelectorNumber;
                                drFile["Subject"] = this.uGridEquipDiscardFile.Rows[i].Cells["Subject"].Value.ToString();
                                drFile["FileName"] = this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString();
                                drFile["EtcDesc"] = this.uGridEquipDiscardFile.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtDiscardD.Rows.Add(drFile);
                            }
                            #endregion
                        }

                    }

                }

                #endregion

                //// ---------- 저장 여부 메세지 박스를 띄운다 . ------------

                DialogResult dResult = new DialogResult();

                if (uCheckGWTFlag.Checked)
                {
                    dResult = msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001264", "M001053", "M000234",
                                                            Infragistics.Win.HAlign.Right);
                }
                else
                {
                    dResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001264", "M001053", "M000936",
                                                            Infragistics.Win.HAlign.Right);
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                if (uCheckGWTFlag.Checked && (dResult == DialogResult.Yes || dResult == DialogResult.No) ||
                    !uCheckGWTFlag.Checked && (dResult == DialogResult.Yes))
                {
                    #region 결재선 팝업창 호출
                    if (uCheckGWTFlag.Checked && dResult == DialogResult.Yes)
                    {
                        ////결재선 팝업창 호출
                        frm.WriteID = uTextDiscardUserID.Text;
                        frm.WriteName = uTextDiscardUserName.Text;

                        _dr = frm.ShowDialog();
                        if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "M001053", "M001030", Infragistics.Win.HAlign.Center);

                            return;
                        }
                    }
                    #endregion

                    #region BL
                    //팝업창을 띄운다
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    //-커서를 변경한다.
                    this.MdiParent.Cursor = Cursors.WaitCursor;


                    //설비폐기등록 BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipDiscardFile), "EquipDiscardFile");
                    QRPEQU.BL.EQUCCS.EquipDiscardFile clsEquipDiscardFile = new QRPEQU.BL.EQUCCS.EquipDiscardFile();
                    brwChannel.mfCredentials(clsEquipDiscardFile);

                    //설비폐기등록 저장매서드 호출
                    string strErrRtn = clsEquipDiscardFile.mfSaveEquipDiscardFile(dtDiscardH, dtDiscardD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //파일서버 연결정보가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAcce);
                    DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비폐기등록 첨부파일 저장경로 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);

                    DataTable dtSysFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0004");
                    #endregion

                    // 처리결과에 따라서 메세지박스를 띄운다.

                    if (ErrRtn.ErrNum == 0)
                    {
                        bool FileUploadCheck = false;
                        #region File
                        // 첨부파일이 신규거나 수정이 되었으면 파일을 업로드시킨다.
                        if (strUpLoadFile != "")
                        {
                            //Upload정보 설정
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            fileAtt.mfInitSetSystemFileInfo(arrFil, "", dtSysAcce.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtSysFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtSysFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAcce.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAcce.Rows[0]["AccessPassword"].ToString());
                            //fileAtt.TopMost = true;
                            //fileAtt.ShowDialog();
                            FileUploadCheck = fileAtt.mfFileUpload_NonAssync();
                        }
                        #endregion

                        if (!FileUploadCheck)
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "M001135", "M001037", "M001186",
                                                            Infragistics.Win.HAlign.Right);
                            return;
                        }

                        #region Brains ( 그룹웨어 인터페이스 )
                        if (uCheckGWTFlag.Checked && dResult == DialogResult.Yes)
                        {
                            // 테스트 코드 -- 업로드시 주석 처리
                            ////frm.dtSendLine.Rows[0]["CD_USERKEY"] = "tica100";
                            ////frm.dtSendLine.Rows[1]["CD_USERKEY"] = "tica100";


                            DataTable dtRtn = clsEquipDiscardFile.mfSaveEquipDiscardGRWApproval(dtDiscardH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                            //DataTable dtRtn = mfSaveEquipDiscardGRWApproval(dtDiscardH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                            if (dtRtn == null) // 그룹웨어 실패
                            {
                                this.MdiParent.Cursor = Cursors.Default;
                                //팝업창을 닫는다.
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000327",
                                                       Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else if (dtRtn.Rows.Count.Equals(0))
                            {
                                this.MdiParent.Cursor = Cursors.Default;
                                //팝업창을 닫는다.
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000327",
                                                       Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else if (!dtRtn.Rows[0]["CD_CODE"].ToString().Equals("00") || !Convert.ToBoolean(dtRtn.Rows[0]["CD_STATUS"].ToString())) // 그룹웨어 실패
                            {
                                this.MdiParent.Cursor = Cursors.Default;
                                //팝업창을 닫는다.
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), dtRtn.Rows[0]["MSG"].ToString(),
                                                       Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }

                        #endregion

                        this.MdiParent.Cursor = Cursors.Default;
                        //팝업창을 닫는다.
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M001037", "M000930",
                                                        Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                    else
                    {
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                        return;
                    }

                }
                else
                {
                    if (strEdit != "")
                    {
                        strEdit = "";
                    }
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001053", "M001030", Infragistics.Win.HAlign.Center);

                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 테스트용 사용금지
        //////////public DataTable mfSaveEquipDiscardGRWApproval(DataTable dtHeader, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        //////////{
        //////////    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
        //////////    brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
        //////////    QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        //////////    brwChannel.mfCredentials(grw);


        //////////    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipDiscardFile), "EquipDiscardFile");
        //////////    QRPEQU.BL.EQUCCS.EquipDiscardFile clsEquipDiscardFile = new QRPEQU.BL.EQUCCS.EquipDiscardFile();
        //////////    brwChannel.mfCredentials(clsEquipDiscardFile);

        //////////    DataTable dtRtn = new DataTable();
            
        //////////    DataTable dtFileInfo = grw.mfSetFileInfoDataTable();

        //////////    try
        //////////    {
        //////////        string strPlantCode = dtHeader.Rows[0]["PlantCode"].ToString();
        //////////        string strEquipCode = dtHeader.Rows[0]["EquipCode"].ToString();
        //////////        string strLegacyKey = strPlantCode + "||" + strEquipCode;

        //////////        string strStationName = dtHeader.Rows[0]["StationName"].ToString();
        //////////        string strGRDate = dtHeader.Rows[0]["GRDate"].ToString();
        //////////        string strSerialNo = dtHeader.Rows[0]["SerialNo"].ToString();
        //////////        string strPurchasePrice = dtHeader.Rows[0]["PurchasePrice"].ToString();
        //////////        string strRemainPrice = dtHeader.Rows[0]["RemainPrice"].ToString();
        //////////        string strReason = dtHeader.Rows[0]["DiscardReason"].ToString();

        //////////        string strFilePath = "EQUEquipDiscardFile\\";

        //////////        DataTable dtfile = clsEquipDiscardFile.mfReadEquipDiscardFile_File(strPlantCode, strEquipCode);

        //////////        foreach (DataRow dr in dtfile.Rows)
        //////////        {
        //////////            DataRow _dr = dtFileInfo.NewRow();
        //////////            _dr["PlantCode"] = strPlantCode;
        //////////            _dr["NM_FILE"] = dr["FileName"].ToString();
        //////////            _dr["NM_LOCATION"] = strFilePath;

        //////////            dtFileInfo.Rows.Add(_dr);
        //////////        }

        //////////        dtRtn = grw.EquipDiscard(strLegacyKey, dtSendLine, dtCcLine, strStationName, strGRDate, strEquipCode, strSerialNo,
        //////////            strPurchasePrice, strRemainPrice, strReason, dtFormInfo, dtFileInfo, strUserIP, strUserID);
        //////////    }
        //////////    catch (Exception ex)
        //////////    {
        //////////        return dtRtn;
        //////////        throw (ex);
        //////////    }

        //////////    return dtRtn;
        //////////}
        #endregion

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            //try
            //{
            //    //System ResourceInfo
            //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

            //    //------------------- 필수 입력 확인 --------------------//
            //    if(this.uGroupBoxContentsArea.Expanded.Equals(false))
            //    {
            //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                  "확인창", "입력사항확인", "취소할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
            //        return;
            //    }

            //    //if(this.uTextEquipCode.Tag != null && this.uTextEquipCode.Tag.Equals("T"))
            //    //{
            //    //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //    //              "확인창", "승인상태확인", "취소할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
            //    //    return;
            //    //}

            //    if (this.uComboPlant.Value.ToString() == "")
            //    {
            //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                  "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

            //        //공장콤보DropDown
            //        this.uComboPlant.DropDown();
            //        return;

            //    }
            //    else if (this.uTextEquipCode.Text == "")
            //    {
            //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                  "확인창", "필수입력사항확인", "설비를 선택해주세요.", Infragistics.Win.HAlign.Right);

            //        // Focus.
            //        this.uTextEquipCode.Focus();
            //        return;
            //    }

            //    //-- 삭제에 필요한 값 저장 //
            //    string strPlantCode = this.uComboPlant.Value.ToString();
            //    string strEquipCode = this.uTextEquipCode.Text;


            //    // ---  폐기취소 메세지 박스를 띄운다 . ---//
            //    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                            "확인창", "폐기취소확인", "선택한 정보를 폐기취소 하시겠습니까?",
            //                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
            //    {
            //        //  팝업창을 띄운다.
            //        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            //        Thread t1 = m_ProgressPopup.mfStartThread();
            //        m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");

            //        // 커서를 변경한다.
            //        this.MdiParent.Cursor = Cursors.WaitCursor;

            //        //설비폐기등록 BL을 호출한다
            //        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
            //        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipDiscardFile), "EquipDiscardFile");
            //        QRPEQU.BL.EQUCCS.EquipDiscardFile clsEquipDiscardFile = new QRPEQU.BL.EQUCCS.EquipDiscardFile();
            //        brwChannel.mfCredentials(clsEquipDiscardFile);

            //        string strErrRtn = clsEquipDiscardFile.mfDeleteEquipDiscardFile(strPlantCode, strEquipCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
            //        TransErrRtn ErrRtn = new TransErrRtn();

            //        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
            //        /////////////

            //        this.MdiParent.Cursor = Cursors.Default;
            //        m_ProgressPopup.mfCloseProgressPopup(this);
            //        System.Windows.Forms.DialogResult result;
            //        if (ErrRtn.ErrNum == 0)
            //        {
            //            result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                                         "처리결과", "폐기취소처리결과", "선택한 정보를 성공적으로 폐기취소했습니다.",
            //                                        Infragistics.Win.HAlign.Right);
            //            mfCreate();
            //        }
            //        else
            //        {
            //            result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                                         "처리결과", "폐기취소처리결과", "선택한 정보를 폐기취소하지 못했습니다.",
            //                                         Infragistics.Win.HAlign.Right);
            //        }
            //    }

            //}
            //catch (Exception ex)
            //{
            //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            //    frmErr.ShowDialog();
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                //ExpandGroupBox가 활성화상태가 아니면 활성화시킨다.
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }


                #region 텍스트 클리어
                this.uTextArea.Clear();
                this.uTextDiscardReason.Clear();
                this.uTextEquipCode.Clear();
                this.uTextEquipGroupName.Clear();
                this.uTextEquipLevel.Clear();
                this.uTextEquipLoc.Clear();
                this.uTextEquipProcess.Clear();
                this.uTextEquipType.Clear();
                this.uTextMakerYear.Clear();
                this.uTextModel.Clear();
                this.uTextSerialNo.Clear();
                this.uTextStation.Clear();
                this.uTextSTSStock.Clear();
                this.uTextSuperEquip.Clear();
                this.uTextSysName.Clear();
                this.uTextVendor.Clear();
                this.uNumPurchasePrice.Value = 0;
                this.uNumRemainPrice.Value = 0;
                this.uDateDiscardTime.Value = DateTime.Now;

                this.uCheckGWResultState.Checked = false;
                this.uCheckGWTFlag.Checked = false;
                this.uCheckGWTFlag.Enabled = true;
                #endregion

                if (this.uTextEquipCode.ReadOnly.Equals(true))
                {
                    this.uComboPlant.ReadOnly = false;
                    this.uTextEquipCode.ReadOnly = false;
                    this.uDateDiscardTime.ReadOnly = false;
                    this.uTextDiscardUserID.ReadOnly = false;
                    this.uNumPurchasePrice.ReadOnly = false;
                    this.uNumRemainPrice.ReadOnly = false;
                    this.uTextDiscardReason.ReadOnly = false;
                }

                //편집시 새로운 생생성 여부
                if (this.uGridEquipDiscardFile.DisplayLayout.Override.AllowAddNew == Infragistics.Win.UltraWinGrid.AllowAddNew.No)
                {
                    this.uGridEquipDiscardFile.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                }

                if (this.uGridEquipDiscardFile.Rows.Count > 0)
                {
                    //-- 전체 행을 선택 하여 삭제한다 --//
                    this.uGridEquipDiscardFile.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipDiscardFile.Rows.All);
                    this.uGridEquipDiscardFile.DeleteSelectedRows(false);
                }

                this.uCheckMDM.Visible = false;

                if (strEdit != "")
                {
                    strEdit = "";
                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        public void mfPrint()
        {
        }
       
        public void mfExcel()
        {

        }
        #endregion

        #region 이벤트

        //기존양식이 있는 안에ㅇ[ 있는 
        private void frmEQUZ0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 140);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquip.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquip.Height = 740;

                    for (int i = 0; i < uGridEquip.Rows.Count; i++)
                    {
                        uGridEquip.Rows[i].Fixed = false;
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        #region 콤보이벤트
        //공장선택전 수정여부가 있을경우 저장여부를 묻는다.
        private void uComboPlant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (strEdit != "" && this.uComboPlant.ReadOnly.Equals(false))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    DialogResult drResult = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000757", Infragistics.Win.HAlign.Right);
                    if(drResult == DialogResult.Yes)
                    {
                        mfSave();
                        
                    }
                    else if (drResult == DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                    else
                    {

                        mfCreate();
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            if (strEdit == "")
            { mfCreate(); }
        }
        #endregion

        #region 버튼이벤트
        //클릭하면 파일이 다운로드된다.
        private void uButtonFileDownload_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridEquipDiscardFile.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridEquipDiscardFile.Rows[i].Hidden == false && this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipDiscardFile.Rows[i].Cells["Check"].Value) == true)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboPlant.Value.ToString();

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비폐기 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0004");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridEquipDiscardFile.Rows.Count; i++)
                    {
                        if (this.uGridEquipDiscardFile.Rows[i].Hidden == false && this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipDiscardFile.Rows[i].Cells["Check"].Value) == true)
                            arrFile.Add(this.uGridEquipDiscardFile.Rows[i].Cells["FileName"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //-- 행삭제 이벤트 --//
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridEquipDiscardFile.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquipDiscardFile.Rows[i].Cells["Check"].Value) == true
                        && this.uGridEquipDiscardFile.Rows[i].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                        this.uGridEquipDiscardFile.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        private void uButtonDownDocFile_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboPlant.Value.ToString();

                //기존양식 설정된 후 주석추리
                if (!strPlantCode.Equals(string.Empty))
                    return;


                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000271", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비폐기 기존양식 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0024");

                    
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    //기존양식 파일 명입력
                    arrFile.Add("설비폐기.xlsx");
                    

                    //Dwonload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region GridEvent

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridEquipDiscardFile_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridEquipDiscardFile, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }

        private void uGridEquipDiscardFile_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            if (strEdit == "")
            { strEdit = "T"; }
        }

        //선택한 줄의 상세정보를 출력한다.
        private void uGridEquip_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                //선택줄 고정
                e.Row.Fixed = true;

                string strPlantCode = e.Row.GetCellValue("PlantCode").ToString();
                string strEquipCode = e.Row.GetCellValue("EquipCode").ToString();

                if (!this.uComboPlant.Value.ToString().Equals(strPlantCode))
                    this.uComboPlant.Value = strPlantCode;

                //승인상태저장
                string strGWResult = e.Row.GetCellValue("GWResultStateDiscard").ToString();
                //this.uTextEquipCode.Tag = strGWResult;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                //설비정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);
                //설비정보 조회 매서드호출
                DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //정보가 있는경우
                if (dtEquip.Rows.Count > 0)
                {
                    this.uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();             //설비코드
                    this.uTextSysName.Text = dtEquip.Rows[0]["EquipName"].ToString();             //설비명
                    this.uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();       //Super설비
                    this.uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();             //Station
                    this.uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();                   //Area
                    this.uTextEquipProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString(); //공정구분
                    this.uTextEquipLoc.Text = dtEquip.Rows[0]["EquipLocName"].ToString();           //위치
                    this.uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();                 //모델
                    this.uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();         //설비유형
                    this.uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();               //SerialNo
                    this.uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();               //거래처
                    this.uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();   //그룹명
                    this.uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();                 //입고일
                    this.uTextMakerYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();              //제작년도
                    this.uTextEquipLevel.Text = dtEquip.Rows[0]["EquipLevelCode"].ToString();       //설비등급


                    //그룹웨어 전송여부
                    if (dtEquip.Rows[0]["GWTFlagDiscard"].ToString().Equals("T"))
                    {
                        if (dtEquip.Rows[0]["GWResultStateDiscard"].ToString().Equals("R"))
                        {
                            uCheckGWTFlag.Enabled = true;
                            this.uCheckGWTFlag.Checked = false;
                        }
                        else
                        {
                            uCheckGWTFlag.Enabled = false;
                            this.uCheckGWTFlag.Checked = true;
                        }
                    }
                    else
                    {
                        uCheckGWTFlag.Enabled = true;
                        this.uCheckGWTFlag.Checked = false;
                    }

                    //그룹웨어승인여부
                    if (dtEquip.Rows[0]["GWResultStateDiscard"].ToString().Equals("T"))
                    {
                        this.uCheckGWResultState.Checked = true;

                        //MDM전송여부
                        if (!dtEquip.Rows[0]["MDMIFDiscardFlag"].ToString().Equals("T"))
                            this.uCheckMDM.Visible = true;
                        else
                            this.uCheckMDM.Visible = false;
                    }
                    else
                    {

                        this.uCheckGWResultState.Checked = false;
                        this.uCheckMDM.Visible = false;
                    }


                    ReadEquipInfo(strPlantCode, strEquipCode);
                    //ReadOnlyControl();
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);


                    if (this.uCheckMDM.Visible)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000086", "M000087", "M000088",
                                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            mfSaveMDM(strPlantCode, strEquipCode);

                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드 안에있는 첨부파일 셀의 버튼을 클릭하였을 때 파일을 선택할 수있는 윈도우 창이뜬다.
        private void uGridEquipDiscardFile_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!this.uCheckGWTFlag.Checked && this.uCheckGWTFlag.Enabled)
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        e.Cell.Value = strImageFile;

                        QRPGlobal grdImg = new QRPGlobal();
                        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }



        #endregion

        #region TextEvent

        // 설비 에디트 버튼을 클릭시 설비정보 팝업창을 띄운다.
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextEquipCode.ReadOnly.Equals(false))
                {
                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    // 공장콤보 값이 공백일 경우 메세지 박스를 띄운다.
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                        //공장 콤보DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    // 공장 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    frmPOP0005 frmEquip = new frmPOP0005();
                    //공장정보 보냄
                    frmEquip.PlantCode = strPlantCode;
                    frmEquip.ShowDialog();


                    // 설비코드 저장
                    string strEquipCode = frmEquip.EquipCode;

                    ReadEquipInfo(strPlantCode, strEquipCode);

                    // 팝업창에서 선택한 설비의 정보를 각 해당 컨트롤에 삽입한다.
                    this.uTextEquipCode.Text = frmEquip.EquipCode;
                    this.uTextArea.Text = frmEquip.AreaName;
                    this.uTextEquipGroupName.Text = frmEquip.EquipGroupName;
                    this.uTextEquipLevel.Text = frmEquip.EquipLevelCode;
                    this.uTextEquipLoc.Text = frmEquip.EquipLocName;
                    this.uTextEquipProcess.Text = frmEquip.EquipProcGubunName;
                    this.uTextEquipType.Text = frmEquip.EquipTypeName;
                    this.uTextMakerYear.Text = frmEquip.MakeYear;
                    this.uTextModel.Text = frmEquip.ModelName;
                    this.uTextSerialNo.Text = frmEquip.SerialNo;
                    this.uTextStation.Text = frmEquip.StationName;
                    this.uTextSTSStock.Text = frmEquip.GRDate;
                    this.uTextSuperEquip.Text = frmEquip.SuperEquipCode;
                    this.uTextSysName.Text = frmEquip.EquipName;
                    this.uTextVendor.Text = frmEquip.VendorName;

                    if (strEdit != "")
                    { strEdit = ""; }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //폐기담당자 에디트버튼을 클릭시 유저정보 팝업창을 띄운다
        private void uTextDiscardUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextDiscardUserID.ReadOnly.Equals(false))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    //-- 공장 이 공백일 경우 메세지박스를 띄운다. --//
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                        //공장 콤보DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }
                    //-- 공장정보 저장 --//
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    frmPOP0011 frmUser = new frmPOP0011();
                    //공장정보 보냄
                    frmUser.PlantCode = strPlantCode;
                    frmUser.ShowDialog();

                    //--각 해당컨트롤에 아이디 명 을 삽입시킨다.
                    this.uTextDiscardUserID.Text = frmUser.UserID;
                    this.uTextDiscardUserName.Text = frmUser.UserName;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uTextEquipCode.ReadOnly.Equals(false) && !this.uTextSysName.Text.Equals(string.Empty))
                {
                    this.uTextSysName.Text = "";      //설비명
                    this.uTextSuperEquip.Text = "";     //Super설비
                    this.uTextStation.Text = "";        //Station
                    this.uTextArea.Text = "";           //Area
                    this.uTextEquipProcess.Text = "";   //공정구분
                    this.uTextEquipLoc.Text = "";       //위치
                    this.uTextModel.Text = "";          //모델
                    this.uTextEquipType.Text = "";      //설비유형
                    this.uTextSerialNo.Text = "";       //SerialNo
                    this.uTextVendor.Text = "";         //거래처
                    this.uTextEquipGroupName.Text = ""; //그룹명
                    this.uTextSTSStock.Text = "";       //입고일
                    this.uTextMakerYear.Text = "";      //제작년도
                    this.uTextEquipLevel.Text = "";     //설비등급

                    IninText();

                    //그리드에 정보가 있을 경우 삭제
                    if (this.uGridEquipDiscardFile.Rows.Count > 0)
                    {
                        this.uGridEquipDiscardFile.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipDiscardFile.Rows.All);
                        this.uGridEquipDiscardFile.DeleteSelectedRows(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비코드입력 후 자동로드
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextEquipCode.ReadOnly.Equals(false))
                {
                    if (this.uTextEquipCode.ReadOnly.Equals(false) && (e.KeyData == Keys.Delete || e.KeyData == Keys.Back))
                    {
                        if (!this.uTextSysName.Text.Equals(string.Empty))
                        {
                            this.uTextSysName.Text = "";      //설비명
                            this.uTextSuperEquip.Text = "";     //Super설비
                            this.uTextStation.Text = "";        //Station
                            this.uTextArea.Text = "";           //Area
                            this.uTextEquipProcess.Text = "";   //공정구분
                            this.uTextEquipLoc.Text = "";       //위치
                            this.uTextModel.Text = "";          //모델
                            this.uTextEquipType.Text = "";      //설비유형
                            this.uTextSerialNo.Text = "";       //SerialNo
                            this.uTextVendor.Text = "";         //거래처
                            this.uTextEquipGroupName.Text = ""; //그룹명
                            this.uTextSTSStock.Text = "";       //입고일
                            this.uTextMakerYear.Text = "";      //제작년도
                            this.uTextEquipLevel.Text = "";     //설비등급

                            IninText();

                            //그리드에 정보가 있을 경우 삭제
                            if (this.uGridEquipDiscardFile.Rows.Count > 0)
                            {
                                this.uGridEquipDiscardFile.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipDiscardFile.Rows.All);
                                this.uGridEquipDiscardFile.DeleteSelectedRows(false);
                            }
                        }

                    }

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    //엔터키를 누를시
                    if (e.KeyData.Equals(Keys.Enter))
                    {
                        if (!this.uTextEquipCode.Text.Equals(string.Empty))
                        {
                            //공장정보 체크
                            if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                                //DropDown
                                this.uComboPlant.DropDown();
                                return;
                            }

                            //공장,설비 정보 저장
                            string strPlant = this.uComboPlant.Value.ToString();
                            string strEquip = this.uTextEquipCode.Text;

                            QRPBrowser brwChannel = new QRPBrowser();
                            //설비정보 BL 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                            QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                            brwChannel.mfCredentials(clsEquip);
                            //설비정보 조회 매서드호출
                            DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlant, strEquip, m_resSys.GetString("SYS_LANG"));

                            //정보가 있는경우
                            if (dtEquip.Rows.Count > 0)
                            {
                                this.uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();             //설비코드
                                this.uTextEquipCode.Tag = dtEquip.Rows[0]["GWResultStateDiscard"].ToString();   //그룹웨어승인상태
                                this.uTextSysName.Text = dtEquip.Rows[0]["EquipName"].ToString();             //설비명
                                this.uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();       //Super설비
                                this.uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();             //Station
                                this.uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();                   //Area
                                this.uTextEquipProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString(); //공정구분
                                this.uTextEquipLoc.Text = dtEquip.Rows[0]["EquipLocName"].ToString();           //위치
                                this.uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();                 //모델
                                this.uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();         //설비유형
                                this.uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();               //SerialNo
                                this.uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();               //거래처
                                this.uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();   //그룹명
                                this.uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();                 //입고일
                                this.uTextMakerYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();              //제작년도
                                this.uTextEquipLevel.Text = dtEquip.Rows[0]["EquipLevelCode"].ToString();       //설비등급


                                ReadEquipInfo(strPlant, strEquip);

                            }
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001116", "M000902", Infragistics.Win.HAlign.Right);

                                if (!this.uTextSysName.Text.Equals(string.Empty))
                                {
                                    this.uTextSysName.Text = "";      //설비명
                                    this.uTextSuperEquip.Text = "";     //Super설비
                                    this.uTextStation.Text = "";        //Station
                                    this.uTextArea.Text = "";           //Area
                                    this.uTextEquipProcess.Text = "";   //공정구분
                                    this.uTextEquipLoc.Text = "";       //위치
                                    this.uTextModel.Text = "";          //모델
                                    this.uTextEquipType.Text = "";      //설비유형
                                    this.uTextSerialNo.Text = "";       //SerialNo
                                    this.uTextVendor.Text = "";         //거래처
                                    this.uTextEquipGroupName.Text = ""; //그룹명
                                    this.uTextSTSStock.Text = "";       //입고일
                                    this.uTextMakerYear.Text = "";      //제작년도
                                    this.uTextEquipLevel.Text = "";     //설비등급


                                }

                                return;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #endregion


        /// <summary>
        /// 설비선택시 자동조회
        /// </summary>  
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        private void ReadEquipInfo(string strPlantCode,string strEquipCode)
        {
            try
            {
                // 설비폐기 BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EquipDiscardFile), "EquipDiscardFile");
                QRPEQU.BL.EQUCCS.EquipDiscardFile clsEquipDiscardFile = new QRPEQU.BL.EQUCCS.EquipDiscardFile();
                brwChannel.mfCredentials(clsEquipDiscardFile);

                //해당 설비의 설비폐기 정보를 가져온다.
                DataTable dtDiscardInfo = clsEquipDiscardFile.mfReadEquipDiscardFile(strPlantCode, strEquipCode);

                //- -정보가 있을 경우 헤더 부분과 첨부파일 부분에 삽입시킨다 //
                if (dtDiscardInfo.Rows.Count > 0)
                {
                    //this.uTextEquipCode.Tag = dtDiscardInfo.Rows[0]["GWTFlagDiscard"].ToString();
                    this.uTextDiscardReason.Text = dtDiscardInfo.Rows[0]["DiscardReason"].ToString();
                    this.uTextDiscardUserID.Text = dtDiscardInfo.Rows[0]["DiscardChargeID"].ToString();
                    this.uTextDiscardUserName.Text = dtDiscardInfo.Rows[0]["DiscardChargeName"].ToString();
                    this.uDateDiscardTime.Value = dtDiscardInfo.Rows[0]["DiscardDate"].ToString();
                    this.uNumPurchasePrice.Value = dtDiscardInfo.Rows[0]["PurchasePrice"];
                    this.uNumRemainPrice.Value = dtDiscardInfo.Rows[0]["RemainPrice"];



                    this.uGridEquipDiscardFile.DataSource = dtDiscardInfo;
                    this.uGridEquipDiscardFile.DataBind();
                }
                else
                {
                    IninText();
                    this.uDateDiscardTime.Value = DateTime.Now;

                    this.uTextDiscardReason.Text = "";
                    if (this.uGridEquipDiscardFile.Rows.Count > 0)
                    {
                        //-- 전체 행을 선택 하여 삭제한다 --//
                        this.uGridEquipDiscardFile.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipDiscardFile.Rows.All);
                        this.uGridEquipDiscardFile.DeleteSelectedRows(false);
                    }
                }

                if(!this.uCheckGWTFlag.Checked && this.uCheckGWTFlag.Enabled)
                    ReadOnlyControl();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 수정가능여부
        /// </summary>
        private void ReadOnlyControl()
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //승인상태가 T 인 경우 수정불가
                if (this.uCheckGWTFlag.Checked.Equals(true) || this.uCheckGWResultState.Checked.Equals(true))
                {
                    this.uComboPlant.ReadOnly = true;
                    this.uTextEquipCode.ReadOnly = true;
                    this.uDateDiscardTime.ReadOnly = true;
                    this.uTextDiscardUserID.ReadOnly = true;
                    this.uNumPurchasePrice.ReadOnly = true;
                    this.uNumRemainPrice.ReadOnly = true;
                    this.uTextDiscardReason.ReadOnly = true;

                    if (this.uGridEquipDiscardFile.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridEquipDiscardFile.Rows.Count; i++)
                        {
                            this.uGridEquipDiscardFile.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }
                    }

                    this.uGridEquipDiscardFile.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                }
                //T가아닌 경우 수정가능
                else
                {
                    if (this.uTextEquipCode.ReadOnly.Equals(true))
                    {

                        this.uComboPlant.ReadOnly = false;
                        this.uTextEquipCode.ReadOnly = false;
                        this.uDateDiscardTime.ReadOnly = false;
                        this.uTextDiscardUserID.ReadOnly = false;
                        this.uNumPurchasePrice.ReadOnly = false;
                        this.uNumRemainPrice.ReadOnly = false;
                        this.uTextDiscardReason.ReadOnly = false;

                        if (this.uGridEquipDiscardFile.DisplayLayout.Override.AllowAddNew == Infragistics.Win.UltraWinGrid.AllowAddNew.No)
                        {
                            this.uGridEquipDiscardFile.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// MDM에 설비폐기정보 재송신
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        private void mfSaveMDM(string strPlantCode,string strEquipCode)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (m_resSys.GetString("SYS_SERVERPATH").Contains("10.61.61.71") || m_resSys.GetString("SYS_SERVERPATH").Contains("10.61.61.73"))
                {

                    //-------------------------------------//

                    WinMessageBox msg = new WinMessageBox();


                    //팝업창을 띄운다
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "재전송중...");
                    //-커서를 변경한다.
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsEquip.mfSaveEquipDiscard_MDM(strPlantCode, strEquipCode, "");

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    //팝업창을 닫는다.
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    DialogResult result = new DialogResult();


                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "M001135", "M001061", "M000931",
                                                               Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strErrMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strErrMes = msg.GetMessge_Text("M000956", strLang);
                        else
                            strErrMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                         Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001061", strLang), strErrMes,
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region 수정여부
        private void uDateDiscardTime_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                
                if (strEdit == "")
                { strEdit = "T"; }
            }
            catch (Exception ex)
            {
            }
            finally
            { }
        }

        private void uTextDiscardUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextDiscardUserID.ReadOnly.Equals(false))
                {
                    if (strEdit == "")
                    { strEdit = "T"; }

                    //아이디지울시 이름도 지움
                    if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                    {
                        this.uTextDiscardUserName.Text = "";
                    }

                    if (e.KeyData == Keys.Enter)
                    {
                        //폐기담당자 공장코드 저장
                        string strDiscardUserID = this.uTextDiscardUserID.Text;
                        string strPlantCode = this.uComboPlant.Value.ToString();

                        WinMessageBox msg = new WinMessageBox();

                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        //----- 아이디 공백 확인
                        if (strDiscardUserID == "")
                        {

                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                            //Focus
                            this.uTextDiscardUserID.Focus();
                            return;
                        }
                        //-- 공장 확인
                        else if (strPlantCode == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M000240", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                            //DropDown
                            this.uComboPlant.DropDown();
                            return;
                        }

                        //-- 유저정보 가져오기 
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strDiscardUserID, m_resSys.GetString("SYS_LANG"));

                        if (dtUser.Rows.Count > 0)
                        {
                            // 이름 텍스트에 유저 이름을 삽입한다.
                            this.uTextDiscardUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDiscardReason_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.uTextDiscardReason.ReadOnly.Equals(false))
            {
                if (strEdit == "")
                { strEdit = "T"; }
            }
        }

        #endregion

        private void frmEQUZ0005_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }




    }
}
