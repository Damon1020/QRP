﻿namespace QRPEQU.UI
{
    partial class frmEQU0005D1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0005D1));
            this.uGroupBoxHeader = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSave = new Infragistics.Win.Misc.UltraButton();
            this.uTextDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateOut = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReairGICode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPMPlanDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboEquipCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelOutDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPMPlanDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxDurable = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridRepair = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteDurableRow = new Infragistics.Win.Misc.UltraButton();
            this.uGroupSparePart = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridUseSP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteSPRow = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxHeader)).BeginInit();
            this.uGroupBoxHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReairGICode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMPlanDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurable)).BeginInit();
            this.uGroupBoxDurable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSparePart)).BeginInit();
            this.uGroupSparePart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUseSP)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxHeader
            // 
            this.uGroupBoxHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxHeader.Controls.Add(this.uButtonClose);
            this.uGroupBoxHeader.Controls.Add(this.uButtonSave);
            this.uGroupBoxHeader.Controls.Add(this.uTextDesc);
            this.uGroupBoxHeader.Controls.Add(this.uTextUserID);
            this.uGroupBoxHeader.Controls.Add(this.uDateOut);
            this.uGroupBoxHeader.Controls.Add(this.uTextPlantCode);
            this.uGroupBoxHeader.Controls.Add(this.uTextReairGICode);
            this.uGroupBoxHeader.Controls.Add(this.uTextUserName);
            this.uGroupBoxHeader.Controls.Add(this.uTextPMPlanDate);
            this.uGroupBoxHeader.Controls.Add(this.uComboEquipCode);
            this.uGroupBoxHeader.Controls.Add(this.uLabelUserID);
            this.uGroupBoxHeader.Controls.Add(this.uLabelDesc);
            this.uGroupBoxHeader.Controls.Add(this.uLabelOutDate);
            this.uGroupBoxHeader.Controls.Add(this.uLabelPMPlanDate);
            this.uGroupBoxHeader.Controls.Add(this.uLabelEquipCode);
            this.uGroupBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxHeader.Name = "uGroupBoxHeader";
            this.uGroupBoxHeader.Size = new System.Drawing.Size(1050, 88);
            this.uGroupBoxHeader.TabIndex = 2;
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(912, 52);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 7;
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uButtonSave
            // 
            this.uButtonSave.Location = new System.Drawing.Point(820, 52);
            this.uButtonSave.Name = "uButtonSave";
            this.uButtonSave.Size = new System.Drawing.Size(88, 28);
            this.uButtonSave.TabIndex = 7;
            this.uButtonSave.Click += new System.EventHandler(this.uButtonSave_Click);
            // 
            // uTextDesc
            // 
            this.uTextDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextDesc.MaxLength = 100;
            this.uTextDesc.Name = "uTextDesc";
            this.uTextDesc.Size = new System.Drawing.Size(512, 21);
            this.uTextDesc.TabIndex = 6;
            // 
            // uTextUserID
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.Appearance = appearance30;
            this.uTextUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton1);
            this.uTextUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextUserID.Location = new System.Drawing.Point(424, 36);
            this.uTextUserID.MaxLength = 20;
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 5;
            this.uTextUserID.ValueChanged += new System.EventHandler(this.uTextUserID_ValueChanged);
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uDateOut
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateOut.Appearance = appearance31;
            this.uDateOut.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateOut.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateOut.Location = new System.Drawing.Point(116, 36);
            this.uDateOut.Name = "uDateOut";
            this.uDateOut.Size = new System.Drawing.Size(100, 21);
            this.uDateOut.TabIndex = 4;
            // 
            // uTextPlantCode
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance1;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(836, 12);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 3;
            this.uTextPlantCode.Visible = false;
            // 
            // uTextReairGICode
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReairGICode.Appearance = appearance29;
            this.uTextReairGICode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReairGICode.Location = new System.Drawing.Point(940, 12);
            this.uTextReairGICode.Name = "uTextReairGICode";
            this.uTextReairGICode.ReadOnly = true;
            this.uTextReairGICode.Size = new System.Drawing.Size(100, 21);
            this.uTextReairGICode.TabIndex = 3;
            this.uTextReairGICode.Visible = false;
            // 
            // uTextUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance2;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(528, 36);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 3;
            // 
            // uTextPMPlanDate
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMPlanDate.Appearance = appearance4;
            this.uTextPMPlanDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMPlanDate.Location = new System.Drawing.Point(424, 12);
            this.uTextPMPlanDate.Name = "uTextPMPlanDate";
            this.uTextPMPlanDate.ReadOnly = true;
            this.uTextPMPlanDate.Size = new System.Drawing.Size(100, 21);
            this.uTextPMPlanDate.TabIndex = 3;
            // 
            // uComboEquipCode
            // 
            this.uComboEquipCode.Location = new System.Drawing.Point(116, 12);
            this.uComboEquipCode.MaxLength = 50;
            this.uComboEquipCode.Name = "uComboEquipCode";
            this.uComboEquipCode.Size = new System.Drawing.Size(144, 21);
            this.uComboEquipCode.TabIndex = 2;
            this.uComboEquipCode.ValueChanged += new System.EventHandler(this.uComboEquipCode_ValueChanged);
            // 
            // uLabelUserID
            // 
            this.uLabelUserID.Location = new System.Drawing.Point(320, 36);
            this.uLabelUserID.Name = "uLabelUserID";
            this.uLabelUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelUserID.TabIndex = 0;
            // 
            // uLabelDesc
            // 
            this.uLabelDesc.Location = new System.Drawing.Point(12, 60);
            this.uLabelDesc.Name = "uLabelDesc";
            this.uLabelDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelDesc.TabIndex = 0;
            // 
            // uLabelOutDate
            // 
            this.uLabelOutDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelOutDate.Name = "uLabelOutDate";
            this.uLabelOutDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelOutDate.TabIndex = 0;
            // 
            // uLabelPMPlanDate
            // 
            this.uLabelPMPlanDate.Location = new System.Drawing.Point(320, 12);
            this.uLabelPMPlanDate.Name = "uLabelPMPlanDate";
            this.uLabelPMPlanDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelPMPlanDate.TabIndex = 0;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCode.TabIndex = 0;
            // 
            // uGroupBoxDurable
            // 
            this.uGroupBoxDurable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxDurable.Controls.Add(this.uGridRepair);
            this.uGroupBoxDurable.Controls.Add(this.uButtonDeleteDurableRow);
            this.uGroupBoxDurable.Location = new System.Drawing.Point(0, 92);
            this.uGroupBoxDurable.Name = "uGroupBoxDurable";
            this.uGroupBoxDurable.Size = new System.Drawing.Size(1050, 304);
            this.uGroupBoxDurable.TabIndex = 3;
            // 
            // uGridRepair
            // 
            this.uGridRepair.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRepair.DisplayLayout.Appearance = appearance17;
            this.uGridRepair.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRepair.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepair.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepair.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGridRepair.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepair.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGridRepair.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRepair.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRepair.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRepair.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGridRepair.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRepair.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRepair.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRepair.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGridRepair.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRepair.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepair.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGridRepair.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGridRepair.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRepair.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGridRepair.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGridRepair.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRepair.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGridRepair.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRepair.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRepair.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRepair.Location = new System.Drawing.Point(12, 36);
            this.uGridRepair.Name = "uGridRepair";
            this.uGridRepair.Size = new System.Drawing.Size(1020, 252);
            this.uGridRepair.TabIndex = 2;
            this.uGridRepair.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridRepair_AfterCellUpdate);
            this.uGridRepair.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridRepair_CellListSelect);
            this.uGridRepair.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridRepair_BeforeCellListDropDown);
            this.uGridRepair.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAll_CellChange);
            // 
            // uButtonDeleteDurableRow
            // 
            this.uButtonDeleteDurableRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteDurableRow.Name = "uButtonDeleteDurableRow";
            this.uButtonDeleteDurableRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteDurableRow.TabIndex = 1;
            this.uButtonDeleteDurableRow.Click += new System.EventHandler(this.uButtonDeleteDurableRow_Click);
            // 
            // uGroupSparePart
            // 
            this.uGroupSparePart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupSparePart.Controls.Add(this.uGridUseSP);
            this.uGroupSparePart.Controls.Add(this.uButtonDeleteSPRow);
            this.uGroupSparePart.Location = new System.Drawing.Point(0, 400);
            this.uGroupSparePart.Name = "uGroupSparePart";
            this.uGroupSparePart.Size = new System.Drawing.Size(1050, 304);
            this.uGroupSparePart.TabIndex = 3;
            // 
            // uGridUseSP
            // 
            this.uGridUseSP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridUseSP.DisplayLayout.Appearance = appearance5;
            this.uGridUseSP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridUseSP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUseSP.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUseSP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridUseSP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUseSP.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridUseSP.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridUseSP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridUseSP.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridUseSP.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridUseSP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridUseSP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridUseSP.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridUseSP.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridUseSP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridUseSP.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUseSP.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridUseSP.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridUseSP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridUseSP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridUseSP.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridUseSP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridUseSP.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridUseSP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridUseSP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridUseSP.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridUseSP.Location = new System.Drawing.Point(12, 36);
            this.uGridUseSP.Name = "uGridUseSP";
            this.uGridUseSP.Size = new System.Drawing.Size(1020, 252);
            this.uGridUseSP.TabIndex = 2;
            this.uGridUseSP.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUseSP_AfterCellUpdate);
            this.uGridUseSP.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUseSP_CellListSelect);
            this.uGridUseSP.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridUseSP_BeforeCellListDropDown);
            this.uGridUseSP.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAll_CellChange);
            // 
            // uButtonDeleteSPRow
            // 
            this.uButtonDeleteSPRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteSPRow.Name = "uButtonDeleteSPRow";
            this.uButtonDeleteSPRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteSPRow.TabIndex = 1;
            this.uButtonDeleteSPRow.Click += new System.EventHandler(this.uButtonDeleteSPRow_Click);
            // 
            // frmEQU0005D1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1050, 715);
            this.Controls.Add(this.uGroupSparePart);
            this.Controls.Add(this.uGroupBoxDurable);
            this.Controls.Add(this.uGroupBoxHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0005D1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "治工具/SparePart交替";
            this.Load += new System.EventHandler(this.frmEQU0005D1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxHeader)).EndInit();
            this.uGroupBoxHeader.ResumeLayout(false);
            this.uGroupBoxHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReairGICode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMPlanDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurable)).EndInit();
            this.uGroupBoxDurable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSparePart)).EndInit();
            this.uGroupSparePart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridUseSP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxHeader;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMPlanDate;
        private Infragistics.Win.Misc.UltraLabel uLabelPMPlanDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReairGICode;
        private Infragistics.Win.Misc.UltraLabel uLabelOutDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateOut;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelDesc;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxDurable;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepair;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteDurableRow;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSparePart;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteSPRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUseSP;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraButton uButtonSave;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
    }
}