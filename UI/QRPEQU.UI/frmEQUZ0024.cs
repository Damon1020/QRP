﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0024.cs                                        */
/* 프로그램명   : 설비점검일지                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-12-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-12-01 : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using Microsoft.VisualBasic;


namespace QRPEQU.UI
{
    public partial class frmEQUZ0024 : Form,IToolbar
    {

        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0024()
        {
            InitializeComponent();
        }

        private void frmEQUZ0024_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0024_Load(object sender, EventArgs e)
        {
            SetToolAuth();

            //컨트롤초기화
            InitTltie();
            InitLabel();
            InitGrid();
            InitCombo();
            InitText();
            
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTltie()
        {
            try
            {
                //System reousrceINSys
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("설비점검결과조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchPlanYear, "계획년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchPMMonth, "점검월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchEquipCode, "설비", m_resSys.GetString("SYS_FONTNAME"), true, true);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                //공장정보조회매서드 실행
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));


                WinComboEditor com = new WinComboEditor();

                // Month Combo Box
                ArrayList Key = new ArrayList();
                ArrayList Value = new ArrayList();

                for (int i = 1; i < 13; i++)
                {
                    if (i < 10)
                    {
                        Key.Add("0" + i.ToString());
                        Value.Add(i.ToString());
                    }
                    else
                    {
                        Key.Add(i);
                        Value.Add(i.ToString());
                    }
                }

                //해당 콤보박스에 삽입

                com.mfSetComboEditor(this.uComboSerachPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE")
                                    , "", "선택", "PlantCode", "PlantName", dtPlant);

                com.mfSetComboEditor(this.uComboSearchPMMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", Key, Value);

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                // ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uNumSerachPlanYear.Value = DateTime.Now.Year;
                this.uComboSearchPMMonth.Value = DateTime.Now.Month.ToString("D2");
                this.uTextEquipCode.Clear();
                this.uTextEquipName.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //설비보전계획표그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridPMResultList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //설비보전계획 그리드 컬럼설정
                grd.mfSetGridColumn(this.uGridPMResultList, 0, "PMPeriodName", "주기", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 40, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResultList, 0, "Idx", "NO", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 30, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridPMResultList, 0, "PMInspectName", "점검항목", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResultList, 0, "PMInspectCriteria", "점검기준", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResultList, 0, "PMInspectRegion", "점검방법", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                #region 날짜컬럼

                for (int i = 0; i < 31; i++)
                {
                    string strKey = "";
                    string strValue = "";
                    if (i < 9)
                    {
                        strKey = "D0" + (i + 1).ToString();
                        strValue = (i + 1).ToString();
                    }
                    else
                    {
                        strKey = "D" + (i + 1).ToString();
                        strValue = (i + 1).ToString();
                    }
                    grd.mfSetGridColumn(this.uGridPMResultList, 0, strKey, strValue, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                }
                #endregion

                this.uGridPMResultList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPMResultList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Toolbar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항

                if (this.uComboSerachPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSerachPlant.DropDown();
                    return;
                }

                if (this.uNumSerachPlanYear.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000250", Infragistics.Win.HAlign.Right);

                    this.uNumSerachPlanYear.Focus();
                    return;
                }

                if (this.uComboSearchPMMonth.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001065", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPMMonth.DropDown();
                    return;
                }

                if (this.uTextEquipCode.Text.Equals(string.Empty) || this.uTextEquipName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000700", Infragistics.Win.HAlign.Right);

                    this.uTextEquipCode.Focus();
                    return;
                }

                #endregion

                //검색정보 저장
                string strPlantCode = this.uComboSerachPlant.Value.ToString();
                string strPlanYear = this.uNumSerachPlanYear.Value.ToString();
                string strMonth = this.uComboSearchPMMonth.Value.ToString();
                string strPMPlanDate = strPlanYear + "-" + this.uComboSearchPMMonth.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //예방점검정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                brwChannel.mfCredentials(clsPMPlanD);

                //설비점검일지 조회 매서드 실행
                DataTable dtMonthResult = clsPMPlanD.mfReadPMPlanD_MonthResult(strPlantCode, strPlanYear, strPMPlanDate, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridPMResultList.DataSource = dtMonthResult;
                this.uGridPMResultList.DataBind();


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtMonthResult.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                    
                }
                else
                {

                    #region 월마다 해당하는 날짜 이후 숨김

                    //월을 인트로 변환
                    int intYear = Convert.ToInt32(strPlanYear);
                    int intMonth = Convert.ToInt32(strMonth);
                    int intColumns = 0;
                    //월이 4,6,9,11 인경우 날짜는 30일까지
                    if (intMonth.Equals(2)
                        || intMonth.Equals(4)
                        || intMonth.Equals(6)
                        || intMonth.Equals(9)
                        || intMonth.Equals(11))
                    {
                        //월이 2월인경우 날짜는 28일 혹은 29일까지 있다.
                        if (intMonth.Equals(2))
                        {
                            // 년도 / 4 하여 몫이 있으면 28일 
                            if (!(intYear % 4).Equals(0))
                            {
                                intColumns = 28;
                            }
                            //없으면 29일
                            else
                            {
                                intColumns = 29;
                            }
                        }
                        else
                        {
                            intColumns = 30;
                        }
                    }
                    else
                    {
                        intColumns = 31;
                    }

                    for (int i = 28; i < 32; i++)
                    {
                        string strKey = "D" + i.ToString();
                        if (i <= intColumns && this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden)
                            this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden = false;

                        if (i > intColumns && this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden.Equals(false))
                        {
                            this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden = true;
                        }
                    }

                    #endregion

                    #region 점검결과가 있는 날의 셀색이 변한다.
                    //for문을 돌며 셀값을 판별하여 색을 준다.
                    for (int i = 0; i < this.uGridPMResultList.Rows.Count; i++)
                    {
                        for (int j = 0; j < 31; j++)
                        {

                            string strKey = "";
                            if (j < 9)
                            {
                                strKey = "D0" + (j + 1).ToString();
                            }
                            else
                            {
                                strKey = "D" + (j + 1).ToString();
                            }

                            if (!this.uGridPMResultList.Rows[i].Cells[strKey].Value.ToString().Equals(string.Empty))
                            {
                                this.uGridPMResultList.Rows[i].Cells[strKey].Appearance.BackColor = Color.Salmon;
                            }
                        }
                    }

                    #endregion

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMResultList.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridPMResultList);
            }
            catch (Exception ex)
            {
            }
            finally
            {

            }
        }
        public void mfPrint()
        {
        }
        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        #endregion

        #region TextEvent

        //설비텍스트 정보가 변할시 설비이름이 초기화
        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextEquipName.Text.Equals(string.Empty))
                {
                    this.uTextEquipName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비텍스트에 코드를 입력후 엔터키를 누를 시 자동조회가 된다.
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //설비코드저장
                string strEquipCode = this.uTextEquipCode.Text;

                if (e.KeyData.Equals(Keys.Enter) && !strEquipCode.Equals(string.Empty))
                {
                    //System ResourcesInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //공장정보저장
                    string strPlantCode = this.uComboSerachPlant.Value.ToString();

                    //공장정보가 공백일 경우
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboSerachPlant.DropDown();
                        return;
                    }

                    //설비정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보조회매서드 실행
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //설비정보가 있는 경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M001116", "M000901", Infragistics.Win.HAlign.Right);
                        if (!this.uTextEquipName.Text.Equals(string.Empty))
                            this.uTextEquipName.Clear();
                    }


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //설비텍스트의 에디트 버튼을 클릭할 경우 설비팝업창이 뜬다.
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = this.uComboSerachPlant.Value.ToString();


                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001233", "M001235", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSerachPlant.DropDown();
                    return;
                }

                frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();


                this.uTextEquipCode.Text = frmEquip.EquipCode;
                this.uTextEquipName.Text = frmEquip.EquipName;



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        #endregion

        #region NumText

        // 스핀버튼을 클릭하였을 때 Value값이 증가하거나 감소된다.
        private void uNumSerachPlanYear_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > (int)ed.MinValue)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uNumSerachPlanYear_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

            // 값을 1로 초기화
            ed.Value = DateTime.Now.Year;
        }

        #endregion

        private void uComboSearchPMMonth_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uNumSerachPlanYear.Value.ToString().Length.Equals(4)
                    && Convert.ToInt32(this.uNumSerachPlanYear.Value) > 1990
                    && Information.IsNumeric(this.uComboSearchPMMonth.Value)
                    && !this.uComboSearchPMMonth.Value.ToString().Equals(string.Empty))
                {
                    #region 콤보로선택한 월의 일수를 구함

                    //월을 인트로 변환
                    int intYear = Convert.ToInt32(this.uNumSerachPlanYear.Value);
                    int intMonth = Convert.ToInt32(this.uComboSearchPMMonth.Value);
                    int intColumns = 0;
                    //월이 4,6,9,11 인경우 날짜는 30일까지
                    if (intMonth.Equals(2)
                        || intMonth.Equals(4)
                        || intMonth.Equals(6)
                        || intMonth.Equals(9)
                        || intMonth.Equals(11))
                    {
                        //월이 2월인경우 날짜는 28일 혹은 29일까지 있다.
                        if (intMonth.Equals(2))
                        {
                            // 년도 / 4 하여 몫이 있으면 28일 
                            if (!(intYear % 4).Equals(0))
                            {
                                intColumns = 28;
                            }
                            //없으면 29일
                            else
                            {
                                intColumns = 29;
                            }
                        }
                        else
                        {
                            intColumns = 30;
                        }
                    }
                    else
                    {
                        intColumns = 31;
                    }

                    #endregion

                    #region 선택한월의 해당하는 Day가 아닌 Day컬럼을 숨김

                    for (int i = 1; i < 32; i++)
                    {
                        string strKey = "";

                        if (i < 10)
                        {
                            strKey = "D0" + i.ToString();
                        }
                        else
                        {
                            strKey = "D" + i.ToString();
                        }

                        if (i > 27 && i < 32)
                        {
                            if (i <= intColumns && this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden)
                                this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden = false;

                            if (i > intColumns && this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden.Equals(false))
                            {
                                this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden = true;
                            }
                        }

                        //해당월의 일요일인 날짜 컬럼색을 빨간색으로 바꿈
                        if (!this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Hidden)
                        {
                            if (Convert.ToDateTime(intYear.ToString() + "-" + intMonth.ToString() + "-" + (i)).DayOfWeek.ToString().Equals("Sunday"))
                            {
                                this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Header.Appearance.ForeColor = Color.Red;
                            }
                            else
                            {
                                this.uGridPMResultList.DisplayLayout.Bands[0].Columns[strKey].Header.Appearance.ForeColor = Color.Black;
                            }

                        }
                    }

                    #endregion

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

    }
}
