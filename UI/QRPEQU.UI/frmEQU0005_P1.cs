﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQU0005_P1.cs                                      */
/* 프로그램명   : 점검결과등록(수치입력 팝업창)                         */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0005_P1 : Form
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();


        private string strNumber;

        public string Number
        {
            get { return strNumber; }
            set { strNumber = value; }
        }


        public frmEQU0005_P1()
        {
            InitializeComponent();
        }

        private void frmEQU0005_P1_Load(object sender, EventArgs e)
        {
            strNumber = "";
            InitButton();
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonPlus, "+", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButtonM, "-", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButtonPoint, ".", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton000, "000", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton00, "00", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton0, "0", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton1, "1", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton2, "2", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton3, "3", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton4, "4", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton5, "5", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton6, "6", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton7, "7", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton8, "8", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButton9, "9", m_resSys.GetString("SYS_FONTNAME"), null);
                btn.mfSetButton(this.uButtonOK, "입력", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCancel, "취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonClear, "Clear", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Refresh);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 숫자버튼입력시 숫자가 텍스트에 입력된다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_num(object sender, EventArgs e)
        {
            try
            {
                Infragistics.Win.Misc.UltraButton btn = (Infragistics.Win.Misc.UltraButton)sender;


                if (btn.Name.Equals("uButtonPoint") && strNumber.Length.Equals(0))
                    strNumber += "0" + btn.Text;
                else if (btn.Name.Equals("uButtonPoint") && !strNumber.Contains('.'))
                    strNumber += btn.Text;
                else if (btn.Name.Equals("uButtonPlus") && !strNumber.Contains('+'))
                    strNumber += btn.Text;
                else if (btn.Name.Equals("uButtonM") && !strNumber.Contains('-'))
                    strNumber += btn.Text;
                else if (!btn.Name.Equals("uButtonPoint") && !btn.Name.Equals("uButtonM") && !btn.Name.Equals("uButtonPlus"))
                    strNumber += btn.Text;

                if (strNumber.Length <= this.uTextReturnValue.MaxLength)
                    this.uTextReturnValue.Text = strNumber;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            if (strNumber != null &&
                !strNumber.Equals(string.Empty) &&
                Convert.ToDecimal(strNumber).Equals(0))
                strNumber = "";

            this.Close();
        }

        /// <summary>
        /// 취소버튼클릭시 입력숫자 초기화 시킨 후 폼창을 닫는다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            strNumber = "";
            this.Close();
        }

        /// <summary>
        /// 클리어버튼클릭시 텍스트 클리어
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonClear_Click(object sender, EventArgs e)
        {
            strNumber = "";
            this.uTextReturnValue.Clear();
        }


        /// <summary>
        /// Back Space누를시 숫자가 줄어듬
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonBack_Click(object sender, EventArgs e)
        {
            if (!this.uTextReturnValue.Text.Equals(string.Empty))
            {
                string strTmp = this.uTextReturnValue.Text;

                int strCount = strTmp.Length;
                strNumber = strTmp.Remove(strCount - 1, 1);
                this.uTextReturnValue.Text = strNumber;
            }
        }

        private void uTextReturnValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                // (.) 키가 눌렸을때 이미 (.)이 찍힌 상황이면 return;
                if (e.KeyChar.Equals('.'))
                {
                    if (this.uTextReturnValue.Text.Contains('.'))
                        e.Handled = true;
                }

                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) || e.KeyChar.Equals('.')))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }







    }
}
