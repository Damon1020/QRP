﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0032.cs                                        */
/* 프로그램명   : PM체크이력조회                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-02-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*              : ~~~~~ 추가 ()                                         */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0032 : Form,IToolbar
    {

        //리소스호출을위한전역변수
        QRPGlobal SysRes = new QRPGlobal();


        public frmEQUZ0032()
        {
            InitializeComponent();
        }

        private void frmEQUZ0032_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfActiveToolBar(this.MdiParent, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0032_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0032_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitCombo();
            InitGrid();

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 타이틀초기화
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("PM체크이력조회", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// Label초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchDate, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, true);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                
                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Grid초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                grd.mfInitGeneralGrid(this.uGridPMHist, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                  false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                  Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                  Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                grd.mfSetGridColumn(this.uGridPMHist, 0, "PlantCode", "공장", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "EquipCode", "설비", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMPlanDate", "점검일자", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMPeriodCode", "점검주기", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMInspectCriteria", "점검기준", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMInspectName", "점검항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "UnitDesc", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMWorkTime", "점검시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMWorker", "점검자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMResult", "점검결과(수치)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMHist, 0, "PMWorkDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                this.uGridPMHist.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPMHist.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #region DropDown
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtPeriod = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridPMHist, 0, "PMPeriodCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPeriod);

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                ClearGrid();

                #region 필수입력확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uDateSearchFrom.Value == null || this.uDateSearchFrom.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000209", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFrom.DropDown();
                    return;
                }
                if (this.uDateSearchTo.Value == null || this.uDateSearchTo.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000209", Infragistics.Win.HAlign.Right);

                    this.uDateSearchTo.DropDown();
                    return;
                }
                if (this.uTextSearchEquipCode.Text.Equals(string.Empty) || this.uTextSearchEquipName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000700", Infragistics.Win.HAlign.Right);

                    this.uTextSearchEquipCode.Focus();
                    return;
                }

                #endregion

                //검색조건 정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromDate = this.uDateSearchFrom.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDate = this.uDateSearchTo.DateTime.Date.ToString("yyyy-MM-dd");

                string strEquipCode = "";
                if (!this.uTextSearchEquipCode.Text.Equals(string.Empty) && !this.uTextSearchEquipName.Text.Equals(string.Empty))
                    strEquipCode = this.uTextSearchEquipCode.Text;


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //PM조회 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.PMDIS), "PMDIS");
                QRPEQU.BL.EQUDIS.PMDIS clsPMDIS = new QRPEQU.BL.EQUDIS.PMDIS();
                brwChannel.mfCredentials(clsPMDIS);

                //PM체크 이력조회 매서드 실행
                DataTable dtPMPlanDHist = clsPMDIS.mfReadPMPlanDHist(strPlantCode, strEquipCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //그리드에 정보 바인드
                this.uGridPMHist.DataSource = dtPMPlanDHist;
                this.uGridPMHist.DataBind();


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtPMPlanDHist.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);


                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMHist.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridPMHist);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
        }

        public void mfCreate()
        {
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        #endregion

        #region Event

        #region Text
        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                this.uTextSearchEquipName.Clear();
        }

        /// <summary>
        /// 설비팝업창호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                frmEquip.PlantCode = strPlant;
                frmEquip.ShowDialog();

                if (frmEquip.PlantCode != "" && strPlant != frmEquip.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode.ToString();
                this.uTextSearchEquipName.Text = frmEquip.EquipName.ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비입력조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Enter)
                    return;

                if (uTextSearchEquipCode.Text == string.Empty)
                    return;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;

                //설비정보BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //설비정보조회 매서드 실행
                DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count != 0)
                {
                    uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();

                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000901", Infragistics.Win.HAlign.Right);
                    uTextSearchEquipName.Clear();

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #endregion

        /// <summary>
        /// 그리드 줄삭제
        /// </summary>
        private void ClearGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (this.uGridPMHist.Rows.Count > 0)
                {
                    this.uGridPMHist.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridPMHist.Rows.All);
                    this.uGridPMHist.DeleteSelectedRows(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

    }
}
