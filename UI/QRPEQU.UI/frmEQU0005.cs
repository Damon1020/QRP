﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQU0005.cs                                         */
/* 프로그램명   : 점검결과등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-25 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Infragistics.Win.UltraWinGrid;
using System.Collections;
using System.IO;


namespace QRPEQU.UI
{
    public partial class frmEQU0005 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";

        //private Infragistics.Win.UltraWinEditors.UltraComboEditor uCom;
        
        // 그리드 셀에 집어넣을 라디오 버튼
        //private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionPMResult;
       
        // 같은 로우의 다른 라디오 버튼을 false 처리 하기 위한 셀 변수
        //private UltraGridCell selectedCell = null;

        public frmEQU0005()
        {
            InitializeComponent();
            //uCom = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            //this.uCom.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uCom_EditorButtonClick);
        }

        private void frmEQU0005_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0005_Load(object sender, EventArgs e)
        {
            SetRunMode();
            SetToolAuth();
            // 초기화 Method
            InitTitle();
            InitButton();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();


            //QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화
        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            //MessageBox.Show(this.Tag.ToString());
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

       

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Title 제목
                titleArea.mfSetLabelText("PM체크 결과등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button
        /// </summary>
        private void InitButton()
        {
            try
            {
                WinButton btn = new WinButton();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                btn.mfSetButton(this.uButtonFileDwon, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 초기값 설정 Method
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                // 점검계획일 초기화
                this.uDateSearchPlanYear.Value = DateTime.Now;
                
                this.uTextTechnician.Text = m_resSys.GetString("SYS_USERID");
                this.uTextTechnicianName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uTextPMWorkerID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextPMWorkName.Text = m_resSys.GetString("SYS_USERNAME");

                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipMentGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcGubun, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchUser, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPMWoker, "PM입력자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                //기본값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();




                //---------점검결과 그리드 콤보박스 -------------//
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                #region 그리드 설정

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column  

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PlantCode", "공장", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 60, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PACKAGE", "PACKAGE", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "ModelName", "ModelName", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "AdminRight", "정/부", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "EquipCode", "설비코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMPeriodName", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMWorkDate", "점검일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "ImageName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "ImageFile", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMWorkTime", "점검시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Time, "", "hh:mm:ss", "");


                //this.uGridPMResult.DisplayLayout.Bands[0].Layout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //this.uGridPMResult.DisplayLayout.Bands[0].Layout.Override.RowSizing = RowSizing.AutoFree;
                //this.uGridPMResult.DisplayLayout.Bands[0].Layout.Override.CellMultiLine =  Infragistics.Win.DefaultableBoolean.True;


                #region 그리드안의 라디오버튼 설정

                ////라디오버튼설정
                //uOptionPMResult = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
                //Infragistics.Win.ValueListItem valueListItem = new Infragistics.Win.ValueListItem();
                //((ISupportInitialize)(this.uOptionPMResult)).BeginInit();

                //valueListItem.DataValue = true;
                //valueListItem.DisplayText = " ";

                //uOptionPMResult.Items.Clear();
                //this.uOptionPMResult.Items.AddRange(new Infragistics.Win.ValueListItem[] { valueListItem });
                //this.uOptionPMResult.Name = "uOptionPMResult";


                #endregion

                #region 셀을 라디오버튼으로 바꾸기

                //for (int i = 0; i < dtResult.Rows.Count; i++)
                //{

                //    string strPMResultCode = dtResult.Rows[i]["PMResultCode"].ToString();
                //    string strPMResultName = dtResult.Rows[i]["PMResultName"].ToString();

                //    wGrid.mfSetGridColumn(this.uGridPMResult, 0, strPMResultCode, strPMResultName, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 60, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "false");

                //    uGridPMResult.DisplayLayout.Bands[0].Columns[strPMResultCode].DataType = typeof(bool);
                //    uGridPMResult.DisplayLayout.Bands[0].Columns[strPMResultCode].Header.VisiblePosition = i + 6;
                //    uGridPMResult.DisplayLayout.Bands[0].Columns[strPMResultCode].EditorComponent = this.uOptionPMResult;


                //}

                #endregion

                //wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResult", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultCode", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.FormattedTextEditor, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "BtnTime", "", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 20, false, false, 0
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultCode", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "Button", "", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 20, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultName", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "MeasureValueFlag", "수치입력여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultValue", "수치", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true,false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "BtnValue", "", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 20, false, false, 1
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "ChangeFlag", "교체여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, true, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "RepairFlag", "수리여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "FilePath", "첨부파일등록", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMWorkDesc", "점검특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMPlanDate", "점검일", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "EquipName", "설비명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 30, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridPMResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPMResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["BtnTime"].Layout.UseFixedHeaders = false;
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["BtnTime"].Header.FixedHeaderIndicator = FixedHeaderIndicator.Default;
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["Button"].Layout.UseFixedHeaders = false;
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["Button"].Header.FixedHeaderIndicator = FixedHeaderIndicator.Default;
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["BtnValue"].Header.FixedHeaderIndicator = FixedHeaderIndicator.Default;

                #region 그리드 콤보박스

                //Infragistics.Win.UltraWinEditors.EditorButtonBase ubtn = new Infragistics.Win.UltraWinEditors.EditorButton();

                //ubtn.Appearance.Image = Properties.Resources.btn_Zoom;
                //ubtn.Appearance.ImageHAlign = Infragistics.Win.HAlign.Center;


                //uCom.ButtonsLeft.Add(ubtn);

                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["PMResultCode"].EditorComponent = uCom;
                
                //---------수리여부,교체여부 콤보 박스 ----------//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtChg = clsCommonCode.mfReadCommonCode("C0053", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridPMResult, 0, "ChangeFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtChg);

                DataTable dtRepair = clsCommonCode.mfReadCommonCode("C0054", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridPMResult, 0, "RepairFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtRepair);

                //---------점검결과 그리드 콤보박스 -------------//

                //string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResult);


                DataTable dtResult = clsPMResult.mfReadPMResultTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridPMResult, 0, "PMResultCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", " ", dtResult);

                // 수치입력 콤보

                dtRepair.Clear();

                dtRepair = clsCommonCode.mfReadCommonCode("C0061", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridPMResult, 0, "PMResultValue", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", " ", dtRepair);
                #endregion

                //this.uGridPMResult.DisplayLayout.Bands[0].Columns["PMInspectName"].CellMultiLine = Infragistics.Win.DefaultableBoolean.True;
                
                #region 주석

                //this.uGridPMResult.Bands[0].Columns[0].Footer.RowLayoutColumnInfo.SpanX = 3;

                //this.uGridPMResult.DisplayLayout.Bands[0].Columns[3].Footer.Total = SummaryInfo.Text;
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns[3].Footer.Style.HorizontalAlign = HorizontalAlign.Right;

                //this.uGridPMResult.DisplayLayout.Bands[0].Columns[4].Footer.Total = SummaryInfo.Text;
                //this.uGridPMResult.DisplayLayout.Bands[0].Columns[4].Footer.Style.HorizontalAlign = HorizontalAlign.Right;


                //this.uGridPMResult.DisplayLayout.StationaryMargins = Infragistics.WebUI.UltraWebGrid.StationaryMargins.HeaderAndFooter;

                #endregion

                //현재 그리드에 저장된 Key이벤트를 삭제한다.
                //this.uGridPMResult.KeyActionMappings.Clear();


                //this.uGridPMResult.DisplayLayout.Override.CellSpacing = 1;
                this.uGridPMResult.DisplayLayout.Override.RowSpacingAfter = 1;
                //this.uGridPMResult.DisplayLayout.Override.RowSpacingBefore = 2;
                this.uGridPMResult.DisplayLayout.Bands[0].Columns["EquipCode"].Layout.Override.FixedRowCellAppearance.BackColor = Color.Empty;
                //this.uGridPMResult.DisplayLayout.Override.FixedRowAppearance.BackColor = Color.Empty;
                //this.uGridPMResult.DisplayLayout.Override.FixedCellAppearance.BackColor = Color.Empty;
                //this.uGridPMResult.DisplayLayout.Override.FixedCellSeparatorColor = Color.Empty;
                this.uGridPMResult.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridPMResult.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                //this.uGridPMResult.DisplayLayout.Override.AllowColMoving = AllowColMoving.NotAllowed;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private Infragistics.Win.UltraWinEditors.DropDownEditorButtonBase DropDownEditorButtonControlConverter(Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                #region 필수입력사항체크

                //-----------------필수 입력사항 체크 -----------------//

                if (this.uDateSearchPlanYear.Value == null || this.uDateSearchPlanYear.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001066", Infragistics.Win.HAlign.Right);

                    this.uDateSearchPlanYear.DropDown();
                    return;
                }
                if (this.uDateSearchPlanYear.DateTime.Date > DateTime.Now.Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001472", Infragistics.Win.HAlign.Right);

                    this.uDateSearchPlanYear.DropDown();
                    return;
                }
                if (this.uComboSearchPlant.Value == null || this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uTextTechnician.Text == "" || this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001078", Infragistics.Win.HAlign.Right);

                    this.uTextTechnician.Focus();
                    return;
                }

                #endregion

                #region 검색조건저장
                //-------------------------값 저장--------------------------//

                string strDate = this.uDateSearchPlanYear.DateTime.Date.ToString("yyyy-MM-dd");
                string strPlanYear = strDate.Substring(0, 4);
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                string strTechnician = this.uTextTechnician.Text;
                string strStation = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboSearchEquipLargeType.Value.ToString();
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();

                string strArea = "";//this.uComboSearchArea.Value.ToString();
                string strGubun = "";//this.uComboSearchProcGubun.Value.ToString();                

                #endregion

                #region BL

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--BL호출--//
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                    clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                    brwChannel.mfCredentials(clsPMPlanD);
                }
                else
                    clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD(m_strDBConn);
                //처리 로직//

                DataTable dtPMResult = new DataTable();

                dtPMResult = clsPMPlanD.mfReadPMPlanDResult(strPlanYear, strDate, strPlantCode, strTechnician,strStation,strEquipLocCode,
                                                            strProcessGroup, strEquipLargeTypeCode, strEquipGroup, strArea, strGubun,
                                                            m_resSys.GetString("SYS_LANG"));

                this.uGridPMResult.DataSource = dtPMResult;
                this.uGridPMResult.DataBind();

                #endregion

                #region Message
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtPMResult.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    brwChannel.mfCredentials(clsRepairReq);

                    //dtPMResult = dtPMResult.DefaultView.ToTable(true, "EquipCode");
                    string strChk = "";
                    string strPackage = "";
                    //Color coRowcolor = new Color();

                    //Random objRandom = new Random();

                    #region Package,ModelName,RowColor

                    Color[] RowColor = new Color[2];
                    RowColor[0] = Color.GhostWhite;
                    RowColor[1] = Color.FromArgb(204,255,255);


                    /*정보가 있는 경우 for문을 돌면서 수치여부가 T 면 점검결과가 수정불가 수치수정가능,F면 점검결과가 수정,수치수정불가로 바뀜 그에 따라 색도 변함
                     * 만약 기존데이터가 있는 경우 색은 변하지 않음
                    */
                    int intcnt = 0;
                    for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                    {
                        //설비에대한패키지정보를조회를하여 값이 있고 그설비와 현재루프의 설비와 같다면 패키지정보를 넣는다.
                        if (!strChk.Equals(string.Empty) && strChk.Equals(this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString()))
                        {
                            this.uGridPMResult.Rows[i].Cells["PACKAGE"].Value = strPackage;
                            this.uGridPMResult.Rows[i].Cells["EquipCode"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMPeriodName"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMInspectCriteria"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMInspectName"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMMethod"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMWorkTime"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["Check"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["ImageName"].Appearance.BackColor = RowColor[intcnt];
                        }
                        //if (strChk.Equals(string.Empty) || !strChk.Equals(this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString()))
                        else
                        {
                            //설비에대한 패키지정보를 조회한다.
                            DataTable dtPackage = clsRepairReq.mfReadEquipRepair_MESDB_Detail(strPlantCode, this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));
                            if (dtPackage.Rows.Count > 0)
                            {
                                strPackage = dtPackage.Rows[0]["PACKAGE"].ToString();
                                this.uGridPMResult.Rows[i].Cells["PACKAGE"].Value = strPackage;
                            }
                            else
                            {
                                strPackage = "";
                                this.uGridPMResult.Rows[i].Cells["PACKAGE"].Value = string.Empty;
                            }
                            //int r = (int)(objRandom.NextDouble() * 255);
                            //int g = (int)(objRandom.NextDouble() * 255);
                            //int b = (int)(objRandom.NextDouble() * 255);

                            //coRowcolor = Color.FromArgb(r, g, b);
                            if (intcnt.Equals(1))
                                intcnt = 0;
                            else
                                intcnt++;

                            strChk = this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString();
                            this.uGridPMResult.Rows[i].Cells["EquipCode"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMPeriodName"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMInspectCriteria"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMInspectName"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMMethod"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["PMWorkTime"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["Check"].Appearance.BackColor = RowColor[intcnt];
                            this.uGridPMResult.Rows[i].Cells["ImageName"].Appearance.BackColor = RowColor[intcnt];
                        }
                        if (this.uGridPMResult.Rows[i].Cells["MeasureValueFlag"].Value.Equals("T"))
                        {
                            this.uGridPMResult.Rows[i].Cells["PMResultCode"].Activation = Activation.NoEdit;
                            
                            if (this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value.ToString().Equals(string.Empty))
                            {
                                this.uGridPMResult.Rows[i].Cells["PMResultValue"].Appearance.BackColor = Color.Salmon;
                            }
                        }
                        else
                        {
                            this.uGridPMResult.Rows[i].Cells["PMResultValue"].Activation = Activation.NoEdit;
                            if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString().Equals(string.Empty))
                            {
                                this.uGridPMResult.Rows[i].Cells["PMResultCode"].Appearance.BackColor = Color.Salmon;
                            }
                        }

                    }
                    //WinGrid grd = new WinGrid();
                    //grd.mfSetAutoResizeColWidth(this.uGridPMResult, 0);

                    #endregion

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            bool bolChk = false;
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMResult.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000882", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 필수 입력사항 체크

                //-----------------필수 입력사항 체크 -----------------//

                if (this.uDateSearchPlanYear.Value == null || this.uDateSearchPlanYear.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001066", Infragistics.Win.HAlign.Right);

                    this.uDateSearchPlanYear.DropDown();
                    return;
                }
                else if (this.uComboSearchPlant.Value ==null || this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                
                else if (this.uTextTechnician.Text == "" || this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001078", Infragistics.Win.HAlign.Right);

                    this.uTextTechnician.Focus();
                    return;
                }
                else if (this.uTextPMWorkerID.Text == "" || this.uTextPMWorkName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000102", Infragistics.Win.HAlign.Right);

                    this.uTextTechnician.Focus();
                    return;
                }
                #endregion


                #region 저장정보 저장

                //-------------------------값 저장--------------------------//
                
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                
                string strTechnician = this.uTextTechnician.Text;
                string strLang = m_resSys.GetString("SYS_LANG");

                //파일 경로 저장할 배열변수
                ArrayList arrFile = new ArrayList();
                ArrayList arrFileName = new ArrayList();
                //------------------------그리드 체크 및 값 저장 ------------------------//
                
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                brwChannel.mfCredentials(clsPMPlanD);

                DataTable dtPMResult = clsPMPlanD.mfSetPMPlanDColumn();

                //팝업창정보를 위한 컬럼생성
                dtPMResult.Columns.Add("EquipName", typeof(string));

                if (this.uGridPMResult.Rows.Count > 0)
                {
                    //-Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.-//
                    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                    {
                        if (this.uGridPMResult.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            #region 필수 입력사항

                            if (this.uGridPMResult.Rows[i].Cells["MeasureValueFlag"].Value.Equals("F") && this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001064", Infragistics.Win.HAlign.Right);

                                this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResultCode"];
                                this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            //수치입력여부가 T인데 수치가 입력안되어있을 경우 메세지
                            if (this.uGridPMResult.Rows[i].Cells["MeasureValueFlag"].Value.Equals("T") && this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001354", Infragistics.Win.HAlign.Right);

                                this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["MeasureValueFlag"];
                                this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            //if (this.uGridPMResult.Rows[i].Cells["PMResult"].Value.ToString() == "" && this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString().Equals(string.Empty))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                    , "확인창", "필수입력사항 확인", "점검결과를 선택해주세요", Infragistics.Win.HAlign.Right);

                            //    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResult"];
                            //    this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}
                            //else if (this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString() == "")
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                    , "확인창", "필수입력사항 확인", "교체여부를 선택해주세요", Infragistics.Win.HAlign.Right);

                            //    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["ChangeFlag"];
                            //    this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}
                            //else if (this.uGridPMResult.Rows[i].Cells["RepairFlag"].Value.ToString() == "")
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                    , "확인창", "필수입력사항 확인", "수리여부를 선택해주세요", Infragistics.Win.HAlign.Right);

                            //    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["RepairFlag"];
                            //    this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}

                            #endregion

                            if (this.uGridPMResult.Rows[i].GetCellValue("FilePath").ToString().Contains(":\\"))
                            {
                                FileInfo fil = new FileInfo(this.uGridPMResult.Rows[i].GetCellValue("FilePath").ToString());

                                //파일이름 설정
                                string strFileName = strPlantCode + "_" + this.uGridPMResult.Rows[i].Cells["Seq"].Value.ToString() + "_" + fil.Name;
                                string strFil = fil.DirectoryName + "\\";

                                

                                //파일 정보 배열 변수에 지정
                                arrFile.Add(strFil + fil.Name);  // 원본 파일 경로
                                arrFileName.Add(strFil + strFileName); //이름변경 파일 경로
                                

                                DataRow drResult;
                                drResult = dtPMResult.NewRow();
                                drResult["EquipCode"] = this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString();         //설비
                                drResult["EquipName"] = this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString();         //팝업창정보를 위한 컬럼정보
                                drResult["Package"] = this.uGridPMResult.Rows[i].Cells["PACKAGE"].Value.ToString();             //Package
                                drResult["Seq"] = this.uGridPMResult.Rows[i].Cells["Seq"].Value.ToString();                     //순번
                                drResult["PMPlanDate"] = this.uGridPMResult.Rows[i].Cells["PMPlanDate"].Value.ToString();       //점검일
                                drResult["PMResultCode"] = this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString();   //점검결과
                                drResult["PMResultValue"] = this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value.ToString().Trim(); //수치
                                drResult["PMWorkDate"] = this.uGridPMResult.Rows[i].Cells["PMPlanDate"].Value.ToString();       //점검결과일
                                drResult["PMWorkTime"] = Convert.ToDateTime(this.uGridPMResult.Rows[i].Cells["PMWorkTime"].Value).ToString("HH:mm:ss");       //점검시간
                                drResult["PMWorkDesc"] = this.uGridPMResult.Rows[i].Cells["PMWorkDesc"].Value.ToString().Trim();       //점검기타사항
                                drResult["ChangeFlag"] = this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString();       //교체여부
                                drResult["RepairFlag"] = this.uGridPMResult.Rows[i].Cells["RepairFlag"].Value.ToString();       //수리여부
                                drResult["FilePath"] = strFileName;           //첨부파일


                                dtPMResult.Rows.Add(drResult);

                            }
                            else
                            {
                                DataRow drResult;
                                drResult = dtPMResult.NewRow();
                                drResult["EquipCode"] = this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString();         //설비
                                drResult["EquipName"] = this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString();         //팝업창정보를 위한 컬럼정보
                                drResult["Package"] = this.uGridPMResult.Rows[i].Cells["PACKAGE"].Value.ToString();             //Package
                                drResult["Seq"] = this.uGridPMResult.Rows[i].Cells["Seq"].Value.ToString();                     //순번
                                drResult["PMPlanDate"] = this.uGridPMResult.Rows[i].Cells["PMPlanDate"].Value.ToString();       //점검일
                                drResult["PMResultCode"] = this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString();   //점검결과
                                drResult["PMResultValue"] = this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value.ToString().Trim(); //수치
                                drResult["PMWorkDate"] = this.uGridPMResult.Rows[i].Cells["PMPlanDate"].Value.ToString();       //점검결과일
                                drResult["PMWorkTime"] = Convert.ToDateTime(this.uGridPMResult.Rows[i].Cells["PMWorkTime"].Value).ToString("HH:mm:ss");       //점검시간
                                drResult["PMWorkDesc"] = this.uGridPMResult.Rows[i].Cells["PMWorkDesc"].Value.ToString().Trim();       //점검기타사항
                                drResult["ChangeFlag"] = this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString();       //교체여부
                                drResult["RepairFlag"] = this.uGridPMResult.Rows[i].Cells["RepairFlag"].Value.ToString();       //수리여부
                                drResult["FilePath"] = this.uGridPMResult.Rows[i].Cells["FilePath"].Value.ToString();       //첨부파일

                                dtPMResult.Rows.Add(drResult);

                                //점검결과가 수정되지 않았을 경우 기존 점검결과코드 저장
                                //if (this.uGridPMResult.Rows[i].Cells["PMResult"].Value.ToString().Equals(string.Empty))
                                //{
                                //    drResult["PMResultCode"] = this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString();
                                //}
                                ////점검결과가 수정되었을 경우 수정된 점검결과 코드 저장
                                //else
                                //{
                                //    drResult["PMResultCode"] = this.uGridPMResult.Rows[i].Cells["PMResult"].Value.ToString();
                                //}
                            }
                        }
                    }
                }

                #endregion

                if (dtPMResult.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M000882", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }
                string strDate = this.uGridPMResult.Rows[0].Cells["PMPlanDate"].Value.ToString();
                string strPlanYear = strDate.Substring(0, 4);
                string strWorkerID = this.uTextPMWorkerID.Text;
               
                #region Test
                ////////교체여부가 교체함이 있을 경우 팝업창을 띄운다.
                //////if (dtPMResult.Select("ChangeFlag = 'T'").Count() > 0)
                //////{
                //////    //팝업창으로 정보를 보낼 테이블과 컬럼설정
                //////    DataTable dtCEquip = new DataTable();
                //////    dtCEquip.Columns.Add("EquipCode", typeof(string));
                //////    dtCEquip.Columns.Add("EquipName", typeof(string));

                //////    DataTable dtDetail = new DataTable();
                //////    dtDetail.Columns.Add("PlantCode", typeof(string));
                //////    dtDetail.Columns.Add("PlanYear", typeof(string));
                //////    dtDetail.Columns.Add("PMPlanDate", typeof(string));

                //////    DataRow[] drChange;
                //////    //교체여부가 교체함인 정보만 복사하여 DataRow에 넣은 후 테이블에 복사한다.
                //////    drChange = dtPMResult.Select("ChangeFlag = 'T'");
                //////    dtCEquip = drChange.CopyToDataTable();

                //////    // 중복 설비가 있으면 Merge
                //////    dtCEquip = dtCEquip.DefaultView.ToTable(true, "EquipCode", "EquipName");

                //////    //공장 ,계획년도 , 점검일 저장
                //////    DataRow drInfo = dtDetail.NewRow();
                //////    drInfo["PlantCode"] = strPlantCode;
                //////    drInfo["PlanYear"] = strPlanYear;
                //////    drInfo["PMPlanDate"] = strDate;
                //////    dtDetail.Rows.Add(drInfo);

                //////    frmEQU0005D1 D1 = new frmEQU0005D1(dtCEquip, dtDetail);
                //////    D1.ShowDialog();
                //////}
                #endregion

                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsPMPlanD.mfSavePMPlanDResult(strPlantCode, strPlanYear, strWorkerID, dtPMResult, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    //------커서 변경-----//
                    this.MdiParent.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    bolChk = true;
                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {

                        #region 파일 업로드

                        //파일 경로가 있는 경우
                        if (arrFileName.Count > 0)
                        {

                            #region 파일 경로 첨부파일 경로 BL
                            //파일 경로 가져오기 
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAcce);

                            DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                            //첨부파일 경로 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);

                            DataTable dtSysFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0029");

                            #endregion

                            for (int i = 0; i < arrFile.Count; i++)
                            {
                                
                                //변경한 화일이 있으면 삭제하기
                                if (File.Exists(arrFileName[i].ToString()))
                                    File.Delete(arrFileName[i].ToString());

                                //변경된파일이름으로 복사하기
                                File.Copy(arrFile[i].ToString(), arrFileName[i].ToString());
                            }

                            //Upload정보 설정
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            fileAtt.mfInitSetSystemFileInfo(arrFileName, "", dtSysAcce.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtSysFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtSysFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAcce.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAcce.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();
                        }

                        #endregion


                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M001037", "M000930",
                                                        Infragistics.Win.HAlign.Right);

                        #region 교체팝업창

                        //////교체여부가 교체함이 있을 경우 팝업창을 띄운다.
                        ////if (dtPMResult.Select("ChangeFlag = 'T'").Count() > 0)
                        ////{
                        ////    //팝업창으로 정보를 보낼 테이블과 컬럼설정
                        ////    DataTable dtCEquip = new DataTable();
                        ////    dtCEquip.Columns.Add("EquipCode", typeof(string));
                        ////    dtCEquip.Columns.Add("EquipName", typeof(string));

                        ////    DataTable dtDetail = new DataTable();
                        ////    dtDetail.Columns.Add("PlantCode", typeof(string));
                        ////    dtDetail.Columns.Add("PlanYear", typeof(string));
                        ////    dtDetail.Columns.Add("PMPlanDate", typeof(string));

                        ////    DataRow[] drChange;
                        ////    //교체여부가 교체함인 정보만 복사하여 DataRow에 넣은 후 테이블에 복사한다.
                        ////    drChange = dtPMResult.Select("ChangeFlag = 'T'");
                        ////    dtCEquip = drChange.CopyToDataTable();

                        ////    // 중복 설비가 있으면 Merge
                        ////    dtCEquip = dtCEquip.DefaultView.ToTable(true, "EquipCode", "EquipName");

                        ////    //공장 ,계획년도 , 점검일 저장
                        ////    DataRow drInfo = dtDetail.NewRow();
                        ////    drInfo["PlantCode"] = strPlantCode;
                        ////    drInfo["PlanYear"] = strPlanYear;
                        ////    drInfo["PMPlanDate"] = strDate;
                        ////    dtDetail.Rows.Add(drInfo);

                        ////    frmEQU0005D1 D1 = new frmEQU0005D1(dtCEquip, dtDetail);
                        ////    D1.ShowDialog();
                        ////}
                        
                        #endregion


                        mfSearch();
                    }
                    else
                    {
                        string strErrMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strErrMes = msg.GetMessge_Text("M000953",strLang);
                        else
                            strErrMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning,m_resSys.GetString("SYS_FONTNAME") ,500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strErrMes,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                if (!bolChk)
                {
                    //------커서 변경-----//
                    this.MdiParent.Cursor = Cursors.Default;
                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }

                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //-----------------필수 입력사항 체크 -----------------//

                if (this.uDateSearchPlanYear.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001066", Infragistics.Win.HAlign.Right);

                    this.uDateSearchPlanYear.DropDown();
                    return;
                }
                else if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                
                else if (this.uTextTechnician.Text == "" || this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001078", Infragistics.Win.HAlign.Right);

                    this.uTextTechnician.Focus();
                    return;
                }

                //-------------------------값 저장--------------------------//
                string strDate = this.uDateSearchPlanYear.DateTime.Date.ToString("yyyy-MM-dd");
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();
                string strArea = this.uComboSearchArea.Value.ToString();
                string strGubun = this.uComboSearchProcGubun.Value.ToString();
                string strStation = this.uComboSearchStation.Value.ToString();
                string strTechnician = this.uTextTechnician.Text;
                
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //Systme ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                if (this.uGridPMResult.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001173", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                //if (this.uGridPMResult.Rows.Count > 0)
                grd.mfDownLoadGridToExcel(this.uGridPMResult);

                /////////////

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        #region GridEvent

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
         private void uGridPMResult_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMResult, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }

                #region 입력사항 공백시 편집이미지 제거

                //교체여부를 교체안함이나 공백일시 다른 필수입력사항정보확인, 그 정보가 공백이고 편집이미지가 있을 경우 편집이미지를 없앤다.
                if (e.Cell.Column.Key.Equals("ChangeFlag"))
                {
                    if ((e.Cell.Value.ToString().Equals(string.Empty) || e.Cell.Value.ToString().Equals("F"))
                        && e.Cell.Row.Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.RowSelectorAppearance.Image != null)
                    {
                        e.Cell.Row.RowSelectorAppearance.Image = null;
                    }
                }

                //점검결과의 정보가 공백일 경우 다른 필수입력사항 정보확인, 그 정보가 공백이고 편집이미지가 있을 경우에 편집이미지를 없앤다.
                if (e.Cell.Column.Key.Equals("PMResultCode"))
                {
                    if (e.Cell.Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty)
                        && (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("F") || e.Cell.Row.Cells["ChangeFlag"].Value.Equals(string.Empty)))
                    {
                        if (e.Cell.Row.RowSelectorAppearance.Image != null)
                        {
                            e.Cell.Row.RowSelectorAppearance.Image = null;
                        }
                    }

                    //점검결과가 입력가능 한상태에서 공백일 경우 색을 입히고 정보가 있을 경우 색을 지운다.
                    if (e.Cell.Row.Cells["MeasureValueFlag"].Value.Equals("F"))
                    {
                        if (e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Appearance.BackColor.Equals(Color.Salmon))
                        {
                            e.Cell.Appearance.BackColor = Color.Salmon;
                        }
                        if (!e.Cell.Value.ToString().Equals(string.Empty) && e.Cell.Appearance.BackColor.Equals(Color.Salmon))
                        {
                            e.Cell.Appearance.BackColor = Color.White ;
                        }
                    }
                }

                //수치컬럼이 입력된 상태
                if (e.Cell.Column.Key.Equals("PMResultValue"))
                {
                    if (e.Cell.Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty)
                        && (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("F") || e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals(string.Empty)))
                    {
                        if (e.Cell.Row.RowSelectorAppearance.Image != null)
                        {
                            e.Cell.Row.RowSelectorAppearance.Image = null;
                        }
                    }

                    //수치가 입력가능 한상태에서 공백일 경우 색을 입히고 정보가 있을 경우 색을 지운다.
                    if (e.Cell.Row.Cells["MeasureValueFlag"].Value.ToString().Equals("T"))
                    {
                        if (e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Appearance.BackColor.Equals(Color.Salmon))
                        {
                            e.Cell.Appearance.BackColor = Color.Salmon;
                        }
                        if (!e.Cell.Value.ToString().Equals(string.Empty) && e.Cell.Appearance.BackColor.Equals(Color.Salmon))
                        {
                            e.Cell.Appearance.BackColor = Color.White;
                        }
                    }
                }

                //교체여부를 교체안함이나 공백일시 다른 필수입력사항정보확인, 그 정보가 공백이고 편집이미지가 있을 경우 편집이미지를 없앤다.
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    if ((e.Cell.Value.ToString().Equals(string.Empty))
                        && e.Cell.Row.Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                        && e.Cell.Row.Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                        && (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("F") || e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals(string.Empty))
                        && e.Cell.Row.RowSelectorAppearance.Image != null)
                    {
                        e.Cell.Row.RowSelectorAppearance.Image = null;
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

         //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGridPMResult_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!e.Cell.Column.Key.Equals("Check"))
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
                //// 컬럼의 DataType이 bool이고 체크박스가 아닌것
                //if (e.Cell.Column.DataType == typeof(bool) && e.Cell.Column.Style != Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
                //{
                //    if (selectedCell == null)
                //    {
                //        selectedCell = e.Cell;
                //    }
                //    //  선택되어 있지 않은 라디오 버튼인 경우
                //    if (!Convert.ToBoolean(e.Cell.Value))
                //        this.SetSelectedCell(e.Cell);
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();

            }
            finally
            {
            }
        }       

        //그리드 리스트 클릭시 PMResultName 텍스트, 해당 점검결과의 교체여부가 T일경우 교체여부 교체함으로 바뀜 삽입
        private void uGridPMResult_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //선택한 리스트의 Value와 Text 저장
                string strValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                string strText = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex).ToString();

                //컬럼이 PMResult 경우 실행
                if (e.Cell.Column.Key.Equals("PMResultCode"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //선택한 리스트의 Value가 공백이아니면 선택한 리스트의 텍스트를 PMResultName에 삽입
                    if (!strValue.Equals(string.Empty))
                    {
                        e.Cell.Row.Cells["PMResultName"].Value = strText;
                    }
                    //공백일 경우 PMResultName 공백처리
                    else
                    {
                        e.Cell.Row.Cells["PMResultName"].Value = "";
                    }

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                    if (!strValue.Equals(string.Empty))
                    {
                        //점검결과등록정보BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                        QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                        brwChannel.mfCredentials(clsPMResultType);

                        //점검결과등록정보 상세정보 조회매서드 실행
                        DataTable dtResultType = clsPMResultType.mfReadPMResultType_Detail(strPlantCode, strValue, m_resSys.GetString("SYS_LANG"));

                        //정보가 있으면 교체여부를 확인한다. (교체여부가 T 인 경우 교체여부콤보를 교체함으로 바뀌준다.)
                        if (dtResultType.Rows.Count > 0)
                        {
                            if (dtResultType.Rows[0]["ChangeFlag"].ToString().Equals("T"))
                                e.Cell.Row.Cells["ChangeFlag"].Value = "T";

                            if (dtResultType.Rows[0]["ChangeFlag"].ToString().Equals("F")
                                && (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("T") || e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals(string.Empty)))
                            {
                                e.Cell.Row.Cells["ChangeFlag"].Value = "F";
                            }

                        }
                    }
                    //선택한 정보가 공백일 경우 교체여부 교체안함으로 바꿈
                    else
                    {
                        if (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("T"))
                            e.Cell.Row.Cells["ChangeFlag"].Value = "F";
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();

            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridPMResult_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (this.uGridPMResult.ActiveCell == null)
                    return;

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {

                    //첨부파일 삭제   
                    if (this.uGridPMResult.ActiveCell.Column.Key.Equals("FilePath"))
                    {
                        string strPlantCode = this.uGridPMResult.ActiveCell.Row.Cells["PlantCode"].Value.ToString();
                        if (this.uGridPMResult.ActiveCell.Text.Contains(this.uGridPMResult.ActiveCell.Row.Cells["PlantCode"].Value.ToString()) &&
                                this.uGridPMResult.ActiveCell.Text.Contains(this.uGridPMResult.ActiveCell.Row.Cells["Seq"].Value.ToString()))
                        {

                            string strEquipCode = this.uGridPMResult.ActiveCell.Row.Cells["EquipCode"].Value.ToString();
                            string strSeq = this.uGridPMResult.ActiveCell.Row.Cells["Seq"].Value.ToString();
                            string strYear = this.uGridPMResult.ActiveCell.Row.Cells["PMPlanDate"].Value.ToString().Substring(0, 4);
                            //화일서버 연결정보 가져오기
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                            //첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0029");

                            //첨부파일 삭제하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridPMResult.ActiveCell.Text);

                            fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                , arrFile
                                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.mfFileUploadNoProgView();

                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                            QRPEQU.BL.EQUMGM.PMPlanD clsD = new QRPEQU.BL.EQUMGM.PMPlanD();
                            brwChannel.mfCredentials(clsD);

                            string strErrRtn = clsD.mfDeletePMPlanDFile_D(strPlantCode, strYear, strEquipCode, strSeq);

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                        }

                        this.uGridPMResult.ActiveCell.Value = string.Empty;

                    }
                }

                ////int intRow = this.uGridPMResult.ActiveCell.Row.Index;
                ////int intColumn = this.uGridPMResult.ActiveCell.Column.Index;

                //if (e.KeyData.Equals(Keys.Right) || e.KeyData.Equals(Keys.Tab))
                //{
                //    if (!this.uGridPMResult.ActiveCell.Column.Key.Equals("PMWorkDesc"))
                //    {
                //        this.uGridPMResult.PerformAction(UltraGridAction.BelowCell);
                //        this.uGridPMResult.PerformAction(UltraGridAction.EnterEditMode);
                //    }
                //}

                //if (e.KeyData.Equals(Keys.Left))
                //{
                //    if (!this.uGridPMResult.ActiveCell.Column.Key.Equals("EquipCode"))
                //    {
                //        this.uGridPMResult.PerformAction(UltraGridAction.AboveCell);
                //    }
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 필수 함수
        private void SetSelectedCell(UltraGridCell cell)
        {
            //#region 해당 row의 모든 라디오 버튼 해제
            //if (this.selectedCell != null)
            //{
            //    int r = cell.Row.Index;
            //    foreach (UltraGridCell aCell in this.uGridPMResult.ActiveRow.Cells)
            //    {
            //        // 컬럼의 DataType이 bool이고 체크박스가 아닌것만 해제
            //        if (aCell.Column.DataType == typeof(bool) && aCell.Column.Style != Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
            //        {
            //            aCell.Value = false;
            //        }
            //    }

            //    cell.Value = true;
            //}
            //#endregion

            //#region 선택한 라디오 버튼 설정
            //this.selectedCell = cell;
            //this.selectedCell.Value = true;
            //#endregion
        }

        //그리드안의 버튼 클릭
        private void uGridPMResult_ClickCellButton(object sender, CellEventArgs e)
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region 점검결과,교체여부 변경
                if (e.Cell.Column.Key.Equals("Button"))
                {
                    //필요정보 코드 저장
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    string strResultCode = e.Cell.Row.Cells["PMResultCode"].Value.ToString();
                    string strChangeFlag = e.Cell.Row.Cells["ChangeFlag"].Value.ToString();

                    //교체여부저장변수
                    string strResultCh = "";
                    if (!strResultCode.Equals(string.Empty))
                    {
                        //점검결과등록정보BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                        QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                        brwChannel.mfCredentials(clsPMResultType);

                        //점검결과등록정보 상세정보 조회매서드 실행
                        DataTable dtResultType = clsPMResultType.mfReadPMResultType_Detail(strPlantCode, strResultCode, m_resSys.GetString("SYS_LANG"));

                        if (dtResultType.Rows.Count > 0)
                        {
                            strResultCh = dtResultType.Rows[0]["ChangeFlag"].ToString();
                        }
                    }
                    //!strResultCode.Equals(string.Empty)&& 

                    if (e.Cell.Row.Cells["PMResultCode"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit))
                    {
                        QRPGlobal grdImg = new QRPGlobal();

                        //for문을 돌면서 같은설비의 점검결과를 똑같이 바꾼다.
                        for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                        {
                            //현재 선택한 버튼의 줄을 제외하고 같은 설비코드인 셀을 찾는다.
                            if (!i.Equals(e.Cell.Row.Index)
                                && this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString().Equals(strEquipCode))
                            {
                                //점검결과가 장기다운인 경우 점검결과 수치상관없이 자동으로채워준다.
                                if (strResultCode.Equals("ZO"))
                                {
                                    //점검결과콤보가 수정가능이면 점검결과콤보에 채워줌 (나중에 바뀔수도있음)
                                    if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit))
                                        this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value = strResultCode;
                                    else
                                        this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value = "장기다운";

                                    this.uGridPMResult.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                                }

                                // 점검결과가 수정가능한 셀을 찾아낸다.
                                if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                    && i > e.Cell.Row.Index)
                                {
                                    //점검결과코드가 공백이아니고 편집이미지가 널인 경우
                                    if (!this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                                        && this.uGridPMResult.Rows[i].RowSelectorAppearance.Image == null)
                                    {
                                    }
                                    else
                                    {
                                        
                                        //바꿀 내용이 공백이고 나머지 교체여부가 F고 비고가 공백일 경우 이미지를 널처리한다.
                                        if (strResultCode.Equals(string.Empty))
                                        {
                                            if (this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals("T"))
                                                this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value = "F";

                                            //비고,첨부파일이 공백일 경우 이미지 널처리
                                            if (this.uGridPMResult.Rows[i].Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty) 
                                                && this.uGridPMResult.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty))
                                                this.uGridPMResult.Rows[i].RowSelectorAppearance.Image = null;

                                            this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value = strResultCode;
                                        }
                                        else
                                        {
                                            //점검결과코드의 교체여부가 교체 일경우 교체여부를 바꿔준다.
                                            if (strResultCh.Equals("T")
                                                && (this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals(string.Empty)
                                                || this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals("F")))
                                            {
                                                this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value = "T";
                                            }
                                            else
                                            {
                                                this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value = "F";
                                            }

                                            //this.uGridPMResult.Rows[i].Cells["PMWorkTime"].Value = ResultTime;
                                            this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value = strResultCode;
                                            this.uGridPMResult.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                                        }

                                    }
                                }
                            }
                        }

                    }
                }

                #endregion

                #region 동일한설비의 점검시간 동일하게 변경

                //점검시간이 널이아니고 정보가 있는경우 같은 설비의 점검시간을 이벤트가 발생한 점검시간으로 바꾼다.
                if (e.Cell.Column.Key.Equals("BtnTime")
                    && e.Cell.Row.Cells["PMWorkTime"].Value != null
                    && !e.Cell.Row.Cells["PMWorkTime"].Value.ToString().Equals(string.Empty))
                {

                    //필요정보 코드 저장
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    string strResultCode = e.Cell.Row.Cells["PMResultCode"].Value.ToString();
                    string strChangeFlag = e.Cell.Row.Cells["ChangeFlag"].Value.ToString();
                    DateTime ResultTime = Convert.ToDateTime(e.Cell.Row.Cells["PMWorkTime"].Value);

                    for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                    {
                        //이벤트가 발생한 줄을 제외시키고 같은설비의 줄의 점검시간을 바꾼다.
                        if (!i.Equals(e.Cell.Row.Index)
                            && i > e.Cell.Row.Index
                            && this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString().Equals(strEquipCode))
                        {
                            this.uGridPMResult.Rows[i].Cells["PMWorkTime"].Value = ResultTime;

                            //점검결과,수치,교체여부,점검특이사항이 입력되어있지않다면 편집이미지를 지운다.
                            if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMResult.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMResult.Rows[i].Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                                && (this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals(string.Empty) || this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals("F")))
                            {
                                this.uGridPMResult.Rows[i].RowSelectorAppearance.Image = null;
                            }


                        }
                    }
                    if (e.Cell.Row.Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                                    && e.Cell.Row.Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                                    && e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty)
                                    && e.Cell.Row.Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                                    && (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals(string.Empty) || e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("F")))
                    {
                        e.Cell.Row.RowSelectorAppearance.Image = null;
                    }

                }

                #endregion

                #region 수치입력변경

                if (e.Cell.Column.Key.Equals("BtnValue") && e.Cell.Row.Cells["PMResultValue"].Activation.Equals(Activation.AllowEdit))
                {
                    
                    //필요정보 코드 저장
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    //string strChangeFlag = e.Cell.Row.Cells["ChangeFlag"].Value.ToString();
                    string strReturnValue = this.uGridPMResult.ActiveRow.Cells["PMResultValue"].Text;

                    for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                    {
                        //이벤트가 발생한 줄을 제외시키고 같은설비의 줄의 수치를 바꾼다.
                        if (!i.Equals(e.Cell.Row.Index)
                            && i > e.Cell.Row.Index
                            && this.uGridPMResult.Rows[i].Cells["EquipCode"].Value.ToString().Equals(strEquipCode)
                            && this.uGridPMResult.Rows[i].Cells["PMResultValue"].Activation.Equals(Activation.AllowEdit))
                        {
                            this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value = strReturnValue;

                            //점검결과,수치,교체여부,점검특이사항이 입력되어있지않다면 편집이미지를 지운다.
                            if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMResult.Rows[i].Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMResult.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMResult.Rows[i].Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                                && (this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals(string.Empty) || this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString().Equals("F")))
                            {
                                this.uGridPMResult.Rows[i].RowSelectorAppearance.Image = null;
                            }
                            else
                            {
                                this.uGridPMResult.Rows[i].RowSelectorAppearance.Image = SysRes.ModifyCellImage;
                            }


                        }
                    }

                    if (strReturnValue.Equals(string.Empty)
                                    && e.Cell.Row.Cells["PMResultValue"].Value.ToString().Equals(string.Empty)
                                    && e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty)
                                    && e.Cell.Row.Cells["PMWorkDesc"].Value.ToString().Equals(string.Empty)
                                    && (e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals(string.Empty) || e.Cell.Row.Cells["ChangeFlag"].Value.ToString().Equals("F")))
                    {
                        e.Cell.Row.RowSelectorAppearance.Image = null;
                    }
                }

                #endregion

                #region 첨부파일
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    if (e.Cell.Text.Contains(this.uGridPMResult.ActiveCell.Row.Cells["PlantCode"].Value.ToString()) &&
                                e.Cell.Text.Contains(this.uGridPMResult.ActiveCell.Row.Cells["Seq"].Value.ToString()))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001150", "M001465", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*"; //"Image files (*.bmp, *.jpg, *.gif)|*.bmp;*.jpg;*gif"; //"Image files(*.jpg,*.jpeg,*jpe,*.jfif,*.bmp,*.dib,*.gif,*.tif,*.tiff,*.png)|*.jpg,*.jpeg,*jpe,*.jfif,*.bmp,*.dib,*.gif,*.tif,*.tiff,*.png";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        //선택된 파일명에 특수문자 사용여부
                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            //첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);
                            return;
                        }

                        if (strImageFile.Contains("\\\\"))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            //업로드가 불가능한 파일입니다. <br/> 다시 선택해주세요.
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001053", "M001452", "M001513", Infragistics.Win.HAlign.Right);
                            return;
                        }
                        e.Cell.Value = strImageFile;

                        this.uGridPMResult.PerformAction(UltraGridAction.ExitEditMode);
                        QRPGlobal grdImg = new QRPGlobal();
                        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        
                    }
                    
                }
                #endregion


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수치입력셀을 더블 클릭 시 수치입력 팝업창이 뜬다.// 첨부파일 다운로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridPMResult_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("PMResultValue") 
                    && e.Cell.Activation.Equals(Activation.AllowEdit))
                {
                    frmEQU0005_P1 frmValue = new frmEQU0005_P1();
                    frmValue.ShowDialog();
                    if(frmValue.Number != null)
                        e.Cell.Value = frmValue.Number;
                }

                //점검항목에 등록되어 있는 첨부파일 다운로드
                if (e.Cell.Column.Key.Equals("ImageName"))
                 {
                     if (e.Cell.Row.Cells["ImageFile"].Value.ToString().Contains(":\\") || e.Cell.Row.Cells["ImageFile"].Value.ToString().Equals(string.Empty))
                         return;
                     
                     System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                     saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                     saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                     saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                     saveFolder.Description = "Download Folder";

                     string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                     if (saveFolder.ShowDialog() == DialogResult.OK)
                     {
                         string strSaveFolder = saveFolder.SelectedPath + "\\";

                         //화일서버 연결정보 가져오기
                         brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                         QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                         brwChannel.mfCredentials(clsSysAccess);
                         DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                         //설비점검정보 첨부파일 저장경로정보 가져오기
                         brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                         QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                         brwChannel.mfCredentials(clsSysFilePath);
                         DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0023");


                         frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                         ArrayList arrFile = new ArrayList();

                         arrFile.Add(e.Cell.Row.Cells["ImageFile"].Value.ToString());
                         

                         //Upload정보 설정
                         fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                dtSysAccess.Rows[0]["AccessPassword"].ToString());
                         fileAtt.ShowDialog();

                         // 파일 실행시키기
                         System.Diagnostics.Process.Start(strSaveFolder + e.Cell.Row.Cells["ImageFile"].Value.ToString());
                     }
                 }

                //PM체크 등록 업로드한 첨부파일 다운로드
                if (e.Cell.Column.Key.Equals("FilePath")) //경로수정필요
                {
                    if (e.Cell.Row.Cells["FilePath"].Value.ToString().Contains(":\\") || e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty))
                        return;

                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                        //설비점검정보 첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0029");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        arrFile.Add(e.Cell.Row.Cells["FilePath"].Value.ToString());


                        //Upload정보 설정
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strSaveFolder + e.Cell.Row.Cells["FilePath"].Value.ToString());
                    }
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        #region ComboEvent

        //공장 선택 시 설비 그룹 Area Station 공정구분 콤보박스 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {

                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    //콤보 아이템 클리어
                    //this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();
                    DeleteRow();

                    
                    //언어저장
                    string strLang = m_resSys.GetString("SYS_LANG");

                    ////------------Area콤보 변화-----------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);

                    //DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, strLang);

                    //wCom.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체",
                    //    "AreaCode", "AreaName", dtArea);

                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체",
                        "StationCode", "StationName", dtStation);

                    ////-----------설비공정구분콤보 변화 -------------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    //brwChannel.mfCredentials(clsEquipProcGubun);

                    //DataTable dtGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, strLang);

                    //wCom.mfSetComboEditor(this.uComboSearchProcGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    //    "", "", "전체", "EquipProcGubunCode", "EquipProcGubunName", dtGubun);

                    

                    //------점검결과 콤보 변화 -------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                    QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                    brwChannel.mfCredentials(clsPMResult);

                    DataTable dtResult = clsPMResult.mfReadPMResultTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    WinGrid wGrd = new WinGrid();
                    wGrd.mfSetGridColumnValueList(this.uGridPMResult, 0, "PMResult", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtResult);

                    
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        //Area 선택 전 설비그룹 선택여부 체크
        private void uComboSearchArea_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uComboSearchArea.Value.ToString() != "")
                {
                    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                    {
                       

                        this.uComboSearchEquipGroup.Value = "";
                        DeleteRow();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station 선택 시위치,설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchEquipLoc.Items.Clear();
                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치변경시 설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                
                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류 콤보선택시 설비 중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비중분류 선택시 설비 그룹이변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        //공정구분 선택 전 설비그룹 선택여부 체크
        private void uComboSearchProcGubun_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uComboSearchProcGubun.Value.ToString() != "")
                {
                    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                    {
                        this.uComboSearchEquipGroup.Value = "";
                        DeleteRow();
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 점검일이 바뀔경우 리플레쉬
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uDateSearchPlanYear_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //점검일이 널과 공백이아니고,그리드가 널이아니고 내용이 있는 경우 해당
                if (this.uDateSearchPlanYear.Value != null 
                    && !this.uDateSearchPlanYear.Value.ToString().Equals(string.Empty)
                    && this.uGridPMResult != null 
                    && this.uGridPMResult.Rows.Count > 0)
                {
                    int intCnt = this.uDateSearchPlanYear.DateTime.Date.ToString("yyyy-MM-dd").Length;
                    if (intCnt.Equals(10) && this.uDateSearchPlanYear.DateTime.Date > this.uDateSearchPlanYear.MinDate)
                    {
                        mfSearch();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion


        #region TextEvent

        //정비사 버튼 클릭 시 유저 정보 보여주기
        private void uTextTechnician_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 유저팝업창에 보낸다
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                //해당 텍스트에 삽입
                this.uTextTechnician.Text = frmUser.UserID;
                this.uTextTechnicianName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 ID입력 후 엔터누를 시 이름 자동입력
        private void uTextTechnician_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    
                        this.uTextTechnicianName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextTechnician.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000798", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextTechnician.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000798", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextTechnicianName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextTechnicianName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTechnician_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextTechnicianName.Text.Equals(string.Empty))
                this.uTextTechnicianName.Clear();
        }

        private void uTextPMWorkerID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextPMWorkName.Text.Equals(string.Empty))
                this.uTextPMWorkName.Clear();
        }

        private void uTextPMWorkerID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {

                    this.uTextPMWorkName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextPMWorkerID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000798", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextPMWorkerID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000798", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextPMWorkName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextPMWorkName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextPMWorkerID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 유저팝업창에 보낸다
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                //해당 텍스트에 삽입
                this.uTextPMWorkerID.Text = frmUser.UserID;
                this.uTextPMWorkName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        private void frmEQU0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            //try
            //{
            //    QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //    grd.mfSaveGridColumnProperty(this);
            //}
            //catch (Exception ex)
            //{
            //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            //    frmErr.ShowDialog();
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// 행 삭제
        /// </summary>
        private void DeleteRow()
        {
            try
            {
                //그리드 행삭제
                while (this.uGridPMResult.Rows.Count > 0)
                {
                    this.uGridPMResult.Rows[0].Delete(false);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 파일다운로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonFileDwon_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridPMResult.Rows[i].Hidden == false && this.uGridPMResult.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridPMResult.Rows[i].Cells["Check"].Value) == true)
                        //|| (this.uGridPMResult.Rows[i].Hidden == false && this.uGridPMResult.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridPMResult.Rows[i].Cells["Check"].Value) == true))
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비점검정보 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0023");


                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                    {
                        if (this.uGridPMResult.Rows[i].Hidden == false && this.uGridPMResult.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridPMResult.Rows[i].Cells["Check"].Value) == true)
                            arrFile.Add(this.uGridPMResult.Rows[i].Cells["ImageFile"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        //특수문자 저장시 0byte파일만 전송되는 오류 특수문자필터
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }









        ////private void uGridPMResult_KeyUp(object sender, KeyEventArgs e)
        ////{
        ////    try
        ////    {

        ////        //// 스페이스 바가 아닌경우 리턴
        ////        //if (e.KeyData != Keys.Space)
        ////        //    return;

        ////        //Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = this.uGridPMResult.ActiveCell;
        ////        //if (activeCell == null)
        ////        //    return;

        ////        //// 컬럼의 DataType이 bool이고 체크박스가 아닌것 그리고 선택되어 있지 않은 라디오 버튼인 경우
        ////        //if (activeCell.Column.DataType == typeof(bool) && activeCell.Column.Style != Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox &&
        ////        //    ((bool)activeCell.Value) == false)
        ////        //{
        ////        //    this.SetSelectedCell(activeCell);
        ////        //}
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////    }
        ////    finally
        ////    {

        ////    }
        ////}
        

    }
}
