﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0028.cs                                        */
/* 프로그램명   : 월간 정지유형 현황조회                                */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-01-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;
using Microsoft.VisualBasic;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0029 : Form,IToolbar
    {
        //ResourceInfo
        QRPGlobal SysRes = new QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0029()
        {
          
            InitializeComponent();
        }


        private void frmEQUZ0029_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.MdiParent, true, false, false, false, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        private void frmEQUZ0029_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
        private void frmEQUZ0029_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitComboBox();
            InitLabel();
            InitGrid();

            //Chart초기화
            QRPSTA.STASPC clsSTASPC = new QRPSTA.STASPC();
            clsSTASPC.mfInitControlChart(this.uChartG);
            clsSTASPC.mfInitControlChart(this.uChartC);

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화

        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("월간 정지유형 현황조회", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();


                //설비보전계획표그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridTotal, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼설정


                grd.mfSetGridColumn(this.uGridTotal, 0, "EquipCode", "설비번호", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridTotal, 0, "ReasonName", "다운", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                

                #region 날짜 컬럼
                string strMonth = string.Empty;
                switch (m_resSys.GetString("SYS_LANG"))
                {
                    case "CHN" :
                        strMonth = "月";
                        break;
                    case "ENG" :
                        strMonth = "M";
                        break;
                    default :
                        strMonth = "월";
                        break;
                }

                for (int i = 0; i < 12; i++)
                {
                    string strKey = "";
                    string strValue = "";
                    if (i < 9)
                    {
                        strKey = "D0" + (i + 1).ToString();
                        strValue = (i + 1).ToString() + strMonth;
                    }
                    else
                    {
                        strKey = "D" + (i + 1).ToString();
                        strValue = (i + 1).ToString() + strMonth;
                    }
                    grd.mfSetGridColumn(this.uGridTotal, 0, strKey, strValue, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                }
                #endregion

                
                //컬럼이동
                this.uGridTotal.DisplayLayout.Override.AllowRowLayoutColMoving = Infragistics.Win.Layout.GridBagLayoutAllowMoving.AllowOriginXChange;
                //폰트 설정
                this.uGridTotal.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridTotal.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDate, "기간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUesr, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDownCode, "다운코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelQRP, "전체/QRP", m_resSys.GetString("SYS_FONTNAME"), true, true);

                //lbl.mfSetLabel(this.uLabelAll, "전체건수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelWait, "미완료 건수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel, "실시율", m_resSys.GetString("SYS_FONTNAME"), true, false);
                this.uOptionSelect.Value = 1;
                this.uOptionSelect.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);


                #region Date

                //검색조건의 기간 콤보 설정
                ArrayList MonthKey = new ArrayList();
                ArrayList MonthValue = new ArrayList();
                ArrayList YearKey = new ArrayList();
                ArrayList YearValue = new ArrayList();
                int intCnt = 2011;

                for (int i = 1; i <= 12; i++)
                {
                    if (intCnt < 2021)
                    {
                        YearKey.Add(intCnt);
                        YearValue.Add(intCnt.ToString());
                        intCnt++;
                    }
                }

                ArrayList QRPKey = new ArrayList();
                ArrayList QRPValue = new ArrayList();
                int intQRP = 1;

                QRPKey.Add(1);
                QRPValue.Add("Total");

                QRPKey.Add(2);
                QRPValue.Add("QRP");



                wCombo.mfSetComboEditor(this.uComboSearchYear, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", YearKey, YearValue);

                

                wCombo.mfSetComboEditor(this.uComboQRP, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", QRPKey, QRPValue);

                #endregion

                // Date ComboBox 기본값 설정
                DateTime DateNow = DateTime.Now;
                this.uComboSearchYear.Value = DateNow.Year;
                this.uComboQRP.Value = 1;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region ToolBar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                

                #region 필수입력사항

                if (this.uComboSearchYear.Value == null
                   || this.uComboSearchYear.Value.ToString().Equals(string.Empty)
                   || !Information.IsNumeric(this.uComboSearchYear.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000208", Infragistics.Win.HAlign.Right);

                    this.uComboSearchYear.DropDown();
                    return;
                }

                
                if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000128", Infragistics.Win.HAlign.Right);

                    this.uComboSearchStation.DropDown();
                    return;
                }

                if (this.uComboQRP.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                              , "M001264", "M001230", "M001063", Infragistics.Win.HAlign.Right);

                    this.uComboQRP.DropDown();
                    return;
                }
                #endregion 

                //검색조건의 정보를 저장한다.
                string strYear = this.uComboSearchYear.Value.ToString();
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strStation = this.uComboSearchStation.Value.ToString();
                string strEquipLoc = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboSearchEquipLargeType.Value.ToString();
                string strEquipEquipGroup = this.uComboSearchEquipGroup.Value.ToString();
                string strDownCode = this.uComboDownCode.Value == null ? "" : this.uComboDownCode.Value.ToString();
                //string strQRPChk = this.uOptionSelect.Value.ToString();
                string strQRPChk = this.uComboQRP.Value.ToString();
                string strUserID = "";

                if(!this.uTextUserID.Text.ToString().Equals(string.Empty) && !this.uTextUserName.Text.Equals(string.Empty))
                    strUserID = this.uTextUserID.Text;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.RepairDIS), "RepairDIS");
                QRPEQU.BL.EQUDIS.RepairDIS clsRepairDIS = new QRPEQU.BL.EQUDIS.RepairDIS();
                brwChannel.mfCredentials(clsRepairDIS);

                //월간정지유형정보조회매서드실행
                DataTable dtInfo = clsRepairDIS.mfReadRepairReq_Year(strPlantCode, strYear, strStation, strEquipLoc,
                    strProcessGroup, strEquipLargeTypeCode, strEquipEquipGroup, strDownCode, strQRPChk, strUserID, m_resSys.GetString("SYS_LANG"));


                this.uGridTotal.DataSource = dtInfo;
                this.uGridTotal.DataBind();

                for (int i = 0; i < this.uGridTotal.Rows.Count; i++)
                {
                    if (this.uGridTotal.Rows[i].Cells["ReasonName"].Value.ToString().Equals("ZTotal"))
                    {
                        //this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                        this.uGridTotal.Rows[i].Cells["ReasonName"].Value = "Total";
                        this.uGridTotal.Rows[i].Appearance.BackColor = Color.Pink;
                        this.uGridTotal.Rows[i].Cells["ReasonName"].Appearance.BackColor = Color.Pink;
                    }

                    if (this.uGridTotal.Rows[i].Cells["EquipCode"].Value.ToString().Equals("Z_GrandTotal"))
                    {
                        //this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                        this.uGridTotal.Rows[i].Cells["EquipCode"].Value = "Total";
                        this.uGridTotal.Rows[i].Appearance.BackColor = Color.Pink;
                        this.uGridTotal.Rows[i].Cells["EquipCode"].Appearance.BackColor = Color.DarkSlateBlue;
                    }

                    //if (!this.uGridUser.Rows[j].Cells[k].Value.ToString().Equals(string.Empty)
                    //    && this.uGridUser.Rows[j].Cells[k].Value.ToString().Equals(dtPMChk.Rows[i]["UserName"].ToString()))
                    //{
                    //    if (!dtPMChk.Rows[i]["PMUserName"].ToString().Equals(string.Empty))
                    //        this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.Salmon;
                    //    else
                    //        this.uGridUser.Rows[j].Cells[k].Appearance.BackColor = Color.White;
                    //}
                }


                if (dtInfo.Rows.Count > 0 && dtInfo.Select("EquipCode = 'Z_GrandTotal' AND ReasonName <> 'ZTotal'").Count() > 0)
                {
                    DataRow[] dr = dtInfo.Select("EquipCode = 'Z_GrandTotal' AND ReasonName <> 'ZTotal'");
                    DataTable dt = dr.CopyToDataTable();
                    dt.Columns["EquipCode"].ColumnName = "Total";
                    dt.Columns.Remove("Total");


                    BindChart(dt);

                }

                 this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                 /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtInfo.Rows.Count == 0)
                {
                    //Chart초기화
                    QRPSTA.STASPC clsSTASPC = new QRPSTA.STASPC();
                    clsSTASPC.mfInitControlChart(this.uChartG);
                    clsSTASPC.mfInitControlChart(this.uChartC);

                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        { }
        public void mfCreate()
        { }
        public void mfPrint()
        { }
        public void mfExcel()
        { }
        public void mfDelete()
        { }

        #endregion


        #region Combo

        //공장 선택 시 Station 콤보박스 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {

                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    //콤보 아이템 클리어
                    //this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();

                    //언어저장
                    string strLang = m_resSys.GetString("SYS_LANG");


                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택",
                        "StationCode", "StationName", dtStation);



                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station 선택 시위치,설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchEquipLoc.Items.Clear();
                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치변경시 설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류 콤보선택시 설비 중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비중분류 선택시 설비 그룹이변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion


        #region Text


        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            if (this.uTextUserName.Text.Equals(string.Empty))
                this.uTextUserName.Clear();
        }

        /// <summary>
        /// 직접입력조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete)) && !this.uTextUserName.Text.Equals(string.Empty))
                {
                    this.uTextUserName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextUserID.Text.Equals(string.Empty))
                {//System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //검색정보 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strUserID = this.uTextUserID.Text;

                    // 공장정보 확인
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //유저정보 조회매서드 실행
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    //정보가 있을 경우 뿌려주고, 없으면메세지를 출력한다.
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000883",Infragistics.Win.HAlign.Right);
                        this.uTextUserName.Clear();

                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 버튼클릭조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboSearchPlant.Value.ToString();

                frmUser.ShowDialog();


                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void BindChart(DataTable dtTotal)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //Chart의 타입을 설정한다.
                this.uChartG.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.StackColumnChart;
                //X축과 Y축을 바꾸어준다
                this.uChartG.Data.SwapRowsAndColumns = true;

                ////루프를 돌며 컬럼의 명을 바꾼다.
                for (int i = 0; i < dtTotal.Columns.Count; i++)
                {
                    dtTotal.Columns[i].ColumnName = dtTotal.Columns[i].ColumnName.Replace("D", "");
                }

                //차트의 범례를 설정한다.
                this.uChartG.Legend.Visible = true;
                this.uChartG.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Right;
                this.uChartG.Legend.Margins.Left = 0;
                this.uChartG.Legend.Margins.Right = 0;
                this.uChartG.Legend.Margins.Top = 0;
                this.uChartG.Legend.SpanPercentage = 15; //범례의 폭
                this.uChartG.Legend.Margins.Bottom = 25; //아래의 공백

                //X축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisX = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisX.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                axisX.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                axisX.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;   // 라인으로 그리는 경우 ContinuousData, 막대로 그리는 경우 GroupBySeries

                axisX.Labels.ItemFormatString = "<ITEM_LABEL>";
                axisX.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
                axisX.Labels.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                axisX.Extent = 20;

                this.uChartG.TitleBottom.Text = "";
                this.uChartG.TitleBottom.HorizontalAlign = StringAlignment.Center;
                this.uChartG.TitleBottom.VerticalAlign = StringAlignment.Center;
                this.uChartG.TitleBottom.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                this.uChartG.TitleBottom.Margins.Left = 0;
                this.uChartG.TitleBottom.Margins.Right = 0;
                this.uChartG.TitleBottom.Margins.Top = 1;
                this.uChartG.TitleBottom.Margins.Bottom = 1;
                this.uChartG.TitleBottom.Visible = true;

                ////Y축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisY = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisY.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                axisY.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                axisY.Labels.ItemFormatString = "<DATA_VALUE:#>";
                axisY.Labels.HorizontalAlign = StringAlignment.Far;
                axisY.TickmarkInterval = 1;
                axisY.Labels.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                axisY.Extent = 20;

                this.uChartG.TitleLeft.Text = "";
                this.uChartG.TitleLeft.HorizontalAlign = StringAlignment.Center;
                this.uChartG.TitleLeft.VerticalAlign = StringAlignment.Center;
                this.uChartG.TitleLeft.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                this.uChartG.TitleLeft.Margins.Left = 5;
                this.uChartG.TitleLeft.Margins.Right = 5;
                this.uChartG.TitleLeft.Margins.Top = 0;
                this.uChartG.TitleLeft.Margins.Bottom = 0;
                this.uChartG.TitleLeft.Visible = true;
                this.uChartG.LineChart.HighLightLines = true;

                Color[] colors = new Color[11];
                Random ran = new Random();
                for (int i = 0; i < 11; i++)
                {
                    int r = (int)(ran.NextDouble() * 255);
                    int g = (int)(ran.NextDouble() * 255);
                    int b = (int)(ran.NextDouble() * 255);
                    colors[i] = Color.FromArgb(r, g, b);

                }
                //    colors[0] = Color.FromArgb(255, 255, 204);
                //colors[1] = Color.FromArgb(204,255,204);
                //colors[2] = Color.FromArgb(255,153,255);
                //colors[3] = Color.FromArgb(255,204,102);
                //colors[4] = Color.FromArgb(204,102,255);
                //colors[5] = Color.FromArgb(102, 153, 255);
                //colors[6] = Color.FromArgb(204,153,204);
                //colors[7] = Color.FromArgb(153, 204, 051);
                //colors[8] = Color.FromArgb(204, 102, 000);
                //colors[9] = Color.FromArgb(000, 153, 255);
                //colors[10] = Color.FromArgb(153, 204, 204);

                this.uChartG.ColorModel.CustomPalette = colors;

                this.uChartG.Axis.X = axisX;
                this.uChartG.Axis.Y = axisY;

                this.uChartG.Tooltips.Display = Infragistics.UltraChart.Shared.Styles.TooltipDisplay.MouseMove;
                this.uChartG.Tooltips.Format = Infragistics.UltraChart.Shared.Styles.TooltipStyle.LabelPlusDataValue;

                //설정된 차트에 데이터를 바인드한다.
                this.uChartG.DataSource = dtTotal;
                this.uChartG.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
