﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_S03.
    /// </summary>
    partial class rptEQUZ0002_S03
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptEQUZ0002_S03));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textPkgType = new DataDynamics.ActiveReports.TextBox();
            this.textSerialNo = new DataDynamics.ActiveReports.TextBox();
            this.textMaker = new DataDynamics.ActiveReports.TextBox();
            this.textModelName = new DataDynamics.ActiveReports.TextBox();
            this.textEquipName = new DataDynamics.ActiveReports.TextBox();
            this.textProc = new DataDynamics.ActiveReports.TextBox();
            this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
            this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            this.txtWorkStandard = new DataDynamics.ActiveReports.TextBox();
            this.label11 = new DataDynamics.ActiveReports.Label();
            this.label12 = new DataDynamics.ActiveReports.Label();
            this.textTechSrandard = new DataDynamics.ActiveReports.TextBox();
            this.textContents = new DataDynamics.ActiveReports.TextBox();
            this.txtEquipStandard = new DataDynamics.ActiveReports.TextBox();
            this.label9 = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.label1 = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.textPkgType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMaker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textModelName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textProc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkStandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTechSrandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textContents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipStandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textPkgType,
            this.textSerialNo,
            this.textMaker,
            this.textModelName,
            this.textEquipName,
            this.textProc});
            this.detail.Height = 0.3020833F;
            this.detail.Name = "detail";
            // 
            // textPkgType
            // 
            this.textPkgType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPkgType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPkgType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPkgType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textPkgType.DataField = "Package";
            this.textPkgType.Height = 0.3F;
            this.textPkgType.Left = 5.531F;
            this.textPkgType.Name = "textPkgType";
            this.textPkgType.Style = "font-size: 8pt; vertical-align: middle";
            this.textPkgType.Text = null;
            this.textPkgType.Top = -1.862645E-09F;
            this.textPkgType.Width = 1.656F;
            // 
            // textSerialNo
            // 
            this.textSerialNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.DataField = "SerialNo";
            this.textSerialNo.Height = 0.3F;
            this.textSerialNo.Left = 4.489F;
            this.textSerialNo.Name = "textSerialNo";
            this.textSerialNo.Style = "font-size: 8pt; vertical-align: middle";
            this.textSerialNo.Text = null;
            this.textSerialNo.Top = -1.862645E-09F;
            this.textSerialNo.Width = 1.041999F;
            // 
            // textMaker
            // 
            this.textMaker.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.DataField = "VendorCode";
            this.textMaker.Height = 0.3F;
            this.textMaker.Left = 3.374F;
            this.textMaker.Name = "textMaker";
            this.textMaker.Style = "font-size: 8pt; vertical-align: middle";
            this.textMaker.Text = null;
            this.textMaker.Top = -1.862645E-09F;
            this.textMaker.Width = 1.115F;
            // 
            // textModelName
            // 
            this.textModelName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModelName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModelName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModelName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModelName.DataField = "ModelName";
            this.textModelName.Height = 0.3F;
            this.textModelName.Left = 2.29F;
            this.textModelName.Name = "textModelName";
            this.textModelName.Style = "font-size: 8pt; vertical-align: middle";
            this.textModelName.Text = null;
            this.textModelName.Top = -1.862645E-09F;
            this.textModelName.Width = 1.084F;
            // 
            // textEquipName
            // 
            this.textEquipName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEquipName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEquipName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEquipName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEquipName.DataField = "EquipName";
            this.textEquipName.Height = 0.3F;
            this.textEquipName.Left = 1.096F;
            this.textEquipName.Name = "textEquipName";
            this.textEquipName.Style = "font-size: 8pt; vertical-align: middle";
            this.textEquipName.Text = null;
            this.textEquipName.Top = -1.862645E-09F;
            this.textEquipName.Width = 1.194F;
            // 
            // textProc
            // 
            this.textProc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textProc.DataField = "ReProcName";
            this.textProc.Height = 0.3F;
            this.textProc.Left = 0F;
            this.textProc.Name = "textProc";
            this.textProc.Style = "font-size: 8pt; vertical-align: middle";
            this.textProc.Text = null;
            this.textProc.Top = -1.862645E-09F;
            this.textProc.Width = 1.096F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtWorkStandard,
            this.label11,
            this.label12,
            this.textTechSrandard,
            this.textContents,
            this.txtEquipStandard,
            this.label9,
            this.label2,
            this.label1});
            this.groupFooter1.Height = 0.41F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // txtWorkStandard
            // 
            this.txtWorkStandard.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtWorkStandard.Height = 0.2F;
            this.txtWorkStandard.Left = 3.503F;
            this.txtWorkStandard.Name = "txtWorkStandard";
            this.txtWorkStandard.Style = "text-align: center";
            this.txtWorkStandard.Text = null;
            this.txtWorkStandard.Top = 0.2F;
            this.txtWorkStandard.Width = 0.73F;
            // 
            // label11
            // 
            this.label11.Height = 0.2F;
            this.label11.HyperLink = null;
            this.label11.Left = 2.794F;
            this.label11.Name = "label11";
            this.label11.Style = "font-size: 9pt";
            this.label11.Text = "작업표준  :";
            this.label11.Top = 0.2F;
            this.label11.Width = 0.7090001F;
            // 
            // label12
            // 
            this.label12.Height = 0.2F;
            this.label12.HyperLink = null;
            this.label12.Left = 4.593999F;
            this.label12.Name = "label12";
            this.label12.Style = "font-size: 9pt";
            this.label12.Text = "기술표준  :";
            this.label12.Top = 0.2F;
            this.label12.Width = 0.6990002F;
            // 
            // textTechSrandard
            // 
            this.textTechSrandard.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textTechSrandard.Height = 0.2F;
            this.textTechSrandard.Left = 5.292999F;
            this.textTechSrandard.Name = "textTechSrandard";
            this.textTechSrandard.Style = "text-align: center";
            this.textTechSrandard.Text = null;
            this.textTechSrandard.Top = 0.196F;
            this.textTechSrandard.Width = 0.73F;
            // 
            // textContents
            // 
            this.textContents.Height = 0.2F;
            this.textContents.Left = 1F;
            this.textContents.Name = "textContents";
            this.textContents.Style = "font-size: 9pt; vertical-align: middle";
            this.textContents.Text = null;
            this.textContents.Top = 0.01F;
            this.textContents.Width = 5.908F;
            // 
            // txtEquipStandard
            // 
            this.txtEquipStandard.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEquipStandard.Height = 0.2F;
            this.txtEquipStandard.Left = 1.704F;
            this.txtEquipStandard.Name = "txtEquipStandard";
            this.txtEquipStandard.Style = "text-align: center";
            this.txtEquipStandard.Text = null;
            this.txtEquipStandard.Top = 0.196F;
            this.txtEquipStandard.Width = 0.73F;
            // 
            // label9
            // 
            this.label9.Height = 0.2F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.006F;
            this.label9.Name = "label9";
            this.label9.Style = "font-size: 9pt";
            this.label9.Text = "설비표준  :";
            this.label9.Top = 0.2F;
            this.label9.Width = 0.6980001F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.117587E-08F;
            this.label2.Name = "label2";
            this.label2.Style = "";
            this.label2.Text = "3 . 관련 표준 :";
            this.label2.Top = 0.2F;
            this.label2.Width = 1F;
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.Name = "label1";
            this.label1.Style = "";
            this.label1.Text = "2 . 목         적 : ";
            this.label1.Top = 0F;
            this.label1.Width = 1F;
            // 
            // rptEQUZ0002_S03
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.208F;
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textPkgType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMaker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textModelName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textProc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkStandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTechSrandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textContents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipStandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.TextBox textPkgType;
        private DataDynamics.ActiveReports.TextBox textSerialNo;
        private DataDynamics.ActiveReports.TextBox textMaker;
        private DataDynamics.ActiveReports.TextBox textModelName;
        private DataDynamics.ActiveReports.TextBox textEquipName;
        private DataDynamics.ActiveReports.TextBox textProc;
        private DataDynamics.ActiveReports.GroupHeader groupHeader1;
        private DataDynamics.ActiveReports.GroupFooter groupFooter1;
        private DataDynamics.ActiveReports.TextBox txtWorkStandard;
        private DataDynamics.ActiveReports.Label label11;
        private DataDynamics.ActiveReports.Label label12;
        private DataDynamics.ActiveReports.TextBox textTechSrandard;
        private DataDynamics.ActiveReports.TextBox textContents;
        private DataDynamics.ActiveReports.TextBox txtEquipStandard;
        private DataDynamics.ActiveReports.Label label9;
        private DataDynamics.ActiveReports.Label label2;
        private DataDynamics.ActiveReports.Label label1;
    }
}
