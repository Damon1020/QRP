﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0022.cs                                        */
/* 프로그램명   : 설비수리등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
namespace QRPEQU.UI
{
    public partial class frmEQUZ0022_S : Form,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        private string strRepairRequestCode;
        private string strRepairGICode;
        private DataTable m_dtPMResult;

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();


        private string RepairRequestCode
        {
            get { return strRepairRequestCode; }
            set { strRepairRequestCode = value; }
        }
        private string RepairGICode
        {
            get { return strRepairGICode; }
            set { strRepairGICode = value; }
        }

        public frmEQUZ0022_S()
        {
            InitializeComponent();
        }

        #region 컨트롤초기화

        private void frmEQUZ0022_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, false, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0022_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            //컨트롤초기화
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀설정
                titleArea.mfSetLabelText("설비상태관리",m_resSys.GetString("SYS_FONTNANME"),12);

                //버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonAccept, "접수", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCancelAccept, "의뢰취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonUseSP, "SP사용등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonChangeMold, "금형치공구교체", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonSaveResult, "결과등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCancelResult, "결과취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonRepair, "수리요청", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonRepairCancel, "요청취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonChg, "품종교체등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCCS, "CCS이동등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonChange, "치공구교체등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                this.uLabelNotice.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uLabelNotice.Appearance.FontData.SizeInPoints = 13;
                this.uLabelNotice.Appearance.FontData.Name = m_resSys.GetString("SYS_FONTNAME");
                this.uLabelNotice.Appearance.ForeColor = Color.Blue;

                //기본값설정
                //--수리요청자
                //uTextRepairReqID.Text = m_resSys.GetString("SYS_USERID");
                //uTextRepairReqName.Text = m_resSys.GetString("SYS_USERNAME");
                //--접수자
                //uTextReceiptID.Text = m_resSys.GetString("SYS_USERID");
                //uTextReceiptName.Text = m_resSys.GetString("SYS_USERNAME");
                ////--수리자
                //uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");
                //uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");

                uDateBreakDownTime.Value = DateTime.Now.ToString("HH:mm:ss");
                //uDateRepairStartTime.Value = DateTime.Now.ToString("HH:mm:ss");
                uDateRepairEndTime.Value = DateTime.Now.ToString("HH:mm:ss");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitValue()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                foreach (Control ctrl in uGroupBoxRequestRepair.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor) || ctrl.GetType() == typeof(System.Windows.Forms.RichTextBox))
                    {
                        ctrl.Text = string.Empty;
                    }
                }
                foreach (Control ctrl in uGroupBoxAcceptRepair.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor) || ctrl.GetType() == typeof(System.Windows.Forms.RichTextBox))
                    {
                        ctrl.Text = string.Empty;
                    }
                }
                foreach (Control ctrl in uGroupBoxRepairResult.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor) || ctrl.GetType() == typeof(System.Windows.Forms.RichTextBox))
                        
                    {
                        ctrl.Text = string.Empty;
                    }
                }

                ////uTextRepairReqID.Text = m_resSys.GetString("SYS_USERID");
                ////uTextReceiptID.Text = m_resSys.GetString("SYS_USERID");
                ////uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");

                ////uTextRepairReqName.Text = m_resSys.GetString("SYS_USERNAME");
                ////uTextReceiptName.Text = m_resSys.GetString("SYS_USERNAME");
                ////uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");

                uDateRepairReqDate.Value = DateTime.Now;
                uDateBreakDownDate.Value = DateTime.Now;
                uDateRepairDate.Value = DateTime.Now;

                uDateBreakDownTime.Value = DateTime.Now.ToString("HH:mm:ss");
                //uDateRepairStartTime.Value = DateTime.Now.ToString("HH:mm:ss");
                uDateRepairEndTime.Value = DateTime.Now.ToString("HH:mm:ss");

                //uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                uComboRepairResult.SelectedIndex = 0;

                uCheckHeadFlag.Enabled = true;

                uCheckCCSReqFlag.Checked = false;
                uCheckChangeFlag.Checked = false;
                uCheckHeadFlag.Checked = false;
                uCheckUrgentFlag.Checked = false;
                uCheckImportant.Checked = false;

                uTextHeadDesc.Enabled = false;
                uTextStageDesc.Enabled = false;

                RepairRequestCode = string.Empty;

                this.uTextFaultTypeCode.Tag = string.Empty;

                //if (m_dtPMResult != null && m_dtPMResult.Rows.Count > 0)
                //    m_dtPMResult.Clear();

                if (this.uCheckUrgentFlag.Enabled.Equals(false))
                    mfEnabledOff(this.uGroupBoxRequestRepair);
                if (this.uTextReceiptID.Enabled.Equals(false))
                    mfEnabledOff(this.uGroupBoxAcceptRepair);
                if (this.uTextRepairUserID.Enabled.Equals(false))
                    mfEnabledOff(this.uGroupBoxRepairResult);
                
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call Bl
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlant, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                   , "PlantCode", "PlantName", dtPlant);

                //공통코드정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                //공통코드 조회 매서드 실행
                DataTable dtState = clsCom.mfReadCommonCode("C0059", m_resSys.GetString("SYS_LANG"));

                //설비상태코드 콤보박스에 저장
                wCombo.mfSetComboEditor(this.uComboSearchEquipState, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "ComCode", "ComCodeName", dtState);



                //------------------ DownCode 콤보박스 ---------------//
                DataTable dtDown = new DataTable();
                dtDown.Columns.Add("Key", typeof(string));
                dtDown.Columns.Add("Values", typeof(string));


                DataRow drKey = dtDown.NewRow();
                drKey["Key"] = "'A','C','D','E'";
                drKey["Values"] = "A,C,D,E";
                dtDown.Rows.Add(drKey);

                drKey = dtDown.NewRow();
                drKey["Key"] = "'A','C','D','E','H'";
                drKey["Values"] = "A,C,D,E,H";
                dtDown.Rows.Add(drKey);



                wCombo.mfSetComboEditor(this.uComboSearchDown, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "Key", "Values", dtDown);


                dtPlant.Dispose();
                clsPlant.Dispose();

                dtState.Dispose();
                clsCom.Dispose();

                //공장콤보박스 편집불가처리
                this.uComboSearchPlant.ReadOnly = true;
                this.uComboPlant.ReadOnly = true;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelInspectEquipment, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelEquipmentProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipLoc, "설비위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipState, "설비상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipType, "설비Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDown, "DownCode", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipmentCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipmentName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairRequestDay, "수리요청일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelRepairRequestUser, "수리요청자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTroubleDate, "고장발생일시", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelGoods, "제품", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCarrier, "Container", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPKType, "PK Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRequestReason, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelAcceptDate, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelAcceptUser, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStage, "Stage", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelNote, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelRepairDate, "수리일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelRepairTime, "수리시간", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSCV, "수리자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelRepairResult, "수리결과", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel, "설비정지유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel1, "문제점 및 조치내용", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelDown, "Down Code", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                this.uLabelEquipmentName.Visible = false;
                this.uLabelRepairRequestDay.Visible = false;
                this.uDateRepairReqDate.Visible = false;
                this.uLabelRepairTime.Visible = false;
                this.uButtonCancelResult.Visible = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipRepair, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "RepairReqCode", "수리요청번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "EquipLoc", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "DOWNSTATUS", "접수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "ACCEPTFLAG", "접수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "EquipType", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "DieNo", "DIE_NO", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "DownCode", "DOWN CODE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "EquipState", "설비상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "EVENTCOMMENT", "EventComment", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "EtcDesc", "설명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "REQUESTTIME", "요청시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "ELAPSEDTIME", "경과시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "PACKAGE", "진행제품", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "UrgentFlag", "긴급처리", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "ModelName", "설비모델", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "ACTIONTYPE", "ActionType", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "MACHINERECIPENAME", "RecipeName", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipRepair, 0, "SUPERREASONCODE", "SUPERREASONCODE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                //grd.mfSetGridColumn(this.uGridEquipRepair, 0, "RepairReqDate", "수리요청일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 15
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridEquipRepair, 0, "RepairReqName", "수리요청자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 15
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridEquipRepair, 0, "BreakDownDate", "고장발생일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 15
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                

                //폰트설정
                uGridEquipRepair.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridEquipRepair.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //this.uGridEquipRepair.DisplayLayout.Bands[0].Columns["UrgentFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                //this.uGridEquipRepair.DisplayLayout.Bands[0].Columns["ACCEPTFLAG"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
               //RowSelectors 활성화 , RowSelector표시방법 줄번호
                this.uGridEquipRepair.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
                this.uGridEquipRepair.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.RowIndex;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBoxRequestRepair, GroupBoxType.INFO, "수리요청", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBoxAcceptRepair, GroupBoxType.INFO, "수리접수내역", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBoxRepairResult, GroupBoxType.INFO, "수리결과", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxRequestRepair.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxRequestRepair.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxAcceptRepair.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxAcceptRepair.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxRepairResult.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxRepairResult.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region Common Event
        public void mfSearch()
        {
            //uGroupBoxContentsArea.Expanded = false;
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            try
            {

                if (uComboSearchPlant.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.Focus();
                    return;
                }


                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strEquipLoc = this.uComboSearchEquipLoc.Value.ToString();
                string strEquipType = this.uComboSearchEquipType.Value.ToString();
                string strEquipState = this.uComboSearchEquipState.Value.ToString();
                string strEquipCode = this.uTextSearchEquipName.Text.Equals(string.Empty) ? "" : this.uTextSearchEquipCode.Text;
                //string strDownCode = "'A'"+","+"'C'"+","+"'D'"+","+"'E'"+","+"'H'";
                //string strDownCode = "'A','C','D','E','H'";
                string strDownCode = this.uComboSearchDown.Value.ToString();
                //for(int i= 0; i < this.uComboSearchDown.ValueList.CheckedItems.Count; i++)
                //{
                //    if (strDownCode.Equals(string.Empty))
                //        strDownCode = this.uComboSearchDown.ValueList.CheckedItems[i].DataValue.ToString();
                //    else
                //        strDownCode = strDownCode + "," + this.uComboSearchDown.ValueList.CheckedItems[i].DataValue.ToString();
                //}
                string strProcessGroupCode = this.uComboProcessGroup.Value.ToString();
                


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");


                //BL호출
                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                //QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
                //brwChannel.mfCredentials(clsEquip);

                //DataTable dtEquip = clsEquip.mfReadEquipRepair_MESDB(strEquipLoc, strEquipType, strEquipState, strEquipCode, m_resSys.GetString("SYS_LANG"));


                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.RepairDIS), "RepairDIS");
                QRPEQU.BL.EQUDIS.RepairDIS clsEquip = new QRPEQU.BL.EQUDIS.RepairDIS();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadRepair_MESDB(strPlantCode, strEquipLoc, strEquipType, strEquipState, strEquipCode, strProcessGroupCode, strDownCode, m_resSys.GetString("SYS_LANG"));
                /////////////
                ///////////// 
                uGridEquipRepair.Refresh();
                uGridEquipRepair.DataSource = dtEquip;
                uGridEquipRepair.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtEquip.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    for (int i = 0; i < this.uGridEquipRepair.Rows.Count; i++)
                    {
                        if (this.uGridEquipRepair.Rows[i].Cells["DOWNSTATUS"].Value.ToString().Equals("Request"))
                            this.uGridEquipRepair.Rows[i].Appearance.BackColor = Color.Salmon;

                        if (this.uGridEquipRepair.Rows[i].Cells["DOWNSTATUS"].Value.ToString().Equals("Accept"))
                            this.uGridEquipRepair.Rows[i].Appearance.BackColor = Color.Plum;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipRepair, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ToolBar Click Save -- 버튼클릭이벤트로 대체
        /// </summary>
        public void mfSave()
        {
        }

        /// <summary>
        /// ToolBar Click Delete -- 버튼클릭이벤트로 대체
        /// </summary>
        public void mfDelete()
        {
        }

        public void mfCreate()
        {
            try
            {
                InitValue();
                //if (!uGroupBoxContentsArea.Expanded)
                //{
                //    uGroupBoxContentsArea.Expanded = true;
                //}
                //else
                //{
                //    uGroupBoxContentsArea.Expanded = false;
                //}
                if(uLabelNotice.Visible)
                    mfVisible("", "");

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch { }
            finally { }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipRepair.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridEquipRepair);

                /////////////

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Grid Event
        private void uGridEquipRepair_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                mfCreate();

                //접수 상태에 따라 수리요청 수리접수내역 결과 등록 이 결정된다.
                //string strRequst = e.Cell.Row.Cells["DOWNSTATUS"].Value.ToString();
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //품종교체팝업창정보나 안내메세지 레이블이 활성화시 숨김처리
                if (this.uLabelNotice.Visible.Equals(true))
                {
                    mfVisible("", "");
                }

                //치공구교체버튼이 활성화되어있을 시 숨김처리
                if (this.uButtonChange.Visible)
                    this.uButtonChange.Visible = false;
                
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipRepair_MESDB_Detail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                if (dtEquip.Rows.Count != 0)
                {

                    if (dtEquip.Rows[0]["DOWNSTATUS"].ToString().Equals(string.Empty))
                    {
                        //수리요청 텍스트에 바인드.
                        this.uTextEquipCode.Text = e.Cell.Row.Cells["EquipCode"].Value.ToString();


                        this.uDateBreakDownDate.Value = DateTime.Now.Date;
                        this.uDateBreakDownTime.Value = DateTime.Now.ToString("HH:mm:ss");
                        mfEnabledOff(uGroupBoxRequestRepair);
                        mfEnabledOn("T");
                        mfEnabledOn("R");
                        if (this.uButtonChg.Visible)
                            mfVisible("u", "u");

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        return;
                    }
                    //CCS 의뢰상태인 경우 요청 불가처리 2012-10-06
                    if (dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    if (dtEquip.Rows[0]["DownCode"].ToString().Equals(string.Empty))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000046", "M000045", Infragistics.Win.HAlign.Right);
                        
                        return;
                    }


                    if (dtEquip.Rows[0]["DOWNSTATUS"].ToString() == "Request")
                    {
                        mfEnabledOff(uGroupBoxAcceptRepair);
                        mfEnabledOn("O");
                        mfEnabledOn("R");

                        //this.uTextRepairReqID.Text = dtEquip.Rows[0]["REQUESTUSER"].ToString();
                        //this.uTextProductCode.Text = dtEquip.Rows[0]["PRODUCTSPECNAME"].ToString();
                        //this.uTextDownCode.Text = dtEquip.Rows[0]["DownCode"].ToString();
                        //this.uTextRepairReqReason.Text = dtEquip.Rows[0]["EVENTCOMMENT"].ToString();
                        //this.uDateBreakDownDate.Value = Convert.ToDateTime(dtEquip.Rows[0]["REQUESTTIME"]).ToString("yyyy-MM-dd");
                        //this.uDateBreakDownTime.Value = Convert.ToDateTime(dtEquip.Rows[0]["REQUESTTIME"]).ToString("HH:mm:ss");

                        this.uTextReceiptDesc.Text = dtEquip.Rows[0]["EVENTCOMMENT"].ToString();
                    }

                    
                    //수리요청창
                    RepairRequestCode = dtEquip.Rows[0]["RepairReqCode"].ToString();
                    this.uTextEquipState.Text = dtEquip.Rows[0]["DOWNSTATUS"].ToString();
                    this.uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();
                    this.uTextRepairReqID.Text = dtEquip.Rows[0]["REQUESTUSER"].ToString();
                    this.uTextRepairReqName.Text = dtEquip.Rows[0]["REQUESTUSERNAME"].ToString();
                    this.uTextProductCode.Text = dtEquip.Rows[0]["REQUESTPRODUCTSPECNAME"].ToString();
                    this.uTextProductName.Text = dtEquip.Rows[0]["ProductName"].ToString();
                    this.uTextPKType.Text = dtEquip.Rows[0]["PackageGroupCode"].ToString();
                    this.uTextPackage.Text = dtEquip.Rows[0]["PackageA"].ToString();
                    this.uTextDownCode.Text = dtEquip.Rows[0]["DownCode"].ToString();
                    //this.uTextDownName.Text = dtEquip.Rows[0]["DownCode"].ToString();
                    this.uTextRepairReqReason.Text = dtEquip.Rows[0]["EVENTCOMMENT"].ToString();
                    this.uDateBreakDownDate.Value = dtEquip.Rows[0]["REQUESTTIME"].ToString() == "" ? "" : Convert.ToDateTime(dtEquip.Rows[0]["REQUESTTIME"]).ToString("yyyy-MM-dd");
                    this.uDateBreakDownTime.Value = dtEquip.Rows[0]["REQUESTTIME"].ToString() == "" ? "" : Convert.ToDateTime(dtEquip.Rows[0]["REQUESTTIME"]).ToString("HH:mm:ss");
                    this.uCheckUrgentFlag.Checked = dtEquip.Rows[0]["EMERGENCYFLAG"].ToString() == "Y" ? true : false;
                    this.uTextFaultTypeCode.Tag = dtEquip.Rows[0]["DownCode"].ToString().Substring(0,1);    //SUPERCODE

                    this.uCheckHeadFlag.Checked = dtEquip.Rows[0]["HeadFlag"].ToString() == "T" ? true : false;
                    this.uTextHeadDesc.Text = dtEquip.Rows[0]["HeadComments"].ToString();
                    this.uTextStageDesc.Text = dtEquip.Rows[0]["StageComments"].ToString();
                    


                    //접수상태가 수리접수까지 하였다면.
                    if (dtEquip.Rows[0]["DOWNSTATUS"].ToString() == "Accept")
                    {
                        mfEnabledOff(uGroupBoxRepairResult);
                        mfEnabledOn("O");
                        mfEnabledOn("T");

                        //접수코멘트에 정보가 없다면 MES에서 보내온 EVENTCOMMENT를 뿌려주고 있다면 접수코멘트를 뿌려줌
                        this.uTextReceiptDesc.Text = dtEquip.Rows[0]["Comments"].ToString() == "" ? dtEquip.Rows[0]["EVENTCOMMENT"].ToString() : dtEquip.Rows[0]["Comments"].ToString();

                        this.uTextReceiptID.Text = dtEquip.Rows[0]["ACCEPTUSER"].ToString();
                        this.uTextReceiptName.Text = dtEquip.Rows[0]["ACCEPTUSERNAME"].ToString();
                        this.uDateReceiptDate.Value = dtEquip.Rows[0]["ACCEPTTIME"].ToString();
                        this.uTextResultEquip.Text = dtEquip.Rows[0]["EquipCode"].ToString();
                        



                        mfVisible(dtEquip.Rows[0]["DownCode"].ToString().Substring(0, 1), dtEquip.Rows[0]["DOWNSTATUS"].ToString());




                    }

                    
                }

                //접수상태가 공백인경우 수리요청을 해야함.
                else
                {
                    //수리요청 텍스트에 바인드.
                    this.uTextEquipCode.Text = e.Cell.Row.Cells["EquipCode"].Value.ToString();


                    this.uDateBreakDownDate.Value = DateTime.Now.Date;
                    this.uDateBreakDownTime.Value = DateTime.Now.ToString("HH:mm:ss");
                    mfEnabledOff(uGroupBoxRequestRepair);
                    mfEnabledOn("T");
                    mfEnabledOn("R");
                    if (this.uButtonChg.Visible)
                        mfVisible("u", "u");
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

              

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                uGridEquipRepair.Rows[e.Cell.Row.Index].Fixed = false;
                this.MdiParent.Cursor = Cursors.Default;
                //uGroupBoxContentsArea.Expanded = false;
            }
            finally
            {
            }
        }

        #endregion

        #region SpinButton
        private void EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼 이벤트 감소 증가
                Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;

                if (ed.ReadOnly)
                    return;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                DateTime Temp = (DateTime)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    Temp = Temp.AddSeconds(1);
                    ed.Value = Temp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem )//&& intTemp > 0)
                {
                    Temp = Temp.AddSeconds(-1);
                    ed.Value = Temp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        private void EditorSpinButtonClick_1(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼 이벤트 감소 증가
                Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;

                if (ed.ReadOnly)
                    return;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                DateTime Temp = (DateTime)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    Temp = Temp.AddMinutes(1);
                    ed.Value = Temp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)//&& intTemp > 0)
                {
                    Temp = Temp.AddMinutes(-1);
                    ed.Value = Temp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        #endregion

        #region UI Event
        /// <summary>
        /// uCheckHeadFlag被选中则更改HEADDESC和Stage状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckHeadFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (uCheckHeadFlag.Checked == true)
            {
                uTextHeadDesc.Enabled = true;
                uTextStageDesc.Enabled = true;

                //BL호출
                //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsToolkit = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsToolkit);

                DataTable dtToolKit = clsToolkit.mfReadEQURepairReq_ToolValue(this.uTextEquipCode.Text.Trim());

                if (dtToolKit.Rows.Count != 0)
                {
                    uTextHeadDesc.Text = dtToolKit.Rows[0]["HEAD"].ToString();
                    uTextStageDesc.Text = dtToolKit.Rows[0]["STAGE"].ToString();
                }
                else
                {
                    uTextHeadDesc.Text = "";
                    uTextStageDesc.Text = "";
                }
            }
            else
            {
                uTextHeadDesc.Text = "";
                uTextStageDesc.Text = "";

                uTextHeadDesc.Enabled = false;
                uTextStageDesc.Enabled = false;
            }
 
        }

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                //if (uGroupBoxContentsArea.Expanded == false)
                //{
                //    Point point = new Point(0, 185);
                //    this.uGroupBoxContentsArea.Location = point;
                //    this.uGridEquipRepair.Height = 60;
                //}
                //else
                //{
                //    Point point = new Point(0, 825);
                //    this.uGroupBoxContentsArea.Location = point;
                //    this.uGridEquipRepair.Height = 720;
                //    for (int i = 0; i < uGridEquipRepair.Rows.Count; i++)
                //    {
                //        uGridEquipRepair.Rows[i].Fixed = false;
                //    }
                //}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검색-공장콤보 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinComboEditor wCombo = new WinComboEditor();

            try
            {
                if (uComboSearchPlant.Value.ToString() != string.Empty)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                    QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                    brwChannel.mfCredentials(clsPMResult);

                    DataTable dtPMResult = clsPMResult.mfReadPMResultTypeCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboRepairResult.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboRepairResult, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                        , "PMResultCode", "PMResultName", dtPMResult);

                   
                   
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtEquipLoc = clsLoc.mfReadLocationCombo(this.uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchEquipLoc.Items.Clear();

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "EquipLocCode", "EquipLocName", dtEquipLoc);

                    dtPMResult.Dispose();
                    clsPMResult.Dispose();

                    dtEquipLoc.Dispose();
                    clsLoc.Dispose();

                    //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    //Thread t1 = m_ProgressPopup.mfStartThread();
                    //m_ProgressPopup.mfOpenProgressPopup(this, "조회중...");
                    //this.MdiParent.Cursor = Cursors.WaitCursor;

                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);

                    //DataTable dtArea = clsArea.mfReadAreaCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    //uComboSearchArea.ValueList.Reset();
                    //wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "AreaCode", "AreaName", dtArea);

                    //dtArea.Dispose();
                    //clsArea.Dispose();

                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    //QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    //brwChannel.mfCredentials(clsStation);

                    //DataTable dtStation = clsStation.mfReadStationCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    //uComboSearchStation.ValueList.Reset();
                    //wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "StationCode", "StationName", dtStation);

                    //dtStation.Dispose();
                    //clsStation.Dispose();

                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    //brwChannel.mfCredentials(clsEquipProcGubun);

                    //DataTable dtEquipProcGugun = clsEquipProcGubun.mfReadProGubunCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    //uComboSearchEquipProcess.ValueList.Reset();
                    //wCombo.mfSetComboEditor(this.uComboSearchEquipProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "EquipProcGubunCode", "EquipProcGubunName", dtEquipProcGugun);

                    //dtEquipProcGugun.Dispose();
                    //clsEquipProcGubun.Dispose();

                    

                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    //QRPMAS.BL.MASEQU.EquipGroup clsGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    //brwChannel.mfCredentials(clsGroup);

                    //DataTable dtGroup = clsGroup.mfReadEquipGroupCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    //uComboEquipGroup.ValueList.Reset();
                    //wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "EquipGroupCode", "EquipGroupName", dtGroup);

                    //dtGroup.Dispose();
                    //clsGroup.Dispose();



                    //this.MdiParent.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    uComboSearchArea.ValueList.Reset();
                    uComboSearchEquipProcess.ValueList.Reset();
                    uComboSearchStation.ValueList.Reset();
                    uComboRepairResult.ValueList.Reset();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 위치가 선택이되면 위치에해당하는 설비유형정보로 변경된다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                WinComboEditor wCombo = new WinComboEditor();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtSearchCombo = clsEquip.mfReadEquip_EquipTypeCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),m_resSys.GetString("SYS_LANG"));

                this.uComboSearchEquipType.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "EquipTypeCode", "EquipTypeName", dtSearchCombo);

                dtSearchCombo.Clear();

                dtSearchCombo = clsEquip.mfReadEquip_ProcessCombo(this.uComboSearchPlant.Value.ToString(), "", this.uComboSearchEquipLoc.Value.ToString());

                wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessGroupCode", "ProcessGroupName", dtSearchCombo);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        /// <summary>
        /// 이미 수리요청이 들어갔는지 확인
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        private void mfCheckEquipOnRepair(string strPlantCode, string strEquipCode)
        {
            if (strEquipCode == string.Empty)
                return;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            DataTable dtEquip = new DataTable();

            brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
            QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
            brwChannel.mfCredentials(clsEquip);

            try
            {
                dtEquip = clsEquip.mfCheckEquipOnRepair(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count != 0)
                {
                    RepairRequestCode = dtEquip.Rows[0]["RepairReqCode"].ToString();

                    if (RepairRequestCode == null || RepairRequestCode.Equals(string.Empty))
                        return;

                    #region RepairRequest/Receipt/Result Detail

                    //BL호출
                    DataTable dtEquipDetail = clsEquip.mfReadEquipRepairTotal_Detail(RepairRequestCode, m_resSys.GetString("SYS_LANG"));

                    if (this.uComboPlant.Enabled.Equals(false))
                        mfEnabledOff(this.uGroupBoxRequestRepair);
                    if (this.uTextReceiptID.Enabled.Equals(false))
                        mfEnabledOff(this.uGroupBoxAcceptRepair);
                    if (this.uTextRepairUserID.Enabled.Equals(false))
                        mfEnabledOff(this.uGroupBoxRepairResult);

                    if (dtEquipDetail.Rows.Count != 0)
                    {
                        for (int i = 0; i < uComboPlant.Items.Count; i++)
                        {
                            uComboPlant.SelectedIndex = i;
                            if (uComboPlant.Value.ToString() == dtEquipDetail.Rows[0]["PlantCode"].ToString())
                            {
                                uComboPlant.SelectedIndex = i;
                                break;
                            }
                        }


                        RepairGICode = dtEquipDetail.Rows[0]["RepairGICode"].ToString();
                        uTextEquipCode.Text = dtEquipDetail.Rows[0]["EquipCode"].ToString();
                        uTextEquipName.Text = dtEquipDetail.Rows[0]["EquipName"].ToString();
                        uDateRepairReqDate.Value = dtEquipDetail.Rows[0]["RepairReqDate"].ToString();
                        uTextRepairReqID.Text = dtEquipDetail.Rows[0]["RepairReqID"].ToString();
                        uTextRepairReqName.Text = dtEquipDetail.Rows[0]["RepairReqName"].ToString();

                        uDateBreakDownDate.Value = dtEquipDetail.Rows[0]["BreakDownDate"].ToString();
                        uDateBreakDownTime.Value = dtEquipDetail.Rows[0]["BreakDownTime"].ToString();
                        uTextProductCode.Text = dtEquipDetail.Rows[0]["ProductCode"].ToString();
                        uTextProductName.Text = dtEquipDetail.Rows[0]["ProductName"].ToString();
                        uCheckUrgentFlag.Checked = Convert.ToBoolean(dtEquipDetail.Rows[0]["UrgentFlag"]);
                        uTextRepairReqReason.Text = dtEquipDetail.Rows[0]["RepairReqReason"].ToString();

                        if (!dtEquipDetail.Rows[0]["ReceiptID"].ToString().Equals(string.Empty))
                        {
                            uDateReceiptDate.Value = dtEquipDetail.Rows[0]["ReceiptDate"].ToString();
                            uTextReceiptID.Text = dtEquipDetail.Rows[0]["ReceiptID"].ToString();
                            uTextReceiptName.Text = dtEquipDetail.Rows[0]["ReceiptName"].ToString();
                            uCheckHeadFlag.Checked = Convert.ToBoolean(dtEquipDetail.Rows[0]["HeadFlag"]);
                            uTextStageDesc.Text = dtEquipDetail.Rows[0]["StageDesc"].ToString();
                            uTextReceiptDesc.Text = dtEquipDetail.Rows[0]["ReceiptDesc"].ToString();
                            mfEnabledOn("O");
                            mfEnabledOn("T");
                        }
                        else
                            mfEnabledOn("O");

                        if (!dtEquipDetail.Rows[0]["RepairID"].ToString().Equals(string.Empty))
                        {
                            uDateRepairDate.Value = dtEquipDetail.Rows[0]["RepairStartDate"].ToString();
                            //uDateRepairStartTime.Value = dtEquipDetail.Rows[0]["RepairStartTime"].ToString();
                            uDateRepairEndTime.Value = dtEquipDetail.Rows[0]["RepairEndTime"].ToString();
                            uTextRepairUserID.Text = dtEquipDetail.Rows[0]["RepairID"].ToString();
                            uTextRepairUserName.Text = dtEquipDetail.Rows[0]["RepairName"].ToString();

                            uCheckChangeFlag.Checked = Convert.ToBoolean(dtEquipDetail.Rows[0]["ChangeFlag"]);

                            for (int i = 0; i < uComboRepairResult.Items.Count; i++)
                            {
                                uComboRepairResult.SelectedIndex = i;
                                if (uComboRepairResult.Value.ToString() == dtEquipDetail.Rows[0]["RepairResultCode"].ToString())
                                {
                                    uComboRepairResult.SelectedIndex = i;
                                    break;
                                }
                            }
                            uTextRepairDesc.Text = dtEquipDetail.Rows[0]["RepairDesc"].ToString();
                            this.uTextFaultTypeCode.Text = dtEquipDetail.Rows[0]["FaultTypeCode"].ToString();
                            this.uTextFaultTypeName.Text = dtEquipDetail.Rows[0]["FaultTypeName"].ToString();
                            mfEnabledOn("R");
                        }


                        
                        //uGroupBoxContentsArea.Expanded = true;
                    }
                    else
                    {
                        mfEnabledOn("T");
                        mfEnabledOn("R");
                    }
                    #endregion
                    

                    
                }

                //Equip.Dispose();
                dtEquip.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                clsEquip.Dispose();
            }
        }

        /// <summary>
        /// 공장콤보 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                foreach (Control ctrl in uGroupBoxRequestRepair.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                    {
                        ctrl.Text = string.Empty;
                    }
                }
                foreach (Control ctrl in uGroupBoxAcceptRepair.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                    {
                        ctrl.Text = string.Empty;
                    }
                }
                foreach (Control ctrl in uGroupBoxRepairResult.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                    {
                        ctrl.Text = string.Empty;
                    }
                }

                //uTextRepairReqID.Text = m_resSys.GetString("SYS_USERID");
                //uTextReceiptID.Text = m_resSys.GetString("SYS_USERID");
                //uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");

                //uTextRepairReqName.Text = m_resSys.GetString("SYS_USERNAME");
                //uTextReceiptName.Text = m_resSys.GetString("SYS_USERNAME");
                //uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");

                uDateRepairReqDate.Value = DateTime.Now;
                uDateBreakDownDate.Value = DateTime.Now;
                uDateRepairDate.Value = DateTime.Now;

                //uDateRepairStartTime.Value = DateTime.Now;
                uDateRepairEndTime.Value = DateTime.Now;

                uComboRepairResult.SelectedIndex = 0;

                RepairRequestCode = string.Empty;

                if (uComboPlant.Value.ToString() != string.Empty)
                {

                    WinComboEditor wCombo = new WinComboEditor();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                    QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                    brwChannel.mfCredentials(clsPMResult);

                    DataTable dtPMResult = clsPMResult.mfReadPMResultTypeCombo(uComboPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboRepairResult.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboRepairResult, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "PMResultCode", "PMResultName", dtPMResult);

                    //m_dtPMResult.Clear();
                    m_dtPMResult = new DataTable();
                    m_dtPMResult = clsPMResult.mfReadMASPMResultType(uComboRepairResult.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    //dtPMResult.Dispose();
                    clsPMResult.Dispose();

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }
         
        
        /// <summary>
        /// 콤보값 변경시 m_dtPMResult 확인해서 uCheckChangeFlag 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboRepairResult_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (uComboRepairResult.Items.Count == 0)
                    return;

                if (uComboPlant.Value.ToString().Equals(string.Empty))
                    return;

                if (uComboRepairResult.Value.ToString().Equals(string.Empty))
                {
                    uCheckChangeFlag.Checked = false;
                    return;
                }

                string strRepairResultCode = uComboRepairResult.Value.ToString();
                for (int i = 0; i < m_dtPMResult.Rows.Count; i++)
                {
                    if (m_dtPMResult.Rows[i]["PMResultCode"].ToString() == strRepairResultCode)
                    {
                        uCheckChangeFlag.Checked = m_dtPMResult.Rows[i]["ChangeFlag"].ToString() == "T" ? true : false;

                        if (uCheckChangeFlag.Checked)
                            break;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// Req코드에 따라 정보를 리턴해줌
        /// </summary>
        /// <param name="strReq"> RE : 수리요청정보, AC : 수리접수정보 , MES: MES전송정보 </param>
        /// <returns></returns>
        private DataTable mfReadData(string strReq)
        {
            DataTable dtReadData = new DataTable();
            try
            {
                //System ResourceInfo 
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();         //공장
                string strRepairReqID = this.uTextRepairReqID.Text;             //수리요청자
                string strEquipCode = this.uTextEquipCode.Text;                 //설비
                string strDownCode = this.uTextDownCode.Text;                   //DownCode
                string strProduct = this.uTextProductCode.Text;                 //제품코드
                string strRepairReqReson = this.uTextRepairReqReason.Text;      //요청사유
                string strUrgent = this.uCheckUrgentFlag.Checked.Equals(false) ? "F" : "T"; //긴급처리
                string strAutoAccept = this.uCheckAutoAccept.Checked.Equals(false) ? "F" : "T"; //자동접수처리

                if (strReq.Equals("RE"))
                {
                    #region 수리요청정보

                    dtReadData.Columns.Add("PlantCode", typeof(string));
                    dtReadData.Columns.Add("RepairReqID", typeof(string));
                    dtReadData.Columns.Add("EquipCode", typeof(string));
                    dtReadData.Columns.Add("DownCode", typeof(string));
                    dtReadData.Columns.Add("RepairReqDate", typeof(string));
                    dtReadData.Columns.Add("RepairReqTime", typeof(string));
                    dtReadData.Columns.Add("BreakDate", typeof(string));
                    dtReadData.Columns.Add("BreakTime", typeof(string));
                    dtReadData.Columns.Add("ProductCode", typeof(string));
                    dtReadData.Columns.Add("RepairReqReson", typeof(string));
                    dtReadData.Columns.Add("Urgent", typeof(string));
                    dtReadData.Columns.Add("AutoAccept", typeof(string));

                    DataRow rwReqCode = dtReadData.NewRow();

                    rwReqCode["PlantCode"] = strPlantCode;                     //공장
                    rwReqCode["RepairReqID"] = strRepairReqID;                          //수리요청자
                    rwReqCode["EquipCode"] = strEquipCode;                              //설비
                    rwReqCode["DownCode"] = strDownCode;                                //DownCode
                    rwReqCode["RepairReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");               //요청일
                    rwReqCode["RepairReqTime"] = DateTime.Now.ToString("HH:mm:ss");                 //요청시간
                    rwReqCode["BreakDate"] = Convert.ToDateTime(this.uDateBreakDownDate.Value).ToString("yyyy-MM-dd"); //고장발생일
                    rwReqCode["BreakTime"] = Convert.ToDateTime(this.uDateBreakDownTime.Value).ToString("HH:mm:ss");   //고장발생시각
                    rwReqCode["ProductCode"] = this.uTextProductCode.Text;                          //제품코드
                    rwReqCode["RepairReqReson"] = this.uTextRepairReqReason.Text;                   //요청사유
                    rwReqCode["Urgent"] = strUrgent;                                                //긴급처리
                    rwReqCode["AutoAccept"] = strAutoAccept;                                        //자동접수처리

                    dtReadData.Rows.Add(rwReqCode);


                    #endregion

                }
                if (strReq.Equals("AC"))
                {
                    #region 수리접수 정보


                    dtReadData.Columns.Add("RepairReqCode");
                    dtReadData.Columns.Add("HeadFlag");
                    dtReadData.Columns.Add("HeadDesc");
                    dtReadData.Columns.Add("StageDesc");
                    dtReadData.Columns.Add("ReceiptDesc");
                    dtReadData.Columns.Add("ReceiptUserID");

                    DataRow drReqCode;

                    drReqCode = dtReadData.NewRow();

                    drReqCode["HeadFlag"] = uCheckHeadFlag.Checked == true ? "T" : "F";
                    drReqCode["HeadDesc"] = uTextHeadDesc.Text;
                    drReqCode["StageDesc"] = uTextStageDesc.Text;
                    drReqCode["ReceiptDesc"] = uTextReceiptDesc.Text;
                    drReqCode["ReceiptUserID"] = !this.uTextReceiptID.Text.Equals(string.Empty) ? this.uTextReceiptID.Text : m_resSys.GetString("SYS_USERID");
                    dtReadData.Rows.Add(drReqCode);

                    #endregion
                }
                if (strReq.Equals("MES"))
                {
                    #region MES정보

                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();

                    brwChannel.mfCredentials(clsRepairReq);

                    dtReadData = clsRepairReq.mfSetMESData();

                    DataRow drMES = dtReadData.NewRow();

                    drMES["EQPID"] = strEquipCode;
                    drMES["EQPSTATE"] = strDownCode.Substring(0, 1) == "D" ? "MDC" : "DOWN";
                    drMES["PRODUCTSPECID"] = strProduct;
                    drMES["MACHINERECIPE"] = "";
                    drMES["REASONCODE"] = strDownCode;
                    drMES["EMERGENCYFLAG"] = strUrgent == "T" ? "Y" : "N";
                    drMES["COMMENT"] = strRepairReqReson;
                    drMES["TOOLLIST"] = "";

                    dtReadData.Rows.Add(drMES);

                    #endregion
                }

                return dtReadData;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtReadData;
            }
            finally
            {
                dtReadData.Dispose();
            }
        }

        /// <summary>
        /// 설비정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <returns>설비정보</returns>
        private DataTable mfSelectEquip(string strPlantCode ,string strEquipCode)
        {
           
            DataTable dtEquip = new DataTable();

            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //설비정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //설비정보조회 매서드 실행
                return dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtEquip;
            }
            finally
            {
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 제품정보 조회
        /// </summary>
        private void mfSelectMaterial()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product Material = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(Material);

                DataTable dtMaterial = Material.mfReadMASMaterialDetail(uComboPlant.Value.ToString(), uTextProductCode.Text, m_resSys.GetString("SYS_LANG"));

                if (dtMaterial.Rows.Count == 0)
                {
                    dtMaterial.Dispose();
                    uTextProductName.Text = string.Empty;
                    return;
                }

                uTextProductName.Text = dtMaterial.Rows[0]["ProductName"].ToString();

                dtMaterial.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strUserID">유저아이디</param>
        /// <returns>사용자정보</returns>
        private DataTable mfSearchUser(string strPlantCode ,string strUserID)
        {
            DataTable dtUser = new DataTable();

            try
            {
                //System ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                //사용자정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                //사용자조회매서드 실행
                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));                

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
                dtUser.Dispose();
            }
        }

        /// <summary>
        /// 다운코드 팝업창
        /// </summary>
        private void mfOpenDownPOP(string strPlantCode,string strEquipCode)
        {
            try
            {
                frmEQUZ0022_P1 frmDown = new frmEQUZ0022_P1();
                frmDown.PlantCode = strPlantCode;
                frmDown.EquipCode = strEquipCode;
                frmDown.ShowDialog();

                //팝업창에서 받아온 DownCode와 DownName을 텍스트에 삽입
                this.uTextDownCode.Text = frmDown.DownCode;
                this.uTextDownName.Text = frmDown.DownName;

                

                //선택한 항목이 품종교체면 제품 입력가능 그외 입력불가능
                if (frmDown.ReasonCode != null && !frmDown.ReasonCode.Equals(string.Empty))
                {
                    //품종교체코드 :  D
                    if (frmDown.ReasonCode.Equals("D"))
                    {
                        //제품 텍스트 수정가능 필수항목으로 변경
                        this.uTextProductCode.ReadOnly = false;
                        this.uLabelGoods.Appearance.ForeColor = Color.Red;
                        this.uTextProductCode.Appearance.BackColor = Color.PowderBlue;
                        this.uTextProductCode.Focus();
                    }
                    else
                    {
                        //제품텍스트 수정불가 필수해제
                        this.uTextProductCode.ReadOnly = true;
                        this.uLabelGoods.Appearance.ForeColor = Color.Black;
                        this.uTextProductCode.Appearance.BackColor = Color.Empty;

                        //제품이 입력되어있다면 클리어
                        if (!this.uTextProductCode.Text.Equals(string.Empty) || !this.uTextProductName.Text.Equals(string.Empty))
                        {
                            this.uTextProductCode.Clear();
                            this.uTextProductName.Clear();
                        }
                        //품종교체가 아닌데 패키지와 패키지 타입이 입력되어있다면 클리어
                        if (!this.uTextPackage.Text.Equals(string.Empty) || !this.uTextPKType.Text.Equals(string.Empty))
                        {
                            this.uTextPackage.Clear();
                            this.uTextPKType.Clear();
                        }
                    }
                    
                }
                else
                {
                    this.uTextContainer.Focus();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비수리 팝업창
        /// </summary>
        private void mfOpenRepairEquipPOP(string strSuperCode)
        {
            try
            {
                frmEQUZ0022_P2 frmReapir = new frmEQUZ0022_P2();
                frmReapir.PlantCode = this.uComboPlant.Value.ToString();
                frmReapir.EquipCode = this.uTextEquipCode.Text;
                frmReapir.SuperCode = strSuperCode;
                frmReapir.ShowDialog();


                this.uTextFaultTypeCode.Text = frmReapir.RepairCode;
                this.uTextFaultTypeName.Text = frmReapir.RepairName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Text EditorButton

        /// <summary>
        /// 설비 팝업창 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                frmEquip.PlantCode = uComboPlant.Value.ToString();

                frmEquip.ShowDialog();

                if (frmEquip.PlantCode != "" && strPlant != frmEquip.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                uTextEquipCode.Text = frmEquip.EquipCode.ToString();
                uTextEquipName.Text = frmEquip.EquipName.ToString();

                mfCheckEquipOnRepair(uComboPlant.Value.ToString(), uTextEquipCode.Text);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비 팝업창 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                //ResourceIfno
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                frmEquip.PlantCode = strPlant;

                frmEquip.ShowDialog();

                
                this.uTextSearchEquipCode.Text = frmEquip.EquipCode.ToString();
                this.uTextSearchEquipName.Text = frmEquip.EquipName.ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 요청자 팝업 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRepairReqID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboPlant.Value.ToString();

                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextRepairReqID.Text = frmUser.UserID;
                this.uTextRepairReqName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 접수자 팝업 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextReceiptID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboPlant.Value.ToString();

                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextReceiptID.Text = frmUser.UserID;
                this.uTextReceiptName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리담당 팝업 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRepairUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboPlant.Value.ToString();

                frmUser.ShowDialog();

                this.uTextRepairUserID.Text = frmUser.UserID;
                this.uTextRepairUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 제품 팝업창 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uTextProductCode.ReadOnly)
                    return;

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0002 frmMaterial = new QRPCOM.UI.frmCOM0002();
                frmMaterial.PlantCode = uComboPlant.Value.ToString();

                frmMaterial.ShowDialog();

                this.uTextProductCode.Text = frmMaterial.ProductCode;
                this.uTextProductName.Text = frmMaterial.ProductName;
                this.uTextPackage.Text = frmMaterial.Package;
                this.uTextPKType.Text = frmMaterial.PackageGroupCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// DownCode EditorButton 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextDownCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if(!this.uTextEquipCode.Text.Equals(string.Empty) && !this.uComboPlant.Value.ToString().Equals(string.Empty))
                    mfOpenDownPOP(this.uComboPlant.Value.ToString(),this.uTextEquipCode.Text);
                //else

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비수리 팝업창호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextFaultTypeCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextFaultTypeCode.Enabled)
                {
                    if (this.uTextFaultTypeCode.Tag == null || this.uTextFaultTypeCode.Tag.ToString().Equals(string.Empty))
                        return;

                    mfOpenRepairEquipPOP(this.uTextFaultTypeCode.Tag.ToString());
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 품종교체 팝업창호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonChg_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtRequst = mfReadData("RE");
                DataTable dtAccept = mfReadData("AC");
                DataTable dtMESInfo = mfReadData("MES");
                frmEQUZ0022_P3_S frmP3 = new frmEQUZ0022_P3_S(dtRequst, dtAccept, dtMESInfo);
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                //수리요청코드가 없으면 요청,접수정보를 추가적으로 보내준다
                if (strRepairRequestCode.Equals(string.Empty))
                {
                    frmP3.PlantCode = this.uComboPlant.Value.ToString();
                    frmP3.EquipCode = this.uTextEquipCode.Text;
                    frmP3.ShowDialog();
                }
                //수리요청코드가 있는 경우 수리요청코드를 추가적으로 보내준다.
                else
                {
                    frmP3.PlantCode = this.uComboPlant.Value.ToString();
                    frmP3.EquipCode = this.uTextEquipCode.Text;
                    frmP3.RepairReqCode = strRepairRequestCode;
                    frmP3.ShowDialog();
                }

                this.uButtonChg.Visible = false;
                this.uButtonCancelAccept.Enabled = false;
                string strLang = m_resSys.GetString("SYS_LANG");
                if (frmP3.SaveChk == null)
                {
                    if(strLang.Equals("KOR"))
                        uLabelNotice.Text = "설비상태리스트를 다시 선택해주세요";
                    else if(strLang.Equals("CHN"))
                        uLabelNotice.Text = "请重新选择设备状态清单。";
                    else if(strLang.Equals("ENG"))
                        uLabelNotice.Text = "설비상태리스트를 다시 선택해주세요";
                    this.uButtonCCS.Visible = false;
                }
                else
                {
                    if (strLang.Equals("KOR"))
                        uLabelNotice.Text = "품종교체 등록하였습니다.\n 버튼을 누르시면 CCS등록화면으로\n이동 됩니다.";
                    else if (strLang.Equals("CHN"))
                        uLabelNotice.Text = "已完成登陆品种交换。\n 点击按钮时会转到CCS登陆画面。";
                    else if (strLang.Equals("ENG"))
                        uLabelNotice.Text = "품종교체 등록하였습니다.\n 버튼을 누르시면 CCS등록화면으로\n이동 됩니다.";
                    this.uButtonCCS.Visible = true;
                }

                mfSearch();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// CCS의뢰 화면이동 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCCS_Click(object sender, EventArgs e)
        {
            try
            {
                //호출하려는 화면을 선언

                QRPCCS.UI.frmCCSZ0001_S frmCCS = new QRPCCS.UI.frmCCSZ0001_S();

                CommonControl cControl = new CommonControl();

                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");

                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;

                //해당화면의 탭이 있는경우 해당 탭 닫기
                if (uTabMenu.Tabs.Exists("QRPCCS" + "," + "frmCCSZ0001_S"))
                {
                    uTabMenu.Tabs["QRPCCS" + "," + "frmCCSZ0001_S"].Close();
                }
                //탭에 추가
                uTabMenu.Tabs.Add("QRPCCS" + "," + "frmCCSZ0001_S", "CCS의뢰");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPCCS,frmCCSZ0001_S"];

                //호출시 화면 속성 설정
                frmCCS.AutoScroll = true;
                frmCCS.MdiParent = this.MdiParent;
                frmCCS.ControlBox = false;
                frmCCS.Dock = DockStyle.Fill;
                frmCCS.FormBorderStyle = FormBorderStyle.None;
                frmCCS.WindowState = FormWindowState.Normal;
                frmCCS.Text = "CCS의뢰";
                frmCCS.FormName = this.Name;
                frmCCS.Show();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 치공구교체팝업창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonChange_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uButtonAccept.Enabled.Equals(false))
                {
                    //팝업창으로 정보를 보낼 테이블과 컬럼설정
                    DataTable dtCEquip = new DataTable();
                    dtCEquip.Columns.Add("EquipCode", typeof(string));
                    dtCEquip.Columns.Add("EquipName", typeof(string));

                    DataTable dtDetail = new DataTable();
                    dtDetail.Columns.Add("PlantCode", typeof(string));
                    dtDetail.Columns.Add("PlanYear", typeof(string));
                    dtDetail.Columns.Add("PMPlanDate", typeof(string));

                    DataRow drEquip = dtCEquip.NewRow();
                    drEquip["EquipCode"] = this.uTextEquipCode.Text;
                    drEquip["EquipName"] = this.uTextEquipCode.Text;
                    dtCEquip.Rows.Add(drEquip);

                    drEquip = dtDetail.NewRow();
                    drEquip["PlantCode"] = this.uComboPlant.Value.ToString();
                    drEquip["PlanYear"] = this.uDateRepairDate.DateTime.Date.ToString("yyyy");
                    drEquip["PMPlanDate"] = this.uDateRepairDate.DateTime.Date.ToString("yyyy-MM-dd");
                    dtDetail.Rows.Add(drEquip);


                    frmEQU0005D1_S frmDurable;
                    if (RepairRequestCode == null || RepairRequestCode.Equals(string.Empty))
                    {
                        DataTable dtRequest = mfReadData("RE");
                        DataTable dtAccept = mfReadData("AC");
                        frmDurable = new frmEQU0005D1_S(dtCEquip, dtDetail, dtRequest, dtAccept);
                        frmDurable.RepairReqCode = "";
                    }
                    else
                    {
                        frmDurable = new frmEQU0005D1_S(dtCEquip, dtDetail);
                        frmDurable.RepairReqCode = RepairRequestCode;
                    }

                    frmDurable.Package = this.uTextPackage.Text;
                    frmDurable.Kind = "RE";
                    frmDurable.ShowDialog();

                    if (!frmDurable.RepairReqCode.Equals(string.Empty) && (RepairRequestCode == null || RepairRequestCode.Equals(string.Empty)))
                        RepairRequestCode = frmDurable.RepairReqCode;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region TextKeyDown조회

        /// <summary>
        /// 설비직접입력이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete) && !this.uTextSearchEquipName.Text.Equals(string.Empty))
                    this.uTextSearchEquipName.Clear();

                if (e.KeyCode != Keys.Enter)
                    return;

                if (this.uTextSearchEquipCode.Text == string.Empty)
                    return;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;

                DataTable dtEquip = mfSelectEquip(strPlantCode, strEquipCode);

                if (dtEquip.Rows.Count != 0)
                {
                    this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();

                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000901", Infragistics.Win.HAlign.Right);
                    this.uTextSearchEquipName.Clear();
                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리자직접입력이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRepairUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete)) && !this.uTextRepairUserName.Text.Equals(string.Empty))
                {
                    this.uTextRepairUserName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextRepairUserID.Text.Equals(string.Empty))
                {//System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //검색정보 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strUserID = this.uTextRepairUserID.Text;

                    // 공장정보 확인
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    //유저정보 조회매서드 실행
                    DataTable dtUser = mfSearchUser(strPlantCode, strUserID);

                    //정보가 있을 경우 뿌려주고, 없으면메세지를 출력한다.
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextRepairUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextRepairUserName.Clear();

                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 접수자직접입력이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextReceiptID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete)) && !this.uTextReceiptName.Text.Equals(string.Empty))
                {
                    this.uTextReceiptName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextReceiptID.Text.Equals(string.Empty))
                {//System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //검색정보 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strUserID = this.uTextReceiptID.Text;

                    // 공장정보 확인
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    //유저정보 조회매서드 실행
                    DataTable dtUser = mfSearchUser(strPlantCode, strUserID);

                    //정보가 있을 경우 뿌려주고, 없으면메세지를 출력한다.
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextReceiptName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextReceiptName.Clear();

                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 수리요청자 입력이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRepairReqID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete)) && !this.uTextRepairReqName.Text.Equals(string.Empty))
                {
                    this.uTextRepairReqName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextRepairReqID.Text.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //검색정보 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strUserID = this.uTextRepairReqID.Text;

                    // 공장정보 확인
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    //유저정보 조회매서드 실행
                    DataTable dtUser = mfSearchUser(strPlantCode, strUserID);

                    //정보가 있을 경우 뿌려주고, 없으면메세지를 출력한다.
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextRepairReqName.Text = dtUser.Rows[0]["UserName"].ToString();

                        mfOpenDownPOP(strPlantCode,this.uTextEquipCode.Text);
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000883", Infragistics.Win.HAlign.Right);
                        this.uTextRepairReqName.Clear();

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비번호 직접입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Enter)
                    return;

                if (uTextEquipCode.Text == string.Empty)
                    return;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;

                DataTable dtEquip = mfSelectEquip(strPlantCode, strEquipCode);

                if (dtEquip.Rows.Count != 0)
                {
                    uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    mfCheckEquipOnRepair(strPlantCode, strEquipCode);

                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001116", "M000901", Infragistics.Win.HAlign.Right);
                    uTextEquipName.Clear();
                    //foreach (Control ctrl in uGroupBoxRequestRepair.Controls)
                    //{
                    //    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                    //    {
                    //        ctrl.Text = string.Empty;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 제품번호 직접입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextProductCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;
            
            if (this.uTextProductCode.ReadOnly)
                return;

            mfSelectMaterial();
        }

        #endregion

        #endregion

        #region POP업창 호출
        /// <summary>
        /// 수리출고등록 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonChangeMold_Click(object sender, EventArgs e)
        {
            try
            {
                QRPEQU.UI.frmEQUZ0010 frmRepairGI = new QRPEQU.UI.frmEQUZ0010();
                frmRepairGI.FormName = this.Name;
                frmRepairGI.RepairReqCode = RepairRequestCode;

                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;

                if (uTabMenu.Tabs.Exists("QRPEQU" + "," + "frmEQUZ0010"))
                {
                    uTabMenu.Tabs["QRPEQU" + "," + "frmEQUZ0010"].Close();
                }

                uTabMenu.Tabs.Add("QRPEQU" + "," + "frmEQUZ0010", "설비수리출고등록");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPEQU,frmEQUZ0010"];

                frmRepairGI.AutoScroll = true;
                frmRepairGI.MdiParent = this.MdiParent;
                frmRepairGI.ControlBox = false;
                frmRepairGI.Dock = DockStyle.Fill;
                frmRepairGI.FormBorderStyle = FormBorderStyle.None;
                frmRepairGI.WindowState = FormWindowState.Normal;
                frmRepairGI.Text = "설비수리완료등록";
                frmRepairGI.Show();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 수리완료등록
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonUseSP_Click(object sender, EventArgs e)
        {
            try
            {
                //호출하려는 화면을 선언
                QRPEQU.UI.frmEQUZ0012 frmRepairGI = new QRPEQU.UI.frmEQUZ0012();
                frmRepairGI.FormName = this.Name;
                frmRepairGI.RepairReqCode = RepairRequestCode;

                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                //해당화면의 탭이 있는경우 해당 탭 닫기
                if (uTabMenu.Tabs.Exists("QRPEQU" + "," + "frmEQUZ0012"))
                {
                    uTabMenu.Tabs["QRPEQU" + "," + "frmEQUZ0012"].Close();
                }
                //탭에 추가
                uTabMenu.Tabs.Add("QRPEQU" + "," + "frmEQUZ0012", "설비수리완료등록");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPEQU,frmEQUZ0012"];

                //호출시 화면 속성 설정
                frmRepairGI.AutoScroll = true;
                frmRepairGI.MdiParent = this.MdiParent;
                frmRepairGI.ControlBox = false;
                frmRepairGI.Dock = DockStyle.Fill;
                frmRepairGI.FormBorderStyle = FormBorderStyle.None;
                frmRepairGI.WindowState = FormWindowState.Normal;
                frmRepairGI.Text = "설비수리완료등록";
                frmRepairGI.Show();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        #endregion

        #region 요청,접수,결과등록버튼

        //수리요청버튼을 누를 경우 요청이된다.
        private void uButtonRepair_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                #region 필수입력확인
                if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001226", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uTextRepairReqID.Text.Equals(string.Empty) || this.uTextRepairReqName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001226", "M000740", "M000741", Infragistics.Win.HAlign.Right);

                    this.uTextRepairReqID.Focus();
                    return;
                }

                if (this.uTextEquipCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001226", "M000715", "M000223", Infragistics.Win.HAlign.Right);

                   

                    return;
                }

                if (this.uTextDownCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001226", "M000042", "M000044", Infragistics.Win.HAlign.Right);

                    this.uTextDownCode.Focus();
                    return;
                }

                if (this.uDateBreakDownDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001226", "M000259", "M000260", Infragistics.Win.HAlign.Right);

                    this.uDateBreakDownDate.DropDown();
                    return;
                }

                if (this.uDateBreakDownTime.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001226", "M000257", "M000258", Infragistics.Win.HAlign.Right);

                    this.uDateBreakDownTime.Focus();
                    return;
                }

                if ((this.uTextProductCode.Text.Equals(string.Empty) || this.uTextProductName.Text.Equals(string.Empty)) && this.uTextProductCode.ReadOnly.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001226", "M001090", "M001088", Infragistics.Win.HAlign.Right);

                    this.uTextProductCode.Focus();
                    return;
                }
                // 해당 설비가 CCS의뢰 상태인 경우 메세지 출력 Return
                if (CheckCCS(uComboPlant.Value.ToString(), this.uTextEquipCode.Text))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);

                    return;
                }
                #endregion

                #region 수리요청정보 저장

                string strPlantCode= this.uComboPlant.Value.ToString();         //공장
                string strRepairReqID = this.uTextRepairReqID.Text;             //수리요청자
                string strEquipCode = this.uTextEquipCode.Text;                 //설비
                string strDownCode = this.uTextDownCode.Text;                   //DownCode
                string strReqDate = DateTime.Now.ToString("yyyy-MM-dd");   //요청일
                string strReqTime = DateTime.Now.ToString("HH:mm:ss");     //요청시간
                string strBreakDate = Convert.ToDateTime(this.uDateBreakDownDate.Value).ToString("yyyy-MM-dd"); //고장발생일
                string strBreakTime = Convert.ToDateTime(this.uDateBreakDownTime.Value).ToString("HH:mm:ss");   //고장발생시각
                string strProduct = this.uTextProductCode.Text;                 //제품코드
                string strRepairReqReson = this.uTextRepairReqReason.Text;      //요청사유
                string strUrgent = this.uCheckUrgentFlag.Checked.Equals(false) ? "F" : "T"; //긴급처리
                string strAutoAccept = this.uCheckAutoAccept.Checked.Equals(false) ? "F" : "T"; //자동접수처리

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();

                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtMESInfo = clsRepairReq.mfSetMESData();

                DataRow drMES = dtMESInfo.NewRow();

                drMES["EQPID"] = strEquipCode;
                drMES["EQPSTATE"] = strDownCode.Substring(0, 1) == "D" ? "MDC" : "DOWN";
                drMES["PRODUCTSPECID"] = strProduct;
                drMES["MACHINERECIPE"] = "";
                drMES["REASONCODE"] = strDownCode;
                drMES["EMERGENCYFLAG"] = strUrgent == "T" ? "Y": "N";
                drMES["COMMENT"] = strRepairReqReson;
                drMES["TOOLLIST"] = "";

                dtMESInfo.Rows.Add(drMES);

                #endregion

                #region Insert RepairRequest

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", strEquipCode + "M000390",
                                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }
               
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                
                // 저장 시작
                string strErrRtn = clsRepairReq.mfSaveEquipRepairRequest_Down(RepairRequestCode, strPlantCode, strEquipCode, strReqDate, strReqTime, strRepairReqID,
                                                        strBreakDate, strBreakTime,strProduct, strRepairReqReson, strUrgent,strDownCode,strAutoAccept,dtMESInfo,
                                                        m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"),this.Name);

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                #endregion

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                string strLang = m_resSys.GetString("SYS_LANG");
                DialogResult result = new DialogResult();
                if (ErrRtn.ErrNum.Equals(0))
                {
                    string strMessage = "";
                    

                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMessage = ErrRtn.InterfaceResultMessage;
                    }
                    else
                        strMessage = msg.GetMessge_Text("M000930",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMessage,
                                                Infragistics.Win.HAlign.Right);
                    
                    mfEnabledOn("O");
                    mfSearch();

                    if (!ErrRtn.mfGetReturnValue(0).Equals(string.Empty))
                        RepairRequestCode = ErrRtn.mfGetReturnValue(0);
                }
                else
                {
                    string strErrMsg = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strErrMsg = ErrRtn.ErrMessage;
                    else
                        strErrMsg = msg.GetMessge_Text("M000953",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strErrMsg,
                                                 Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비수리관리등록 접수 버튼을 누를 경우 접수가 된다.
        private void uButtonAccept_Click_1(object sender, EventArgs e)
        {
            try
            {
                
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항
                if (this.uTextEquipCode.Text.Equals(string.Empty) || this.uTextEquipCode.Enabled)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000827", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uTextReceiptID.Text.Equals(string.Empty) || this.uTextReceiptName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001235", "M001072", "M001071", Infragistics.Win.HAlign.Right);
                    this.uTextReceiptID.Focus();
                    return;
                }

                if (this.uDateReceiptDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001235", "M001070", "M001069", Infragistics.Win.HAlign.Right);
                    this.uDateReceiptDate.DropDown();
                    return;
                }
                #endregion

                // 해당 설비가 CCS의뢰 상태인 경우 메세지 출력 Return
                if (CheckCCS(uComboPlant.Value.ToString(), this.uTextEquipCode.Text))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001053", "M000670",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string strRtn = mfSave_ReqNone(this.uTextEquipState.Text);

                //string strRtn = clsRepairReq.mfSaveEquipRepairRequest_Receipt(dtRepair, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);
                this.MdiParent.Cursor = Cursors.Default;

                DialogResult result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");

                if (ErrRtn.ErrNum != 0)
                {
                    string strMsg = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = ErrRtn.ErrMessage;
                    else
                        strMsg = msg.GetMessge_Text("M000953",strLang);
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMsg,
                                                 Infragistics.Win.HAlign.Right);
                    
                    return;
                }

                if (ErrRtn.ErrNum == 0)
                {

                    string strMessage = "";

                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMessage = ErrRtn.InterfaceResultMessage;
                    }
                    else
                        strMessage = msg.GetMessge_Text("M000930",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMessage,
                                                Infragistics.Win.HAlign.Right);
                    mfEnabledOn("T");
                    mfSearch();
                }

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리결과 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonResultSave_Click(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            #region Check Indispensable

            if (this.uTextEquipCode.Enabled || this.uTextEquipCode.Text.Equals(string.Empty))
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000227", Infragistics.Win.HAlign.Right);
                return;
            }

            if (uDateRepairDate.Value.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000733", Infragistics.Win.HAlign.Right);
                uDateRepairDate.DropDown();
                return;
            }
            if (uTextRepairUserID.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000732", Infragistics.Win.HAlign.Right);
                uTextRepairUserID.Focus();
                return;
            }
            if (uTextRepairUserName.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000732", Infragistics.Win.HAlign.Right);
                uTextRepairUserName.Focus();
                return;
            }


            if (this.uTextFaultTypeCode.Text.Equals(string.Empty) || this.uTextFaultTypeName.Text.Equals(string.Empty))
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000043", Infragistics.Win.HAlign.Right);
                uTextFaultTypeCode.Focus();
                return;
            }

            //콤보박스 선택값 Validation Check//////////
            QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
            if (!check.mfCheckValidValueBeforSave(this)) return;
            ///////////////////////////////////////////
            #endregion

            // 해당 설비가 CCS의뢰 상태인 경우 메세지 출력 Return
            if (CheckCCS(uComboPlant.Value.ToString(), this.uTextEquipCode.Text))
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);
                
                return;
            }


            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            "M001264", "M001053", "M000936",
            Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

              
                string strRtn = mfSave_ReqNone(this.uTextEquipState.Text);
                //string strRtn = clsEquipReq.mfSaveEquipRepairResult(
                //                                                        strRepairReqCode, strRepairStartDate, "",
                //                                                        strRepairEndDate, strRepairEndTime, strRepairUserID,
                //                                                        strRepairResultCode, strDownCode, strDownCodeName, strCCSReqFlag,
                //                                                        strRepairDesc, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID")
                //                                                    );

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
               

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                string strLang = m_resSys.GetString("SYS_LANG");
                
                if (ErrRtn.ErrNum == 0)
                {
                    string strMessage = "";

                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMessage = ErrRtn.InterfaceResultMessage;
                    }
                    else
                    {
                        strMessage = msg.GetMessge_Text("M000930",strLang);

                        ////결과등록성공시 리턴값을 받는데 그 리턴값이(요청번호가) 있는 경우 치공구교체등록가능한 버튼을 활성화한다.
                        //if (ErrRtn.mfGetReturnValue(0) != null && !ErrRtn.mfGetReturnValue(0).Equals(string.Empty))
                        //{
                        //    RepairRequestCode = ErrRtn.mfGetReturnValue(0);
                        //    this.uLabelNotice.Text = "치공구교체를 원하시면\n버튼을 눌러주세요.";
                            
                        //    mfVisible("Change", "Change");
                            
                        //}

                    }
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMessage,
                                                Infragistics.Win.HAlign.Right);
                    
                    mfEnabledOn("R");
                    mfSearch();
                }
                else
                {
                    string strMsg = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = ErrRtn.ErrMessage;
                    else
                        strMsg = msg.GetMessge_Text("M000953",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMsg,
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 취소버튼

        /// <summary>
        /// 접수취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancelAccept_Click(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            if (this.uTextEquipCode.Text.Equals(string.Empty))
                return;

            // 해당 설비가 CCS의뢰 상태인 경우 메세지 출력 Return
            if (CheckCCS(uComboPlant.Value.ToString(), this.uTextEquipCode.Text))
            {
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);

                return;
            }

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001053", "M000671",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }


            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");

                #region 의뢰취소정보

                string strRequest = RepairRequestCode;
                DataTable dtRepair = new DataTable();
                dtRepair.Columns.Add("RepairReqCode");
                dtRepair.Columns.Add("PlantCode");
                dtRepair.Columns.Add("RepairReqID");
                dtRepair.Columns.Add("AutoAccept");

                DataRow rwReqCode = dtRepair.NewRow();

                rwReqCode["RepairReqCode"] = RepairRequestCode;
                rwReqCode["PlantCode"] = this.uComboPlant.Value.ToString();                     //공장
                rwReqCode["RepairReqID"] = this.uTextRepairReqID.Text;                          //수리요청자
                //rwReqCode["EquipCode"] = this.uTextEquipCode.Text;                              //설비
                //rwReqCode["DownCode"] = this.uTextDownCode.Text;                                //DownCode
                //rwReqCode["RepairReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");               //요청일
                //rwReqCode["RepairReqTime"] = DateTime.Now.ToString("HH:mm:ss");                 //요청시간
                //rwReqCode["BreakDate"] = Convert.ToDateTime(this.uDateBreakDownDate.Value).ToString("yyyy-MM-dd"); //고장발생일
                //rwReqCode["BreakTime"] = Convert.ToDateTime(this.uDateBreakDownTime.Value).ToString("HH:mm:ss");   //고장발생시각
                //rwReqCode["ProductCode"] = this.uTextProductCode.Text;                          //제품코드
                //rwReqCode["RepairReqReson"] = this.uTextRepairReqReason.Text;                   //요청사유
                //rwReqCode["Urgent"] = this.uCheckUrgentFlag.Checked.Equals(false) ? "F" : "T"; //긴급처리
                rwReqCode["AutoAccept"] = this.uCheckAutoAccept.Checked.Equals(false) ? "F" : "T"; //자동접수처리

                dtRepair.Rows.Add(rwReqCode);

                #endregion

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);

                #region MES

                DataTable dtMESInfo = clsEquipReq.mfSetMESData();

                DataRow drMES = dtMESInfo.NewRow();

                drMES["EQPID"] = this.uTextEquipCode.Text;
                drMES["EQPSTATE"] = this.uTextDownCode.Text.Substring(0, 1) == "D" ? "MDC" : "DOWN";
                drMES["PRODUCTSPECID"] = this.uTextProductCode.Text;
                drMES["MACHINERECIPE"] = "";
                drMES["REASONCODE"] = "";
                drMES["EMERGENCYFLAG"] = this.uCheckUrgentFlag.Checked.Equals(false) ? "N" : "Y";
                drMES["COMMENT"] = this.uTextRepairReqReason.Text;
                drMES["TOOLLIST"] = "";

                dtMESInfo.Rows.Add(drMES);

                #endregion

                string strRtn = clsEquipReq.mfDeleteEquipRepair_Receipt(dtRepair,dtMESInfo,m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"),this.Name);

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                string strLang = m_resSys.GetString("SYS_LANG");

                if (ErrRtn.ErrNum == 0)
                {
                    string strMessage = "";

                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMessage = ErrRtn.InterfaceResultMessage;
                    }
                    else
                        strMessage = msg.GetMessge_Text("M000932",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001177",strLang), strMessage,
                                                Infragistics.Win.HAlign.Right);

                    mfCreate();
                    mfSearch();
                }
                else
                {
                    string strMsg = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = ErrRtn.ErrMessage;
                    else
                        strMsg = msg.GetMessge_Text("M000674",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001177",strLang), strMsg,
                                                 Infragistics.Win.HAlign.Right);
                }

                dtRepair.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리결과취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancelResult_Click(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            #region Check Indispensable
            if (RepairRequestCode == null || RepairRequestCode.Equals(string.Empty))
            {
                msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001230", "M000228", Infragistics.Win.HAlign.Right);
                //uGridRepairResult.Focus();
                return;
            }
            #endregion

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001179", "M000669",
                                    Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");

                string strRepairReqCode = RepairRequestCode;

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);

                string strRtn = clsEquipReq.mfDeleteEquipRepairResult(strRepairReqCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                // 저장 끝

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                string strLang = m_resSys.GetString("SYS_LANG");

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001177", "M000737",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                    //uGroupBoxContentsArea.Expanded = false;
                }
                else
                {
                    string strMsg = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = ErrRtn.ErrMessage;
                    else
                        strMsg = msg.GetMessge_Text("M000736",strLang);


                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001177",strLang), strMsg,
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리요청취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonRepairCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                string strLang = m_resSys.GetString("SYS_LANG");

                if (this.uTextEquipCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001264", "M000739", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"),500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000650",strLang)
                        , uTextEquipName.Text + msg.GetMessge_Text("M000839",strLang)
                        , Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);


                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string strRtn = clsEquipReq.mfDeleteEquipRepairRequest(RepairRequestCode);

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();

                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M000638", "M000734",
                                                Infragistics.Win.HAlign.Right);
                    InitValue();
                }
                else
                {
                    string strMsg = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = ErrRtn.ErrMessage;
                    else
                        strMsg = msg.GetMessge_Text("M000735",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Warning,  m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M000638",strLang), strMsg,
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region NameClear
        private void uTextSearchEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextSearchEquipCode.Text.Equals(string.Empty))
                this.uTextSearchEquipName.Clear();
        }

        private void uTextRepairReqID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextRepairReqName.Text.Equals(string.Empty))
                this.uTextRepairReqName.Clear();
        }

        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextEquipName.Text.Equals(string.Empty))
                this.uTextEquipName.Clear();
        }

        private void uTextDownCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextDownName.Text.Equals(string.Empty))
                this.uTextDownName.Clear();
        }

        private void uTextProductCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextProductName.Text.Equals(string.Empty))
                this.uTextProductName.Clear();
        }

        private void uTextReceiptID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextReceiptName.Text.Equals(string.Empty))
                this.uTextReceiptName.Clear();
        }

        private void uTextRepairUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextRepairUserName.Text.Equals(string.Empty))
                this.uTextRepairUserName.Clear();
        }

        #endregion
        
        #region Enabled , Visible
        /// <summary>
        /// Enabled상태로 바꾼다.
        /// </summary>
        /// <param name="strGroup">O : 요청,T : 접수,R : 결과</param>
        private void mfEnabledOn(string strGroup)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (strGroup.Equals("O"))
                {
                    //수리요청
                    this.uCheckUrgentFlag.Enabled = false;
                    //this.uCheckAutoAccept.Enabled = false;
                   // this.uComboPlant.Enabled = false;
                    this.uTextRepairReqID.Enabled = false;
                    this.uTextEquipCode.Enabled = false;
                    this.uTextDownCode.Enabled = false;
                    this.uDateBreakDownDate.Enabled = false;
                    this.uDateBreakDownTime.Enabled = false;
                    this.uTextProductCode.Enabled = false;
                    this.uTextRepairReqReason.Enabled = false;
                    this.uButtonRepair.Enabled = false;
                    this.uTextContainer.Enabled = false;
                }
                if (strGroup.Equals("T"))
                {
                    //수리접수
                    this.uTextReceiptID.Enabled = false;
                    this.uDateReceiptDate.Enabled = false;
                    this.uTextReceiptDesc.Enabled = false;
                    this.uButtonAccept.Enabled = false;
                    this.uButtonCancelAccept.Enabled = false;
                    this.uCheckHeadFlag.Enabled = false;
                    this.uTextHeadDesc.Enabled = false;
                }

                if (strGroup.Equals("R"))
                {
                    
                    //수리결과
                    this.uTextRepairUserID.Enabled = false;
                    this.uTextFaultTypeCode.Enabled = false;
                    this.uTextFaultTypeName.Enabled = false;
                    this.uTextRepairDesc.Enabled = false;
                    this.uDateRepairDate.Enabled = false;
                    //this.uDateRepairStartTime.Enabled = false;
                    this.uDateRepairEndTime.Enabled = false;
                    this.uComboRepairResult.Enabled = false;
                    this.uButtonSaveResult.Enabled = false;
                    this.uButtonUseSP.Enabled = false;
                    this.uButtonChangeMold.Enabled = false;
                    this.uButtonChange.Enabled = false;
                    this.uCheckImportant.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Enabled상태를 해제한다.
        /// </summary>
        /// <param name="Ctrl">해제하고자하는 그룹박스 컨트롤 이름</param>
        private void mfEnabledOff(Control Ctrl)
        {
            try
            {

                foreach (Control ctrl in Ctrl.Controls)
                {
                    if (ctrl.Enabled.Equals(false))
                    {
                        if (ctrl.Name.Equals("uCheckChangeFlag") 
                            || ctrl.Name.Equals("uCheckHeadFlag")
                            || ctrl.Name.Equals("uTextHeadDesc")
                            || ctrl.Name.Equals("uTextStageDesc") 
                            //|| ctrl.Name.Equals("uTextReceiptDesc")
                            )
                        {

                        }
                        else
                          ctrl.Enabled = true;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리결과 그룹박스창 Visible 처리
        /// </summary>
        /// <param name="strSuperCode"></param>
        /// <param name="strState"></param>
        private void mfVisible(string strSuperCode,string strState)
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG");

                //SperCode가 품종교체(D)인 경우 품종교체 버튼 활성화
                if (strSuperCode.Equals("D") && strState.Equals("Accept"))
                {
                    if (strLang.Equals("KOR"))
                        uLabelNotice.Text = "품종교체 의뢰/접수하였습니다.\n 버튼을 누르시면 팝업화면으로\n이동 됩니다.";
                    else if (strLang.Equals("CHN"))
                        uLabelNotice.Text = "品种交换 委托/接受成功。\n点击按钮时会转到弹出画面中。";
                    else if (strLang.Equals("ENG"))
                        uLabelNotice.Text = "품종교체 의뢰/접수하였습니다.\n 버튼을 누르시면 팝업화면으로\n이동 됩니다.";
                    
                    foreach (Control ctrl in this.uGroupBoxRepairResult.Controls)
                    {
                        if (!ctrl.Name.Equals("uButtonChg") && !ctrl.Name.Equals("uLabelNotice"))
                        {
                            ctrl.Visible = false;
                        }
                        else
                        {
                            ctrl.Visible = true;
                            
                            if(this.uButtonCancelAccept.Enabled.Equals(false))
                                uButtonCancelAccept.Enabled = true;
                        }
                    }
                }
                else if (strSuperCode.Equals("Change") && strState.Equals("Change"))
                {
                    foreach (Control ctrl in this.uGroupBoxRepairResult.Controls)
                    {

                        if(ctrl.Name.Equals("uLabelNotice"))
                        {
                            ctrl.Visible = true;
                        }
                        else if (ctrl.Name.Equals("uButtonChange"))
                        {
                            ctrl.Visible = true;
                        }
                        else
                        {
                            ctrl.Visible = false;
                        }
                    }
                }
                else
                {
                    foreach (Control ctrl in this.uGroupBoxRepairResult.Controls)
                    {
                        if (ctrl.Name.Equals("uButtonChg"))
                        {
                            ctrl.Visible = false;
                        }
                        else if(ctrl.Name.Equals("uLabelNotice"))
                        {
                            ctrl.Visible = false;
                        }
                        else if (ctrl.Name.Equals("uButtonCCS"))
                        {
                            ctrl.Visible = false;
                        }
                        //else if (ctrl.Name.Equals("uButtonChange"))
                        //{
                        //    ctrl.Visible = false;
                        //}
                        else
                        {
                            ctrl.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        /// <summary>
        /// 설비수리 통합저장
        /// </summary>
        /// <param name="strReceive">접수상태</param>
        /// <returns></returns>
        private string mfSave_ReqNone(string strReceive)
        {
            string strErrRtn = "";
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region 수리요청정보

                string strPlantCode = this.uComboPlant.Value.ToString();         //공장
                string strRepairReqID = this.uTextRepairReqID.Text;             //수리요청자
                string strEquipCode = this.uTextEquipCode.Text;                 //설비
                string strDownCode = this.uTextDownCode.Text;                   //DownCode
                string strReqDate = "";
                string strReqTime = "";
                string strBreakDate = "";
                string strBreakTime = "";
                
                if (this.uDateBreakDownDate.Value != null && !this.uDateBreakDownDate.Value.ToString().Equals(string.Empty))
                {
                    strReqDate = this.uDateBreakDownDate.DateTime.Date.ToString("yyyy-MM-dd"); //DateTime.Now.ToString("yyyy-MM-dd");   //요청일
                    strBreakDate = this.uDateBreakDownDate.DateTime.Date.ToString("yyyy-MM-dd"); //고장발생일
                }
                if (this.uDateBreakDownTime.Value != null && !this.uDateBreakDownTime.Value.ToString().Equals(string.Empty))
                {
                    strReqTime = this.uDateBreakDownTime.DateTime.ToString("HH:mm:ss"); //DateTime.Now.ToString("HH:mm:ss");     //요청시간
                    strBreakTime = this.uDateBreakDownTime.DateTime.ToString("HH:mm:ss");   //고장발생시각
                }

                string strProduct = this.uTextProductCode.Text;                 //제품코드
                string strRepairReqReson = this.uTextRepairReqReason.Text;      //요청사유
                string strUrgent = this.uCheckUrgentFlag.Checked.Equals(false) ? "F" : "T"; //긴급처리
                string strAutoAccept = this.uCheckAutoAccept.Checked.Equals(false) ? "F" : "T"; //자동접수처리

                #endregion

                #region 수리접수 정보

                //string strRequst = RepairRequestCode;

                DataTable dtRepair = mfReadData("AC");

                //dtRepair.Columns.Add("RepairReqCode");
                //dtRepair.Columns.Add("HeadFlag");
                //dtRepair.Columns.Add("StageDesc");
                //dtRepair.Columns.Add("ReceiptDesc");

                //DataRow rwReqCode = dtRepair.NewRow();

                //rwReqCode["RepairReqCode"] = RepairRequestCode;
                //rwReqCode["HeadFlag"] = uCheckHeadFlag.Checked == true ? "T" : "F";
                //rwReqCode["StageDesc"] = uTextStageDesc.Text;
                //rwReqCode["ReceiptDesc"] = uTextReceiptDesc.Text;

                //dtRepair.Rows.Add(rwReqCode);

                #endregion

                #region 수리결과정보

                string strRepairReqCode = RepairRequestCode;
                string strRepairStartDate = Convert.ToDateTime(uDateRepairDate.Value).ToString("yyyy-MM-dd");
                //string strRepairStartTime = Convert.ToDateTime(uDateRepairStartTime.Value).ToString("HH:mm:ss");
                string strRepairEndDate = Convert.ToDateTime(uDateRepairDate.Value).ToString("yyyy-MM-dd");
                string strRepairEndTime = Convert.ToDateTime(uDateRepairEndTime.Value).ToString("HH:mm:ss");
                string strRepairUserID = uTextRepairUserID.Text;
                string strRepairResultCode = "GE";//uComboRepairResult.Value.ToString();
                string strResultDownCode = this.uTextFaultTypeCode.Text;
                string strResultDownCodeName = this.uTextFaultTypeName.Text;
                string strCCSReqFlag = uCheckCCSReqFlag.Checked == true ? "T" : "F";
                string strRepairDesc = uTextRepairDesc.Text;
                string strImportantFlag = this.uCheckImportant.Checked == true ? "T" : "F";
                #endregion


                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();

                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtMESInfo = clsRepairReq.mfSetMESData();

                DataRow drMES = dtMESInfo.NewRow();

                drMES["EQPID"] = strEquipCode;
                drMES["EQPSTATE"] = strDownCode.Substring(0, 1) == "D" ? "MDC" : "DOWN";
                drMES["PRODUCTSPECID"] = strProduct;
                drMES["MACHINERECIPE"] = "";
                drMES["REASONCODE"] = strDownCode;
                drMES["EMERGENCYFLAG"] = strUrgent == "T" ? "Y" : "N";
                drMES["COMMENT"] = strRepairDesc;
                drMES["TOOLLIST"] = "";

                dtMESInfo.Rows.Add(drMES);

                return strErrRtn = clsRepairReq.mfSaveEquipRepair_EquipState(strReceive, RepairRequestCode, strPlantCode, strEquipCode, strReqDate, strReqTime, strRepairReqID,
                                                                                strBreakDate, strBreakTime, strProduct, strRepairReqReson, strUrgent, strDownCode, strAutoAccept,
                                                                                strRepairEndDate, strRepairEndTime, strRepairUserID, strRepairResultCode, strResultDownCode,
                                                                                strResultDownCodeName, strCCSReqFlag, strRepairDesc, strImportantFlag, dtRepair, dtMESInfo, this.Name,
                                                                                m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strErrRtn;
            }
            finally
            { }
        }



        /// <summary>
        /// CCS 의뢰상태여부 검색 (의뢰상태면 요청,접수,결과등록 불가처리)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <returns>true : CCS 의뢰상태</returns>
        private bool CheckCCS(string strPlantCode, string strEquipCode)
        {
            try
            {
                bool bolChk = false;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.RepairDIS), "RepairDIS");
                QRPEQU.BL.EQUDIS.RepairDIS clsEquip = new QRPEQU.BL.EQUDIS.RepairDIS();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadRepair_MESDB(strPlantCode, "", "", "", strEquipCode, "", "", m_resSys.GetString("SYS_LANG"));

                //CCS 의뢰상태인 경우 요청 불가처리 2012-10-06
                if (dtEquip.Rows.Count > 0 && dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))
                    bolChk = true;


                return bolChk;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally 
            { }
        }














    }
}
