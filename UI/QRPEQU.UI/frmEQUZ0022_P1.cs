﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0022D1.cs                                      */
/* 프로그램명   : 다운코드 팝업창                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-12-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0022_P1 : Form
    {
        #region 전역변수
        //리소스 호출을 위한 전연변수
        QRPGlobal SysRes = new QRPGlobal();

        // 공장 설비 DownCode, DownName
        private string strPlantCode;
        private string strEquipCode;
        private string strDownCode;
        private string strDownName;
        private string strReasonCode;

        public string ReasonCode
        {
            get { return strReasonCode; }
            set { strReasonCode = value; }
        }

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string EquipCode
        {
            get { return strEquipCode; }
            set { strEquipCode = value; }
        }
        public string DownCode
        {
            get { return strDownCode; }
            set { strDownCode = value; }
        }
        
        public string DownName
        {
            get { return strDownName; }
            set { strDownName = value; }
        }

        #endregion
        
        public frmEQUZ0022_P1()
        {
            InitializeComponent();
        }

        private void frmEQUZ0022D1_Load(object sender, EventArgs e)
        {
            InitTitle();
            InitButton();
            InitGroupBox();
            InitGrid();
            
            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);

            GridBind(1,"");

            
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("설비고장코드", m_resSys.GetString("SYS_FONTNAME"), 12);

               // m_resSys.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonOk, "선택", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCancel, "취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);

                //m_resSys.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grb = new WinGroupBox();

                grb.mfSetGroupBox(this.uGroupInfo, GroupBoxType.INFO, "원인정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grb.mfSetGroupBox(this.uGroupDetailInfo, GroupBoxType.INFO, "정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                uGroupInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupDetailInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupDetailInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //m_resSys.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                #region 원인정보
                grd.mfInitGeneralGrid(this.uGridCauseInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridCauseInfo, 0, "REASONCODE", "원인코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCauseInfo, 0, "DESCRIPTION", "설명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridCauseInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;

                #endregion

                #region 정보
                grd.mfInitGeneralGrid(this.uGridInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridInfo, 0, "REASONCODE", "원인코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "DESCRIPTION", "설명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 바인드
        /// </summary>
        private void GridBind(int intSeq,string strCauseCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (strPlantCode == null || strPlantCode.Equals(string.Empty) ||
                    strEquipCode == null || strEquipCode.Equals(string.Empty))
                    return;

                //사유코드 정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                QRPMAS.BL.MASPRC.ReasonCode clsReasonCode = new QRPMAS.BL.MASPRC.ReasonCode();
                brwChannel.mfCredentials(clsReasonCode);
                DataTable dtDownCode = new DataTable();

                if (intSeq.Equals(1))
                {
                    //DownCode조회매서드 실행
                    dtDownCode = clsReasonCode.mfReadReasonCode_DownCode(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //그리드에 바인드
                    this.uGridCauseInfo.DataSource = dtDownCode;
                    this.uGridCauseInfo.DataBind();
                }
                else
                {
                    //DownCode 상세조회 매서드 실행
                    dtDownCode = clsReasonCode.mfReadReasonCode_DownCodeInfo(strPlantCode, strEquipCode, strCauseCode);

                    //그리드에 바인드
                    this.uGridInfo.DataSource = dtDownCode;
                    this.uGridInfo.DataBind();
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Event
        
        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridCauseInfo_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                //원인코드 저장
                string strCode = e.Cell.Row.Cells["REASONCODE"].Value.ToString();
                
                if (!strCode.Equals(string.Empty))
                {
                    strReasonCode = strCode;
                    GridBind(2, strCode);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridInfo.ActiveRow == null) return;

                if (this.uGridInfo.ActiveRow.Index >= 0)
                {
                    strDownCode = this.uGridInfo.ActiveRow.Cells["REASONCODE"].Text.ToString();
                    strDownName = this.uGridInfo.ActiveRow.Cells["DESCRIPTION"].Text.ToString();
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridInfo_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strDownCode = e.Row.Cells["REASONCODE"].Text.ToString();
                strDownName = e.Row.Cells["DESCRIPTION"].Text.ToString();
                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion
    }
}
