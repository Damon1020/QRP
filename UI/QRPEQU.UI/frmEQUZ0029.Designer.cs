﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0029
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement2 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect2 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0029));
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.uChartG = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboQRP = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelQRP = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionSelect = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uChartC = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uComboDownCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUesr = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDownCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchYear = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridTotal = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uChartG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboQRP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // uChartG
            // 
            this.uChartG.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartG.Axis.PE = paintElement1;
            this.uChartG.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartG.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartG.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartG.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartG.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.X.LineThickness = 1;
            this.uChartG.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartG.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.X.MajorGridLines.Visible = true;
            this.uChartG.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartG.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.X.MinorGridLines.Visible = false;
            this.uChartG.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartG.Axis.X.Visible = true;
            this.uChartG.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartG.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartG.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartG.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartG.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartG.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.X2.Labels.Visible = false;
            this.uChartG.Axis.X2.LineThickness = 1;
            this.uChartG.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartG.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.X2.MajorGridLines.Visible = true;
            this.uChartG.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartG.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.X2.MinorGridLines.Visible = false;
            this.uChartG.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartG.Axis.X2.Visible = false;
            this.uChartG.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartG.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartG.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartG.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartG.Axis.Y.Labels.SeriesLabels.FormatString = "";
            this.uChartG.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartG.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartG.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Y.LineThickness = 1;
            this.uChartG.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartG.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Y.MajorGridLines.Visible = true;
            this.uChartG.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartG.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Y.MinorGridLines.Visible = false;
            this.uChartG.Axis.Y.TickmarkInterval = 20;
            this.uChartG.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartG.Axis.Y.Visible = true;
            this.uChartG.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartG.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartG.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartG.Axis.Y2.Labels.SeriesLabels.FormatString = "";
            this.uChartG.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartG.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Y2.Labels.Visible = false;
            this.uChartG.Axis.Y2.LineThickness = 1;
            this.uChartG.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartG.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartG.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartG.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartG.Axis.Y2.TickmarkInterval = 20;
            this.uChartG.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartG.Axis.Y2.Visible = false;
            this.uChartG.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartG.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.Z.Labels.ItemFormatString = "";
            this.uChartG.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartG.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Z.LineThickness = 1;
            this.uChartG.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartG.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Z.MajorGridLines.Visible = true;
            this.uChartG.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartG.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Z.MinorGridLines.Visible = false;
            this.uChartG.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartG.Axis.Z.Visible = false;
            this.uChartG.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartG.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartG.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartG.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartG.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartG.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartG.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartG.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartG.Axis.Z2.Labels.Visible = false;
            this.uChartG.Axis.Z2.LineThickness = 1;
            this.uChartG.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartG.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartG.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartG.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartG.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartG.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartG.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartG.Axis.Z2.Visible = false;
            this.uChartG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartG.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartG.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartG.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartG.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartG.Effects.Effects.Add(gradientEffect1);
            this.uChartG.Location = new System.Drawing.Point(0, 120);
            this.uChartG.Name = "uChartG";
            this.uChartG.Size = new System.Drawing.Size(1070, 316);
            this.uChartG.TabIndex = 4;
            this.uChartG.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartG.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboQRP);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelQRP);
            this.uGroupBoxSearchArea.Controls.Add(this.uOptionSelect);
            this.uGroupBoxSearchArea.Controls.Add(this.uChartC);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboDownCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelUesr);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDownCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uComboQRP
            // 
            this.uComboQRP.Location = new System.Drawing.Point(708, 56);
            this.uComboQRP.MaxLength = 50;
            this.uComboQRP.Name = "uComboQRP";
            this.uComboQRP.Size = new System.Drawing.Size(120, 21);
            this.uComboQRP.TabIndex = 30;
            // 
            // uLabelQRP
            // 
            this.uLabelQRP.Location = new System.Drawing.Point(604, 56);
            this.uLabelQRP.Name = "uLabelQRP";
            this.uLabelQRP.Size = new System.Drawing.Size(100, 20);
            this.uLabelQRP.TabIndex = 29;
            // 
            // uOptionSelect
            // 
            this.uOptionSelect.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSelect.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "0";
            valueListItem1.DisplayText = "정비사";
            valueListItem2.DataValue = "1";
            valueListItem2.DisplayText = "설비";
            this.uOptionSelect.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionSelect.Location = new System.Drawing.Point(768, 20);
            this.uOptionSelect.Name = "uOptionSelect";
            this.uOptionSelect.Size = new System.Drawing.Size(104, 16);
            this.uOptionSelect.TabIndex = 28;
            this.uOptionSelect.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            //			'UltraChart' properties's serialization: Since 'ChartType' changes the way axes look,
            //			'ChartType' must be persisted ahead of any Axes change made in design time.
            //		
            this.uChartC.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.PieChart;
            // 
            // uChartC
            // 
            this.uChartC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uChartC.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement2.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement2.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartC.Axis.PE = paintElement2;
            this.uChartC.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartC.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartC.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartC.Axis.X.Labels.SeriesLabels.FormatString = "";
            this.uChartC.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.X.LineThickness = 1;
            this.uChartC.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartC.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.X.MajorGridLines.Visible = true;
            this.uChartC.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartC.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.X.MinorGridLines.Visible = false;
            this.uChartC.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartC.Axis.X.Visible = true;
            this.uChartC.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartC.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.X2.Labels.ItemFormatString = "";
            this.uChartC.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartC.Axis.X2.Labels.SeriesLabels.FormatString = "";
            this.uChartC.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.X2.Labels.Visible = false;
            this.uChartC.Axis.X2.LineThickness = 1;
            this.uChartC.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartC.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.X2.MajorGridLines.Visible = true;
            this.uChartC.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartC.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.X2.MinorGridLines.Visible = false;
            this.uChartC.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartC.Axis.X2.Visible = false;
            this.uChartC.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartC.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartC.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartC.Axis.Y.Labels.SeriesLabels.FormatString = "";
            this.uChartC.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Y.LineThickness = 1;
            this.uChartC.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartC.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Y.MajorGridLines.Visible = true;
            this.uChartC.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartC.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Y.MinorGridLines.Visible = false;
            this.uChartC.Axis.Y.TickmarkInterval = 10;
            this.uChartC.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartC.Axis.Y.Visible = true;
            this.uChartC.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartC.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Y2.Labels.ItemFormatString = "";
            this.uChartC.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartC.Axis.Y2.Labels.SeriesLabels.FormatString = "";
            this.uChartC.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Y2.Labels.Visible = false;
            this.uChartC.Axis.Y2.LineThickness = 1;
            this.uChartC.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartC.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartC.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartC.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartC.Axis.Y2.TickmarkInterval = 10;
            this.uChartC.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartC.Axis.Y2.Visible = false;
            this.uChartC.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartC.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Z.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartC.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartC.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Z.Labels.Visible = false;
            this.uChartC.Axis.Z.LineThickness = 1;
            this.uChartC.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartC.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Z.MajorGridLines.Visible = true;
            this.uChartC.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartC.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Z.MinorGridLines.Visible = false;
            this.uChartC.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartC.Axis.Z.Visible = false;
            this.uChartC.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartC.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Z2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartC.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartC.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartC.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartC.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartC.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartC.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartC.Axis.Z2.Labels.Visible = false;
            this.uChartC.Axis.Z2.LineThickness = 1;
            this.uChartC.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartC.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartC.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartC.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartC.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartC.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartC.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartC.Axis.Z2.Visible = false;
            this.uChartC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartC.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartC.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartC.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartC.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartC.Effects.Effects.Add(gradientEffect2);
            this.uChartC.Location = new System.Drawing.Point(956, 28);
            this.uChartC.Name = "uChartC";
            this.uChartC.Size = new System.Drawing.Size(92, 44);
            this.uChartC.TabIndex = 3;
            this.uChartC.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartC.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            this.uChartC.Visible = false;
            // 
            // uComboDownCode
            // 
            this.uComboDownCode.Location = new System.Drawing.Point(116, 56);
            this.uComboDownCode.Name = "uComboDownCode";
            this.uComboDownCode.Size = new System.Drawing.Size(120, 21);
            this.uComboDownCode.TabIndex = 27;
            // 
            // uTextUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance2;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(496, 56);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 25;
            // 
            // uTextUserID
            // 
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton1);
            this.uTextUserID.Location = new System.Drawing.Point(392, 56);
            this.uTextUserID.MaxLength = 20;
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 26;
            this.uTextUserID.ValueChanged += new System.EventHandler(this.uTextUserID_ValueChanged);
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uLabelUesr
            // 
            this.uLabelUesr.Location = new System.Drawing.Point(288, 56);
            this.uLabelUesr.Name = "uLabelUesr";
            this.uLabelUesr.Size = new System.Drawing.Size(100, 20);
            this.uLabelUesr.TabIndex = 24;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(628, 32);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipGroup.TabIndex = 23;
            // 
            // uComboSearchEquipLargeType
            // 
            this.uComboSearchEquipLargeType.Location = new System.Drawing.Point(392, 32);
            this.uComboSearchEquipLargeType.MaxLength = 50;
            this.uComboSearchEquipLargeType.Name = "uComboSearchEquipLargeType";
            this.uComboSearchEquipLargeType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLargeType.TabIndex = 22;
            this.uComboSearchEquipLargeType.ValueChanged += new System.EventHandler(this.uComboSearchEquType_ValueChanged);
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 32);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboProcessGroup.TabIndex = 21;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(628, 8);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLoc.TabIndex = 20;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(392, 8);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchStation.TabIndex = 19;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 32);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 15;
            // 
            // uLabelDownCode
            // 
            this.uLabelDownCode.Location = new System.Drawing.Point(12, 56);
            this.uLabelDownCode.Name = "uLabelDownCode";
            this.uLabelDownCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelDownCode.TabIndex = 14;
            // 
            // uLabelEquipLoc
            // 
            this.uLabelEquipLoc.Location = new System.Drawing.Point(524, 8);
            this.uLabelEquipLoc.Name = "uLabelEquipLoc";
            this.uLabelEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLoc.TabIndex = 14;
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(288, 32);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 16;
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(524, 32);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroup.TabIndex = 18;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(288, 8);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 17;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1032, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(28, 21);
            this.uComboSearchPlant.TabIndex = 13;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1020, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(8, 20);
            this.uLabelSearchPlant.TabIndex = 12;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uComboSearchYear
            // 
            this.uComboSearchYear.Location = new System.Drawing.Point(116, 8);
            this.uComboSearchYear.MaxLength = 5;
            this.uComboSearchYear.Name = "uComboSearchYear";
            this.uComboSearchYear.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchYear.TabIndex = 4;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 3;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 5;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridTotal
            // 
            this.uGridTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridTotal.DisplayLayout.Appearance = appearance4;
            this.uGridTotal.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridTotal.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTotal.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTotal.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridTotal.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTotal.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridTotal.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridTotal.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridTotal.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridTotal.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridTotal.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridTotal.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridTotal.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridTotal.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGridTotal.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridTotal.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTotal.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGridTotal.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridTotal.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridTotal.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridTotal.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridTotal.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridTotal.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridTotal.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTotal.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTotal.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridTotal.Location = new System.Drawing.Point(0, 436);
            this.uGridTotal.Name = "uGridTotal";
            this.uGridTotal.Size = new System.Drawing.Size(1070, 376);
            this.uGridTotal.TabIndex = 6;
            // 
            // frmEQUZ0029
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridTotal);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uChartG);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0029";
            this.Load += new System.EventHandler(this.frmEQUZ0029_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0029_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0029_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uChartG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboQRP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinChart.UltraChart uChartG;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboQRP;
        private Infragistics.Win.Misc.UltraLabel uLabelQRP;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSelect;
        private Infragistics.Win.UltraWinChart.UltraChart uChartC;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDownCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelUesr;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelDownCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTotal;

    }
}