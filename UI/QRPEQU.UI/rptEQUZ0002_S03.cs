﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Data;

namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_S03.
    /// </summary>
    public partial class rptEQUZ0002_S03 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0002_S03()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        public rptEQUZ0002_S03(DataTable dtMainSubEquip)
        {
            
            this.DataSource = dtMainSubEquip;
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            if (dtMainSubEquip.Rows.Count > 0)
            {
                this.txtEquipStandard.Text = dtMainSubEquip.Rows[0]["EquipStandard"].ToString();
                this.txtWorkStandard.Text = dtMainSubEquip.Rows[0]["WorkStandard"].ToString();
                this.textTechSrandard.Text = dtMainSubEquip.Rows[0]["TechStandard"].ToString();
                this.textContents.Text = dtMainSubEquip.Rows[0]["Object"].ToString();
            }
        }
    }
}
