﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0010_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptViewDoc = new DataDynamics.ActiveReports.Viewer.Viewer();
            this.SuspendLayout();
            // 
            // rptViewDoc
            // 
            this.rptViewDoc.BackColor = System.Drawing.SystemColors.Control;
            this.rptViewDoc.Document = new DataDynamics.ActiveReports.Document.Document("ARNet Document");
            this.rptViewDoc.Location = new System.Drawing.Point(0, 8);
            this.rptViewDoc.Name = "rptViewDoc";
            this.rptViewDoc.ReportViewer.CurrentPage = 0;
            this.rptViewDoc.ReportViewer.MultiplePageCols = 3;
            this.rptViewDoc.ReportViewer.MultiplePageRows = 2;
            this.rptViewDoc.ReportViewer.ViewType = DataDynamics.ActiveReports.Viewer.ViewType.Normal;
            this.rptViewDoc.Size = new System.Drawing.Size(728, 564);
            this.rptViewDoc.TabIndex = 1;
            this.rptViewDoc.TableOfContents.Text = "Table Of Contents";
            this.rptViewDoc.TableOfContents.Width = 200;
            this.rptViewDoc.TabTitleLength = 35;
            this.rptViewDoc.Toolbar.Font = new System.Drawing.Font("굴림", 9F);
            // 
            // frmEQUZ0010_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 583);
            this.Controls.Add(this.rptViewDoc);
            this.Name = "frmEQUZ0010_Report";
            this.Text = "frmEQUZ0010_Report";
            this.ResumeLayout(false);

        }

        #endregion

        private DataDynamics.ActiveReports.Viewer.Viewer rptViewDoc;
    }
}