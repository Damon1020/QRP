﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using System.Data;
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Threading;
using System.Resources;
using System.EnterpriseServices;


namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_01.
    /// </summary>
    public partial class rptEQUZ0002_Main : DataDynamics.ActiveReports.ActiveReport
    {
        
        public rptEQUZ0002_Main()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            
        }
        public rptEQUZ0002_Main(DataTable dtSetupCondition, DataTable dtCertiItem, DataTable dtMainSubEquip)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            subReport1.Report = new rptEQUZ0002_S02(dtSetupCondition, dtMainSubEquip.Rows.Count);
            subReport.Report = new rptEQUZ0002_S01(dtCertiItem,dtMainSubEquip.Rows.Count);
            subReport2.Report = new rptEQUZ0002_S03(dtMainSubEquip);

        }

        private void rptEQUZ0002_01_ReportStart(object sender, EventArgs e)
        {

          
        }

        //리포트 헤더부분의 데이터들을 삽입하는 경우 
        private void reportHeader1_Format(object sender, EventArgs e)
        {
            try
            {
                //필요정보 저장
                string strPlantCode = txtPlantCode.Text;
                string strEquipCode = txtEquipCode.Text;
                string strUserID = txtCertiReqID.Text;

                #region SubReport

                //QRPBrowser brwChannel = new QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupCondition), "EQUSetupCondition");
                //QRPEQU.BL.EQUCCS.EQUSetupCondition clsEQUSetupCondition = new QRPEQU.BL.EQUCCS.EQUSetupCondition();
                //brwChannel.mfCredentials(clsEQUSetupCondition);

                //DataTable dtCondition = clsEQUSetupCondition.mfReadSetupCondition(txtPlantCode.Text, txtDocCode.Text, "KOR");

                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItem), "EQUEquipCertiItem");
                //QRPEQU.BL.EQUCCS.EQUEquipCertiItem clsCertiItem = new QRPEQU.BL.EQUCCS.EQUEquipCertiItem();
                //brwChannel.mfCredentials(clsCertiItem);

                ////지금의 ActiveReport의 Detail안에있는 서브리포터를 rptEQUZ0002_02,03으로한다
                //DataTable dtItem = clsCertiItem.mfReadCertiItem(txtPlantCode.Text, txtDocCode.Text, "KOR");
                //subReport1.Report = new rptEQUZ0002_S02(dtCondition);
                //subReport.Report = new rptEQUZ0002_S01(dtItem);

                #endregion

                #region 설비정보

                QRPGlobal SysRes = new QRPGlobal();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //설비정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();

                #endregion

                #region 의뢰자정보

                //유저정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                //유저정보조회 매서드 호출
                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                //의뢰부서 의뢰자 삽입
                if (dtUser.Rows.Count > 0)
                {
                    textDept.Text = dtUser.Rows[0]["DeptName"].ToString();
                    textCerReqName.Text = dtUser.Rows[0]["UserName"].ToString();
                }

                #endregion

                #region 설비인증의뢰유형

                //설비인증의뢰유형
                string strReqTypeCode = txtEquipCertiReqTypeCode.Text;
                // 값이 널인 경우 패스
                if (strReqTypeCode == null)
                    return;

                //01 인경우 신규설비 체크박스에 체크한다.
                if (strReqTypeCode.Equals("01"))
                    this.chbEquipCertiReqTypeCode01.Checked = true;

                //02 인경우 이전설비 체크박스에 체크한다.
                if (strReqTypeCode.Equals("02"))
                    this.chbEquipCertiReqTypeCode02.Checked = true;

                //03 인 경우 개조설비 체크박스에 체크한다.
                if (strReqTypeCode.Equals("03"))
                    this.chbEquipCertiReqTypeCode03.Checked = true;

                //04 인 경우 신금형(신다이) 체크박스에 체크한다.
                if (strReqTypeCode.Equals("04"))
                    this.chbEquipCertiReqTypeCode04.Checked = true;

                //05인 경우 재인증체크박스에 체크한다.
                if (strReqTypeCode.Equals("05"))
                    this.chbEquipCertiReqTypeCode05.Checked = true;

                //06인 경우 운휴설비체크박스에 체크한다.
                if (strReqTypeCode.Equals("06"))
                    this.chbEquipCertiReqTypeCode06.Checked = true;

                #endregion

                //공정구분정보가 널이 아닌 경우 타이틀의 공정구분 텍스트박스에 삽입한다
                string strProcGubun = this.textProc.Text;

                if (strProcGubun == null)
                    return;
                else
                    labelTitleProcGubun.Text = "(" + strProcGubun + ")";
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        //groupHeader1_Format 부분의 Data를 삽입하는 경우
        private void groupHeader1_Format(object sender, EventArgs e)
        {

        }

        private void rptEQUZ0002_Main_ReportEnd(object sender, EventArgs e)
        {
            //t this.PageNumber
        }
    }
}
