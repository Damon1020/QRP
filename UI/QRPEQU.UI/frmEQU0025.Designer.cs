﻿namespace QRPEQU.UI
{
    partial class frmEQU0025
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0025));
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uComboSPInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSPInventoryCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDiscardChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDiscardChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextDiscardReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDiscardReason = new Infragistics.Win.Misc.UltraLabel();
            this.uDateDiscardDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDiscardChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDiscardDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToDiscardDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDiscardDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDiscardDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDiscardDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDiscardDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 18;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboSPInventory);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSPInventoryCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDiscardChargeID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDiscardChargeName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDiscardReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDiscardReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateDiscardDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDiscardChargeID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDiscardDate);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uComboSPInventory
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSPInventory.Appearance = appearance2;
            this.uComboSPInventory.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSPInventory.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSPInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboSPInventory.Location = new System.Drawing.Point(116, 36);
            this.uComboSPInventory.MaxLength = 50;
            this.uComboSPInventory.Name = "uComboSPInventory";
            this.uComboSPInventory.Size = new System.Drawing.Size(120, 19);
            this.uComboSPInventory.TabIndex = 101;
            this.uComboSPInventory.Text = "ultraComboEditor1";
            this.uComboSPInventory.AfterCloseUp += new System.EventHandler(this.uComboSPInventory_AfterCloseUp);
            this.uComboSPInventory.ValueChanged += new System.EventHandler(this.uComboSPInventory_ValueChanged);
            // 
            // uLabelSPInventoryCode
            // 
            this.uLabelSPInventoryCode.Location = new System.Drawing.Point(12, 36);
            this.uLabelSPInventoryCode.Name = "uLabelSPInventoryCode";
            this.uLabelSPInventoryCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSPInventoryCode.TabIndex = 100;
            this.uLabelSPInventoryCode.Text = "ultraLabel1";
            // 
            // uTextDiscardChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDiscardChargeID.Appearance = appearance15;
            this.uTextDiscardChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDiscardChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDiscardChargeID.ButtonsRight.Add(editorButton1);
            this.uTextDiscardChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDiscardChargeID.Location = new System.Drawing.Point(404, 12);
            this.uTextDiscardChargeID.MaxLength = 20;
            this.uTextDiscardChargeID.Name = "uTextDiscardChargeID";
            this.uTextDiscardChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextDiscardChargeID.TabIndex = 99;
            this.uTextDiscardChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDiscardChargeID_KeyDown);
            this.uTextDiscardChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDiscardChargeID_EditorButtonClick);
            // 
            // uTextDiscardChargeName
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDiscardChargeName.Appearance = appearance67;
            this.uTextDiscardChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDiscardChargeName.Location = new System.Drawing.Point(508, 12);
            this.uTextDiscardChargeName.Name = "uTextDiscardChargeName";
            this.uTextDiscardChargeName.ReadOnly = true;
            this.uTextDiscardChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextDiscardChargeName.TabIndex = 98;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uGrid2);
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox2.Location = new System.Drawing.Point(8, 80);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1052, 608);
            this.uGroupBox2.TabIndex = 47;
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance22;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance29.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance28;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 36);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1032, 560);
            this.uGrid2.TabIndex = 46;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 45;
            this.uButtonDeleteRow.Text = "행삭제";
            // 
            // uTextDiscardReason
            // 
            this.uTextDiscardReason.Location = new System.Drawing.Point(404, 36);
            this.uTextDiscardReason.MaxLength = 1000;
            this.uTextDiscardReason.Name = "uTextDiscardReason";
            this.uTextDiscardReason.Size = new System.Drawing.Size(384, 21);
            this.uTextDiscardReason.TabIndex = 45;
            // 
            // uLabelDiscardReason
            // 
            this.uLabelDiscardReason.Location = new System.Drawing.Point(300, 36);
            this.uLabelDiscardReason.Name = "uLabelDiscardReason";
            this.uLabelDiscardReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelDiscardReason.TabIndex = 43;
            this.uLabelDiscardReason.Text = "ultraLabel6";
            // 
            // uDateDiscardDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDiscardDate.Appearance = appearance16;
            this.uDateDiscardDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDiscardDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateDiscardDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDiscardDate.Location = new System.Drawing.Point(116, 12);
            this.uDateDiscardDate.Name = "uDateDiscardDate";
            this.uDateDiscardDate.Size = new System.Drawing.Size(100, 19);
            this.uDateDiscardDate.TabIndex = 40;
            // 
            // uLabelDiscardChargeID
            // 
            this.uLabelDiscardChargeID.Location = new System.Drawing.Point(300, 12);
            this.uLabelDiscardChargeID.Name = "uLabelDiscardChargeID";
            this.uLabelDiscardChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelDiscardChargeID.TabIndex = 39;
            this.uLabelDiscardChargeID.Text = "ultraLabel3";
            // 
            // uLabelDiscardDate
            // 
            this.uLabelDiscardDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelDiscardDate.Name = "uLabelDiscardDate";
            this.uLabelDiscardDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelDiscardDate.TabIndex = 38;
            this.uLabelDiscardDate.Text = "ultraLabel4";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance5;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 17;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDiscardDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDiscardDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDiscardDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 16;
            // 
            // ultraLabel2
            // 
            appearance32.TextHAlignAsString = "Center";
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance32;
            this.ultraLabel2.Location = new System.Drawing.Point(508, 14);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel2.TabIndex = 7;
            this.ultraLabel2.Text = "~";
            // 
            // uDateToDiscardDate
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToDiscardDate.Appearance = appearance31;
            this.uDateToDiscardDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToDiscardDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateToDiscardDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToDiscardDate.Location = new System.Drawing.Point(524, 12);
            this.uDateToDiscardDate.MaskInput = "";
            this.uDateToDiscardDate.Name = "uDateToDiscardDate";
            this.uDateToDiscardDate.Size = new System.Drawing.Size(100, 19);
            this.uDateToDiscardDate.TabIndex = 4;
            // 
            // uDateFromDiscardDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromDiscardDate.Appearance = appearance4;
            this.uDateFromDiscardDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromDiscardDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateFromDiscardDate.DateTime = new System.DateTime(2011, 6, 1, 0, 0, 0, 0);
            this.uDateFromDiscardDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromDiscardDate.Location = new System.Drawing.Point(404, 12);
            this.uDateFromDiscardDate.MaskInput = "";
            this.uDateFromDiscardDate.Name = "uDateFromDiscardDate";
            this.uDateFromDiscardDate.Size = new System.Drawing.Size(100, 19);
            this.uDateFromDiscardDate.TabIndex = 3;
            this.uDateFromDiscardDate.Value = new System.DateTime(2011, 6, 1, 0, 0, 0, 0);
            // 
            // uLabelSearchDiscardDate
            // 
            this.uLabelSearchDiscardDate.Location = new System.Drawing.Point(300, 12);
            this.uLabelSearchDiscardDate.Name = "uLabelSearchDiscardDate";
            this.uLabelSearchDiscardDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDiscardDate.TabIndex = 2;
            this.uLabelSearchDiscardDate.Text = "ultraLabel2";
            // 
            // uComboPlant
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance3;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 15;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQU0025
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0025";
            this.Load += new System.EventHandler(this.frmEQU0025_Load);
            this.Activated += new System.EventHandler(this.frmEQU0025_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0025_FormClosing);
            this.Resize += new System.EventHandler(this.frmEQU0025_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDiscardDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDiscardDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardReason;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardReason;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDiscardDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDiscardDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDiscardDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDiscardDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardChargeName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSPInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelSPInventoryCode;

    }
}
