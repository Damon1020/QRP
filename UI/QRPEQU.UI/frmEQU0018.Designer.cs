﻿namespace QRPEQU.UI
{
    partial class frmEQU0018
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0018));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextWorkerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWorkerID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboWeek = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelWeek = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uWeekView = new Infragistics.Win.UltraWinSchedule.UltraWeekView();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uWeekView)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uNumYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextWorkerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextWorkerID);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel4);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboWeek);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelWeek);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uNumYear
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumYear.Appearance = appearance6;
            this.uNumYear.BackColor = System.Drawing.Color.PowderBlue;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumYear.ButtonsLeft.Add(editorButton1);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumYear.ButtonsRight.Add(spinEditorButton1);
            this.uNumYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumYear.Location = new System.Drawing.Point(116, 12);
            this.uNumYear.MaxValue = 9999;
            this.uNumYear.MinValue = 1998;
            this.uNumYear.Name = "uNumYear";
            this.uNumYear.PromptChar = ' ';
            this.uNumYear.Size = new System.Drawing.Size(96, 21);
            this.uNumYear.TabIndex = 11;
            this.uNumYear.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumYear_EditorSpinButtonClick);
            this.uNumYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uNumYear_KeyDown);
            this.uNumYear.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumYear_EditorButtonClick);
            // 
            // uTextWorkerName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkerName.Appearance = appearance3;
            this.uTextWorkerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkerName.Location = new System.Drawing.Point(220, 36);
            this.uTextWorkerName.Name = "uTextWorkerName";
            this.uTextWorkerName.ReadOnly = true;
            this.uTextWorkerName.Size = new System.Drawing.Size(100, 21);
            this.uTextWorkerName.TabIndex = 10;
            // 
            // uTextWorkerID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkerID.Appearance = appearance2;
            this.uTextWorkerID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkerID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance5;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWorkerID.ButtonsRight.Add(editorButton2);
            this.uTextWorkerID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkerID.Location = new System.Drawing.Point(116, 36);
            this.uTextWorkerID.MaxLength = 20;
            this.uTextWorkerID.Name = "uTextWorkerID";
            this.uTextWorkerID.Size = new System.Drawing.Size(100, 19);
            this.uTextWorkerID.TabIndex = 9;
            this.uTextWorkerID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWorkerID_KeyDown);
            this.uTextWorkerID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWorkerID_EditorButtonClick);
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(12, 36);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel4.TabIndex = 8;
            this.ultraLabel4.Text = "ultraLabel4";
            // 
            // uComboWeek
            // 
            this.uComboWeek.Location = new System.Drawing.Point(668, 12);
            this.uComboWeek.MaxLength = 2;
            this.uComboWeek.Name = "uComboWeek";
            this.uComboWeek.Size = new System.Drawing.Size(72, 21);
            this.uComboWeek.TabIndex = 7;
            this.uComboWeek.ValueChanged += new System.EventHandler(this.uComboWeek_ValueChanged);
            // 
            // uLabelWeek
            // 
            this.uLabelWeek.Location = new System.Drawing.Point(564, 12);
            this.uLabelWeek.Name = "uLabelWeek";
            this.uLabelWeek.Size = new System.Drawing.Size(100, 20);
            this.uLabelWeek.TabIndex = 6;
            this.uLabelWeek.Text = "ultraLabel3";
            // 
            // uComboMonth
            // 
            this.uComboMonth.Location = new System.Drawing.Point(460, 12);
            this.uComboMonth.MaxLength = 3;
            this.uComboMonth.Name = "uComboMonth";
            this.uComboMonth.Size = new System.Drawing.Size(72, 21);
            this.uComboMonth.TabIndex = 5;
            this.uComboMonth.ValueChanged += new System.EventHandler(this.uComboMonth_ValueChanged);
            // 
            // uLabelMonth
            // 
            this.uLabelMonth.Location = new System.Drawing.Point(356, 12);
            this.uLabelMonth.Name = "uLabelMonth";
            this.uLabelMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelMonth.TabIndex = 4;
            this.uLabelMonth.Text = "ultraLabel2";
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(12, 12);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 2;
            this.uLabelYear.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uWeekView
            // 
            this.uWeekView.DayDisplayStyle = Infragistics.Win.UltraWinSchedule.DayDisplayStyleEnum.Full;
            this.uWeekView.Location = new System.Drawing.Point(1, 101);
            this.uWeekView.Name = "uWeekView";
            this.uWeekView.ScrollbarVisible = false;
            this.uWeekView.Size = new System.Drawing.Size(1071, 747);
            this.uWeekView.TabIndex = 4;
            this.uWeekView.BeforeScroll += new Infragistics.Win.UltraWinSchedule.BeforeScrollEventHandler(this.uWeekView_BeforeScroll);
            this.uWeekView.MoreActivityIndicatorClicked += new Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventHandler(this.uWeekView_MoreActivityIndicatorClicked);
            // 
            // frmEQU0018
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uWeekView);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0018";
            this.Load += new System.EventHandler(this.frmEQU0018_Load);
            this.Activated += new System.EventHandler(this.frmEQU0018_Activated);
            this.Resize += new System.EventHandler(this.frmEQU0018_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uWeekView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkerID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboWeek;
        private Infragistics.Win.Misc.UltraLabel uLabelWeek;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinSchedule.UltraWeekView uWeekView;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumYear;
    }
}