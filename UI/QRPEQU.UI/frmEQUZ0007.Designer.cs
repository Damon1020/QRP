﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0007
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0007));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uNumSearchYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextTechnician = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTechnicianName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTechnician = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPMPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearch2 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipMentGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearch1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uGridPMPlan = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPMPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMPlan)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uNumSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnician);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnicianName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelTechnician);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPMPeriod);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearch2);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipMentGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearch1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uComboSearchMonth
            // 
            this.uComboSearchMonth.Location = new System.Drawing.Point(212, 8);
            this.uComboSearchMonth.MaxLength = 5;
            this.uComboSearchMonth.Name = "uComboSearchMonth";
            this.uComboSearchMonth.Size = new System.Drawing.Size(60, 21);
            this.uComboSearchMonth.TabIndex = 23;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 56);
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(132, 21);
            this.uComboProcessGroup.TabIndex = 22;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboEquipLargeType
            // 
            this.uComboEquipLargeType.Location = new System.Drawing.Point(384, 56);
            this.uComboEquipLargeType.MaxLength = 50;
            this.uComboEquipLargeType.Name = "uComboEquipLargeType";
            this.uComboEquipLargeType.Size = new System.Drawing.Size(132, 21);
            this.uComboEquipLargeType.TabIndex = 20;
            this.uComboEquipLargeType.ValueChanged += new System.EventHandler(this.uComboEquType_ValueChanged);
            // 
            // uLabelEquType
            // 
            this.uLabelEquType.Location = new System.Drawing.Point(280, 56);
            this.uLabelEquType.Name = "uLabelEquType";
            this.uLabelEquType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquType.TabIndex = 21;
            // 
            // uComboEquipLoc
            // 
            this.uComboEquipLoc.Location = new System.Drawing.Point(652, 32);
            this.uComboEquipLoc.MaxLength = 50;
            this.uComboEquipLoc.Name = "uComboEquipLoc";
            this.uComboEquipLoc.Size = new System.Drawing.Size(132, 21);
            this.uComboEquipLoc.TabIndex = 18;
            this.uComboEquipLoc.ValueChanged += new System.EventHandler(this.uComboLoc_ValueChanged);
            // 
            // uLabelLoc
            // 
            this.uLabelLoc.Location = new System.Drawing.Point(548, 32);
            this.uLabelLoc.Name = "uLabelLoc";
            this.uLabelLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelLoc.TabIndex = 19;
            // 
            // uNumSearchYear
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumSearchYear.Appearance = appearance2;
            this.uNumSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Text = "0";
            this.uNumSearchYear.ButtonsLeft.Add(editorButton1);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumSearchYear.ButtonsRight.Add(spinEditorButton1);
            this.uNumSearchYear.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumSearchYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumSearchYear.Location = new System.Drawing.Point(116, 8);
            this.uNumSearchYear.MaskInput = "nnnnnnnnn";
            this.uNumSearchYear.MaxValue = 9999;
            this.uNumSearchYear.MinValue = 2000;
            this.uNumSearchYear.Name = "uNumSearchYear";
            this.uNumSearchYear.PromptChar = ' ';
            this.uNumSearchYear.Size = new System.Drawing.Size(92, 21);
            this.uNumSearchYear.TabIndex = 1;
            this.uNumSearchYear.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumSearchYear_EditorSpinButtonClick);
            this.uNumSearchYear.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumSearchYear_EditorButtonClick);
            // 
            // uTextTechnician
            // 
            appearance70.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance70.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance70;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTechnician.ButtonsRight.Add(editorButton2);
            this.uTextTechnician.Location = new System.Drawing.Point(652, 8);
            this.uTextTechnician.MaxLength = 20;
            this.uTextTechnician.Name = "uTextTechnician";
            this.uTextTechnician.Size = new System.Drawing.Size(100, 21);
            this.uTextTechnician.TabIndex = 4;
            this.uTextTechnician.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextTechnician_KeyDown);
            this.uTextTechnician.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextTechnician_EditorButtonClick);
            // 
            // uTextTechnicianName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Appearance = appearance3;
            this.uTextTechnicianName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Location = new System.Drawing.Point(756, 8);
            this.uTextTechnicianName.Name = "uTextTechnicianName";
            this.uTextTechnicianName.ReadOnly = true;
            this.uTextTechnicianName.Size = new System.Drawing.Size(100, 21);
            this.uTextTechnicianName.TabIndex = 17;
            // 
            // uLabelTechnician
            // 
            this.uLabelTechnician.Location = new System.Drawing.Point(548, 8);
            this.uLabelTechnician.Name = "uLabelTechnician";
            this.uLabelTechnician.Size = new System.Drawing.Size(100, 20);
            this.uLabelTechnician.TabIndex = 16;
            // 
            // uComboSearchPMPeriod
            // 
            this.uComboSearchPMPeriod.Location = new System.Drawing.Point(116, 32);
            this.uComboSearchPMPeriod.MaxLength = 7;
            this.uComboSearchPMPeriod.Name = "uComboSearchPMPeriod";
            this.uComboSearchPMPeriod.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPMPeriod.TabIndex = 8;
            // 
            // uLabelSearch2
            // 
            this.uLabelSearch2.Location = new System.Drawing.Point(12, 32);
            this.uLabelSearch2.Name = "uLabelSearch2";
            this.uLabelSearch2.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearch2.TabIndex = 14;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(652, 56);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchEquipGroup.TabIndex = 3;
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 56);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 12;
            // 
            // uLabelSearchEquipMentGroup
            // 
            this.uLabelSearchEquipMentGroup.Location = new System.Drawing.Point(548, 56);
            this.uLabelSearchEquipMentGroup.Name = "uLabelSearchEquipMentGroup";
            this.uLabelSearchEquipMentGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipMentGroup.TabIndex = 12;
            // 
            // uComboSearchProcGubun
            // 
            this.uComboSearchProcGubun.Location = new System.Drawing.Point(1000, 36);
            this.uComboSearchProcGubun.MaxLength = 50;
            this.uComboSearchProcGubun.Name = "uComboSearchProcGubun";
            this.uComboSearchProcGubun.Size = new System.Drawing.Size(12, 21);
            this.uComboSearchProcGubun.TabIndex = 7;
            this.uComboSearchProcGubun.Visible = false;
            // 
            // uLabelSearch1
            // 
            this.uLabelSearch1.Location = new System.Drawing.Point(988, 35);
            this.uLabelSearch1.Name = "uLabelSearch1";
            this.uLabelSearch1.Size = new System.Drawing.Size(10, 20);
            this.uLabelSearch1.TabIndex = 10;
            this.uLabelSearch1.Text = "ultraLabel2";
            this.uLabelSearch1.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(384, 33);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchStation.TabIndex = 6;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(280, 32);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 8;
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1020, 36);
            this.uComboSearchArea.MaxLength = 50;
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchArea.TabIndex = 5;
            this.uComboSearchArea.Visible = false;
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(1004, 36);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchArea.TabIndex = 6;
            this.uLabelSearchArea.Text = "ultraLabel2";
            this.uLabelSearchArea.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(384, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPlant.TabIndex = 2;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(280, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 2;
            // 
            // uGridPMPlan
            // 
            this.uGridPMPlan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMPlan.DisplayLayout.Appearance = appearance16;
            this.uGridPMPlan.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMPlan.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMPlan.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMPlan.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.uGridPMPlan.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMPlan.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGridPMPlan.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMPlan.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMPlan.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMPlan.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridPMPlan.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMPlan.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMPlan.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMPlan.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridPMPlan.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMPlan.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMPlan.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.uGridPMPlan.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridPMPlan.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMPlan.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMPlan.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridPMPlan.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMPlan.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGridPMPlan.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMPlan.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMPlan.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMPlan.Location = new System.Drawing.Point(0, 120);
            this.uGridPMPlan.Name = "uGridPMPlan";
            this.uGridPMPlan.Size = new System.Drawing.Size(1070, 715);
            this.uGridPMPlan.TabIndex = 4;
            this.uGridPMPlan.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPMPlan_AfterCellUpdate);
            this.uGridPMPlan.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPMPlan_ClickCellButton);
            this.uGridPMPlan.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // frmEQUZ0007
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridPMPlan);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0007";
            this.Load += new System.EventHandler(this.frmEQUZ0007_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0007_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0007_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPMPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMPlan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelSearch1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPMPeriod;
        private Infragistics.Win.Misc.UltraLabel uLabelSearch2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipMentGroup;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMPlan;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnician;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnicianName;
        private Infragistics.Win.Misc.UltraLabel uLabelTechnician;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMonth;
    }
}