﻿namespace QRPEQU.UI
{
    partial class frmEQU0027_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0027_S));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSparePartName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSparePartCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSPInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSparePart = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSPInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSparePart = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSparePartName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSparePartCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSparePart)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSparePartName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSparePartCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSPInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSparePart);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSPInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextSparePartName
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSparePartName.Appearance = appearance14;
            this.uTextSparePartName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSparePartName.Location = new System.Drawing.Point(848, 36);
            this.uTextSparePartName.Name = "uTextSparePartName";
            this.uTextSparePartName.ReadOnly = true;
            this.uTextSparePartName.Size = new System.Drawing.Size(100, 21);
            this.uTextSparePartName.TabIndex = 2;
            // 
            // uTextEquipName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance16;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(532, 36);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 2;
            // 
            // uTextSparePartCode
            // 
            appearance15.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSparePartCode.ButtonsRight.Add(editorButton1);
            this.uTextSparePartCode.Location = new System.Drawing.Point(744, 36);
            this.uTextSparePartCode.MaxLength = 20;
            this.uTextSparePartCode.Name = "uTextSparePartCode";
            this.uTextSparePartCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSparePartCode.TabIndex = 2;
            this.uTextSparePartCode.ValueChanged += new System.EventHandler(this.uTextSparePartCode_ValueChanged);
            this.uTextSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSparePartCode_KeyDown);
            this.uTextSparePartCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSparePartCode_EditorButtonClick);
            // 
            // uTextEquipCode
            // 
            appearance17.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance17.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance17;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton2);
            this.uTextEquipCode.Location = new System.Drawing.Point(428, 36);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 2;
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uComboSPInventory
            // 
            this.uComboSPInventory.Location = new System.Drawing.Point(116, 36);
            this.uComboSPInventory.MaxLength = 50;
            this.uComboSPInventory.Name = "uComboSPInventory";
            this.uComboSPInventory.Size = new System.Drawing.Size(200, 21);
            this.uComboSPInventory.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSparePart
            // 
            this.uLabelSparePart.Location = new System.Drawing.Point(640, 36);
            this.uLabelSparePart.Name = "uLabelSparePart";
            this.uLabelSparePart.Size = new System.Drawing.Size(100, 20);
            this.uLabelSparePart.TabIndex = 0;
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(324, 36);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquip.TabIndex = 0;
            // 
            // uLabelSPInventory
            // 
            this.uLabelSPInventory.Location = new System.Drawing.Point(12, 36);
            this.uLabelSPInventory.Name = "uLabelSPInventory";
            this.uLabelSPInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelSPInventory.TabIndex = 0;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uGridSparePart
            // 
            this.uGridSparePart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSparePart.DisplayLayout.Appearance = appearance5;
            this.uGridSparePart.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSparePart.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSparePart.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSparePart.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridSparePart.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSparePart.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridSparePart.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSparePart.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSparePart.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSparePart.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridSparePart.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSparePart.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSparePart.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSparePart.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridSparePart.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSparePart.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSparePart.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridSparePart.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridSparePart.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSparePart.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridSparePart.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridSparePart.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSparePart.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridSparePart.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSparePart.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSparePart.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSparePart.Location = new System.Drawing.Point(0, 100);
            this.uGridSparePart.Name = "uGridSparePart";
            this.uGridSparePart.Size = new System.Drawing.Size(1068, 740);
            this.uGridSparePart.TabIndex = 2;
            this.uGridSparePart.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridSparePart_DoubleClickRow);
            // 
            // frmEQU0027_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridSparePart);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0027_S";
            this.Load += new System.EventHandler(this.frmEQU0027_Load);
            this.Activated += new System.EventHandler(this.frmEQU0027_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0027_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSparePartName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSparePartCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSparePart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSparePart;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSparePartName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSparePartCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSPInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelSparePart;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelSPInventory;
    }
}