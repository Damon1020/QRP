﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0010));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboGIFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelGIFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableMatRepairH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridDurableMatD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uLabelReleaseComponent = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairGIReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairGIReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReleaseRequestUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairGIReqDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReleaseRequestDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipStopTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairResultName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairEndDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStopType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairDay = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairGICode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReleaseCode = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboGIFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatRepairH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairGIReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResultName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboGIFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelGIFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(428, 36);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboProcessGroup.TabIndex = 34;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboEquipLargeType
            // 
            this.uComboEquipLargeType.Location = new System.Drawing.Point(668, 36);
            this.uComboEquipLargeType.MaxLength = 50;
            this.uComboEquipLargeType.Name = "uComboEquipLargeType";
            this.uComboEquipLargeType.Size = new System.Drawing.Size(130, 21);
            this.uComboEquipLargeType.TabIndex = 32;
            this.uComboEquipLargeType.ValueChanged += new System.EventHandler(this.uComboEquType_ValueChanged);
            // 
            // uLabelEquType
            // 
            this.uLabelEquType.Location = new System.Drawing.Point(564, 36);
            this.uLabelEquType.Name = "uLabelEquType";
            this.uLabelEquType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquType.TabIndex = 33;
            // 
            // uComboEquipLoc
            // 
            this.uComboEquipLoc.Location = new System.Drawing.Point(116, 36);
            this.uComboEquipLoc.MaxLength = 50;
            this.uComboEquipLoc.Name = "uComboEquipLoc";
            this.uComboEquipLoc.Size = new System.Drawing.Size(131, 21);
            this.uComboEquipLoc.TabIndex = 30;
            this.uComboEquipLoc.ValueChanged += new System.EventHandler(this.uComboEquipLoc_ValueChanged);
            // 
            // uLabelLoc
            // 
            this.uLabelLoc.Location = new System.Drawing.Point(12, 36);
            this.uLabelLoc.Name = "uLabelLoc";
            this.uLabelLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelLoc.TabIndex = 31;
            // 
            // uComboGIFlag
            // 
            this.uComboGIFlag.Location = new System.Drawing.Point(916, 12);
            this.uComboGIFlag.Name = "uComboGIFlag";
            this.uComboGIFlag.Size = new System.Drawing.Size(130, 21);
            this.uComboGIFlag.TabIndex = 10;
            this.uComboGIFlag.Visible = false;
            // 
            // uLabelGIFlag
            // 
            this.uLabelGIFlag.Location = new System.Drawing.Point(812, 12);
            this.uLabelGIFlag.Name = "uLabelGIFlag";
            this.uLabelGIFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelGIFlag.TabIndex = 9;
            this.uLabelGIFlag.Visible = false;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(908, 36);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipGroup.TabIndex = 8;
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(804, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 7;
            // 
            // uComboSearchEquipProcess
            // 
            this.uComboSearchEquipProcess.Location = new System.Drawing.Point(1040, 12);
            this.uComboSearchEquipProcess.Name = "uComboSearchEquipProcess";
            this.uComboSearchEquipProcess.Size = new System.Drawing.Size(16, 21);
            this.uComboSearchEquipProcess.TabIndex = 6;
            this.uComboSearchEquipProcess.Visible = false;
            // 
            // uLabelSearchEquipProcess
            // 
            this.uLabelSearchEquipProcess.Location = new System.Drawing.Point(1024, 12);
            this.uLabelSearchEquipProcess.Name = "uLabelSearchEquipProcess";
            this.uLabelSearchEquipProcess.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchEquipProcess.TabIndex = 0;
            this.uLabelSearchEquipProcess.Text = "ultraLabel1";
            this.uLabelSearchEquipProcess.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(668, 12);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchStation.TabIndex = 5;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(324, 36);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 7;
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(564, 12);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 0;
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1048, 12);
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchArea.TabIndex = 4;
            this.uComboSearchArea.Visible = false;
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(1012, 12);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchArea.TabIndex = 0;
            this.uLabelSearchArea.Text = "ultraLabel1";
            this.uLabelSearchArea.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(428, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(324, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uDateSearchToDate
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDate.Appearance = appearance22;
            this.uDateSearchToDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToDate.Location = new System.Drawing.Point(224, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchToDate.TabIndex = 2;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(212, 20);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDate.Appearance = appearance18;
            this.uDateSearchFromDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchFromDate.TabIndex = 1;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 0;
            // 
            // uGridDurableMatRepairH
            // 
            this.uGridDurableMatRepairH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMatRepairH.DisplayLayout.Appearance = appearance6;
            this.uGridDurableMatRepairH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMatRepairH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatRepairH.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatRepairH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridDurableMatRepairH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatRepairH.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridDurableMatRepairH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMatRepairH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMatRepairH.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMatRepairH.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridDurableMatRepairH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMatRepairH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatRepairH.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMatRepairH.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridDurableMatRepairH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMatRepairH.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatRepairH.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGridDurableMatRepairH.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridDurableMatRepairH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMatRepairH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMatRepairH.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridDurableMatRepairH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMatRepairH.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGridDurableMatRepairH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMatRepairH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMatRepairH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMatRepairH.Location = new System.Drawing.Point(0, 100);
            this.uGridDurableMatRepairH.Name = "uGridDurableMatRepairH";
            this.uGridDurableMatRepairH.Size = new System.Drawing.Size(1070, 650);
            this.uGridDurableMatRepairH.TabIndex = 3;
            this.uGridDurableMatRepairH.Text = "ultraGrid1";
            this.uGridDurableMatRepairH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDurableMatRepairH_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox3.Controls.Add(this.uGridDurableMatD);
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox3.Controls.Add(this.uLabelReleaseComponent);
            this.uGroupBox3.Controls.Add(this.uLabelUnusual);
            this.uGroupBox3.Controls.Add(this.uTextRepairGIReqName);
            this.uGroupBox3.Controls.Add(this.uTextRepairGIReqID);
            this.uGroupBox3.Controls.Add(this.uLabelReleaseRequestUser);
            this.uGroupBox3.Controls.Add(this.uDateRepairGIReqDate);
            this.uGroupBox3.Controls.Add(this.uLabelReleaseRequestDay);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 148);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1044, 492);
            this.uGroupBox3.TabIndex = 2;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(128, 52);
            this.uTextEtcDesc.MaxLength = 100;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(468, 21);
            this.uTextEtcDesc.TabIndex = 25;
            // 
            // uGridDurableMatD
            // 
            this.uGridDurableMatD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMatD.DisplayLayout.Appearance = appearance20;
            this.uGridDurableMatD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMatD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatD.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.uGridDurableMatD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatD.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.uGridDurableMatD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMatD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMatD.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMatD.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.uGridDurableMatD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMatD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatD.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMatD.DisplayLayout.Override.CellAppearance = appearance30;
            this.uGridDurableMatD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMatD.DisplayLayout.Override.CellPadding = 0;
            appearance31.BackColor = System.Drawing.SystemColors.Control;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatD.DisplayLayout.Override.GroupByRowAppearance = appearance31;
            appearance33.TextHAlignAsString = "Left";
            this.uGridDurableMatD.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGridDurableMatD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMatD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMatD.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridDurableMatD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMatD.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridDurableMatD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMatD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMatD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMatD.Location = new System.Drawing.Point(12, 92);
            this.uGridDurableMatD.Name = "uGridDurableMatD";
            this.uGridDurableMatD.Size = new System.Drawing.Size(1026, 368);
            this.uGridDurableMatD.TabIndex = 24;
            this.uGridDurableMatD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMatD_AfterCellUpdate);
            this.uGridDurableMatD.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMatD_CellListSelect);
            this.uGridDurableMatD.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMatD_AfterCellListCloseUp);
            this.uGridDurableMatD.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridDurableMatD_BeforeCellListDropDown);
            this.uGridDurableMatD.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMatD_CellChange);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(132, 84);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 23;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uLabelReleaseComponent
            // 
            this.uLabelReleaseComponent.Location = new System.Drawing.Point(12, 88);
            this.uLabelReleaseComponent.Name = "uLabelReleaseComponent";
            this.uLabelReleaseComponent.Size = new System.Drawing.Size(116, 20);
            this.uLabelReleaseComponent.TabIndex = 22;
            // 
            // uLabelUnusual
            // 
            this.uLabelUnusual.Location = new System.Drawing.Point(12, 51);
            this.uLabelUnusual.Name = "uLabelUnusual";
            this.uLabelUnusual.Size = new System.Drawing.Size(112, 20);
            this.uLabelUnusual.TabIndex = 21;
            // 
            // uTextRepairGIReqName
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGIReqName.Appearance = appearance28;
            this.uTextRepairGIReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGIReqName.Location = new System.Drawing.Point(500, 28);
            this.uTextRepairGIReqName.Name = "uTextRepairGIReqName";
            this.uTextRepairGIReqName.ReadOnly = true;
            this.uTextRepairGIReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGIReqName.TabIndex = 20;
            // 
            // uTextRepairGIReqID
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairGIReqID.Appearance = appearance19;
            this.uTextRepairGIReqID.BackColor = System.Drawing.Color.PowderBlue;
            appearance32.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance32.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance32;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairGIReqID.ButtonsRight.Add(editorButton1);
            this.uTextRepairGIReqID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepairGIReqID.Location = new System.Drawing.Point(396, 28);
            this.uTextRepairGIReqID.MaxLength = 20;
            this.uTextRepairGIReqID.Name = "uTextRepairGIReqID";
            this.uTextRepairGIReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGIReqID.TabIndex = 19;
            this.uTextRepairGIReqID.ValueChanged += new System.EventHandler(this.uTextRepairGIReqID_ValueChanged);
            this.uTextRepairGIReqID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRepairGIReqID_KeyDown);
            this.uTextRepairGIReqID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRepairGIReqID_EditorButtonClick);
            // 
            // uLabelReleaseRequestUser
            // 
            this.uLabelReleaseRequestUser.Location = new System.Drawing.Point(276, 28);
            this.uLabelReleaseRequestUser.Name = "uLabelReleaseRequestUser";
            this.uLabelReleaseRequestUser.Size = new System.Drawing.Size(116, 20);
            this.uLabelReleaseRequestUser.TabIndex = 2;
            // 
            // uDateRepairGIReqDate
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairGIReqDate.Appearance = appearance29;
            this.uDateRepairGIReqDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairGIReqDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairGIReqDate.Location = new System.Drawing.Point(128, 28);
            this.uDateRepairGIReqDate.Name = "uDateRepairGIReqDate";
            this.uDateRepairGIReqDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairGIReqDate.TabIndex = 1;
            this.uDateRepairGIReqDate.AfterExitEditMode += new System.EventHandler(this.uDateRepairGIReqDate_AfterExitEditMode);
            // 
            // uLabelReleaseRequestDay
            // 
            this.uLabelReleaseRequestDay.Location = new System.Drawing.Point(12, 28);
            this.uLabelReleaseRequestDay.Name = "uLabelReleaseRequestDay";
            this.uLabelReleaseRequestDay.Size = new System.Drawing.Size(112, 20);
            this.uLabelReleaseRequestDay.TabIndex = 0;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uTextRepairID);
            this.uGroupBox1.Controls.Add(this.uTextRepairDesc);
            this.uGroupBox1.Controls.Add(this.uTextEquipStopTypeName);
            this.uGroupBox1.Controls.Add(this.uTextRepairResultName);
            this.uGroupBox1.Controls.Add(this.uTextRepairEndDate);
            this.uGroupBox1.Controls.Add(this.uLabelRepairUnusual);
            this.uGroupBox1.Controls.Add(this.uLabelStopType);
            this.uGroupBox1.Controls.Add(this.uLabelRepairResult);
            this.uGroupBox1.Controls.Add(this.uTextRepairName);
            this.uGroupBox1.Controls.Add(this.uLabelRepairUser);
            this.uGroupBox1.Controls.Add(this.uLabelRepairDay);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Controls.Add(this.uTextRepairGICode);
            this.uGroupBox1.Controls.Add(this.uLabelReleaseCode);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1044, 128);
            this.uGroupBox1.TabIndex = 0;
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(644, 28);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipName.TabIndex = 46;
            // 
            // uTextRepairID
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairID.Appearance = appearance42;
            this.uTextRepairID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairID.Location = new System.Drawing.Point(408, 52);
            this.uTextRepairID.Name = "uTextRepairID";
            this.uTextRepairID.ReadOnly = true;
            this.uTextRepairID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairID.TabIndex = 45;
            // 
            // uTextRepairDesc
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDesc.Appearance = appearance39;
            this.uTextRepairDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDesc.Location = new System.Drawing.Point(116, 100);
            this.uTextRepairDesc.Name = "uTextRepairDesc";
            this.uTextRepairDesc.ReadOnly = true;
            this.uTextRepairDesc.Size = new System.Drawing.Size(468, 21);
            this.uTextRepairDesc.TabIndex = 44;
            // 
            // uTextEquipStopTypeName
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipStopTypeName.Appearance = appearance44;
            this.uTextEquipStopTypeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipStopTypeName.Location = new System.Drawing.Point(408, 76);
            this.uTextEquipStopTypeName.Name = "uTextEquipStopTypeName";
            this.uTextEquipStopTypeName.ReadOnly = true;
            this.uTextEquipStopTypeName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipStopTypeName.TabIndex = 43;
            // 
            // uTextRepairResultName
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairResultName.Appearance = appearance41;
            this.uTextRepairResultName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairResultName.Location = new System.Drawing.Point(116, 76);
            this.uTextRepairResultName.Name = "uTextRepairResultName";
            this.uTextRepairResultName.ReadOnly = true;
            this.uTextRepairResultName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairResultName.TabIndex = 42;
            // 
            // uTextRepairEndDate
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairEndDate.Appearance = appearance40;
            this.uTextRepairEndDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairEndDate.Location = new System.Drawing.Point(116, 52);
            this.uTextRepairEndDate.Name = "uTextRepairEndDate";
            this.uTextRepairEndDate.ReadOnly = true;
            this.uTextRepairEndDate.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairEndDate.TabIndex = 41;
            // 
            // uLabelRepairUnusual
            // 
            this.uLabelRepairUnusual.Location = new System.Drawing.Point(12, 100);
            this.uLabelRepairUnusual.Name = "uLabelRepairUnusual";
            this.uLabelRepairUnusual.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairUnusual.TabIndex = 40;
            // 
            // uLabelStopType
            // 
            this.uLabelStopType.Location = new System.Drawing.Point(304, 75);
            this.uLabelStopType.Name = "uLabelStopType";
            this.uLabelStopType.Size = new System.Drawing.Size(100, 20);
            this.uLabelStopType.TabIndex = 39;
            // 
            // uLabelRepairResult
            // 
            this.uLabelRepairResult.Location = new System.Drawing.Point(12, 75);
            this.uLabelRepairResult.Name = "uLabelRepairResult";
            this.uLabelRepairResult.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairResult.TabIndex = 38;
            // 
            // uTextRepairName
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairName.Appearance = appearance43;
            this.uTextRepairName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairName.Location = new System.Drawing.Point(512, 52);
            this.uTextRepairName.Name = "uTextRepairName";
            this.uTextRepairName.ReadOnly = true;
            this.uTextRepairName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairName.TabIndex = 37;
            // 
            // uLabelRepairUser
            // 
            this.uLabelRepairUser.Location = new System.Drawing.Point(304, 52);
            this.uLabelRepairUser.Name = "uLabelRepairUser";
            this.uLabelRepairUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairUser.TabIndex = 36;
            // 
            // uLabelRepairDay
            // 
            this.uLabelRepairDay.Location = new System.Drawing.Point(12, 52);
            this.uLabelRepairDay.Name = "uLabelRepairDay";
            this.uLabelRepairDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairDay.TabIndex = 35;
            // 
            // uTextEquipName
            // 
            appearance15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextEquipName.Appearance = appearance15;
            this.uTextEquipName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextEquipName.Location = new System.Drawing.Point(748, 28);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(116, 21);
            this.uTextEquipName.TabIndex = 5;
            // 
            // uTextEquipCode
            // 
            appearance16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextEquipCode.Appearance = appearance16;
            this.uTextEquipCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextEquipCode.Location = new System.Drawing.Point(408, 28);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipCode.TabIndex = 3;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(304, 28);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCode.TabIndex = 2;
            // 
            // uTextRepairGICode
            // 
            appearance17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextRepairGICode.Appearance = appearance17;
            this.uTextRepairGICode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uTextRepairGICode.Location = new System.Drawing.Point(116, 28);
            this.uTextRepairGICode.Name = "uTextRepairGICode";
            this.uTextRepairGICode.ReadOnly = true;
            this.uTextRepairGICode.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairGICode.TabIndex = 1;
            // 
            // uLabelReleaseCode
            // 
            this.uLabelReleaseCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelReleaseCode.Name = "uLabelReleaseCode";
            this.uLabelReleaseCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelReleaseCode.TabIndex = 0;
            // 
            // frmEQUZ0010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridDurableMatRepairH);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0010";
            this.Load += new System.EventHandler(this.frmEQUZ0010_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0010_Activated);
            this.Resize += new System.EventHandler(this.frmEQUZ0010_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboGIFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatRepairH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairGIReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResultName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMatRepairH;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelReleaseCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGICode;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairGIReqDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReleaseRequestDay;
        private Infragistics.Win.Misc.UltraLabel uLabelReleaseRequestUser;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.Misc.UltraLabel uLabelReleaseComponent;
        private Infragistics.Win.Misc.UltraLabel uLabelUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGIReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGIReqID;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMatD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipStopTypeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairResultName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairEndDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUnusual;
        private Infragistics.Win.Misc.UltraLabel uLabelStopType;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairName;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUser;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboGIFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelGIFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
    }
}