﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQU0009.cs                                         */
/* 프로그램명   : 수리의뢰접수                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-30                                            */
/* 수정이력     : 2011-08-31 : 이종민 수정                              */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0009 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
        private Boolean m_bolSaveCheck = false;

        public frmEQU0009()
        {
            InitializeComponent();
        }

        #region 컨트롤초기화

        private void frmEQU0009_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0009_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("수리의뢰접수", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitValue();
            InitGrid();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchDate, "수리요청일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceipt, "접수여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                for (int i = 0; i < uComboSearchPlant.Items.Count; i++)
                {
                    uComboSearchPlant.SelectedIndex = i;
                    if (uComboSearchPlant.Value.ToString().Equals(m_resSys.GetString("SYS_PLANTCODE").ToString()))
                    {
                        break;
                    }
                    else
                    {
                        uComboSearchPlant.SelectedIndex = 0;
                    }
                }

                uComboReceipt.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                if (uComboReceipt.Items.Count == 0)
                {
                    uComboReceipt.Clear();
                    uComboReceipt.Items.Add("F", "N");
                    uComboReceipt.Items.Add("T", "Y");
                    uComboReceipt.SelectedText = "N";
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Component Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                uDateSearchFromDate.Value = DateTime.Now;
                uDateSearchToDate.Value = DateTime.Now;
                uComboSearchArea.Value = "";
                uComboSearchEquipProcess.Value = "";
                //uComboSearchPlant.Value = "";
                uComboSearchStation.Value = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                uGridRepairReq.Refresh();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 그리드 속성 설정
                wGrid.mfInitGeneralGrid(this.uGridRepairReq, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Default
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼 설정
                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "HeadFlag", "Head", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "StageDesc", "Stage", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "ReceiptDesc", "접수특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "RepairReqCode", "수리요청번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "UrgentFlag", "긴급처리", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "RepairReqDate", "수리요청일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "UserName", "수리요청자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "BreakDownDate", "고장발생일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "BreakDownTime", "고장발생시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairReq, 0, "RepairReqReason", "요청사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 250, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 헤더에 CheckBox 없애는 구문
                this.uGridRepairReq.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridRepairReq.DisplayLayout.Bands[0].Columns["HeadFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridRepairReq.DisplayLayout.Bands[0].Columns["UrgentFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                // 폰트Size 지정
                this.uGridRepairReq.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridRepairReq.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region CommonEvent

        public void mfSearch()
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            try
            {
                if (uComboSearchPlant.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택하세요.", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.Focus();
                    return;
                }
                if (Convert.ToDateTime(this.uDateSearchFromDate.Value).Date > Convert.ToDateTime(this.uDateSearchToDate.Value).Date)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검색날짜 확인", "검색시작일이 종료일보다 많습니다.", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }


                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(uDateSearchFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(uDateSearchToDate.Value).ToString("yyyy-MM-dd");
                string strArea = uComboSearchArea.Value.ToString();
                string strStation = uComboSearchStation.Value.ToString();
                string strEquipProc = uComboSearchEquipProcess.Value.ToString();
                string strReceipt = uComboReceipt.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");


                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipRepairRequest(strPlantCode, strFromDate, strToDate, strArea, strStation, strEquipProc, strReceipt, m_resSys.GetString("SYS_LANG"));

                //DataTable dtPlan = clsPMPlan.mfReadPMPlan(strPlantCode, strYear, strGroupCode);
                /////////////
                ///////////// 
                uGridRepairReq.Refresh();
                uGridRepairReq.DataSource = dtEquip;
                uGridRepairReq.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtEquip.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "조회처리결과", "조회결과가 없습니다.", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }

        /// <summary>
        /// Colmns["Check"] 선택된거 전부 접수
        /// </summary>
        public void mfSave()
        {
            titleArea.Focus();
            if (!m_bolSaveCheck)
                return;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            int cnt = 0;
            for (int i = 0; i < uGridRepairReq.Rows.Count; i++)
            {
                if (Convert.ToBoolean(uGridRepairReq.Rows[i].GetCellValue("Check")))
                {
                    cnt++;
                }
            }

            if (cnt == 0)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "확인창", "접수할 수리요청을 선택하세요", Infragistics.Win.HAlign.Right);
                return;
            }

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "확인창", "저장확인", "선택한 수리요청을 접수 하시겠습니까?",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                DataTable dtRepair = new DataTable();
                dtRepair.Columns.Add("RepairReqCode");
                dtRepair.Columns.Add("HeadFlag");
                dtRepair.Columns.Add("StageDesc");
                dtRepair.Columns.Add("ReceiptDesc");

                for (int i = 0; i < uGridRepairReq.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridRepairReq.Rows[i].GetCellValue("Check")))
                    {
                        DataRow rwReqCode = dtRepair.NewRow();

                        rwReqCode["RepairReqCode"] = uGridRepairReq.Rows[i].GetCellValue("RepairReqCode").ToString();
                        rwReqCode["HeadFlag"] = Convert.ToBoolean(uGridRepairReq.Rows[i].GetCellValue("HeadFlag")) == true ? "T" : "F";
                        rwReqCode["StageDesc"] = uGridRepairReq.Rows[i].GetCellValue("StageDesc").ToString();
                        rwReqCode["ReceiptDesc"] = uGridRepairReq.Rows[i].GetCellValue("ReceiptDesc").ToString();

                        dtRepair.Rows.Add(rwReqCode);
                    }
                }

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);

                string strRtn = clsEquipReq.mfSaveEquipRepairRequest_Receipt(dtRepair, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    m_bolSaveCheck = false;
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                 Infragistics.Win.HAlign.Right);
                }

                dtRepair.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            titleArea.Focus();
            if (m_bolSaveCheck)
                return;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            int cnt = 0;
            for (int i = 0; i < uGridRepairReq.Rows.Count; i++)
            {
                if (Convert.ToBoolean(uGridRepairReq.Rows[i].GetCellValue("Check")))
                {
                    cnt++;
                }
            }

            if (cnt == 0)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "확인창", "취소할 수리요청을 선택하세요", Infragistics.Win.HAlign.Right);
                return;
            }

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "확인창", "저장확인", "선택한 수리요청을 취소 하시겠습니까?",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }


            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");

                DataTable dtRepair = new DataTable();
                dtRepair.Columns.Add("RepairReqCode");

                for (int i = 0; i < uGridRepairReq.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridRepairReq.Rows[i].GetCellValue("Check")))
                    {
                        DataRow rwReqCode = dtRepair.NewRow();

                        rwReqCode["RepairReqCode"] = uGridRepairReq.Rows[i].GetCellValue("RepairReqCode").ToString();

                        dtRepair.Rows.Add(rwReqCode);
                    }
                }

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);

                string strRtn = clsEquipReq.mfDeleteEquipRepairRequest_Receipt(dtRepair, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "취소처리결과", "선택한 접수를 취소했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    m_bolSaveCheck = false;
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "취소처리결과", "선택한 접수를 취소하지 못했습니다.",
                                                 Infragistics.Win.HAlign.Right);
                }

                dtRepair.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        #endregion

        #region GridEvent
        private void uGridRepairReq_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "StageDesc")
                {
                    if (e.Cell.Value.ToString().Length >= 50)
                    {
                        e.Cell.Value = e.Cell.Value.ToString().Substring(0, 50);
                    }
                }
                else if (e.Cell.Column.Key == "ReceiptDesc")
                {
                    if (e.Cell.Value.ToString().Length >= 100)
                    {
                        e.Cell.Value = e.Cell.Value.ToString().Substring(0, 100);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region UI Event

        /// <summary>
        /// 공장콤보 변경시 다른 콤보박스 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {

            InitGrid();

            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinComboEditor wCombo = new WinComboEditor();

            try
            {
                if (uComboSearchPlant.Value.ToString() != string.Empty)
                {

                    //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    //Thread t1 = m_ProgressPopup.mfStartThread();
                    //m_ProgressPopup.mfOpenProgressPopup(this, "조회중...");
                    //this.MdiParent.Cursor = Cursors.WaitCursor;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);

                    DataTable dtArea = clsArea.mfReadAreaCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboSearchArea.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboSearchArea, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "AreaCode", "AreaName", dtArea);

                    dtArea.Dispose();
                    clsArea.Dispose();

                    brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboSearchStation.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "StationCode", "StationName", dtStation);

                    dtStation.Dispose();
                    clsStation.Dispose();

                    brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    brwChannel.mfCredentials(clsEquipProcGubun);

                    DataTable dtEquipProcGugun = clsEquipProcGubun.mfReadProGubunCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboSearchEquipProcess.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboSearchEquipProcess, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "EquipProcGubunCode", "EquipProcGubunName", dtEquipProcGugun);

                    dtEquipProcGugun.Dispose();
                    clsEquipProcGubun.Dispose();

                    //this.MdiParent.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    uComboSearchArea.ValueList.Reset();
                    uComboSearchEquipProcess.ValueList.Reset();
                    uComboSearchStation.ValueList.Reset();
                    uComboSearchArea.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                    uComboSearchEquipProcess.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                    uComboSearchStation.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 접수여부 변경시 그리드 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboReceipt_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (uComboReceipt.Items.Count != 0)
                {
                    InitGrid();
                    if (uComboReceipt.Value.ToString() == "F")
                        m_bolSaveCheck = true;
                    else
                        m_bolSaveCheck = false;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
    }
}
