﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQU0023.cs                                         */
/* 프로그램명   : 입고확인등록                                          */
/* 작성자       : 권종구  , 코딩: 남현식                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0023 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQU0023()
        {
            InitializeComponent();
        }

        private void frmEQU0023_Activated(object sender, EventArgs e)
        {

            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmEQU0023_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("입고확인등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextGRConfirmID.Text = m_resSys.GetString("SYS_USERID");
                uTextGRConfirmName.Text = m_resSys.GetString("SYS_USERNAME");
                uTextPONumber.Text = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelGRDate, "구매의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelGRConfirmDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelGRConfirmID, "입고담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelGRSPInventory, "입고창고", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelInputPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelPONumber, "PO번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
                wCombo.mfSetComboEditor(this.uComboInputPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                //this.uComboInventory.Text = "선택";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정
                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, true, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRCode", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRSeq", "입고순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid1, 0, "SparePartCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "SparePartName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, true, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100, Infragistics.Win.HAlign.Left,
                   Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRQty", "입고수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DataFlag", "데이터여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, true, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 체크박스 Type 설정
                //this.uGrid1.DisplayLayout.Bands[0].Columns["Check"].DataType = typeof(Boolean);
                //Grid초기화 후 Font크기를 아래와 같이 적용
                
                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                //한줄생성
                grd.mfAddRowGrid(this.uGrid1, 0);


                //채널연결
                QRPBrowser brwChannel = new QRPBrowser(); 

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGrid1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "입고리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.Default, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트지정
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
#endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                //InitText();
                //InitGrid();
                WinMessageBox msg = new WinMessageBox();
                //this.uComboInventory.Items.Clear();

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strFromGRDate = this.uDateFromGRDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDGRDate = this.uDateToGRDate.DateTime.Date.ToString("yyyy-MM-dd");



                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPGR), "SPGR");
                QRPEQU.BL.EQUSPA.SPGR clsSPGR = new QRPEQU.BL.EQUSPA.SPGR();
                brwChannel.mfCredentials(clsSPGR);

                // Call Method
                DataTable dtSPGR = clsSPGR.mfReadEQUSPGR(strPlantCode, strFromGRDate, strToDGRDate, "F", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtSPGR;
                uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtSPGR.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information,  500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001115", "M001102",
                                                                           Infragistics.Win.HAlign.Right);
                }
                else
                {
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {

                        uGrid1.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        uGrid1.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;

                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                    
                }
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtSPStock = new DataTable();
                DataTable dtSPStockMoveHist = new DataTable();
                DataTable dtSPGR = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();
                int intRowCheck = 0;

                // 필수입력사항 확인
                if (this.uComboInputPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboInputPlant.DropDown();
                    return;
                }

                if (this.uComboInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000870", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboInventory.DropDown();
                    return;
                }

                if (this.uTextGRConfirmID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000865", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextGRConfirmID.Focus();
                    return;
                }

                if (this.uTextGRConfirmName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000865", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextGRConfirmID.Focus();
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                string strLang = m_resSys.GetString("SYS_LANG");

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (this.uGrid1.Rows[i].Cells["DataFlag"].Value.ToString() != "T")
                        {
                            intRowCheck = intRowCheck + 1;

                            if (this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000475", strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["SparePartCode"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000500", strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["UnitCode"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            if (this.uGrid1.Rows[i].Cells["GRQty"].Value.ToString() == "0")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000518", strLang)
                                                , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["GRQty"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001239", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();

                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //-------- 1.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                dtSPStock = clsSPStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (this.uGrid1.Rows[i].Cells["DataFlag"].Value.ToString() != "T")
                        {
                            row = dtSPStock.NewRow();

                            row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uComboInventory.Value.ToString();
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["Qty"] = this.uGrid1.Rows[i].Cells["GRQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStock.Rows.Add(row);
                        }
                    }
                }

                //-------- 2.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                dtSPStockMoveHist = clsSPStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        //if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        if (this.uGrid1.Rows[i].Cells["DataFlag"].Value.ToString() != "T")
                        {
                            row = dtSPStockMoveHist.NewRow();

                            row["MoveGubunCode"] = "M02";       //입고확인일 경우는 "M02"
                            row["DocCode"] = "";
                            row["MoveDate"] = this.uDateGRConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["MoveChargeID"] = this.uTextGRConfirmID.Text;
                            row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uComboInventory.Value.ToString();
                            row["EquipCode"] = "";
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["MoveQty"] = this.uGrid1.Rows[i].Cells["GRQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStockMoveHist.Rows.Add(row);
                        }
                    }
                }

                //-------- 3.  EQUSPGR(SparePart 입고확인 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPGR), "SPGR");
                QRPEQU.BL.EQUSPA.SPGR clsSPGR = new QRPEQU.BL.EQUSPA.SPGR();
                brwChannel.mfCredentials(clsSPGR);

                dtSPGR = clsSPGR.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        //if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        if (this.uGrid1.Rows[i].Cells["DataFlag"].Value.ToString() != "T")
                        {
                            row = dtSPGR.NewRow();
                            row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                            row["GRCode"] = this.uGrid1.Rows[i].Cells["GRCode"].Value.ToString();
                            row["GRSeq"] = "1"; //1로 고정하기로 결정함
                            row["GRDate"] = this.uDateGRConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["GRQty"] = this.uGrid1.Rows[i].Cells["GRQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();
                            row["GRConfirmFlag"] = "T";
                            row["GRConfirmDate"] = this.uDateGRConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["GRConfirmID"] = this.uTextGRConfirmID.Text;
                            row["GRSPInventoryCode"] = this.uComboInventory.Value.ToString();
                            row["PONumber"] = this.uTextPONumber.Text;
                            dtSPGR.Rows.Add(row);
                        }
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
 
                // 저장함수 호출
                //-------- BL 호출 : 입고정보, 현재고갱신, 재고이동 이력 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SAVESparePartStock), "SAVESparePartStock");
                QRPEQU.BL.EQUSPA.SAVESparePartStock clsSAVESparePartStock = new QRPEQU.BL.EQUSPA.SAVESparePartStock();
                brwChannel.mfCredentials(clsSAVESparePartStock);
                string rtMSG = clsSAVESparePartStock.mfSaveSPGR(dtSPGR, dtSPStock, dtSPStockMoveHist, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                //InitCombo();
                //InitLabel();
                InitText();
                if (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGrid1.Rows.All);
                    this.uGrid1.DeleteSelectedRows(false);
                }
                this.uComboInventory.Value = "";
                //InitGrid();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGrid1);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;



                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;

                if (strColumn == "SparePartCode")
                {
                    // 채널연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                    QRPMAS.BL.MASEQU.SparePart clsSparePart = new QRPMAS.BL.MASEQU.SparePart();
                    brwChannel.mfCredentials(clsSparePart);
                    string strSparePartCode = e.Cell.Row.Cells["SparePartCode"].Value.ToString();

                    DataTable dtSparePart = clsSparePart.mfReadSparePartDetail(this.uComboInputPlant.Value.ToString(), strSparePartCode, m_resSys.GetString("SYS_LANG"));

                    if (dtSparePart.Rows.Count > 0)
                    {
                        e.Cell.Row.Cells["Spec"].Value = dtSparePart.Rows[0]["Spec"].ToString();
                        e.Cell.Row.Cells["Maker"].Value = dtSparePart.Rows[0]["Maker"].ToString();
                    }

                }

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            

             
        }

        private void frmEQU0023_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uGrid1_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();

                //string strPlantCode = this.uTextPlantCode.Text;
                //string strSPInventoryCode = e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString();

                //if (strPlantCode == "")
                //    return;

                //if (strSPInventoryCode == "")
                //    return;

                // 필수입력사항 확인
                if (this.uComboInputPlant.Value == null || this.uComboInputPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboInputPlant.DropDown();
                    return;
                }

                if (e.Cell.Row.Cells["DataFlag"].Value.ToString() == "T")
                    return;

                frmPOP0008 frm = new frmPOP0008();
                frm.PlantCode = this.uComboInputPlant.Value.ToString();
                frm.ShowDialog();

                e.Cell.Row.Cells["SparePartCode"].Value = frm.SparePartCode;
                e.Cell.Row.Cells["SparePartName"].Value = frm.SparePartName;
                e.Cell.Row.Cells["PlantCode"].Value = frm.PlantCode;
                e.Cell.Row.Cells["PlantName"].Value = frm.PlantName;
                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                //if (e.Cell.Row.Cells["ChgSparePartCode"].Value != "")
                //{
                //    e.Cell.Row.Cells["ChgSparePartName"].Value = frm.SparePartName;
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_AfterDataBinding()
        {
            try
            {
                
            }
            catch (System.Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                    {
                        if (this.uGrid1.Rows[i].Cells["DataFlag"].Value.ToString().Equals(string.Empty) || this.uGrid1.Rows[i].Cells["DataFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid1.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboInputPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                //mfSearch();

                string strPlantCode = uComboInputPlant.Value.ToString();

                if (strPlantCode == "")
                    return;

                ////////----------------- SparePart 창고 콤보 불러오기------------/////////////
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);

                // Call Method
                DataTable dtSPInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));



                wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "SPInventoryCode", "SPInventoryName", dtSPInventory);
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
        }

        private void uComboInputPlant_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();



                //dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 변경확인", "입력하시던 정보가 초기화 됩니다. 계속하시겠습니까? "
                //                            , Infragistics.Win.HAlign.Right);

                //if (dir == DialogResult.No)
                //{
                //    uComboInputPlant.Value = "";
                //    uTextPlantCode.Focus(); 
                //    return;

                //}


       

                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }
                InitText();

                //frmPOP0007 frm = new frmPOP0007();
                //frm.PlantCode = this.uComboPlant.Value.ToString();
                //frm.ShowDialog();

                //uTextPlantCode.Text = frm.PlantCode;
                //uTextPlantName.Text = frm.PlantName;
                //uTextDurableMatCode.Text = frm.DurableMatCode;
                //uTextDurableMatName.Text = frm.DurableMatName;

            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uTextGRConfirmID_Click(object sender, EventArgs e)
        {
            this.uTextGRConfirmName.Text = "";
            this.uTextGRConfirmID.Text = "";
        }

        private void uTextGRConfirmID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboInputPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboInputPlant.DropDown();
                    return;

                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboInputPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextGRConfirmID.Text = frmPOP.UserID;
                this.uTextGRConfirmName.Text = frmPOP.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextGRConfirmID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                
                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextGRConfirmID;
                uTextName = this.uTextGRConfirmName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0023_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
