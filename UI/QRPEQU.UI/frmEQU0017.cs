﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{

    public partial class frmEQU0017 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        public frmEQU0017()
        {
            InitializeComponent();
        }

        private void frmEQU0017_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0017_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // Title 제목
            titleArea.mfSetLabelText("My Job - 월간", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 정비사 ID 설정
            this.uTextUserID.Text = m_resSys.GetString("SYS_USERID");
            this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");

            // 년도 TextBox 기본값 설정
            this.uTextYear.Text = DateTime.Now.Year.ToString();

            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitMonthView();
        }

        #region 초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitMonthView()
        {
            try
            {
                
                // WeekHeader DisplayStyle
                this.uMonthView.WeekHeaderDisplayStyle = Infragistics.Win.UltraWinSchedule.WeekHeaderDisplayStyle.DateRange;
                // 토, 일 따로 나오게 하는 구문
                this.uMonthView.WeekendDisplayStyle = Infragistics.Win.UltraWinSchedule.WeekendDisplayStyleEnum.Full;

                this.uCalendarLook.ViewStyle = Infragistics.Win.UltraWinSchedule.ViewStyle.Office2007;

                //this.uCalendarInfo.EventManager.AllEventsEnabled = true;
                
                this.uMonthView.CalendarInfo.SelectTypeActivity = Infragistics.Win.UltraWinSchedule.SelectType.Single;
                
                this.uMonthView.CalendarInfo = this.uCalendarInfo;
                this.uMonthView.CalendarLook = this.uCalendarLook;
                this.uMonthView.ShowClickToAddIndicator = Infragistics.Win.DefaultableBoolean.False;

                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelYear, "조회년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMonth, "조회월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.ultraLabel3, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                ArrayList Key = new ArrayList();
                ArrayList Value = new ArrayList();
                for (int i = 1; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        Key.Add("0" + i);
                        Value.Add("0" + i.ToString());
                    }
                    else
                    {
                        Key.Add(i);
                        Value.Add(i.ToString());
                    }
                }
                wCombo.mfSetComboEditor(this.uComboMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", Key, Value);

                // Month ComboBox 기본값 설정
                this.uComboMonth.Value = DateTime.Now.Month.ToString("D2");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Toolbar

        public void mfSearch()
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                
            try
            {
                #region Indispensable
                if (this.uTextYear.Text =="")
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M001108", Infragistics.Win.HAlign.Right);

                    this.uTextYear.Focus();
                    return;
                }

                if (this.uComboMonth.Value.ToString() == "")
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000206", "M001111", Infragistics.Win.HAlign.Right);

                    this.uComboMonth.DropDown();
                    return;
                }

                #endregion
                string strDate = this.uTextYear.Text + "-" + uComboMonth.Value.ToString() + "-" + "08";
                string strDate2 = this.uTextYear.Text + "-" + uComboMonth.Value.ToString() + "-" + "01";
                DateTime datWeek = Convert.ToDateTime(strDate);

                //1. 이번달 1일 날짜를 가져오는 방법 //결과 : 2011-04-01 오전 1:48:03 DateTime dtFirstDay = dateToday.AddDays(1 - dateToday.Day); 
                DateTime dtFirstDay = datWeek.AddDays(1 - datWeek.Day);
                DateTime dtMonthFirstSunday = dtFirstDay.AddDays(0 - (int)(dtFirstDay.DayOfWeek));



                //this.uMonthView.CalendarInfo.ActivateDay(dtMonthFirstSunday.AddDays(1));
                this.uMonthView.CalendarInfo.ActivateDay(Convert.ToDateTime(strDate));
                this.uMonthView.CalendarInfo.ActivateDay(Convert.ToDateTime(strDate2));

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //Infragistics.Win.UltraWinSchedule.UltraCalendarInfo ultraCalendarInfo1 = new Infragistics.Win.UltraWinSchedule.UltraCalendarInfo();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.PMDIS), "PMDIS");
                QRPEQU.BL.EQUDIS.PMDIS clsPMPlanD = new QRPEQU.BL.EQUDIS.PMDIS();
                brwChannel.mfCredentials(clsPMPlanD);

                DataTable dtPMResult = new DataTable();
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                dtPMResult = clsPMPlanD.mfReadMyJob_Month
                    (strPlantCode
                    , this.uTextYear.Text
                    , this.uComboMonth.Value.ToString()
                    , this.uTextUserID.Text
                    ,  m_resSys.GetString("SYS_LANG"));

                uCalendarInfo.DataBindingsForNotes.BindingContextControl = this;

                uCalendarInfo.DataBindingsForNotes.DataSource = dtPMResult;
                uCalendarInfo.DataBindingsForNotes.DateMember = "PMPlanDate";
                uCalendarInfo.DataBindingsForNotes.DescriptionMember = "ResultTotal";
                uCalendarInfo.DataBindingsForNotes.DataKeyMember = "EquipCode";
                uCalendarInfo.DataBindingsForNotes.OwnerKeyMember = "Total";


                

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtPMResult.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }

                //#region Var
                //string strPlantCode = uComboSearchPlant.Value.ToString();
                //string strFromDate = Convert.ToDateTime(uDateSearchFromDate.Value).ToString("yyyy-MM-dd");
                //string strToDate = Convert.ToDateTime(uDateSearchToDate.Value).ToString("yyyy-MM-dd");
                //string strArea = uComboSearchArea.Value.ToString();
                //string strStation = uComboSearchStation.Value.ToString();
                //string strEquipProc = uComboSearchEquipProcess.Value.ToString();
                //string strGIFlag = uComboGIFlag.Value.ToString();
                //#endregion



                //#region BL
                //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                //QRPEQU.BL.EQUMGM.DurableMatRepairH clsEquip = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                //brwChannel.mfCredentials(clsEquip);

                //DataTable dtEquip = clsEquip.mfReadDurableMatRepairH_Repair(strPlantCode, strFromDate, strToDate, strArea, strStation, strEquipProc, strGIFlag, m_resSys.GetString("SYS_LANG"));

                ////DataTable dtPlan = clsPMPlan.mfReadPMPlan(strPlantCode, strYear, strGroupCode);
                ///////////////
                /////////////// 
                //uGridDurableMatRepairH.Refresh();
                //uGridDurableMatRepairH.DataSource = dtEquip;
                //uGridDurableMatRepairH.DataBind();
                //#endregion

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {

        }

        public void mfDelete()
        {

        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {

        }

        public void Excel()
        {

        }

        #endregion

        #region Event

        // AppointmentDialog 이벤트
        private void uCalendarInfo_BeforeDisplayAppointmentDialog(object sender, Infragistics.Win.UltraWinSchedule.DisplayAppointmentDialogEventArgs e)
        {
            try
            {
                // 기본 AppointmentDialog Calcel
                e.Cancel = true;

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uCalendarInfo_AfterSelectedNotesChange(object sender, EventArgs e)
        {
            try
            {
                if (this.uCalendarInfo.SelectedNotes.Count > 0)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventArgs ft = new Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventArgs(this.uMonthView, DateTime.Today);
                    //MessageBox.Show("The number of selected Notes is " + this.uCalendarInfo.SelectedNotes.Count.ToString() + "\n", "AfterSelectedNotesChange", MessageBoxButtons.OK);

                    //int intIndex = this.uCalendarInfo.SelectedNotes.IndexOf(this.uMonthView);



                    //int intCount = this.uCalendarInfo.SelectedNotes.Count;
                    Infragistics.Win.UltraWinSchedule.Note note = this.uCalendarInfo.SelectedNotes[0];

                    string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                    string strEquipCode = note.DataKey.ToString();
                    string strDate = note.Date.ToString("yyyy-MM-dd");
                    string strUserID = this.uTextUserID.Text;

                    // 상세 정보 Form Load
                    frmEQU0017D1 childe = new frmEQU0017D1();
                    childe.Show();
                    childe.Display_Detail(strPlantCode, strEquipCode, strDate, strUserID);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
       

        private void test()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                brwChannel.mfCredentials(clsEquipPMD);

                //DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForEquipGroup("1", "IMGTEST001");


                //예방점검계획을 생성한다.
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlan), "PMPlan");
                QRPEQU.BL.EQUMGM.PMPlan clsPMPlan = new QRPEQU.BL.EQUMGM.PMPlan();
                brwChannel.mfCredentials(clsPMPlan);

                //DataTable dtTable = clsPMPlan.GeneratePMSchedule("1", "2011", "2011-10-13", dtEquipPMD);

                string str = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                frm.ShowDialog();

                uTextUserID.Text = frm.UserID;
                uTextUserName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextUserID;
                uTextName = this.uTextUserName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        //DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
                        //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //                    "확인창", "입력확인", "공장을 선택해주세요.",
                        //                    Infragistics.Win.HAlign.Right);

                        //this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information,  500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uMonthView_MoreActivityIndicatorClicked(object sender, Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventArgs e)
        {
            try
            {
                //// SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                Infragistics.Win.UltraWinSchedule.Note note = e.Day.Notes[0];
                

                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                //QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                //brwChannel.mfCredentials(clsPMPlanD);

                //DataTable dtPMResult = new DataTable();

                //dtPMResult = clsPMPlanD.mfReadMyJob_Month
                //    (m_resSys.GetString("SYS_PLANTCODE")
                //    , this.uTextYear.Text
                //    , this.uComboMonth.Value.ToString()
                //    , this.uTextUserID.Text
                //    , m_resSys.GetString("SYS_LANG"));



                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strEquipCode = note.DataKey.ToString();
                string strDate = note.Date.ToString("yyyy-MM-dd");
                string strUserID = this.uTextUserID.Text;

                //e.Day.Notes.Remove(note); //TEST해야할것

                //Infragistics.Win.UltraWinSchedule.Note Temp;
                //for (int i = 0; i < e.Day.Notes.Count; i++)
                //{
                //    Temp = e.Day.Notes[0];
                //}

                this.uMonthView.CalendarInfo.Notes.Remove(note);
                this.uMonthView.CalendarInfo.Notes.Add(note);
                
                note.CalendarInfo.SelectTypeActivity = Infragistics.Win.UltraWinSchedule.SelectType.Single;

                //uCalendarInfo.DataBindingsForNotes.BindingContextControl = this;
                //uCalendarInfo.DataBindingsForNotes.DataSource = dtPMResult;
                //uCalendarInfo.DataBindingsForNotes.DateMember = "PMPlanDate";
                //uCalendarInfo.DataBindingsForNotes.DescriptionMember = "ResultTotal";
                //uCalendarInfo.DataBindingsForNotes.DataKeyMember = "EquipCode";
                //uCalendarInfo.DataBindingsForNotes.OwnerKeyMember = "Total";

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0017_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uMonthView.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uMonthView.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion





    }
}
