﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0007.cs                                        */
/* 프로그램명   : 점검계획조정                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-24 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

using Microsoft.VisualBasic;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0007 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 

        //수정여부를 위한 전역변수
        string strChk = "F";

        
        public frmEQUZ0007()
        {
            InitializeComponent();
        }

        private void frmEQUZ0007_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0007_Load(object sender, EventArgs e)
        {
            SetRunMode();
            SetToolAuth();

            // 초기화 Method            
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            //MessageBox.Show(this.Tag.ToString());
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 초기값 설정 Method
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Title 제목
                titleArea.mfSetLabelText("점검계획조정", m_resSys.GetString("SYS_FONTNAME"), 12);

                // 계획년도 TextBox 기본값 설정
                this.uNumSearchYear.Value = DateTime.Now.Year.ToString();

                //정비사 기본값 설정
                //this.uTextTechnician.Text = m_resSys.GetString("SYS_USERID"); 
                //this.uTextTechnicianName.Text = m_resSys.GetString("SYS_USERNAME");
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "계획년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipMentGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearch1, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearch2, "점검주기", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelTechnician, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtPM = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPMPeriod, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "ComCode", "ComCodeName", dtPM);




                ArrayList Key = new ArrayList();
                ArrayList Value = new ArrayList();
                for (int i = 1; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        Key.Add("0" + i);
                        Value.Add("0" + i.ToString());
                    }
                    else
                    {
                        Key.Add(i);
                        Value.Add(i.ToString());
                    }
                }
                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", Key, Value);

                // Month ComboBox 기본값 설정
                this.uComboSearchMonth.Value = DateTime.Now.Month.ToString("D2");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMPlan, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "PlanYear", "계획년도", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "PMPeriodCode", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "PMPlanDate", "점검계획일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "PMReviseDate", "점검계획일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "yyyy-MM-dd", "", "");

                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "BtnDate", "", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 20, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");
                
                wGrid.mfSetGridColumn(this.uGridPMPlan, 0, "PMResultName", "점검진행여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                this.uGridPMPlan.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPMPlan.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #region DropDown
                
                //시스템공통코드정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                //점검주기 조회 매서드 실행
                DataTable dtPMPer = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                //그리드에 점검주기콤보 삽입
                wGrid.mfSetGridColumnValueList(this.uGridPMPlan, 0, "PMPeriodCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPMPer);


                DataTable dtResult = clsCommonCode.mfReadCommonCode("C0058", m_resSys.GetString("SYS_LANG"));

                //그리드에 콤보 삽입
                wGrid.mfSetGridColumnValueList(this.uGridPMPlan, 0, "PMResultName", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtResult);

                #endregion


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항 체크
                //검색하기 전에 수정사항이 있으면 저장여부를 체크한다.
                if (strChk == "T")
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                         , "M001264", "M000882", "M000757", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        mfSave();
                    else
                        strChk = "F";
                }

                if (this.uGridPMPlan.Rows.Count > 0)
                {
                    InitDisplay();
                }

                //수정여부 초기화
                if (strChk == "T")
                {
                    strChk = "F";
                }

                //--------필수 입력 사항 확인 ------//
                if (this.uNumSearchYear.Value == null
                    || this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000250", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                if (this.uComboSearchMonth.Value == null 
                    || this.uComboSearchMonth.Value.ToString().Equals(string.Empty)
                    || !Information.IsNumeric(this.uComboSearchMonth.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000252", Infragistics.Win.HAlign.Right);

                    this.uComboSearchMonth.DropDown();
                    return;
                }
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uComboSearchPMPeriod.Value.ToString().Equals("DAY"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000217", "M000862", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPMPeriod.DropDown();
                    return;

                }
                if (this.uComboSearchPMPeriod.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001228", "M001067", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPMPeriod.DropDown();
                    return;
                }
                if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001228", "M000128", Infragistics.Win.HAlign.Right);

                    this.uComboSearchStation.DropDown();
                    return;
                }
                if (this.uComboEquipLoc.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001228", "M000831", Infragistics.Win.HAlign.Right);

                    this.uComboEquipLoc.DropDown();
                    return;
                }

                #endregion

                //------------ 값 저장 -----------//
                string strPlanYear = this.uNumSearchYear.Value.ToString();          //계획년도
                string strPlantCode = this.uComboSearchPlant.Value.ToString();      //공장코드
                

                string strUserID = "";                                              //정비사

                if (!this.uTextTechnician.Text.Equals(string.Empty) && !this.uTextTechnicianName.Text.Equals(string.Empty))
                {
                    strUserID = this.uTextTechnician.Text;
                }
                
                
                string strStation = this.uComboSearchStation.Value.ToString();      //Station
                string strEquipLoc = this.uComboEquipLoc.Value.ToString();               //설비위치
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();  //설비대분류
                string strEquipLargeType = this.uComboEquipLargeType.Value.ToString();  //설비중분류
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();//설비그룹코드

                string strPMPeriod = this.uComboSearchPMPeriod.Value.ToString();    //점검주기
                string strPMMonth = this.uComboSearchMonth.Value.ToString();    //점검월
                //-------------POPUP창 오픈----------------//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //------------BL호출-----------------//


                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                    clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                    brwChannel.mfCredentials(clsPMPlanD);
                }
                else
                    clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD(m_strDBConn);

                DataTable dtPMPlanD = clsPMPlanD.mfReadPMPlanD_Revise(strPlanYear, strPlantCode, strStation, strEquipLoc, strProcessGroup, strEquipLargeType, strEquipGroup, strUserID, strPMMonth, strPMPeriod, m_resSys.GetString("SYS_LANG"));

                #region 주석처리
                //--------------------------------------매서드호출---------------------------------//
                
                
                //if (strEquipGroup == "" && strUserID == "")
                //{
                    //dtPMPlanD = clsPMPlanD.mfReadPMPlanD(strPlanYear, strPlantCode, strArea, strStation, strProcGubun,strEquipLoc, strEquipType, m_resSys.GetString("SYS_LANG"));
                //}
                //else
                //{
                //    if (strEquipGroup != "")
                //        dtPMPlanD = clsPMPlanD.mfReadPMPlanDEquipGroup(strPlantCode, strEquipGroup, strPlanYear, m_resSys.GetString("SYS_LANG"));
                //    else
                //    {
                //        if (strUserID != "")
                //            dtPMPlanD = clsPMPlanD.mfReadPMPlanDEquipWorker(strPlantCode, strUserID, strPlanYear, m_resSys.GetString("SYS_LANG"));
                //    }
                //}
                #endregion

                this.uGridPMPlan.DataSource = dtPMPlanD;
                this.uGridPMPlan.DataBind();

                //------------정보가 있을 경우 ------------//
                if (dtPMPlanD.Rows.Count != 0)
                {
                    
                    for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    {
                        if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                        {
                            this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }

                        #region 점검주기별 색 바꿈
                        //if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("DAY"))
                        //{
                        //    //this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.LightSalmon;
                        //}
                        //if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("WEK"))
                        //{
                        //    this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.Aquamarine;
                        //}
                        //if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("MON"))
                        //{
                        //    this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.CornflowerBlue;
                        //}
                        //if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("QUA"))
                        //{
                        //    this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.Orchid;
                        //}
                        //if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("HAF"))
                        //{
                        //    this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.LemonChiffon;
                        //}
                        //if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("YEA"))
                        //{
                        //    this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.OldLace;
                        //}
                        #endregion

                    }

                    //DataTable dtCopy = new DataTable();
                    //DataRow[] drCopy;


                    #region 점검 기준에 따라 그리드에 데이터 바인드
                    ////조건에 맞은 정보가 한 줄이상 있을 시 정보를 복사하여 저장 시킨 후 바인드
                    //WinGrid grd = new WinGrid();
                    //switch (strPMPeriod)
                    //{
                    //    //전체
                    //    case "" :
                    //        this.uGridPMPlan.DataSource = dtPMPlanD;
                    //        this.uGridPMPlan.DataBind();
                    //        for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //        {
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //            {
                    //                this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //            }
                    //            #region 점검주기별 색 바꿈
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("DAY"))
                    //            {
                    //                //this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.LightSalmon;
                    //            }
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("WEK"))
                    //            {
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.Aquamarine;
                    //            }
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("MON"))
                    //            {
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.CornflowerBlue;
                    //            }
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("QUA"))
                    //            {
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.Orchid;
                    //            }
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("HAF"))
                    //            {
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.LemonChiffon;
                    //            }
                    //            if (this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.Equals("YEA"))
                    //            {
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.OldLace;
                    //            }
                    //            #endregion

                    //        }
                            
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        break;

                    //    //일간
                    //    case "DAY":
                    //        if (dtPMPlanD.Select("PMPeriodCode = 'DAY'").Count() != 0)
                    //        {

                    //            drCopy = dtPMPlanD.Select("PMPeriodCode = 'DAY'"); //검색된 값을 그리드 소스에 넣을 시 줄만 들어가서 컬럼인식을 못하여 화면에 안나옴
                    //            dtCopy = drCopy.CopyToDataTable();  //Row가 0 일 시 에러가 남

                    //            this.uGridPMPlan.DataSource = dtCopy;
                    //            this.uGridPMPlan.DataBind();
                    //            for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //            {
                    //                if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //                {
                    //                    this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //                }
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.LightSalmon;
                    //            }
                                
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        }
                    //        break;

                    //    //주간
                    //    case "WEK":
                    //        if (dtPMPlanD.Select("PMPeriodCode = 'WEK'").Count() != 0)
                    //        {
                    //            drCopy = dtPMPlanD.Select("PMPeriodCode = 'WEK'");
                    //            dtCopy = drCopy.CopyToDataTable();

                    //            this.uGridPMPlan.DataSource = dtCopy;
                    //            this.uGridPMPlan.DataBind();

                    //            for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //            {
                    //                if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //                {
                    //                    this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //                }
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.Aquamarine;
                    //            }
                                
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        }
                    //        break;

                    //    //월간
                    //    case "MON":
                    //        if (dtPMPlanD.Select("PMPeriodCode = 'MON'").Count() != 0)
                    //        {
                    //            drCopy = dtPMPlanD.Select("PMPeriodCode = 'MON'");
                    //            dtCopy = drCopy.CopyToDataTable();

                    //            this.uGridPMPlan.DataSource = dtCopy;
                    //            this.uGridPMPlan.DataBind();


                    //            for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //            {
                    //                if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //                {
                    //                    this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //                }
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.CornflowerBlue;
                    //            }
                                
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        }
                    //        break;

                    //    //분기
                    //    case "QUA":
                    //        if (dtPMPlanD.Select("PMPeriodCode = 'QUA'").Count() != 0)
                    //        {
                    //            drCopy = dtPMPlanD.Select("PMPeriodCode = 'QUA'");
                    //            dtCopy = drCopy.CopyToDataTable();

                    //            this.uGridPMPlan.DataSource = dtCopy;
                    //            this.uGridPMPlan.DataBind();


                    //            for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //            {
                    //                if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //                {
                    //                    this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //                }
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.Orchid;
                    //            }
                                
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        }
                    //        break;
                    //    //반기
                    //    case "HAF":
                    //        if (dtPMPlanD.Select("PMPeriodCode = 'HAF'").Count() != 0)
                    //        {
                    //            drCopy = dtPMPlanD.Select("PMPeriodCode = 'HAF'");
                    //            dtCopy = drCopy.CopyToDataTable();

                    //            this.uGridPMPlan.DataSource = dtCopy;
                    //            this.uGridPMPlan.DataBind();


                    //            for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //            {
                    //                if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //                {
                    //                    this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //                }
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.LemonChiffon;
                    //            }
                                
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        }
                    //        break;

                    //    //년간
                    //    case "YEA":
                    //        if (dtPMPlanD.Select("PMPeriodCode = 'YEA'").Count() != 0)
                    //        {
                    //            drCopy = dtPMPlanD.Select("PMPeriodCode = 'YEA'");
                    //            dtCopy = drCopy.CopyToDataTable();

                    //            this.uGridPMPlan.DataSource = dtCopy;
                    //            this.uGridPMPlan.DataBind();


                    //            for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    //            {
                    //                if (this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.Equals("Y"))
                    //                {
                    //                    this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //                }
                    //                this.uGridPMPlan.Rows[i].Appearance.BackColor = Color.OldLace;
                    //            }
                                
                    //            grd.mfSetAutoResizeColWidth(this.uGridPMPlan, 0);
                    //        }
                    //        break;

                    //}
                    #endregion
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtPMPlanD.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001115", "M001102",
                                                Infragistics.Win.HAlign.Right);
                }
               
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

               
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                brwChannel.mfCredentials(clsPMPlanD);

                DataTable dtPMPlanD = clsPMPlanD.mfSetPMPlanD_Revise();

                #region 필수입력사항
                //--------필수 입력 사항 확인 ------//
                if (this.uNumSearchYear.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000250", Infragistics.Win.HAlign.Right);

                    this.uNumSearchYear.Focus();
                    return;
                }
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                //if (this.uComboSearchEquipGroup.Value.ToString() == "" && this.uTextTechnicianName.Text == "" && this.uComboSearchArea.Value.ToString() == "" && this.uComboSearchProcGubun.Value.ToString() == "" && this.uComboSearchStation.Value.ToString() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "필수입력사항 확인", "(설비점검그룹) 또는 (작업자) 또는 (Area, Station, 설비공정구분) 중 선택해주세요", Infragistics.Win.HAlign.Right);

                //    return;
                //}

                #endregion

                #region 점검계획조정정보 저장
                if (this.uGridPMPlan.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridPMPlan.ActiveCell = this.uGridPMPlan.Rows[0].Cells[0];

                        int intCnt = this.uGridPMPlan.Rows[i].RowSelectorNumber;

                        if (this.uGridPMPlan.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Value == null 
                                || this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", intCnt + "M000540", Infragistics.Win.HAlign.Right);

                                this.uGridPMPlan.ActiveCell = this.uGridPMPlan.Rows[i].Cells["PMPlanDate"];
                                this.uGridPMPlan.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                DataRow drPM;
                                drPM = dtPMPlanD.NewRow();
                                drPM["PlanYear"] = Convert.ToDateTime(this.uGridPMPlan.Rows[i].Cells["PMPlanDate"].Value).Date.ToString("yyyy-MM-dd").Substring(0, 4);
                                drPM["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                                drPM["EquipCode"] = this.uGridPMPlan.Rows[i].Cells["EquipCode"].Value.ToString();
                                drPM["PMPeriodCode"] = this.uGridPMPlan.Rows[i].Cells["PMPeriodCode"].Value.ToString();
                                drPM["PMPlanDate"] = this.uGridPMPlan.Rows[i].Cells["PMPlanDate"].Value.ToString();
                                drPM["PMReviseDate"] = this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Value.ToString();
                                dtPMPlanD.Rows.Add(drPM);
                            }
                        }                        
                    }
                }

                if (dtPMPlanD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001053", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    string strRtn = clsPMPlanD.mfSavePMPlanD_Revise(dtPMPlanD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        if (strChk.Equals("T"))
                        {
                            strChk = "F";
                        }
                        mfSearch();
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strMsg = "";
                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMsg = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning,m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), strMsg,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfDelete()
        {
        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMPlan.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                
                //처리 로직//
                WinGrid grd = new WinGrid();

                //if (this.uGridPMPlan.Rows.Count > 0)
                grd.mfDownLoadGridToExcel(this.uGridPMPlan);

                /////////////


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        #region 콤보이벤트
        //공장선택에 따라 설비그룹  Station,위치,설비유형 콤보박스 Change
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    
                    this.uComboSearchStation.Items.Clear();

                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택",
                        "StationCode", "StationName", dtStation);

                    InitDisplay();

                    #region 주석
                    //this.uComboSearchArea.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();
                    ////-----------설비공정구분콤보 변화 -------------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    //brwChannel.mfCredentials(clsEquipProcGubun);

                    //DataTable dtGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //wCom.mfSetComboEditor(this.uComboSearchProcGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    //    "", "", "전체", "EquipProcGubunCode", "EquipProcGubunName", dtGubun);


                    ////------------Area콤보 변화-----------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);

                    //DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //wCom.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체",
                    //    "AreaCode", "AreaName", dtArea);

                    #endregion

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboEquipLoc.Items.Clear();

                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboEquipLoc, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);


                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    //해당콤보의 텍스트정보와 코드정보를 저장하여 아이템정보에 존재여부확인후 검색
                    string strText = this.uComboEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), "", "", 3);

                }
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    //해당콤보의 텍스트와 코드값을 저장하여 아이템정보에 존재여부확인후 검색
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboEquType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), this.uComboEquipLargeType.Value.ToString(), 1);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 텍스트이벤트

        private void uNumSearchYear_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 현재 년도로 초기화
                ed.Value = DateTime.Now.Year;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //SpinEditorButton 증감
        private void uNumSearchYear_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 버튼 클릭 시 유저 정보 보여주기
        private void uTextTechnician_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보 보내줌.
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                //해당컨트롤에 삽입
                this.uTextTechnician.Text = frmUser.UserID;
                this.uTextTechnicianName.Text = frmUser.UserName;

                //this.uComboSearchEquipGroup.SelectedIndex = 0;
                //this.uComboSearchStation.SelectedIndex = 0;
                //this.uComboEquType.SelectedIndex = 0;
                //this.uComboLoc.SelectedIndex = 0;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 ID입력 후 엔터누를 시 이름 자동입력
        private void uTextTechnician_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                   
                        this.uTextTechnicianName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextTechnician.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextTechnician.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextTechnicianName.Text = dtUser.Rows[0]["UserName"].ToString();
                        //this.uComboSearchEquipGroup.SelectedIndex = 0;
                        //this.uComboSearchStation.SelectedIndex = 0;
                        //this.uComboEquType.SelectedIndex = 0;
                        //this.uComboLoc.SelectedIndex = 0;
                        
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextTechnicianName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 그리드이벤트

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            if (strChk != "T")
            { strChk = "T"; }
        }

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        private void uGridPMPlan_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMPlan, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);
                    
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridPMPlan_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 버튼이벤트 클릭시 컬럼이 BtnDate 경우에 발생하며 점검진행여부가 미등록일경우 실행
                if (e.Cell.Column.Key.Equals("BtnDate")
                    && e.Cell.Row.Cells["PMResultName"].Value.ToString().Equals("N"))
                {
                    if (e.Cell.Row.Cells["PMReviseDate"].Value == null
                        || e.Cell.Row.Cells["PMReviseDate"].Value.ToString().Equals(string.Empty))
                        return;

                    //계획조정일
                    string strPMDate = Convert.ToDateTime(e.Cell.Row.Cells["PMPlanDate"].Value).Date.ToString("yyyy-MM-dd"); //점검계획일
                    string strDate = Convert.ToDateTime(e.Cell.Row.Cells["PMReviseDate"].Value).Date.ToString("yyyy-MM-dd"); //점검조정일
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString(); // 설비코드

                    e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                    //점검주기가 주간일 경우 동일한 계획일의 조정일을 복사한다.
                    if (e.Cell.Row.Cells["PMPeriodCode"].Value.ToString().Equals("WEK"))
                    {
                        //루프를 돌며 같은설비의 점검일을 교체한다.
                        for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                        {

                            //이벤트 발생줄이 아니고 발생줄 이후 같은 설비의 점검조정일을 복사한다.
                            if (!i.Equals(e.Cell.Row.Index)
                                && i > e.Cell.Row.Index
                                && this.uGridPMPlan.Rows[i].Cells["PMPlanDate"].Value.ToString().Equals(strPMDate)
                                && this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.ToString().Equals("N"))
                            {


                                this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Value = strDate;
                                this.uGridPMPlan.Rows[i].RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < this.uGridPMPlan.Rows.Count; i++)
                        {

                            //이벤트 발생줄이 아니고 발생줄 이후 같은 설비의 점검조정일을 복사한다.
                            if (!i.Equals(e.Cell.Row.Index)
                                && i > e.Cell.Row.Index
                                //&& this.uGridPMPlan.Rows[i].Cells["EquipCode"].Value.ToString().Equals(strEquipCode)
                                && this.uGridPMPlan.Rows[i].Cells["PMResultName"].Value.ToString().Equals("N"))
                            {


                                this.uGridPMPlan.Rows[i].Cells["PMReviseDate"].Value = strDate;
                                this.uGridPMPlan.Rows[i].RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #endregion

        //폼이 닫히기전에 그리드 설정값을 저장시킨다
        private void frmEQUZ0007_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 그리드 행 정리
        /// </summary>
        private void InitDisplay()
        {
            try
            {
                //Row삭제
                this.uGridPMPlan.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridPMPlan.Rows.All);
                this.uGridPMPlan.DeleteSelectedRows(false);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }



        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }






       
        

    }
}
