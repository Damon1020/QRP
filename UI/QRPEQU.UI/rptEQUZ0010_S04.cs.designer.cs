﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S04.
    /// </summary>
    partial class rptEQUZ0010_S04
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptEQUZ0010_S04));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textModel = new DataDynamics.ActiveReports.TextBox();
            this.textMaker = new DataDynamics.ActiveReports.TextBox();
            this.textSerialNo = new DataDynamics.ActiveReports.TextBox();
            this.txtPkg = new DataDynamics.ActiveReports.TextBox();
            this.txtBox9 = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.textModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMaker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPkg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textModel,
            this.textMaker,
            this.textSerialNo,
            this.txtPkg,
            this.txtBox9});
            this.detail.Height = 0.3041666F;
            this.detail.Name = "detail";
            // 
            // textModel
            // 
            this.textModel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textModel.DataField = "ModelName";
            this.textModel.Height = 0.3F;
            this.textModel.Left = 0.0189991F;
            this.textModel.Name = "textModel";
            this.textModel.Style = "font-size: 8pt; vertical-align: middle";
            this.textModel.Text = null;
            this.textModel.Top = 0F;
            this.textModel.Width = 1.3F;
            // 
            // textMaker
            // 
            this.textMaker.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textMaker.DataField = "VendorCode";
            this.textMaker.Height = 0.3F;
            this.textMaker.Left = 1.319F;
            this.textMaker.Name = "textMaker";
            this.textMaker.Style = "font-size: 8pt; vertical-align: middle";
            this.textMaker.Text = null;
            this.textMaker.Top = 0F;
            this.textMaker.Width = 1.35F;
            // 
            // textSerialNo
            // 
            this.textSerialNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSerialNo.DataField = "SerialNo";
            this.textSerialNo.Height = 0.3F;
            this.textSerialNo.Left = 2.669F;
            this.textSerialNo.Name = "textSerialNo";
            this.textSerialNo.Style = "font-size: 8pt; vertical-align: middle";
            this.textSerialNo.Text = null;
            this.textSerialNo.Top = 0F;
            this.textSerialNo.Width = 1.429F;
            // 
            // txtPkg
            // 
            this.txtPkg.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtPkg.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtPkg.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtPkg.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtPkg.DataField = "Package";
            this.txtPkg.Height = 0.3F;
            this.txtPkg.Left = 4.098F;
            this.txtPkg.Name = "txtPkg";
            this.txtPkg.Style = "font-size: 8pt; vertical-align: middle";
            this.txtPkg.Text = null;
            this.txtPkg.Top = 0F;
            this.txtPkg.Width = 1.637F;
            // 
            // txtBox9
            // 
            this.txtBox9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox9.DataField = "EtcDesc";
            this.txtBox9.Height = 0.3F;
            this.txtBox9.Left = 5.735F;
            this.txtBox9.Name = "txtBox9";
            this.txtBox9.Style = "font-size: 8pt; vertical-align: middle";
            this.txtBox9.Text = null;
            this.txtBox9.Top = 0F;
            this.txtBox9.Width = 1.469F;
            // 
            // rptEQUZ0010_S04
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.204F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMaker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPkg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.TextBox textModel;
        private DataDynamics.ActiveReports.TextBox textMaker;
        private DataDynamics.ActiveReports.TextBox textSerialNo;
        private DataDynamics.ActiveReports.TextBox txtPkg;
        private DataDynamics.ActiveReports.TextBox txtBox9;

    }
}
