﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQU0018 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

        public frmEQU0018()
        {
            InitializeComponent();
        }

        // 툴바 활성화를 위한 폼 이벤트

        private void frmEQU0018_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #region 컨트롤초기화

        private void frmEQU0018_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // Title 제목
            titleArea.mfSetLabelText("My Job - 주간", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 정비사 ID 설정
            this.uTextWorkerID.Text = m_resSys.GetString("SYS_USERID");
            this.uTextWorkerName.Text = m_resSys.GetString("SYS_USERNAME");

            // 년도 TextBox 기본값 설정
            this.uNumYear.Text = DateTime.Now.Year.ToString();
            SetToolAuth();
            InitLabel();
            InitCombo();
            InitWeekView();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelYear, "조회년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMonth, "조회월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelWeek, "조회주", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.ultraLabel4, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Month Combo Box
                ArrayList Key = new ArrayList();
                ArrayList Value = new ArrayList();

                for (int i = 1; i < 13; i++)
                {
                    if (i < 10)
                    {
                        Key.Add("0" + i.ToString());
                        Value.Add(i.ToString() + "月");
                    }
                    else
                    {
                        Key.Add(i);
                        Value.Add(i.ToString() + "月");
                    }
                }

                wCombo.mfSetComboEditor(this.uComboMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), Key, Value);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitWeekView()
        {
            try
            {
                //this.uWeekView.CalendarInfo = this.ultraCalendarInfo1; //소스사용?
                this.uWeekView.CalendarLook.ViewStyle = Infragistics.Win.UltraWinSchedule.ViewStyle.Office2007; //컨트롤 ViewStyle 바꿈

                this.uWeekView.AutoAppointmentDialog = false;   //기본적인 창나오는거막음 (무슨창인지는모름..)
                this.uWeekView.ShowClickToAddIndicator = Infragistics.Win.DefaultableBoolean.False; //기본적인 스케줄 추가기능 사용안함
                this.uWeekView.CalendarInfo.SelectTypeActivity = Infragistics.Win.UltraWinSchedule.SelectType.Single; //달력안의 스케줄을 선택타입.. (펼침,자동펼침,클릭안함,선택,SingleAutoDrag?)

                //this.uWeekView.CalendarLook.DayHeaderAppearance.NonDefaultSettings
                //this.uWeekView.CanSelect.Equals(true); //모지..
                //CalendarInfo.WeekRule = System.Globalization.CalendarWeekRule.FirstDay; // 해당달의 주의 시작 설정
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region 활성화 Toolbar

        public void mfSearch()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strYear = this.uNumYear.Value.ToString();
                string strMonth = this.uComboMonth.Value.ToString();
                string strWeek = this.uComboWeek.Value.ToString();
                string strWorker = this.uTextWorkerID.Text;

                #region 필수입력사항
                if (strYear.Equals(string.Empty) || !strYear.Length.Equals(4))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000207", "M001108", Infragistics.Win.HAlign.Right);

                    this.uNumYear.Focus();
                    return;
                }

                if (strMonth.Equals(string.Empty) || strMonth.Length > 2)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000213", "M001110", Infragistics.Win.HAlign.Right);

                    this.uComboMonth.DropDown();
                    return;
                }
                string strLang = m_resSys.GetString("SYS_LANG");
                if (strWeek.Equals(string.Empty) || strWeek.Length > 1)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000219",strLang)
                                                , strMonth + msg.GetMessge_Text("M000364",strLang), Infragistics.Win.HAlign.Right);

                    this.uComboWeek.DropDown();
                    return;
                }

                if (strWorker.Equals(string.Empty) || this.uTextWorkerName.Text.Equals(string.Empty))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001076", "M001079", Infragistics.Win.HAlign.Right);

                    this.uTextWorkerID.Focus();
                    return;
                }

                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.PMDIS), "PMDIS");
                QRPEQU.BL.EQUDIS.PMDIS clsPMPlanD = new QRPEQU.BL.EQUDIS.PMDIS();
                brwChannel.mfCredentials(clsPMPlanD);


                //DataTable dtPMPlanD = clsPMPlanD.mfReadMyJob_Week(m_resSys.GetString("SYS_PLANTCODE") ,strYear , strMonth , strWeek,strWorker , m_resSys.GetString("SYS_LANG"));

                //this.uWeekView.CalendarInfo.DataBindingsForNotes.BindingContextControl = this;          //??
                //this.uWeekView.CalendarInfo.DataBindingsForNotes.DataSource = dtPMPlanD;                //Source
                //this.uWeekView.CalendarInfo.DataBindingsForNotes.DateMember = "PMPlanDate";             //어느 컬럼기준의 날짜
                //this.uWeekView.CalendarInfo.DataBindingsForNotes.DescriptionMember = "PMResultInfo";   // 눈에보여지는 텍스트
                //this.uWeekView.CalendarInfo.DataBindingsForNotes.DataKeyMember = "EquipCode";           //해당텍스트의 키값


                DataTable dtPMResult = new DataTable();

                dtPMResult = clsPMPlanD.mfReadMyJob_Month
                    (m_resSys.GetString("SYS_PLANTCODE")
                    , strYear
                    , strMonth
                    , strWorker
                    , m_resSys.GetString("SYS_LANG"));

                this.uWeekView.CalendarInfo.DataBindingsForNotes.BindingContextControl = this;          //??
                this.uWeekView.CalendarInfo.DataBindingsForNotes.DataSource = dtPMResult;                //Source
                this.uWeekView.CalendarInfo.DataBindingsForNotes.DateMember = "PMPlanDate";             //어느 컬럼기준의 날짜
                this.uWeekView.CalendarInfo.DataBindingsForNotes.DescriptionMember = "ResultTotal";   // 눈에보여지는 텍스트
                this.uWeekView.CalendarInfo.DataBindingsForNotes.DataKeyMember = "EquipCode";           //해당텍스트의 키값


                //this.uWeekView.CalendarInfo.DataBindingsForNotes.OwnerKeyMember = "";  //컨트롤의 오너가(컨트롤안에 블럭) 나늬어져있으면 해당 오너에 바인드시킨다. 
                //this.uWeekView.CalendarInfo.DataBindingsForAppointments.BindingContextControl = this;

                //this.uWeekView.CalendarInfo.DataBindingsForAppointments.DataSource = dtPMPlanD;
                //this.uWeekView.CalendarInfo.DataBindingsForAppointments.
                

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                ActiveDay();

                if (dtPMResult.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {

        }

        public void mfDelete()
        {

        }

        public void mfCreate()
        {
           
        }

        public void mfPrint()
        {

        }

        public void mfExcel()
        {

        }

        #endregion

        #region Event..

        private void uComboMonth_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uNumYear.Value.ToString().Equals(string.Empty) || this.uComboMonth.Value.ToString().Equals(string.Empty))
                {
                    return;
                }

                int intYear = (int)(this.uNumYear.Value);
                int intMonth = Convert.ToInt32(this.uComboMonth.Value);
                int intDay = DateTime.DaysInMonth(intYear, intMonth);

                DateTime dateTemp = new DateTime(intYear, intMonth,1);

                int intCount = 1;

                // 선택한 달의 마지막날까지 중 일요일 인것만 골라서 Count에 + 시킨다.
                for (int i = 1; i <= intDay; i++)
                {
                    if(dateTemp.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        if(i>1)
                            intCount++;
                    }

                    dateTemp = dateTemp.AddDays(1);
                }


                //주자 콤보 설정하기
                DataTable dtWeekCount = new DataTable();
                dtWeekCount.Columns.Add("Key", typeof(string));
                dtWeekCount.Columns.Add("Value", typeof(string));

                
                for (int i = 0; i < intCount; i++)
                {
                    DataRow drCount = dtWeekCount.NewRow();

                    drCount["Key"] = i+1;
                    drCount["Value"] = i + 1 + "週";

                    dtWeekCount.Rows.Add(drCount);
                    
                    
                }

                this.uComboWeek.Items.Clear();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCom = new WinComboEditor();
                wCom.mfSetComboEditor(this.uComboWeek, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true,
                    false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "Key", "Value", dtWeekCount);
                //DateTime today = DateTime.Today;

                ////1일 날짜를 가져오는 방법
                //DateTime First_day = today.AddDays(1 - today.Day);

                ////첫재쭈 일요일
                //DateTime first_Sunday = First_day.AddDays(0 - (int)(First_day.DayOfWeek));

                ////마지막날짜
                //DateTime last_Day = today.AddMonths(1).AddDays(0 - today.Day);

                ////마지막주의 일요일
                //DateTime last_Sunday = last_Day.AddDays(0 - (int)(last_Day.DayOfWeek));

                ////이번달주
                //int this_mont_weekCount = ((last_Sunday.DayOfYear - first_Sunday.DayOfYear) / 7);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboWeek_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ActiveDay();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWorkerID_KeyDown(object sender, KeyEventArgs e)
        {
            //---아이디를 입력 후 어떤 키를 누를 때 마다 이름이 자동생성이 되거나 지워진다
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextWorkerName.Clear();
                }

                if (e.KeyData == Keys.Enter)
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    //승인자자 공장코드 저장
                    string strCarryOutAdmitID = this.uTextWorkerID.Text;
                    string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                    

                    //----- 아이디 공백 확인
                    if (strCarryOutAdmitID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextWorkerID.Focus();
                        return;
                    }

                    //-- 유저정보 가져오기 
                    
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCarryOutAdmitID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        // 이름 텍스트에 유저 이름을 삽입한다.
                        this.uTextWorkerName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001116", "M000883", Infragistics.Win.HAlign.Right);
                        this.uTextWorkerName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWorkerID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = m_resSys.GetString("SYS_PLANTCODE");
                //if (strPlant == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                //    this.uComboSearchPlant.DropDown();
                //    return;
                //}
                //-- 상속된폼 띄우기 -- //
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();


                // -- 상속된폼에서 선택된 정보 중 아이디와 이름을 가져와 각텍스트에 넣는다
                this.uTextWorkerID.Text = frmUser.UserID;
                this.uTextWorkerName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uNumYear_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete))
                {
                    if (!this.uComboWeek.Value.ToString().Equals(string.Empty))
                    {
                        this.uComboWeek.Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uWeekView_MoreActivityIndicatorClicked(object sender, Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventArgs e)
        {
            try
            {
                //if (e.Day.Notes[0].Tag == null)
                //{
                //    e.Day.Notes[0].Tag = e.Day.Notes[0].DataKey.ToString() + "," + e.Day.Notes[0].Date.ToString();

                //}
                Infragistics.Win.UltraWinSchedule.Note note = e.Day.Notes[0];
                this.uWeekView.CalendarInfo.Notes.Remove(note);
                this.uWeekView.CalendarInfo.Notes.Add(note);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uWeekView_BeforeScroll(object sender, Infragistics.Win.UltraWinSchedule.BeforeScrollEventArgs e)
        {
            //액션을 통한 이벤트를 제외한 다른 스크롤이동 이벤트는 취소시킨다.
            if (!e.ScrollAction.Equals(ScrollEventType.ThumbPosition))
                e.Cancel = true;

        }

        private void uNumYear_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼 이벤트 감소 증가
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uNumYear_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                this.uNumYear.Value = DateTime.Now.Year;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

         #endregion

        private void frmEQU0018_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uWeekView.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uWeekView.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보에 선택된 주로이동한다.
        /// </summary>
        private void ActiveDay()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //확실한제어필요, 년도 4자리 확인 && 월,조회 주 확인
                if (this.uNumYear.Value.ToString().Equals(string.Empty) || this.uComboMonth.Value.ToString().Equals(string.Empty) || this.uComboWeek.Value.ToString().Equals(string.Empty))
                {
                    //msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                    "확인창", "조회년도, 조회월 확인", "조회년도, 조회월를 확인해주세요.", Infragistics.Win.HAlign.Right);
                    return;
                }

                // 년도 , 월,, 주 정보 저장
                int intYear = (int)(this.uNumYear.Value);
                int intMonth = Convert.ToInt32(this.uComboMonth.Value);

                // 해당월의 일수 , 주 정보 저장
                int intDay = DateTime.DaysInMonth(intYear, intMonth);
                int intWeek = Convert.ToInt32(this.uComboWeek.Value);

                //특정년도의 월의 일을 1로 초기화한다
                DateTime dateTemp = new DateTime(intYear, intMonth, 1);



                //선택한 달의 월요일을 저장할 변수선언
                //string[] strMonday = new string[6];
                DateTime[] date = new DateTime[6];

                string strFirstDay = dateTemp.DayOfWeek.ToString();

                int intCount = 1;

                //선택한 달의 마지막날까지 중 월요일 인것만 골라서 저장.
                for (int i = 0; i < intDay; i++)
                {

                    if (i.Equals(0))
                    {
                        date[i] = dateTemp;
                    }


                    if (!i.Equals(0) && dateTemp.DayOfWeek.Equals(DayOfWeek.Monday))
                    {
                        date[intCount] = dateTemp;
                        intCount++;
                    }

                    dateTemp = dateTemp.AddDays(1);
                }

                //검색한월요일중 주차에맞는 월요일로 이동
                if (intWeek.Equals(1))
                {
                    if (!date[intWeek - 1].DayOfWeek.Equals(DayOfWeek.Monday))
                    {
                        this.uWeekView.CalendarInfo.ActivateDay(date[intWeek].AddDays(-7));
                    }
                }
                else if (date[intWeek] == null || date[intWeek].ToString().Equals(string.Empty))
                {
                    this.uWeekView.CalendarInfo.ActivateDay(date[intWeek - 2].AddDays(7));
                }
                else
                {
                    this.uWeekView.CalendarInfo.ActivateDay(date[intWeek - 1]);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //private void ultraCalendarInfo1_AfterSelectedNotesChange(object sender, EventArgs e)
        //{
        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //    if (this.ultraCalendarInfo1.SelectedNotes.Count == 1)
        //    {
        //        string strEquipCode = "";
        //        string strDate = "";
                
                
        //        if (this.ultraCalendarInfo1.SelectedNotes.Tag != null)
        //        {
        //            string[] strSplit;
        //            strSplit = this.ultraCalendarInfo1.SelectedNotes.Tag.ToString().Split(',');

        //            strEquipCode = strSplit[0];
        //            strDate = strSplit[1];
        //        }
        //        else
        //        {
        //            Infragistics.Win.UltraWinSchedule.Note note = this.ultraCalendarInfo1.SelectedNotes[0];


        //            strEquipCode = note.DataKey.ToString();
        //            strDate = note.Date.ToString();



        //        }
        //        string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
        //        string strUserID = this.uTextWorkerID.Text;

        //        // 상세 정보 Form Load
        //        frmEQU0017D1 childe = new frmEQU0017D1();
        //        childe.Show();
        //        childe.Display_Detail(strPlantCode, strEquipCode, strDate, strUserID);
        //        ////Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventArgs note = new Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventArgs(this.uWeekView, this.uWeekView.);

        //        ////Infragistics.Win.UltraWinSchedule.Note no = note.Day.Notes[0];

        //        //string stre = note.DataKey.ToString();
        //    }
        //}
        //private void ultraCalendarInfo1_AfterNoteAdded(object sender, Infragistics.Win.UltraWinSchedule.NoteEventArgs e)
        //{
            
        //    e.Note.CalendarInfo.SelectTypeActivity = Infragistics.Win.UltraWinSchedule.SelectType.Single;
        //}

        




    }
}
