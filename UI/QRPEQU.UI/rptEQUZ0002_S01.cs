﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_02.
    /// </summary>
    public partial class rptEQUZ0002_S01 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0002_S01()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            
        }
        public rptEQUZ0002_S01(DataTable dtItem,int intRow)
        {

            // 화면 비율 맞추기.
            int intCnt;
            DataRow dr;
            if (intRow == 1)
            {
                if (dtItem.Rows.Count.Equals(0) || dtItem.Rows.Count < 4)
                {
                    intCnt = 4 - dtItem.Rows.Count;

                    for (int i = 0; i < intCnt; i++)
                    {
                        dr = dtItem.NewRow();
                        dtItem.Rows.Add(dr);
                    }

                }

            }
            else
            {
                if (dtItem.Rows.Count == 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        dr = dtItem.NewRow();
                        dtItem.Rows.Add(dr);
                    }
                }
                if (5 - intRow > 0)
                {
                    // 10 - 8 - 5 = -1
                    intCnt = 5 - intRow - dtItem.Rows.Count;
                    if (intCnt > 0)
                    {
                        for (int i = 0; i < intCnt; i++)
                        {
                            dr = dtItem.NewRow();
                            dtItem.Rows.Add(dr);
                        }
                    }
                }
                
            }

            this.DataSource = dtItem;
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
 


        }
    }
}
