﻿namespace QRPEQU.UI
{
    partial class frmEQU0019
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0019));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProcGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckSPChg = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckDurableTransfe = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckReapairReq = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckPMResult = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckDeviceChg = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckCerti = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCheck = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uButtonSelect = new Infragistics.Win.Misc.UltraButton();
            this.uTextEquipImageFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridEquipDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uPicEquipImage = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uTextERPASSETSNAME = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSTSDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelERPASSETSNAME = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVedor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSTSDate = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSPChg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDurableTransfe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReapairReq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckPMResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDeviceChg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCerti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipImageFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextERPASSETSNAME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uComboSearchType
            // 
            this.uComboSearchType.Location = new System.Drawing.Point(660, 36);
            this.uComboSearchType.MaxLength = 50;
            this.uComboSearchType.Name = "uComboSearchType";
            this.uComboSearchType.Size = new System.Drawing.Size(124, 21);
            this.uComboSearchType.TabIndex = 15;
            // 
            // uLabelSearchType
            // 
            this.uLabelSearchType.Location = new System.Drawing.Point(556, 36);
            this.uLabelSearchType.Name = "uLabelSearchType";
            this.uLabelSearchType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchType.TabIndex = 14;
            // 
            // uComboSearchLoc
            // 
            this.uComboSearchLoc.Location = new System.Drawing.Point(388, 36);
            this.uComboSearchLoc.MaxLength = 50;
            this.uComboSearchLoc.Name = "uComboSearchLoc";
            this.uComboSearchLoc.Size = new System.Drawing.Size(124, 21);
            this.uComboSearchLoc.TabIndex = 13;
            // 
            // uLabelSearchLoc
            // 
            this.uLabelSearchLoc.Location = new System.Drawing.Point(284, 36);
            this.uLabelSearchLoc.Name = "uLabelSearchLoc";
            this.uLabelSearchLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLoc.TabIndex = 12;
            // 
            // uComboProcGubun
            // 
            this.uComboProcGubun.Location = new System.Drawing.Point(1024, 8);
            this.uComboProcGubun.MaxLength = 50;
            this.uComboProcGubun.Name = "uComboProcGubun";
            this.uComboProcGubun.Size = new System.Drawing.Size(24, 21);
            this.uComboProcGubun.TabIndex = 11;
            this.uComboProcGubun.Visible = false;
            // 
            // uComboStation
            // 
            this.uComboStation.Location = new System.Drawing.Point(116, 36);
            this.uComboStation.MaxLength = 50;
            this.uComboStation.Name = "uComboStation";
            this.uComboStation.Size = new System.Drawing.Size(124, 21);
            this.uComboStation.TabIndex = 11;
            this.uComboStation.ValueChanged += new System.EventHandler(this.uComboStation_ValueChanged);
            // 
            // uComboArea
            // 
            this.uComboArea.Location = new System.Drawing.Point(1000, 8);
            this.uComboArea.MaxLength = 50;
            this.uComboArea.Name = "uComboArea";
            this.uComboArea.Size = new System.Drawing.Size(24, 21);
            this.uComboArea.TabIndex = 11;
            this.uComboArea.Visible = false;
            // 
            // uLabelProcGubun
            // 
            this.uLabelProcGubun.Location = new System.Drawing.Point(1032, 8);
            this.uLabelProcGubun.Name = "uLabelProcGubun";
            this.uLabelProcGubun.Size = new System.Drawing.Size(12, 20);
            this.uLabelProcGubun.TabIndex = 0;
            this.uLabelProcGubun.Visible = false;
            // 
            // uComboEquipGroup
            // 
            this.uComboEquipGroup.Location = new System.Drawing.Point(388, 12);
            this.uComboEquipGroup.MaxLength = 50;
            this.uComboEquipGroup.Name = "uComboEquipGroup";
            this.uComboEquipGroup.Size = new System.Drawing.Size(124, 21);
            this.uComboEquipGroup.TabIndex = 11;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(12, 36);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 0;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(124, 21);
            this.uComboSearchPlant.TabIndex = 11;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelArea
            // 
            this.uLabelArea.Location = new System.Drawing.Point(1008, 8);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(12, 20);
            this.uLabelArea.TabIndex = 0;
            this.uLabelArea.Visible = false;
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(284, 12);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroup.TabIndex = 0;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            // 
            // uDateFromDate
            // 
            this.uDateFromDate.Location = new System.Drawing.Point(432, 160);
            this.uDateFromDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateFromDate.Name = "uDateFromDate";
            this.uDateFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDate.TabIndex = 20;
            // 
            // uDateToDate
            // 
            this.uDateToDate.Location = new System.Drawing.Point(544, 160);
            this.uDateToDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateToDate.Name = "uDateToDate";
            this.uDateToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToDate.TabIndex = 19;
            // 
            // uCheckSPChg
            // 
            this.uCheckSPChg.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSPChg.Location = new System.Drawing.Point(668, 208);
            this.uCheckSPChg.Name = "uCheckSPChg";
            this.uCheckSPChg.Size = new System.Drawing.Size(100, 20);
            this.uCheckSPChg.TabIndex = 17;
            this.uCheckSPChg.Text = "SP교체";
            // 
            // uCheckDurableTransfe
            // 
            this.uCheckDurableTransfe.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckDurableTransfe.Location = new System.Drawing.Point(544, 208);
            this.uCheckDurableTransfe.Name = "uCheckDurableTransfe";
            this.uCheckDurableTransfe.Size = new System.Drawing.Size(120, 20);
            this.uCheckDurableTransfe.TabIndex = 17;
            this.uCheckDurableTransfe.Text = "치공구교체";
            // 
            // uCheckReapairReq
            // 
            this.uCheckReapairReq.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckReapairReq.Location = new System.Drawing.Point(432, 208);
            this.uCheckReapairReq.Name = "uCheckReapairReq";
            this.uCheckReapairReq.Size = new System.Drawing.Size(108, 20);
            this.uCheckReapairReq.TabIndex = 17;
            this.uCheckReapairReq.Text = "수리결과";
            // 
            // uCheckPMResult
            // 
            this.uCheckPMResult.Checked = true;
            this.uCheckPMResult.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uCheckPMResult.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckPMResult.Location = new System.Drawing.Point(656, 184);
            this.uCheckPMResult.Name = "uCheckPMResult";
            this.uCheckPMResult.Size = new System.Drawing.Size(108, 20);
            this.uCheckPMResult.TabIndex = 16;
            this.uCheckPMResult.Text = "점검결과";
            // 
            // uCheckDeviceChg
            // 
            this.uCheckDeviceChg.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckDeviceChg.Location = new System.Drawing.Point(544, 184);
            this.uCheckDeviceChg.Name = "uCheckDeviceChg";
            this.uCheckDeviceChg.Size = new System.Drawing.Size(108, 20);
            this.uCheckDeviceChg.TabIndex = 15;
            this.uCheckDeviceChg.Text = "품종교체";
            // 
            // uCheckCerti
            // 
            this.uCheckCerti.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCerti.Location = new System.Drawing.Point(432, 184);
            this.uCheckCerti.Name = "uCheckCerti";
            this.uCheckCerti.Size = new System.Drawing.Size(108, 20);
            this.uCheckCerti.TabIndex = 13;
            this.uCheckCerti.Text = "설비인증";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(532, 164);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "~";
            // 
            // uLabelSearchCheck
            // 
            this.uLabelSearchCheck.Location = new System.Drawing.Point(328, 184);
            this.uLabelSearchCheck.Name = "uLabelSearchCheck";
            this.uLabelSearchCheck.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCheck.TabIndex = 3;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(328, 160);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 2;
            // 
            // uGridEquipList
            // 
            this.uGridEquipList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance9;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance10;
            appearance11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance11;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance12;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance28;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance31;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(0, 100);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(1070, 720);
            this.uGridEquipList.TabIndex = 7;
            this.uGridEquipList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridEquipList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 8;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonSelect);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateFromDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipImageFile);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateToDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridEquipDetail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckSPChg);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uPicEquipImage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckDurableTransfe);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextERPASSETSNAME);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckReapairReq);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSTSDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckPMResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckDeviceChg);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckCerti);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextModel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelERPASSETSNAME);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelModel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVedor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraLabel1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSTSDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSearchCheck);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSearchDate);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uButtonSelect
            // 
            this.uButtonSelect.Location = new System.Drawing.Point(776, 200);
            this.uButtonSelect.Name = "uButtonSelect";
            this.uButtonSelect.Size = new System.Drawing.Size(88, 28);
            this.uButtonSelect.TabIndex = 23;
            this.uButtonSelect.Text = "ultraButton1";
            this.uButtonSelect.Click += new System.EventHandler(this.uButtonSelect_Click);
            // 
            // uTextEquipImageFile
            // 
            this.uTextEquipImageFile.Location = new System.Drawing.Point(852, 16);
            this.uTextEquipImageFile.Name = "uTextEquipImageFile";
            this.uTextEquipImageFile.ReadOnly = true;
            this.uTextEquipImageFile.Size = new System.Drawing.Size(200, 21);
            this.uTextEquipImageFile.TabIndex = 22;
            this.uTextEquipImageFile.Visible = false;
            // 
            // uGridEquipDetail
            // 
            this.uGridEquipDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipDetail.DisplayLayout.Appearance = appearance14;
            this.uGridEquipDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridEquipDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipDetail.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipDetail.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridEquipDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDetail.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipDetail.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridEquipDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipDetail.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDetail.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridEquipDetail.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridEquipDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipDetail.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridEquipDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridEquipDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipDetail.Location = new System.Drawing.Point(8, 236);
            this.uGridEquipDetail.Name = "uGridEquipDetail";
            this.uGridEquipDetail.Size = new System.Drawing.Size(1044, 412);
            this.uGridEquipDetail.TabIndex = 21;
            this.uGridEquipDetail.Text = "ultraGrid1";
            // 
            // uPicEquipImage
            // 
            this.uPicEquipImage.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPicEquipImage.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uPicEquipImage.Location = new System.Drawing.Point(12, 12);
            this.uPicEquipImage.Name = "uPicEquipImage";
            this.uPicEquipImage.Size = new System.Drawing.Size(304, 220);
            this.uPicEquipImage.TabIndex = 20;
            // 
            // uTextERPASSETSNAME
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextERPASSETSNAME.Appearance = appearance8;
            this.uTextERPASSETSNAME.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextERPASSETSNAME.Location = new System.Drawing.Point(432, 88);
            this.uTextERPASSETSNAME.Name = "uTextERPASSETSNAME";
            this.uTextERPASSETSNAME.ReadOnly = true;
            this.uTextERPASSETSNAME.Size = new System.Drawing.Size(100, 21);
            this.uTextERPASSETSNAME.TabIndex = 19;
            // 
            // uTextSTSDate
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSDate.Appearance = appearance7;
            this.uTextSTSDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSDate.Location = new System.Drawing.Point(700, 64);
            this.uTextSTSDate.Name = "uTextSTSDate";
            this.uTextSTSDate.ReadOnly = true;
            this.uTextSTSDate.Size = new System.Drawing.Size(100, 21);
            this.uTextSTSDate.TabIndex = 18;
            // 
            // uTextVendor
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance6;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(432, 64);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(100, 21);
            this.uTextVendor.TabIndex = 17;
            // 
            // uTextSerialNo
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance5;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(700, 40);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSerialNo.TabIndex = 16;
            // 
            // uTextModel
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance4;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(432, 40);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(100, 21);
            this.uTextModel.TabIndex = 15;
            // 
            // uTextEquipName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance3;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(700, 16);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 14;
            // 
            // uTextEquipCode
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance2;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(432, 16);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 13;
            // 
            // uLabelERPASSETSNAME
            // 
            this.uLabelERPASSETSNAME.Location = new System.Drawing.Point(328, 88);
            this.uLabelERPASSETSNAME.Name = "uLabelERPASSETSNAME";
            this.uLabelERPASSETSNAME.Size = new System.Drawing.Size(100, 20);
            this.uLabelERPASSETSNAME.TabIndex = 10;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(328, 16);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCode.TabIndex = 4;
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(596, 16);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipName.TabIndex = 5;
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(328, 40);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(100, 20);
            this.uLabelModel.TabIndex = 6;
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(596, 40);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSerialNo.TabIndex = 7;
            // 
            // uLabelVedor
            // 
            this.uLabelVedor.Location = new System.Drawing.Point(328, 64);
            this.uLabelVedor.Name = "uLabelVedor";
            this.uLabelVedor.Size = new System.Drawing.Size(100, 20);
            this.uLabelVedor.TabIndex = 8;
            // 
            // uLabelSTSDate
            // 
            this.uLabelSTSDate.Location = new System.Drawing.Point(596, 64);
            this.uLabelSTSDate.Name = "uLabelSTSDate";
            this.uLabelSTSDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSTSDate.TabIndex = 9;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 4;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQU0019
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquipList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0019";
            this.Load += new System.EventHandler(this.frmEQU0019_Load);
            this.Activated += new System.EventHandler(this.frmEQU0019_Activated);
            this.Resize += new System.EventHandler(this.frmEQU0019_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSPChg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDurableTransfe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReapairReq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckPMResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDeviceChg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCerti)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipImageFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextERPASSETSNAME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReapairReq;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckPMResult;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckDeviceChg;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCerti;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCheck;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipDetail;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPicEquipImage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextERPASSETSNAME;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSTSDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelERPASSETSNAME;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelVedor;
        private Infragistics.Win.Misc.UltraLabel uLabelSTSDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSPChg;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckDurableTransfe;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboArea;
        private Infragistics.Win.Misc.UltraLabel uLabelProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipImageFile;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton uButtonSelect;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLoc;
    }
}