﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQU0005.cs                                         */
/* 프로그램명   : 점검결과등록(팝업창)                                  */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0005D1 : Form
    {
        #region 전역변수
        

        private DataTable dtRequest;
        private DataTable dtAccept;

        private string strRepairReqCode;

        private string strPackage;

        private string strKind;

        public string Kind
        {
            get { return strKind; }
            set { strKind = value; }
        }

        public string Package
        {
            get { return strPackage; }
            set { strPackage = value; }
        }

        public string RepairReqCode
        {
            get { return strRepairReqCode; }
            set { strRepairReqCode = value; }
        }

        // 리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        #endregion

        #region Load

        public frmEQU0005D1()
        {
            InitializeComponent();
        }

        public frmEQU0005D1(DataTable dtEquipCode,DataTable dtDetail)
        {
            InitializeComponent();

            //컨트롤초기화
            InitLabel();
            InitText(dtDetail);
            InitButton();
            InitGrid(dtDetail.Rows[0]["PlantCode"].ToString());
            InitGroupBox();
            InitCombo(dtEquipCode);
        }

        public frmEQU0005D1(DataTable dtEquipCode, DataTable dtDetail, DataTable _dtRequest, DataTable _dtAccept)
        {
            InitializeComponent();

            //컨트롤초기화
            InitLabel();
            InitText(dtDetail);
            InitButton();
            InitGrid(dtDetail.Rows[0]["PlantCode"].ToString());
            InitGroupBox();
            InitCombo(dtEquipCode);

            dtRequest = _dtRequest;
            dtAccept = _dtAccept;
        }

        private void frmEQU0005D1_Load(object sender, EventArgs e)
        {
            //PM이아닌 수리에서 넘어 왔다면.
            if (strKind != null)
            {
                this.uLabelPMPlanDate.Visible = false;
                this.uTextPMPlanDate.Visible = false;
            }

            QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #endregion

        #region 컨트롤초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelPMPlanDate, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelOutDate, "사용일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelUserID, "사용담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDesc, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        /// <param name="dtEquip">설비정보</param>
        private void InitCombo(DataTable dtEquip)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor com = new WinComboEditor();

                if(dtEquip.Rows.Count == 1)
                    com.mfSetComboEditor(this.uComboEquipCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true,
                                    false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, dtEquip.Rows[0]["EquipCode"].ToString(), "", "선택", "EquipCode", "EquipName", dtEquip);
                else
                    com.mfSetComboEditor(this.uComboEquipCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true,
                                    false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "EquipCode", "EquipName", dtEquip);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        /// <param name="dtDetail">상세정보</param>
        private void InitText(DataTable dtDetail)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                this.uTextPlantCode.Text = dtDetail.Rows[0]["PlantCode"].ToString();
                this.uTextPMPlanDate.Text = dtDetail.Rows[0]["PMPlanDate"].ToString();
                this.uDateOut.Value = dtDetail.Rows[0]["PMPlanDate"];
                this.uTextUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSave, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
                
                btn.mfSetButton(this.uButtonDeleteDurableRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteSPRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid(string strPlantCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                #region 수리출고 구성품
                // 수리출고구성품 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridRepair, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridRepair, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                wGrid.mfSetGridColumn(this.uGridRepair, 0, "RepairGICode", "수리출고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "RepairGIGubunCode", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "INN");

                
                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CurDurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CurDurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "CurLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

               
                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgDurableInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgDurableMatCode", "교체구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgDurableMatName", "교체구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "Qty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "ChgQty", "교체수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                wGrid.mfSetGridColumn(this.uGridRepair, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridRepair, 0, "CancelFlag", "수리취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                #region DropDown

                //--헤더에있는 체크박스를 사용안함 --//
               //this.uGridRepair.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //--수리출고구분--//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtRepairGICode = clsCommonCode.mfReadCommonCode("C0016", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "RepairGIGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtRepairGICode);

                //-----금형치공구창고정보-----//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                wGrid.mfSetGridColumnValueList(this.uGridRepair, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                #endregion

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridRepair, 0);

                #endregion

                #region 사용자 SparePart Grid

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridUseSP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurSparePartCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurSparePartName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurInputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurSpec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurMaker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSPInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSparePartCode", "사용구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSparePartName", "사용구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "Qty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgInputQty", "사용량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSpec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgMaker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CancelFlag", "수리취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                //헤더에있는 체크박스 안보임
                //this.uGridUseSP.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #region DropDown

                //단위정보
                wGrid.mfSetGridColumnValueList(this.uGridUseSP, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);


                // SP 창고정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);
                DataTable dtSPInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridUseSP, 0, "ChgSPInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "선택", dtSPInventory);

                #endregion

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridUseSP, 0);

                #endregion

                this.uGridRepair.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridRepair.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridUseSP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridUseSP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxDurable, GroupBoxType.DETAIL, "치공구교체", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxDurable.Appearance.FontData.SizeInPoints = 9;
                this.uGroupBoxDurable.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupSparePart, GroupBoxType.DETAIL, "SparePart교체", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupSparePart.Appearance.FontData.SizeInPoints = 9;
                this.uGroupSparePart.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion 

        #region 이벤트

        //설비코드가 바뀔시 기존정보 유무를 판단하여 코드를 가져온다.
        private void uComboEquipCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                InitDisplay();

                //설비코드,공장정보저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uComboEquipCode.Value.ToString();


                if (!strPlantCode.Equals(string.Empty) && !strEquipCode.Equals(string.Empty))
                {

                    //현재 콤보텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string str = this.uComboEquipCode.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipCode.Items.Count; i++)
                    {
                        if (strEquipCode.Equals(this.uComboEquipCode.Items[i].DataValue.ToString()) && str.Equals(this.uComboEquipCode.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        //점검일,계획년도 저장
                        string strPMPlanDate = this.uTextPMPlanDate.Text;
                        string strPlanYear = strPMPlanDate.Substring(0, 4);

                        //치공구교체 그리드가 수정불가시 수정가능으로 초기화
                        if (this.uGridRepair.Enabled.Equals(false))
                        {
                            this.uGridRepair.Enabled = true;
                        }


                        //수리출고구분코드 및 설비모델 정보를 가져온다.
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                        QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                        brwChannel.mfCredentials(clsDurableMatRepairH);

                        DataTable dtCode = clsDurableMatRepairH.mfReadDurableMatReapairHCode(strPlantCode, strPlanYear, "PM", strEquipCode, strPMPlanDate);

                        //정보가 있을시 수리출고구분 텍스트에 삽입
                        if (dtCode.Rows.Count > 0)
                        {
                            this.uTextReairGICode.Text = dtCode.Rows[0]["RepairGICode"].ToString();
                            this.uComboEquipCode.Tag = dtCode.Rows[0]["ModelName"].ToString();
                        }



                        //BOM정보를 가져온다
                        SearchSPBOM(strPlantCode, strEquipCode);
                        SearchDurableBOM(strPlantCode, strEquipCode);
                    }
                }
                else
                {
                    //임시 데이터 테이블 생성하여 그리드 콤보에 뿌려줌
                    DataTable dtTemp = new DataTable();
                    dtTemp.Columns.Add("DurableMatCode");
                    dtTemp.Columns.Add("DurableMatName");
                    dtTemp.Columns.Add("SparePartCode");
                    dtTemp.Columns.Add("SparePartName");
                    dtTemp.Columns.Add("LotNo");
                    dtTemp.Columns.Add("InputQty");
                    dtTemp.Columns.Add("UnitName");

                    DataRow dr = dtTemp.NewRow();
                    dtTemp.Rows.Add(dr);

                    WinGrid wGrid = new WinGrid();

                    string strValue = "SparePartCode,SparePartName,InputQty,UnitName";
                    string strText = "구성품코드,구성품명,Qty,단위";

                    this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();
                    wGrid.mfSetGridColumnValueGridList(this.uGridUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                        , "SparePartCode", "SparePartName", dtTemp);

                    strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                    strText = "구성품코드,구성품명,LotNo,수량,단위";

                    this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                    wGrid.mfSetGridColumnValueGridList(this.uGridRepair, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                        , "DurableMatCode", "DurableMatName", dtTemp);

                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region TextEvent

        //출고요청자 키다운발생시
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //출고요청자ID, 공장 저장
                string strUserID = this.uTextUserID.Text;
                string strPlantCode = this.uTextPlantCode.Text;

                //키 데이터가 엔터키 고 내용이 있는 경우 조회한다.
                if (e.KeyData.Equals(Keys.Enter) && !strUserID.Equals(string.Empty))
                {
                    //공장정보조회
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //유저정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //유저정보조회매서드 호출
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    //정보가 있을경우 등록 없을 경우 명초기화
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                             , "M001264", "M001104", "M000883", Infragistics.Win.HAlign.Right);

                        if (!this.uTextUserName.Text.Equals(string.Empty))
                            this.uTextUserName.Clear();

                        return;
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //출고요청자 버튼클릭시 유저정보팝업창을 띄운다.
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = this.uTextPlantCode.Text;

                //공장정보조회
                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //출고요청자 값 변경시 이름초기화
        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextUserName.Text.Equals(string.Empty))
                    this.uTextUserName.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드 셀변경시 편집이미지 삽입
        private void uGridAll_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        #endregion

        #region SparePart그리드

        private void uGridUseSP_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ChgSPInventoryCode"))
                {
                    //설비코드 미선택 시
                    if (this.uComboEquipCode.Value.ToString().Equals(string.Empty))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M000882", "M000721", Infragistics.Win.HAlign.Right);
                        this.uComboEquipCode.DropDown();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.(SparePart)
        private void uGridUseSP_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridUseSP, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }
                //System ResourceInfo 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //--이벤트 발생한 셀의 컬럼 저장--//
                string strColumn = e.Cell.Column.Key;

                //--체크박스가 수정불가 일시 false로 되돌린다 
                if (strColumn == "Check")
                {
                    if (e.Cell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    {
                        e.Cell.Value = false;
                        return;
                    }
                }
                ////-- 수리취소 체크 박스를 체크 해제하면 편집이미지 X --//
                //if (strColumn == "CancelFlag")
                //{
                //    if (e.Cell.Value.ToString() == "False")
                //    {
                //        e.Cell.Row.RowSelectorAppearance.Image = null;
                //        return;
                //    }
                //}


                #region 구성품수량
                if (strColumn.Equals("CurInputQty"))
                {
                    if (!e.Cell.Value.ToString().Equals("0") && !e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty))
                    {
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000314", "M000874", Infragistics.Win.HAlign.Right);
                            e.Cell.Value = e.Cell.Tag;
                            return;
                        }
                    }
                }
                #endregion

                #region 교체수량

                if (strColumn.Equals("ChgInputQty"))
                {
                    if (!e.Cell.Value.ToString().Equals("0") && !e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString().Equals(string.Empty))
                    {
                        //교체수량이 기존수량보다 많거나 사용량이 재고량보다 많을 경우 메세지 박스
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Tag) && !e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000302", "M000875", Infragistics.Win.HAlign.Right);

                            //재고량이 기존수량보다 크고 기존수량이 있는 경우
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Tag)
                                && e.Cell.Row.Cells["CurInputQty"].Tag != null)
                            {
                                e.Cell.Value = e.Cell.Row.Cells["CurInputQty"].Tag;
                            }
                            else
                            {
                                e.Cell.Value = e.Cell.Row.Cells["Qty"].Value;
                            }
                            return;
                        }

                        #region 중복구성품재고구분

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;


                        for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                        {
                            if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridUseSP.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridUseSP.Rows[i].Hidden.Equals(false))
                            {

                                if (e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value) &&
                                    e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value.ToString();

                            msg.mfSetMessageBox(MessageBoxType.Error,m_resSys.GetString("SYS_FONTNAME") ,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000617", strLang), msg.GetMessge_Text("M000892", strLang) + strQty + msg.GetMessge_Text("M000010", strLang), Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }


                        #endregion

                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드 내에 있는 콤보박스의 선택 정보를 읽음
        private void uGridUseSP_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //컬럼저장
                string strColumns = e.Cell.Column.Key;

                //선택한 리스트의 Value값과 Text값 저장
                string strValue = "";
                string strText = "";
                

                //ColumValueList 인지 CellValueList인지에 따라 저장
                if (e.Cell.Column.ValueList != null)
                {
                    strValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strText = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strValue = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strText = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }

                //BL호출
                //공장정보,설비코드 저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uComboEquipCode.Value.ToString();

                #region 구성품명

                if (strColumns.Equals("CurSparePartName"))
                {
                    //Key 값이 공백이 아닐경우
                    if (!strValue.Equals(string.Empty))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                        QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                        brwChannel.mfCredentials(clsEquipSPBOM);

                        DataTable dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, strValue, m_resSys.GetString("SYS_LANG"));


                        //정보가 있을 경우
                        if (dtSPBOM.Rows.Count > 0)
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");

                            //단위저장 
                            e.Cell.Row.Cells["CurSparePartCode"].Tag = dtSPBOM.Rows[0]["UnitCode"];

                            e.Cell.Row.Cells["CurSparePartCode"].Value = strValue;

                            e.Cell.Row.Cells["CurInputQty"].Tag = Convert.ToInt32(dtSPBOM.Rows[0]["InputQty"]) - Convert.ToInt32(dtSPBOM.Rows[0]["ChgStandbyQty"]);
                            e.Cell.Row.Cells["CurInputQty"].Value = Convert.ToInt32(dtSPBOM.Rows[0]["InputQty"]) - Convert.ToInt32(dtSPBOM.Rows[0]["ChgStandbyQty"]);
                            e.Cell.Row.Cells["CurSpec"].Value = dtSPBOM.Rows[0]["Spec"];
                            e.Cell.Row.Cells["CurMaker"].Value = dtSPBOM.Rows[0]["Maker"];

                            //선택한 정보가 그리드내에 있으면 다시입력
                            for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                            {
                                if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null
                                    && this.uGridUseSP.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                    && this.uGridUseSP.Rows[i].Hidden.Equals(false))
                                {
                                    if (!i.Equals(e.Cell.Row.Index) &&
                                        this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(strValue))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error,m_resSys.GetString("SYS_FONTNAME") ,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000311",strLang)
                                                    , msg.GetMessge_Text("M000843",strLang) + this.uGridUseSP.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000459",strLang), Infragistics.Win.HAlign.Right);

                                        //단위저장 
                                        e.Cell.Row.Cells["CurSparePartCode"].Tag = string.Empty;

                                        e.Cell.Row.Cells["CurSparePartCode"].Value = string.Empty;
                                        e.Cell.Row.Cells["CurSparePartName"].Value = string.Empty;

                                        e.Cell.Row.Cells["CurInputQty"].Tag = 0;
                                        e.Cell.Row.Cells["CurInputQty"].Value = 0;

                                        e.Cell.Row.Cells["CurSpec"].Value = string.Empty;
                                        e.Cell.Row.Cells["CurMaker"].Value = string.Empty;

                                        return;
                                    }
                                }
                            }

                            if (Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Value))
                            {
                                e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                            }

                        }
                    }

                }

                #endregion

                #region SP창고

                if (strColumns.Equals("ChgSPInventoryCode"))
                {
                    //SP창고가 공백이 아닌경우
                    if (!strValue.Equals(string.Empty))
                    {
                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                        DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strValue, m_resSys.GetString("SYS_LANG"));

                        if (dtChgSP.Rows.Count > 0)
                        {
                            string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                            string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                            WinGrid wGrid = new WinGrid();

                            //GridList 그리드에 삽입
                            wGrid.mfSetGridCellValueGridList(this.uGridUseSP, 0, e.Cell.Row.Index, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                                , "SparePartCode", "SparePartName", dtChgSP);
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000121", "M001252", Infragistics.Win.HAlign.Right);
                            return;
                        }

                    }
                }

                #endregion

                #region 사용구성품

                if (strColumns.Equals("ChgSparePartName"))
                {
                    if (!strValue.Equals(string.Empty))
                    {
                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //SP현재고 조회
                        DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString(), strValue, m_resSys.GetString("SYS_LANG"));

                        if (dtSPStock.Rows.Count > 0)
                        {
                            //기존구성품코드가 있는 상태에서 선택한 SP의 단위와 기존SP의 단위가 틀리면 리턴
                            if (!e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty) && !e.Cell.Row.Cells["CurSparePartCode"].Tag.Equals(dtSPStock.Rows[0]["UnitCode"]))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000360", "M000661", Infragistics.Win.HAlign.Right);
                                return;
                            }

                            //선택 구성품이 바뀔 때 교체수량이 있을 경우 초기화
                            if (e.Cell.Row.Cells["ChgInputQty"].Value.ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                            }

                            e.Cell.Row.Cells["ChgSparePartCode"].Value = dtSPStock.Rows[0]["SparePartCode"];
                            e.Cell.Row.Cells["Qty"].Value = dtSPStock.Rows[0]["Qty"];
                            e.Cell.Row.Cells["Qty"].Tag = dtSPStock.Rows[0]["Qty"];

                            e.Cell.Row.Cells["UnitCode"].Value = dtSPStock.Rows[0]["UnitCode"];

                            e.Cell.Row.Cells["ChgSpec"].Value = dtSPStock.Rows[0]["Spec"];
                            e.Cell.Row.Cells["ChgMaker"].Value = dtSPStock.Rows[0]["Maker"];
                        }
                        else
                        {

                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region 치공구 그리드

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        private void uGridRepair_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridRepair, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }

                //--체크박스가 수정불가 일시 false로 되돌린다 
                if (strColumn == "Check")
                {
                    if (e.Cell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    {
                        e.Cell.Value = false;
                        return;
                    }
                }
                ////-- 수리취소 체크 박스를 체크 해제하면 편집이미지 X --//
                //if (strColumn == "CancelFlag")
                //{
                //    if (e.Cell.Value.ToString() == "False")
                //    {
                //        e.Cell.Row.RowSelectorAppearance.Image = null;
                //        return;
                //    }
                //}

                #region 수리출고 구분 셀업데이트시 창고 교체량 교체구성품코드 셀의 헤더 색이 필수로 변한다
                if (strColumn == "RepairGIGubunCode")
                {
                    if (e.Cell.Value.ToString() != "")
                    {
                        //-- 헤더 색변화 --//
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Red;
                    }
                    else
                    {
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridRepair.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Black;
                    }
                }
                #endregion

                #region 수량 비교
                //수량을 입력한 후 해당설비구성품의 수량과 비교함 (입력한 수량보다 설비구성품의수량이 크면 안됨)
                if (strColumn.Equals("InputQty")
                    && !e.Cell.Value.ToString().Equals(string.Empty) 
                    && !e.Cell.Value.ToString().Equals("0"))
                {
                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;
                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridRepair.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                            {
                                if (
                                    e.Cell.Row.Cells["CurDurableMatCode"].Value.Equals(this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value))
                                {

                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value);

                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["CurLotNo"].Tag;

                            msg.mfSetMessageBox(MessageBoxType.Error,m_resSys.GetString("SYS_FONTNAME") ,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000617",strLang)
                                                   , msg.GetMessge_Text("M000659",strLang) + strQty + msg.GetMessge_Text("M000010",strLang), Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }

                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000730", "M000903", Infragistics.Win.HAlign.Right);
                            //수량되돌림
                            e.Cell.Value = e.Cell.Tag;
                            //PerFormAction
                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[e.Cell.Row.Index].Cells["InputQty"];
                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                    }
                    else
                        e.Cell.Value = 0;
                }
                #endregion

                #region 교체수량비교

                if (strColumn.Equals("ChgQty"))
                {
                    if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if ((!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) 
                                && Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag)) 
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000730", "M000890", Infragistics.Win.HAlign.Right);

                            //교체하는 금형치공구 수량적용
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag))
                            {
                                e.Cell.Value = 0;//e.Cell.Row.Cells["InputQty"].Tag;
                            }
                            else
                            {
                                e.Cell.Value = 0;// e.Cell.Row.Cells["Qty"].Value;
                            }

                            //PerFormAction
                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[e.Cell.Row.Index].Cells["ChgQty"];
                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        #region 중복구성품재고수량체크

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridRepair.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridRepair.Rows[i].Hidden.Equals(false)
                                && this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(string.Empty)
                                )
                            {
                                if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value) &&
                                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridRepair.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000302",strLang)
                                                   , msg.GetMessge_Text("M000891",strLang) + strQty + msg.GetMessge_Text("M000010",strLang)
                                                   , Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }


                        #endregion
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //수리출고구성품그리드에서 창고 콤보를 선택하기 전 수리출고구분 체크
        private void uGridRepair_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //---창고 컬럼일때 발생--//
                if (e.Cell.Column.Key == "ChgDurableInventoryCode")
                {
                    //설비코드 미선택 시
                    if (this.uComboEquipCode.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000881", "M000721", Infragistics.Win.HAlign.Right);
                        this.uComboEquipCode.DropDown();
                        return;
                    }
                    //--- 수리출고구분이 "" 일경우 메세지 박스 ---//
                    if (e.Cell.Row.Cells["RepairGIGubunCode"].Value.ToString() == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000881", "M000743", Infragistics.Win.HAlign.Right);

                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[e.Cell.Row.Index].Cells["RepairGIGubunCode"];
                        this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //-- 그리드 컬럼중 리스트 컬럼의 아이템을 선택하였을 때 발생함 --//
        private void uGridRepair_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;
                int intSelectRow;
                //공장 정보저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uComboEquipCode.Value == null ? "" : this.uComboEquipCode.Value.ToString();

                //선택한 리스트의 Key값과 Value값 저장(컬럼그리드콤보가 없으면 셀그리드 콤보 정보로
                string strKey = "";
                string strValue = "";

                if (e.Cell.Column.ValueList != null)
                {
                    strKey = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }


                #region 구성품명 의 리스트 선택시 선택한 이름과 코드를 각 셀에 넣는다

                if (strColumn == "CurDurableMatName")
                {

                    //공백일 경우
                    if (strKey.Equals(string.Empty))
                    {
                        //수량,구성품코드 공백처리
                        e.Cell.Row.Cells["InputQty"].Value = 0;
                        e.Cell.Row.Cells["CurDurableMatCode"].Value = "";

                    }
                    else
                    {
                        intSelectRow = e.Cell.Column.ValueList.SelectedItemIndex;

                        //설비구성품정보 BL 호출

                        //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        //QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        //brwChannel.mfCredentials(clsEquipDurableBOM);

                        //DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOM_DurableMatInputQty(strPlantCode, this.uTextEquipCode.Text, strKey, m_resSys.GetString("SYS_LANG"));

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        brwChannel.mfCredentials(clsEquipDurableBOM);

                        DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                        e.Cell.Row.Cells["CurDurableMatCode"].Value = strKey;

                        //LotNo가 없으면 수량을 직접입력할수있고(100개중 50개만 교체대상이 될수도있음) LotNo가 있으면 할수없다.(LotNo가있는 구성품은 무조건1개라서)
                        if (dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                        {
                            //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            this.uGridRepair.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            e.Cell.Row.Cells["InputQty"].Tag = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["CurLotNo"].Value = "";
                        }
                        else
                        {
                            //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridRepair.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            e.Cell.Row.Cells["InputQty"].Tag = 1;
                            e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["CurLotNo"].Value = dtDurableMatInfo.Rows[intSelectRow]["LotNo"];
                        }

                        //단위저장
                        e.Cell.Row.Cells["CurLotNo"].Tag = dtDurableMatInfo.Rows[intSelectRow]["UnitCode"];

                        //Lot정보가 있는 구성품을 선택하였을 경우 리스트내에 똑같은 구성품이 올수없다.
                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString()) &&
                                !dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                !this.uGridRepair.Rows[i].Appearance.BackColor.Equals(Color.Yellow) &&
                                this.uGridRepair.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000068", "M000853", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["InputQty"].Value = 0;
                                e.Cell.Row.Cells["InputQty"].Tag = 0;
                                e.Cell.Row.Cells["CurLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["CurLotNo"].Tag = string.Empty;
                                e.Cell.Row.Cells["CurDurableMatCode"].Value = string.Empty;
                                e.Cell.Row.Cells["CurDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                return;
                            }


                        }

                        //창고 콤보가 선택되어있다면,
                        if (!e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString().Equals(string.Empty))
                        {
                            if (!e.Cell.Row.Cells["CurLotNo"].Value.Equals(e.Cell.Row.Cells["ChgLotNo"].Value))
                            {
                                ChgDurbleMat(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(), e.Cell.Row.Index);
                                e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["Qty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                            else
                            {
                                if (e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty) || Convert.ToInt32(e.Cell.Row.Cells["ChgQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value))
                                    e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                        }

                    }

                }
                #endregion

                #region 교체구성품창고의 구성품코드 리스트를 가져온다
                //선택한 교체구성품창고의 구성품코드리스트를 가져온다.
                if (strColumn.Equals("ChgDurableInventoryCode"))
                {
                    if (!strKey.Equals(string.Empty))
                    {
                        ChgDurbleMat(strPlantCode, strKey, e.Cell.Row.Index);
                    }

                }
                #endregion

                #region 교체구성품코드 선택시 가용량 Lot정보 삽입

                if (strColumn.Equals("ChgDurableMatName"))
                {
                    if (!strKey.Equals(string.Empty))
                    {

                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                        QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                        brwChannel.mfCredentials(clsRepairReq);

                        DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                        if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                            return;
                        

                        //금형치공구정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                        brwChannel.mfCredentials(clsDurableStock);

                        //금형치공구정보 Detail매서드 호출
                        DataTable dtStock = new DataTable();


                        //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                        string strLotChk = "";

                        if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                            strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                        dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                strLotChk, m_resSys.GetString("SYS_LANG"));

                        if (dtStock.Rows.Count > 0)
                        {
                            intSelectRow = e.Cell.ValueList.SelectedItemIndex;

                            ////구성품끼리의 단위가 다를경우 경고
                            //if (!e.Cell.Row.Cells["CurLotNo"].Tag.Equals(dtStock.Rows[intSelectRow]["UnitCode"]))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                       , "확인창", "구성품 단위 확인", "기존구성품의 단위와 교체구성품의 단위가 다릅니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                            //    return;
                            //}
                            //그리드콤보에 선택한 정보를 삽입
                            e.Cell.Row.Cells["ChgLotNo"].Value = dtStock.Rows[intSelectRow]["LotNo"];
                            e.Cell.Row.Cells["Qty"].Value = dtStock.Rows[intSelectRow]["Qty"];
                            e.Cell.Row.Cells["ChgDurableMatCode"].Value = strKey;
                            //e.Cell.Row.Cells["UnitCode"].Value = dtStock.Rows[intSelectRow]["UnitCode"];

                            //Lot정보가 있을경우 교체수량1개 수정불가로 변경
                            if (!dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgQty"].Value = 1;

                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                            }
                            else
                            {
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            }

                            //선택한 정보가 그리드내에 있으면 다시입력
                            for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtStock.Rows[intSelectRow]["LotNo"].ToString()) &&
                                    !dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                    this.uGridRepair.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit &&
                                   !this.uGridRepair.Rows[i].Appearance.BackColor.Equals(Color.Yellow))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000068", "M000853", Infragistics.Win.HAlign.Right);

                                    e.Cell.Row.Cells["Qty"].Value = 0;
                                    e.Cell.Row.Cells["ChgQty"].Value = 0;

                                    e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                    return;
                                }
                            }
                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 버튼이벤트

        //SparePart그리드 행삭제
        private void uButtonDeleteSPRow_Click(object sender, EventArgs e)
        {
            try
            {
                //System ReosurceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridUseSP.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteDurableRow_Click(object sender, EventArgs e)
        {
            try
            {
                //-- 체크 박스에 체크된 행을 숨김 처리한다 --//
                for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridRepair.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //닫기버튼 클릭시 폼이 닫힌다
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //저장을 누를 시 해당 구성품 정보를 저장한다.
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                #region 필수 입력확인 정보확인
                //-------------------------------필수 입력확인 , 정보 확인---------------------------------//
                if (this.uGridUseSP.Rows.Count == 0 && this.uGridRepair.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000882", "M001047", Infragistics.Win.HAlign.Right);

                    return;
                }
                if (this.uTextPlantCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000274", "M000273", Infragistics.Win.HAlign.Right);

                    return;
                }
                if (this.uComboEquipCode.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000699", Infragistics.Win.HAlign.Right);

                    this.uComboEquipCode.DropDown();
                    return;
                }

                if (this.uTextUserID.Text.Equals(string.Empty) || this.uTextUserName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M001170", Infragistics.Win.HAlign.Right);

                    this.uTextUserID.Focus();
                    return;
                }

                #endregion

                if (strKind !=null && strKind.Equals("RE"))
                {
                    mfSaveRE();
                    
                }
                else
                    mfSavePM();               

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #endregion

        #region SparePart구성품

        /// <summary>
        /// 그리드안 설비구성품 콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        private void SearchSPBOM(string strPlantCode, string strEquipCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // 설비 구성품 정보 가지고옴
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);
                DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //--- 해당 설비의 SP 정보에 따라 처리 ---//
                if (dtEquipSPBOM.Rows.Count > 0)
                {
                    if(this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].CellActivation.Equals(Infragistics.Win.UltraWinGrid.Activation.NoEdit))
                        this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                else
                {
                    this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }

                this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();

                string strValue = "SparePartCode,SparePartName,InputQty,UnitName,Spec,Maker";
                string strText = "구성품코드,구성품명,Qty,단위,Spec,Maker";

                grd.mfSetGridColumnValueGridList(this.uGridUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                , "SparePartCode", "SparePartName", dtEquipSPBOM);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 그리드안 창고재고 콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strInventoryCode">창고</param>
        /// <param name="intIndex">줄번호</param>
        private void SearchSPStock(string strPlantCode, string strInventoryCode, int intIndex)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //SP현재고 정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                if (dtChgSP.Rows.Count > 0)
                {
                    string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                    string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                    WinGrid wGrid = new WinGrid();

                    //GridList 그리드에 삽입
                    wGrid.mfSetGridCellValueGridList(this.uGridUseSP, 0, intIndex, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                        , "SparePartCode", "SparePartName", dtChgSP);
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000121", "M001252", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 치공구 구성품

        /// <summary>
        /// 해당 설비의 치공구BOM정보
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        private string SearchDurableBOM(string strPlantCode,string strEquipCode)
        {
            string strRtn = "";
            try
            {
              //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);

                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));



                if (dtDurable.Rows.Count == 0)
                {
                    this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridRepair.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                }
                else
                {
                    this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridRepair.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                }

                //콤보 그리드 리스트 클러어
                this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();
                //콤보그리드 컬럼설정
                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                //--그리드에 추가 --
                WinGrid wGrid = new WinGrid();
                wGrid.mfSetGridColumnValueGridList(this.uGridRepair, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                    , "DurableMatCode", "DurableMatName", dtDurable);

               
                return strRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtn;
            }
            finally
            { }

        }

        /// <summary>
        /// 교체구성품그리드콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInven">창고정보</param>
        /// <param name="e">Infragistics.Win.UltraWinGrid.CellEventArgs</param>
        private void ChgDurbleMat(string strPlantCode, string strInven, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                string strLotChk = "";
                string strEquipCode = this.uComboEquipCode.Value == null ? "" : this.uComboEquipCode.Value.ToString();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (!this.uGridRepair.Rows[intIndex].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    strLotChk = this.uGridRepair.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //금형치공구창고에 해당하는 금형치공구 재고 조회
                DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, strInven,
                                                                                dtEquipPackModel.Rows[0]["PackageB"].ToString(),dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                strLotChk, m_resSys.GetString("SYS_LANG"));

                //금형치공구정보에 검색조건 적용
                WinGrid wGrid = new WinGrid();

                //                this.uGridRepair.Rows[intIndex].Cells["ChgDurableMatName"].Column.Layout.ValueLists.Clear();
                string strDropDownValue = "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName";
                string strDropDownText = "치공구코드,치공구,LOTNO,수량,단위코드,단위";

                //콤보그리드 초기화.
                //e.Cell.Column.Band.Layout.ValueLists.Clear();

                wGrid.mfSetGridCellValueGridList(this.uGridRepair, 0, intIndex, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                   , strDropDownValue, strDropDownText, "DurableMatCode", "DurableMatName", dtStock);

                if (dtStock.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        /// <summary>
        /// 화면초기화
        /// </summary>
        private void InitDisplay()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                this.uDateOut.Value = this.uTextPMPlanDate.Text;
                this.uTextUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextDesc.Clear();
                this.uTextReairGICode.Clear();

                if (this.uGridRepair.Rows.Count > 0)
                {
                    this.uGridRepair.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridRepair.Rows.All);
                    this.uGridRepair.DeleteSelectedRows(false);
                }

                if (this.uGridUseSP.Rows.Count > 0)
                {
                    this.uGridUseSP.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridUseSP.Rows.All);
                    this.uGridUseSP.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 수리에서 넘어온 정보저장
        /// </summary>
        private void mfSaveRE()
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //-------------------헤더 값 저장-----------------//
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uComboEquipCode.Value.ToString();
                string strPMPlanDate = this.uTextPMPlanDate.Text;
                string strPlanYear = strPMPlanDate.Substring(0, 4);
                string strReqDate = Convert.ToDateTime(this.uDateOut.Value).ToString("yyyy-MM-dd");
                string strChargID = this.uTextUserID.Text;
                string strEtcDesc = this.uTextDesc.Text;
                string strLang = m_resSys.GetString("SYS_LANG");

                #region 치공구

                #region Header

                //치공구교체정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                //---헤더값을 넣을 데이터 테이블---//
                DataTable dtDurableRepairH = clsDurableMatRepairH.mfDataSetInfo();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsDurableMatRepairD);

                //--상세정보를 넣을 데이터 테이블--//
                DataTable dtDurableRepairD = clsDurableMatRepairD.mfsetDataInfo();
                //--행삭제된 정보를 넣을 데이터테이블--//
                //DataTable dtDurableRepairDel = clsDurableMatRepairD.mfsetDataInfo();
                //dtDurableRepairD.Columns.Add("UnitCode", typeof(string));
                dtDurableRepairD.Columns.Add("UnitName", typeof(string));
                dtDurableRepairD.Columns.Add("Qty", typeof(string));
                dtDurableRepairD.Columns.Add("ChgDurableMatName", typeof(string));
                dtDurableRepairD.Columns.Add("ItemGubunCode", typeof(string));


                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                #endregion

                if (this.uGridRepair.Rows.Count > 0)
                {
                    #region 치공구 교체 정보 저장

                    #region 상세그리드 저장

                    if (this.uGridRepair.Rows.Count > 0)
                    {
                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.

                            ////조회된 정보에서 수리취소가 false인데 편집이미지가 있으면 없앤다(수리출고구분 수정가능일 시 다른방법을..)
                            //if (this.uGridRepair.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                            //    && Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value) == false && this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                            //{
                            //    this.uGridRepair.Rows[i].RowSelectorAppearance.Image = null;
                            //}

                            if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                if (this.uGridRepair.Rows[i].Hidden == false)
                                {
                                    if (this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() != "")
                                    {
                                        #region 상세 필수입력사항 확인
                                        string strRowNum = this.uGridRepair.Rows[i].RowSelectorNumber.ToString();

                                        if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) && this.uGridRepair.Rows[i].Cells["InputQty"].Value.ToString().Equals("0"))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME") ,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000519",strLang), Infragistics.Win.HAlign.Right);

                                            //PerFormAction
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["InputQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        if (this.uGridRepair.Rows[i].Cells["InputQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridRepair.Rows[i].Cells["InputQty"].Tag != null && Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value) > Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Tag))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000730", strLang)
                                                , strRowNum + msg.GetMessge_Text("M000539",strLang), Infragistics.Win.HAlign.Right);
                                            //PerFormAction
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["InputQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000552",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;

                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Value.ToString() == "" || this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000452",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridRepair.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000453",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && Convert.ToInt32(this.uGridRepair.Rows[i].Cells["Qty"].Value) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000882",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000454",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        #region 구성품 수량체크

                                        //구성품 교체 시
                                        //if (!Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                        //{
                                        //BOM정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                        this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //BOM 정보와 비교하여 정보가 없거나 수량이 부족할 경우 콤보를 다시 뿌려줌
                                            if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                                    , strRowNum + msg.GetMessge_Text("M000497",strLang), Infragistics.Win.HAlign.Right);

                                                #region DropDown

                                                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                                                //--해당설비의 BOM 이없을 경우 리턴 --
                                                if (dtDurable.Rows.Count == 0)
                                                {
                                                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M000715", "M001251", Infragistics.Win.HAlign.Right);

                                                    return;
                                                }
                                                //콤보 그리드 리스트 클러어
                                                this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                                                //콤보그리드 컬럼설정
                                                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                                                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                                                //--그리드에 추가 --
                                                WinGrid wGrid = new WinGrid();
                                                wGrid.mfSetGridColumnValueGridList(this.uGridRepair, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "DurableMatCode", "DurableMatName", dtDurable);
                                                #endregion

                                                this.uGridRepair.Rows[i].Cells["InputQty"].Value = 0;
                                                this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;

                                                this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CurDurableMatName"];
                                                this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }
                                        //Stock의 재고정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //재고정보와 비교하여 정보가 없거나 수량이 부족 할 경우 콤보를 다시뿌려줌
                                        if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                                , strRowNum + msg.GetMessge_Text("M000487",strLang), Infragistics.Win.HAlign.Right);

                                            ChgDurbleMat(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                            this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                            this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                            this.uGridRepair.Rows[i].Cells["Qty"].Value = 0;
                                            this.uGridRepair.Rows[i].Cells["ChgQty"].Value = 0;
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }

                                        //}
                                        //// 수리취소 할 경우
                                        //else
                                        //{
                                        //    DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                        //                                                                this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                        //                                                            this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                        //                                                            this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    //BOM정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "확인창", "수리취소 정보확인", strRowNum + "번째 열의 기존구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //    //재고정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "확인창", "수리취소 정보확인", strRowNum + "번째 열의 교체구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //}

                                        #endregion

                                        #endregion


                                        DataRow drRepairD;
                                        drRepairD = dtDurableRepairD.NewRow();
                                        drRepairD["PlantCode"] = strPlantCode;                                                                  //공장코드
                                        drRepairD["Seq"] = this.uGridRepair.Rows[i].Cells["Seq"].Value.ToString();                           //순번
                                        drRepairD["ItemGubunCode"] = "TL";
                                        drRepairD["RepairGIGubunCode"] = this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Value.ToString();//수리출고구분
                                        drRepairD["CurDurableMatCode"] = this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(); //구성품
                                        drRepairD["CurQty"] = this.uGridRepair.Rows[i].Cells["InputQty"].Value.ToString();                  //기존수량
                                        drRepairD["CurLotNo"] = this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString();                //기존 LotNo
                                        drRepairD["ChgDurableInventoryCode"] = this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString();//교체창고
                                        drRepairD["ChgDurableMatCode"] = this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();//교체구성품
                                        drRepairD["ChgDurableMatName"] = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Text;
                                        drRepairD["ChgQty"] = this.uGridRepair.Rows[i].Cells["ChgQty"].Value.ToString();                //교체수량
                                        drRepairD["Qty"] = this.uGridRepair.Rows[i].Cells["Qty"].Value.ToString();                      //가용수량
                                        drRepairD["ChgLotNo"] = this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString();            //변경된LotNo
                                        drRepairD["UnitCode"] = this.uGridRepair.Rows[i].Cells["UnitCode"].Value.ToString();            //단위
                                        //drRepairD["UnitName"] = this.uGridRepair.Rows[i].Cells["UnitName"].Value.ToString();
                                        drRepairD["EtcDesc"] = this.uGridRepair.Rows[i].Cells["EtcDesc"].Value.ToString();              //특이사항
                                        drRepairD["CancelFlag"] = "False";//this.uGridRepair.Rows[i].Cells["CancelFlag"].Value.ToString().Substring(0, 1);
                                        dtDurableRepairD.Rows.Add(drRepairD);

                                    }

                                }
                            }
                        }
                    }
                    #endregion

                    if (dtDurableRepairD.Rows.Count > 0)
                    {
                        #region 치공구교체헤더저장

                        DataRow drRepairH;
                        drRepairH = dtDurableRepairH.NewRow();
                        drRepairH["PlantCode"] = strPlantCode;
                        drRepairH["EquipCode"] = strEquipCode;


                        drRepairH["RepairReqCode"] = RepairReqCode;
                        drRepairH["GITypeCode"] = "RE";

                        //else
                        //{
                        //    drRepairH["RepairGICode"] = this.uTextReairGICode.Text;
                        //    drRepairH["GITypeCode"] = "PM";
                        //    drRepairH["PMPlanDate"] = strPMPlanDate;
                        //    drRepairH["PlanYear"] = strPlanYear;
                        //}


                        drRepairH["RepairGIReqDate"] = strReqDate;
                        drRepairH["RepairGIReqID"] = strChargID;
                        drRepairH["EtcDesc"] = strEtcDesc;


                        dtDurableRepairH.Rows.Add(drRepairH);

                        #endregion
                        
                    }
                   
                    #endregion
                }

                #endregion

                #region SparePart

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairUseSP), "RepairUseSP");
                QRPEQU.BL.EQUREP.RepairUseSP clsRepairUseSP = new QRPEQU.BL.EQUREP.RepairUseSP();
                brwChannel.mfCredentials(clsRepairUseSP);

                DataTable dtUseSPH = clsRepairUseSP.mfDataSetInfoH();

                DataTable dtUseSPD = clsRepairUseSP.mfDataSetInfoD();

                if (this.uGridUseSP.Rows.Count > 0)
                {

                    #region Detail

                    //  해당설비의 구성품
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChannel.mfCredentials(clsEquipSPBOM);
                    DataTable dtSPBOM = new DataTable();

                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                    QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                    brwChannel.mfCredentials(clsSPStock);

                    DataTable dtStock = new DataTable();

                    for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                    {
                        if (!this.uGridUseSP.Rows[i].Hidden)
                        {
                            //if (Convert.ToBoolean(this.uGridUseSP.Rows[i].GetCellValue("CancelFlag")).Equals(false) && this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null
                            //    && this.uGridUseSP.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                            //{
                            //    this.uGridUseSP.Rows[i].RowSelectorAppearance.Image = null;
                            //}

                            if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                #region 필수 입력 사항 확인
                                int intRowSelect = this.uGridUseSP.Rows[i].RowSelectorNumber;
                                if (this.uGridUseSP.Rows[i].Cells["CurInputQty"].Tag != null
                                    && (this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.ToString().Equals(string.Empty)
                                    || this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.Equals(0)))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000511",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CurInputQty"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000552",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgSparePartName"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000507",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSparePartName"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.Equals(0) || this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000508",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgInputQty"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                #endregion

                                #region 테이블의 수량 확인

                                ////교체등록시 CurSP가 설비구성품,ChgSP가 재고에 정보가 없거나 수량이 부족한 경우 메세지박스
                                //if (!Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                //{
                                //기존구성품교체 시 
                                if (!this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty))
                                {
                                    //SparePartBOM
                                    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) 
                                        < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                    {

                                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000297",strLang)
                                                            , intRowSelect + msg.GetMessge_Text("M000471",strLang), Infragistics.Win.HAlign.Right);

                                        SearchSPBOM(strPlantCode, strEquipCode);
                                        this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value = 0;
                                        this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value = string.Empty;
                                        this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CurSparePartName"];
                                        this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;

                                    }
                                }

                                //SparePartStock
                                dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) 
                                    < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                {

                                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000297",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000471",strLang), Infragistics.Win.HAlign.Right);

                                    SearchSPStock(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), i);
                                    this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value = 0;
                                    this.uGridUseSP.Rows[i].Cells["Qty"].Value = 0;
                                    this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value = string.Empty;
                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSparePartName"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                //}
                                ////수리취소 시 ChgSP가 설비구성품,CurSP가 재고에 정보가 없거나 수량이 부족한 경우 메세지박스
                                //else
                                //{
                                //    //SparePartBOM
                                //    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));



                                //    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                //    {

                                //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                            "확인창", "수리취소 정보확인", intRowSelect + "번째 열의 SparePart가 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                //        this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CancelFlag"];
                                //        return;

                                //    }
                                //    if (!this.uGridUseSP.Rows[i].GetCellValue("CurSparePartCode").ToString().Equals(string.Empty))
                                //    {
                                //        //SparePartStock
                                //        dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                //        if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                //        {

                                //            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                                "확인창", "수리취소 정보확인", intRowSelect + "번째 SparePart가 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                //            this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CancelFlag"];
                                //            return;
                                //        }
                                //    }
                                //}

                                #endregion

                                DataRow drRepairD = dtUseSPD.NewRow();

                                drRepairD["PlantCode"] = strPlantCode;
                                drRepairD["RepairReqCode"] = strRepairReqCode;
                                drRepairD["Seq"] = 0;
                                drRepairD["CancelFlag"] = "F";
                                drRepairD["EquipCode"] = strEquipCode;
                                drRepairD["CurSparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value;
                                drRepairD["CurInputQty"] = this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value;
                                drRepairD["ChgSPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value;
                                drRepairD["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value;
                                drRepairD["ChgInputQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value;
                                drRepairD["UnitCode"] = this.uGridUseSP.Rows[i].GetCellValue("UnitCode");
                                drRepairD["ChgDesc"] = this.uGridUseSP.Rows[i].Cells["ChgDesc"].Value;

                                dtUseSPD.Rows.Add(drRepairD);

                            }
                        }
                    }

                    #endregion

                    if (dtUseSPD.Rows.Count > 0)
                    {
                        #region Header

                        DataRow drUseSPH = dtUseSPH.NewRow();

                        drUseSPH["PlantCode"] = strPlantCode;
                        drUseSPH["RepairReqCode"] = RepairReqCode;
                        drUseSPH["UseSPChgDate"] = strReqDate;
                        drUseSPH["UseSPChgID"] = strChargID;
                        drUseSPH["UseSPEtcDesc"] = strEtcDesc;

                        dtUseSPH.Rows.Add(drUseSPH);

                        #endregion
                    }
                }
                if (dtUseSPD.Rows.Count == 0 && dtDurableRepairD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001032", "M001025", Infragistics.Win.HAlign.Right);

                    return;
                }
                #endregion

                clsDurableMatRepairH.Dispose();
                clsDurableMatRepairD.Dispose();
                clsRepairUseSP.Dispose();
                clsEquipDurableBOM.Dispose();
                clsDurableStock.Dispose();

                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    //수리정보 BL정보 호출
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    brwChannel.mfCredentials(clsRepairReq);

                    string strErrRtn = clsRepairReq.mfSaveRepairReq_ChangePOP(strRepairReqCode, dtDurableRepairH, dtDurableRepairD, 
                                                                            dtUseSPH, dtUseSPD, dtRequest, dtAccept, 
                                                                            m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));



                    ///---Decoding---//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--커서변경--//
                    this.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                        strRepairReqCode = ErrRtn.mfGetReturnValue(0);
                        this.Close();

                    }
                    else
                    {

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// PM에서 넘어온 정보저장
        /// </summary>
        private void mfSavePM()
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //-------------------헤더 값 저장-----------------//
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uComboEquipCode.Value.ToString();
                string strPMPlanDate = this.uTextPMPlanDate.Text;
                string strPlanYear = strPMPlanDate.Substring(0, 4);
                string strReqDate = Convert.ToDateTime(this.uDateOut.Value).ToString("yyyy-MM-dd");
                string strChargID = this.uTextUserID.Text;
                string strEtcDesc = this.uTextDesc.Text;
                string strLang = m_resSys.GetString("SYS_LANG");

                #region 치공구

                //치공구교체정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                //---헤더값을 넣을 데이터 테이블---//
                DataTable dtDurableRepairH = clsDurableMatRepairH.mfDataSetInfo();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsDurableMatRepairD);

                //--상세정보를 넣을 데이터 테이블--//
                DataTable dtDurableRepairD = clsDurableMatRepairD.mfsetDataInfo();
                //--행삭제된 정보를 넣을 데이터테이블--//
                //DataTable dtDurableRepairDel = clsDurableMatRepairD.mfsetDataInfo();
                //dtDurableRepairD.Columns.Add("UnitCode", typeof(string));
                dtDurableRepairD.Columns.Add("UnitName", typeof(string));
                dtDurableRepairD.Columns.Add("Qty", typeof(string));
                dtDurableRepairD.Columns.Add("ChgDurableMatName", typeof(string));
                dtDurableRepairD.Columns.Add("ItemGubunCode", typeof(string));


                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);



                if (this.uGridRepair.Rows.Count > 0)
                {

                    #region 상세그리드 저장

                    if (this.uGridRepair.Rows.Count > 0)
                    {
                        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridRepair.Rows.Count; i++)
                        {
                            //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.

                            ////조회된 정보에서 수리취소가 false인데 편집이미지가 있으면 없앤다(수리출고구분 수정가능일 시 다른방법을..)
                            //if (this.uGridRepair.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                            //    && Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value) == false && this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                            //{
                            //    this.uGridRepair.Rows[i].RowSelectorAppearance.Image = null;
                            //}

                            if (this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                if (this.uGridRepair.Rows[i].Hidden == false)
                                {
                                    if (this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() != "")
                                    {
                                        #region 상세 필수입력사항 확인
                                        string strRowNum = this.uGridRepair.Rows[i].RowSelectorNumber.ToString();

                                        if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) && this.uGridRepair.Rows[i].Cells["InputQty"].Value.ToString().Equals("0"))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000519",strLang), Infragistics.Win.HAlign.Right);

                                            //PerFormAction
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["InputQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        if (this.uGridRepair.Rows[i].Cells["InputQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridRepair.Rows[i].Cells["InputQty"].Tag != null && Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value) > Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Tag))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000539",strLang), Infragistics.Win.HAlign.Right);
                                            //PerFormAction
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["InputQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000552",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;

                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Value.ToString() == "" 
                                            || this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000452",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridRepair.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000453",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridRepair.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && Convert.ToInt32(this.uGridRepair.Rows[i].Cells["Qty"].Value) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000882",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000454",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgQty"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        #region 구성품 수량체크

                                        //구성품 교체 시
                                        //if (!Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                        //{
                                        //BOM정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        if (!this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                        this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //BOM 정보와 비교하여 정보가 없거나 수량이 부족할 경우 콤보를 다시 뿌려줌
                                            if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) 
                                                < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                                    , strRowNum + msg.GetMessge_Text("M000497",strLang), Infragistics.Win.HAlign.Right);

                                                #region DropDown

                                                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                                                //--해당설비의 BOM 이없을 경우 리턴 --
                                                if (dtDurable.Rows.Count == 0)
                                                {
                                                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M000715", "M001251", Infragistics.Win.HAlign.Right);

                                                    return;
                                                }
                                                //콤보 그리드 리스트 클러어
                                                this.uGridRepair.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                                                //콤보그리드 컬럼설정
                                                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                                                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                                                //--그리드에 추가 --
                                                WinGrid wGrid = new WinGrid();
                                                wGrid.mfSetGridColumnValueGridList(this.uGridRepair, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "DurableMatCode", "DurableMatName", dtDurable);
                                                #endregion

                                                this.uGridRepair.Rows[i].Cells["InputQty"].Value = 0;
                                                this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;

                                                this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CurDurableMatName"];
                                                this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }
                                        //Stock의 재고정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //재고정보와 비교하여 정보가 없거나 수량이 부족 할 경우 콤보를 다시뿌려줌
                                        if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) 
                                            < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                                , strRowNum + msg.GetMessge_Text("M000487",strLang), Infragistics.Win.HAlign.Right);

                                            ChgDurbleMat(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                            this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                            this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                            this.uGridRepair.Rows[i].Cells["Qty"].Value = 0;
                                            this.uGridRepair.Rows[i].Cells["ChgQty"].Value = 0;
                                            this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridRepair.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }

                                        //}
                                        //// 수리취소 할 경우
                                        //else
                                        //{
                                        //    DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                        //                                                                this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                        //                                                            this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                        //                                                            this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    //BOM정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "확인창", "수리취소 정보확인", strRowNum + "번째 열의 기존구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //    //재고정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "확인창", "수리취소 정보확인", strRowNum + "번째 열의 교체구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //}

                                        #endregion

                                        #endregion


                                        DataRow drRepairD;
                                        drRepairD = dtDurableRepairD.NewRow();
                                        drRepairD["PlantCode"] = strPlantCode;                                                                  //공장코드
                                        drRepairD["Seq"] = this.uGridRepair.Rows[i].Cells["Seq"].Value.ToString();                           //순번
                                        drRepairD["ItemGubunCode"] = "TL";
                                        drRepairD["RepairGIGubunCode"] = this.uGridRepair.Rows[i].Cells["RepairGIGubunCode"].Value.ToString();//수리출고구분
                                        drRepairD["CurDurableMatCode"] = this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(); //구성품
                                        drRepairD["CurQty"] = this.uGridRepair.Rows[i].Cells["InputQty"].Value.ToString();                  //기존수량
                                        drRepairD["CurLotNo"] = this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString();                //기존 LotNo
                                        drRepairD["ChgDurableInventoryCode"] = this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString();//교체창고
                                        drRepairD["ChgDurableMatCode"] = this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();//교체구성품
                                        drRepairD["ChgDurableMatName"] = this.uGridRepair.Rows[i].Cells["ChgDurableMatName"].Text;
                                        drRepairD["ChgQty"] = this.uGridRepair.Rows[i].Cells["ChgQty"].Value.ToString();                //교체수량
                                        drRepairD["Qty"] = this.uGridRepair.Rows[i].Cells["Qty"].Value.ToString();                      //가용수량
                                        drRepairD["ChgLotNo"] = this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString();            //변경된LotNo
                                        drRepairD["UnitCode"] = this.uGridRepair.Rows[i].Cells["UnitCode"].Value.ToString();            //단위
                                        //drRepairD["UnitName"] = this.uGridRepair.Rows[i].Cells["UnitName"].Value.ToString();
                                        drRepairD["EtcDesc"] = this.uGridRepair.Rows[i].Cells["EtcDesc"].Value.ToString();              //특이사항
                                        drRepairD["CancelFlag"] = "False";//this.uGridRepair.Rows[i].Cells["CancelFlag"].Value.ToString().Substring(0, 1);
                                        dtDurableRepairD.Rows.Add(drRepairD);

                                    }

                                }
                            }
                        }
                    }
                    #endregion

                    if (dtDurableRepairD.Rows.Count > 0)
                    {
                        #region 치공구교체헤더저장

                        DataRow drRepairH;
                        drRepairH = dtDurableRepairH.NewRow();
                        drRepairH["PlantCode"] = strPlantCode;
                        drRepairH["EquipCode"] = strEquipCode;
                        drRepairH["RepairGICode"] = this.uTextReairGICode.Text;
                        drRepairH["GITypeCode"] = "PM";
                        drRepairH["PMPlanDate"] = strPMPlanDate;
                        drRepairH["PlanYear"] = strPlanYear;
                        drRepairH["RepairGIReqDate"] = strReqDate;
                        drRepairH["RepairGIReqID"] = strChargID;
                        drRepairH["EtcDesc"] = strEtcDesc;


                        dtDurableRepairH.Rows.Add(drRepairH);

                        #endregion

                    }

                }

                #endregion

                #region SparePart

                #region 컬럼설정

                //정비결과정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMUseSP), "PMUseSP");
                QRPEQU.BL.EQUMGM.PMUseSP clsPMUseSP = new QRPEQU.BL.EQUMGM.PMUseSP();
                brwChannel.mfCredentials(clsPMUseSP);

                //컬럼셋
                DataTable dtPMUseSPH = clsPMUseSP.mfSetHeaderDataInfo();
                DataTable dtPMUseSP = clsPMUseSP.mfSetDataInfo();


                //SP현재고 정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //SP현재고 컬럼정보 저장
                DataTable dtSPStock_Minus = clsSPStock.mfSetDatainfo();
                DataTable dtSPStock_Plus = clsSPStock.mfSetDatainfo();

                //자재출고이력정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                //컬럼정보가져오기
                DataTable dtSPStockMoveHist_Minus = clsSPStockMoveHist.mfSetDatainfo();
                DataTable dtSPStockMoveHist_Plus = clsSPStockMoveHist.mfSetDatainfo();

                //설비구성품정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);
                //설비구성품 컬럼정보가져오기
                DataTable dtEquipSPBOM = clsSPTransferD.mfSetDatainfo();
                //DataTable dtEquipSPBOM_Cancel = clsSPTransferD.mfSetDatainfo();

                DataRow drMinus;
                DataRow drPlus;
                DataRow drBOM;

                #endregion

                if (this.uGridUseSP.Rows.Count > 0)
                {

                    #region 상세정보 저장(SparePart사용정보)

                    //  해당설비의 구성품
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChannel.mfCredentials(clsEquipSPBOM);
                    DataTable dtSPBOM = new DataTable();

                    DataTable dtStock = new DataTable();

                    //----------------상세 저장----------------//


                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                    {

                        ////기존정보에서 수리취소여부가 False인데 편집이미지가 있는 경우 이미지를 지운다.
                        //if (this.uGridUseSP.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                        //    && Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(false)
                        //    && this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null)
                        //{
                        //    this.uGridUseSP.Rows[i].RowSelectorAppearance.Image = null;
                        //}

                        if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridUseSP.Rows[i].Hidden.Equals(false))
                            {
                                #region 필수 입력 사항 확인
                                int intRowSelect = this.uGridUseSP.Rows[i].RowSelectorNumber;
                                if (this.uGridUseSP.Rows[i].Cells["CurInputQty"].Tag != null
                                    && (this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.ToString().Equals(string.Empty)
                                    || this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.Equals(0)))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000511",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CurInputQty"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000552",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgSparePartName"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000507",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSparePartName"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.Equals(0) || this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001226",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000508",strLang), Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgInputQty"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                #endregion

                                #region 테이블의 수량 확인

                                ////교체등록시 CurSP가 설비구성품,ChgSP가 재고에 정보가 없거나 수량이 부족한 경우 메세지박스
                                //if (!Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                //{
                                //기존구성품교체 시 
                                if (!this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty))
                                {
                                    //SparePartBOM
                                    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                    {

                                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000297",strLang)
                                                            , intRowSelect + msg.GetMessge_Text("M000471",strLang), Infragistics.Win.HAlign.Right);

                                        SearchSPBOM(strPlantCode, strEquipCode);
                                        this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value = 0;
                                        this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value = string.Empty;
                                        this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CurSparePartName"];
                                        this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;

                                    }
                                }

                                //SparePartStock
                                dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) 
                                    < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                {

                                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000297",strLang)
                                                        , intRowSelect + msg.GetMessge_Text("M000471",strLang), Infragistics.Win.HAlign.Right);

                                    SearchSPStock(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), i);
                                    this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value = 0;
                                    this.uGridUseSP.Rows[i].Cells["Qty"].Value = 0;
                                    this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value = string.Empty;
                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSparePartName"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                //}
                                ////수리취소 시 ChgSP가 설비구성품,CurSP가 재고에 정보가 없거나 수량이 부족한 경우 메세지박스
                                //else
                                //{
                                //    //SparePartBOM
                                //    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));



                                //    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                //    {

                                //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                            "확인창", "수리취소 정보확인", intRowSelect + "번째 열의 SparePart가 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                //        this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CancelFlag"];
                                //        return;

                                //    }
                                //    if (!this.uGridUseSP.Rows[i].GetCellValue("CurSparePartCode").ToString().Equals(string.Empty))
                                //    {
                                //        //SparePartStock
                                //        dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                //        if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                //        {

                                //            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                                "확인창", "수리취소 정보확인", intRowSelect + "번째 SparePart가 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                //            this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CancelFlag"];
                                //            return;
                                //        }
                                //    }
                                //}

                                #endregion



                                //UseSP 테이블
                                DataRow drPMUseSP;
                                drPMUseSP = dtPMUseSP.NewRow();
                                drPMUseSP["Seq"] = this.uGridUseSP.Rows[i].Cells["Seq"].Value.ToString();
                                drPMUseSP["CurSparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                drPMUseSP["CurInputQty"] = this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.ToString();
                                drPMUseSP["ChgSPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                drPMUseSP["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                drPMUseSP["ChgInputQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                drPMUseSP["ChgDesc"] = this.uGridUseSP.Rows[i].Cells["ChgDesc"].Value.ToString();
                                drPMUseSP["CancelFlag"] = "False";//this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value.ToString();
                                dtPMUseSP.Rows.Add(drPMUseSP);

                                #region 창고재고에서 투입한 SparePart에 대한 정보 ,교체처리 : 취소정보는 제외

                                // 일반 저장시 창고에서 (-) 처리, 취소시 창고에서(+) 처리함.
                                if (this.uGridUseSP.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit) // 기존 저장된 데이터가 아닌것
                                {
                                    //EQUSPStock
                                    drMinus = dtSPStock_Minus.NewRow();

                                    drMinus["PlantCode"] = strPlantCode;
                                    drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    drMinus["Qty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStock_Minus.Rows.Add(drMinus);

                                    //----------------------------------------------------------------------------------------//
                                    //EQUSPStockMoveHist(재고이력 테이블)
                                    drMinus = dtSPStockMoveHist_Minus.NewRow();

                                    drMinus["MoveGubunCode"] = "M03";       //자재출고일 경우는 "M03"
                                    drMinus["DocCode"] = "";
                                    drMinus["MoveDate"] = strReqDate;
                                    drMinus["MoveChargeID"] = strChargID;
                                    drMinus["PlantCode"] = strPlantCode;
                                    drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    drMinus["EquipCode"] = strEquipCode;
                                    drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    drMinus["MoveQty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStockMoveHist_Minus.Rows.Add(drMinus);

                                    //----------------------------------------------------------------------------------------//
                                    //MASEquipSPBOM 테이블 : 교체처리 : 취소정보는 제외
                                    drBOM = dtEquipSPBOM.NewRow();
                                    drBOM["PlantCode"] = strPlantCode;
                                    drBOM["TransferSPCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();        //새로운 SP
                                    drBOM["TransferQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();

                                    drBOM["ChgEquipCode"] = strEquipCode;
                                    drBOM["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(); // 기존에 있는 SP
                                    drBOM["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                    dtEquipSPBOM.Rows.Add(drBOM);


                                }
                                else
                                {
                                    //if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true)) // 취소여부 체크한 것만
                                    //{
                                    //    //EQUSPStock
                                    //    drPlus = dtSPStock_Plus.NewRow();

                                    //    drPlus["PlantCode"] = strPlantCode;
                                    //    drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    //    drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    //    drPlus["Qty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    //    drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                    //    dtSPStock_Plus.Rows.Add(drPlus);

                                    //    //----------------------------------------------------------------------------------------//
                                    //    //EQUSPStockMoveHist(재고이력 테이블)
                                    //    drPlus = dtSPStockMoveHist_Plus.NewRow();

                                    //    drPlus["MoveGubunCode"] = "M04";       //자재출고일 경우는 "M03"
                                    //    drPlus["DocCode"] = "";
                                    //    drPlus["MoveDate"] = strReqDate;
                                    //    drPlus["MoveChargeID"] = strChargID;
                                    //    drPlus["PlantCode"] = strPlantCode;
                                    //    drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    //    drPlus["EquipCode"] = strEquipCode;
                                    //    drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    //    drPlus["MoveQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    //    drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                    //    dtSPStockMoveHist_Plus.Rows.Add(drPlus);

                                    //    //----------------------------------------------------------------------------------------//
                                    //    //MASEquipSPBOM 테이블 : 교체처리 : 취소정보는 제외
                                    //    drBOM = dtEquipSPBOM.NewRow();
                                    //    drBOM["PlantCode"] = strPlantCode;
                                    //    drBOM["TransferSPCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();        //새로운 SP
                                    //    drBOM["TransferQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();

                                    //    drBOM["ChgEquipCode"] = strEquipCode;
                                    //    drBOM["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(); // 기존에 있는 SP
                                    //    drBOM["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                    //    dtEquipSPBOM.Rows.Add(drBOM);

                                    //}
                                }
                                #endregion

                                #region 설비에 투입되어있는(SPBOM) SparePart에 대한 정보
                                if (this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString() != "")
                                {
                                    if (this.uGridUseSP.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit) // 기존 저장된 데이터가 아닌것
                                    {
                                        //EQUSPStock
                                        drPlus = dtSPStock_Plus.NewRow();

                                        drPlus["PlantCode"] = strPlantCode;
                                        drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        drPlus["Qty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                        dtSPStock_Plus.Rows.Add(drPlus);

                                        //----------------------------------------------------------------------------------------//
                                        //EQUSPStockMoveHist(재고이력 테이블)
                                        drPlus = dtSPStockMoveHist_Plus.NewRow();

                                        drPlus["MoveGubunCode"] = "M04";       //자재반납일 경우는 "M04"
                                        drPlus["DocCode"] = "";
                                        drPlus["MoveDate"] = strReqDate;
                                        drPlus["MoveChargeID"] = strChargID;
                                        drPlus["PlantCode"] = strPlantCode;
                                        drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        drPlus["EquipCode"] = strEquipCode;
                                        drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        drPlus["MoveQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                        dtSPStockMoveHist_Plus.Rows.Add(drPlus);


                                    }
                                    else
                                    {
                                        //if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true)) // 취소여부 체크한 것만
                                        //{
                                        //    //EQUSPStock
                                        //    drMinus = dtSPStock_Minus.NewRow();

                                        //    drMinus["PlantCode"] = strPlantCode;
                                        //    drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        //    drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        //    drMinus["Qty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        //    drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                        //    dtSPStock_Minus.Rows.Add(drMinus);

                                        //    //----------------------------------------------------------------------------------------//
                                        //    //EQUSPStockMoveHist(재고이력 테이블)
                                        //    drMinus = dtSPStockMoveHist_Minus.NewRow();

                                        //    drMinus["MoveGubunCode"] = "M03";       //자재반납일 경우는 "M04"
                                        //    drMinus["DocCode"] = "";
                                        //    drMinus["MoveDate"] = strReqDate;
                                        //    drMinus["MoveChargeID"] = strChargID;
                                        //    drMinus["PlantCode"] = strPlantCode;
                                        //    drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        //    drMinus["EquipCode"] = strEquipCode;
                                        //    drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        //    drMinus["MoveQty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        //    drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                        //    dtSPStockMoveHist_Minus.Rows.Add(drMinus);

                                        //}
                                    }
                                }
                                #endregion

                            }

                        }

                    }
                    #endregion

                    if (dtPMUseSP.Rows.Count > 0)
                    {
                        #region 헤더저장
                        //-----------------헤더 값저장-----------------//

                        DataRow drH = dtPMUseSPH.NewRow();
                        drH["PlantCode"] = strPlantCode;  //공장
                        drH["PlanYear"] = strPlanYear;   //계획년도
                        drH["EquipCode"] = strEquipCode;  //설비코드
                        drH["PMPlanDate"] = strPMPlanDate; //점검일
                        drH["PMUseDate"] = strReqDate;  //사용일
                        drH["SPUseChargeID"] = strChargID;//사용담당자
                        drH["SPUseEtcDesc"] = strEtcDesc;   //비고
                        dtPMUseSPH.Rows.Add(drH);

                        #endregion
                    }



                }

                #endregion

                if (dtPMUseSP.Rows.Count == 0 && dtDurableRepairD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001053", "M001025", Infragistics.Win.HAlign.Right);

                    return;
                }

                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsDurableMatRepairH.mfSaveDurableMatRepairH_POP(dtDurableRepairH, dtDurableRepairD,
                                                                                        dtPMUseSPH, dtPMUseSP,
                                                                                        dtSPStock_Minus, dtSPStock_Plus,
                                                                                        dtSPStockMoveHist_Minus, dtSPStockMoveHist_Plus,
                                                                                        dtEquipSPBOM,
                                                                                        m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    ///---Decoding---//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--커서변경--//
                    this.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);


                        this.uComboEquipCode.Value = "";

                    }
                    else
                    {
                        string strMeg = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMeg = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMeg = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMeg,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


       






    }
}
