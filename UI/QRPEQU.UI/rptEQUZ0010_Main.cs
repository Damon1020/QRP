﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//using추가
using System.Data;
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Threading;
using System.Resources;
using System.EnterpriseServices;


namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_Main.
    /// </summary>
    public partial class rptEQUZ0010_Main : DataDynamics.ActiveReports.ActiveReport
    {

        public rptEQUZ0010_Main()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        public rptEQUZ0010_Main(DataTable dtSetup, DataTable dtSetupCondition, DataTable dtCertiItem,DataTable dtMainSubEquip)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            int intRow = dtMainSubEquip.Rows.Count;
            subReportEquip.Report = new rptEQUZ0010_S04(dtMainSubEquip);
            subReportState.Report = new rptEQUZ0010_S01(dtSetup, intRow);
            subReportJobPK.Report = new rptEQUZ0010_S02(dtSetupCondition, intRow);
            subReportCheck.Report = new rptEQUZ0010_S03(dtCertiItem, intRow);
        }

        private void groupHeader_Format(object sender, EventArgs e)
        {
            try
            {
                #region SubReport
                //QRPGlobal SysRes = new QRPGlobal();
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //QRPBrowser brwChannel = new QRPBrowser();

                //string strPlantCode = textPlantCode.Text;
                //string strDocCode = textDocCode.Text;
                //string strLang = m_resSys.GetString("SYS_LANG");

                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipSetup), "EQUEquipSetup");
                //QRPEQU.BL.EQUCCS.EQUEquipSetup clsEQUEquipSetup = new QRPEQU.BL.EQUCCS.EQUEquipSetup();
                //brwChannel.mfCredentials(clsEQUEquipSetup);
                //DataTable dtEQUEquipSetup = clsEQUEquipSetup.mfReadEquipSetup(strPlantCode, strDocCode, strLang);

                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupCondition), "EQUSetupCondition");
                //QRPEQU.BL.EQUCCS.EQUSetupCondition clsEQUSetupCondition = new QRPEQU.BL.EQUCCS.EQUSetupCondition();
                //brwChannel.mfCredentials(clsEQUSetupCondition);
                //DataTable dtEQUSetupCondition = clsEQUSetupCondition.mfReadSetupCondition(strPlantCode, strDocCode, strLang);

                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItem), "EQUEquipCertiItem");
                //QRPEQU.BL.EQUCCS.EQUEquipCertiItem clsEQUEquipCertiItem = new QRPEQU.BL.EQUCCS.EQUEquipCertiItem();
                //brwChannel.mfCredentials(clsEQUEquipCertiItem);
                //DataTable dtEQUEquipCertiItem = clsEQUEquipCertiItem.mfReadCertiItem(strPlantCode, strDocCode, strLang);

                //subReportState.Report = new rptEQUZ0010_S01(dtEQUEquipSetup);
                //subReportJobPK.Report = new rptEQUZ0010_S02(dtEQUSetupCondition);
                //subReportCheck.Report = new rptEQUZ0010_S03(dtEQUEquipCertiItem);
                #endregion
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
    }
}
