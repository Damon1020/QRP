﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0022_P3_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0022_P3_S));
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            this.uGroupEquip = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextStopTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStopTypeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStopType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUser = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUser = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBeforeProduct = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextProductionB = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageBB = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageB = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageTypeB = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductionB = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackageB = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackageTypeB = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupAfterProduct = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSeq = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEMC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerial = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRecipe = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductionA = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageAA = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageTypeA = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageA = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelComment = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEMC = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerial = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRecipe = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProductionA = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackageTypeA = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackageA = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupRecipe = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridRecipe = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupDurable = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDurableMat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonCancel = new Infragistics.Win.Misc.UltraButton();
            this.uButtonChgCancel = new Infragistics.Win.Misc.UltraButton();
            this.uGroupDurableLot = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDurableLot = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRows = new Infragistics.Win.Misc.UltraButton();
            this.uLabelNoticeTool = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupEquip)).BeginInit();
            this.uGroupEquip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStopTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStopTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBeforeProduct)).BeginInit();
            this.uGroupBeforeProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductionB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageBB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageTypeB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupAfterProduct)).BeginInit();
            this.uGroupAfterProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRecipe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductionA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageAA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageTypeA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupRecipe)).BeginInit();
            this.uGroupRecipe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRecipe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurable)).BeginInit();
            this.uGroupDurable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurableLot)).BeginInit();
            this.uGroupDurableLot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLot)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupEquip
            // 
            this.uGroupEquip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupEquip.Controls.Add(this.uTextStopTypeName);
            this.uGroupEquip.Controls.Add(this.uTextStopTypeCode);
            this.uGroupEquip.Controls.Add(this.uLabelStopType);
            this.uGroupEquip.Controls.Add(this.uTextUserName);
            this.uGroupEquip.Controls.Add(this.uTextUser);
            this.uGroupEquip.Controls.Add(this.uLabelUser);
            this.uGroupEquip.Location = new System.Drawing.Point(0, 40);
            this.uGroupEquip.Name = "uGroupEquip";
            this.uGroupEquip.Size = new System.Drawing.Size(940, 60);
            this.uGroupEquip.TabIndex = 1;
            // 
            // uTextStopTypeName
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStopTypeName.Appearance = appearance26;
            this.uTextStopTypeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStopTypeName.Location = new System.Drawing.Point(428, 28);
            this.uTextStopTypeName.MaxLength = 20;
            this.uTextStopTypeName.Name = "uTextStopTypeName";
            this.uTextStopTypeName.ReadOnly = true;
            this.uTextStopTypeName.Size = new System.Drawing.Size(140, 21);
            this.uTextStopTypeName.TabIndex = 1;
            // 
            // uTextStopTypeCode
            // 
            appearance36.BackColor = System.Drawing.Color.White;
            this.uTextStopTypeCode.Appearance = appearance36;
            this.uTextStopTypeCode.BackColor = System.Drawing.Color.White;
            appearance25.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance25.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance25;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextStopTypeCode.ButtonsRight.Add(editorButton1);
            this.uTextStopTypeCode.Location = new System.Drawing.Point(344, 28);
            this.uTextStopTypeCode.MaxLength = 20;
            this.uTextStopTypeCode.Name = "uTextStopTypeCode";
            this.uTextStopTypeCode.ReadOnly = true;
            this.uTextStopTypeCode.Size = new System.Drawing.Size(80, 21);
            this.uTextStopTypeCode.TabIndex = 2;
            this.uTextStopTypeCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextStopTypeCode_EditorButtonClick);
            // 
            // uLabelStopType
            // 
            this.uLabelStopType.Location = new System.Drawing.Point(240, 28);
            this.uLabelStopType.Name = "uLabelStopType";
            this.uLabelStopType.Size = new System.Drawing.Size(100, 20);
            this.uLabelStopType.TabIndex = 0;
            this.uLabelStopType.Text = "ultraLabel1";
            // 
            // uTextUserName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance37;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(724, 28);
            this.uTextUserName.MaxLength = 20;
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(108, 21);
            this.uTextUserName.TabIndex = 1;
            this.uTextUserName.Visible = false;
            // 
            // uTextUser
            // 
            appearance39.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance39.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance39;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUser.ButtonsRight.Add(editorButton2);
            this.uTextUser.Location = new System.Drawing.Point(116, 28);
            this.uTextUser.MaxLength = 20;
            this.uTextUser.Name = "uTextUser";
            this.uTextUser.Size = new System.Drawing.Size(108, 21);
            this.uTextUser.TabIndex = 1;
            this.uTextUser.ValueChanged += new System.EventHandler(this.uTextUser_ValueChanged);
            this.uTextUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUser_KeyDown);
            this.uTextUser.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUser_EditorButtonClick);
            // 
            // uLabelUser
            // 
            this.uLabelUser.Location = new System.Drawing.Point(12, 28);
            this.uLabelUser.Name = "uLabelUser";
            this.uLabelUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelUser.TabIndex = 0;
            this.uLabelUser.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(944, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBeforeProduct
            // 
            this.uGroupBeforeProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBeforeProduct.Controls.Add(this.uTextProductionB);
            this.uGroupBeforeProduct.Controls.Add(this.uTextPackageBB);
            this.uGroupBeforeProduct.Controls.Add(this.uTextPackageB);
            this.uGroupBeforeProduct.Controls.Add(this.uTextPackageTypeB);
            this.uGroupBeforeProduct.Controls.Add(this.uLabelProductionB);
            this.uGroupBeforeProduct.Controls.Add(this.uLabelPackageB);
            this.uGroupBeforeProduct.Controls.Add(this.uLabelPackageTypeB);
            this.uGroupBeforeProduct.Location = new System.Drawing.Point(0, 100);
            this.uGroupBeforeProduct.Name = "uGroupBeforeProduct";
            this.uGroupBeforeProduct.Size = new System.Drawing.Size(940, 80);
            this.uGroupBeforeProduct.TabIndex = 1;
            // 
            // uTextProductionB
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductionB.Appearance = appearance32;
            this.uTextProductionB.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductionB.Location = new System.Drawing.Point(116, 52);
            this.uTextProductionB.MaxLength = 50;
            this.uTextProductionB.Name = "uTextProductionB";
            this.uTextProductionB.ReadOnly = true;
            this.uTextProductionB.Size = new System.Drawing.Size(108, 21);
            this.uTextProductionB.TabIndex = 1;
            // 
            // uTextPackageBB
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageBB.Appearance = appearance29;
            this.uTextPackageBB.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageBB.Location = new System.Drawing.Point(560, 28);
            this.uTextPackageBB.MaxLength = 50;
            this.uTextPackageBB.Name = "uTextPackageBB";
            this.uTextPackageBB.ReadOnly = true;
            this.uTextPackageBB.Size = new System.Drawing.Size(132, 21);
            this.uTextPackageBB.TabIndex = 1;
            // 
            // uTextPackageB
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageB.Appearance = appearance30;
            this.uTextPackageB.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageB.Location = new System.Drawing.Point(344, 28);
            this.uTextPackageB.MaxLength = 50;
            this.uTextPackageB.Name = "uTextPackageB";
            this.uTextPackageB.ReadOnly = true;
            this.uTextPackageB.Size = new System.Drawing.Size(212, 21);
            this.uTextPackageB.TabIndex = 1;
            // 
            // uTextPackageTypeB
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageTypeB.Appearance = appearance31;
            this.uTextPackageTypeB.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageTypeB.Location = new System.Drawing.Point(116, 28);
            this.uTextPackageTypeB.MaxLength = 50;
            this.uTextPackageTypeB.Name = "uTextPackageTypeB";
            this.uTextPackageTypeB.ReadOnly = true;
            this.uTextPackageTypeB.Size = new System.Drawing.Size(108, 21);
            this.uTextPackageTypeB.TabIndex = 1;
            // 
            // uLabelProductionB
            // 
            this.uLabelProductionB.Location = new System.Drawing.Point(12, 52);
            this.uLabelProductionB.Name = "uLabelProductionB";
            this.uLabelProductionB.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductionB.TabIndex = 0;
            this.uLabelProductionB.Text = "ultraLabel1";
            // 
            // uLabelPackageB
            // 
            this.uLabelPackageB.Location = new System.Drawing.Point(240, 28);
            this.uLabelPackageB.Name = "uLabelPackageB";
            this.uLabelPackageB.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackageB.TabIndex = 0;
            this.uLabelPackageB.Text = "ultraLabel1";
            // 
            // uLabelPackageTypeB
            // 
            this.uLabelPackageTypeB.Location = new System.Drawing.Point(12, 28);
            this.uLabelPackageTypeB.Name = "uLabelPackageTypeB";
            this.uLabelPackageTypeB.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackageTypeB.TabIndex = 0;
            this.uLabelPackageTypeB.Text = "ultraLabel1";
            // 
            // uGroupAfterProduct
            // 
            this.uGroupAfterProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupAfterProduct.Controls.Add(this.uTextSeq);
            this.uGroupAfterProduct.Controls.Add(this.uTextComment);
            this.uGroupAfterProduct.Controls.Add(this.uTextEMC);
            this.uGroupAfterProduct.Controls.Add(this.uTextSerial);
            this.uGroupAfterProduct.Controls.Add(this.uTextRecipe);
            this.uGroupAfterProduct.Controls.Add(this.uTextProductionA);
            this.uGroupAfterProduct.Controls.Add(this.uTextPackageAA);
            this.uGroupAfterProduct.Controls.Add(this.uTextPackageTypeA);
            this.uGroupAfterProduct.Controls.Add(this.uTextPackageA);
            this.uGroupAfterProduct.Controls.Add(this.uLabelComment);
            this.uGroupAfterProduct.Controls.Add(this.uLabelEMC);
            this.uGroupAfterProduct.Controls.Add(this.uLabelSerial);
            this.uGroupAfterProduct.Controls.Add(this.uLabelSeq);
            this.uGroupAfterProduct.Controls.Add(this.uLabelRecipe);
            this.uGroupAfterProduct.Controls.Add(this.uLabelProductionA);
            this.uGroupAfterProduct.Controls.Add(this.uLabelPackageTypeA);
            this.uGroupAfterProduct.Controls.Add(this.uLabelPackageA);
            this.uGroupAfterProduct.Location = new System.Drawing.Point(0, 184);
            this.uGroupAfterProduct.Name = "uGroupAfterProduct";
            this.uGroupAfterProduct.Size = new System.Drawing.Size(940, 148);
            this.uGroupAfterProduct.TabIndex = 1;
            // 
            // uTextSeq
            // 
            this.uTextSeq.Location = new System.Drawing.Point(460, 76);
            this.uTextSeq.MinValue = 0;
            this.uTextSeq.Name = "uTextSeq";
            this.uTextSeq.PromptChar = ' ';
            this.uTextSeq.Size = new System.Drawing.Size(232, 21);
            this.uTextSeq.TabIndex = 6;
            this.uTextSeq.Value = 1;
            // 
            // uTextComment
            // 
            this.uTextComment.Location = new System.Drawing.Point(116, 124);
            this.uTextComment.MaxLength = 50;
            this.uTextComment.Name = "uTextComment";
            this.uTextComment.Size = new System.Drawing.Size(576, 21);
            this.uTextComment.TabIndex = 5;
            // 
            // uTextEMC
            // 
            this.uTextEMC.Location = new System.Drawing.Point(116, 100);
            this.uTextEMC.MaxLength = 50;
            this.uTextEMC.Name = "uTextEMC";
            this.uTextEMC.Size = new System.Drawing.Size(224, 21);
            this.uTextEMC.TabIndex = 3;
            // 
            // uTextSerial
            // 
            this.uTextSerial.Location = new System.Drawing.Point(460, 100);
            this.uTextSerial.MaxLength = 50;
            this.uTextSerial.Name = "uTextSerial";
            this.uTextSerial.Size = new System.Drawing.Size(232, 21);
            this.uTextSerial.TabIndex = 4;
            // 
            // uTextRecipe
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRecipe.Appearance = appearance35;
            this.uTextRecipe.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRecipe.Location = new System.Drawing.Point(116, 76);
            this.uTextRecipe.MaxLength = 50;
            this.uTextRecipe.Name = "uTextRecipe";
            this.uTextRecipe.ReadOnly = true;
            this.uTextRecipe.Size = new System.Drawing.Size(224, 21);
            this.uTextRecipe.TabIndex = 1;
            // 
            // uTextProductionA
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductionA.Appearance = appearance34;
            this.uTextProductionA.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductionA.Location = new System.Drawing.Point(116, 52);
            this.uTextProductionA.MaxLength = 50;
            this.uTextProductionA.Name = "uTextProductionA";
            this.uTextProductionA.ReadOnly = true;
            this.uTextProductionA.Size = new System.Drawing.Size(108, 21);
            this.uTextProductionA.TabIndex = 1;
            // 
            // uTextPackageAA
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageAA.Appearance = appearance28;
            this.uTextPackageAA.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageAA.Location = new System.Drawing.Point(560, 28);
            this.uTextPackageAA.MaxLength = 50;
            this.uTextPackageAA.Name = "uTextPackageAA";
            this.uTextPackageAA.ReadOnly = true;
            this.uTextPackageAA.Size = new System.Drawing.Size(132, 21);
            this.uTextPackageAA.TabIndex = 1;
            // 
            // uTextPackageTypeA
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageTypeA.Appearance = appearance33;
            this.uTextPackageTypeA.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageTypeA.Location = new System.Drawing.Point(116, 28);
            this.uTextPackageTypeA.MaxLength = 50;
            this.uTextPackageTypeA.Name = "uTextPackageTypeA";
            this.uTextPackageTypeA.ReadOnly = true;
            this.uTextPackageTypeA.Size = new System.Drawing.Size(108, 21);
            this.uTextPackageTypeA.TabIndex = 1;
            // 
            // uTextPackageA
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageA.Appearance = appearance27;
            this.uTextPackageA.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageA.Location = new System.Drawing.Point(344, 28);
            this.uTextPackageA.MaxLength = 50;
            this.uTextPackageA.Name = "uTextPackageA";
            this.uTextPackageA.ReadOnly = true;
            this.uTextPackageA.Size = new System.Drawing.Size(212, 21);
            this.uTextPackageA.TabIndex = 1;
            // 
            // uLabelComment
            // 
            this.uLabelComment.Location = new System.Drawing.Point(12, 124);
            this.uLabelComment.Name = "uLabelComment";
            this.uLabelComment.Size = new System.Drawing.Size(100, 20);
            this.uLabelComment.TabIndex = 0;
            this.uLabelComment.Text = "ultraLabel1";
            // 
            // uLabelEMC
            // 
            this.uLabelEMC.Location = new System.Drawing.Point(12, 100);
            this.uLabelEMC.Name = "uLabelEMC";
            this.uLabelEMC.Size = new System.Drawing.Size(100, 20);
            this.uLabelEMC.TabIndex = 0;
            this.uLabelEMC.Text = "ultraLabel1";
            // 
            // uLabelSerial
            // 
            this.uLabelSerial.Location = new System.Drawing.Point(356, 100);
            this.uLabelSerial.Name = "uLabelSerial";
            this.uLabelSerial.Size = new System.Drawing.Size(100, 20);
            this.uLabelSerial.TabIndex = 0;
            this.uLabelSerial.Text = "ultraLabel1";
            // 
            // uLabelSeq
            // 
            this.uLabelSeq.Location = new System.Drawing.Point(356, 76);
            this.uLabelSeq.Name = "uLabelSeq";
            this.uLabelSeq.Size = new System.Drawing.Size(100, 20);
            this.uLabelSeq.TabIndex = 0;
            this.uLabelSeq.Text = "ultraLabel1";
            // 
            // uLabelRecipe
            // 
            this.uLabelRecipe.Location = new System.Drawing.Point(12, 76);
            this.uLabelRecipe.Name = "uLabelRecipe";
            this.uLabelRecipe.Size = new System.Drawing.Size(100, 20);
            this.uLabelRecipe.TabIndex = 0;
            this.uLabelRecipe.Text = "ultraLabel1";
            // 
            // uLabelProductionA
            // 
            this.uLabelProductionA.Location = new System.Drawing.Point(12, 52);
            this.uLabelProductionA.Name = "uLabelProductionA";
            this.uLabelProductionA.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductionA.TabIndex = 0;
            this.uLabelProductionA.Text = "ultraLabel1";
            // 
            // uLabelPackageTypeA
            // 
            this.uLabelPackageTypeA.Location = new System.Drawing.Point(12, 28);
            this.uLabelPackageTypeA.Name = "uLabelPackageTypeA";
            this.uLabelPackageTypeA.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackageTypeA.TabIndex = 0;
            this.uLabelPackageTypeA.Text = "ultraLabel1";
            // 
            // uLabelPackageA
            // 
            this.uLabelPackageA.Location = new System.Drawing.Point(240, 28);
            this.uLabelPackageA.Name = "uLabelPackageA";
            this.uLabelPackageA.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackageA.TabIndex = 0;
            this.uLabelPackageA.Text = "ultraLabel1";
            // 
            // uGroupRecipe
            // 
            this.uGroupRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupRecipe.Controls.Add(this.uGridRecipe);
            this.uGroupRecipe.Location = new System.Drawing.Point(0, 336);
            this.uGroupRecipe.Name = "uGroupRecipe";
            this.uGroupRecipe.Size = new System.Drawing.Size(940, 112);
            this.uGroupRecipe.TabIndex = 1;
            // 
            // uGridRecipe
            // 
            this.uGridRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRecipe.DisplayLayout.Appearance = appearance13;
            this.uGridRecipe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRecipe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRecipe.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRecipe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridRecipe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRecipe.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridRecipe.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRecipe.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRecipe.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRecipe.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridRecipe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRecipe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRecipe.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRecipe.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridRecipe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRecipe.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRecipe.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridRecipe.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridRecipe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRecipe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridRecipe.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridRecipe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRecipe.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridRecipe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRecipe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRecipe.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRecipe.Location = new System.Drawing.Point(12, 8);
            this.uGridRecipe.Name = "uGridRecipe";
            this.uGridRecipe.Size = new System.Drawing.Size(920, 94);
            this.uGridRecipe.TabIndex = 0;
            this.uGridRecipe.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridRecipe_DoubleClickRow);
            // 
            // uGroupDurable
            // 
            this.uGroupDurable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupDurable.Controls.Add(this.uGridDurableMat);
            this.uGroupDurable.Controls.Add(this.uButtonDeleteRow);
            this.uGroupDurable.Location = new System.Drawing.Point(0, 448);
            this.uGroupDurable.Name = "uGroupDurable";
            this.uGroupDurable.Size = new System.Drawing.Size(940, 152);
            this.uGroupDurable.TabIndex = 1;
            // 
            // uGridDurableMat
            // 
            this.uGridDurableMat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMat.DisplayLayout.Appearance = appearance1;
            this.uGridDurableMat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridDurableMat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMat.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridDurableMat.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMat.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMat.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridDurableMat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMat.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridDurableMat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMat.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridDurableMat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMat.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridDurableMat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMat.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridDurableMat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMat.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMat.Location = new System.Drawing.Point(12, 36);
            this.uGridDurableMat.Name = "uGridDurableMat";
            this.uGridDurableMat.Size = new System.Drawing.Size(920, 106);
            this.uGridDurableMat.TabIndex = 0;
            this.uGridDurableMat.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMat_AfterCellUpdate);
            this.uGridDurableMat.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMat_CellListSelect);
            this.uGridDurableMat.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridDurableMat_BeforeCellListDropDown);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 2;
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(744, 724);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 6;
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonCancel
            // 
            this.uButtonCancel.Location = new System.Drawing.Point(840, 724);
            this.uButtonCancel.Name = "uButtonCancel";
            this.uButtonCancel.Size = new System.Drawing.Size(88, 28);
            this.uButtonCancel.TabIndex = 7;
            this.uButtonCancel.Click += new System.EventHandler(this.uButtonCancel_Click);
            // 
            // uButtonChgCancel
            // 
            this.uButtonChgCancel.Location = new System.Drawing.Point(532, 724);
            this.uButtonChgCancel.Name = "uButtonChgCancel";
            this.uButtonChgCancel.Size = new System.Drawing.Size(160, 28);
            this.uButtonChgCancel.TabIndex = 5;
            this.uButtonChgCancel.Click += new System.EventHandler(this.uButtonChgCancel_Click);
            // 
            // uGroupDurableLot
            // 
            this.uGroupDurableLot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupDurableLot.Controls.Add(this.uGridDurableLot);
            this.uGroupDurableLot.Controls.Add(this.uButtonDeleteRows);
            this.uGroupDurableLot.Controls.Add(this.uLabelNoticeTool);
            this.uGroupDurableLot.Location = new System.Drawing.Point(0, 604);
            this.uGroupDurableLot.Name = "uGroupDurableLot";
            this.uGroupDurableLot.Size = new System.Drawing.Size(516, 152);
            this.uGroupDurableLot.TabIndex = 8;
            // 
            // uGridDurableLot
            // 
            this.uGridDurableLot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableLot.DisplayLayout.Appearance = appearance38;
            this.uGridDurableLot.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableLot.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLot.DisplayLayout.GroupByBox.Appearance = appearance40;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLot.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
            this.uGridDurableLot.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLot.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.uGridDurableLot.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableLot.DisplayLayout.MaxRowScrollRegions = 1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableLot.DisplayLayout.Override.ActiveCellAppearance = appearance43;
            appearance44.BackColor = System.Drawing.SystemColors.Highlight;
            appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableLot.DisplayLayout.Override.ActiveRowAppearance = appearance44;
            this.uGridDurableLot.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableLot.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLot.DisplayLayout.Override.CardAreaAppearance = appearance45;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableLot.DisplayLayout.Override.CellAppearance = appearance46;
            this.uGridDurableLot.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableLot.DisplayLayout.Override.CellPadding = 0;
            appearance47.BackColor = System.Drawing.SystemColors.Control;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLot.DisplayLayout.Override.GroupByRowAppearance = appearance47;
            appearance48.TextHAlignAsString = "Left";
            this.uGridDurableLot.DisplayLayout.Override.HeaderAppearance = appearance48;
            this.uGridDurableLot.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableLot.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableLot.DisplayLayout.Override.RowAppearance = appearance49;
            this.uGridDurableLot.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableLot.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.uGridDurableLot.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableLot.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableLot.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableLot.Location = new System.Drawing.Point(12, 36);
            this.uGridDurableLot.Name = "uGridDurableLot";
            this.uGridDurableLot.Size = new System.Drawing.Size(496, 110);
            this.uGridDurableLot.TabIndex = 1;
            // 
            // uButtonDeleteRows
            // 
            this.uButtonDeleteRows.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRows.Name = "uButtonDeleteRows";
            this.uButtonDeleteRows.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRows.TabIndex = 0;
            // 
            // uLabelNoticeTool
            // 
            appearance51.ForeColor = System.Drawing.Color.Red;
            appearance51.TextHAlignAsString = "Center";
            appearance51.TextVAlignAsString = "Middle";
            this.uLabelNoticeTool.Appearance = appearance51;
            this.uLabelNoticeTool.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelNoticeTool.Location = new System.Drawing.Point(104, 32);
            this.uLabelNoticeTool.Name = "uLabelNoticeTool";
            this.uLabelNoticeTool.Size = new System.Drawing.Size(185, 20);
            this.uLabelNoticeTool.TabIndex = 0;
            this.uLabelNoticeTool.Text = "등록안된 툴만 사용";
            // 
            // frmEQUZ0022_P3_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(944, 760);
            this.Controls.Add(this.uGroupDurableLot);
            this.Controls.Add(this.uButtonCancel);
            this.Controls.Add(this.uGroupDurable);
            this.Controls.Add(this.uButtonChgCancel);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uGroupRecipe);
            this.Controls.Add(this.uGroupAfterProduct);
            this.Controls.Add(this.uGroupBeforeProduct);
            this.Controls.Add(this.uGroupEquip);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0022_P3_S";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmEQUZ0022_P3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupEquip)).EndInit();
            this.uGroupEquip.ResumeLayout(false);
            this.uGroupEquip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStopTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStopTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBeforeProduct)).EndInit();
            this.uGroupBeforeProduct.ResumeLayout(false);
            this.uGroupBeforeProduct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductionB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageBB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageTypeB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupAfterProduct)).EndInit();
            this.uGroupAfterProduct.ResumeLayout(false);
            this.uGroupAfterProduct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRecipe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductionA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageAA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageTypeA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupRecipe)).EndInit();
            this.uGroupRecipe.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridRecipe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurable)).EndInit();
            this.uGroupDurable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurableLot)).EndInit();
            this.uGroupDurableLot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupEquip;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBeforeProduct;
        private Infragistics.Win.Misc.UltraGroupBox uGroupAfterProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUser;
        private Infragistics.Win.Misc.UltraLabel uLabelUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductionB;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageBB;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageB;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageTypeB;
        private Infragistics.Win.Misc.UltraLabel uLabelProductionB;
        private Infragistics.Win.Misc.UltraLabel uLabelPackageB;
        private Infragistics.Win.Misc.UltraLabel uLabelPackageTypeB;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductionA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageAA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageTypeA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageA;
        private Infragistics.Win.Misc.UltraLabel uLabelProductionA;
        private Infragistics.Win.Misc.UltraLabel uLabelPackageTypeA;
        private Infragistics.Win.Misc.UltraLabel uLabelPackageA;
        private Infragistics.Win.Misc.UltraGroupBox uGroupRecipe;
        private Infragistics.Win.Misc.UltraGroupBox uGroupDurable;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStopTypeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStopTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelStopType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextComment;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEMC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRecipe;
        private Infragistics.Win.Misc.UltraLabel uLabelComment;
        private Infragistics.Win.Misc.UltraLabel uLabelEMC;
        private Infragistics.Win.Misc.UltraLabel uLabelSerial;
        private Infragistics.Win.Misc.UltraLabel uLabelSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelRecipe;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRecipe;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMat;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonCancel;
        private Infragistics.Win.Misc.UltraButton uButtonChgCancel;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uTextSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.Misc.UltraGroupBox uGroupDurableLot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableLot;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRows;
        private Infragistics.Win.Misc.UltraLabel uLabelNoticeTool;
    }
}