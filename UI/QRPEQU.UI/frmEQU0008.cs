﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQU0008.cs                                         */
/* 프로그램명   : 수리의뢰등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-30                                            */
/* 수정이력     : 2011-08-31 : 이종민 수정                              */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

//using QRPDB;
//using System.Data.SqlClient;

namespace QRPEQU.UI
{
    public partial class frmEQU0008 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
        private static Boolean blSaveCheck; //저장확인용 Boolean 변수
        private static Boolean blMaterialCheck;
        private string strRepairRequestcode = "";

        private Boolean m_bolDebugMode = false;
        private string m_strDBConn = "";

        public frmEQU0008()
        {
            InitializeComponent();
            SetRunMode();
            SetToolAuth();
        }

        private string RepairRequestCode
        {
            get { return strRepairRequestcode; }
            set { strRepairRequestcode = value; }
        }

        #region 컨트롤초기화

        private void frmEQU0008_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, true, true, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0008_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("수리의뢰등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //this.ControlBox = false;
            //this.WindowState = FormWindowState.Maximized;
            //foreach (Control ctrl in this.Controls)
            //{
            //    if (!ctrl.Name.Equals("titleArea"))
            //    {
            //        ctrl.Anchor = ((AnchorStyles)((AnchorStyles.Top) | AnchorStyles.Left));

            //        if (ctrl.HasChildren)
            //        {
            //            foreach (Control _ctrl in ctrl.Controls)
            //            {
            //                _ctrl.Anchor = ((AnchorStyles)((AnchorStyles.Top) | AnchorStyles.Left));

            //                if (_ctrl.HasChildren)
            //                {
            //                    foreach (Control _ctrl2 in _ctrl.Controls)
            //                    {
            //                        _ctrl2.Anchor = ((AnchorStyles)((AnchorStyles.Top) | AnchorStyles.Left));
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}

            // 초기화 Method
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitValue();
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "설비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.Appearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.DETAIL, "수리요청상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                //폰트설정
                this.uGroupBox2.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.Appearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSuper, "Super설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLocation, "Location", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipGroupName, "설비그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSTSStock, "STS입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProductYear, "제작년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipRating, "설비등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelRepairDate, "수리요청일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRepairTime, "수리요청시간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRepairUser, "수리요청자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelFailureDay, "고장발생일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelFailureTime, "고장발생시간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProduct, "제품", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelPKGType, "PKG Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestReason, "요청사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call Bl
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                for (int i = 0; i < uComboPlant.Items.Count; i++)
                {
                    uComboPlant.SelectedIndex = i;
                    if (uComboPlant.Value.ToString().Equals(m_resSys.GetString("SYS_PLANTCODE").ToString()))
                    {
                        break;
                    }
                    else
                    {
                        uComboPlant.SelectedIndex = 0;
                    }
                }                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Component Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //this.uComboPlant.Value = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipGroupName.Text = "";
                this.uTextSuper.Text = "";
                this.uTextArea.Text = "";
                this.uTextStation.Text = "";
                this.uTextLocation.Text = "";
                this.uTextEquipProcess.Text = "";
                this.uTextModel.Text = "";
                this.uTextSerialNo.Text = "";
                this.uTextEquipType.Text = "";
                this.uTextEquipGroupName.Text = "";
                this.uTextVendor.Text = "";
                this.uTextSTSStock.Text = "";
                this.uTextProductYear.Text = "";
                this.uTextEquipRating.Text = "";
                this.uDateRepairDate.Value = DateTime.Now;
                this.uDateRepairTime.Value = DateTime.Now;
                this.uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextRepairUserName.Text = "";
                this.uDateFailureDay.Value = DateTime.Now;
                this.uDateFailureTime.Value = DateTime.Now;
                this.uCheckEmerHandle.CheckedValue = false;
                this.uTextProductCode.Text = "";
                this.uTextProductName.Text = "";
                this.uTextPKGType.Text = "";
                this.uTextRequestReason.Text = "";
                RepairRequestCode = string.Empty;
                blSaveCheck = false;
                blMaterialCheck = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region CommonEvent
        public void mfSearch()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            
            try
            {
                // 필수입력 사항 확인


                #region 필수확인
                if (uTextEquipCode.Text == string.Empty || uTextEquipName.Text == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "수리 요청할 설비를 입력하세요", Infragistics.Win.HAlign.Right);
                    uTextEquipCode.Focus();
                    return;
                }

                if (uDateRepairDate.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "수리 요청일을 입력하세요", Infragistics.Win.HAlign.Right);
                    uDateRepairDate.Focus();
                    return;
                }

                if (uDateRepairTime.Value.ToString() == string.Empty || uDateRepairTime.Value.ToString() == "00:00:00")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "수리 요청일을 입력하세요", Infragistics.Win.HAlign.Right);
                    uDateRepairTime.Focus();
                    return;
                }

                if (uTextRepairUserID.Text == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "수리 요청자를 입력하세요", Infragistics.Win.HAlign.Right);
                    uTextRepairUserID.Focus();
                    return;
                }

                if (uDateFailureDay.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "고장 발생일을 입력하세요", Infragistics.Win.HAlign.Right);
                    uDateFailureDay.Focus();
                    return;
                }

                if (uDateFailureTime.Value.ToString() == string.Empty || uDateFailureTime.Value.ToString() == "00:00:00")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "고장 발생일을 입력하세요", Infragistics.Win.HAlign.Right);
                    uDateFailureTime.Focus();
                    return;
                }

                //if (!blMaterialCheck)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                , "확인창", "필수입력사항 확인", "제품을 입력하세요", Infragistics.Win.HAlign.Right);
                //    uTextProductCode.Focus();
                //    return;
                //}

                if (uTextProductCode.Text == string.Empty || uTextProductName.Text == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "제품을 입력하세요", Infragistics.Win.HAlign.Right);
                    uTextProductCode.Focus();
                    return;
                }
                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                if (blSaveCheck)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "저장확인", uTextEquipName.Text + "를 수리 요청 하시겠습니까?",
                                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                    {
                        return;
                    }
                }
                else
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "저장확인", uTextEquipName.Text + "를 수정 하시겠습니까?",
                                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                    {
                        return;
                    }
                }
                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);

                // 저장 시작
                string strRtn = clsEquipReq.mfSaveEquipRepairRequest(
                                                                        RepairRequestCode,
                                                                        uComboPlant.Value.ToString(), uTextEquipCode.Text.Trim().ToString(),
                                                                        Convert.ToDateTime(uDateRepairDate.Value.ToString()).ToString("yyyy-MM-dd"), Convert.ToDateTime(uDateRepairTime.Value.ToString()).ToString("HH:mm:ss"),
                                                                        uTextRepairUserID.Text.Trim().ToString(), Convert.ToDateTime(uDateFailureDay.Value.ToString()).ToString("yyyy-MM-dd"),
                                                                        Convert.ToDateTime(uDateFailureTime.Value.ToString()).ToString("HH:mm:ss"), uTextProductCode.Text.Trim().ToString(),
                                                                        uTextRequestReason.Text.Trim().ToString(), uCheckEmerHandle.Checked == true ? "T" : "F",
                                                                        m_resSys.GetString("SYS_USERIP").ToString(), m_resSys.GetString("SYS_USERID").ToString()
                                                                    );

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                // 저장 끝

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    blSaveCheck = false;
                    InitValue();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                //clsEquipReq.Dispose();
            }
        }

        public void mfDelete()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            if (RepairRequestCode == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "확인창", "수리요청을 취소할 설비를 입력하세요.", Infragistics.Win.HAlign.Right);
                return;
            }

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "확인창", "삭제확인", uTextEquipName.Text + "의 수리요청을 취소 하시겠습니까?",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            Thread t1 = m_ProgressPopup.mfStartThread();
            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
            QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
            brwChannel.mfCredentials(clsEquipReq);

            try
            {
                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string strRtn = clsEquipReq.mfDeleteEquipRepairRequest(RepairRequestCode);

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();

                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "삭제처리결과", "수리 요청을 성공적으로 삭제했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    blSaveCheck = false;
                    InitValue();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "삭제처리결과", "수리 요청을 삭제하지 못했습니다.",
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitValue();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        #endregion

        #region UI Event

        /// <summary>
        /// 설비번호 직접입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode != Keys.Enter)
                    return;

                if (uTextEquipCode.Text == string.Empty)
                    return;

                mfSelectEquip();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비정보 조회
        /// </summary>
        private void mfSelectEquip()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            DataTable dtEquip = new DataTable();

            try
            {
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                QRPMAS.BL.MASEQU.Equip Equip = new QRPMAS.BL.MASEQU.Equip();
                dtEquip = Equip.mfReadEquipInfoDetail(uComboPlant.Value.ToString(), uTextEquipCode.Text.Trim().ToString(), m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count != 0)
                {
                        uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                        uTextArea.Text = dtEquip.Rows[0]["AreaCode"].ToString();
                        uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();
                        uTextEquipProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString();
                        uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();
                        uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();
                        uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();
                        uTextEquipRating.Text = dtEquip.Rows[0]["EquipLevelCode"].ToString();
                        uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();
                        uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();
                        uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();
                        uTextProductYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();
                        uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();
                        //uTextSuper.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();
                        uTextSuper.Text = dtEquip.Rows[0]["SuperEquipCode"].ToString();

                        blSaveCheck = true;
                        mfCheckEquipOnRepair(uComboPlant.Value.ToString(), uTextEquipCode.Text);
                }
                else
                {
                    foreach (Control ctrl in uGroupBox1.Controls)
                    {
                        if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                        {
                            ctrl.Text = string.Empty;
                        }
                    }
                    blSaveCheck = false;
                }
            }
            catch (Exception ex)
            {
                blSaveCheck = false;
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비번호 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                blSaveCheck = false;
                if (uTextEquipCode.Text == string.Empty)
                {
                    foreach (Control ctrl in uGroupBox1.Controls)
                    {
                        if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                        {
                            if (ctrl.Name != uTextEquipCode.Name)
                                ctrl.Text = string.Empty;
                        }
                    }
                }
                else
                {
                    //mfSelectEquip();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
             finally
            {
            }
        }

        /// <summary>
        /// 제품번호 직접입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextProductCode_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Enter)
                    return;

                mfSelectMaterial();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 제품정보 조회
        /// </summary>
        private void mfSelectMaterial()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product Material = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(Material);

                DataTable dtMaterial = Material.mfReadMASMaterialDetail(uComboPlant.Value.ToString(), uTextProductCode.Text, m_resSys.GetString("SYS_LANG"));

                if (dtMaterial.Rows.Count == 0)
                {
                    dtMaterial.Dispose();
                    blMaterialCheck = false;
                    uTextProductName.Text = string.Empty;
                    uTextPKGType.Text = string.Empty;
                    return;
                }

                uTextProductName.Text = dtMaterial.Rows[0]["ProductName"].ToString();
                blMaterialCheck = true;

                dtMaterial.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 제품번호 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextProductCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (uTextProductCode.Text == string.Empty)
                {
                    uTextProductName.Text = string.Empty;
                    blMaterialCheck = false;
                }
                else
                {
                    //mfSelectMaterial();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공장콤보 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            if (!blSaveCheck && uTextEquipCode.Text != string.Empty)
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtEquip = new DataTable();

                try
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    QRPMAS.BL.MASEQU.Equip Equip = new QRPMAS.BL.MASEQU.Equip();
                    dtEquip = Equip.mfReadEquipInfoDetail(uComboPlant.Value.ToString(), uTextEquipCode.Text.Trim().ToString(), m_resSys.GetString("SYS_LANG"));

                    if (dtEquip.Rows.Count != 0)
                    {
                        uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                        uTextArea.Text = dtEquip.Rows[0]["AreaCode"].ToString();
                        uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();
                        uTextEquipProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString();
                        uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();
                        uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();
                        uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();
                        uTextEquipRating.Text = dtEquip.Rows[0]["EquipLevelCode"].ToString();
                        uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();
                        uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();
                        uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();
                        uTextProductYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();
                        uTextStation.Text = dtEquip.Rows[0]["StationCode"].ToString();
                        //uTextSuper.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();
                        uTextSuper.Text = dtEquip.Rows[0]["SuperEquipCode"].ToString();

                        blSaveCheck = true;
                        dtEquip.Dispose();
                    }
                    else
                    {
                        foreach (Control ctrl in uGroupBox1.Controls)
                        {
                            if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                            {
                                if (ctrl.Name != uTextEquipCode.Name)
                                    ctrl.Text = string.Empty;
                            }
                        }
                        blSaveCheck = false;
                        dtEquip.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    blSaveCheck = false;
                    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                    frmErr.ShowDialog();
                }
                finally
                {
                    dtEquip.Dispose();
                }               
            }
        }

        /// <summary>
        /// 설비번호 팝업창 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                frmEquip.PlantCode = uComboPlant.Value.ToString();

                frmEquip.ShowDialog();


                uTextEquipCode.Text = frmEquip.EquipCode;
                uTextEquipName.Text = frmEquip.EquipName;
                uTextEquipProcess.Text = frmEquip.EquipProcGubunName;
                uTextEquipGroupName.Text = frmEquip.EquipGroupName;
                uTextEquipRating.Text = frmEquip.EquipLevelCode;
                uTextEquipType.Text = frmEquip.EquipTypeName;
                uTextLocation.Text = frmEquip.EquippLocCode;
                uTextArea.Text = frmEquip.AreaName;
                uTextVendor.Text = frmEquip.VendorName;
                uTextStation.Text = frmEquip.StationName;
                uTextModel.Text = frmEquip.ModelName;
                uTextProductYear.Text = frmEquip.MakeYear;
                uTextSerialNo.Text = frmEquip.SerialNo;
                uTextSuper.Text = frmEquip.SuperEquipCode;
                uTextSTSStock.Text = frmEquip.GRDate;
                uTextSerialNo.Text = frmEquip.SerialNo;
                uTextSTSStock.Text = frmEquip.GRDate;

                mfCheckEquipOnRepair(uComboPlant.Value.ToString(), uTextEquipCode.Text);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void mfCheckEquipOnRepair(string strPlantCode, string strEquipCode)
        {
            if (strEquipCode == string.Empty)
                return;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            DataTable dtEquip = new DataTable();

            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
            QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
            brwChannel.mfCredentials(clsEquip);

            try
            {
                dtEquip = clsEquip.mfCheckEquipOnRepair(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count != 0)
                {
                    RepairRequestCode = dtEquip.Rows[0]["RepairReqCode"].ToString();
                    uDateRepairDate.Value = dtEquip.Rows[0]["RepairReqDate"].ToString();
                    uDateRepairTime.Value = dtEquip.Rows[0]["RepairReqTime"].ToString();
                    uDateFailureDay.Value = dtEquip.Rows[0]["BreakDownDate"].ToString();
                    uDateFailureTime.Value = dtEquip.Rows[0]["BreakDownTime"].ToString();
                    uTextRepairUserID.Text = dtEquip.Rows[0]["RepairReqID"].ToString();
                    uTextRepairUserName.Text = dtEquip.Rows[0]["UserName"].ToString();

                    if (dtEquip.Rows[0]["UrgentFlag"].ToString() == "T")
                        uCheckEmerHandle.Checked = true;
                    else
                        uCheckEmerHandle.Checked = false;

                    uTextProductCode.Text = dtEquip.Rows[0]["ProductCode"].ToString();
                    uTextProductName.Text = dtEquip.Rows[0]["ProductName"].ToString();
                    uTextRequestReason.Text = dtEquip.Rows[0]["RepairReqReason"].ToString();

                    blMaterialCheck = false;
                    blSaveCheck = false;
                }
                else
                {
                    this.uDateRepairDate.Value = DateTime.Now;
                    this.uDateRepairTime.Value = DateTime.Now;
                    this.uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");
                    this.uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");
                    this.uTextRepairUserName.Text = "";
                    this.uDateFailureDay.Value = DateTime.Now;
                    this.uDateFailureTime.Value = DateTime.Now;
                    this.uCheckEmerHandle.CheckedValue = false;
                    this.uTextProductCode.Text = "";
                    this.uTextProductName.Text = "";
                    this.uTextPKGType.Text = "";
                    this.uTextRequestReason.Text = "";
                    RepairRequestCode = string.Empty;
                    blSaveCheck = false;
                    blMaterialCheck = false;
                }

                //Equip.Dispose();
                dtEquip.Dispose();
            }
            catch (Exception ex)
            {
                blSaveCheck = false;
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                //clsEquip.Dispose();
            }
        }

        /// <summary>
        /// 수리요청ID 팝업창 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRepairUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboPlant.Value.ToString();

                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextRepairUserID.Text = frmUser.UserID;
                this.uTextRepairUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 제품번호 팝업창 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0002 frmMaterial = new QRPCOM.UI.frmCOM0002();
                frmMaterial.PlantCode = uComboPlant.Value.ToString();

                frmMaterial.ShowDialog();

                if (frmMaterial.PlantCode != "" && strPlant != frmMaterial.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextProductCode.Text = frmMaterial.ProductCode;
                this.uTextProductName.Text = frmMaterial.ProductName;
                this.uTextPKGType.Text = frmMaterial.MaterialTypeName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //스핀버튼을 누르면 초가늘어남
        private void uDateTime_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼
                Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                DateTime dateTemp = (DateTime)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    ed.Value = dateTemp.AddSeconds(1);
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && dateTemp > DateTime.MinValue)
                {
                    ed.Value = dateTemp.AddSeconds(-1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Debuging

        private void SetRunMode()
        {
            m_strDBConn = "data source=10.60.24.173;user id=QRPUSR;password=jinjujin;Initial Catalog=QRP_STS_DEV;persist security info=true";
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion



    }
}
