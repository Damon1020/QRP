﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0020
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0020));
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextStockTakeConfirmDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockTakeConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStockTakeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockTakeCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStockTakeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStockTakeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextStockTakeEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockTakeEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateStockTakeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelStockTakeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStockTakeChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboStockTakeMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStockTakeMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uComboStockTakeYear = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStockTakeYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSPInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSPInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStockTakeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextStockTakeConfirmDate);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeConfirmDate);
            this.uGroupBox.Controls.Add(this.uTextStockTakeCode);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeCode);
            this.uGroupBox.Controls.Add(this.uLabel5);
            this.uGroupBox.Controls.Add(this.uTextStockTakeChargeID);
            this.uGroupBox.Controls.Add(this.uTextStockTakeChargeName);
            this.uGroupBox.Controls.Add(this.uGrid1);
            this.uGroupBox.Controls.Add(this.uTextStockTakeEtcDesc);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeEtcDesc);
            this.uGroupBox.Controls.Add(this.uDateStockTakeDate);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeDate);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeChargeID);
            this.uGroupBox.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 764);
            this.uGroupBox.TabIndex = 18;
            // 
            // uTextStockTakeConfirmDate
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeConfirmDate.Appearance = appearance67;
            this.uTextStockTakeConfirmDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeConfirmDate.Location = new System.Drawing.Point(404, 44);
            this.uTextStockTakeConfirmDate.Name = "uTextStockTakeConfirmDate";
            this.uTextStockTakeConfirmDate.ReadOnly = true;
            this.uTextStockTakeConfirmDate.Size = new System.Drawing.Size(100, 21);
            this.uTextStockTakeConfirmDate.TabIndex = 104;
            // 
            // uLabelStockTakeConfirmDate
            // 
            this.uLabelStockTakeConfirmDate.Location = new System.Drawing.Point(300, 44);
            this.uLabelStockTakeConfirmDate.Name = "uLabelStockTakeConfirmDate";
            this.uLabelStockTakeConfirmDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeConfirmDate.TabIndex = 103;
            // 
            // uTextStockTakeCode
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeCode.Appearance = appearance20;
            this.uTextStockTakeCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeCode.Location = new System.Drawing.Point(116, 44);
            this.uTextStockTakeCode.Name = "uTextStockTakeCode";
            this.uTextStockTakeCode.ReadOnly = true;
            this.uTextStockTakeCode.Size = new System.Drawing.Size(100, 21);
            this.uTextStockTakeCode.TabIndex = 102;
            // 
            // uLabelStockTakeCode
            // 
            this.uLabelStockTakeCode.Location = new System.Drawing.Point(12, 44);
            this.uLabelStockTakeCode.Name = "uLabelStockTakeCode";
            this.uLabelStockTakeCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeCode.TabIndex = 101;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 116);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(100, 20);
            this.uLabel5.TabIndex = 100;
            // 
            // uTextStockTakeChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStockTakeChargeID.Appearance = appearance15;
            this.uTextStockTakeChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStockTakeChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextStockTakeChargeID.ButtonsRight.Add(editorButton1);
            this.uTextStockTakeChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextStockTakeChargeID.Location = new System.Drawing.Point(404, 68);
            this.uTextStockTakeChargeID.MaxLength = 20;
            this.uTextStockTakeChargeID.Name = "uTextStockTakeChargeID";
            this.uTextStockTakeChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextStockTakeChargeID.TabIndex = 99;
            this.uTextStockTakeChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextStockTakeChargeID_KeyDown);
            this.uTextStockTakeChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextStockTakeChargeID_EditorButtonClick);
            // 
            // uTextStockTakeChargeName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeChargeName.Appearance = appearance21;
            this.uTextStockTakeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeChargeName.Location = new System.Drawing.Point(508, 68);
            this.uTextStockTakeChargeName.Name = "uTextStockTakeChargeName";
            this.uTextStockTakeChargeName.ReadOnly = true;
            this.uTextStockTakeChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextStockTakeChargeName.TabIndex = 98;
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance6;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance17.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance17;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance18;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance19;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 120);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1040, 628);
            this.uGrid1.TabIndex = 60;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uTextStockTakeEtcDesc
            // 
            this.uTextStockTakeEtcDesc.Location = new System.Drawing.Point(116, 92);
            this.uTextStockTakeEtcDesc.MaxLength = 1000;
            this.uTextStockTakeEtcDesc.Name = "uTextStockTakeEtcDesc";
            this.uTextStockTakeEtcDesc.Size = new System.Drawing.Size(492, 21);
            this.uTextStockTakeEtcDesc.TabIndex = 58;
            // 
            // uLabelStockTakeEtcDesc
            // 
            this.uLabelStockTakeEtcDesc.Location = new System.Drawing.Point(12, 92);
            this.uLabelStockTakeEtcDesc.Name = "uLabelStockTakeEtcDesc";
            this.uLabelStockTakeEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeEtcDesc.TabIndex = 57;
            // 
            // uDateStockTakeDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateStockTakeDate.Appearance = appearance16;
            this.uDateStockTakeDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateStockTakeDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateStockTakeDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateStockTakeDate.Location = new System.Drawing.Point(116, 68);
            this.uDateStockTakeDate.Name = "uDateStockTakeDate";
            this.uDateStockTakeDate.Size = new System.Drawing.Size(116, 19);
            this.uDateStockTakeDate.TabIndex = 54;
            // 
            // uLabelStockTakeDate
            // 
            this.uLabelStockTakeDate.Location = new System.Drawing.Point(12, 68);
            this.uLabelStockTakeDate.Name = "uLabelStockTakeDate";
            this.uLabelStockTakeDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeDate.TabIndex = 52;
            // 
            // uLabelStockTakeChargeID
            // 
            this.uLabelStockTakeChargeID.Location = new System.Drawing.Point(300, 68);
            this.uLabelStockTakeChargeID.Name = "uLabelStockTakeChargeID";
            this.uLabelStockTakeChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeChargeID.TabIndex = 53;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboStockTakeMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStockTakeMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboStockTakeYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStockTakeYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSPInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSPInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 17;
            // 
            // uComboStockTakeMonth
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeMonth.Appearance = appearance2;
            this.uComboStockTakeMonth.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeMonth.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboStockTakeMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboStockTakeMonth.Location = new System.Drawing.Point(904, 12);
            this.uComboStockTakeMonth.Name = "uComboStockTakeMonth";
            this.uComboStockTakeMonth.Size = new System.Drawing.Size(120, 19);
            this.uComboStockTakeMonth.TabIndex = 7;
            this.uComboStockTakeMonth.ValueChanged += new System.EventHandler(this.uComboStockTakeMonth_ValueChanged);
            this.uComboStockTakeMonth.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboStockTakeMonth_BeforeDropDown);
            // 
            // uLabelStockTakeMonth
            // 
            this.uLabelStockTakeMonth.Location = new System.Drawing.Point(800, 12);
            this.uLabelStockTakeMonth.Name = "uLabelStockTakeMonth";
            this.uLabelStockTakeMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeMonth.TabIndex = 6;
            // 
            // uComboStockTakeYear
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeYear.Appearance = appearance3;
            this.uComboStockTakeYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeYear.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboStockTakeYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboStockTakeYear.Location = new System.Drawing.Point(644, 12);
            this.uComboStockTakeYear.Name = "uComboStockTakeYear";
            this.uComboStockTakeYear.Size = new System.Drawing.Size(120, 19);
            this.uComboStockTakeYear.TabIndex = 5;
            this.uComboStockTakeYear.ValueChanged += new System.EventHandler(this.uComboStockTakeYear_ValueChanged);
            // 
            // uLabelStockTakeYear
            // 
            this.uLabelStockTakeYear.Location = new System.Drawing.Point(540, 12);
            this.uLabelStockTakeYear.Name = "uLabelStockTakeYear";
            this.uLabelStockTakeYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeYear.TabIndex = 4;
            // 
            // uComboSPInventory
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSPInventory.Appearance = appearance4;
            this.uComboSPInventory.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSPInventory.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSPInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboSPInventory.Location = new System.Drawing.Point(380, 12);
            this.uComboSPInventory.MaxLength = 50;
            this.uComboSPInventory.Name = "uComboSPInventory";
            this.uComboSPInventory.Size = new System.Drawing.Size(120, 19);
            this.uComboSPInventory.TabIndex = 3;
            this.uComboSPInventory.ValueChanged += new System.EventHandler(this.uComboSPInventory_ValueChanged);
            // 
            // uLabelSPInventory
            // 
            this.uLabelSPInventory.Location = new System.Drawing.Point(276, 12);
            this.uLabelSPInventory.Name = "uLabelSPInventory";
            this.uLabelSPInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelSPInventory.TabIndex = 2;
            // 
            // uComboPlant
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance5;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 19;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQUZ0020
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0020";
            this.Load += new System.EventHandler(this.frmEQUZ0020_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0020_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmEQUZ0020_FormClosed);
            this.Resize += new System.EventHandler(this.frmEQUZ0020_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStockTakeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateStockTakeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeChargeID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSPInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelSPInventory;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeChargeName;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStockTakeMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeMonth;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStockTakeYear;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeConfirmDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeCode;

    }
}
