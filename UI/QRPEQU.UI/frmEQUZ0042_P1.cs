﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 전산SHEET관리                                         */
/* 프로그램ID   : frmEQUZ0042_P1.cs                                     */
/* 프로그램명   : CLEANER RUBBER 교체 주기(설비등록팝업창)              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-04-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0042_P1 : Form
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmEQUZ0042_P1()
        {
            InitializeComponent();
        }

        private void frmEQUZ0042_P1_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0042_P1_Load(object sender, EventArgs e)
        {
            InitButton();
            InitGrid();
            mfSearchEquip();

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //Resource 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSave, "등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
                btn.mfSetButton(this.uButtonCancel, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonDel, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();


                #region 설비팝업창

                // 일반설정
                grd.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column  
                grd.mfSetGridColumn(this.uGridEquip, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "QUA1", "교체시작일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "QUA2", "교체시작일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                #endregion


                grd.mfSetGridColumnValueList(this.uGridEquip, 0, "QUA1", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", SetCombo());

                this.uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                grd.mfAddRowGrid(this.uGridEquip, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        /// <summary>
        /// 셀이 수정될시 편집이미지
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquip_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquip, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                

                // 설비중복 확인
                if (e.Cell.Column.Key.Equals("EquipCode")  && !e.Cell.Value.ToString().Equals(string.Empty))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult result = new DialogResult();

                    for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                    {
                        if (!i.Equals(e.Cell.Row.Index) 
                            && e.Cell.Value.ToString().Equals(this.uGridEquip.Rows[i].GetCellValue("EquipCode").ToString()))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "확인창", "입력사항확인", "이미 입력하신 설비입니다.",
                                                            Infragistics.Win.HAlign.Right);
                            e.Cell.Value = string.Empty;
                            return;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
       
        /// <summary>
        /// 행삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquip.Rows[i].Cells["Check"].Value)
                        && !this.uGridEquip.Rows[i].Hidden)
                    {
                        this.uGridEquip.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 취소버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 등록버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Resource 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGridEquip.Rows.Count > 0)
                    this.uGridEquip.ActiveCell = this.uGridEquip.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY2), "ASSY2");
                QRPEQU.BL.EQUSHT.ASSY2 clsASSY2 = new QRPEQU.BL.EQUSHT.ASSY2();
                brwChannel.mfCredentials(clsASSY2);

                DataTable dtDataInfo = clsASSY2.mfSetDataInfo_CleanerH();
                DataTable dtDelDataInfo = clsASSY2.mfSetDelDataInfo_Cleaner();

                for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                {
                    if (this.uGridEquip.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (!this.uGridEquip.Rows[i].Hidden)
                        {
                            #region 필수입력사항
                            if (this.uGridEquip.Rows[i].GetCellValue("EquipCode").ToString().Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "확인창", "필수입력사항확인", "설비코드를 입력해주세요.",
                                                                Infragistics.Win.HAlign.Right);
                                this.uGridEquip.ActiveCell = this.uGridEquip.Rows[i].Cells["EquipCode"];
                                this.uGridEquip.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "확인창", "필수입력사항확인", "교체주기를 선택해주세요.",
                                                                Infragistics.Win.HAlign.Right);
                                this.uGridEquip.ActiveCell = this.uGridEquip.Rows[i].Cells["QUA1"];
                                this.uGridEquip.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            #endregion

                            DataRow drRow = dtDataInfo.NewRow();

                            drRow["EquipCode"] = this.uGridEquip.Rows[i].GetCellValue("EquipCode");
                            drRow["QUA1"] = this.uGridEquip.Rows[i].GetCellValue("QUA1");

                            #region 교체주기
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("01-05"))
                            {
                                drRow["QUA2"] = "04-05";
                                drRow["QUA3"] = "07-05";
                                drRow["QUA4"] = "10-05";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("01-15"))
                            {
                                drRow["QUA2"] = "04-15";
                                drRow["QUA3"] = "07-15";
                                drRow["QUA4"] = "10-15";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("01-25"))
                            {
                                drRow["QUA2"] = "04-25";
                                drRow["QUA3"] = "07-25";
                                drRow["QUA4"] = "10-25";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("02-05"))
                            {
                                drRow["QUA2"] = "05-05";
                                drRow["QUA3"] = "08-05";
                                drRow["QUA4"] = "11-05";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("02-15"))
                            {
                                drRow["QUA2"] = "05-15";
                                drRow["QUA3"] = "08-15";
                                drRow["QUA4"] = "11-15";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("02-25"))
                            {
                                drRow["QUA2"] = "05-25";
                                drRow["QUA3"] = "08-25";
                                drRow["QUA4"] = "11-25";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("03-05"))
                            {
                                drRow["QUA2"] = "06-05";
                                drRow["QUA3"] = "09-05";
                                drRow["QUA4"] = "12-05";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("03-15"))
                            {
                                drRow["QUA2"] = "06-15";
                                drRow["QUA3"] = "09-15";
                                drRow["QUA4"] = "12-15";
                            }
                            if (this.uGridEquip.Rows[i].GetCellValue("QUA1").ToString().Equals("03-25"))
                            {
                                drRow["QUA2"] = "06-25";
                                drRow["QUA3"] = "09-25";
                                drRow["QUA4"] = "12-25";
                            }
                            #endregion

                            dtDataInfo.Rows.Add(drRow);
                        }
                        else
                        {
                            //행삭제정보 (설비코드가 존재하고 기존정보일 경우)
                            if (!this.uGridEquip.Rows[i].GetCellValue("EquipCode").ToString().Equals(string.Empty)
                                && !this.uGridEquip.Rows[i].GetCellValue("QUA2").ToString().Equals(string.Empty))
                            {
                               
                                DataRow drRow = dtDelDataInfo.NewRow();

                                drRow["EquipCode"] = this.uGridEquip.Rows[i].GetCellValue("EquipCode");

                                dtDelDataInfo.Rows.Add(drRow);
                            }
                        }
                    }
                }


                if (dtDataInfo.Rows.Count == 0 && dtDelDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;

                }


                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = clsASSY2.mfSaveCleanerRubberH(dtDataInfo, dtDelDataInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearchEquip();
                    }
                    else
                    {
                        string strMsg = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = "입력한 정보를 저장하지 못했습니다.";
                        else
                            strMsg = ErrRtn.ErrMessage;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", strMsg,
                                                     Infragistics.Win.HAlign.Right);
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 교체주기콤보
        /// </summary>
        /// <returns></returns>
        private DataTable SetCombo()
        {
            DataTable dtDate = new DataTable();

            try
            {

                dtDate.Columns.Add("Key", typeof(string));
                dtDate.Columns.Add("Value", typeof(string));


                DataRow drRow = dtDate.NewRow();

                drRow["Key"] = "01-05";
                drRow["Value"] = "1월5일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();
                drRow["Key"] = "01-15";
                drRow["Value"] = "1월15일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "01-25";
                drRow["Value"] = "1월25일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "02-05";
                drRow["Value"] = "2월5일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "02-15";
                drRow["Value"] = "2월15일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "02-25";
                drRow["Value"] = "2월25일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "03-05";
                drRow["Value"] = "3월5일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "03-15";
                drRow["Value"] = "3월15일";

                dtDate.Rows.Add(drRow);

                drRow = dtDate.NewRow();

                drRow["Key"] = "03-25";
                drRow["Value"] = "3월25일";

                dtDate.Rows.Add(drRow);


                return dtDate;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDate;
            }
            finally
            {
                dtDate.Dispose();
            }
        }

        /// <summary>
        /// 교체주기 설비조회
        /// </summary>
        private void mfSearchEquip()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY2), "ASSY2");
                QRPEQU.BL.EQUSHT.ASSY2 clsASSY2 = new QRPEQU.BL.EQUSHT.ASSY2();
                brwChannel.mfCredentials(clsASSY2);

                DataTable dtData = clsASSY2.mfReadCleanerRubberH("");

                this.uGridEquip.DataSource = dtData;
                this.uGridEquip.DataBind();

                if (dtData.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                    {
                        this.uGridEquip.Rows[i].Cells["EquipCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        

    }
}
