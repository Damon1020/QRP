﻿namespace QRPEQU.UI
{
    partial class frmEQU0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0008));
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEquipRating = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipRating = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductYear = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSTSStock = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSTSStock = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLocation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLocation = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uTextArea = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSuper = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSuper = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateRepairTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRepairTime = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRequestReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPKGType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPKGType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckEmerHandle = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateFailureTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelFailureTime = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFailureDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelFailureDay = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipRating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPKGType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEmerHandle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFailureTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFailureDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextEquipRating);
            this.uGroupBox1.Controls.Add(this.uLabelEquipRating);
            this.uGroupBox1.Controls.Add(this.uTextProductYear);
            this.uGroupBox1.Controls.Add(this.uLabelProductYear);
            this.uGroupBox1.Controls.Add(this.uTextSTSStock);
            this.uGroupBox1.Controls.Add(this.uLabelSTSStock);
            this.uGroupBox1.Controls.Add(this.uTextVendor);
            this.uGroupBox1.Controls.Add(this.uLabelVendor);
            this.uGroupBox1.Controls.Add(this.uTextEquipGroupName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipGroupName);
            this.uGroupBox1.Controls.Add(this.uTextEquipType);
            this.uGroupBox1.Controls.Add(this.uLabelEquipType);
            this.uGroupBox1.Controls.Add(this.uTextSerialNo);
            this.uGroupBox1.Controls.Add(this.uLabelSerialNo);
            this.uGroupBox1.Controls.Add(this.uTextModel);
            this.uGroupBox1.Controls.Add(this.uLabelModel);
            this.uGroupBox1.Controls.Add(this.uTextEquipProcess);
            this.uGroupBox1.Controls.Add(this.uLabelEquipProcess);
            this.uGroupBox1.Controls.Add(this.uTextLocation);
            this.uGroupBox1.Controls.Add(this.uLabelLocation);
            this.uGroupBox1.Controls.Add(this.uTextStation);
            this.uGroupBox1.Controls.Add(this.uLabelStation);
            this.uGroupBox1.Controls.Add(this.uTextArea);
            this.uGroupBox1.Controls.Add(this.uLabelArea);
            this.uGroupBox1.Controls.Add(this.uTextSuper);
            this.uGroupBox1.Controls.Add(this.uLabelSuper);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Controls.Add(this.uComboPlant);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1060, 192);
            this.uGroupBox1.TabIndex = 1;
            // 
            // uTextEquipRating
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipRating.Appearance = appearance21;
            this.uTextEquipRating.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipRating.Location = new System.Drawing.Point(116, 148);
            this.uTextEquipRating.Name = "uTextEquipRating";
            this.uTextEquipRating.ReadOnly = true;
            this.uTextEquipRating.Size = new System.Drawing.Size(160, 21);
            this.uTextEquipRating.TabIndex = 31;
            // 
            // uLabelEquipRating
            // 
            this.uLabelEquipRating.Location = new System.Drawing.Point(12, 148);
            this.uLabelEquipRating.Name = "uLabelEquipRating";
            this.uLabelEquipRating.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipRating.TabIndex = 30;
            this.uLabelEquipRating.Text = "ultraLabel12";
            // 
            // uTextProductYear
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductYear.Appearance = appearance3;
            this.uTextProductYear.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductYear.Location = new System.Drawing.Point(716, 124);
            this.uTextProductYear.Name = "uTextProductYear";
            this.uTextProductYear.ReadOnly = true;
            this.uTextProductYear.Size = new System.Drawing.Size(160, 21);
            this.uTextProductYear.TabIndex = 29;
            // 
            // uLabelProductYear
            // 
            this.uLabelProductYear.Location = new System.Drawing.Point(612, 124);
            this.uLabelProductYear.Name = "uLabelProductYear";
            this.uLabelProductYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductYear.TabIndex = 28;
            this.uLabelProductYear.Text = "ultraLabel9";
            // 
            // uTextSTSStock
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Appearance = appearance6;
            this.uTextSTSStock.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Location = new System.Drawing.Point(408, 124);
            this.uTextSTSStock.Name = "uTextSTSStock";
            this.uTextSTSStock.ReadOnly = true;
            this.uTextSTSStock.Size = new System.Drawing.Size(160, 21);
            this.uTextSTSStock.TabIndex = 27;
            // 
            // uLabelSTSStock
            // 
            this.uLabelSTSStock.Location = new System.Drawing.Point(304, 124);
            this.uLabelSTSStock.Name = "uLabelSTSStock";
            this.uLabelSTSStock.Size = new System.Drawing.Size(100, 20);
            this.uLabelSTSStock.TabIndex = 26;
            this.uLabelSTSStock.Text = "ultraLabel10";
            // 
            // uTextVendor
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance7;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 124);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(160, 21);
            this.uTextVendor.TabIndex = 25;
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 124);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelVendor.TabIndex = 24;
            this.uLabelVendor.Text = "ultraLabel11";
            // 
            // uTextEquipGroupName
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Appearance = appearance8;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(716, 100);
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.ReadOnly = true;
            this.uTextEquipGroupName.Size = new System.Drawing.Size(160, 21);
            this.uTextEquipGroupName.TabIndex = 23;
            // 
            // uLabelEquipGroupName
            // 
            this.uLabelEquipGroupName.Location = new System.Drawing.Point(612, 100);
            this.uLabelEquipGroupName.Name = "uLabelEquipGroupName";
            this.uLabelEquipGroupName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroupName.TabIndex = 22;
            this.uLabelEquipGroupName.Text = "ultraLabel6";
            // 
            // uTextEquipType
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Appearance = appearance9;
            this.uTextEquipType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Location = new System.Drawing.Point(408, 100);
            this.uTextEquipType.Name = "uTextEquipType";
            this.uTextEquipType.ReadOnly = true;
            this.uTextEquipType.Size = new System.Drawing.Size(160, 21);
            this.uTextEquipType.TabIndex = 21;
            // 
            // uLabelEquipType
            // 
            this.uLabelEquipType.Location = new System.Drawing.Point(304, 100);
            this.uLabelEquipType.Name = "uLabelEquipType";
            this.uLabelEquipType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipType.TabIndex = 20;
            this.uLabelEquipType.Text = "ultraLabel7";
            // 
            // uTextSerialNo
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance10;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(116, 100);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(160, 21);
            this.uTextSerialNo.TabIndex = 19;
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(12, 100);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSerialNo.TabIndex = 18;
            this.uLabelSerialNo.Text = "ultraLabel8";
            // 
            // uTextModel
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance11;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(716, 76);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(160, 21);
            this.uTextModel.TabIndex = 17;
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(612, 76);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(100, 20);
            this.uLabelModel.TabIndex = 16;
            this.uLabelModel.Text = "ultraLabel5";
            // 
            // uTextEquipProcess
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcess.Appearance = appearance12;
            this.uTextEquipProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcess.Location = new System.Drawing.Point(408, 76);
            this.uTextEquipProcess.Name = "uTextEquipProcess";
            this.uTextEquipProcess.ReadOnly = true;
            this.uTextEquipProcess.Size = new System.Drawing.Size(160, 21);
            this.uTextEquipProcess.TabIndex = 15;
            // 
            // uLabelEquipProcess
            // 
            this.uLabelEquipProcess.Location = new System.Drawing.Point(304, 76);
            this.uLabelEquipProcess.Name = "uLabelEquipProcess";
            this.uLabelEquipProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipProcess.TabIndex = 14;
            this.uLabelEquipProcess.Text = "ultraLabel4";
            // 
            // uTextLocation
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Appearance = appearance13;
            this.uTextLocation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Location = new System.Drawing.Point(116, 76);
            this.uTextLocation.Name = "uTextLocation";
            this.uTextLocation.ReadOnly = true;
            this.uTextLocation.Size = new System.Drawing.Size(160, 21);
            this.uTextLocation.TabIndex = 13;
            // 
            // uLabelLocation
            // 
            this.uLabelLocation.Location = new System.Drawing.Point(12, 76);
            this.uLabelLocation.Name = "uLabelLocation";
            this.uLabelLocation.Size = new System.Drawing.Size(100, 20);
            this.uLabelLocation.TabIndex = 12;
            this.uLabelLocation.Text = "ultraLabel3";
            // 
            // uTextStation
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Appearance = appearance14;
            this.uTextStation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Location = new System.Drawing.Point(716, 52);
            this.uTextStation.Name = "uTextStation";
            this.uTextStation.ReadOnly = true;
            this.uTextStation.Size = new System.Drawing.Size(160, 21);
            this.uTextStation.TabIndex = 11;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(612, 52);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 10;
            this.uLabelStation.Text = "ultraLabel2";
            // 
            // uTextArea
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Appearance = appearance15;
            this.uTextArea.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Location = new System.Drawing.Point(408, 52);
            this.uTextArea.Name = "uTextArea";
            this.uTextArea.ReadOnly = true;
            this.uTextArea.Size = new System.Drawing.Size(160, 21);
            this.uTextArea.TabIndex = 9;
            // 
            // uLabelArea
            // 
            this.uLabelArea.Location = new System.Drawing.Point(304, 52);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelArea.TabIndex = 8;
            this.uLabelArea.Text = "ultraLabel2";
            // 
            // uTextSuper
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuper.Appearance = appearance16;
            this.uTextSuper.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuper.Location = new System.Drawing.Point(116, 52);
            this.uTextSuper.Name = "uTextSuper";
            this.uTextSuper.ReadOnly = true;
            this.uTextSuper.Size = new System.Drawing.Size(160, 21);
            this.uTextSuper.TabIndex = 7;
            // 
            // uLabelSuper
            // 
            this.uLabelSuper.Location = new System.Drawing.Point(12, 52);
            this.uLabelSuper.Name = "uLabelSuper";
            this.uLabelSuper.Size = new System.Drawing.Size(100, 20);
            this.uLabelSuper.TabIndex = 6;
            this.uLabelSuper.Text = "ultraLabel2";
            // 
            // uTextEquipName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance4;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(716, 28);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(160, 21);
            this.uTextEquipName.TabIndex = 5;
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(612, 28);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipName.TabIndex = 4;
            this.uLabelEquipName.Text = "ultraLabel1";
            // 
            // uTextEquipCode
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.Appearance = appearance18;
            this.uTextEquipCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance19;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(408, 28);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(160, 19);
            this.uTextEquipCode.TabIndex = 3;
            this.uTextEquipCode.TextChanged += new System.EventHandler(this.uTextEquipCode_TextChanged);
            this.uTextEquipCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyUp);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(304, 28);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCode.TabIndex = 2;
            this.uLabelEquipCode.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(160, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uDateRepairTime);
            this.uGroupBox2.Controls.Add(this.uLabelRepairTime);
            this.uGroupBox2.Controls.Add(this.uTextRequestReason);
            this.uGroupBox2.Controls.Add(this.uLabelRequestReason);
            this.uGroupBox2.Controls.Add(this.uTextPKGType);
            this.uGroupBox2.Controls.Add(this.uLabelPKGType);
            this.uGroupBox2.Controls.Add(this.uTextProductName);
            this.uGroupBox2.Controls.Add(this.uTextProductCode);
            this.uGroupBox2.Controls.Add(this.uLabelProduct);
            this.uGroupBox2.Controls.Add(this.uCheckEmerHandle);
            this.uGroupBox2.Controls.Add(this.uDateFailureTime);
            this.uGroupBox2.Controls.Add(this.uLabelFailureTime);
            this.uGroupBox2.Controls.Add(this.uDateFailureDay);
            this.uGroupBox2.Controls.Add(this.uLabelFailureDay);
            this.uGroupBox2.Controls.Add(this.uTextRepairUserName);
            this.uGroupBox2.Controls.Add(this.uTextRepairUserID);
            this.uGroupBox2.Controls.Add(this.uLabelRepairUser);
            this.uGroupBox2.Controls.Add(this.uDateRepairDate);
            this.uGroupBox2.Controls.Add(this.uLabelRepairDate);
            this.uGroupBox2.Location = new System.Drawing.Point(0, 235);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1060, 233);
            this.uGroupBox2.TabIndex = 2;
            // 
            // uDateRepairTime
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairTime.Appearance = appearance28;
            this.uDateRepairTime.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairTime.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateRepairTime.ButtonsRight.Add(spinEditorButton1);
            this.uDateRepairTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateRepairTime.Location = new System.Drawing.Point(500, 27);
            this.uDateRepairTime.MaskInput = "hh:mm:ss";
            this.uDateRepairTime.Name = "uDateRepairTime";
            this.uDateRepairTime.Size = new System.Drawing.Size(100, 19);
            this.uDateRepairTime.TabIndex = 50;
            this.uDateRepairTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uDateTime_EditorSpinButtonClick);
            // 
            // uLabelRepairTime
            // 
            this.uLabelRepairTime.Location = new System.Drawing.Point(396, 27);
            this.uLabelRepairTime.Name = "uLabelRepairTime";
            this.uLabelRepairTime.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairTime.TabIndex = 49;
            this.uLabelRepairTime.Text = "ultraLabel13";
            // 
            // uTextRequestReason
            // 
            this.uTextRequestReason.Location = new System.Drawing.Point(116, 125);
            this.uTextRequestReason.Name = "uTextRequestReason";
            this.uTextRequestReason.Size = new System.Drawing.Size(588, 21);
            this.uTextRequestReason.TabIndex = 48;
            // 
            // uLabelRequestReason
            // 
            this.uLabelRequestReason.Location = new System.Drawing.Point(12, 125);
            this.uLabelRequestReason.Name = "uLabelRequestReason";
            this.uLabelRequestReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestReason.TabIndex = 47;
            this.uLabelRequestReason.Text = "ultraLabel13";
            // 
            // uTextPKGType
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPKGType.Appearance = appearance17;
            this.uTextPKGType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPKGType.Location = new System.Drawing.Point(500, 101);
            this.uTextPKGType.Name = "uTextPKGType";
            this.uTextPKGType.ReadOnly = true;
            this.uTextPKGType.Size = new System.Drawing.Size(204, 21);
            this.uTextPKGType.TabIndex = 46;
            // 
            // uLabelPKGType
            // 
            this.uLabelPKGType.Location = new System.Drawing.Point(396, 101);
            this.uLabelPKGType.Name = "uLabelPKGType";
            this.uLabelPKGType.Size = new System.Drawing.Size(100, 20);
            this.uLabelPKGType.TabIndex = 45;
            this.uLabelPKGType.Text = "ultraLabel13";
            // 
            // uTextProductName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance5;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(220, 101);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(100, 21);
            this.uTextProductName.TabIndex = 44;
            // 
            // uTextProductCode
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProductCode.Appearance = appearance1;
            this.uTextProductCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProductCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance2;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextProductCode.ButtonsRight.Add(editorButton2);
            this.uTextProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProductCode.Location = new System.Drawing.Point(116, 101);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.Size = new System.Drawing.Size(100, 19);
            this.uTextProductCode.TabIndex = 43;
            this.uTextProductCode.TextChanged += new System.EventHandler(this.uTextProductCode_TextChanged);
            this.uTextProductCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.uTextProductCode_KeyUp);
            this.uTextProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextProductCode_EditorButtonClick);
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(12, 101);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelProduct.TabIndex = 42;
            this.uLabelProduct.Text = "ultraLabel1";
            // 
            // uCheckEmerHandle
            // 
            this.uCheckEmerHandle.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uCheckEmerHandle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckEmerHandle.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckEmerHandle.Location = new System.Drawing.Point(608, 77);
            this.uCheckEmerHandle.Name = "uCheckEmerHandle";
            this.uCheckEmerHandle.Size = new System.Drawing.Size(72, 20);
            this.uCheckEmerHandle.TabIndex = 41;
            this.uCheckEmerHandle.Text = "긴급처리";
            this.uCheckEmerHandle.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateFailureTime
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFailureTime.Appearance = appearance20;
            this.uDateFailureTime.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFailureTime.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateFailureTime.ButtonsRight.Add(spinEditorButton2);
            this.uDateFailureTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFailureTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateFailureTime.Location = new System.Drawing.Point(500, 77);
            this.uDateFailureTime.MaskInput = "hh:mm:ss";
            this.uDateFailureTime.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.uDateFailureTime.Name = "uDateFailureTime";
            this.uDateFailureTime.Size = new System.Drawing.Size(100, 19);
            this.uDateFailureTime.TabIndex = 39;
            this.uDateFailureTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uDateTime_EditorSpinButtonClick);
            // 
            // uLabelFailureTime
            // 
            this.uLabelFailureTime.Location = new System.Drawing.Point(396, 77);
            this.uLabelFailureTime.Name = "uLabelFailureTime";
            this.uLabelFailureTime.Size = new System.Drawing.Size(100, 20);
            this.uLabelFailureTime.TabIndex = 38;
            this.uLabelFailureTime.Text = "ultraLabel13";
            // 
            // uDateFailureDay
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFailureDay.Appearance = appearance27;
            this.uDateFailureDay.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFailureDay.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateFailureDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFailureDay.Location = new System.Drawing.Point(116, 78);
            this.uDateFailureDay.Name = "uDateFailureDay";
            this.uDateFailureDay.Size = new System.Drawing.Size(100, 19);
            this.uDateFailureDay.TabIndex = 37;
            // 
            // uLabelFailureDay
            // 
            this.uLabelFailureDay.Location = new System.Drawing.Point(12, 77);
            this.uLabelFailureDay.Name = "uLabelFailureDay";
            this.uLabelFailureDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelFailureDay.TabIndex = 36;
            this.uLabelFailureDay.Text = "ultraLabel13";
            // 
            // uTextRepairUserName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Appearance = appearance22;
            this.uTextRepairUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Location = new System.Drawing.Point(604, 52);
            this.uTextRepairUserName.Name = "uTextRepairUserName";
            this.uTextRepairUserName.ReadOnly = true;
            this.uTextRepairUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserName.TabIndex = 35;
            // 
            // uTextRepairUserID
            // 
            appearance23.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairUserID.Appearance = appearance23;
            this.uTextRepairUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairUserID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.Image = ((object)(resources.GetObject("appearance24.Image")));
            appearance24.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance24;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairUserID.ButtonsRight.Add(editorButton3);
            this.uTextRepairUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepairUserID.Location = new System.Drawing.Point(500, 52);
            this.uTextRepairUserID.Name = "uTextRepairUserID";
            this.uTextRepairUserID.Size = new System.Drawing.Size(100, 19);
            this.uTextRepairUserID.TabIndex = 34;
            this.uTextRepairUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRepairUserID_EditorButtonClick);
            // 
            // uLabelRepairUser
            // 
            this.uLabelRepairUser.Location = new System.Drawing.Point(396, 52);
            this.uLabelRepairUser.Name = "uLabelRepairUser";
            this.uLabelRepairUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairUser.TabIndex = 33;
            this.uLabelRepairUser.Text = "ultraLabel1";
            // 
            // uDateRepairDate
            // 
            appearance25.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairDate.Appearance = appearance25;
            this.uDateRepairDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRepairDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateRepairDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairDate.Location = new System.Drawing.Point(116, 28);
            this.uDateRepairDate.Name = "uDateRepairDate";
            this.uDateRepairDate.Size = new System.Drawing.Size(100, 19);
            this.uDateRepairDate.TabIndex = 32;
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairDate.TabIndex = 31;
            this.uLabelRepairDate.Text = "ultraLabel13";
            // 
            // frmEQU0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0008";
            this.Load += new System.EventHandler(this.frmEQU0008_Load);
            this.Activated += new System.EventHandler(this.frmEQU0008_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipRating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPKGType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEmerHandle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFailureTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFailureDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSuper;
        private Infragistics.Win.Misc.UltraLabel uLabelSuper;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipRating;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipRating;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductYear;
        private Infragistics.Win.Misc.UltraLabel uLabelProductYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSTSStock;
        private Infragistics.Win.Misc.UltraLabel uLabelSTSStock;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLocation;
        private Infragistics.Win.Misc.UltraLabel uLabelLocation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStation;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextArea;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFailureTime;
        private Infragistics.Win.Misc.UltraLabel uLabelFailureTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFailureDay;
        private Infragistics.Win.Misc.UltraLabel uLabelFailureDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUser;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEmerHandle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRequestReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPKGType;
        private Infragistics.Win.Misc.UltraLabel uLabelPKGType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairTime;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairTime;
    }
}