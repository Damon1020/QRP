﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQUZ0002.cs                                        */
/* 프로그램명   : 기초재고등록                                          */
/* 작성자       : 권종구                                               */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0014 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQUZ0014()
        {
            InitializeComponent();
        }
        //활성화시
        private void frmEQUZ0014_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        //로드
        private void frmEQUZ0014_Load(object sender, EventArgs e)
        {
            
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitText();
            InitLable();
            InitGrid();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 타이틀초기화
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("기초재고등록", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextMoveChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextMoveChargeName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLable()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMoveDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMoveChargeID, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelInventory, "입고창고", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //컬럼설정

                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "SparePartCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "SparePartName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50, Infragistics.Win.HAlign.Left
                    ,Infragistics.Win.VAlign.Middle,Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"","","");

                grd.mfSetGridColumn(this.uGrid1, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100, Infragistics.Win.HAlign.Center,
                   Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                grd.mfSetGridColumn(this.uGrid1, 0, "Qty", "현재고량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");



                //grd.mfSetGridColumn(this.uGrid1, 0, "SparePartNameCh", "SparePart명_중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "SparePartNameEn", "SparePart명_영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Vendor", "Vendor", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Model", "모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Min", "최소재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Right,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Max", "최대재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Right
                //    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadUnit("", m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGrid1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant,true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                //this.uComboInventory.Text = "선택";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
#endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtSPStock = new DataTable();
                DataTable dtSPStockMoveHist = new DataTable();
                DataTable dtSaveType = new DataTable();
                DataTable dtDelType = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();
                int intRowCheck = 0;


                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uComboInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000870", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboInventory.DropDown();
                    return;
                }

                if (this.uTextMoveChargeID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000865", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextMoveChargeID.Focus();
                    return;
                }

                if (this.uTextMoveChargeName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000865", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextMoveChargeID.Focus();
                    return;
                }
                
                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                if (!this.uGrid1.Enabled)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001051", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                string strLang = m_resSys.GetString("SYS_LANG");


                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;

                            if (this.uGrid1.Rows[i].Cells["Qty"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                                , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000510",strLang)
                                                , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["Qty"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                                , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000499",strLang)
                                                , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["UnitCode"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }
                        //}
                    }
                }


                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001051", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }


                DialogResult dir = new DialogResult();


                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);


                
                if (dir == DialogResult.No)
                {
                    return;
                }


                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();


                //-------- 1.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                dtSPStock = clsSPStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtSPStock.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uComboInventory.Value.ToString();
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["Qty"] = this.uGrid1.Rows[i].Cells["Qty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStock.Rows.Add(row);
                        }
                    }
                }


                //-------- 2.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                dtSPStockMoveHist = clsSPStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtSPStockMoveHist.NewRow();

                            row["MoveGubunCode"] = "M01";       //기초재고일 경우는 "M01"
                            row["DocCode"] = "";
                            row["MoveDate"] = this.uDateMoveDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["MoveChargeID"] = this.uTextMoveChargeID.Text;
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["SPInventoryCode"] = this.uComboInventory.Value.ToString();
                            row["EquipCode"] = "";
                            row["SparePartCode"] = this.uGrid1.Rows[i].Cells["SparePartCode"].Value.ToString();
                            row["MoveQty"] = this.uGrid1.Rows[i].Cells["Qty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtSPStockMoveHist.Rows.Add(row);
                        }
                    }
                }
                


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 처리 로직 //
                // 저장함수 호출

                //-------- BL 호출 : SparePart 재고이동 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SAVESparePartStock), "SAVESparePartStock");
                QRPEQU.BL.EQUSPA.SAVESparePartStock clsSAVESparePartStock = new QRPEQU.BL.EQUSPA.SAVESparePartStock();
                brwChannel.mfCredentials(clsSAVESparePartStock);
                string rtMSG = clsSAVESparePartStock.mfSaveBasicStock(dtSPStock, dtSPStockMoveHist, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                   
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }

                
            


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                InitCombo();                
                InitLable();
                InitText();
                InitGrid();

                this.uGrid1.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGrid1);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
#endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQUZ0014_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uComboPlant_AfterCloseUp(object sender, EventArgs e)
        {
            
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitText();
                if (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGrid1.Rows.All);
                    this.uGrid1.DeleteSelectedRows(false);
                }

                this.uComboInventory.Items.Clear();

                string strPlantCode = this.uComboPlant.Value.ToString();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                // Call Method
                DataTable dtSPStock = clsSPStock.mfReadMASSparePart_InputSPStock(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtSPStock;
                uGrid1.DataBind();

                if (dtSPStock.Rows.Count > 0)
                {
                    uGrid1.Enabled = true;
                }
                else
                {
                    uGrid1.Enabled = false;
                }

                if (strPlantCode == "")
                    return;

                ////////----------------- SparePart 창고 콤보 불러오기------------/////////////
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);

                // Call Method
                DataTable dtSPInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                

                wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "SPInventoryCode", "SPInventoryName", dtSPInventory);



                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextMoveChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strUserID = this.uTextMoveChargeID.Text;
                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextMoveChargeID;
                uTextName = this.uTextMoveChargeName;

                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {
                    

                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextMoveChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextMoveChargeID.Text = frmPOP.UserID;
                this.uTextMoveChargeName.Text = frmPOP.UserName;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmEQUZ0014_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void mfSearchSPInfo()
        {
            try
            {
                InitText();
                InitGrid();

                string strPlantCode = this.uComboPlant.Value.ToString();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                // Call Method
                DataTable dtSPStock = clsSPStock.mfReadMASSparePart_InputSPStock(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtSPStock;
                uGrid1.DataBind();

                if (dtSPStock.Rows.Count > 0)
                {
                    uGrid1.Enabled = true;
                }
                else
                {
                    uGrid1.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
