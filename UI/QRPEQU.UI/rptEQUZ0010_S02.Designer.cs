﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S02.
    /// </summary>
    partial class rptEQUZ0010_S02
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptEQUZ0010_S02));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textSpecStandard = new DataDynamics.ActiveReports.TextBox();
            this.textWorkCondition = new DataDynamics.ActiveReports.TextBox();
            this.textParameter = new DataDynamics.ActiveReports.TextBox();
            this.groupHeader = new DataDynamics.ActiveReports.GroupHeader();
            this.label18 = new DataDynamics.ActiveReports.Label();
            this.label22 = new DataDynamics.ActiveReports.Label();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.groupFooter = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textSpecStandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWorkCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textParameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textSpecStandard,
            this.textWorkCondition,
            this.textParameter});
            this.detail.Height = 0.5F;
            this.detail.Name = "detail";
            // 
            // textSpecStandard
            // 
            this.textSpecStandard.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpecStandard.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpecStandard.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpecStandard.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpecStandard.DataField = "SpecStandard";
            this.textSpecStandard.Height = 0.5F;
            this.textSpecStandard.Left = 0.03000045F;
            this.textSpecStandard.Name = "textSpecStandard";
            this.textSpecStandard.Style = "font-size: 7.5pt; text-align: left; vertical-align: middle";
            this.textSpecStandard.Text = null;
            this.textSpecStandard.Top = 0F;
            this.textSpecStandard.Width = 1.831F;
            // 
            // textWorkCondition
            // 
            this.textWorkCondition.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textWorkCondition.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textWorkCondition.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textWorkCondition.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textWorkCondition.DataField = "WorkCondition";
            this.textWorkCondition.Height = 0.5F;
            this.textWorkCondition.Left = 4.604F;
            this.textWorkCondition.Name = "textWorkCondition";
            this.textWorkCondition.Style = "font-size: 7.5pt; text-align: left; vertical-align: middle";
            this.textWorkCondition.Text = null;
            this.textWorkCondition.Top = 0F;
            this.textWorkCondition.Width = 2.553F;
            // 
            // textParameter
            // 
            this.textParameter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.DataField = "Parameter";
            this.textParameter.Height = 0.5F;
            this.textParameter.Left = 1.861F;
            this.textParameter.Name = "textParameter";
            this.textParameter.Style = "font-size: 7.5pt; text-align: left; vertical-align: middle";
            this.textParameter.Text = null;
            this.textParameter.Top = 0F;
            this.textParameter.Width = 2.743F;
            // 
            // groupHeader
            // 
            this.groupHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label18,
            this.label22,
            this.label1,
            this.label2});
            this.groupHeader.Height = 0.543F;
            this.groupHeader.Name = "groupHeader";
            // 
            // label18
            // 
            this.label18.Height = 0.2F;
            this.label18.HyperLink = null;
            this.label18.Left = 0.02999952F;
            this.label18.Name = "label18";
            this.label18.Style = "font-weight: bold";
            this.label18.Text = "4 . 작업조건";
            this.label18.Top = 0.07300011F;
            this.label18.Width = 2.134F;
            // 
            // label22
            // 
            this.label22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Height = 0.2F;
            this.label22.HyperLink = null;
            this.label22.Left = 1.860999F;
            this.label22.Name = "label22";
            this.label22.Style = "font-weight: bold; text-align: center";
            this.label22.Text = "Parameter";
            this.label22.Top = 0.343F;
            this.label22.Width = 2.743001F;
            // 
            // label1
            // 
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.03F;
            this.label1.Name = "label1";
            this.label1.Style = "font-weight: bold; text-align: center";
            this.label1.Text = "항목";
            this.label1.Top = 0.343F;
            this.label1.Width = 1.831F;
            // 
            // label2
            // 
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 4.604F;
            this.label2.Name = "label2";
            this.label2.Style = "font-weight: bold; text-align: center";
            this.label2.Text = "Spec";
            this.label2.Top = 0.343F;
            this.label2.Width = 2.553F;
            // 
            // groupFooter
            // 
            this.groupFooter.Height = 0F;
            this.groupFooter.Name = "groupFooter";
            // 
            // rptEQUZ0010_S02
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.184F;
            this.Sections.Add(this.groupHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textSpecStandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWorkCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textParameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.GroupHeader groupHeader;
        private DataDynamics.ActiveReports.GroupFooter groupFooter;
        private DataDynamics.ActiveReports.TextBox textSpecStandard;
        private DataDynamics.ActiveReports.TextBox textWorkCondition;
        private DataDynamics.ActiveReports.TextBox textParameter;
        private DataDynamics.ActiveReports.Label label18;
        private DataDynamics.ActiveReports.Label label22;
        private DataDynamics.ActiveReports.Label label1;
        private DataDynamics.ActiveReports.Label label2;
    }
}
