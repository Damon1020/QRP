﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0008));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearch = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextSearchUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipMentGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipMentGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearch1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearch1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipMentGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearch1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearch);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipMentGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipMentGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearch1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearch1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uDateSearch
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearch.Appearance = appearance4;
            this.uDateSearch.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearch.Location = new System.Drawing.Point(116, 12);
            this.uDateSearch.Name = "uDateSearch";
            this.uDateSearch.Size = new System.Drawing.Size(100, 21);
            this.uDateSearch.TabIndex = 17;
            // 
            // uTextSearchUserName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchUserName.Appearance = appearance3;
            this.uTextSearchUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchUserName.Location = new System.Drawing.Point(732, 35);
            this.uTextSearchUserName.Name = "uTextSearchUserName";
            this.uTextSearchUserName.ReadOnly = true;
            this.uTextSearchUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchUserName.TabIndex = 16;
            // 
            // uTextSearchUserID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchUserID.Appearance = appearance2;
            this.uTextSearchUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchUserID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance5;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchUserID.ButtonsRight.Add(editorButton1);
            this.uTextSearchUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchUserID.Location = new System.Drawing.Point(628, 35);
            this.uTextSearchUserID.Name = "uTextSearchUserID";
            this.uTextSearchUserID.Size = new System.Drawing.Size(100, 19);
            this.uTextSearchUserID.TabIndex = 15;
            // 
            // uLabelSearchUser
            // 
            this.uLabelSearchUser.Location = new System.Drawing.Point(524, 35);
            this.uLabelSearchUser.Name = "uLabelSearchUser";
            this.uLabelSearchUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchUser.TabIndex = 14;
            this.uLabelSearchUser.Text = "ultraLabel3";
            // 
            // uComboSearchEquipMentGroup
            // 
            this.uComboSearchEquipMentGroup.Location = new System.Drawing.Point(628, 12);
            this.uComboSearchEquipMentGroup.Name = "uComboSearchEquipMentGroup";
            this.uComboSearchEquipMentGroup.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchEquipMentGroup.TabIndex = 13;
            this.uComboSearchEquipMentGroup.Text = "ultraComboEditor2";
            // 
            // uLabelSearchEquipMentGroup
            // 
            this.uLabelSearchEquipMentGroup.Location = new System.Drawing.Point(524, 12);
            this.uLabelSearchEquipMentGroup.Name = "uLabelSearchEquipMentGroup";
            this.uLabelSearchEquipMentGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipMentGroup.TabIndex = 12;
            this.uLabelSearchEquipMentGroup.Text = "ultraLabel2";
            // 
            // uComboSearch1
            // 
            this.uComboSearch1.Location = new System.Drawing.Point(372, 35);
            this.uComboSearch1.Name = "uComboSearch1";
            this.uComboSearch1.Size = new System.Drawing.Size(144, 21);
            this.uComboSearch1.TabIndex = 11;
            this.uComboSearch1.Text = "ultraComboEditor2";
            // 
            // uLabelSearch1
            // 
            this.uLabelSearch1.Location = new System.Drawing.Point(268, 35);
            this.uLabelSearch1.Name = "uLabelSearch1";
            this.uLabelSearch1.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearch1.TabIndex = 10;
            this.uLabelSearch1.Text = "ultraLabel2";
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(116, 35);
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchStation.TabIndex = 9;
            this.uComboSearchStation.Text = "ultraComboEditor1";
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(12, 35);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 8;
            this.uLabelSearchStation.Text = "ultraLabel1";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(884, 12);
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchArea.TabIndex = 7;
            this.uComboSearchArea.Text = "ultraComboEditor2";
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(780, 12);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchArea.TabIndex = 6;
            this.uLabelSearchArea.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(372, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 5;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(268, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 2;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uGrid
            // 
            this.uGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance9;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance16.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance16;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid.Location = new System.Drawing.Point(0, 100);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1070, 720);
            this.uGrid.TabIndex = 5;
            // 
            // frmEQUZ0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0008";
            this.Load += new System.EventHandler(this.frmEQUZ0008_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0008_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipMentGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearch1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipMentGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipMentGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearch1;
        private Infragistics.Win.Misc.UltraLabel uLabelSearch1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearch;
    }
}