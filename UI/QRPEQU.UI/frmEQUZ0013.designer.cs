﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0013));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextAdmitName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAdmitID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAdmiter = new Infragistics.Win.Misc.UltraLabel();
            this.uDateAdmitDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAdmitDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCarryOutAdmit = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryOutAdmit)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDay);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(476, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 4;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(216, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(15, 15);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "~";
            // 
            // uDateToDay
            // 
            this.uDateToDay.Location = new System.Drawing.Point(232, 12);
            this.uDateToDay.Name = "uDateToDay";
            this.uDateToDay.Size = new System.Drawing.Size(100, 21);
            this.uDateToDay.TabIndex = 2;
            // 
            // uDateFromDay
            // 
            this.uDateFromDay.Location = new System.Drawing.Point(116, 12);
            this.uDateFromDay.Name = "uDateFromDay";
            this.uDateFromDay.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDay.TabIndex = 1;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(372, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uLabelSearchDay
            // 
            this.uLabelSearchDay.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDay.Name = "uLabelSearchDay";
            this.uLabelSearchDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDay.TabIndex = 0;
            this.uLabelSearchDay.Text = "ultraLabel1";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextAdmitName);
            this.uGroupBox.Controls.Add(this.uTextAdmitID);
            this.uGroupBox.Controls.Add(this.uLabelAdmiter);
            this.uGroupBox.Controls.Add(this.uDateAdmitDate);
            this.uGroupBox.Controls.Add(this.uLabelAdmitDate);
            this.uGroupBox.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 44);
            this.uGroupBox.TabIndex = 2;
            // 
            // uTextAdmitName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitName.Appearance = appearance3;
            this.uTextAdmitName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitName.Location = new System.Drawing.Point(476, 12);
            this.uTextAdmitName.Name = "uTextAdmitName";
            this.uTextAdmitName.ReadOnly = true;
            this.uTextAdmitName.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitName.TabIndex = 12;
            // 
            // uTextAdmitID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextAdmitID.Appearance = appearance2;
            this.uTextAdmitID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextAdmitID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance5;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAdmitID.ButtonsRight.Add(editorButton1);
            this.uTextAdmitID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAdmitID.Location = new System.Drawing.Point(372, 12);
            this.uTextAdmitID.MaxLength = 20;
            this.uTextAdmitID.Name = "uTextAdmitID";
            this.uTextAdmitID.Size = new System.Drawing.Size(100, 19);
            this.uTextAdmitID.TabIndex = 11;
            this.uTextAdmitID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAdmitID_KeyDown);
            this.uTextAdmitID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAdmitID_EditorButtonClick);
            // 
            // uLabelAdmiter
            // 
            this.uLabelAdmiter.Location = new System.Drawing.Point(268, 12);
            this.uLabelAdmiter.Name = "uLabelAdmiter";
            this.uLabelAdmiter.Size = new System.Drawing.Size(100, 20);
            this.uLabelAdmiter.TabIndex = 2;
            this.uLabelAdmiter.Text = "ultraLabel2";
            // 
            // uDateAdmitDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAdmitDate.Appearance = appearance4;
            this.uDateAdmitDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAdmitDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateAdmitDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateAdmitDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAdmitDate.Location = new System.Drawing.Point(116, 12);
            this.uDateAdmitDate.Name = "uDateAdmitDate";
            this.uDateAdmitDate.Size = new System.Drawing.Size(100, 19);
            this.uDateAdmitDate.TabIndex = 1;
            // 
            // uLabelAdmitDate
            // 
            this.uLabelAdmitDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelAdmitDate.Name = "uLabelAdmitDate";
            this.uLabelAdmitDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelAdmitDate.TabIndex = 0;
            this.uLabelAdmitDate.Text = "ultraLabel2";
            // 
            // uGridCarryOutAdmit
            // 
            this.uGridCarryOutAdmit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCarryOutAdmit.DisplayLayout.Appearance = appearance6;
            this.uGridCarryOutAdmit.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCarryOutAdmit.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutAdmit.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryOutAdmit.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridCarryOutAdmit.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryOutAdmit.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridCarryOutAdmit.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCarryOutAdmit.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCarryOutAdmit.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCarryOutAdmit.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridCarryOutAdmit.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCarryOutAdmit.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutAdmit.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCarryOutAdmit.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridCarryOutAdmit.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCarryOutAdmit.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryOutAdmit.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGridCarryOutAdmit.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridCarryOutAdmit.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCarryOutAdmit.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGridCarryOutAdmit.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGridCarryOutAdmit.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCarryOutAdmit.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.uGridCarryOutAdmit.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCarryOutAdmit.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCarryOutAdmit.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCarryOutAdmit.Location = new System.Drawing.Point(0, 128);
            this.uGridCarryOutAdmit.Name = "uGridCarryOutAdmit";
            this.uGridCarryOutAdmit.Size = new System.Drawing.Size(1070, 700);
            this.uGridCarryOutAdmit.TabIndex = 3;
            this.uGridCarryOutAdmit.Text = "ultraGrid1";
            this.uGridCarryOutAdmit.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutAdmit_AfterCellUpdate);
            this.uGridCarryOutAdmit.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryOutAdmit_CellChange);
            // 
            // frmEQUZ0013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCarryOutAdmit);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0013";
            this.Load += new System.EventHandler(this.frmEQUZ0013_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0013_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0013_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryOutAdmit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDay;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDay;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.Misc.UltraLabel uLabelAdmiter;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAdmitDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAdmitDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitID;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCarryOutAdmit;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}