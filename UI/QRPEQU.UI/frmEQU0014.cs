﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQU0014.cs                                         */
/* 프로그램명   : 반입확인                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-xx-xx : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQU0014 : Form, IToolbar
    {

        //리소스호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        public frmEQU0014()
        {
            InitializeComponent();
        }

        private void frmEQU0014_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0014_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // Title 제목
            titleArea.mfSetLabelText("반입확인", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitLabel();
            InitGrid();
            InitText();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                // 승인자 TextBox 사용자 ID 설정
                this.uTextCarryInID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCarryInName.Text = m_resSys.GetString("SYS_USERNAME");

                //날짜 콤보 기본값으로 변경
                this.uDateCarryInConfirm.Value = DateTime.Now;
                this.uDateFromDay.Value = DateTime.Now;
                this.uDateToDay.Value = DateTime.Now;

                //발행번호 초기화
                this.uTextDocCode.Text = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchDay, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocCode, "발행번호", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCarryInConfirm, "반입일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelConfirmID, "반입자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelConfirmID, "반입자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelItemGubun, "구성품구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInventory, "반입창고", m_resSys.GetString("SYS_FONTNAME"), true, true);

                this.uLabelInventory.Visible = false;
                this.uComboInventory.Visible = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCarryInConfirm, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Extended, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "Check", "반입", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CancelCheck", "취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CarryInSeq", "반입순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "ItemGubunCode", "구성품구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "DocCode", "발행번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "DeptName", "반출부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CarryOutChargeName", "반출담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "EquipCarryOutGubunName", "반출구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "EquipCarryInGubunName", "반입구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CarryInEstDate", "반입예정일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CarryOutReason", "반출사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "VendorName", "반출처", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "ItemCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "ItemName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CarryOutQty", "반출수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "CarryInQty", "현반입수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCarryInConfirm, 0, "GRInventoryCode", "반입창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
               

                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinComboEditor wCom = new QRPCOM.QRPUI.WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                ////설비구성품구분 콤보
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //QRPSYS.BL.SYSPGM.CommonCode clsSYS = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChannel.mfCredentials(clsSYS);

                //DataTable dtCommon = clsSYS.mfReadCommonCode("C0015", m_resSys.GetString("SYS_LANG"));

                //wCom.mfSetComboEditor(this.uComboItemGubun, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                //    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true
                //    , 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtCommon);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try 
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                WinGrid grd = new WinGrid();

                //-----------------------필수 입력사항 확인------------------//
                if (Convert.ToDateTime(this.uDateFromDay.Value) > Convert.ToDateTime(this.uDateToDay.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000774", Infragistics.Win.HAlign.Right);
                    this.uDateFromDay.DropDown();
                }

                

                //-- 검색에 필요한 값 저장 --/
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strDocCode = this.uTextDocCode.Text.Trim();
                string strFromDate = Convert.ToDateTime(this.uDateFromDay.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(this.uDateToDay.Value).ToString("yyyy-MM-dd");
                string strItemGubunCode = this.uComboItemGubun.Value.ToString();

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M001230", "M000266",
                                                            Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (strItemGubunCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M001230", "M000312",
                                                            Infragistics.Win.HAlign.Right);
                    this.uComboItemGubun.DropDown();
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //  BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryInD), "CarryInD");
                QRPEQU.BL.EQUCAR.CarryInD clsCarryInD = new QRPEQU.BL.EQUCAR.CarryInD();
                brwChannel.mfCredentials(clsCarryInD);

                //처리 로직//
                DataTable dtCarryIn = clsCarryInD.mfReadCarryInD(strPlantCode,strItemGubunCode, strDocCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //-- 데이터 바인드 --
                this.uGridCarryInConfirm.DataSource = dtCarryIn;
                this.uGridCarryInConfirm.DataBind();


                // SystemInfo ResourceSet
                WinComboEditor wCombo = new WinComboEditor();

                
                


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtCarryIn.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                            Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항
                //------------------- 필수 입력사항 확인 --------------------------//

                // 필수입력사항 확인
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uComboItemGubun.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000312", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboItemGubun.DropDown();
                    return;
                }

                // 필수입력사항 확인
                ////if (this.uComboInventory.Value.ToString() == "")
                ////{
                ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                    , "확인창", "필수입력사항 확인", "창고를 선택해주세요", Infragistics.Win.HAlign.Center);

                ////    // Focus
                ////    this.uComboInventory.DropDown();
                ////    return;
                ////}    

                if (this.uGridCarryInConfirm.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M000882", "M000421", Infragistics.Win.HAlign.Right);


                    return;
                }
                if (this.uDateCarryInConfirm.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000423", Infragistics.Win.HAlign.Right);

                    this.uDateCarryInConfirm.DropDown();
                    return;
                }

                if (this.uTextCarryInID.Text.Trim() == "" || this.uTextCarryInName.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001228", "M000424", Infragistics.Win.HAlign.Right);

                    this.uTextCarryInID.Focus();
                    return;
                }

                #endregion

                //  BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryInD), "CarryInD");
                QRPEQU.BL.EQUCAR.CarryInD clsCarryInD = new QRPEQU.BL.EQUCAR.CarryInD();
                brwChannel.mfCredentials(clsCarryInD);

                DataTable dtCarryOutAdmit = clsCarryInD.mfDataSetInfo();

                //--------그리드 줄이 0 이상일 경우 편집이미지가 null이아닌 저장한다. -----//
                if (this.uGridCarryInConfirm.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridCarryInConfirm.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridCarryInConfirm.ActiveCell = this.uGridCarryInConfirm.Rows[0].Cells[0];

                        if (this.uGridCarryInConfirm.Rows[i].RowSelectorAppearance.Image != null)
                        {

                            ////////--- 반입수량이 반출수량보다 많을 시 메세지박스를 띄어주고 다시 수정 할 수있도록 이동 시켜준다 --//
                            //////if (Convert.ToInt32(this.uGridCarryInConfirm.Rows[i].Cells["InQty"].Value) + Convert.ToInt32(this.uGridCarryInConfirm.Rows[i].Cells["CarryInQty"].Value)
                            //////    > Convert.ToInt32(this.uGridCarryInConfirm.Rows[i].Cells["CarryOutQty"].Value))
                            //////{
                            //////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //////         "확인창", "입력사항확인", "반입수량이 반출수량보다 많습니다.", Infragistics.Win.HAlign.Right);

                            //////    this.uGridCarryInConfirm.ActiveCell = this.uGridCarryInConfirm.Rows[i].Cells["InQty"];
                            //////    this.uGridCarryInConfirm.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                            //////    return;
                            //////}

                            DataRow drAdmit;
                            drAdmit = dtCarryOutAdmit.NewRow();
                            drAdmit["PlantCode"] = this.uGridCarryInConfirm.Rows[i].Cells["PlantCode"].Value.ToString();
                            drAdmit["DocCode"] = this.uGridCarryInConfirm.Rows[i].Cells["DocCode"].Value.ToString();
                            drAdmit["Seq"] = this.uGridCarryInConfirm.Rows[i].Cells["Seq"].Value.ToString();
                            drAdmit["CarryInSeq"] = this.uGridCarryInConfirm.Rows[i].Cells["CarryInSeq"].Value.ToString();
                            drAdmit["GRDate"] = Convert.ToDateTime(this.uDateCarryInConfirm.Value).ToString("yyyy-MM-dd");
                            drAdmit["GRChargeID"] = this.uTextCarryInID.Text;
                            drAdmit["InventoryCode"] = this.uGridCarryInConfirm.Rows[i].Cells["GRInventoryCode"].Value.ToString();
                            drAdmit["GRQty"] = this.uGridCarryInConfirm.Rows[i].Cells["CarryInQty"].Value.ToString();

                            //drAdmit["CarryInDate"] = Convert.ToDateTime(this.uDateCarryInConfirm.Value).ToString("yyyy-MM-dd");
                            //drAdmit["CarryInChargeID"] = this.uTextCarryInID.Text;
                            //drAdmit["CarryInQty"] = this.uGridCarryInConfirm.Rows[i].Cells["CarryInQty"].Value.ToString();

                            if (Convert.ToBoolean(this.uGridCarryInConfirm.Rows[i].Cells["Check"].Value) == true &&
                            Convert.ToBoolean(this.uGridCarryInConfirm.Rows[i].Cells["CancelCheck"].Value) == false)
                            {
                                drAdmit["GRFlag"] = "T";
                            }

                            else if (Convert.ToBoolean(this.uGridCarryInConfirm.Rows[i].Cells["Check"].Value) == false &&
                            Convert.ToBoolean(this.uGridCarryInConfirm.Rows[i].Cells["CancelCheck"].Value) == true)
                            {
                                drAdmit["GRFlag"] = "C";
                            }



                            dtCarryOutAdmit.Rows.Add(drAdmit);
                        }
                    }

                    if (dtCarryOutAdmit.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M000882", "M000421", Infragistics.Win.HAlign.Right);


                        return;
                    }

                }
        

                //-------저장 여부 메세지 보여준다 ------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000680",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //-- 팝업창을 보여준다 --//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                    // --- 커서변결 ---//
                    this.MdiParent.Cursor = Cursors.WaitCursor;



                    //처리 로직//
                    // 매서드 호출
                    string strErrRtn = clsCarryInD.mfSaveCarrInD(dtCarryOutAdmit, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //-- 처리 결과 판단 --//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    //--커서변경--//
                    this.MdiParent.Cursor = Cursors.Default;
                    //--- 팝업창을 띄운다 --//
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    // 저장 처리 여부에 따라 메세지를 보여준다.
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001135", "M001037", "M000678",
                                   Infragistics.Win.HAlign.Right);
                        //InitText();
                        mfSearch();

                    }
                    else
                    {
                        string strMeg = "";
                        string strLang = m_resSys.GetString("SYS_LANG");

                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMeg = msg.GetMessge_Text("M000682",strLang);
                        else
                            strMeg = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), strMeg,
                                    Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfDelete()
        {

        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {

        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread t1 = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridCarryInConfirm.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridCarryInConfirm);

                /////////////

                //this.MdiParent.Cursor = Cursors.Default;
                //m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region 이벤트

        // 반입자 텍스트의 입력에따라 자동 조회되거나 텍스트에 공백을 채운다.
        private void uTextCarryInID_KeyDown(object sender, KeyEventArgs e)
        {
            //---아이디를 입력 후 어떤 키를 누를 때 마다 이름이 자동생성이 되거나 지워진다
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextCarryInName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //승인자자 공장코드 저장
                    string strCarryOutAdmitID = this.uTextCarryInID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //----- 아이디 공백 확인
                    if (strCarryOutAdmitID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCarryInID.Focus();
                        return;
                    }
                    //-- 공장 확인
                    else if (strPlantCode == "" && this.uTextCarryInName.Text.Trim() != "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //-- 유저정보 가져오기 
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCarryOutAdmitID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        // 이름 텍스트에 유저 이름을 삽입한다.
                        this.uTextCarryInName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001100", "M000883", Infragistics.Win.HAlign.Right);
                        this.uTextCarryInName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //반입자EditorButtonClick 시 유저 정보 팝업 창을 띄운다. --
        private void uTextCarryInID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                //-- 상속된폼 띄우기 -- //
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

              
                // -- 상속된폼에서 선택된 정보 중 아이디와 이름을 가져와 각텍스트에 넣는다
                this.uTextCarryInID.Text = frmUser.UserID;
                this.uTextCarryInName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드에 입력후 셀체인지가 발생할 떄 편집이미지 삽입
        private void uGridCarryInConfirm_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            
            
        }

        private void uGridCarryInConfirm_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {

                if (e.Cell.Column.Key.Equals("Check"))
                {
                    //if (Convert.ToBoolean(this.uGridCarryInConfirm.Rows[e.Cell.Row.Index].Cells["Check"].Value) == true)
                    //{
                        e.Cell.Row.Cells["CancelCheck"].Value = false;
                    //}
                }

                if (e.Cell.Column.Key.Equals("CancelCheck"))
                {
                    //if (Convert.ToBoolean(this.uGridCarryInConfirm.Rows[e.Cell.Row.Index].Cells["CancelCheck"].Value) == true)
                    //{
                        e.Cell.Row.Cells["Check"].Value = false;
                    //}
                }

                if (Convert.ToBoolean(this.uGridCarryInConfirm.Rows[e.Cell.Row.Index].Cells["Check"].Value) == false &&
                    Convert.ToBoolean(this.uGridCarryInConfirm.Rows[e.Cell.Row.Index].Cells["CancelCheck"].Value) == false)
                {
                    e.Cell.Row.RowSelectorAppearance.Image = null;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //그리드에 편집일 일어나면 발생 편집이미지 삽입
        private void uGridCarryInConfirm_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }

                

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridCarryInConfirm, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0014_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        #endregion

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strItemGubunCode = this.uComboItemGubun.Value.ToString();
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid grd = new WinGrid();

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                if (strPlantCode == "")
                    return;

                if (strItemGubunCode == "")
                    return;


                this.uComboItemGubun.Items.Clear();

                //설비구성품구분 콤보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsSYS = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsSYS);

                DataTable dtCommon = clsSYS.mfReadCommonCode("C0015", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboItemGubun, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true
                    , 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtCommon);
               
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboItemGubun_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid grd = new WinGrid();

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();


                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strItemGubunCode = this.uComboItemGubun.Value.ToString();

                if (strPlantCode == "")
                    return;

                if (strItemGubunCode == "")
                    return;

                ////////----------------- 창고 콤보 불러오기------------/////////////
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);
                DataTable dtInventory = new DataTable();

                // Call Method

                if (strItemGubunCode == "SP")
                {
                    dtInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "SPInventoryCode", "SPInventoryName", dtInventory);
                }
                else if (strItemGubunCode == "TL")
                {
                    dtInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "DurableInventoryCode", "DurableInventoryName", dtInventory);
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

       
    }
}
