﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_01.
    /// </summary>
    partial class rptEQUZ0002_Main
    {
        private DataDynamics.ActiveReports.PageHeader pageHeader;
        private DataDynamics.ActiveReports.Detail detail;
        private DataDynamics.ActiveReports.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEQUZ0002_Main));
            this.pageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.crossSectionLine2 = new DataDynamics.ActiveReports.CrossSectionLine();
            this.crossSectionLine1 = new DataDynamics.ActiveReports.CrossSectionLine();
            this.line6 = new DataDynamics.ActiveReports.Line();
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.subReport = new DataDynamics.ActiveReports.SubReport();
            this.subReport1 = new DataDynamics.ActiveReports.SubReport();
            this.subReport2 = new DataDynamics.ActiveReports.SubReport();
            this.pageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.line7 = new DataDynamics.ActiveReports.Line();
            this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
            this.labelTitle = new DataDynamics.ActiveReports.Label();
            this.oleObject1 = new DataDynamics.ActiveReports.OleObject();
            this.labelTitleProcGubun = new DataDynamics.ActiveReports.Label();
            this.label3 = new DataDynamics.ActiveReports.Label();
            this.label4 = new DataDynamics.ActiveReports.Label();
            this.oleObject2 = new DataDynamics.ActiveReports.OleObject();
            this.label5 = new DataDynamics.ActiveReports.Label();
            this.oleObject3 = new DataDynamics.ActiveReports.OleObject();
            this.textDept = new DataDynamics.ActiveReports.TextBox();
            this.textBox2 = new DataDynamics.ActiveReports.TextBox();
            this.textCerReqName = new DataDynamics.ActiveReports.TextBox();
            this.line1 = new DataDynamics.ActiveReports.Line();
            this.label18 = new DataDynamics.ActiveReports.Label();
            this.label19 = new DataDynamics.ActiveReports.Label();
            this.label20 = new DataDynamics.ActiveReports.Label();
            this.label21 = new DataDynamics.ActiveReports.Label();
            this.label22 = new DataDynamics.ActiveReports.Label();
            this.line8 = new DataDynamics.ActiveReports.Line();
            this.line9 = new DataDynamics.ActiveReports.Line();
            this.line10 = new DataDynamics.ActiveReports.Line();
            this.line11 = new DataDynamics.ActiveReports.Line();
            this.line12 = new DataDynamics.ActiveReports.Line();
            this.line14 = new DataDynamics.ActiveReports.Line();
            this.line15 = new DataDynamics.ActiveReports.Line();
            this.line16 = new DataDynamics.ActiveReports.Line();
            this.line17 = new DataDynamics.ActiveReports.Line();
            this.line18 = new DataDynamics.ActiveReports.Line();
            this.textProc = new DataDynamics.ActiveReports.TextBox();
            this.txtPlantCode = new DataDynamics.ActiveReports.TextBox();
            this.txtDocCode = new DataDynamics.ActiveReports.TextBox();
            this.txtEquipCode = new DataDynamics.ActiveReports.TextBox();
            this.txtEquipCertiReqTypeCode = new DataDynamics.ActiveReports.TextBox();
            this.txtCertiReqID = new DataDynamics.ActiveReports.TextBox();
            this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
            this.oleObject4 = new DataDynamics.ActiveReports.OleObject();
            this.label13 = new DataDynamics.ActiveReports.Label();
            this.label14 = new DataDynamics.ActiveReports.Label();
            this.label15 = new DataDynamics.ActiveReports.Label();
            this.label16 = new DataDynamics.ActiveReports.Label();
            this.line3 = new DataDynamics.ActiveReports.Line();
            this.label17 = new DataDynamics.ActiveReports.Label();
            this.label23 = new DataDynamics.ActiveReports.Label();
            this.label24 = new DataDynamics.ActiveReports.Label();
            this.label25 = new DataDynamics.ActiveReports.Label();
            this.label26 = new DataDynamics.ActiveReports.Label();
            this.label27 = new DataDynamics.ActiveReports.Label();
            this.line13 = new DataDynamics.ActiveReports.Line();
            this.line19 = new DataDynamics.ActiveReports.Line();
            this.line20 = new DataDynamics.ActiveReports.Line();
            this.line21 = new DataDynamics.ActiveReports.Line();
            this.line22 = new DataDynamics.ActiveReports.Line();
            this.line23 = new DataDynamics.ActiveReports.Line();
            this.line24 = new DataDynamics.ActiveReports.Line();
            this.line25 = new DataDynamics.ActiveReports.Line();
            this.line26 = new DataDynamics.ActiveReports.Line();
            this.line27 = new DataDynamics.ActiveReports.Line();
            this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
            this.chbEquipCertiReqTypeCode06 = new DataDynamics.ActiveReports.CheckBox();
            this.chbEquipCertiReqTypeCode05 = new DataDynamics.ActiveReports.CheckBox();
            this.chbEquipCertiReqTypeCode04 = new DataDynamics.ActiveReports.CheckBox();
            this.chbEquipCertiReqTypeCode03 = new DataDynamics.ActiveReports.CheckBox();
            this.chbEquipCertiReqTypeCode02 = new DataDynamics.ActiveReports.CheckBox();
            this.chbEquipCertiReqTypeCode01 = new DataDynamics.ActiveReports.CheckBox();
            this.line2 = new DataDynamics.ActiveReports.Line();
            this.label7 = new DataDynamics.ActiveReports.Label();
            this.label8 = new DataDynamics.ActiveReports.Label();
            this.labelModelTitle = new DataDynamics.ActiveReports.Label();
            this.label10 = new DataDynamics.ActiveReports.Label();
            this.labelSerial = new DataDynamics.ActiveReports.Label();
            this.labelPkgType = new DataDynamics.ActiveReports.Label();
            this.label6 = new DataDynamics.ActiveReports.Label();
            this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            this.textBox1 = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.labelTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTitleProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCerReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textProc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipCertiReqTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCertiReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModelTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSerial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPkgType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.crossSectionLine2,
            this.crossSectionLine1,
            this.line6});
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0.1874999F;
            this.crossSectionLine2.Left = 0F;
            this.crossSectionLine2.LineWeight = 2F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 0F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0.1875F;
            this.crossSectionLine1.Left = 7.294F;
            this.crossSectionLine1.LineWeight = 2F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0F;
            // 
            // line6
            // 
            this.line6.Height = 0F;
            this.line6.Left = 0F;
            this.line6.LineWeight = 2F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 7.292F;
            this.line6.X1 = 0F;
            this.line6.X2 = 7.292F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0F;
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.subReport,
            this.subReport1,
            this.subReport2});
            this.detail.Height = 1.052583F;
            this.detail.Name = "detail";
            // 
            // subReport
            // 
            this.subReport.CloseBorder = false;
            this.subReport.Height = 0.2590001F;
            this.subReport.Left = 0.043F;
            this.subReport.Name = "subReport";
            this.subReport.Report = null;
            this.subReport.ReportName = "subReport";
            this.subReport.Top = 0.701F;
            this.subReport.Width = 7.187F;
            // 
            // subReport1
            // 
            this.subReport1.CloseBorder = false;
            this.subReport1.Height = 0.343F;
            this.subReport1.Left = 0.043F;
            this.subReport1.Name = "subReport1";
            this.subReport1.Report = null;
            this.subReport1.ReportName = "subReport1";
            this.subReport1.Top = 0.348F;
            this.subReport1.Width = 7.187F;
            // 
            // subReport2
            // 
            this.subReport2.CloseBorder = false;
            this.subReport2.Height = 0.343F;
            this.subReport2.Left = 0.043F;
            this.subReport2.Name = "subReport2";
            this.subReport2.Report = null;
            this.subReport2.ReportName = "subReport2";
            this.subReport2.Top = 0.005000001F;
            this.subReport2.Width = 7.187F;
            // 
            // pageFooter
            // 
            this.pageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.line7});
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // line7
            // 
            this.line7.Height = 0F;
            this.line7.Left = 0F;
            this.line7.LineWeight = 2F;
            this.line7.Name = "line7";
            this.line7.Top = 0.187F;
            this.line7.Width = 7.292F;
            this.line7.X1 = 0F;
            this.line7.X2 = 7.292F;
            this.line7.Y1 = 0.187F;
            this.line7.Y2 = 0.187F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.labelTitle,
            this.oleObject1,
            this.labelTitleProcGubun,
            this.label3,
            this.label4,
            this.oleObject2,
            this.label5,
            this.oleObject3,
            this.textDept,
            this.textBox2,
            this.textCerReqName,
            this.line1,
            this.label18,
            this.label19,
            this.label20,
            this.label21,
            this.label22,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.textProc,
            this.txtPlantCode,
            this.txtDocCode,
            this.txtEquipCode,
            this.txtEquipCertiReqTypeCode,
            this.txtCertiReqID});
            this.reportHeader1.Height = 1.427F;
            this.reportHeader1.Name = "reportHeader1";
            this.reportHeader1.Format += new System.EventHandler(this.reportHeader1_Format);
            // 
            // labelTitle
            // 
            this.labelTitle.Height = 0.364F;
            this.labelTitle.HyperLink = null;
            this.labelTitle.Left = 0.176F;
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Style = "font-size: 20pt; font-weight: bold";
            this.labelTitle.Text = "설비인증의뢰 / 통보서";
            this.labelTitle.Top = 0F;
            this.labelTitle.Width = 3.055F;
            // 
            // oleObject1
            // 
            this.oleObject1.Class = "Paint.Picture";
            this.oleObject1.DataValue = ((System.IO.Stream)(resources.GetObject("oleObject1.DataValue")));
            this.oleObject1.Height = 0.1669999F;
            this.oleObject1.Left = 4.081F;
            this.oleObject1.Name = "oleObject1";
            this.oleObject1.Top = 1.01F;
            this.oleObject1.Visible = false;
            this.oleObject1.Width = 0.153F;
            // 
            // labelTitleProcGubun
            // 
            this.labelTitleProcGubun.Height = 0.818F;
            this.labelTitleProcGubun.HyperLink = null;
            this.labelTitleProcGubun.Left = 3.238F;
            this.labelTitleProcGubun.Name = "labelTitleProcGubun";
            this.labelTitleProcGubun.Style = "font-size: 17pt; font-weight: bold; vertical-align: top";
            this.labelTitleProcGubun.Text = "";
            this.labelTitleProcGubun.Top = 0.01F;
            this.labelTitleProcGubun.Width = 1.86F;
            // 
            // label3
            // 
            this.label3.Height = 0.22F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.212F;
            this.label3.Name = "label3";
            this.label3.Style = "font-size: 11pt; font-weight: bold; vertical-align: middle";
            this.label3.Text = "◆ 의뢰  부서  :";
            this.label3.Top = 0.608F;
            this.label3.Width = 1.219F;
            // 
            // label4
            // 
            this.label4.Height = 0.22F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.212F;
            this.label4.Name = "label4";
            this.label4.Style = "font-size: 11pt; font-weight: bold; vertical-align: middle";
            this.label4.Text = "◆ 의뢰  일자  :";
            this.label4.Top = 0.8570001F;
            this.label4.Width = 1.219F;
            // 
            // oleObject2
            // 
            this.oleObject2.Class = "Paint.Picture";
            this.oleObject2.DataValue = ((System.IO.Stream)(resources.GetObject("oleObject2.DataValue")));
            this.oleObject2.Height = 0.167F;
            this.oleObject2.Left = 4.234F;
            this.oleObject2.Name = "oleObject2";
            this.oleObject2.Top = 1.01F;
            this.oleObject2.Visible = false;
            this.oleObject2.Width = 0.153F;
            // 
            // label5
            // 
            this.label5.Height = 0.22F;
            this.label5.HyperLink = null;
            this.label5.Left = 0.212F;
            this.label5.Name = "label5";
            this.label5.Style = "font-size: 11pt; font-weight: bold; vertical-align: middle";
            this.label5.Text = "◆ 의    뢰  자  :";
            this.label5.Top = 1.114F;
            this.label5.Width = 1.219F;
            // 
            // oleObject3
            // 
            this.oleObject3.Class = "Paint.Picture";
            this.oleObject3.DataValue = ((System.IO.Stream)(resources.GetObject("oleObject3.DataValue")));
            this.oleObject3.Height = 0.167F;
            this.oleObject3.Left = 4.387F;
            this.oleObject3.Name = "oleObject3";
            this.oleObject3.Top = 1.01F;
            this.oleObject3.Visible = false;
            this.oleObject3.Width = 0.153F;
            // 
            // textDept
            // 
            this.textDept.DataField = "CertiReqDeptName";
            this.textDept.Height = 0.2F;
            this.textDept.Left = 1.445F;
            this.textDept.Name = "textDept";
            this.textDept.Style = "font-size: 10pt; vertical-align: middle";
            this.textDept.Text = null;
            this.textDept.Top = 0.618F;
            this.textDept.Width = 1.732F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "CertiReqDate";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 1.445F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-size: 9pt; vertical-align: middle";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.867F;
            this.textBox2.Width = 1F;
            // 
            // textCerReqName
            // 
            this.textCerReqName.DataField = "CertiReqName";
            this.textCerReqName.Height = 0.2F;
            this.textCerReqName.Left = 1.445F;
            this.textCerReqName.Name = "textCerReqName";
            this.textCerReqName.Style = "font-size: 10pt; vertical-align: middle";
            this.textCerReqName.Text = null;
            this.textCerReqName.Top = 1.127F;
            this.textCerReqName.Width = 1.25F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 1.562F;
            this.line1.Width = 6.49F;
            this.line1.X1 = 0F;
            this.line1.X2 = 6.49F;
            this.line1.Y1 = 1.562F;
            this.line1.Y2 = 1.562F;
            // 
            // label18
            // 
            this.label18.Height = 0.25F;
            this.label18.HyperLink = null;
            this.label18.Left = 5.232F;
            this.label18.Name = "label18";
            this.label18.Style = "font-size: 10pt; font-weight: bold; text-align: center; text-justify: auto; verti" +
                "cal-align: middle";
            this.label18.Text = "설  비  인  증  의  뢰  부  서";
            this.label18.Top = 0F;
            this.label18.Width = 2.06F;
            // 
            // label19
            // 
            this.label19.Height = 1.124F;
            this.label19.HyperLink = null;
            this.label19.Left = 5.23F;
            this.label19.Name = "label19";
            this.label19.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-font-vertical: none";
            this.label19.Text = "의뢰부서";
            this.label19.Top = 0.2F;
            this.label19.Width = 0.2290001F;
            // 
            // label20
            // 
            this.label20.Height = 0.2F;
            this.label20.HyperLink = null;
            this.label20.Left = 5.459F;
            this.label20.Name = "label20";
            this.label20.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.label20.Text = "입     안";
            this.label20.Top = 0.25F;
            this.label20.Width = 0.6030002F;
            // 
            // label21
            // 
            this.label21.Height = 0.2F;
            this.label21.HyperLink = null;
            this.label21.Left = 6.082F;
            this.label21.Name = "label21";
            this.label21.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.label21.Text = "실     사";
            this.label21.Top = 0.25F;
            this.label21.Width = 0.6029996F;
            // 
            // label22
            // 
            this.label22.Height = 0.2F;
            this.label22.HyperLink = null;
            this.label22.Left = 6.685F;
            this.label22.Name = "label22";
            this.label22.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.label22.Text = "결     정";
            this.label22.Top = 0.25F;
            this.label22.Width = 0.6069998F;
            // 
            // line8
            // 
            this.line8.Height = 1.104F;
            this.line8.Left = 5.459F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.22F;
            this.line8.Width = 0F;
            this.line8.X1 = 5.459F;
            this.line8.X2 = 5.459F;
            this.line8.Y1 = 1.324F;
            this.line8.Y2 = 0.22F;
            // 
            // line9
            // 
            this.line9.Height = 1.324F;
            this.line9.Left = 5.23F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0.001999855F;
            this.line9.X1 = 5.23F;
            this.line9.X2 = 5.232F;
            this.line9.Y1 = 1.324F;
            this.line9.Y2 = 0F;
            // 
            // line10
            // 
            this.line10.Height = 0F;
            this.line10.Left = 5.459F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.45F;
            this.line10.Width = 1.833F;
            this.line10.X1 = 5.459F;
            this.line10.X2 = 7.292F;
            this.line10.Y1 = 0.45F;
            this.line10.Y2 = 0.45F;
            // 
            // line11
            // 
            this.line11.Height = 0F;
            this.line11.Left = 5.459F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 1.097F;
            this.line11.Width = 1.833F;
            this.line11.X1 = 5.459F;
            this.line11.X2 = 7.292F;
            this.line11.Y1 = 1.097F;
            this.line11.Y2 = 1.097F;
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 5.232F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 1.324F;
            this.line12.Width = 2.062001F;
            this.line12.X1 = 5.232F;
            this.line12.X2 = 7.294001F;
            this.line12.Y1 = 1.324F;
            this.line12.Y2 = 1.324F;
            // 
            // line14
            // 
            this.line14.Height = 1.324F;
            this.line14.Left = 7.292F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0.002000332F;
            this.line14.X1 = 7.294F;
            this.line14.X2 = 7.292F;
            this.line14.Y1 = 1.324F;
            this.line14.Y2 = 0F;
            // 
            // line15
            // 
            this.line15.Height = 1.104F;
            this.line15.Left = 6.685F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0.22F;
            this.line15.Width = 0F;
            this.line15.X1 = 6.685F;
            this.line15.X2 = 6.685F;
            this.line15.Y1 = 1.324F;
            this.line15.Y2 = 0.22F;
            // 
            // line16
            // 
            this.line16.Height = 1.104F;
            this.line16.Left = 6.062F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.22F;
            this.line16.Width = 0F;
            this.line16.X1 = 6.062F;
            this.line16.X2 = 6.062F;
            this.line16.Y1 = 1.324F;
            this.line16.Y2 = 0.22F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 5.23F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.22F;
            this.line17.Width = 2.062F;
            this.line17.X1 = 5.23F;
            this.line17.X2 = 7.292F;
            this.line17.Y1 = 0.22F;
            this.line17.Y2 = 0.22F;
            // 
            // line18
            // 
            this.line18.Height = 0F;
            this.line18.Left = 5.23F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 2.062F;
            this.line18.X1 = 5.23F;
            this.line18.X2 = 7.292F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0F;
            // 
            // textProc
            // 
            this.textProc.DataField = "ReProcName";
            this.textProc.Height = 0.2F;
            this.textProc.Left = 3.238F;
            this.textProc.Name = "textProc";
            this.textProc.Style = "font-size: 7pt";
            this.textProc.Text = null;
            this.textProc.Top = 1.097F;
            this.textProc.Visible = false;
            this.textProc.Width = 0.131F;
            // 
            // txtPlantCode
            // 
            this.txtPlantCode.DataField = "PlantCode";
            this.txtPlantCode.Height = 0.1979167F;
            this.txtPlantCode.Left = 3.369F;
            this.txtPlantCode.Name = "txtPlantCode";
            this.txtPlantCode.Style = "font-size: 3pt";
            this.txtPlantCode.Text = null;
            this.txtPlantCode.Top = 1.099F;
            this.txtPlantCode.Visible = false;
            this.txtPlantCode.Width = 0.1459999F;
            // 
            // txtDocCode
            // 
            this.txtDocCode.DataField = "DocCode";
            this.txtDocCode.Height = 0.1979167F;
            this.txtDocCode.Left = 3.515F;
            this.txtDocCode.Name = "txtDocCode";
            this.txtDocCode.Style = "font-size: 3pt";
            this.txtDocCode.Text = null;
            this.txtDocCode.Top = 1.099F;
            this.txtDocCode.Visible = false;
            this.txtDocCode.Width = 0.115F;
            // 
            // txtEquipCode
            // 
            this.txtEquipCode.DataField = "EquipCode";
            this.txtEquipCode.Height = 0.1979167F;
            this.txtEquipCode.Left = 3.63F;
            this.txtEquipCode.Name = "txtEquipCode";
            this.txtEquipCode.Style = "font-size: 3pt";
            this.txtEquipCode.Text = null;
            this.txtEquipCode.Top = 1.097F;
            this.txtEquipCode.Visible = false;
            this.txtEquipCode.Width = 0.1250002F;
            // 
            // txtEquipCertiReqTypeCode
            // 
            this.txtEquipCertiReqTypeCode.DataField = "EquipCertiReqTypeCode";
            this.txtEquipCertiReqTypeCode.Height = 0.1979167F;
            this.txtEquipCertiReqTypeCode.Left = 3.755F;
            this.txtEquipCertiReqTypeCode.Name = "txtEquipCertiReqTypeCode";
            this.txtEquipCertiReqTypeCode.Style = "font-size: 3pt";
            this.txtEquipCertiReqTypeCode.Text = null;
            this.txtEquipCertiReqTypeCode.Top = 1.097F;
            this.txtEquipCertiReqTypeCode.Visible = false;
            this.txtEquipCertiReqTypeCode.Width = 0.07800031F;
            // 
            // txtCertiReqID
            // 
            this.txtCertiReqID.DataField = "CertiReqID";
            this.txtCertiReqID.Height = 0.1979167F;
            this.txtCertiReqID.Left = 3.833F;
            this.txtCertiReqID.Name = "txtCertiReqID";
            this.txtCertiReqID.Style = "font-size: 3pt";
            this.txtCertiReqID.Text = null;
            this.txtCertiReqID.Top = 1.097F;
            this.txtCertiReqID.Visible = false;
            this.txtCertiReqID.Width = 0.1150002F;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.oleObject4,
            this.label13,
            this.label14,
            this.label15,
            this.label16,
            this.line3,
            this.label17,
            this.label23,
            this.label24,
            this.label25,
            this.label26,
            this.label27,
            this.line13,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.textBox1});
            this.reportFooter1.Height = 1.367806F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // oleObject4
            // 
            this.oleObject4.Class = "Paint.Picture";
            this.oleObject4.DataValue = ((System.IO.Stream)(resources.GetObject("oleObject4.DataValue")));
            this.oleObject4.Height = 0.147F;
            this.oleObject4.Left = 4.234F;
            this.oleObject4.Name = "oleObject4";
            this.oleObject4.Top = 0.137F;
            this.oleObject4.Visible = false;
            this.oleObject4.Width = 0.143F;
            // 
            // label13
            // 
            this.label13.Height = 0.752F;
            this.label13.HyperLink = null;
            this.label13.Left = 0.212F;
            this.label13.LineSpacing = 2F;
            this.label13.Name = "label13";
            this.label13.Style = "font-size: 13pt; font-weight: bold; ddo-char-set: 1";
            this.label13.Text = "상기 평가결과 근거에 의거 설비에 대한 품질인증이  ( 合格 , 不合格 )   되었음을 통보합니다.";
            this.label13.Top = 0.06F;
            this.label13.Width = 3.574F;
            // 
            // label14
            // 
            this.label14.Height = 0.2F;
            this.label14.HyperLink = null;
            this.label14.Left = 0.2120001F;
            this.label14.Name = "label14";
            this.label14.Style = "font-size: 10pt; font-weight: bold";
            this.label14.Text = "인증일자  :";
            this.label14.Top = 0.9809999F;
            this.label14.Width = 0.77F;
            // 
            // label15
            // 
            this.label15.Height = 0.2F;
            this.label15.HyperLink = null;
            this.label15.Left = 2.019F;
            this.label15.Name = "label15";
            this.label15.Style = "font-size: 10pt; font-weight: bold";
            this.label15.Text = "인증담당자  :";
            this.label15.Top = 0.9809999F;
            this.label15.Width = 0.9299999F;
            // 
            // label16
            // 
            this.label16.Height = 0.2F;
            this.label16.HyperLink = null;
            this.label16.Left = 0.983F;
            this.label16.Name = "label16";
            this.label16.Style = "font-size: 9pt; font-weight: bold";
            this.label16.Text = "20     .     .     .\r\n\r\n   .";
            this.label16.Top = 0.9909999F;
            this.label16.Width = 0.936F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 2.961F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.151F;
            this.line3.Width = 1.000002F;
            this.line3.X1 = 2.961F;
            this.line3.X2 = 3.961002F;
            this.line3.Y1 = 1.151F;
            this.line3.Y2 = 1.151F;
            // 
            // label17
            // 
            this.label17.Height = 0.1599999F;
            this.label17.HyperLink = null;
            this.label17.Left = 3.974002F;
            this.label17.Name = "label17";
            this.label17.Style = "";
            this.label17.Text = "印";
            this.label17.Top = 0.9909999F;
            this.label17.Width = 0.1660004F;
            // 
            // label23
            // 
            this.label23.Height = 0.25F;
            this.label23.HyperLink = null;
            this.label23.Left = 4.736F;
            this.label23.Name = "label23";
            this.label23.Style = "font-size: 10pt; font-weight: bold; text-align: center; text-justify: auto; verti" +
                "cal-align: middle";
            this.label23.Text = "품  질   인  증   부  서";
            this.label23.Top = 0.06F;
            this.label23.Width = 2.293F;
            // 
            // label24
            // 
            this.label24.Height = 0.934F;
            this.label24.HyperLink = null;
            this.label24.Left = 4.736F;
            this.label24.Name = "label24";
            this.label24.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-font-vertical: none";
            this.label24.Text = "접 수 부 서";
            this.label24.Top = 0.284F;
            this.label24.Width = 0.2290001F;
            // 
            // label25
            // 
            this.label25.Height = 0.2F;
            this.label25.HyperLink = null;
            this.label25.Left = 4.967F;
            this.label25.Name = "label25";
            this.label25.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.label25.Text = "입     안";
            this.label25.Top = 0.3239999F;
            this.label25.Width = 0.683F;
            // 
            // label26
            // 
            this.label26.Height = 0.2F;
            this.label26.HyperLink = null;
            this.label26.Left = 5.658999F;
            this.label26.Name = "label26";
            this.label26.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.label26.Text = "실     사";
            this.label26.Top = 0.3239999F;
            this.label26.Width = 0.6829996F;
            // 
            // label27
            // 
            this.label27.Height = 0.2F;
            this.label27.HyperLink = null;
            this.label27.Left = 6.342F;
            this.label27.Name = "label27";
            this.label27.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.label27.Text = "결     정";
            this.label27.Top = 0.3239999F;
            this.label27.Width = 0.6869996F;
            // 
            // line13
            // 
            this.line13.Height = 0.921F;
            this.line13.Left = 4.975F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0.3099999F;
            this.line13.Width = 0F;
            this.line13.X1 = 4.975F;
            this.line13.X2 = 4.975F;
            this.line13.Y1 = 1.231F;
            this.line13.Y2 = 0.3099999F;
            // 
            // line19
            // 
            this.line19.Height = 1.158F;
            this.line19.Left = 4.733999F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.06F;
            this.line19.Width = 0.002001286F;
            this.line19.X1 = 4.733999F;
            this.line19.X2 = 4.736F;
            this.line19.Y1 = 1.218F;
            this.line19.Y2 = 0.06F;
            // 
            // line20
            // 
            this.line20.Height = 2.980232E-07F;
            this.line20.Left = 4.965F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.5239999F;
            this.line20.Width = 2.062F;
            this.line20.X1 = 4.965F;
            this.line20.X2 = 7.027F;
            this.line20.Y1 = 0.5239999F;
            this.line20.Y2 = 0.5240002F;
            // 
            // line21
            // 
            this.line21.Height = 0F;
            this.line21.Left = 4.987F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.9909999F;
            this.line21.Width = 2.04F;
            this.line21.X1 = 4.987F;
            this.line21.X2 = 7.027F;
            this.line21.Y1 = 0.9909999F;
            this.line21.Y2 = 0.9909999F;
            // 
            // line22
            // 
            this.line22.Height = 1.158F;
            this.line22.Left = 7.027F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.06F;
            this.line22.Width = 0.001999855F;
            this.line22.X1 = 7.029F;
            this.line22.X2 = 7.027F;
            this.line22.Y1 = 1.218F;
            this.line22.Y2 = 0.06F;
            // 
            // line23
            // 
            this.line23.Height = 0.921F;
            this.line23.Left = 6.342F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.3099999F;
            this.line23.Width = 0F;
            this.line23.X1 = 6.342F;
            this.line23.X2 = 6.342F;
            this.line23.Y1 = 1.231F;
            this.line23.Y2 = 0.3099999F;
            // 
            // line24
            // 
            this.line24.Height = 0.921F;
            this.line24.Left = 5.658999F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.3099999F;
            this.line24.Width = 0.001000881F;
            this.line24.X1 = 5.66F;
            this.line24.X2 = 5.658999F;
            this.line24.Y1 = 1.231F;
            this.line24.Y2 = 0.3099999F;
            // 
            // line25
            // 
            this.line25.Height = 1.788139E-07F;
            this.line25.Left = 4.733999F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.3039998F;
            this.line25.Width = 2.291001F;
            this.line25.X1 = 4.733999F;
            this.line25.X2 = 7.025F;
            this.line25.Y1 = 0.304F;
            this.line25.Y2 = 0.3039998F;
            // 
            // line26
            // 
            this.line26.Height = 0F;
            this.line26.Left = 4.733999F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 1.228F;
            this.line26.Width = 2.293001F;
            this.line26.X1 = 4.733999F;
            this.line26.X2 = 7.027F;
            this.line26.Y1 = 1.228F;
            this.line26.Y2 = 1.228F;
            // 
            // line27
            // 
            this.line27.Height = 0F;
            this.line27.Left = 4.736F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.07000001F;
            this.line27.Width = 2.291F;
            this.line27.X1 = 4.736F;
            this.line27.X2 = 7.027F;
            this.line27.Y1 = 0.07000001F;
            this.line27.Y2 = 0.07000001F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.chbEquipCertiReqTypeCode06,
            this.chbEquipCertiReqTypeCode05,
            this.chbEquipCertiReqTypeCode04,
            this.chbEquipCertiReqTypeCode03,
            this.chbEquipCertiReqTypeCode02,
            this.chbEquipCertiReqTypeCode01,
            this.line2,
            this.label7,
            this.label8,
            this.labelModelTitle,
            this.label10,
            this.labelSerial,
            this.labelPkgType,
            this.label6});
            this.groupHeader1.Height = 0.7811669F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.Format += new System.EventHandler(this.groupHeader1_Format);
            // 
            // chbEquipCertiReqTypeCode06
            // 
            this.chbEquipCertiReqTypeCode06.Height = 0.2F;
            this.chbEquipCertiReqTypeCode06.Left = 6.127999F;
            this.chbEquipCertiReqTypeCode06.Name = "chbEquipCertiReqTypeCode06";
            this.chbEquipCertiReqTypeCode06.Style = "font-size: 9pt; font-weight: bold";
            this.chbEquipCertiReqTypeCode06.Text = "운휴설비";
            this.chbEquipCertiReqTypeCode06.Top = 0.03F;
            this.chbEquipCertiReqTypeCode06.Width = 0.823F;
            // 
            // chbEquipCertiReqTypeCode05
            // 
            this.chbEquipCertiReqTypeCode05.Height = 0.2F;
            this.chbEquipCertiReqTypeCode05.Left = 4.927F;
            this.chbEquipCertiReqTypeCode05.Name = "chbEquipCertiReqTypeCode05";
            this.chbEquipCertiReqTypeCode05.Style = "font-size: 9pt; font-weight: bold";
            this.chbEquipCertiReqTypeCode05.Text = "재인증설비";
            this.chbEquipCertiReqTypeCode05.Top = 0.03F;
            this.chbEquipCertiReqTypeCode05.Width = 0.943F;
            // 
            // chbEquipCertiReqTypeCode04
            // 
            this.chbEquipCertiReqTypeCode04.Height = 0.2F;
            this.chbEquipCertiReqTypeCode04.Left = 3.479F;
            this.chbEquipCertiReqTypeCode04.Name = "chbEquipCertiReqTypeCode04";
            this.chbEquipCertiReqTypeCode04.Style = "font-size: 9pt; font-weight: bold";
            this.chbEquipCertiReqTypeCode04.Text = "신금형(신다이)\r\n";
            this.chbEquipCertiReqTypeCode04.Top = 0.03F;
            this.chbEquipCertiReqTypeCode04.Width = 1.195F;
            // 
            // chbEquipCertiReqTypeCode03
            // 
            this.chbEquipCertiReqTypeCode03.Height = 0.2F;
            this.chbEquipCertiReqTypeCode03.Left = 2.333F;
            this.chbEquipCertiReqTypeCode03.Name = "chbEquipCertiReqTypeCode03";
            this.chbEquipCertiReqTypeCode03.Style = "font-size: 9pt; font-weight: bold";
            this.chbEquipCertiReqTypeCode03.Text = "개조설비";
            this.chbEquipCertiReqTypeCode03.Top = 0.03F;
            this.chbEquipCertiReqTypeCode03.Width = 0.823F;
            // 
            // chbEquipCertiReqTypeCode02
            // 
            this.chbEquipCertiReqTypeCode02.Height = 0.2F;
            this.chbEquipCertiReqTypeCode02.Left = 1.275F;
            this.chbEquipCertiReqTypeCode02.Name = "chbEquipCertiReqTypeCode02";
            this.chbEquipCertiReqTypeCode02.Style = "font-size: 9pt; font-weight: bold";
            this.chbEquipCertiReqTypeCode02.Text = "이전설비";
            this.chbEquipCertiReqTypeCode02.Top = 0.03F;
            this.chbEquipCertiReqTypeCode02.Width = 0.823F;
            // 
            // chbEquipCertiReqTypeCode01
            // 
            this.chbEquipCertiReqTypeCode01.Height = 0.2F;
            this.chbEquipCertiReqTypeCode01.Left = 0.18F;
            this.chbEquipCertiReqTypeCode01.Name = "chbEquipCertiReqTypeCode01";
            this.chbEquipCertiReqTypeCode01.Style = "font-size: 9pt; font-weight: bold";
            this.chbEquipCertiReqTypeCode01.Text = "신규설비\r\n";
            this.chbEquipCertiReqTypeCode01.Top = 0.03F;
            this.chbEquipCertiReqTypeCode01.Width = 0.823F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.293F;
            this.line2.Width = 7.292F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.292F;
            this.line2.Y1 = 0.293F;
            this.line2.Y2 = 0.293F;
            // 
            // label7
            // 
            this.label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label7.Height = 0.2F;
            this.label7.HyperLink = null;
            this.label7.Left = 0.043F;
            this.label7.Name = "label7";
            this.label7.Style = "font-weight: bold; text-align: center";
            this.label7.Text = "공정";
            this.label7.Top = 0.581F;
            this.label7.Width = 1.096F;
            // 
            // label8
            // 
            this.label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label8.Height = 0.2F;
            this.label8.HyperLink = null;
            this.label8.Left = 1.139F;
            this.label8.Name = "label8";
            this.label8.Style = "font-weight: bold; text-align: center";
            this.label8.Text = "설비명";
            this.label8.Top = 0.581F;
            this.label8.Width = 1.194F;
            // 
            // labelModelTitle
            // 
            this.labelModelTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelModelTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelModelTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelModelTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelModelTitle.Height = 0.2F;
            this.labelModelTitle.HyperLink = null;
            this.labelModelTitle.Left = 2.333F;
            this.labelModelTitle.Name = "labelModelTitle";
            this.labelModelTitle.Style = "font-weight: bold; text-align: center";
            this.labelModelTitle.Text = "MODEL명";
            this.labelModelTitle.Top = 0.581F;
            this.labelModelTitle.Width = 1.084F;
            // 
            // label10
            // 
            this.label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label10.Height = 0.2F;
            this.label10.HyperLink = null;
            this.label10.Left = 3.417001F;
            this.label10.Name = "label10";
            this.label10.Style = "font-weight: bold; text-align: center";
            this.label10.Text = "MAKER";
            this.label10.Top = 0.581F;
            this.label10.Width = 1.115F;
            // 
            // labelSerial
            // 
            this.labelSerial.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelSerial.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelSerial.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelSerial.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelSerial.Height = 0.2F;
            this.labelSerial.HyperLink = null;
            this.labelSerial.Left = 4.532001F;
            this.labelSerial.Name = "labelSerial";
            this.labelSerial.Style = "font-weight: bold; text-align: center";
            this.labelSerial.Text = "SERIAL NO";
            this.labelSerial.Top = 0.581F;
            this.labelSerial.Width = 1.042F;
            // 
            // labelPkgType
            // 
            this.labelPkgType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelPkgType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelPkgType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelPkgType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelPkgType.Height = 0.2F;
            this.labelPkgType.HyperLink = null;
            this.labelPkgType.Left = 5.574001F;
            this.labelPkgType.Name = "labelPkgType";
            this.labelPkgType.Style = "font-weight: bold; text-align: center";
            this.labelPkgType.Text = "적용 PKG TYPE\r\n";
            this.labelPkgType.Top = 0.581F;
            this.labelPkgType.Width = 1.656F;
            // 
            // label6
            // 
            this.label6.Height = 0.2F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.043F;
            this.label6.Name = "label6";
            this.label6.Style = "";
            this.label6.Text = "1 . 설비 개요";
            this.label6.Top = 0.366F;
            this.label6.Width = 0.908F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.012F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-size: 13pt; ddo-char-set: 1";
            this.textBox1.Text = "◆";
            this.textBox1.Top = 0.06F;
            this.textBox1.Width = 0.21F;
            // 
            // rptEQUZ0002_Main
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.431F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            this.ReportEnd += new System.EventHandler(this.rptEQUZ0002_Main_ReportEnd);
            this.ReportStart += new System.EventHandler(this.rptEQUZ0002_01_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.labelTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTitleProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCerReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textProc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipCertiReqTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCertiReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbEquipCertiReqTypeCode01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModelTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSerial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPkgType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.ReportHeader reportHeader1;
        private DataDynamics.ActiveReports.ReportFooter reportFooter1;
        private DataDynamics.ActiveReports.Label labelTitle;
        private DataDynamics.ActiveReports.OleObject oleObject1;
        private DataDynamics.ActiveReports.Label labelTitleProcGubun;
        private DataDynamics.ActiveReports.Label label3;
        private DataDynamics.ActiveReports.Label label4;
        private DataDynamics.ActiveReports.OleObject oleObject2;
        private DataDynamics.ActiveReports.Label label5;
        private DataDynamics.ActiveReports.OleObject oleObject3;
        private DataDynamics.ActiveReports.TextBox textDept;
        private DataDynamics.ActiveReports.TextBox textBox2;
        private DataDynamics.ActiveReports.TextBox textCerReqName;
        private DataDynamics.ActiveReports.CrossSectionLine crossSectionLine2;
        private DataDynamics.ActiveReports.Line line1;
        private DataDynamics.ActiveReports.CrossSectionLine crossSectionLine1;
        private DataDynamics.ActiveReports.Line line6;
        private DataDynamics.ActiveReports.Line line7;
        private DataDynamics.ActiveReports.GroupHeader groupHeader1;
        private DataDynamics.ActiveReports.GroupFooter groupFooter1;
        private DataDynamics.ActiveReports.SubReport subReport;
        private DataDynamics.ActiveReports.SubReport subReport1;
        private DataDynamics.ActiveReports.Label label18;
        private DataDynamics.ActiveReports.Label label19;
        private DataDynamics.ActiveReports.Label label20;
        private DataDynamics.ActiveReports.Label label21;
        private DataDynamics.ActiveReports.Label label22;
        private DataDynamics.ActiveReports.Line line8;
        private DataDynamics.ActiveReports.Line line9;
        private DataDynamics.ActiveReports.Line line10;
        private DataDynamics.ActiveReports.Line line11;
        private DataDynamics.ActiveReports.Line line12;
        private DataDynamics.ActiveReports.Line line14;
        private DataDynamics.ActiveReports.Line line15;
        private DataDynamics.ActiveReports.Line line16;
        private DataDynamics.ActiveReports.Line line17;
        private DataDynamics.ActiveReports.Line line18;
        private DataDynamics.ActiveReports.TextBox txtDocCode;
        private DataDynamics.ActiveReports.TextBox txtEquipCertiReqTypeCode;
        private DataDynamics.ActiveReports.TextBox txtPlantCode;
        private DataDynamics.ActiveReports.TextBox txtCertiReqID;
        private DataDynamics.ActiveReports.TextBox txtEquipCode;
        private DataDynamics.ActiveReports.CheckBox chbEquipCertiReqTypeCode06;
        private DataDynamics.ActiveReports.CheckBox chbEquipCertiReqTypeCode05;
        private DataDynamics.ActiveReports.CheckBox chbEquipCertiReqTypeCode04;
        private DataDynamics.ActiveReports.CheckBox chbEquipCertiReqTypeCode03;
        private DataDynamics.ActiveReports.CheckBox chbEquipCertiReqTypeCode02;
        private DataDynamics.ActiveReports.CheckBox chbEquipCertiReqTypeCode01;
        private DataDynamics.ActiveReports.Line line2;
        private DataDynamics.ActiveReports.TextBox textProc;
        private DataDynamics.ActiveReports.SubReport subReport2;
        private DataDynamics.ActiveReports.Label label7;
        private DataDynamics.ActiveReports.Label label8;
        private DataDynamics.ActiveReports.Label labelModelTitle;
        private DataDynamics.ActiveReports.Label label10;
        private DataDynamics.ActiveReports.Label labelSerial;
        private DataDynamics.ActiveReports.Label labelPkgType;
        private DataDynamics.ActiveReports.Label label6;
        private DataDynamics.ActiveReports.OleObject oleObject4;
        private DataDynamics.ActiveReports.Label label13;
        private DataDynamics.ActiveReports.Label label14;
        private DataDynamics.ActiveReports.Label label15;
        private DataDynamics.ActiveReports.Label label16;
        private DataDynamics.ActiveReports.Line line3;
        private DataDynamics.ActiveReports.Label label17;
        private DataDynamics.ActiveReports.Label label23;
        private DataDynamics.ActiveReports.Label label24;
        private DataDynamics.ActiveReports.Label label25;
        private DataDynamics.ActiveReports.Label label26;
        private DataDynamics.ActiveReports.Label label27;
        private DataDynamics.ActiveReports.Line line13;
        private DataDynamics.ActiveReports.Line line19;
        private DataDynamics.ActiveReports.Line line20;
        private DataDynamics.ActiveReports.Line line21;
        private DataDynamics.ActiveReports.Line line22;
        private DataDynamics.ActiveReports.Line line23;
        private DataDynamics.ActiveReports.Line line24;
        private DataDynamics.ActiveReports.Line line25;
        private DataDynamics.ActiveReports.Line line26;
        private DataDynamics.ActiveReports.Line line27;
        private DataDynamics.ActiveReports.TextBox textBox1;
    }
}
