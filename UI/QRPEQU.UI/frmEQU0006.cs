﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQU0006.cs                                         */
/* 프로그램명   : 정비결과등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-00-xx : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0006 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQU0006()
        {
            InitializeComponent();
        }

        #region Form

        private void frmEQU0006_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmEQU0006_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("정비결과등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 초기화 Method
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitButton();
            InitValue();

            // ExtandableGroupBox 설정
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmEQU0006_Activated(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 툴바 활성여부 처리
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #endregion

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "점검결과정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.Appearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "SparePart사용정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3.Appearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 초기값 설정 Method
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 점검일 초기화
                this.uDateSearchFromDay.Value = DateTime.Now;
                this.uDateSearchToDay.Value = DateTime.Now;

                //사용일
                this.uDateUsingDate.Value = DateTime.Now;


                // 사용담당자
                this.uTextUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");
                
                // 정비사 ID 설정
                this.uTextTechnician.Text = m_resSys.GetString("SYS_USERID");
                this.uTextTechnicianName.Text = m_resSys.GetString("SYS_USERNAME");

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipMentGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcGubun, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchUser, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabel2, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ultraLabel2, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel4, "점검일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                wLabel.mfSetLabel(this.uLabel5, "사용일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel6, "사용담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelUnusual, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabel7, "사용SparePart", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel8, "점검결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call Bl
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //////////////////////
                /// 나머지들 추가/////
                //////////////////////
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 정비결과 Grid
                 
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Extended, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "PMPlanDate", "점검일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "PMWorkTime", "최종점검시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "PMEndDate", "사용일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "SPUseChargeID", "사용담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "SPUseChargeName", "사용담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMEquip, 0, "SPUseEtcDesc", "사용특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 사용자 SparePart Grid
                
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridUseSP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurSparePartCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurSparePartName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurInputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurSpec", "구성품Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CurMaker", "구성품Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSPInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSparePartCode", "사용구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSparePartName", "사용구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "Qty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgInputQty", "사용량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgSpec", "사용구성품Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgMaker", "사용구성품Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "ChgDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUseSP, 0, "CancelFlag", "수리취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //헤더에있는 체크박스 안보임
                this.uGridUseSP.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //채널연결

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                wGrid.mfSetGridColumnValueList(this.uGridUseSP, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridUseSP, 0);

                #endregion

                #region 점검결과 Grid
                 
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPMResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMPeriodName", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultName", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMResultValue", "수치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPMResult, 0, "PMWorkDesc", "점검특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                #endregion


                //폰트설정
                this.uGridPMEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridPMResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridPMResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridUseSP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridUseSP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ExpandGroupBox Value초기화
        /// </summary>
        private void ClearValue()
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용SparePart
                while (this.uGridUseSP.Rows.Count > 0)
                {
                    this.uGridUseSP.Rows[0].Delete(false);
                }
                //점검결과조회
                while (this.uGridPMResult.Rows.Count > 0)
                {
                    this.uGridPMResult.Rows[0].Delete(false);
                }
                //텍스트 박스들
                this.uTextEquipCode.Text = "";
                this.uTextEquipCode.Tag = "";
                this.uTextEquipName.Text = "";
                this.uTextPMPlanDate.Text = "";
                this.uTextEtcDesc.Text = "";


                //수리출고요청일
                this.uDateUsingDate.Value = DateTime.Now;


                string strPlantcode = m_resSys.GetString("SYS_PLANTCODE");
                string strUserID = m_resSys.GetString("SYS_USERID");

                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //brwChannel.mfCredentials(clsUser);

                //DataTable dtUser = clsUser.mfReadSYSUser(strPlantcode, strUserID, m_resSys.GetString("SYS_LANG"));

                string strUserName = m_resSys.GetString("SYS_USERNAME");


                // 사용자담당자 ID 설정
                this.uTextUserID.Text = strUserID;
                this.uTextUserName.Text = strUserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                    ClearValue();

                }

                #region 필수 입력사항
                //-----------------필수 입력사항 체크 -----------------//

                if (this.uDateSearchFromDay.Value.ToString() == "" || this.uDateSearchToDay.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "점검일을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDay.DropDown();
                    return;
                }
                if (this.uDateSearchFromDay.DateTime.Date > this.uDateSearchToDay.DateTime.Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "입력사항 확인", "범위가 올바르지 않습니다. 다시 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDay.DropDown();
                    return;

                }
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uTextTechnician.Text == "" || this.uTextTechnicianName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "정비사를 입력 또는 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextTechnician.Focus();
                    return;
                }

                #endregion

                //-------------------------값 저장--------------------------//
                string strDateFrom = this.uDateSearchFromDay.DateTime.Date.ToString("yyyy-MM-dd");              //검색시작일
                string strDateTo = this.uDateSearchToDay.DateTime.Date.ToString("yyyy-MM-dd");                  //검색종료일
                string strPlantCode = this.uComboSearchPlant.Value.ToString();              //공장코드
                string strTechnician = this.uTextTechnician.Text;                           //정비사
                string strStation = this.uComboSearchStation.Value.ToString();              //Station
                string strEquipLocCode = this.uComboEquipLoc.Value.ToString();              //위치
                string strProcessGroupCode = this.uComboProcessGroup.Value.ToString();      //설비대분류
                string strEquipLargeTypeCode = this.uComboEquipLargeType.Value.ToString();  //설비중분류
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();        //설비그룹
                //string strArea = this.uComboSearchArea.Value.ToString();                    //Area
                //string strGubun = this.uComboSearchProcGubun.Value.ToString();              //공정구분

                //----------POPUP 창 띄우기 ----------//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //--------커서 변경--------//
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMUseSP), "PMUseSP");
                QRPEQU.BL.EQUMGM.PMUseSP clsPMUseSP = new QRPEQU.BL.EQUMGM.PMUseSP();
                brwChannel.mfCredentials(clsPMUseSP);

                DataTable dtPMUseSP = new DataTable();

                //----검색조건이 (설비점검그룹) 또는 (Area, Station, 설비공정구분) 조회---//
                dtPMUseSP = clsPMUseSP.mfReadPMUseSP(strPlantCode,strTechnician,strStation,strEquipLocCode,strProcessGroupCode,strEquipLargeTypeCode,strEquipGroup,"","", 
                                                    strDateFrom, strDateTo, m_resSys.GetString("SYS_LANG"));


                //---점검결과 그리드에 바인드--//
                this.uGridPMEquip.DataSource = dtPMUseSP;
                this.uGridPMEquip.DataBind();

                //-------커서 기본값으로 변경-------//
                this.MdiParent.Cursor = Cursors.Default;

                //----------POPUP창 닫기--------//
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtPMUseSP.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridPMEquip, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수입력확인
                //-------------------------------필수 입력확인 , 정보 확인---------------------------------//
                if (this.uGridUseSP.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uGroupBoxContentsArea.Expanded == false || this.uTextEquipCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "입력사항 확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextUserID.Text == "" || this.uTextUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "확인창", "필수입력사항 확인", "사용담당자를 입력해주세요", Infragistics.Win.HAlign.Right);

                    this.uTextUserID.Focus();
                    return;
                }
                #endregion

                #region 컬럼설정
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMUseSP), "PMUseSP");
                QRPEQU.BL.EQUMGM.PMUseSP clsPMUseSP = new QRPEQU.BL.EQUMGM.PMUseSP();
                brwChannel.mfCredentials(clsPMUseSP);

                DataTable dtPMUseSPH = clsPMUseSP.mfSetHeaderDataInfo();
                DataTable dtPMUseSP = clsPMUseSP.mfSetDataInfo();
                

                //SP현재고 정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //SP현재고 컬럼정보 저장
                DataTable dtSPStock_Minus = clsSPStock.mfSetDatainfo();
                DataTable dtSPStock_Plus = clsSPStock.mfSetDatainfo();

                //자재출고이력정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                //컬럼정보가져오기
                DataTable dtSPStockMoveHist_Minus = clsSPStockMoveHist.mfSetDatainfo();
                DataTable dtSPStockMoveHist_Plus = clsSPStockMoveHist.mfSetDatainfo();

                //설비구성품정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);
                //설비구성품 컬럼정보가져오기
                DataTable dtEquipSPBOM = clsSPTransferD.mfSetDatainfo();
                DataTable dtEquipSPBOM_Cancel = clsSPTransferD.mfSetDatainfo();

                DataRow drMinus;
                DataRow drPlus;
                DataRow drBOM;
                #endregion

                #region 헤더저장
                //-----------------헤더 값저장-----------------//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;
                string strPMPlanDate = this.uTextPMPlanDate.Text;
                string strPlanYear = strPMPlanDate.Substring(0, 4);
                string strUseDate = Convert.ToDateTime(this.uDateUsingDate.Value).ToString("yyyy-MM-dd");
                string strSPUseChargID = this.uTextUserID.Text;
                string strEtcDesc = this.uTextEtcDesc.Text;

                DataRow drH = dtPMUseSPH.NewRow();
                drH["PlantCode"] = strPlantCode;  //공장
                drH["PlanYear"] = strPlanYear;   //계획년도
                drH["EquipCode"] = strEquipCode;  //설비코드
                drH["PMPlanDate"] = strPMPlanDate; //점검일
                drH["PMUseDate"] = strUseDate;  //사용일
                drH["SPUseChargeID"] = strSPUseChargID;//사용담당자
                drH["SPUseEtcDesc"] = strEtcDesc;   //비고
                dtPMUseSPH.Rows.Add(drH);

                #endregion

                #region 상세정보 저장(SparePart사용정보)

                //  해당설비의 구성품
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);
                DataTable dtSPBOM = new DataTable();

                DataTable dtStock = new DataTable();
                
                //----------------상세 저장----------------//
                
                if (this.uGridUseSP.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                    {
                        //기존정보에서 수리취소여부가 False인데 편집이미지가 있는 경우 이미지를 지운다.
                        if (this.uGridUseSP.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                            && Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(false) 
                            && this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            this.uGridUseSP.Rows[i].RowSelectorAppearance.Image = null;
                        }

                        if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridUseSP.Rows[i].Hidden.Equals(false))
                            {
                                #region 필수 입력 사항 확인
                                int intRowSelect = this.uGridUseSP.Rows[i].RowSelectorNumber;
                                if (this.uGridUseSP.Rows[i].Cells["CurInputQty"].Tag != null 
                                    && (this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.ToString().Equals(string.Empty) 
                                    || this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.Equals(0)))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "필수입력 확인창", intRowSelect + "번째 열의 수량이 0 이상이여야 합니다.", Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CurInputQty"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "필수입력 확인창", intRowSelect + "번째 열의 창고를 선택해주세요.", Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgSparePartName"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "필수입력 확인창", intRowSelect + "번째 열의 사용구성품명을 선택해주세요.", Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSparePartName"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.Equals(0) || this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "필수입력 확인창", intRowSelect + "번째 열의 사용량을 입력해주세요.", Infragistics.Win.HAlign.Right);

                                    this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgInputQty"];
                                    this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                #endregion

                                #region 테이블의 수량 확인

                                //교체등록시 CurSP가 설비구성품,ChgSP가 재고에 정보가 없거나 수량이 부족한 경우 메세지박스
                                if (!Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                {
                                    //기존구성품교체 시 
                                    if (!this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty))
                                    {
                                        //SparePartBOM
                                        dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                        {

                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "확인창", "교체 정보확인", intRowSelect + "번째 열의 SparePart가 교체되거나 수량이 부족하여 등록 할 수 없습니다.", Infragistics.Win.HAlign.Right);

                                            SearchSPBOM(strPlantCode, strEquipCode);
                                            this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value = 0;
                                            this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value = string.Empty;
                                            this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CurSparePartName"];
                                            this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;

                                        }
                                    }

                                    //SparePartStock
                                    dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                    {

                                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "확인창", "교체 정보확인", intRowSelect + "번째 SparePart가 교체되거나 수량이 부족하여 등록 할 수 없습니다.", Infragistics.Win.HAlign.Right);

                                        SearchSPStock(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), i);
                                        this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value = 0;
                                        this.uGridUseSP.Rows[i].Cells["Qty"].Value = 0;
                                        this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value = string.Empty;
                                        this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["ChgSparePartName"];
                                        this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                }
                                //수리취소 시 ChgSP가 설비구성품,CurSP가 재고에 정보가 없거나 수량이 부족한 경우 메세지박스
                                else
                                {
                                    //SparePartBOM
                                    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));



                                    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                    {

                                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "확인창", "수리취소 정보확인", intRowSelect + "번째 열의 SparePart가 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CancelFlag"];
                                        return;

                                    }
                                    if (!this.uGridUseSP.Rows[i].GetCellValue("CurSparePartCode").ToString().Equals(string.Empty))
                                    {
                                        //SparePartStock
                                        dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value))
                                        {

                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "확인창", "수리취소 정보확인", intRowSelect + "번째 SparePart가 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                            this.uGridUseSP.ActiveCell = this.uGridUseSP.Rows[i].Cells["CancelFlag"];
                                            return;
                                        }
                                    }
                                }
                                
                                #endregion

                                

                                //UseSP 테이블
                                DataRow drPMUseSP;
                                drPMUseSP = dtPMUseSP.NewRow();
                                //drPMUseSP["Seq"] = this.uGridUseSP.Rows[i].RowSelectorNumber.ToString();
                                drPMUseSP["Seq"] = this.uGridUseSP.Rows[i].Cells["Seq"].Value.ToString();
                                drPMUseSP["CurSparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                drPMUseSP["CurInputQty"] = this.uGridUseSP.Rows[i].Cells["CurInputQty"].Value.ToString();
                                drPMUseSP["ChgSPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                drPMUseSP["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                drPMUseSP["ChgInputQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                drPMUseSP["ChgDesc"] = this.uGridUseSP.Rows[i].Cells["ChgDesc"].Value.ToString();
                                drPMUseSP["CancelFlag"] = this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value.ToString();
                                dtPMUseSP.Rows.Add(drPMUseSP);

                                #region 창고재고에서 투입한 SparePart에 대한 정보 ,교체처리 : 취소정보는 제외

                                // 일반 저장시 창고에서 (-) 처리, 취소시 창고에서(+) 처리함.
                                if (this.uGridUseSP.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit) // 기존 저장된 데이터가 아닌것
                                {
                                    //EQUSPStock
                                    drMinus = dtSPStock_Minus.NewRow();

                                    drMinus["PlantCode"] = strPlantCode;
                                    drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    drMinus["Qty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStock_Minus.Rows.Add(drMinus);

                                    //----------------------------------------------------------------------------------------//
                                    //EQUSPStockMoveHist(재고이력 테이블)
                                    drMinus = dtSPStockMoveHist_Minus.NewRow();

                                    drMinus["MoveGubunCode"] = "M03";       //자재출고일 경우는 "M03"
                                    drMinus["DocCode"] = "";
                                    drMinus["MoveDate"] = strUseDate;
                                    drMinus["MoveChargeID"] = strSPUseChargID;
                                    drMinus["PlantCode"] = strPlantCode;
                                    drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    drMinus["EquipCode"] = strEquipCode;
                                    drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    drMinus["MoveQty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStockMoveHist_Minus.Rows.Add(drMinus);

                                    //----------------------------------------------------------------------------------------//
                                    //MASEquipSPBOM 테이블 : 교체처리 : 취소정보는 제외
                                    drBOM = dtEquipSPBOM.NewRow();
                                    drBOM["PlantCode"] = strPlantCode;
                                    drBOM["TransferSPCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();        //새로운 SP
                                    drBOM["TransferQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();

                                    drBOM["ChgEquipCode"] = strEquipCode;
                                    drBOM["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString(); // 기존에 있는 SP
                                    drBOM["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                    dtEquipSPBOM.Rows.Add(drBOM);


                                }
                                else
                                {
                                    if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true)) // 취소여부 체크한 것만
                                    {
                                        //EQUSPStock
                                        drPlus = dtSPStock_Plus.NewRow();

                                        drPlus["PlantCode"] = strPlantCode;
                                        drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                        drPlus["Qty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                        dtSPStock_Plus.Rows.Add(drPlus);

                                        //----------------------------------------------------------------------------------------//
                                        //EQUSPStockMoveHist(재고이력 테이블)
                                        drPlus = dtSPStockMoveHist_Plus.NewRow();

                                        drPlus["MoveGubunCode"] = "M04";       //자재출고일 경우는 "M03"
                                        drPlus["DocCode"] = "";
                                        drPlus["MoveDate"] = strUseDate;
                                        drPlus["MoveChargeID"] = strSPUseChargID;
                                        drPlus["PlantCode"] = strPlantCode;
                                        drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        drPlus["EquipCode"] = strEquipCode;
                                        drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                        drPlus["MoveQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                        dtSPStockMoveHist_Plus.Rows.Add(drPlus);

                                        //----------------------------------------------------------------------------------------//
                                        //MASEquipSPBOM 테이블 : 교체처리 : 취소정보는 제외
                                        drBOM = dtEquipSPBOM.NewRow();
                                        drBOM["PlantCode"] = strPlantCode;
                                        drBOM["TransferSPCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();        //새로운 SP
                                        drBOM["TransferQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();

                                        drBOM["ChgEquipCode"] = strEquipCode;
                                        drBOM["ChgSparePartCode"] = this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value.ToString(); // 기존에 있는 SP
                                        drBOM["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                        dtEquipSPBOM.Rows.Add(drBOM);

                                    }
                                }
                                #endregion

                                #region 설비에 투입되어있는(SPBOM) SparePart에 대한 정보
                                if (this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString() != "")
                                {
                                    if (this.uGridUseSP.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit) // 기존 저장된 데이터가 아닌것
                                    {
                                        //EQUSPStock
                                        drPlus = dtSPStock_Plus.NewRow();

                                        drPlus["PlantCode"] = strPlantCode;
                                        drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        drPlus["Qty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                        dtSPStock_Plus.Rows.Add(drPlus);

                                        //----------------------------------------------------------------------------------------//
                                        //EQUSPStockMoveHist(재고이력 테이블)
                                        drPlus = dtSPStockMoveHist_Plus.NewRow();

                                        drPlus["MoveGubunCode"] = "M04";       //자재반납일 경우는 "M04"
                                        drPlus["DocCode"] = "";
                                        drPlus["MoveDate"] = strUseDate;
                                        drPlus["MoveChargeID"] = strSPUseChargID;
                                        drPlus["PlantCode"] = strPlantCode;
                                        drPlus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        drPlus["EquipCode"] = strEquipCode;
                                        drPlus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        drPlus["MoveQty"] = this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        drPlus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                        dtSPStockMoveHist_Plus.Rows.Add(drPlus);


                                    }
                                    else
                                    {
                                        if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true)) // 취소여부 체크한 것만
                                        {
                                            //EQUSPStock
                                            drMinus = dtSPStock_Minus.NewRow();

                                            drMinus["PlantCode"] = strPlantCode;
                                            drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                            drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                            drMinus["Qty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                            drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();
                                            dtSPStock_Minus.Rows.Add(drMinus);

                                            //----------------------------------------------------------------------------------------//
                                            //EQUSPStockMoveHist(재고이력 테이블)
                                            drMinus = dtSPStockMoveHist_Minus.NewRow();

                                            drMinus["MoveGubunCode"] = "M03";       //자재반납일 경우는 "M04"
                                            drMinus["DocCode"] = "";
                                            drMinus["MoveDate"] = strUseDate;
                                            drMinus["MoveChargeID"] = strSPUseChargID;
                                            drMinus["PlantCode"] = strPlantCode;
                                            drMinus["SPInventoryCode"] = this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                            drMinus["EquipCode"] = strEquipCode;
                                            drMinus["SparePartCode"] = this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                            drMinus["MoveQty"] = "-" + this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                            drMinus["UnitCode"] = this.uGridUseSP.Rows[i].Cells["UnitCode"].Value.ToString();

                                            dtSPStockMoveHist_Minus.Rows.Add(drMinus);

                                        }
                                    }
                                }
                                #endregion

                            }

                        }
                    }
                }
                
                if (dtPMUseSP.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "저장정보 확인", "저장데이터가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                #endregion


                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//

                    string strErrRtn = clsPMUseSP.mfSavePMUseSPH(dtPMUseSPH, dtPMUseSP ,dtSPStock_Minus,dtSPStock_Plus,dtSPStockMoveHist_Minus,dtSPStockMoveHist_Plus
                                                                ,dtEquipSPBOM, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    TransErrRtn ErrRtn = new TransErrRtn();

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////


                    this.MdiParent.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ////System ResourceInfo
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //#region 필수입력사항확인
                ////-------------------------------필수 입력확인 , 정보 확인---------------------------------//
                //if (this.uGridUseSP.Rows.Count == 0)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                //    return;
                //}
                //else if (this.uGroupBoxContentsArea.Expanded == false || this.uTextEquipCode.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "입력사항 확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                //    return;
                //}
                //else if (this.uComboSearchPlant.Value.ToString() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    this.uComboSearchPlant.DropDown();
                //    return;
                //}
                //else if (this.uTextUserID.Text == "" || this.uTextUserName.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                               , "확인창", "필수입력사항 확인", "수리출고요청자를 입력해주세요", Infragistics.Win.HAlign.Right);

                //    this.uTextUserID.Focus();
                //    return;
                //}

                //#endregion

                //string strPlantCode = this.uComboSearchPlant.Value.ToString();
                //string strEquipCode = this.uTextEquipCode.Text;
                //string strPMPlanDate = this.uTextPMPlanDate.Text;
                //string strPlanYear = strPMPlanDate.Substring(0, 4);


                ////------------------삭제여부 묻기 -----------------------//
                //if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                //                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                //{

                //    //------------POPUP창 보여줌------------//
                //    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //    Thread t1 = m_ProgressPopup.mfStartThread();
                //    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");

                //    //-------------커서변경------------//
                //    this.MdiParent.Cursor = Cursors.WaitCursor;

                //    //처리 로직//
                //    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMUseSP), "PMUseSP");
                //    QRPEQU.BL.EQUMGM.PMUseSP clsPMUseSP = new QRPEQU.BL.EQUMGM.PMUseSP();
                //    brwChannel.mfCredentials(clsPMUseSP);

                //    string strErrRtn = clsPMUseSP.mfDeletePMUseSP(strPlantCode, strPlanYear, strEquipCode, strPMPlanDate, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                //    TransErrRtn ErrRtn = new TransErrRtn();

                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //    /////////////

                //    //-------------커서변경------------//
                //    this.MdiParent.Cursor = Cursors.Default;

                //    //-------------POPUP창 닫기---------------//
                //    m_ProgressPopup.mfCloseProgressPopup(this);

                //    //----------------처리결과에 따른 메세지 박스 -----------------//
                //    System.Windows.Forms.DialogResult result;
                //    if (ErrRtn.ErrNum == 0)
                //    {
                //        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                //                                    Infragistics.Win.HAlign.Right);
                //    }
                //    else
                //    {
                //        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                //                                     Infragistics.Win.HAlign.Right);
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        public void mfCreate()
        {
            
        }

        
        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //Systme ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMEquip.Rows.Count == 0 && (this.uGridUseSP.Rows.Count == 0 && this.uGridPMResult.Rows.Count == 0)|| this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;

                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridPMEquip.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPMEquip);

                if (this.uGridUseSP.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridUseSP);

                if (this.uGridPMResult.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridPMResult);

                /////////////

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        // GroupContentsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridPMEquip.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridPMEquip.Height = 720;

                    for (int i = 0; i < uGridPMEquip.Rows.Count; i++)
                    {
                        uGridPMEquip.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 새로운줄이 생기면 수리취소 체크박스 수정불가.
        private void uGridUseSP_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            try
            {
                //수리취소 수정불가
                e.Row.Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

            #region 콤보

        //공장이 바뀔때 마다 설비그룹,Area,Station,공정구분이 바뀜
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    //콤보 아이템 클리어
                    //this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();

                    if (this.uGroupBoxContentsArea.Expanded == true)
                    {
                        this.uGroupBoxContentsArea.Expanded = false;
                    }
                    ClearValue();

                    //----- 점검결과정보 클리어-----//
                    while (this.uGridPMEquip.Rows.Count > 0)
                    {
                        this.uGridPMEquip.Rows[0].Delete(false);
                    }

                    ////------------Area콤보 변화-----------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);

                    //DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //wCom.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체",
                    //    "AreaCode", "AreaName", dtArea);

                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCom.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체",
                        "StationCode", "StationName", dtStation);

                    ////-----------설비공정구분콤보 변화 -------------//
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    //QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    //brwChannel.mfCredentials(clsEquipProcGubun);

                    //DataTable dtGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //wCom.mfSetComboEditor(this.uComboSearchProcGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    //    "", "", "전체", "EquipProcGubunCode", "EquipProcGubunName", dtGubun);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Area 선택 전 설비그룹 선택여부 체크
        private void uComboSearchArea_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //if (this.uComboSearchArea.Value.ToString() != "")
                //{
                //    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                //    {
                      
                //        this.uComboSearchEquipGroup.Value = "";

                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station 선택 시 설비위치 설비대분류,설비중분류,설비그룹이 바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboEquipLoc.Items.Clear();

                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //--> 공정구분 선택 전 설비그룹 선택여부 체크 <--//
        private void uComboSearchProcGubun_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //if (this.uComboSearchProcGubun.Value.ToString() != "")
                //{
                //    if (this.uComboSearchEquipGroup.Value.ToString() != "")
                //    {
                        
                //        this.uComboSearchEquipGroup.Value = "";

                //    }
                //}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비위치 선택시 설비대분류,설비중분류,설비그룹이바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLoc.Value.ToString();

                //설비위치가 공백이아닌경우
                if (!strCode.Equals(string.Empty))
                {
                    //현재 텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string strText = this.uComboEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비대분류 선택시 설비중분류 , 설비그룹이바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                if (!strCode.Equals(string.Empty))
                {
                    //현재 텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(),
                                        this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비중분류 선택시 설비그룹이바뀜
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboEquipLargeType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLargeType.Value.ToString();

                if (!strCode.Equals(string.Empty))
                {
                    //현재 텍스트정보와 코드정보를 저장하여 해당콤보의 아이템정보에 존재한다면 검색함
                    string strText = this.uComboEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(),
                                        this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

            #endregion

            #region 텍스트 이벤트
        // 정비사 버튼 클릭 시 유저 정보 보여주기 //
        private void uTextTechnician_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboSearchPlant.Value.ToString();
                //공장정보보기.
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

               
                this.uTextTechnician.Text = frmUser.UserID;
                this.uTextTechnicianName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 정비사 ID입력 후 엔터누를 시 이름 자동입력 //
        private void uTextTechnician_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    
                        this.uTextTechnicianName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strTechnicianID = this.uTextTechnician.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strTechnicianID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "확인창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextTechnician.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "확인창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextTechnicianName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    //존재하지 않은 정보일 경우 메세지 박스후 정비사명 클리어
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "조회결과 확인", "입력하신 ID가 존재하지 않습니다", Infragistics.Win.HAlign.Right);
                        
                        this.uTextTechnicianName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTechnician_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextTechnicianName.Text.Equals(string.Empty))
                this.uTextTechnicianName.Clear();
        }

        //사용담당자 Id입력 후 엔터누를 시 이름 자동입력 //
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    
                        this.uTextUserName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strUserID = this.uTextUserID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strUserID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextTechnician.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "조회결과 확인", "입력하신 ID가 존재하지않습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 사용담당자 버튼 클릭 시 유저 정보 보여주기 //
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();

                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

               

                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextUserName.Text.Equals(string.Empty))
                this.uTextUserName.Clear();
        }

        //행삭제 버튼
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                //System ReosurceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridUseSP.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

            #endregion

            #region 사용SparePart그리드

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.(SparePart)
        private void uGridUseSP_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
               
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridUseSP, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }
                //System ResourceInfo 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //--이벤트 발생한 셀의 컬럼 저장--//
                string strColumn = e.Cell.Column.Key;

                //--체크박스가 수정불가 일시 false로 되돌린다 
                if (strColumn == "Check")
                {
                    if (e.Cell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    {
                        e.Cell.Value = false;
                        return;
                    }
                }
                //-- 수리취소 체크 박스를 체크 해제하면 편집이미지 X --//
                if (strColumn == "CancelFlag")
                {
                    if (e.Cell.Value.ToString() == "False")
                    {
                        e.Cell.Row.RowSelectorAppearance.Image = null;
                        return;
                    }
                }


                #region 구성품수량
                if (strColumn.Equals("CurInputQty"))
                {
                    if (!e.Cell.Value.Equals(0) && !e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty))
                    {
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "구성품수량 확인", "입력 하신 구성품 수량이 기존 수량보다 많습니다.", Infragistics.Win.HAlign.Right);
                            e.Cell.Value = e.Cell.Tag;
                            return;
                        }
                    }
                }
                #endregion

                #region 교체수량

                if (strColumn.Equals("ChgInputQty"))
                {
                    if (!e.Cell.Value.Equals(0) && !e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString().Equals(string.Empty))
                    {
                        //교체수량이 기존수량보다 많거나 사용량이 재고량보다 많을 경우 메세지 박스
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Tag) && !e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "교체수량 확인", "입력 하신 사용량이 재고량이나 기존수량보다 많습니다.", Infragistics.Win.HAlign.Right);

                            //재고량이 기존수량보다 크고 기존수량이 있는 경우
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Tag) 
                                && e.Cell.Row.Cells["CurInputQty"].Tag != null)
                            {
                                e.Cell.Value = e.Cell.Row.Cells["CurInputQty"].Tag;
                            }
                            else
                            {
                                e.Cell.Value = e.Cell.Row.Cells["Qty"].Value;
                            }
                            return;
                        }

                        #region 중복구성품재고구분

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        
                        for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                        {
                            if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridUseSP.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridUseSP.Rows[i].Hidden.Equals(false))
                            {
                                //if (!i.Equals(e.Cell.Row.Index)
                                //    && e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value)
                                //    && e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value))
                                //{
                                if (e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value) && 
                                    e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {
                            
                            string strQty = Convert.ToString(intChgQty - intQty) +" "+e.Cell.Row.Cells["UnitCode"].Value;
                             msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "사용량 확인", "입력하신 구성품의 재고량이 " + strQty + " 초과하였습니다.", Infragistics.Win.HAlign.Right);

                             //e.Cell.Row.Cells["ChgSparePartCode"].Value = string.Empty;
                             //e.Cell.Row.Cells["ChgSparePartName"].Value = string.Empty;
                             //e.Cell.Row.Cells["Qty"].Value = 0;
                             //e.Cell.Row.Cells["Qty"].Tag = 0;
                             e.Cell.Value = 0;
                             //e.Cell.Row.Cells["UnitCode"].Value = string.Empty;

                             return;
                        }


                        #endregion

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //valueList 누르기 전 입력사항 체크
        private void uGridUseSP_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //이벤트발생 컬럼명 저장
                string strColumns = e.Cell.Column.Key;

                WinMessageBox msg = new WinMessageBox();
                
                //System ResourceInfo 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //구성품선택전 헤더에 정보유무
                //if (strColumns.Equals("CurSparePartName"))
                //{
                //    if (this.uTextEquipCode.Text.Equals(string.Empty))
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                            , "확인창", "정보 확인", "", Infragistics.Win.HAlign.Right);
                //        e.Cancel = true;
                //        return;
                //    }
                //}

                //교체구성품 선택 전
                //if (strColumns.Equals("ChgSparePartName"))
                //{
                //    if (e.Cell.Row.Cells["CurSparePartName"].Value.ToString().Equals(string.Empty))
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                            , "확인창", "구성품정보 확인", "구성품을 선택해주세요.", Infragistics.Win.HAlign.Right);

                //        //DropDown
                //        this.uGridUseSP.ActiveCell = e.Cell.Row.Cells["CurSparePartName"];
                //        this.uGridUseSP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                //        return;
                //    }
                //}

               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }        

        //--더블 클릭시 발생 해당셀의 상세정보를 ExPandGroupBox 에서 보여준다.
        private void uGridPMEquip_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (!grd.mfCheckCellDataInRow(this.uGridUseSP, 0, e.Cell.Row.Index))
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                    }
                    e.Cell.Row.Fixed = true;
                    
                    //필요한 값저장
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    string strPMPlanDate = e.Cell.Row.Cells["PMPlanDate"].Value.ToString();
                    string strPlanYear = strPMPlanDate.Substring(0, 4);

                    //BL호출

                    #region 설비 구성품 , 창고 설정 , 헤더 설정
                    // 설비 구성품 정보 가지고옴
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChannel.mfCredentials(clsEquipSPBOM);
                    DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));


                    //--- 해당 설비의 SP 정보에 따라 처리 ---//


                    this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();

                    string strValue = "SparePartCode,SparePartName,InputQty,UnitName,Spec,Maker";
                    string strText = "구성품코드,구성품명,Qty,단위,Spec,Maker";

                    grd.mfSetGridColumnValueGridList(this.uGridUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "SparePartCode", "SparePartName", dtEquipSPBOM);

                    // SP 창고정보
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                    QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                    brwChannel.mfCredentials(clsSPInventory);
                    DataTable dtSPInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    grd.mfSetGridColumnValueList(this.uGridUseSP, 0, "ChgSPInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                        , "", "", dtSPInventory);


                    //----- 상위 그리드 값 텍스트에 보여주기 -----//
                    this.uTextEquipCode.Text = strEquipCode;
                    this.uTextEquipName.Text = e.Cell.Row.Cells["EquipName"].Value.ToString();
                    this.uTextPMPlanDate.Text = strPMPlanDate;
                    if (e.Cell.Row.Cells["PMEndDate"].Value.ToString() != "")
                    {
                        this.uDateUsingDate.Value = e.Cell.Row.Cells["PMEndDate"].Value;
                    }
                    if (e.Cell.Row.Cells["SPUseChargeID"].Value.ToString() != "")
                    {
                        this.uTextUserID.Text = e.Cell.Row.Cells["SPUseChargeID"].Value.ToString();
                        this.uTextUserName.Text = e.Cell.Row.Cells["SPUseChargeName"].Value.ToString();
                    }
                    this.uTextEtcDesc.Text = e.Cell.Row.Cells["SPUseEtcDesc"].Value.ToString();

                    #endregion

                    //공장정보저장
                    this.uTextEquipCode.Tag = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                    #region 사용SparePart그리드
                    //------ 해당 설비의 SP 정보 조회 -------//
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMUseSP), "PMUseSP");
                    QRPEQU.BL.EQUMGM.PMUseSP clsPMUseSP = new QRPEQU.BL.EQUMGM.PMUseSP();
                    brwChannel.mfCredentials(clsPMUseSP);

                    DataTable dtPMUseSP = clsPMUseSP.mfReadPMUseSP(strPlantCode, strEquipCode, strPMPlanDate, m_resSys.GetString("SYS_LANG"));

                    //-- SP 데이터 바인드 --//
                    this.uGridUseSP.DataSource = dtPMUseSP;
                    this.uGridUseSP.DataBind();

                    //정보가 있을 경우 바인드 된 컬럼 수정 불가 가능 처리
                    if (dtPMUseSP.Rows.Count > 0)
                    {
                        grd.mfSetAutoResizeColWidth(this.uGridUseSP, 0);
                        for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                        {
                            //this.uGridUseSP.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                            this.uGridUseSP.Rows[i].Cells["CurSparePartName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["CurInputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["ChgSparePartName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["ChgInputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["UnitCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridUseSP.Rows[i].Cells["ChgDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                            //수리취소 된줄이있으면 색변경
                            if (Convert.ToBoolean(this.uGridUseSP.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                            {
                                this.uGridUseSP.Rows[i].Appearance.BackColor = Color.Yellow;
                                this.uGridUseSP.Rows[i].Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        }
                    }

                    #endregion

                    #region 점검결과 그리드

                    //--점검결과 조회 --//
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMResult), "PMResult");
                    QRPEQU.BL.EQUMGM.PMResult clsPMResult = new QRPEQU.BL.EQUMGM.PMResult();
                    brwChannel.mfCredentials(clsPMResult);

                    DataTable dtPMResult = clsPMResult.mfReadPMResult(strPlantCode, strPlanYear, strEquipCode, strPMPlanDate, m_resSys.GetString("SYS_LANG"));

                    //--점검결과 데이터 바인드 --//
                    this.uGridPMResult.DataSource = dtPMResult;
                    this.uGridPMResult.DataBind();

                    if (dtPMResult.Rows.Count > 0)
                    {
                        grd.mfSetAutoResizeColWidth(this.uGridPMResult, 0);
                    }
                    #endregion

                    ////else
                    ////{
                    ////    WinMessageBox msg = new WinMessageBox();
                    ////    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                            , "확인창", "설비정보 확인", "해당설비에 대한 SpareaPart BOM정보가 없습니다. 설비관리기준정보 - 설비구성품정보로 등록하세요.", Infragistics.Win.HAlign.Right);

                    ////    if (this.uGroupBoxContentsArea.Expanded == true)
                    ////    {
                    ////        this.uGroupBoxContentsArea.Expanded = false;
                    ////    }
                    ////}
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //그리드 내에 있는 콤보박스의 선택 정보를 읽음
        private void uGridUseSP_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //컬럼저장
                string strColumns = e.Cell.Column.Key;

                //선택한 리스트의 Value값과 Text값 저장
                string strValue = "";
                string strText = "";

                //ColumValueList 인지 CellValueList인지에 따라 저장
                if (e.Cell.Column.ValueList != null)
                {
                    strValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strText = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strValue = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strText = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }

                //BL호출
                //공장정보,설비코드 저장
                string strPlantCode = this.uTextEquipCode.Tag.ToString();
                string strEquipCode = this.uTextEquipCode.Text;

                #region 구성품명

                if (strColumns.Equals("CurSparePartName"))
                {
                    //Key 값이 공백이 아닐경우
                    if (!strValue.Equals(string.Empty))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                        QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                        brwChannel.mfCredentials(clsEquipSPBOM);

                        DataTable dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, strValue, m_resSys.GetString("SYS_LANG"));

                        //정보가 있을 경우
                        if (dtSPBOM.Rows.Count > 0)
                        {
                            //단위저장 
                            e.Cell.Row.Cells["CurSparePartCode"].Tag = dtSPBOM.Rows[0]["UnitCode"];

                            e.Cell.Row.Cells["CurSparePartCode"].Value = strValue;

                            e.Cell.Row.Cells["CurInputQty"].Tag = Convert.ToInt32(dtSPBOM.Rows[0]["InputQty"]) - Convert.ToInt32(dtSPBOM.Rows[0]["ChgStandbyQty"]);
                            e.Cell.Row.Cells["CurInputQty"].Value = Convert.ToInt32(dtSPBOM.Rows[0]["InputQty"]) - Convert.ToInt32(dtSPBOM.Rows[0]["ChgStandbyQty"]);
                            e.Cell.Row.Cells["CurSpec"].Value = dtSPBOM.Rows[0]["Spec"];
                            e.Cell.Row.Cells["CurMaker"].Value = dtSPBOM.Rows[0]["Maker"];

                            //선택한 정보가 그리드내에 있으면 다시입력
                            for (int i = 0; i < this.uGridUseSP.Rows.Count; i++)
                            {
                                if (this.uGridUseSP.Rows[i].RowSelectorAppearance.Image != null 
                                    && this.uGridUseSP.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                    && this.uGridUseSP.Rows[i].Hidden.Equals(false))
                                {
                                    if (!i.Equals(e.Cell.Row.Index) &&
                                        this.uGridUseSP.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(strValue))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "구성품 확인", "이미" + this.uGridUseSP.Rows[i].RowSelectorNumber + "번째 열에 입력한 구성품 입니다.", Infragistics.Win.HAlign.Right);

                                        //단위저장 
                                        e.Cell.Row.Cells["CurSparePartCode"].Tag = string.Empty;
                                        
                                        e.Cell.Row.Cells["CurSparePartCode"].Value = string.Empty;
                                        e.Cell.Row.Cells["CurSparePartName"].Value = string.Empty;

                                        e.Cell.Row.Cells["CurInputQty"].Tag = 0;
                                        e.Cell.Row.Cells["CurInputQty"].Value = 0;
                                        
                                        e.Cell.Row.Cells["CurSpec"].Value = string.Empty;
                                        e.Cell.Row.Cells["CurMaker"].Value = string.Empty;

                                        return;
                                    }
                                }
                            }

                            if (Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Value))
                            {
                                e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                            }

                        }
                    }

                }

                #endregion

                #region SP창고

                if (strColumns.Equals("ChgSPInventoryCode"))
                {
                    //SP창고가 공백이 아닌경우
                    if (!strValue.Equals(string.Empty))
                    {
                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);
                        
                        //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                        DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strValue, m_resSys.GetString("SYS_LANG"));

                        if (dtChgSP.Rows.Count > 0)
                        {
                            string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                            string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                            WinGrid wGrid = new WinGrid();

                            //GridList 그리드에 삽입
                            wGrid.mfSetGridCellValueGridList(this.uGridUseSP, 0, e.Cell.Row.Index, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                                , "SparePartCode", "SparePartName", dtChgSP);
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, "", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "SparePart정보 확인", "해당창고의 SparePart가 없습니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                            return;
                        }

                    }
                }

                #endregion

                #region 사용구성품

                if (strColumns.Equals("ChgSparePartName"))
                {
                    if (!strValue.Equals(string.Empty))
                    {
                        //SP현재고 정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                        QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                        brwChannel.mfCredentials(clsSPStock);

                        //SP현재고 조회
                        DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString(), strValue, m_resSys.GetString("SYS_LANG"));

                        if (dtSPStock.Rows.Count > 0)
                        {
                            //기존구성품코드가 있는 상태에서 선택한 SP의 단위와 기존SP의 단위가 틀리면 리턴
                            if (!e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty) && !e.Cell.Row.Cells["CurSparePartCode"].Tag.Equals(dtSPStock.Rows[0]["UnitCode"]))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "확인창", "단위 확인", "선택하신 사용구성품의 단위와 기존구성품의 단위가 맞지않습니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                                return;
                            }

                            //선택 구성품이 바뀔 때 교체수량이 있을 경우 초기화
                            if (!e.Cell.Row.Cells["ChgInputQty"].Value.ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                            }

                            e.Cell.Row.Cells["ChgSparePartCode"].Value = dtSPStock.Rows[0]["SparePartCode"];
                            e.Cell.Row.Cells["Qty"].Value = dtSPStock.Rows[0]["Qty"];
                            e.Cell.Row.Cells["Qty"].Tag = dtSPStock.Rows[0]["Qty"];

                            e.Cell.Row.Cells["UnitCode"].Value = dtSPStock.Rows[0]["UnitCode"];
                            e.Cell.Row.Cells["ChgSpec"].Value = dtSPStock.Rows[0]["Spec"];
                            e.Cell.Row.Cells["ChgMaker"].Value = dtSPStock.Rows[0]["Maker"];
                            
                        }
                        else
                        {

                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

            #endregion
        
        #endregion

        #region 구성품,재고
        /// <summary>
        /// 그리드안 설비구성품 콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        private void SearchSPBOM(string strPlantCode,string strEquipCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // 설비 구성품 정보 가지고옴
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);
                DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                this.uGridUseSP.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();

                //--- 해당 설비의 SP 정보에 따라 처리 ---//
                if (dtEquipSPBOM.Rows.Count > 0)
                {
                    string strValue = "SparePartCode,SparePartName,InputQty,UnitName,Spec,Maker";
                    string strText = "구성품코드,구성품명,Qty,단위,Spec,Maker";

                    grd.mfSetGridColumnValueGridList(this.uGridUseSP, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "SparePartCode", "SparePartName", dtEquipSPBOM);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
                
        }

        /// <summary>
        /// 그리드안 창고재고 콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strInventoryCode">창고</param>
        /// <param name="intIndex">줄번호</param>
        private void SearchSPStock(string strPlantCode,string strInventoryCode, int intIndex)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //SP현재고 정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                if (dtChgSP.Rows.Count > 0)
                {
                    string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                    string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                    WinGrid wGrid = new WinGrid();

                    //GridList 그리드에 삽입
                    wGrid.mfSetGridCellValueGridList(this.uGridUseSP, 0, intIndex, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                        , "SparePartCode", "SparePartName", dtChgSP);
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "SparePart정보 확인", "해당창고의 SparePart가 없습니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");

                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }








    }
}
