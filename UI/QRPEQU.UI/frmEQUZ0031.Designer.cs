﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0031));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchFromMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchYear = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUesr = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSearchEquipInfo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).BeginInit();
            this.uGroupSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchFromMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSearchEquipInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupSearchArea
            // 
            this.uGroupSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupSearchArea.Appearance = appearance1;
            this.uGroupSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupSearchArea.Controls.Add(this.uTextUserName);
            this.uGroupSearchArea.Controls.Add(this.uTextUserID);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchFromMonth);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipLargeType);
            this.uGroupSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchYear);
            this.uGroupSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelUesr);
            this.uGroupSearchArea.Controls.Add(this.uLabelEquipLoc);
            this.uGroupSearchArea.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupSearchArea.Controls.Add(this.uLabelEquipGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupSearchArea.Name = "uGroupSearchArea";
            this.uGroupSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupSearchArea.TabIndex = 2;
            // 
            // uTextUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance2;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(772, 56);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 2;
            // 
            // uTextUserID
            // 
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton1);
            this.uTextUserID.Location = new System.Drawing.Point(668, 56);
            this.uTextUserID.MaxLength = 20;
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 12;
            this.uTextUserID.ValueChanged += new System.EventHandler(this.uTextUserID_ValueChanged);
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uComboSearchFromMonth
            // 
            this.uComboSearchFromMonth.Location = new System.Drawing.Point(484, 8);
            this.uComboSearchFromMonth.MaxLength = 4;
            this.uComboSearchFromMonth.Name = "uComboSearchFromMonth";
            this.uComboSearchFromMonth.Size = new System.Drawing.Size(56, 21);
            this.uComboSearchFromMonth.TabIndex = 3;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(380, 56);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipGroup.TabIndex = 11;
            // 
            // uComboSearchEquipLargeType
            // 
            this.uComboSearchEquipLargeType.Location = new System.Drawing.Point(116, 56);
            this.uComboSearchEquipLargeType.MaxLength = 50;
            this.uComboSearchEquipLargeType.Name = "uComboSearchEquipLargeType";
            this.uComboSearchEquipLargeType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLargeType.TabIndex = 10;
            this.uComboSearchEquipLargeType.ValueChanged += new System.EventHandler(this.uComboSearchEquType_ValueChanged);
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(668, 32);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboProcessGroup.TabIndex = 9;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(380, 32);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLoc.TabIndex = 8;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(116, 32);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchStation.TabIndex = 7;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uComboSearchYear
            // 
            this.uComboSearchYear.Location = new System.Drawing.Point(380, 8);
            this.uComboSearchYear.MaxLength = 5;
            this.uComboSearchYear.Name = "uComboSearchYear";
            this.uComboSearchYear.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchYear.TabIndex = 2;
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(564, 32);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 0;
            // 
            // uLabelUesr
            // 
            this.uLabelUesr.Location = new System.Drawing.Point(564, 56);
            this.uLabelUesr.Name = "uLabelUesr";
            this.uLabelUesr.Size = new System.Drawing.Size(100, 20);
            this.uLabelUesr.TabIndex = 0;
            // 
            // uLabelEquipLoc
            // 
            this.uLabelEquipLoc.Location = new System.Drawing.Point(276, 32);
            this.uLabelEquipLoc.Name = "uLabelEquipLoc";
            this.uLabelEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLoc.TabIndex = 0;
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(12, 56);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 0;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(276, 56);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroup.TabIndex = 0;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(12, 32);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 0;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(276, 8);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 0;
            // 
            // uGridSearchEquipInfo
            // 
            this.uGridSearchEquipInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSearchEquipInfo.DisplayLayout.Appearance = appearance7;
            this.uGridSearchEquipInfo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSearchEquipInfo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSearchEquipInfo.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSearchEquipInfo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.uGridSearchEquipInfo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSearchEquipInfo.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGridSearchEquipInfo.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSearchEquipInfo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSearchEquipInfo.DisplayLayout.Override.ActiveCellAppearance = appearance15;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSearchEquipInfo.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridSearchEquipInfo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSearchEquipInfo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSearchEquipInfo.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSearchEquipInfo.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridSearchEquipInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSearchEquipInfo.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSearchEquipInfo.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance14.TextHAlignAsString = "Left";
            this.uGridSearchEquipInfo.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridSearchEquipInfo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSearchEquipInfo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridSearchEquipInfo.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridSearchEquipInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSearchEquipInfo.DisplayLayout.Override.TemplateAddRowAppearance = appearance11;
            this.uGridSearchEquipInfo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSearchEquipInfo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSearchEquipInfo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSearchEquipInfo.Location = new System.Drawing.Point(0, 120);
            this.uGridSearchEquipInfo.Name = "uGridSearchEquipInfo";
            this.uGridSearchEquipInfo.Size = new System.Drawing.Size(1070, 700);
            this.uGridSearchEquipInfo.TabIndex = 3;
            // 
            // frmEQUZ0031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridSearchEquipInfo);
            this.Controls.Add(this.uGroupSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0031";
            this.Load += new System.EventHandler(this.frmEQUZ0031_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0031_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).EndInit();
            this.uGroupSearchArea.ResumeLayout(false);
            this.uGroupSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchFromMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSearchEquipInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchFromMonth;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelUesr;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSearchEquipInfo;
    }
}