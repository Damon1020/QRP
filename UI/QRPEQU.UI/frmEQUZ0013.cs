﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0013.cs                                        */
/* 프로그램명   : 반출승인                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-08 : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0013 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        public frmEQUZ0013()
        {
            InitializeComponent();
        }

        private void frmEQUZ0013_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0013_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // Title 제목
            titleArea.mfSetLabelText("반출승인", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitLabel();
            InitGrid();
            InitCombo();
            InitText();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 승인자 TextBox에 사용자 ID 기본값 설정
                this.uTextAdmitID.Text = m_resSys.GetString("SYS_USERID");

                //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //brwChannel.mfCredentials(clsUser);

                //DataTable dtUser = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                this.uTextAdmitName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchDay, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelAdmitDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAdmiter, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCarryOutAdmit, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Extended, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "AdmitDeptName", "승인자부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "CarryOutAdmitID", "승인자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "CarryOutAdmitName", "승인자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "DocCode", "발행번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "DeptName", "반출부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "CarryOutChargeName", "반출담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "EquipCarryOutGubunName", "반출구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 5
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "EquipCarryInGubunName", "반입구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 5
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "CarryInEstDate", "반입예정일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "CarryOutReason", "반출사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 230, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "VendorName", "반출처", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "ItemCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "ItemName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "CarryOutQty", "반출수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCarryOutAdmit, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                this.uGridCarryOutAdmit.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCarryOutAdmit.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                // 공장 정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinComboEditor wCom = new QRPCOM.QRPUI.WinComboEditor();

                //가져온 공장정보 콤보에 추가
                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //-----------------------필수 입력사항 확인------------------//
                if (Convert.ToDateTime(this.uDateFromDay.Value) > Convert.ToDateTime(this.uDateToDay.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "시작일이 종료일보다 많습니다.", Infragistics.Win.HAlign.Right);
                    this.uDateFromDay.DropDown();
                }

                //-- 검색에 필요한 값 저장 --/
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(this.uDateFromDay.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(this.uDateToDay.Value).ToString("yyyy-MM-dd");

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //  BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryOutH), "CarryOutH");
                QRPEQU.BL.EQUCAR.CarryOutH clsCarryOutH = new QRPEQU.BL.EQUCAR.CarryOutH();
                brwChannel.mfCredentials(clsCarryOutH);

                //처리 로직//
                DataTable dtCarryOut = clsCarryOutH.mfReadCarryOutAdmit(strPlantCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //-- 데이터 바인드 --
                this.uGridCarryOutAdmit.DataSource = dtCarryOut;
                this.uGridCarryOutAdmit.DataBind();
                
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtCarryOut.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                                            Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //------------------- 필수 입력사항 확인 --------------------------//
                if (this.uGridCarryOutAdmit.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "승인할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    
                    return;
                }
                if(this.uDateAdmitDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "승인일을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    this.uDateAdmitDate.DropDown();
                    return;
                }
                else if (this.uTextAdmitID.Text.Trim() == "" || this.uTextAdmitName.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "승인자를 입력해주세요.", Infragistics.Win.HAlign.Right);

                    this.uTextAdmitID.Focus();
                    return;
                }

                //  BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryOutH), "CarryOutH");
                QRPEQU.BL.EQUCAR.CarryOutH clsCarryOutH = new QRPEQU.BL.EQUCAR.CarryOutH();
                brwChannel.mfCredentials(clsCarryOutH);

                DataTable dtCarryOutAdmit = clsCarryOutH.mfSetDataInfoAdmit();

                //--------그리드 줄이 0 이상일 경우 편집이미지가 null이아니고 체크된 정보만 저장한다. -----//
                if(this.uGridCarryOutAdmit.Rows.Count > 0)
                {
                    for(int i=0;i<this.uGridCarryOutAdmit.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridCarryOutAdmit.ActiveCell = this.uGridCarryOutAdmit.Rows[0].Cells[0];

                        if (this.uGridCarryOutAdmit.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(this.uGridCarryOutAdmit.Rows[i].Cells["Check"].Value) == true)
                            {
                                DataRow drAdmit;
                                drAdmit = dtCarryOutAdmit.NewRow();
                                drAdmit["PlantCode"] = this.uGridCarryOutAdmit.Rows[i].Cells["PlantCode"].Value.ToString();
                                drAdmit["DocCode"] = this.uGridCarryOutAdmit.Rows[i].Cells["DocCode"].Value.ToString();
                                drAdmit["Seq"] = this.uGridCarryOutAdmit.Rows[i].Cells["Seq"].Value.ToString();
                                drAdmit["AdmitID"] = this.uTextAdmitID.Text;
                                drAdmit["AdmitDate"] = Convert.ToDateTime(this.uDateAdmitDate.Value).ToString("yyyy-MM-dd");
                                dtCarryOutAdmit.Rows.Add(drAdmit);
                            }
                            else
                            {
                                
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "입력사항확인", "승인할 정보가 없습니다.", Infragistics.Win.HAlign.Right);


                                return;
                                
                            }
                        }
                        
                    }

                    if (dtCarryOutAdmit.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "입력사항확인", "반출승인정보가 없습니다.", Infragistics.Win.HAlign.Right);
                        return;
                    }

                }

                //-------저장 여부 메세지 보여준다 ------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "선택한 정보를 저장(승인)하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //-- 팝업창을 보여준다 --//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    // --- 커서변결 ---//
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    

                    //처리 로직//
                    // 매서드 호출
                    string strErrRtn = clsCarryOutH.mfSaveCarryOutAdmit(dtCarryOutAdmit, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //-- 처리 결과 판단 --//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    //--커서변경--//
                    this.MdiParent.Cursor = Cursors.Default;
                    //--- 팝업창을 띄운다 --//
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    // 저장 처리 여부에 따라 메세지를 보여준다.
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "저장처리결과", "선택한 정보를 성공적으로 저장(승인)했습니다.",
                                   Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "저장처리결과", "선택한 정보를 저장(승인)하지 못했습니다.",
                                    Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {

        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {

        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread t1 = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                //Systme resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridCarryOutAdmit.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridCarryOutAdmit);

                /////////////

                //this.MdiParent.Cursor = Cursors.Default;
                //m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGridCarryOutAdmit_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        private void uGridCarryOutAdmit_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            string strColumn = e.Cell.Column.Key;

            try
            {
                //-- 선택된 줄의 승인자와 승인자 텍스트의 아이디가 다를 경우 메세지박스를 띄운다.(체크박스 False로 되돌리고 편집이미지 삭제.)
                if (strColumn == "Check")
                {
                    if (Convert.ToBoolean(e.Cell.Value) == true)
                    {
                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                        if (this.uTextAdmitID.Text.Trim() != e.Cell.Row.Cells["CarryOutAdmitID"].Value.ToString())
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "입력사항확인", "승인자를 확인해주세요.", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = false;
                            e.Cell.Row.RowSelectorAppearance.Image = null;
                            return;
                        }
                        else
                        {
                            if (this.uTextAdmitName.Text.Trim() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "입력사항확인", "승인자를 확인해주세요.", Infragistics.Win.HAlign.Right);

                                e.Cell.Value = false;
                                e.Cell.Row.RowSelectorAppearance.Image = null;
                                return;
                            }
                        }
                    }
                }

                // Cell 수정시 RowSelector 이미지 변경
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridCarryOutAdmit, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //-- 승인자ID 입력 후 엔터를 누를 시 자동 이름 생성 
        private void uTextAdmitID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextAdmitName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //승인자자 공장코드 저장
                    string strCarryOutAdmitID = this.uTextAdmitID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //----- 아이디 공백 확인
                    if (strCarryOutAdmitID == "" )
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextAdmitID.Focus();
                        return;
                    }
                        //-- 공장 확인
                    else if (strPlantCode == "" && this.uTextAdmitName.Text.Trim() != "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //-- 유저정보 가져오기 
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCarryOutAdmitID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        // 이름 텍스트에 유저 이름을 삽입한다.
                        this.uTextAdmitName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "조회결과 확인", "입력하신 ID가 존재하지 않습니다.", Infragistics.Win.HAlign.Right);

                        this.uTextAdmitName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 승인자 버튼을 클릭 시 유저정보 폼을 띄운다
        private void uTextAdmitID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                //-- 상속된폼 띄우기 -- //
                QRPEQU.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                // -- 상속된폼에서 선택된 정보 중 아이디와 이름을 가져와 각텍스트에 넣는다
                this.uTextAdmitID.Text = frmUser.UserID;
                this.uTextAdmitName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmEQUZ0013_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }
    }
}
