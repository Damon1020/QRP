﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmEQUZ0004.cs                                        */
/* 프로그램명   : 품종교체등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-26 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0004 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmEQUZ0004()
        {
            InitializeComponent();
        }
        private void frmEQUZ0004_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
            //검색툴
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, true, true, m_rssSys.GetString("SYS_USERID"), this.Name);

        }
        private void frmEQUZ0004_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
             ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정 
            titleArea.mfSetLabelText("품종교체점검등록", m_rssSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();

            //컨트롤초기화
            InitLable();
            InitGrid();
            InitGroupbox();
            InitText();
            InitCombo();

            // ExpandGroupBox를 숨김상태로 바꾼다 
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLable()
        {
            //Syste ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                WinLabel lbl = new WinLabel();
              
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSysCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSysName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSuperSys, "Super설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLocation, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysGroupName, "설비그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSTSStock, "STS입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelYear, "제작년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysGrade, "설비등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel1, "품종교체문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel2, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel3, "작업조", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel4, "교체점검일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel5, "교체점검자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel6, "CSS의뢰여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel7, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel8, "교체점검항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel9, "교체내역", m_resSys.GetString("SYS_FONTNAME"), true, false);
               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트
        /// </summary>
        private void InitText()
        {
            try
            {
                //Syste ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strUserID = m_resSys.GetString("SYS_USERID");

                this.uTextDeviceChgUserID.Text = strUserID;
                this.uTextDeviceChgUserName.Text = m_resSys.GetString("SYS_USERNAME");
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                
                QRPBrowser brwChannel = new QRPBrowser();
                
                //-- 공장 콤보 --//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();
                   //-- Expand 안 공장콤보 --//
                wCom.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

                // - 검색조건의 공장 코드 -//
                wCom.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center,
                    m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtWork = clsCommonCode.mfReadCommonCode("C0025", m_resSys.GetString("SYS_LANG"));

                wCom.mfSetComboEditor(this.uComboWorkGubun, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtWork);


                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //Syste ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                // 품종교체점검항목 헤더
                grd.mfInitGeneralGrid(this.uGridDeviceChgResultH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "DeviceChgDocCode", "품종교체문서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "EquipName", "설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50,
                     Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                      Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10,
                     Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                      Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "ProcessName", "공정명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50,
                     Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "ChgCheckDate", "교체점검일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "WorkGubunCode", "작업조코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "WorkGubunName", "작업조", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                     Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "ChgCheckID", "교체점검자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "ChgCheckName", "교체점검자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "CCSReqFlag", "CCS의뢰여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultH, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridDeviceChgResultH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeviceChgResultH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //교체점검항목
                grd.mfInitGeneralGrid(this.uGridDeviceChgResultD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "PMInspectGubun", "점검항목구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "PMInspectCriteria", "점검기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "LevelCode", "난이도코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "LevelName", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "DeviceChgPointCode", "점검Point", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "PMStatus", "상태", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "ActionDesc", "Action사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgResultD, 0, "FinalStatus", "최종상태", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                //Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridDeviceChgResultD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeviceChgResultD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                grd.mfAddRowGrid(this.uGridDeviceChgResultD, 0);

                //교체내역
                grd.mfInitGeneralGrid(this.uGridDurableMatRepairChg, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns, false
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridDurableMatRepairChg, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridDurableMatRepairChg, 0, "RepairGIGubunName", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatRepairChg, 0, "CurDurableMatName", "원금형치공구", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatRepairChg, 0, "ChgDurableMatName", "교체금형치공구", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatRepairChg, 0, "ChgQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridDurableMatRepairChg, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 160, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridDurableMatRepairChg.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMatRepairChg.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                grd.mfAddRowGrid(this.uGridDurableMatRepairChg, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupbox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBox, GroupBoxType.INFO, "설비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                    Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                
                //폰트설정
                uGroupBox.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox.Appearance.FontData.SizeInPoints = 9;
                
                grpbox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "품종교체등록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 툴바관련

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                


                //--- 검색조건에 있는 정보 저장 ---//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipCode = "";

                //설비코드가 입력되어 있으면 설비코드 저장 //
                if (this.uTextSearchEquipCode.Text != "" && this.uTextSearchEquipName.Text != "")
                {
                    strEquipCode = this.uTextSearchEquipCode.Text;
                }
                // -- 팝업창을 띄운다--//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //-- 커서변경 --//
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // -- 품종교체점검등록 헤더 BL 호출 //
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultH), "DeviceChgResultH");
                QRPEQU.BL.EQUCCS.DeviceChgResultH clsDeviceChgResultH = new QRPEQU.BL.EQUCCS.DeviceChgResultH();
                brwChannel.mfCredentials(clsDeviceChgResultH);

                // -- 품종교체점검등록 헤더 조회 매서드 호출 --//
                DataTable dtDeviceChgResult = clsDeviceChgResultH.mfReadDeviceChgResultH(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                // - 그리드에 바인드 -//
                this.uGridDeviceChgResultH.DataSource = dtDeviceChgResult;
                this.uGridDeviceChgResultH.DataBind();

                //-- 커서기본값으로 변경 --//
                this.MdiParent.Cursor = Cursors.Default;

                //-- 팝업창을 닫느다.--//
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtDeviceChgResult.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false || this.uGridDeviceChgResultD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "입력사항확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }


                #region 필수 입력사항

                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "확인창", "필수입력사항확인","공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    // Dropdown
                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextEquipCode.Text == "" || this.uTextEquipName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "필수입력사항확인", "설비코드를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextEquipCode.Focus();
                    return;
                }
                else if (this.uComboProcess.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "필수입력사항확인", "공정을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    // DropDown
                    this.uComboProcess.DropDown();
                    return;
                }
                else if (this.uTextDeviceChgUserID.Text == "" || this.uTextDeviceChgUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "필수입력사항확인", "교체점검자를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextDeviceChgUserID.Focus();
                    return;
                }
                else if (this.uComboWorkGubun.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "필수입력사항확인", "작업조를 선택해주세요.", Infragistics.Win.HAlign.Right);
                    // DropDown
                    this.uComboWorkGubun.DropDown();
                    return;
                }
                //else if (this.uOptionCCSFlag.Value == null)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                         "확인창", "필수입력사항확인", "CSS의뢰여부를 선택해주세요.", Infragistics.Win.HAlign.Right);
                //    this.uOptionCCSFlag.Focus();
                //    return;
                //}

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                #endregion

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                // 품종교체점검등록 헤더 BL 호출 //
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultH), "DeviceChgResultH");
                QRPEQU.BL.EQUCCS.DeviceChgResultH clsDeviceChgResultH = new QRPEQU.BL.EQUCCS.DeviceChgResultH();
                brwChannel.mfCredentials(clsDeviceChgResultH);

                // 품종교체점검등록 헤더 컬럼정보 매서드 호출 //
                DataTable dtDeviceChgResultH = clsDeviceChgResultH.mfSetDataInfo();

                //품종교체점검등록 상세 BL호출 //
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultD), "DeviceChgResultD");
                QRPEQU.BL.EQUCCS.DeviceChgResultD clsDeviceChgResultD = new QRPEQU.BL.EQUCCS.DeviceChgResultD();
                brwChannel.mfCredentials(clsDeviceChgResultH);

                // 품종교체점검등록 상세 컬럼정보 매서드 호출 //
                DataTable dtDeviceChgResultD = clsDeviceChgResultD.mfSetDataInfo();

                //공장정보 저장 //
                string strPlantCode = this.uComboPlant.Value.ToString();

                
                // 품종교체점검등록 헤더 정보 저장 //
                DataRow drH;
                drH = dtDeviceChgResultH.NewRow();
                drH["PlantCode"] = strPlantCode;
                drH["DeviceChgDocCode"] = this.uTextDeviceChgDocCode.Text;
                drH["EquipCode"] = this.uTextEquipCode.Text;
                drH["ProcessCode"] = this.uComboProcess.Value.ToString();
                drH["ChgCheckDate"] = this.uDateDeviceChgDate.DateTime.Date.ToString("yyyy-MM-dd");
                drH["WorkGubunCode"] = this.uComboWorkGubun.Value.ToString();
                drH["ChgCheckID"] = this.uTextDeviceChgUserID.Text;
                drH["StdNumber"] = this.uTextStdNumber.Text;
                drH["VersionNum"] = this.uTextVersionNum.Text;
                drH["CCSReqFlag"] = "False"; //this.uOptionCCSFlag.Value.ToString(); 
                drH["EtcDesc"] = this.uTextEtcDesc.Text;
                dtDeviceChgResultH.Rows.Add(drH);

                //-- 품종교체점검등록 상세 정보 저장 --//
                if (this.uGridDeviceChgResultD.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridDeviceChgResultD.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridDeviceChgResultD.ActiveCell = this.uGridDeviceChgResultD.Rows[0].Cells[0];

                        if (this.uGridDeviceChgResultD.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            DataRow drD;
                            drD = dtDeviceChgResultD.NewRow();
                            drD["PlantCode"]= strPlantCode;
                            drD["Seq"] = this.uGridDeviceChgResultD.Rows[i].Cells["Seq"].Value.ToString();
                            drD["PMInspectGubun"] = this.uGridDeviceChgResultD.Rows[i].Cells["PMInspectGubun"].Value.ToString(); ;
                            drD["PMInspectName"] = this.uGridDeviceChgResultD.Rows[i].Cells["PMInspectName"].Value.ToString(); ;
                            drD["PMInspectCriteria"] = this.uGridDeviceChgResultD.Rows[i].Cells["PMInspectCriteria"].Value.ToString(); ;
                            drD["LevelCode"] = this.uGridDeviceChgResultD.Rows[i].Cells["LevelCode"].Value.ToString(); ;
                            drD["DeviceChgPointCode"] = this.uGridDeviceChgResultD.Rows[i].Cells["DeviceChgPointCode"].Value.ToString(); ;
                            drD["PMStatus"] = this.uGridDeviceChgResultD.Rows[i].Cells["PMStatus"].Value.ToString(); ;
                            drD["ActionDesc"] = this.uGridDeviceChgResultD.Rows[i].Cells["ActionDesc"].Value.ToString(); ;
                            drD["FinalStatus"] = this.uGridDeviceChgResultD.Rows[i].Cells["FinalStatus"].Value.ToString(); ;
                            dtDeviceChgResultD.Rows.Add(drD);
                        }
                    }
                }

                // 표준번호  개정번호 저장 //
                if (this.uTextStdNumber.Text == "")
                {
                    dtDeviceChgResultH.Rows[0]["StdNumber"] = this.uGridDeviceChgResultD.Rows[0].Cells["StdNumber"].Value.ToString();
                    dtDeviceChgResultH.Rows[0]["VersionNum"] = this.uGridDeviceChgResultD.Rows[0].Cells["VersionNum"].Value.ToString();
                }
                //else if (this.uTextVersionNum.Text != this.uGridDeviceChgResultD.Rows[0].Cells["VersionNum"].Value.ToString())
                //{
                //    dtDeviceChgResultH.Rows[0]["StdNumber"] = this.uGridDeviceChgResultD.Rows[0].Cells["StdNumber"].Value.ToString();
                //    dtDeviceChgResultH.Rows[0]["VersionNum"] = this.uGridDeviceChgResultD.Rows[0].Cells["VersionNum"].Value.ToString();
                //}
                else
                {
                    dtDeviceChgResultH.Rows[0]["StdNumber"] = this.uTextStdNumber.Text;
                    dtDeviceChgResultH.Rows[0]["VersionNum"] = this.uTextVersionNum.Text;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // -- 품종교체점검등록 저장 매서드 호출 -- //
                    string strErrRtn = clsDeviceChgResultH.mfSaveDeviceChgResultH(dtDeviceChgResultH, dtDeviceChgResultD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // DeCoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //- -----------  필 수 입 력 사 항 확 인 ------------- //
                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextDeviceChgDocCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "입력사항확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    // Dropdown
                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextEquipCode.Text == "" || this.uTextEquipName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "확인창", "필수입력사항확인", "설비코드를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextEquipCode.Focus();
                    return;
                }

                //------------- 삭제할 정보 저장 -------------//
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDeviceChgDocCode = this.uTextDeviceChgDocCode.Text;

                //--- 삭제 여부 메세제 박스를 띄운다 --//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 팝업창을 띄운다 //
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");

                    // 커서변경 //
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //품종교체점검등록 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultH), "DeviceChgResultH");
                    QRPEQU.BL.EQUCCS.DeviceChgResultH clsDeviceChgResultH = new QRPEQU.BL.EQUCCS.DeviceChgResultH();
                    brwChannel.mfCredentials(clsDeviceChgResultH);

                    //품종교체점검등록 삭제매서드 호출
                    string strErrRtn = clsDeviceChgResultH.mfDeleteDeviceChgResult(strPlantCode, strDeviceChgDocCode);
                    
                    //--DeCoding --//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //-- 커서 기본값으로 변경 //
                    this.MdiParent.Cursor = Cursors.Default;

                    //팝업창을 닫는다 ..//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라 메세지 박스를 띄운다..//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "출력중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // ExpandGroupBox가 숨겨져있는상태면 펼친다.
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 수정불가 컨트롤 수정가능으로 바꿈 //
                this.uComboPlant.ReadOnly = false;
                this.uTextEquipCode.ReadOnly = false;
                

                InitExpandControl();
                this.uComboProcess.Value = "";


                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                
                //-- 전체 행을 선택 하여 삭제한다 --//
                this.uGridDeviceChgResultH.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDeviceChgResultH.Rows.All);
                this.uGridDeviceChgResultH.DeleteSelectedRows(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread t1 = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridDeviceChgResultH.Rows.Count == 0 && ((this.uGridDeviceChgResultD.Rows.Count == 0 && this.uGridDurableMatRepairChg.Rows.Count == 0) || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀출력정보가 없습니다", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridDeviceChgResultH);

                if(this.uGridDeviceChgResultD.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                grd.mfDownLoadGridToExcel(this.uGridDeviceChgResultD);

                if (this.uGridDurableMatRepairChg.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                grd.mfDownLoadGridToExcel(this.uGridDurableMatRepairChg);
                /////////////

                //this.MdiParent.Cursor = Cursors.Default;
                //m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        #endregion

        #region 이벤트

        
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridDeviceChgResultD_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
               QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDeviceChgResultD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridDeviceChgResultD_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        //공장콤보 Value값이 바뀔때 마다 공정콤보 점검Point콤보가 바뀐다. //
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.uComboProcess.Items.Clear();

                string strPlantCode = this.uComboPlant.Value.ToString();

                string strPlant = this.uComboPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }
                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    QRPBrowser brwChannel = new QRPBrowser();

                    InitExpandControl();
                    

                    // 공정 정보 콤보 //
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    DataTable dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboProcess, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ProcessCode", "ProcessName", dtProcess);

                    // 품종교체점검Point정보 콤보 //
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPoint), "DeviceChgPoint");
                    QRPMAS.BL.MASEQU.DeviceChgPoint clsDeviceChgPoint = new QRPMAS.BL.MASEQU.DeviceChgPoint();
                    brwChannel.mfCredentials(clsDeviceChgPoint);

                    DataTable dtDeviceChgPointCombo = clsDeviceChgPoint.mfReadDeviceChgPointCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();

                    wGrid.mfSetGridColumnValueList(this.uGridDeviceChgResultD, 0, "DeviceChgPointCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDeviceChgPointCombo);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //ExpandGroupBox 펼침숨김상태
        private void uGroupContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDeviceChgResultH.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDeviceChgResultH.Height = 740;

                    for (int i = 0; i < uGridDeviceChgResultH.Rows.Count; i++)
                    {
                        uGridDeviceChgResultH.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 설비텍스트 에디터버튼을 클릭시 설비정보창을 띄운다 //
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {

            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlant.ReadOnly == false)
                {
                    // 공장콤보 값이 공백일 경우 메세지 박스를 띄운다.
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //공장 콤보DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    // 공장 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    frmPOP0005 frmEquip = new frmPOP0005();
                    
                    //공장정보 보냄
                    frmEquip.PlantCode = strPlantCode;
                    frmEquip.ShowDialog();

                   
                    // 설비코드 저장
                    string strEquipCode = frmEquip.EquipCode;
                    string strEquipTypeCode = frmEquip.EquipTypeCode;

                    if (strEquipTypeCode != "")
                    {

                        // 팝업창에서 선택한 설비의 정보를 각 해당 컨트롤에 삽입한다.
                        this.uTextEquipCode.Text = frmEquip.EquipCode;
                        this.uTextArea.Text = frmEquip.AreaName;
                        this.uTextEquipGroupName.Text = frmEquip.EquipGroupName;
                        this.uTextEquipLevel.Text = frmEquip.EquipLevelCode;
                        this.uTextLocation.Text = frmEquip.EquipLocName;
                        this.uTextSysProcess.Text = frmEquip.EquipProcGubunName;
                        this.uTextEquipType.Text = frmEquip.EquipTypeName;
                        this.uTextYear.Text = frmEquip.MakeYear;
                        this.uTextModel.Text = frmEquip.ModelName;
                        this.uTextSerialNo.Text = frmEquip.SerialNo;
                        this.uTextStation.Text = frmEquip.StationName;
                        this.uTextSTSStock.Text = frmEquip.GRDate;
                        this.uTextSuperEquip.Text = frmEquip.SuperEquipCode;
                        this.uTextEquipName.Text = frmEquip.EquipName;
                        this.uTextVendor.Text = frmEquip.VendorName;

                        DeviceChgResultInfo(strPlantCode, strEquipCode, strEquipTypeCode);

                    }
                    else
                    {
                        if (!this.uTextEquipName.Text.Equals(string.Empty))
                        {
                            this.uTextEquipName.Text = "";      //설비명
                            this.uTextSuperEquip.Text = "";     //Super설비
                            this.uTextStation.Text = "";        //Station
                            this.uTextArea.Text = "";           //Area
                            this.uTextSysProcess.Text = "";   //공정구분
                            this.uTextLocation.Text = "";       //위치
                            this.uTextModel.Text = "";          //모델
                            this.uTextEquipType.Text = "";      //설비유형
                            this.uTextSerialNo.Text = "";       //SerialNo
                            this.uTextVendor.Text = "";         //거래처
                            this.uTextEquipGroupName.Text = ""; //그룹명
                            this.uTextSTSStock.Text = "";       //입고일
                            this.uTextYear.Text = "";      //제작년도
                            this.uTextEquipLevel.Text = "";     //설비등급

                            this.uComboProcess.Value = "";

                            this.uDateDeviceChgDate.Value = DateTime.Now;  //교체점검일
                            this.uComboWorkGubun.Value = "";     //작업조
                            this.uTextDeviceChgDocCode.Text = "";//품종교체문서번호
                            this.uOptionCCSFlag.Value = "";      //CSS의뢰여부
                            this.uTextEtcDesc.Text = "";         //특이사항
                            this.uTextStdNumber.Text = "";
                            this.uTextVersionNum.Text = "";

                            //그리드 
                            //-- 전체 행을 선택 하여 삭제한다 --//

                            if (this.uGridDeviceChgResultD.Rows.Count > 0)
                            {
                                this.uGridDeviceChgResultD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDeviceChgResultD.Rows.All);
                                this.uGridDeviceChgResultD.DeleteSelectedRows(false);
                            }
                            if (this.uGridDurableMatRepairChg.Rows.Count > 0)
                            {
                                this.uGridDurableMatRepairChg.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableMatRepairChg.Rows.All);
                                this.uGridDurableMatRepairChg.DeleteSelectedRows(false);
                            }

                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }


        }

        //교체점검자에디터버튼을 클릭시 유저정보창을 띄운다.
        private void uTextDeviceChgUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {

                try
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    //-- 공장 이 공백일 경우 메세지박스를 띄운다. --//
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //공장 콤보DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }
                    //-- 공장정보 저장 --//
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = strPlantCode;
                    frmUser.ShowDialog();


                    //--각 해당컨트롤에 아이디 명 을 삽입시킨다.
                    this.uTextDeviceChgUserID.Text = frmUser.UserID;
                    this.uTextDeviceChgUserName.Text = frmUser.UserName;

                }
                catch (Exception ex)
                {
                    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                    frmErr.ShowDialog();
                }
                finally
                {
                }
 
        }

        //교체점검자엔터키누를시 사용자정보를 가져온다 
        private void uTextDeviceChgUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextDeviceChgUserName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //교체점검자 공장코드 저장
                    string strDeviceChgUserID = this.uTextDeviceChgUserID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //----- 아이디 공백 확인
                    if (strDeviceChgUserID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextDeviceChgUserID.Focus();
                        return;
                    }
                    //-- 공장 확인
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    //-- 유저정보 가져오기 
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strDeviceChgUserID, m_resSys.GetString("SYS_LANG"));
                    if (dtUser.Rows.Count > 0)
                    {
                        // 이름 텍스트에 유저 이름을 삽입한다.
                        this.uTextDeviceChgUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "조회결과 확인", "입력하신 ID가 존재하지 않습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextDeviceChgUserName.Clear();
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //-- 헤더 그리드의 셀을 더블클릭 하였을때 ExpandGroupBox에 자동로드 
        private void uGridDeviceChgResultH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                
                //ExpandGroupBox 숨김상태라면 펼친다 .
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                e.Cell.Row.Fixed = true;
                // 선택한 셀이 공백이 아닌 경우 //
                if (e.Cell.Value.ToString() != "")
                {
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();

                    this.uComboPlant.Value = strPlantCode;                                              //공장
                    this.uTextEquipCode.Text = strEquipCode;                                            //설비코드
                    this.uComboProcess.Value = e.Cell.Row.Cells["ProcessCode"].Value.ToString();       //공정
                    this.uTextDeviceChgUserID.Text = e.Cell.Row.Cells["ChgCheckID"].Value.ToString(); //교체점검자
                    this.uTextDeviceChgUserName.Text = e.Cell.Row.Cells["ChgCheckName"].Value.ToString();//교체점검자명
                    this.uDateDeviceChgDate.Value = e.Cell.Row.Cells["ChgCheckDate"].Value.ToString();  //교체점검일
                    this.uComboWorkGubun.Value = e.Cell.Row.Cells["WorkGubunCode"].Value.ToString();     //작업조
                    this.uTextDeviceChgDocCode.Text = e.Cell.Row.Cells["DeviceChgDocCode"].Value.ToString();//품종교체문서번호
                    this.uTextStdNumber.Text = e.Cell.Row.Cells["StdNumber"].Value.ToString();          //표준번호
                    this.uTextVersionNum.Text = e.Cell.Row.Cells["VersionNum"].Value.ToString();        //개정번호
                    this.uOptionCCSFlag.Value = e.Cell.Row.Cells["CCSReqFlag"].Value.ToString();      //CSS의뢰여부
                    this.uTextEtcDesc.Text = e.Cell.Row.Cells["EtcDesc"].Value.ToString();         //특이사항

                    //공장 설비 수정불가처리
                    this.uComboPlant.ReadOnly = true;
                    this.uTextEquipCode.ReadOnly = true;

                    //-- 설비정보 조회 --//
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    string strEquipType = "";

                    //정보가 있을경우 텍스트박스에 삽입
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();             //설비명
                        this.uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();       //Super설비
                        this.uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();             //Station
                        this.uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();                   //Area
                        this.uTextSysProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString();   //공정구분
                        this.uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();           //위치
                        this.uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();                 //모델
                        this.uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();         //설비유형
                        this.uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();               //SerialNo
                        this.uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();               //거래처
                        this.uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();   //그룹명
                        this.uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();                 //입고일
                        this.uTextYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();                   //제작년도
                        this.uTextEquipLevel.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();        //설비등급

                        strEquipType = dtEquip.Rows[0]["EquipTypeCode"].ToString();

                        DeviceChgResultInfo(strPlantCode, strEquipCode, strEquipType);

                        ////-- 품종교체등록 상세 BL 호출 --//
                        //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultD), "DeviceChgResultD");
                        //QRPEQU.BL.EQUCCS.DeviceChgResultD clsDeviceChgResultD = new QRPEQU.BL.EQUCCS.DeviceChgResultD();
                        //brwChannel.mfCredentials(clsDeviceChgResultD);

                        ////품종교체등록 상세 조회 매서드 호출 //
                        //DataTable dtChgPoint = clsDeviceChgResultD.mfReadDeviceChgResultD(strPlantCode, strEquipCode, strEquipType, m_resSys.GetString("SYS_LANG"));

                        //// 그리드에 바인드 //
                        //this.uGridDeviceChgResultD.DataSource = dtChgPoint;
                        //this.uGridDeviceChgResultD.DataBind();

                        

                        ////--금형치공구교체내역 BL 호출 -- //
                        //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                        //QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                        //brwChannel.mfCredentials(clsDurableMatRepairH);

                        ////금형치공구교체내역 조회 매서드 호출 //
                        //DataTable dtDurableMatChg = clsDurableMatRepairH.mfReadDurableMatRepairChg(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                        ////그리드에 바인드 ..
                        //this.uGridDurableMatRepairChg.DataSource = dtDurableMatChg;
                        //this.uGridDurableMatRepairChg.DataBind();
                        

                    }
                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //--검색조건에 있는 설비코드 텍스트 에디터버튼을 클릭하였을 때 설비정보 팝업창을 띄운다 ..
        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();



                string strPlantCode = "";

                if (this.uComboSearchPlant.Value.ToString() != "")
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                frmPOP0005 frmEquip = new frmPOP0005();
                //공장정보를 보낸다.
                frmEquip.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                frmEquip.ShowDialog();


                // 선택한 정보 각 컨트롤에 삽입 //
                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextSearchEquipName.Text = frmEquip.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비코드 입력 후 자동조회
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextEquipCode.ReadOnly.Equals(false) && (e.KeyData == Keys.Delete || e.KeyData == Keys.Back))
                {
                    if (!this.uTextEquipName.Text.Equals(string.Empty))
                    {
                        this.uTextEquipName.Text = "";      //설비명
                        this.uTextSuperEquip.Text = "";     //Super설비
                        this.uTextStation.Text = "";        //Station
                        this.uTextArea.Text = "";           //Area
                        this.uTextSysProcess.Text = "";   //공정구분
                        this.uTextLocation.Text = "";       //위치
                        this.uTextModel.Text = "";          //모델
                        this.uTextEquipType.Text = "";      //설비유형
                        this.uTextSerialNo.Text = "";       //SerialNo
                        this.uTextVendor.Text = "";         //거래처
                        this.uTextEquipGroupName.Text = ""; //그룹명
                        this.uTextSTSStock.Text = "";       //입고일
                        this.uTextYear.Text = "";      //제작년도
                        this.uTextEquipLevel.Text = "";     //설비등급

                        this.uComboProcess.Value = "";

                        this.uDateDeviceChgDate.Value = DateTime.Now;  //교체점검일
                        this.uComboWorkGubun.Value = "";     //작업조
                        this.uTextDeviceChgDocCode.Text = "";//품종교체문서번호
                        this.uOptionCCSFlag.Value = "";      //CSS의뢰여부
                        this.uTextEtcDesc.Text = "";         //특이사항
                        this.uTextStdNumber.Text = "";
                        this.uTextVersionNum.Text = "";

                        //그리드 
                        //-- 전체 행을 선택 하여 삭제한다 --//

                        if (this.uGridDeviceChgResultD.Rows.Count > 0)
                        {
                            this.uGridDeviceChgResultD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDeviceChgResultD.Rows.All);
                            this.uGridDeviceChgResultD.DeleteSelectedRows(false);
                        }
                        if (this.uGridDurableMatRepairChg.Rows.Count > 0)
                        {
                            this.uGridDurableMatRepairChg.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableMatRepairChg.Rows.All);
                            this.uGridDurableMatRepairChg.DeleteSelectedRows(false);
                        }

                    }
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextEquipCode.Text.Equals(string.Empty))
                {
                    //공장정보가 없으면 메세지를 보여준다.
                    if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "공장정보확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    //공장정보, 설비코드 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strEquipCode = this.uTextEquipCode.Text;

                    QRPBrowser brwChannel = new QRPBrowser();
                    //설비정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    //설비정보 조회 매서드호출
                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //정보가 있는경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        //설비유형이 있을 경우 자동조회
                        if (!dtEquip.Rows[0]["EquipTypeCode"].ToString().Equals(string.Empty))
                        {
                            this.uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();             //설비코드
                            this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();             //설비명
                            this.uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipName"].ToString();       //Super설비
                            this.uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();             //Station
                            this.uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();                   //Area
                            this.uTextSysProcess.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString(); //공정구분
                            this.uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();           //위치
                            this.uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();                 //모델
                            this.uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();         //설비유형
                            this.uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();               //SerialNo
                            this.uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();               //거래처
                            this.uTextEquipGroupName.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();   //그룹명
                            this.uTextSTSStock.Text = dtEquip.Rows[0]["GRDate"].ToString();                 //입고일
                            this.uTextYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();              //제작년도
                            this.uTextEquipLevel.Text = dtEquip.Rows[0]["EquipLevelCode"].ToString();       //설비등급

                            DeviceChgResultInfo(strPlantCode, strEquipCode, dtEquip.Rows[0]["EquipTypeCode"].ToString());

                        }


                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "조회처리결과 확인", "입력하신 설비코드를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);

                        if (!this.uTextEquipName.Text.Equals(string.Empty))
                        {
                            this.uTextEquipName.Text = "";      //설비명
                            this.uTextSuperEquip.Text = "";     //Super설비
                            this.uTextStation.Text = "";        //Station
                            this.uTextArea.Text = "";           //Area
                            this.uTextSysProcess.Text = "";   //공정구분
                            this.uTextLocation.Text = "";       //위치
                            this.uTextModel.Text = "";          //모델
                            this.uTextEquipType.Text = "";      //설비유형
                            this.uTextSerialNo.Text = "";       //SerialNo
                            this.uTextVendor.Text = "";         //거래처
                            this.uTextEquipGroupName.Text = ""; //그룹명
                            this.uTextSTSStock.Text = "";       //입고일
                            this.uTextYear.Text = "";      //제작년도
                            this.uTextEquipLevel.Text = "";     //설비등급
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //검색조건에 있는 설비코드를 입력하면 설비명을 자동으로 삽입
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //Delete키나 Backspace키를 누르면 설비명 공백처리
                if (e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back))
                {
                    this.uTextSearchEquipName.Clear();
                }

                //Enter키를 누를 시 해당설비의 설비명을 가져온다.
                if (e.KeyData.Equals(Keys.Enter) && !this.uTextSearchEquipCode.Text.Equals(string.Empty))
                {
                    //공장정보가 없는 경우
                    if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "공장정보확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //공장 설비 정보 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strEquipCode = this.uTextSearchEquipCode.Text;

                    //설비정보BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보 상세조회매서드 호출
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //정보가 있으면 설비명을 보여준다.
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "조회결과 확인", "입력하신 설비의 정보를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion



        private void frmEQUZ0004_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }


        /// <summary>
        /// 품종교체점검등록 자동조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strEquipTypeCode">설비유형</param>
        private void DeviceChgResultInfo(string strPlantCode, string strEquipCode, string strEquipTypeCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                QRPBrowser brwChannel = new QRPBrowser();

                // 품종교체점검등록 헤더BL 호출 //
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultH), "DeviceChgResultH");
                QRPEQU.BL.EQUCCS.DeviceChgResultH clsDeviceChgResultH = new QRPEQU.BL.EQUCCS.DeviceChgResultH();
                brwChannel.mfCredentials(clsDeviceChgResultH);

                //품종교체점검등록조회 매서드 호출//
                DataTable dtChgResultH = clsDeviceChgResultH.mfReadDeviceChgResultH(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //정보가 있을경우 각 해당 컨트롤에 정보를 삽입한다.
                if (dtChgResultH.Rows.Count > 0)
                {
                    this.uComboProcess.Value = dtChgResultH.Rows[0]["ProcessCode"].ToString();
                    this.uTextDeviceChgUserID.Text = dtChgResultH.Rows[0]["ChgCheckID"].ToString();
                    this.uTextDeviceChgUserName.Text = dtChgResultH.Rows[0]["ChgCheckName"].ToString();
                    this.uDateDeviceChgDate.Value = dtChgResultH.Rows[0]["ChgCheckDate"].ToString();
                    this.uComboWorkGubun.Value = dtChgResultH.Rows[0]["WorkGubunCode"].ToString();
                    this.uTextDeviceChgDocCode.Text = dtChgResultH.Rows[0]["DeviceChgDocCode"].ToString();
                    this.uTextStdNumber.Text = dtChgResultH.Rows[0]["StdNumber"].ToString();
                    this.uTextVersionNum.Text = dtChgResultH.Rows[0]["VersionNum"].ToString();
                    this.uOptionCCSFlag.Value = dtChgResultH.Rows[0]["CCSReqFlag"].ToString();
                    this.uTextEtcDesc.Text = dtChgResultH.Rows[0]["EtcDesc"].ToString();
                }
                // 정보가 없는데 품종교체문서번호가 남아있다면 헤더 컨트롤 초기화 //
                else
                {
                    if (this.uTextDeviceChgDocCode.Text != "")
                    {
                        this.uComboProcess.Value = "";
                        InitText();
                        this.uDateDeviceChgDate.Value = DateTime.Now;
                        this.uComboWorkGubun.Value = "";
                        this.uTextDeviceChgDocCode.Text = "";
                        this.uTextStdNumber.Text = "";
                        this.uTextVersionNum.Text = "";
                        this.uTextEtcDesc.Text = "";

                    }
                }
                // 품종교체점검등록 상세 BL 호출 //
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DeviceChgResultD), "DeviceChgResultD");
                QRPEQU.BL.EQUCCS.DeviceChgResultD clsDeviceChgResultD = new QRPEQU.BL.EQUCCS.DeviceChgResultD();
                brwChannel.mfCredentials(clsDeviceChgResultD);

                // 품종교체점검등록 상세 조회 매서드 호출 //
                DataTable dtChgResultD = clsDeviceChgResultD.mfReadDeviceChgResultD(strPlantCode, strEquipCode, strEquipTypeCode, m_resSys.GetString("SYS_LANG"));

                //상세정보가 있을경우 바인드
                if (dtChgResultD.Rows.Count > 0)
                {
                    //그리드에 데이터 바인드 //
                    this.uGridDeviceChgResultD.DataSource = dtChgResultD;
                    this.uGridDeviceChgResultD.DataBind();

                    //품종교체문서번호가 없을 경우 신규로 인식하여 바인드된 줄에 편집이미지를 넣는다 
                    if (this.uTextDeviceChgDocCode.Text == "")
                    {
                        QRPGlobal grdImg = new QRPGlobal();

                        for (int i = 0; i < this.uGridDeviceChgResultD.Rows.Count; i++)
                        {

                            this.uGridDeviceChgResultD.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        }
                    }
                }
                //없을 경우 메세지 박스를 띄운다 //
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "설비정보 확인", "해당설비에 대한 교체점검항목정보가 없습니다. 설비관리기준정보 - 설비품종교체점검정보등록으로 등록하세요.", Infragistics.Win.HAlign.Right);

                    if (this.uGridDeviceChgResultD.Rows.Count > 0)
                    {
                        this.uGridDeviceChgResultD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDeviceChgResultD.Rows.All);
                        this.uGridDeviceChgResultD.DeleteSelectedRows(false);
                    }
                    if (this.uGridDurableMatRepairChg.Rows.Count > 0)
                    {
                        this.uGridDurableMatRepairChg.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableMatRepairChg.Rows.All);
                        this.uGridDurableMatRepairChg.DeleteSelectedRows(false);
                    }

                    this.uTextEquipCode.Text = "";
                    this.uTextArea.Text = "";
                    this.uTextEquipGroupName.Text = "";
                    this.uTextEquipLevel.Text = "";
                    this.uTextLocation.Text = "";
                    this.uTextSysProcess.Text = "";
                    this.uTextEquipType.Text = "";
                    this.uTextYear.Text = "";
                    this.uTextModel.Text = "";
                    this.uTextSerialNo.Text = "";
                    this.uTextStation.Text = "";
                    this.uTextSTSStock.Text = "";
                    this.uTextSuperEquip.Text = "";
                    this.uTextEquipName.Text = "";
                    this.uTextVendor.Text = "";

                    return;
                }

                //금형치공구 교체내역 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);

                // 금형치공구 교체내역 조회 매서드 호출 //
                DataTable dtDurableMatChg = clsDurableMatRepairH.mfReadDurableMatRepairChg(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드 
                this.uGridDurableMatRepairChg.DataSource = dtDurableMatChg;
                this.uGridDurableMatRepairChg.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// ExpandGroupBox안의 컨트롤 초기화
        /// </summary>
        private void InitExpandControl()
        {
            try
            {
                this.uTextEquipCode.Text = "";  //설비코드
                this.uTextEquipName.Text = "";  //설비명
                this.uTextSuperEquip.Text = ""; //Super설비
                this.uTextStation.Text = "";    //Station
                this.uTextArea.Text = "";       //Area
                this.uTextSysProcess.Text = ""; //공정구분
                this.uTextLocation.Text = "";   //위치
                this.uTextModel.Text = "";      //모델
                this.uTextEquipType.Text = ""; //설비유형
                this.uTextSerialNo.Text = "";   //SerialNo
                this.uTextVendor.Text = "";     //거래처
                this.uTextEquipGroupName.Text = "";//그룹명
                this.uTextSTSStock.Text = "";       //입고일
                this.uTextYear.Text = "";           //제작년도
                this.uTextEquipLevel.Text = "";     //설비등급

                
                                                       
                //this.uComboProcess.Value = "";       //공정
                InitText();
                this.uDateDeviceChgDate.Value = DateTime.Now;  //교체점검일
                this.uComboWorkGubun.Value = "";     //작업조
                this.uTextDeviceChgDocCode.Text = "";//품종교체문서번호
                this.uOptionCCSFlag.Value = "";      //CSS의뢰여부
                this.uTextEtcDesc.Text = "";         //특이사항
                this.uTextStdNumber.Text = "";
                this.uTextVersionNum.Text = "";

                //그리드 
                //-- 전체 행을 선택 하여 삭제한다 --//

                if (this.uGridDeviceChgResultD.Rows.Count > 0)
                {
                    this.uGridDeviceChgResultD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDeviceChgResultD.Rows.All);
                    this.uGridDeviceChgResultD.DeleteSelectedRows(false);
                }
                if (this.uGridDurableMatRepairChg.Rows.Count > 0)
                {
                    this.uGridDurableMatRepairChg.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableMatRepairChg.Rows.All);
                    this.uGridDurableMatRepairChg.DeleteSelectedRows(false);
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



    }
}
