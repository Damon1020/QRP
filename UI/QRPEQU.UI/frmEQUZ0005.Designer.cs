﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0005));
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckMDM = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckGWResultState = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckGWTFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextSTSStock = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSTSStock = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipLevel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakerYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipLoc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextArea = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSuperEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSysName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquipGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLocation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSuperSys = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDownDocFile = new Infragistics.Win.Misc.UltraButton();
            this.uNumRemainPrice = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumPurchasePrice = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uButtonDownLoad = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextDiscardUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridEquipDiscardFile = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelFile = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDiscardReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRemainPrice = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPurchasePrice = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDiscardUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDiscardDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateDiscardTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDiscardUser = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMDM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckGWResultState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckGWTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSysName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRemainPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumPurchasePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipDiscardFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uCheckMDM);
            this.uGroupBox.Controls.Add(this.uCheckGWResultState);
            this.uGroupBox.Controls.Add(this.uCheckGWTFlag);
            this.uGroupBox.Controls.Add(this.uTextSTSStock);
            this.uGroupBox.Controls.Add(this.uLabelSTSStock);
            this.uGroupBox.Controls.Add(this.uTextEquipLevel);
            this.uGroupBox.Controls.Add(this.uTextMakerYear);
            this.uGroupBox.Controls.Add(this.uTextVendor);
            this.uGroupBox.Controls.Add(this.uTextEquipGroupName);
            this.uGroupBox.Controls.Add(this.uTextSerialNo);
            this.uGroupBox.Controls.Add(this.uTextEquipType);
            this.uGroupBox.Controls.Add(this.uTextModel);
            this.uGroupBox.Controls.Add(this.uTextEquipProcess);
            this.uGroupBox.Controls.Add(this.uTextEquipLoc);
            this.uGroupBox.Controls.Add(this.uTextStation);
            this.uGroupBox.Controls.Add(this.uTextArea);
            this.uGroupBox.Controls.Add(this.uTextSuperEquip);
            this.uGroupBox.Controls.Add(this.uTextSysName);
            this.uGroupBox.Controls.Add(this.uTextEquipCode);
            this.uGroupBox.Controls.Add(this.uComboPlant);
            this.uGroupBox.Controls.Add(this.uLabelEquipGrade);
            this.uGroupBox.Controls.Add(this.uLabelYear);
            this.uGroupBox.Controls.Add(this.uLabelVendor);
            this.uGroupBox.Controls.Add(this.uLabelEquipGroupName);
            this.uGroupBox.Controls.Add(this.uLabelSerialNo);
            this.uGroupBox.Controls.Add(this.uLabelEquipType);
            this.uGroupBox.Controls.Add(this.uLabelModel);
            this.uGroupBox.Controls.Add(this.uLabelEquipProcess);
            this.uGroupBox.Controls.Add(this.uLabelLocation);
            this.uGroupBox.Controls.Add(this.uLabelStation);
            this.uGroupBox.Controls.Add(this.uLabelArea);
            this.uGroupBox.Controls.Add(this.uLabelSuperSys);
            this.uGroupBox.Controls.Add(this.uLabelEquipName);
            this.uGroupBox.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox.Controls.Add(this.uLabelPlant);
            this.uGroupBox.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1040, 180);
            this.uGroupBox.TabIndex = 5;
            // 
            // uCheckMDM
            // 
            appearance49.TextVAlignAsString = "Middle";
            this.uCheckMDM.Appearance = appearance49;
            this.uCheckMDM.Enabled = false;
            this.uCheckMDM.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMDM.Location = new System.Drawing.Point(796, 152);
            this.uCheckMDM.Name = "uCheckMDM";
            this.uCheckMDM.Size = new System.Drawing.Size(128, 20);
            this.uCheckMDM.TabIndex = 305;
            this.uCheckMDM.Text = "MDM전송여부";
            this.uCheckMDM.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckMDM.Visible = false;
            // 
            // uCheckGWResultState
            // 
            appearance48.TextVAlignAsString = "Middle";
            this.uCheckGWResultState.Appearance = appearance48;
            this.uCheckGWResultState.Enabled = false;
            this.uCheckGWResultState.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckGWResultState.Location = new System.Drawing.Point(690, 149);
            this.uCheckGWResultState.Name = "uCheckGWResultState";
            this.uCheckGWResultState.Size = new System.Drawing.Size(100, 20);
            this.uCheckGWResultState.TabIndex = 305;
            this.uCheckGWResultState.Text = "결재 결과";
            this.uCheckGWResultState.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckGWTFlag
            // 
            appearance50.TextVAlignAsString = "Middle";
            this.uCheckGWTFlag.Appearance = appearance50;
            this.uCheckGWTFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckGWTFlag.Location = new System.Drawing.Point(584, 149);
            this.uCheckGWTFlag.Name = "uCheckGWTFlag";
            this.uCheckGWTFlag.Size = new System.Drawing.Size(100, 20);
            this.uCheckGWTFlag.TabIndex = 304;
            this.uCheckGWTFlag.Text = "결재 요청";
            this.uCheckGWTFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextSTSStock
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Appearance = appearance5;
            this.uTextSTSStock.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Location = new System.Drawing.Point(688, 124);
            this.uTextSTSStock.Name = "uTextSTSStock";
            this.uTextSTSStock.ReadOnly = true;
            this.uTextSTSStock.Size = new System.Drawing.Size(108, 21);
            this.uTextSTSStock.TabIndex = 69;
            // 
            // uLabelSTSStock
            // 
            this.uLabelSTSStock.Location = new System.Drawing.Point(584, 124);
            this.uLabelSTSStock.Name = "uLabelSTSStock";
            this.uLabelSTSStock.Size = new System.Drawing.Size(100, 20);
            this.uLabelSTSStock.TabIndex = 68;
            this.uLabelSTSStock.Text = "STS입고일";
            // 
            // uTextEquipLevel
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Appearance = appearance22;
            this.uTextEquipLevel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Location = new System.Drawing.Point(400, 148);
            this.uTextEquipLevel.Name = "uTextEquipLevel";
            this.uTextEquipLevel.ReadOnly = true;
            this.uTextEquipLevel.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipLevel.TabIndex = 67;
            // 
            // uTextMakerYear
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerYear.Appearance = appearance4;
            this.uTextMakerYear.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerYear.Location = new System.Drawing.Point(116, 148);
            this.uTextMakerYear.Name = "uTextMakerYear";
            this.uTextMakerYear.ReadOnly = true;
            this.uTextMakerYear.Size = new System.Drawing.Size(108, 21);
            this.uTextMakerYear.TabIndex = 66;
            // 
            // uTextVendor
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance6;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 124);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(108, 21);
            this.uTextVendor.TabIndex = 65;
            // 
            // uTextEquipGroupName
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Appearance = appearance7;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(400, 124);
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.ReadOnly = true;
            this.uTextEquipGroupName.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipGroupName.TabIndex = 64;
            // 
            // uTextSerialNo
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance8;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(688, 100);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(228, 21);
            this.uTextSerialNo.TabIndex = 63;
            // 
            // uTextEquipType
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Appearance = appearance9;
            this.uTextEquipType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Location = new System.Drawing.Point(400, 100);
            this.uTextEquipType.Name = "uTextEquipType";
            this.uTextEquipType.ReadOnly = true;
            this.uTextEquipType.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipType.TabIndex = 62;
            // 
            // uTextModel
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance10;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(116, 100);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(108, 21);
            this.uTextModel.TabIndex = 61;
            // 
            // uTextEquipProcess
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcess.Appearance = appearance11;
            this.uTextEquipProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcess.Location = new System.Drawing.Point(400, 76);
            this.uTextEquipProcess.Name = "uTextEquipProcess";
            this.uTextEquipProcess.ReadOnly = true;
            this.uTextEquipProcess.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipProcess.TabIndex = 60;
            // 
            // uTextEquipLoc
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLoc.Appearance = appearance12;
            this.uTextEquipLoc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLoc.Location = new System.Drawing.Point(688, 76);
            this.uTextEquipLoc.Name = "uTextEquipLoc";
            this.uTextEquipLoc.ReadOnly = true;
            this.uTextEquipLoc.Size = new System.Drawing.Size(228, 21);
            this.uTextEquipLoc.TabIndex = 59;
            // 
            // uTextStation
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Appearance = appearance13;
            this.uTextStation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Location = new System.Drawing.Point(688, 52);
            this.uTextStation.Name = "uTextStation";
            this.uTextStation.ReadOnly = true;
            this.uTextStation.Size = new System.Drawing.Size(228, 21);
            this.uTextStation.TabIndex = 58;
            // 
            // uTextArea
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Appearance = appearance16;
            this.uTextArea.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextArea.Location = new System.Drawing.Point(116, 76);
            this.uTextArea.Name = "uTextArea";
            this.uTextArea.ReadOnly = true;
            this.uTextArea.Size = new System.Drawing.Size(108, 21);
            this.uTextArea.TabIndex = 57;
            // 
            // uTextSuperEquip
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Appearance = appearance17;
            this.uTextSuperEquip.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Location = new System.Drawing.Point(400, 52);
            this.uTextSuperEquip.Name = "uTextSuperEquip";
            this.uTextSuperEquip.ReadOnly = true;
            this.uTextSuperEquip.Size = new System.Drawing.Size(108, 21);
            this.uTextSuperEquip.TabIndex = 56;
            // 
            // uTextSysName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSysName.Appearance = appearance18;
            this.uTextSysName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSysName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextSysName.Location = new System.Drawing.Point(116, 52);
            this.uTextSysName.Name = "uTextSysName";
            this.uTextSysName.ReadOnly = true;
            this.uTextSysName.Size = new System.Drawing.Size(108, 21);
            this.uTextSysName.TabIndex = 55;
            // 
            // uTextEquipCode
            // 
            appearance33.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.Appearance = appearance33;
            this.uTextEquipCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance34.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance34;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(400, 28);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(108, 19);
            this.uTextEquipCode.TabIndex = 54;
            this.uTextEquipCode.ValueChanged += new System.EventHandler(this.uTextEquipCode_ValueChanged);
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uComboPlant
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance19;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(108, 19);
            this.uComboPlant.TabIndex = 53;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            this.uComboPlant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboPlant_BeforeDropDown);
            // 
            // uLabelEquipGrade
            // 
            this.uLabelEquipGrade.Location = new System.Drawing.Point(296, 148);
            this.uLabelEquipGrade.Name = "uLabelEquipGrade";
            this.uLabelEquipGrade.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGrade.TabIndex = 52;
            this.uLabelEquipGrade.Text = "설비등급";
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(12, 148);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 51;
            this.uLabelYear.Text = "제작년도";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 124);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelVendor.TabIndex = 50;
            this.uLabelVendor.Text = "Vendor";
            // 
            // uLabelEquipGroupName
            // 
            this.uLabelEquipGroupName.Location = new System.Drawing.Point(296, 124);
            this.uLabelEquipGroupName.Name = "uLabelEquipGroupName";
            this.uLabelEquipGroupName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroupName.TabIndex = 49;
            this.uLabelEquipGroupName.Text = "설비그룹명";
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(584, 100);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSerialNo.TabIndex = 48;
            this.uLabelSerialNo.Text = "SerialNo";
            // 
            // uLabelEquipType
            // 
            this.uLabelEquipType.Location = new System.Drawing.Point(296, 100);
            this.uLabelEquipType.Name = "uLabelEquipType";
            this.uLabelEquipType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipType.TabIndex = 47;
            this.uLabelEquipType.Text = "설비유형";
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(12, 100);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(100, 20);
            this.uLabelModel.TabIndex = 46;
            this.uLabelModel.Text = "모델";
            // 
            // uLabelEquipProcess
            // 
            this.uLabelEquipProcess.Location = new System.Drawing.Point(296, 76);
            this.uLabelEquipProcess.Name = "uLabelEquipProcess";
            this.uLabelEquipProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipProcess.TabIndex = 45;
            this.uLabelEquipProcess.Text = "설비공정부분";
            // 
            // uLabelLocation
            // 
            this.uLabelLocation.Location = new System.Drawing.Point(584, 76);
            this.uLabelLocation.Name = "uLabelLocation";
            this.uLabelLocation.Size = new System.Drawing.Size(100, 20);
            this.uLabelLocation.TabIndex = 44;
            this.uLabelLocation.Text = "위치";
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(584, 52);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 43;
            this.uLabelStation.Text = "Station";
            // 
            // uLabelArea
            // 
            this.uLabelArea.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelArea.Location = new System.Drawing.Point(12, 76);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelArea.TabIndex = 42;
            this.uLabelArea.Text = "Area";
            // 
            // uLabelSuperSys
            // 
            this.uLabelSuperSys.Location = new System.Drawing.Point(296, 52);
            this.uLabelSuperSys.Name = "uLabelSuperSys";
            this.uLabelSuperSys.Size = new System.Drawing.Size(100, 20);
            this.uLabelSuperSys.TabIndex = 41;
            this.uLabelSuperSys.Text = "Super설비";
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelEquipName.Location = new System.Drawing.Point(12, 52);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipName.TabIndex = 40;
            this.uLabelEquipName.Text = "설비명";
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(296, 28);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipCode.TabIndex = 39;
            this.uLabelEquipCode.Text = "설비코드";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 38;
            this.uLabelPlant.Text = "공장";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uButtonDownDocFile);
            this.uGroupBox2.Controls.Add(this.uNumRemainPrice);
            this.uGroupBox2.Controls.Add(this.uNumPurchasePrice);
            this.uGroupBox2.Controls.Add(this.uButtonDownLoad);
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox2.Controls.Add(this.uTextDiscardUserID);
            this.uGroupBox2.Controls.Add(this.uGridEquipDiscardFile);
            this.uGroupBox2.Controls.Add(this.uLabelFile);
            this.uGroupBox2.Controls.Add(this.uTextDiscardReason);
            this.uGroupBox2.Controls.Add(this.uLabelRemainPrice);
            this.uGroupBox2.Controls.Add(this.uLabelPurchasePrice);
            this.uGroupBox2.Controls.Add(this.uLabelReason);
            this.uGroupBox2.Controls.Add(this.uTextDiscardUserName);
            this.uGroupBox2.Controls.Add(this.uLabelDiscardDate);
            this.uGroupBox2.Controls.Add(this.uDateDiscardTime);
            this.uGroupBox2.Controls.Add(this.uLabelDiscardUser);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 196);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 488);
            this.uGroupBox2.TabIndex = 6;
            // 
            // uButtonDownDocFile
            // 
            this.uButtonDownDocFile.Location = new System.Drawing.Point(300, 104);
            this.uButtonDownDocFile.Name = "uButtonDownDocFile";
            this.uButtonDownDocFile.Size = new System.Drawing.Size(140, 28);
            this.uButtonDownDocFile.TabIndex = 2;
            this.uButtonDownDocFile.Click += new System.EventHandler(this.uButtonDownDocFile_Click);
            // 
            // uNumRemainPrice
            // 
            this.uNumRemainPrice.Location = new System.Drawing.Point(400, 52);
            this.uNumRemainPrice.MaskInput = "n,nnn,nnn,nnn.nn";
            this.uNumRemainPrice.MaxValue = ((long)(9999999999));
            this.uNumRemainPrice.Name = "uNumRemainPrice";
            this.uNumRemainPrice.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.uNumRemainPrice.PromptChar = ' ';
            this.uNumRemainPrice.Size = new System.Drawing.Size(108, 21);
            this.uNumRemainPrice.TabIndex = 97;
            // 
            // uNumPurchasePrice
            // 
            this.uNumPurchasePrice.Location = new System.Drawing.Point(116, 52);
            this.uNumPurchasePrice.MaskInput = "n,nnn,nnn,nnn.nn";
            this.uNumPurchasePrice.MaxValue = ((long)(9999999999));
            this.uNumPurchasePrice.Name = "uNumPurchasePrice";
            this.uNumPurchasePrice.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.uNumPurchasePrice.PromptChar = ' ';
            this.uNumPurchasePrice.Size = new System.Drawing.Size(108, 21);
            this.uNumPurchasePrice.TabIndex = 97;
            // 
            // uButtonDownLoad
            // 
            this.uButtonDownLoad.Location = new System.Drawing.Point(208, 104);
            this.uButtonDownLoad.Name = "uButtonDownLoad";
            this.uButtonDownLoad.Size = new System.Drawing.Size(88, 28);
            this.uButtonDownLoad.TabIndex = 95;
            this.uButtonDownLoad.Click += new System.EventHandler(this.uButtonFileDownload_Click);
            // 
            // uButtonDeleteRow
            // 
            appearance3.FontData.BoldAsString = "True";
            appearance3.Image = global::QRPEQU.UI.Properties.Resources.btn_delTable;
            this.uButtonDeleteRow.Appearance = appearance3;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(116, 104);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 94;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uTextDiscardUserID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDiscardUserID.Appearance = appearance15;
            this.uTextDiscardUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDiscardUserID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance35;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDiscardUserID.ButtonsRight.Add(editorButton2);
            this.uTextDiscardUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDiscardUserID.Location = new System.Drawing.Point(400, 28);
            this.uTextDiscardUserID.MaxLength = 20;
            this.uTextDiscardUserID.Name = "uTextDiscardUserID";
            this.uTextDiscardUserID.Size = new System.Drawing.Size(100, 19);
            this.uTextDiscardUserID.TabIndex = 93;
            this.uTextDiscardUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDiscardUserID_KeyDown);
            this.uTextDiscardUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDiscardUserID_EditorButtonClick);
            // 
            // uGridEquipDiscardFile
            // 
            this.uGridEquipDiscardFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipDiscardFile.DisplayLayout.Appearance = appearance2;
            this.uGridEquipDiscardFile.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipDiscardFile.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDiscardFile.DisplayLayout.GroupByBox.Appearance = appearance32;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipDiscardFile.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridEquipDiscardFile.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipDiscardFile.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGridEquipDiscardFile.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipDiscardFile.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipDiscardFile.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipDiscardFile.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.uGridEquipDiscardFile.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipDiscardFile.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDiscardFile.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipDiscardFile.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGridEquipDiscardFile.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipDiscardFile.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDiscardFile.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGridEquipDiscardFile.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGridEquipDiscardFile.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipDiscardFile.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipDiscardFile.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGridEquipDiscardFile.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipDiscardFile.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGridEquipDiscardFile.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipDiscardFile.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipDiscardFile.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipDiscardFile.Location = new System.Drawing.Point(12, 112);
            this.uGridEquipDiscardFile.Name = "uGridEquipDiscardFile";
            this.uGridEquipDiscardFile.Size = new System.Drawing.Size(1020, 364);
            this.uGridEquipDiscardFile.TabIndex = 92;
            this.uGridEquipDiscardFile.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipDiscardFile_AfterCellUpdate);
            this.uGridEquipDiscardFile.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipDiscardFile_ClickCellButton);
            this.uGridEquipDiscardFile.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipDiscardFile_CellChange);
            // 
            // uLabelFile
            // 
            this.uLabelFile.Location = new System.Drawing.Point(12, 108);
            this.uLabelFile.Name = "uLabelFile";
            this.uLabelFile.Size = new System.Drawing.Size(100, 20);
            this.uLabelFile.TabIndex = 90;
            this.uLabelFile.Text = "첨부문서";
            // 
            // uTextDiscardReason
            // 
            this.uTextDiscardReason.Location = new System.Drawing.Point(116, 76);
            this.uTextDiscardReason.MaxLength = 1000;
            this.uTextDiscardReason.Name = "uTextDiscardReason";
            this.uTextDiscardReason.Size = new System.Drawing.Size(792, 21);
            this.uTextDiscardReason.TabIndex = 89;
            this.uTextDiscardReason.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDiscardReason_KeyDown);
            // 
            // uLabelRemainPrice
            // 
            this.uLabelRemainPrice.Location = new System.Drawing.Point(296, 52);
            this.uLabelRemainPrice.Name = "uLabelRemainPrice";
            this.uLabelRemainPrice.Size = new System.Drawing.Size(100, 20);
            this.uLabelRemainPrice.TabIndex = 88;
            this.uLabelRemainPrice.Text = "설비잔존가";
            // 
            // uLabelPurchasePrice
            // 
            this.uLabelPurchasePrice.Location = new System.Drawing.Point(12, 52);
            this.uLabelPurchasePrice.Name = "uLabelPurchasePrice";
            this.uLabelPurchasePrice.Size = new System.Drawing.Size(100, 20);
            this.uLabelPurchasePrice.TabIndex = 88;
            this.uLabelPurchasePrice.Text = "설비도입가";
            // 
            // uLabelReason
            // 
            this.uLabelReason.Location = new System.Drawing.Point(12, 76);
            this.uLabelReason.Name = "uLabelReason";
            this.uLabelReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelReason.TabIndex = 88;
            this.uLabelReason.Text = "폐기사유";
            // 
            // uTextDiscardUserName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDiscardUserName.Appearance = appearance36;
            this.uTextDiscardUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDiscardUserName.Location = new System.Drawing.Point(504, 28);
            this.uTextDiscardUserName.Name = "uTextDiscardUserName";
            this.uTextDiscardUserName.ReadOnly = true;
            this.uTextDiscardUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextDiscardUserName.TabIndex = 87;
            // 
            // uLabelDiscardDate
            // 
            this.uLabelDiscardDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelDiscardDate.Name = "uLabelDiscardDate";
            this.uLabelDiscardDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelDiscardDate.TabIndex = 84;
            this.uLabelDiscardDate.Text = "폐기일";
            // 
            // uDateDiscardTime
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDiscardTime.Appearance = appearance1;
            this.uDateDiscardTime.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDiscardTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDiscardTime.Location = new System.Drawing.Point(116, 28);
            this.uDateDiscardTime.MaskInput = "{date}";
            this.uDateDiscardTime.Name = "uDateDiscardTime";
            this.uDateDiscardTime.Size = new System.Drawing.Size(100, 21);
            this.uDateDiscardTime.TabIndex = 83;
            this.uDateDiscardTime.ValueChanged += new System.EventHandler(this.uDateDiscardTime_ValueChanged);
            // 
            // uLabelDiscardUser
            // 
            this.uLabelDiscardUser.Location = new System.Drawing.Point(296, 28);
            this.uLabelDiscardUser.Name = "uLabelDiscardUser";
            this.uLabelDiscardUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelDiscardUser.TabIndex = 85;
            this.uLabelDiscardUser.Text = "교체";
            // 
            // titleArea
            // 
            this.titleArea.BackColor = System.Drawing.SystemColors.Control;
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 4;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridEquip
            // 
            this.uGridEquip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquip.DisplayLayout.Appearance = appearance38;
            this.uGridEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.uGridEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance37.BackColor2 = System.Drawing.SystemColors.Control;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance37;
            this.uGridEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquip.DisplayLayout.Override.ActiveCellAppearance = appearance46;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquip.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.uGridEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.CardAreaAppearance = appearance40;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            appearance39.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquip.DisplayLayout.Override.CellAppearance = appearance39;
            this.uGridEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquip.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance45.TextHAlignAsString = "Left";
            this.uGridEquip.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.uGridEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquip.DisplayLayout.Override.RowAppearance = appearance44;
            this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance42;
            this.uGridEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquip.Location = new System.Drawing.Point(0, 80);
            this.uGridEquip.Name = "uGridEquip";
            this.uGridEquip.Size = new System.Drawing.Size(1070, 740);
            this.uGridEquip.TabIndex = 7;
            this.uGridEquip.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridEquip_DoubleClickRow);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance47;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 8;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchPlant.TabIndex = 1;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 9;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // frmEQUZ0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGridEquip);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0005";
            this.Load += new System.EventHandler(this.frmEQUZ0005_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0005_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0005_FormClosing);
            this.Resize += new System.EventHandler(this.frmEQUZ0005_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMDM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckGWResultState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckGWTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSysName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRemainPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumPurchasePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipDiscardFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDiscardTime;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardReason;
        private Infragistics.Win.Misc.UltraLabel uLabelReason;
        private Infragistics.Win.Misc.UltraLabel uLabelFile;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipDiscardFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSTSStock;
        private Infragistics.Win.Misc.UltraLabel uLabelSTSStock;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipLevel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakerYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSuperEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSysName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelLocation;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSuperSys;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardUserID;
        private Infragistics.Win.Misc.UltraButton uButtonDownLoad;
        private Infragistics.Win.Misc.UltraLabel uLabelRemainPrice;
        private Infragistics.Win.Misc.UltraLabel uLabelPurchasePrice;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumPurchasePrice;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRemainPrice;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckGWResultState;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckGWTFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquip;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraButton uButtonDownDocFile;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMDM;
    }
}