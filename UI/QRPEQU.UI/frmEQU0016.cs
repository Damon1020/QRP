﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 통문관리                                              */
/* 프로그램ID   : frmEQU0014.cs                                         */
/* 프로그램명   : 반입취소                                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-01-02                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-xx-xx : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQU0016 : Form,IToolbar
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();


        public frmEQU0016()
        {
            InitializeComponent();
        }

        private void frmEQU0016_Activated(object sender, EventArgs e)
        {
            //Resource Info
            ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"),this.Name);
        }

        private void frmEQU0016_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQU0016_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitText();
            InitCombo();
            InitGrid();

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchDate, "반입일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchMaterial, "구성품구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDocCode, "발행번호", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelCancelDate, "반입취소일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelCancelChgID, "반입취소담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("반입취소", m_resSys.GetString("SYSFONTNAME"), 12);
               


            }catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextChgID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextChgName.Text = m_resSys.GetString("SYS_USERNAME");


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor com = new WinComboEditor();

                com.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true,
                                    false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "",
                                    "선택", "PlantCode", "PlantName", dtPlant);

                //설비구성품구분 콤보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsSYS = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsSYS);

                DataTable dtCommon = clsSYS.mfReadCommonCode("C0015", m_resSys.GetString("SYS_LANG"));

                com.mfSetComboEditor(this.uComboMaterial, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true
                    , 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtCommon);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCancel, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Extended, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCancel, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "GRCancelReason", "반입취소사유", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "DocCode", "발행번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "CarryInSeq", "반입순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "ItemGubunCode", "구성품구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "ItemGubunName", "구성품구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "ItemCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "ItemName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "GRQty", "반입수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCancel, 0, "GRInventoryCode", "반입창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Toolbar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                System.Windows.Forms.DialogResult result;

                #region 필수입력사항체크

                if (this.uDateTimeFrom.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001234", "M000209", Infragistics.Win.HAlign.Right);

                    this.uDateTimeFrom.DropDown();
                    return;
                }
                if (this.uDateTimeTo.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001234", "M000218", Infragistics.Win.HAlign.Right);

                    this.uDateTimeTo.DropDown();
                    return;
                }
                if (Convert.ToDateTime(this.uDateTimeFrom.Value) > Convert.ToDateTime(this.uDateTimeTo.Value))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001234", "M000422", Infragistics.Win.HAlign.Right);

                    this.uDateTimeFrom.DropDown();
                    return;
                }

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboMaterial.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001235", "M000313", "M000312", Infragistics.Win.HAlign.Right);

                    this.uComboMaterial.DropDown();
                    return;
                }

                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //반입정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryInD), "CarryInD");
                QRPEQU.BL.EQUCAR.CarryInD clsCarryInD = new QRPEQU.BL.EQUCAR.CarryInD();
                brwChannel.mfCredentials(clsCarryInD);

                //반입취소가능한 정보조회
                DataTable dtCancel = clsCarryInD.mfReadCarryInD_Cancel(this.uComboSearchPlant.Value.ToString(),this.uDateTimeFrom.DateTime.Date.ToString("yyyy-MM-dd"),
                                                                        this.uDateTimeTo.DateTime.Date.ToString("yyyy-MM-dd"),this.uComboMaterial.Value.ToString(),uTextDocCode.Text,
                                                                        m_resSys.GetString("SYS_LANG"));
                
                //그리드에 바인드
                this.uGridCancel.DataSource = dtCancel;
                this.uGridCancel.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtCancel.Rows.Count == 0)
                {
                    
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001135", "M001115", "M001102",
                                    Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridCancel, 0);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                System.Windows.Forms.DialogResult result;
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항체크

                if (this.uGridCancel.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001034", "M001032", "M001047", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001235", "M000271", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboMaterial.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001235", "M000313", "M000312", Infragistics.Win.HAlign.Right);

                    this.uComboMaterial.DropDown();
                    return;
                }
                if (this.uDateCancel.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001235", "M000429", "M000430", Infragistics.Win.HAlign.Right);

                    this.uDateCancel.DropDown();
                    return;
                }

                //반입취소담장자
                if (this.uTextChgID.Text.Equals(string.Empty) || this.uTextChgName.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   "M001235", "M000426", "M000425", Infragistics.Win.HAlign.Right);

                    this.uTextChgID.Focus();
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                #endregion

                #region BL정보저장
                //반입정보 Bl호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCAR.CarryInD), "CarryInD");
                QRPEQU.BL.EQUCAR.CarryInD clsCarryInD = new QRPEQU.BL.EQUCAR.CarryInD();
                brwChannel.mfCredentials(clsCarryInD);

                //반입취소컬럼정보매서드 실행
                DataTable dtCancel = clsCarryInD.mfDataSetCarryInD_Cancel();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strItemGubunCode = this.uComboMaterial.Value.ToString();
                string strCancelDate = this.uDateCancel.DateTime.Date.ToString("yyyy-MM-dd");
                string strCancelChgID = this.uTextChgID.Text;
                #endregion

                #region 반입취소정보저장

                if (this.uGridCancel.Rows.Count > 0)
                {
                    this.uGridCancel.ActiveCell = this.uGridCancel.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridCancel.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCancel.Rows[i].Cells["Check"].Value))
                        {
                            if (this.uGridCancel.Rows[i].GetCellValue("GRCancelReason").ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001235", "M000428", "M000427", Infragistics.Win.HAlign.Right);

                                this.uGridCancel.ActiveCell = this.uGridCancel.Rows[i].Cells["GRCancelReason"];
                                this.uGridCancel.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            //필요정보 저장
                            DataRow drCancel = dtCancel.NewRow();

                            drCancel["PlantCode"] = strPlantCode;                                                   //공장
                            drCancel["DocCode"] = this.uGridCancel.Rows[i].GetCellValue("DocCode");                 //문서번호
                            drCancel["Seq"] = this.uGridCancel.Rows[i].GetCellValue("Seq");                         //순번
                            drCancel["CarryInSeq"] = this.uGridCancel.Rows[i].GetCellValue("CarryInSeq");           //반입순번
                            drCancel["GRCancelID"] = strCancelChgID;                                                //반입취소담당자
                            drCancel["GRCancelDate"] = strCancelDate;                                               //반입취소일
                            drCancel["GRCancelReason"] = this.uGridCancel.Rows[i].GetCellValue("GRCancelReason");     //반입취소사유
                            drCancel["ItemGubunCode"] = this.uGridCancel.Rows[i].GetCellValue("ItemGubunCode");     //구성품구분
                            drCancel["GRInventoryCode"] = this.uGridCancel.Rows[i].GetCellValue("GRInventoryCode"); //반입된창고
                            drCancel["ItemCode"] = this.uGridCancel.Rows[i].GetCellValue("ItemCode");               //자재코드
                            drCancel["LotNo"] = this.uGridCancel.Rows[i].GetCellValue("LotNo");                     //LotNo
                            drCancel["GRQty"] = this.uGridCancel.Rows[i].GetCellValue("GRQty");                     //반입수량
                            drCancel["UnitCode"] = this.uGridCancel.Rows[i].GetCellValue("UnitCode");               //단위
                            drCancel["GRFlag"] = "E";           // 반입강제취소일 경우는 WEB반출에 "E"로 전송함
                            dtCancel.Rows.Add(drCancel);


                        }

                    }
                }

                if (dtCancel.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                  "저장정보확인창", "저장정보 확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;

                }

                #endregion


                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000431", "M000957",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    //반입취소 매서드 실행
                    string strErrRtn = clsCarryInD.mfSaveCarryInD_Cancel(dtCancel, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    //처리결과에 따라서 메세지박스를 띄운다.
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001177", "M000933",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMessage = "";
                        string strLang = m_resSys.GetString("SYS_LANG");

                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000958",strLang);
                        else
                            strMessage = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001177",strLang), strMessage,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridCancel.Rows.Count == 0)
                {
                    DialogResult dr = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001174", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridCancel);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
        }

        public void mfCreate()
        {

        }

        public void mfDelete()
        {
        }

        #endregion

        #region Combo

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            mfGridClear();
        }

        private void uComboMaterial_ValueChanged(object sender, EventArgs e)
        {
            mfGridClear();
        }
        
        #endregion

        #region Text

        /// <summary>
        /// 반입취소담당자가 바뀔시 담당자명 클리어
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextChgID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextChgName.Text.Equals(string.Empty))
                this.uTextChgName.Clear();
        }

        /// <summary>
        /// 반입취소담당자 직접입력이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextChgID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //입력된 키가 백스페이스나 Delete키일 경우 담당자명을 클리어시킨다.
                if ((e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete)) && !this.uTextChgName.Text.Equals(string.Empty))
                {
                    this.uTextChgName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextChgID.Text.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //유저정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //사용자정보 조회매서드 실행
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, this.uTextChgID.Text, m_resSys.GetString("SYS_LANG"));

                    //정보가 존재 할경우 담당자명을 채우고 정보가 없는 경우 메세지를 띄운다.
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextChgName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001116", "M000883", Infragistics.Win.HAlign.Right);

                        if (!this.uTextChgName.Text.Equals(string.Empty))
                            this.uTextChgName.Clear();

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 반입취소담당자 사용자팝업창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextChgID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                {
                    WinMessageBox msg = new WinMessageBox();
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;

                }

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextChgID.Text = frmUser.UserID;
                this.uTextChgName.Text = frmUser.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        /// <summary>
        /// 그리드 클리어
        /// </summary>
        private void mfGridClear()
        {
            try
            {
                //반입취소 그리드의 한줄이상일 경우 전체행을 선택하여 삭제한다.
                if (this.uGridCancel.Rows.Count > 0)
                {
                    this.uGridCancel.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridCancel.Rows.All);
                    this.uGridCancel.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }









    }


}
