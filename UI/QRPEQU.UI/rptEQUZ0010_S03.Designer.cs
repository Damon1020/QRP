﻿namespace QRPEQU.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0010_S03.
    /// </summary>
    partial class rptEQUZ0010_S03
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptEQUZ0010_S03));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textSetupDate = new DataDynamics.ActiveReports.TextBox();
            this.textJudgeResultName = new DataDynamics.ActiveReports.TextBox();
            this.textJudgeStandard = new DataDynamics.ActiveReports.TextBox();
            this.textSampleSize = new DataDynamics.ActiveReports.TextBox();
            this.textEvalResultCode = new DataDynamics.ActiveReports.TextBox();
            this.groupHeader = new DataDynamics.ActiveReports.GroupHeader();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.label4 = new DataDynamics.ActiveReports.Label();
            this.label5 = new DataDynamics.ActiveReports.Label();
            this.label6 = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.label3 = new DataDynamics.ActiveReports.Label();
            this.groupFooter = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textSetupDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textJudgeResultName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textJudgeStandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSampleSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEvalResultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textSetupDate,
            this.textJudgeResultName,
            this.textJudgeStandard,
            this.textSampleSize,
            this.textEvalResultCode});
            this.detail.Height = 0.497917F;
            this.detail.Name = "detail";
            // 
            // textSetupDate
            // 
            this.textSetupDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSetupDate.DataField = "CertiItem";
            this.textSetupDate.Height = 0.5F;
            this.textSetupDate.Left = 0.04F;
            this.textSetupDate.Name = "textSetupDate";
            this.textSetupDate.Style = "font-size: 7.5pt; font-weight: normal; text-align: left; vertical-align: middle";
            this.textSetupDate.Text = null;
            this.textSetupDate.Top = 0F;
            this.textSetupDate.Width = 1.395001F;
            // 
            // textJudgeResultName
            // 
            this.textJudgeResultName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeResultName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeResultName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeResultName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeResultName.DataField = "JudgeResultName";
            this.textJudgeResultName.Height = 0.5F;
            this.textJudgeResultName.Left = 5.917F;
            this.textJudgeResultName.Name = "textJudgeResultName";
            this.textJudgeResultName.Style = "font-size: 7.5pt; font-weight: normal; text-align: center; vertical-align: middle" +
                "";
            this.textJudgeResultName.Text = null;
            this.textJudgeResultName.Top = 3.278255E-07F;
            this.textJudgeResultName.Width = 1.242997F;
            // 
            // textJudgeStandard
            // 
            this.textJudgeStandard.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeStandard.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeStandard.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeStandard.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textJudgeStandard.DataField = "JudgeStandard";
            this.textJudgeStandard.Height = 0.5F;
            this.textJudgeStandard.Left = 2.446F;
            this.textJudgeStandard.Name = "textJudgeStandard";
            this.textJudgeStandard.Style = "font-size: 7.5pt; font-weight: normal; text-align: left; vertical-align: middle";
            this.textJudgeStandard.Text = null;
            this.textJudgeStandard.Top = 0F;
            this.textJudgeStandard.Width = 2.278F;
            // 
            // textSampleSize
            // 
            this.textSampleSize.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSampleSize.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSampleSize.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSampleSize.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSampleSize.DataField = "SampleSize";
            this.textSampleSize.Height = 0.5F;
            this.textSampleSize.Left = 1.435F;
            this.textSampleSize.Name = "textSampleSize";
            this.textSampleSize.Style = "font-size: 7.5pt; font-weight: normal; text-align: right; vertical-align: middle";
            this.textSampleSize.Text = null;
            this.textSampleSize.Top = 0F;
            this.textSampleSize.Width = 1.011F;
            // 
            // textEvalResultCode
            // 
            this.textEvalResultCode.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEvalResultCode.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEvalResultCode.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEvalResultCode.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEvalResultCode.DataField = "EvalResultCode";
            this.textEvalResultCode.Height = 0.5F;
            this.textEvalResultCode.Left = 4.724F;
            this.textEvalResultCode.Name = "textEvalResultCode";
            this.textEvalResultCode.Style = "font-size: 7.5pt; font-weight: normal; text-align: left; vertical-align: middle";
            this.textEvalResultCode.Text = null;
            this.textEvalResultCode.Top = 0F;
            this.textEvalResultCode.Width = 1.193F;
            // 
            // groupHeader
            // 
            this.groupHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label1,
            this.label4,
            this.label5,
            this.label6,
            this.label2,
            this.label3});
            this.groupHeader.Height = 0.5110836F;
            this.groupHeader.Name = "groupHeader";
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.03999949F;
            this.label1.Name = "label1";
            this.label1.Style = "font-weight: bold";
            this.label1.Text = "5. 품질 CHECK";
            this.label1.Top = 0.05199978F;
            this.label1.Width = 2.134F;
            // 
            // label4
            // 
            this.label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Height = 0.2F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.03999949F;
            this.label4.Name = "label4";
            this.label4.Style = "font-weight: bold; text-align: center";
            this.label4.Text = "인증항목";
            this.label4.Top = 0.3109997F;
            this.label4.Width = 1.395001F;
            // 
            // label5
            // 
            this.label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Height = 0.2F;
            this.label5.HyperLink = null;
            this.label5.Left = 5.917001F;
            this.label5.Name = "label5";
            this.label5.Style = "font-weight: bold; text-align: center";
            this.label5.Text = "판정";
            this.label5.Top = 0.311F;
            this.label5.Width = 1.242999F;
            // 
            // label6
            // 
            this.label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Height = 0.2F;
            this.label6.HyperLink = null;
            this.label6.Left = 2.446F;
            this.label6.Name = "label6";
            this.label6.Style = "font-weight: bold; text-align: center";
            this.label6.Text = "판정기준";
            this.label6.Top = 0.311F;
            this.label6.Width = 2.278F;
            // 
            // label2
            // 
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.435F;
            this.label2.Name = "label2";
            this.label2.Style = "font-weight: bold; text-align: center";
            this.label2.Text = "SampleSize";
            this.label2.Top = 0.3109997F;
            this.label2.Width = 1.011F;
            // 
            // label3
            // 
            this.label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.724002F;
            this.label3.Name = "label3";
            this.label3.Style = "font-weight: bold; text-align: center";
            this.label3.Text = "평가결과";
            this.label3.Top = 0.3109997F;
            this.label3.Width = 1.193F;
            // 
            // groupFooter
            // 
            this.groupFooter.Height = 0F;
            this.groupFooter.Name = "groupFooter";
            // 
            // rptEQUZ0010_S03
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.184F;
            this.Sections.Add(this.groupHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textSetupDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textJudgeResultName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textJudgeStandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSampleSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEvalResultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.GroupHeader groupHeader;
        private DataDynamics.ActiveReports.GroupFooter groupFooter;
        private DataDynamics.ActiveReports.Label label1;
        private DataDynamics.ActiveReports.Label label4;
        private DataDynamics.ActiveReports.Label label5;
        private DataDynamics.ActiveReports.Label label6;
        private DataDynamics.ActiveReports.Label label2;
        private DataDynamics.ActiveReports.Label label3;
        private DataDynamics.ActiveReports.TextBox textSetupDate;
        private DataDynamics.ActiveReports.TextBox textJudgeResultName;
        private DataDynamics.ActiveReports.TextBox textJudgeStandard;
        private DataDynamics.ActiveReports.TextBox textSampleSize;
        private DataDynamics.ActiveReports.TextBox textEvalResultCode;
    }
}
