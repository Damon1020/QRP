﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQUZ0017.cs                                        */
/* 프로그램명   : 자재교체등록                                          */
/* 작성자       : 권종구, 코딩:남현식                                   */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0017 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0017()
        {
            InitializeComponent();
        }
       

        private void frmEQUZ0017_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmEQUZ0017_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmEQUZ0017_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("자재교체등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값지정
                uTextChgChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextChgChargeName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelChgDate, "교체일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelChgChargeID, "교체담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGridSPTransferD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,true , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferCode", "출고문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSeq", "출고순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferDate", "출고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferChargeID", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferChargeName", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "자재출고유형", "자재출고유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPCode", "Sparepart코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPName", "Sparepart명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPInventoryCode", "출고창고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferSPInventoryName", "출고창고명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgEquipCode", "교체설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit , "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgEquipName", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgSparePartCode", "교체SparePart", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgSparePartName", "교체SparePart", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgSpec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "ChgMaker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "TransferQty", "출고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPTransferD, 0, "InputQty", "출고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

               
                //grd.mfSetGridColumn(this.uGrid1, 0, "교체처리방법", "교체처리방법", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "선택");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridSPTransferD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSPTransferD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //그룹박스초기화
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "자재교체리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                    Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트지정
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
#endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;

                // Call Method
                DataTable dtSPTransferD = clsSPTransferD.mfReadEQUSPTransferD_Chg(strPlantCode, strEquipCode, "", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridSPTransferD.DataSource = dtSPTransferD;
                uGridSPTransferD.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtSPTransferD.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001115", "M001102",
                                                                           Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSPTransferD, 0);
                }
                        //if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value) == true)
                        //{
                            
                        //}

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;
                int intRowCheck = 0;

                // 필수입력사항 확인
                if (this.uTextChgChargeID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000296", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextChgChargeID.Focus();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextChgChargeName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000296", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextChgChargeID.Focus();
                    return;
                }

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsBOM);


                // 필수입력사항 확인
                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];

                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;


                            string strPlantCode = this.uGridSPTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            string strEquipCode = this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            string strSparePartCode = this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString();

                            DataTable dtBOM = new DataTable();


                            dtBOM = clsBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));
                            string strLang = m_resSys.GetString("SYS_LANG");
                            if (dtBOM.Rows.Count > 0)
                            {
                                int intBOMInputQty = Convert.ToInt32(dtBOM.Rows[0]["InputQty"].ToString());
                                int intInputQty = Convert.ToInt32(this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString());

                                if (intInputQty > intBOMInputQty)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000461",strLang)
                                            , Infragistics.Win.HAlign.Center);

                                    // Focus Cell
                                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["Check"];
                                    this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                            }
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridSPTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000460",strLang)
                                            , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[i].Cells["Check"];
                                this.uGridSPTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }                        
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001236", Infragistics.Win.HAlign.Center);
                    return;
                }


                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000681"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                

                //-------- 1.  EQUSPTransferD 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);

                DataTable dtSPTransferD = clsSPTransferD.mfSetDatainfo();

                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtSPTransferD.NewRow();
                            row["PlantCode"] = this.uGridSPTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["TransferCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferCode"].Value.ToString();
                            row["TransferSeq"] = this.uGridSPTransferD.Rows[i].Cells["TransferSeq"].Value.ToString();
                            row["TransferSPInventoryCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPInventoryCode"].Value.ToString();
                            row["TransferSPCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                            row["TransferQty"] = this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["ChgEquipCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            row["ChgSparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                            row["InputQty"] = this.uGridSPTransferD.Rows[i].Cells["InputQty"].Value.ToString();
                            row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            row["TransferEtcDesc"] = this.uGridSPTransferD.Rows[i].Cells["TransferEtcDesc"].Value.ToString();
                            row["ChgFlag"] = "T";
                            row["ChgDate"] = this.uDateChgDate.DateTime.Date.ToString("yyyy-MM-dd");
                            row["ChgChargeID"] = this.uTextChgChargeID.Text;
                            row["ChgEtcDesc"] = "";
                            row["ReturnFlag"] = "";
                            row["ReturnDate"] = "";
                            row["ReturnChargeID"] = "";
                            row["ReturnSPInventoryCode"] = "";
                            row["ReturnEtcDesc"] = "";
                            dtSPTransferD.Rows.Add(row);
                        }
                    }
                }

                //-------- 2.  EQUSPChgStandby 테이블에 저장 (수량 - 처리) : 설비에 투입될 SparePart : TransferSPCode ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPChgStandby), "SPChgStandby");
                QRPEQU.BL.EQUSPA.SPChgStandby clsSPChgStandby = new QRPEQU.BL.EQUSPA.SPChgStandby();
                brwChannel.mfCredentials(clsSPChgStandby);
                DataTable dtSPChgStandbyInput = clsSPChgStandby.mfSetDatainfo();

                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtSPChgStandbyInput.NewRow();
                            row["PlantCode"] = this.uGridSPTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["SparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                            row["ChgQty"] = "-" + this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            dtSPChgStandbyInput.Rows.Add(row);
                        }
                    }
                }


                //-------- 3.  EQUSPChgStandby 테이블에 저장 (수량 + 처리) : 설비에서 교체된 SparePart : ChgSparePartCode ----------------------------------------------//;
                DataTable dtSPChgStandbyOut = clsSPChgStandby.mfSetDatainfo();

                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtSPChgStandbyOut.NewRow();
                            row["PlantCode"] = this.uGridSPTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["SparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                            row["ChgQty"] =  this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            dtSPChgStandbyOut.Rows.Add(row);
                        }
                    }
                }

                //-------- 4.  MASEquipSPBOM 테이블 : 교체처리 ----------------------------------------------//;
                DataTable dtEquipSPBOM = clsSPTransferD.mfSetDatainfo();
                for (int i = 0; i < this.uGridSPTransferD.Rows.Count; i++)
                {
                    this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    if (this.uGridSPTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridSPTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtEquipSPBOM.NewRow();
                            row["PlantCode"] = this.uGridSPTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["TransferSPCode"] = this.uGridSPTransferD.Rows[i].Cells["TransferSPCode"].Value.ToString();
                            row["TransferQty"] = this.uGridSPTransferD.Rows[i].Cells["TransferQty"].Value.ToString();

                            row["ChgEquipCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            row["ChgSparePartCode"] = this.uGridSPTransferD.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                            row["UnitCode"] = this.uGridSPTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            dtEquipSPBOM.Rows.Add(row);
                        }
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SAVESparePartStock), "SAVESparePartStock");
                QRPEQU.BL.EQUSPA.SAVESparePartStock clsSAVESparePartStock = new QRPEQU.BL.EQUSPA.SAVESparePartStock();
                brwChannel.mfCredentials(clsSAVESparePartStock);
                string rtMSG = clsSAVESparePartStock.mfSaveSparePartChange(dtSPTransferD, dtSPChgStandbyInput, dtSPChgStandbyOut, dtEquipSPBOM, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    //mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGridSPTransferD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridSPTransferD);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Event

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridSPTransferD, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }

        #region 교체담당자
        private void uTextChgChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextChgChargeID;
                uTextName = this.uTextChgChargeName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextChgChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextChgChargeID.Text = frmPOP.UserID;
                this.uTextChgChargeName.Text = frmPOP.UserName;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextChgChargeID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextChgChargeName.Text.Equals(string.Empty))
                this.uTextChgChargeName.Clear();
        }
        #endregion
        private void frmEQUZ0017_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextEquipName.Text.Equals(string.Empty))
                this.uTextEquipName.Clear();
        }

        //설비 팝업창띄우기
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //공장정보 저장
                string strPlantCode = this.uComboPlant.Value.ToString();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                //공장정보가 공백일 경우 메세지를 띄운다.
                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                this.uTextEquipCode.Text = frmEquip.EquipCode;
                this.uTextEquipName.Text = frmEquip.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비코드입력후 엔터 키 누를 시 설비정보 검색
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!this.uTextEquipCode.Text.Equals(string.Empty) &&
                    e.KeyData.Equals(Keys.Enter))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    //공장정보 저장
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    //공장정보가 공백일 경우 메세지
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;
                    }

                    //설비정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보조회매서드 실행
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                    //정보가 있을 경우 설비명에 입력 정보가 없을 경우 메세지 박스를 띄운다.
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001113", "M000901", Infragistics.Win.HAlign.Right);
                        this.uTextEquipName.Clear();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

    }
}
