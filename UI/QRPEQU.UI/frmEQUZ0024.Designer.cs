﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0024
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0024));
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uComboSearchPMMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridPMResultList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uNumSerachPlanYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uComboSerachPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPMMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchPlanYear = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPMMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResultList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSerachPlanYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSerachPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).BeginInit();
            this.uGroupSearchArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uComboSearchPMMonth
            // 
            this.uComboSearchPMMonth.Location = new System.Drawing.Point(616, 12);
            this.uComboSearchPMMonth.MaxLength = 3;
            this.uComboSearchPMMonth.Name = "uComboSearchPMMonth";
            this.uComboSearchPMMonth.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchPMMonth.TabIndex = 3;
            this.uComboSearchPMMonth.ValueChanged += new System.EventHandler(this.uComboSearchPMMonth_ValueChanged);
            // 
            // uTextEquipName
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance14;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(948, 12);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 4;
            // 
            // uTextEquipCode
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.Appearance = appearance16;
            this.uTextEquipCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance15.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(844, 12);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 4;
            this.uTextEquipCode.ValueChanged += new System.EventHandler(this.uTextEquipCode_ValueChanged);
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uGridPMResultList
            // 
            this.uGridPMResultList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMResultList.DisplayLayout.Appearance = appearance2;
            this.uGridPMResultList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMResultList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResultList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResultList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridPMResultList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResultList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridPMResultList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMResultList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMResultList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMResultList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridPMResultList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMResultList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMResultList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMResultList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridPMResultList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMResultList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResultList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridPMResultList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridPMResultList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMResultList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMResultList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridPMResultList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMResultList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridPMResultList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMResultList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMResultList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMResultList.Location = new System.Drawing.Point(0, 80);
            this.uGridPMResultList.Name = "uGridPMResultList";
            this.uGridPMResultList.Size = new System.Drawing.Size(1070, 760);
            this.uGridPMResultList.TabIndex = 4;
            // 
            // uNumSerachPlanYear
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumSerachPlanYear.Appearance = appearance18;
            this.uNumSerachPlanYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumSerachPlanYear.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumSerachPlanYear.ButtonsLeft.Add(editorButton2);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumSerachPlanYear.ButtonsRight.Add(spinEditorButton1);
            this.uNumSerachPlanYear.Location = new System.Drawing.Point(388, 12);
            this.uNumSerachPlanYear.MaxValue = 3000;
            this.uNumSerachPlanYear.MinValue = 2000;
            this.uNumSerachPlanYear.Name = "uNumSerachPlanYear";
            this.uNumSerachPlanYear.PromptChar = ' ';
            this.uNumSerachPlanYear.Size = new System.Drawing.Size(100, 19);
            this.uNumSerachPlanYear.TabIndex = 2;
            this.uNumSerachPlanYear.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumSerachPlanYear_EditorSpinButtonClick);
            this.uNumSerachPlanYear.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumSerachPlanYear_EditorButtonClick);
            // 
            // uComboSerachPlant
            // 
            this.uComboSerachPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSerachPlant.MaxLength = 50;
            this.uComboSerachPlant.Name = "uComboSerachPlant";
            this.uComboSerachPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSerachPlant.TabIndex = 1;
            // 
            // uLabelSearchEquipCode
            // 
            this.uLabelSearchEquipCode.Location = new System.Drawing.Point(740, 12);
            this.uLabelSearchEquipCode.Name = "uLabelSearchEquipCode";
            this.uLabelSearchEquipCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipCode.TabIndex = 0;
            // 
            // uLabelSearchPMMonth
            // 
            this.uLabelSearchPMMonth.Location = new System.Drawing.Point(512, 12);
            this.uLabelSearchPMMonth.Name = "uLabelSearchPMMonth";
            this.uLabelSearchPMMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPMMonth.TabIndex = 0;
            // 
            // uGroupSearchArea
            // 
            this.uGroupSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupSearchArea.Appearance = appearance1;
            this.uGroupSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupSearchArea.Controls.Add(this.uComboSearchPMMonth);
            this.uGroupSearchArea.Controls.Add(this.uTextEquipName);
            this.uGroupSearchArea.Controls.Add(this.uTextEquipCode);
            this.uGroupSearchArea.Controls.Add(this.uNumSerachPlanYear);
            this.uGroupSearchArea.Controls.Add(this.uComboSerachPlant);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchEquipCode);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchPMMonth);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchPlanYear);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupSearchArea.Name = "uGroupSearchArea";
            this.uGroupSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupSearchArea.TabIndex = 3;
            // 
            // uLabelSearchPlanYear
            // 
            this.uLabelSearchPlanYear.Location = new System.Drawing.Point(284, 12);
            this.uLabelSearchPlanYear.Name = "uLabelSearchPlanYear";
            this.uLabelSearchPlanYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlanYear.TabIndex = 0;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // frmEQUZ0024
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridPMResultList);
            this.Controls.Add(this.uGroupSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0024";
            this.Load += new System.EventHandler(this.frmEQUZ0024_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0024_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPMMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResultList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSerachPlanYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSerachPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).EndInit();
            this.uGroupSearchArea.ResumeLayout(false);
            this.uGroupSearchArea.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPMMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMResultList;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSerachPlanYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSerachPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPMMonth;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlanYear;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}