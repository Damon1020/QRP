﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0004
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0004));
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSTSStock = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSTSStock = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipLevel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSysProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLocation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextArea = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSuperEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSysGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLocation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSuperSys = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSysCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextDeviceChgUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridDurableMatRepairChg = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionCCSFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uGridDeviceChgResultD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDeviceChgUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateDeviceChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboWorkGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDeviceChgDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridDeviceChgResultH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSysProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeviceChgUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatRepairChg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCCSFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgResultD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeviceChgUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDeviceChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWorkGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeviceChgDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgResultH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uTextSTSStock);
            this.uGroupBox.Controls.Add(this.uLabelSTSStock);
            this.uGroupBox.Controls.Add(this.uTextEquipLevel);
            this.uGroupBox.Controls.Add(this.uTextYear);
            this.uGroupBox.Controls.Add(this.uTextVendor);
            this.uGroupBox.Controls.Add(this.uTextEquipGroupName);
            this.uGroupBox.Controls.Add(this.uTextSerialNo);
            this.uGroupBox.Controls.Add(this.uTextEquipType);
            this.uGroupBox.Controls.Add(this.uTextModel);
            this.uGroupBox.Controls.Add(this.uTextSysProcess);
            this.uGroupBox.Controls.Add(this.uTextLocation);
            this.uGroupBox.Controls.Add(this.uTextStation);
            this.uGroupBox.Controls.Add(this.uTextArea);
            this.uGroupBox.Controls.Add(this.uTextSuperEquip);
            this.uGroupBox.Controls.Add(this.uTextEquipName);
            this.uGroupBox.Controls.Add(this.uTextEquipCode);
            this.uGroupBox.Controls.Add(this.uComboPlant);
            this.uGroupBox.Controls.Add(this.uLabelSysGrade);
            this.uGroupBox.Controls.Add(this.uLabelYear);
            this.uGroupBox.Controls.Add(this.uLabelVendor);
            this.uGroupBox.Controls.Add(this.uLabelSysGroupName);
            this.uGroupBox.Controls.Add(this.uLabelSerialNo);
            this.uGroupBox.Controls.Add(this.uLabelSysType);
            this.uGroupBox.Controls.Add(this.uLabelModel);
            this.uGroupBox.Controls.Add(this.uLabelSysProcess);
            this.uGroupBox.Controls.Add(this.uLabelLocation);
            this.uGroupBox.Controls.Add(this.uLabelStation);
            this.uGroupBox.Controls.Add(this.uLabelArea);
            this.uGroupBox.Controls.Add(this.uLabelSuperSys);
            this.uGroupBox.Controls.Add(this.uLabelSysName);
            this.uGroupBox.Controls.Add(this.uLabelSysCode);
            this.uGroupBox.Controls.Add(this.uLabelPlant);
            this.uGroupBox.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1040, 196);
            this.uGroupBox.TabIndex = 3;
            // 
            // uTextSTSStock
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Appearance = appearance5;
            this.uTextSTSStock.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTSStock.Location = new System.Drawing.Point(688, 124);
            this.uTextSTSStock.Name = "uTextSTSStock";
            this.uTextSTSStock.ReadOnly = true;
            this.uTextSTSStock.Size = new System.Drawing.Size(108, 21);
            this.uTextSTSStock.TabIndex = 69;
            // 
            // uLabelSTSStock
            // 
            this.uLabelSTSStock.Location = new System.Drawing.Point(584, 124);
            this.uLabelSTSStock.Name = "uLabelSTSStock";
            this.uLabelSTSStock.Size = new System.Drawing.Size(100, 20);
            this.uLabelSTSStock.TabIndex = 68;
            this.uLabelSTSStock.Text = "STS입고일";
            // 
            // uTextEquipLevel
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Appearance = appearance22;
            this.uTextEquipLevel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Location = new System.Drawing.Point(400, 148);
            this.uTextEquipLevel.Name = "uTextEquipLevel";
            this.uTextEquipLevel.ReadOnly = true;
            this.uTextEquipLevel.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipLevel.TabIndex = 67;
            // 
            // uTextYear
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextYear.Appearance = appearance4;
            this.uTextYear.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextYear.Location = new System.Drawing.Point(116, 148);
            this.uTextYear.Name = "uTextYear";
            this.uTextYear.ReadOnly = true;
            this.uTextYear.Size = new System.Drawing.Size(108, 21);
            this.uTextYear.TabIndex = 66;
            // 
            // uTextVendor
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance6;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 124);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(108, 21);
            this.uTextVendor.TabIndex = 65;
            // 
            // uTextEquipGroupName
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Appearance = appearance7;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(400, 124);
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.ReadOnly = true;
            this.uTextEquipGroupName.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipGroupName.TabIndex = 64;
            // 
            // uTextSerialNo
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance8;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(688, 100);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(228, 21);
            this.uTextSerialNo.TabIndex = 63;
            // 
            // uTextEquipType
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Appearance = appearance9;
            this.uTextEquipType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Location = new System.Drawing.Point(400, 100);
            this.uTextEquipType.Name = "uTextEquipType";
            this.uTextEquipType.ReadOnly = true;
            this.uTextEquipType.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipType.TabIndex = 62;
            // 
            // uTextModel
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance10;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(116, 100);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(108, 21);
            this.uTextModel.TabIndex = 61;
            // 
            // uTextSysProcess
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSysProcess.Appearance = appearance11;
            this.uTextSysProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSysProcess.Location = new System.Drawing.Point(400, 76);
            this.uTextSysProcess.Name = "uTextSysProcess";
            this.uTextSysProcess.ReadOnly = true;
            this.uTextSysProcess.Size = new System.Drawing.Size(108, 21);
            this.uTextSysProcess.TabIndex = 60;
            // 
            // uTextLocation
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Appearance = appearance12;
            this.uTextLocation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Location = new System.Drawing.Point(688, 76);
            this.uTextLocation.Name = "uTextLocation";
            this.uTextLocation.ReadOnly = true;
            this.uTextLocation.Size = new System.Drawing.Size(228, 21);
            this.uTextLocation.TabIndex = 59;
            // 
            // uTextStation
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Appearance = appearance13;
            this.uTextStation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Location = new System.Drawing.Point(688, 52);
            this.uTextStation.Name = "uTextStation";
            this.uTextStation.ReadOnly = true;
            this.uTextStation.Size = new System.Drawing.Size(228, 21);
            this.uTextStation.TabIndex = 58;
            // 
            // uTextArea
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Appearance = appearance16;
            this.uTextArea.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextArea.Location = new System.Drawing.Point(116, 76);
            this.uTextArea.Name = "uTextArea";
            this.uTextArea.ReadOnly = true;
            this.uTextArea.Size = new System.Drawing.Size(108, 21);
            this.uTextArea.TabIndex = 57;
            // 
            // uTextSuperEquip
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Appearance = appearance17;
            this.uTextSuperEquip.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Location = new System.Drawing.Point(400, 52);
            this.uTextSuperEquip.Name = "uTextSuperEquip";
            this.uTextSuperEquip.ReadOnly = true;
            this.uTextSuperEquip.Size = new System.Drawing.Size(108, 21);
            this.uTextSuperEquip.TabIndex = 56;
            // 
            // uTextEquipName
            // 
            appearance63.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance63;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextEquipName.Location = new System.Drawing.Point(116, 52);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipName.TabIndex = 55;
            // 
            // uTextEquipCode
            // 
            appearance64.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.Appearance = appearance64;
            this.uTextEquipCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance65.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance65.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance65;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(400, 28);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(108, 19);
            this.uTextEquipCode.TabIndex = 4;
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uComboPlant
            // 
            appearance50.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance50;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(108, 19);
            this.uComboPlant.TabIndex = 3;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelSysGrade
            // 
            this.uLabelSysGrade.Location = new System.Drawing.Point(296, 148);
            this.uLabelSysGrade.Name = "uLabelSysGrade";
            this.uLabelSysGrade.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysGrade.TabIndex = 52;
            this.uLabelSysGrade.Text = "설비등급";
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(12, 148);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 51;
            this.uLabelYear.Text = "제작년도";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 124);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelVendor.TabIndex = 50;
            this.uLabelVendor.Text = "Vendor";
            // 
            // uLabelSysGroupName
            // 
            this.uLabelSysGroupName.Location = new System.Drawing.Point(296, 124);
            this.uLabelSysGroupName.Name = "uLabelSysGroupName";
            this.uLabelSysGroupName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysGroupName.TabIndex = 49;
            this.uLabelSysGroupName.Text = "설비그룹명";
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(584, 100);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSerialNo.TabIndex = 48;
            this.uLabelSerialNo.Text = "SerialNo";
            // 
            // uLabelSysType
            // 
            this.uLabelSysType.Location = new System.Drawing.Point(296, 100);
            this.uLabelSysType.Name = "uLabelSysType";
            this.uLabelSysType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysType.TabIndex = 47;
            this.uLabelSysType.Text = "설비유형";
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(12, 100);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(100, 20);
            this.uLabelModel.TabIndex = 46;
            this.uLabelModel.Text = "모델";
            // 
            // uLabelSysProcess
            // 
            this.uLabelSysProcess.Location = new System.Drawing.Point(296, 76);
            this.uLabelSysProcess.Name = "uLabelSysProcess";
            this.uLabelSysProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysProcess.TabIndex = 45;
            this.uLabelSysProcess.Text = "설비공정부분";
            // 
            // uLabelLocation
            // 
            this.uLabelLocation.Location = new System.Drawing.Point(584, 76);
            this.uLabelLocation.Name = "uLabelLocation";
            this.uLabelLocation.Size = new System.Drawing.Size(100, 20);
            this.uLabelLocation.TabIndex = 44;
            this.uLabelLocation.Text = "위치";
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(584, 52);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 43;
            this.uLabelStation.Text = "Station";
            // 
            // uLabelArea
            // 
            this.uLabelArea.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelArea.Location = new System.Drawing.Point(12, 76);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelArea.TabIndex = 42;
            this.uLabelArea.Text = "Area";
            // 
            // uLabelSuperSys
            // 
            this.uLabelSuperSys.Location = new System.Drawing.Point(296, 52);
            this.uLabelSuperSys.Name = "uLabelSuperSys";
            this.uLabelSuperSys.Size = new System.Drawing.Size(100, 20);
            this.uLabelSuperSys.TabIndex = 41;
            this.uLabelSuperSys.Text = "Super설비";
            // 
            // uLabelSysName
            // 
            this.uLabelSysName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelSysName.Location = new System.Drawing.Point(12, 52);
            this.uLabelSysName.Name = "uLabelSysName";
            this.uLabelSysName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysName.TabIndex = 40;
            this.uLabelSysName.Text = "설비명";
            // 
            // uLabelSysCode
            // 
            this.uLabelSysCode.Location = new System.Drawing.Point(296, 28);
            this.uLabelSysCode.Name = "uLabelSysCode";
            this.uLabelSysCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysCode.TabIndex = 39;
            this.uLabelSysCode.Text = "설비코드";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 38;
            this.uLabelPlant.Text = "공장";
            // 
            // titleArea
            // 
            this.titleArea.BackColor = System.Drawing.SystemColors.Control;
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextDeviceChgUserID);
            this.uGroupBox1.Controls.Add(this.uGridDurableMatRepairChg);
            this.uGroupBox1.Controls.Add(this.uLabel9);
            this.uGroupBox1.Controls.Add(this.uOptionCCSFlag);
            this.uGroupBox1.Controls.Add(this.uGridDeviceChgResultD);
            this.uGroupBox1.Controls.Add(this.uLabel8);
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabel7);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.uTextDeviceChgUserName);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uDateDeviceChgDate);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uComboWorkGubun);
            this.uGroupBox1.Controls.Add(this.uLabel3);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Controls.Add(this.uTextVersionNum);
            this.uGroupBox1.Controls.Add(this.uTextStdNumber);
            this.uGroupBox1.Controls.Add(this.uTextDeviceChgDocCode);
            this.uGroupBox1.Controls.Add(this.uLabel1);
            this.uGroupBox1.Controls.Add(this.uComboProcess);
            this.uGroupBox1.Controls.Add(this.uLabelProcess);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 215);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 473);
            this.uGroupBox1.TabIndex = 4;
            // 
            // uTextDeviceChgUserID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDeviceChgUserID.Appearance = appearance15;
            this.uTextDeviceChgUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDeviceChgUserID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance35;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDeviceChgUserID.ButtonsRight.Add(editorButton2);
            this.uTextDeviceChgUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDeviceChgUserID.Location = new System.Drawing.Point(416, 28);
            this.uTextDeviceChgUserID.MaxLength = 20;
            this.uTextDeviceChgUserID.Name = "uTextDeviceChgUserID";
            this.uTextDeviceChgUserID.Size = new System.Drawing.Size(100, 19);
            this.uTextDeviceChgUserID.TabIndex = 9;
            this.uTextDeviceChgUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDeviceChgUserID_KeyDown);
            this.uTextDeviceChgUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDeviceChgUserID_EditorButtonClick);
            // 
            // uGridDurableMatRepairChg
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMatRepairChg.DisplayLayout.Appearance = appearance2;
            this.uGridDurableMatRepairChg.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMatRepairChg.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatRepairChg.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatRepairChg.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridDurableMatRepairChg.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatRepairChg.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGridDurableMatRepairChg.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMatRepairChg.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance47.BackColor = System.Drawing.SystemColors.Highlight;
            appearance47.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.ActiveRowAppearance = appearance47;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGridDurableMatRepairChg.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMatRepairChg.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGridDurableMatRepairChg.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMatRepairChg.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMatRepairChg.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMatRepairChg.Location = new System.Drawing.Point(12, 324);
            this.uGridDurableMatRepairChg.Name = "uGridDurableMatRepairChg";
            this.uGridDurableMatRepairChg.Size = new System.Drawing.Size(1016, 144);
            this.uGridDurableMatRepairChg.TabIndex = 94;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(12, 300);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(100, 20);
            this.uLabel9.TabIndex = 93;
            this.uLabel9.Text = "교체";
            // 
            // uOptionCCSFlag
            // 
            this.uOptionCCSFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionCCSFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem2.DataValue = true;
            valueListItem2.DisplayText = "의뢰함";
            valueListItem3.DataValue = false;
            valueListItem3.DisplayText = "의뢰안함";
            this.uOptionCCSFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem3});
            this.uOptionCCSFlag.Location = new System.Drawing.Point(416, 76);
            this.uOptionCCSFlag.Name = "uOptionCCSFlag";
            this.uOptionCCSFlag.Size = new System.Drawing.Size(132, 20);
            this.uOptionCCSFlag.TabIndex = 11;
            this.uOptionCCSFlag.TextIndentation = 2;
            this.uOptionCCSFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionCCSFlag.Visible = false;
            // 
            // uGridDeviceChgResultD
            // 
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDeviceChgResultD.DisplayLayout.Appearance = appearance29;
            this.uGridDeviceChgResultD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDeviceChgResultD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgResultD.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgResultD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.uGridDeviceChgResultD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgResultD.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            this.uGridDeviceChgResultD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDeviceChgResultD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDeviceChgResultD.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDeviceChgResultD.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGridDeviceChgResultD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDeviceChgResultD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgResultD.DisplayLayout.Override.CardAreaAppearance = appearance41;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDeviceChgResultD.DisplayLayout.Override.CellAppearance = appearance42;
            this.uGridDeviceChgResultD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDeviceChgResultD.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgResultD.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance44.TextHAlignAsString = "Left";
            this.uGridDeviceChgResultD.DisplayLayout.Override.HeaderAppearance = appearance44;
            this.uGridDeviceChgResultD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDeviceChgResultD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.Color.Silver;
            this.uGridDeviceChgResultD.DisplayLayout.Override.RowAppearance = appearance45;
            this.uGridDeviceChgResultD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDeviceChgResultD.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
            this.uGridDeviceChgResultD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDeviceChgResultD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDeviceChgResultD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDeviceChgResultD.Location = new System.Drawing.Point(12, 152);
            this.uGridDeviceChgResultD.Name = "uGridDeviceChgResultD";
            this.uGridDeviceChgResultD.Size = new System.Drawing.Size(1016, 144);
            this.uGridDeviceChgResultD.TabIndex = 13;
            this.uGridDeviceChgResultD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDeviceChgResultD_AfterCellUpdate);
            this.uGridDeviceChgResultD.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDeviceChgResultD_CellChange);
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 128);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(100, 20);
            this.uLabel8.TabIndex = 88;
            this.uLabel8.Text = "교체";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 100);
            this.uTextEtcDesc.MaxLength = 1000;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(792, 21);
            this.uTextEtcDesc.TabIndex = 12;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 100);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(100, 20);
            this.uLabel7.TabIndex = 86;
            this.uLabel7.Text = "특이사항";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(296, 76);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(116, 20);
            this.uLabel6.TabIndex = 83;
            this.uLabel6.Text = "CSS";
            this.uLabel6.Visible = false;
            // 
            // uTextDeviceChgUserName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeviceChgUserName.Appearance = appearance36;
            this.uTextDeviceChgUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeviceChgUserName.Location = new System.Drawing.Point(520, 28);
            this.uTextDeviceChgUserName.Name = "uTextDeviceChgUserName";
            this.uTextDeviceChgUserName.ReadOnly = true;
            this.uTextDeviceChgUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextDeviceChgUserName.TabIndex = 82;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(644, 28);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(100, 20);
            this.uLabel4.TabIndex = 64;
            this.uLabel4.Text = "교체일시";
            // 
            // uDateDeviceChgDate
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDeviceChgDate.Appearance = appearance1;
            this.uDateDeviceChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDeviceChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDeviceChgDate.Location = new System.Drawing.Point(748, 28);
            this.uDateDeviceChgDate.MaskInput = "{date}";
            this.uDateDeviceChgDate.Name = "uDateDeviceChgDate";
            this.uDateDeviceChgDate.Size = new System.Drawing.Size(100, 21);
            this.uDateDeviceChgDate.TabIndex = 7;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(296, 28);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(116, 20);
            this.uLabel5.TabIndex = 80;
            this.uLabel5.Text = "교체";
            // 
            // uComboWorkGubun
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboWorkGubun.Appearance = appearance39;
            this.uComboWorkGubun.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboWorkGubun.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboWorkGubun.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboWorkGubun.Location = new System.Drawing.Point(116, 52);
            this.uComboWorkGubun.MaxLength = 5;
            this.uComboWorkGubun.Name = "uComboWorkGubun";
            this.uComboWorkGubun.Size = new System.Drawing.Size(108, 19);
            this.uComboWorkGubun.TabIndex = 8;
            this.uComboWorkGubun.Text = "ultraComboEditor1";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(12, 52);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(100, 20);
            this.uLabel3.TabIndex = 59;
            this.uLabel3.Text = "작업조";
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(12, 76);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(100, 20);
            this.uLabel2.TabIndex = 57;
            this.uLabel2.Text = "표준";
            // 
            // uTextVersionNum
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Appearance = appearance37;
            this.uTextVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Location = new System.Drawing.Point(204, 76);
            this.uTextVersionNum.Name = "uTextVersionNum";
            this.uTextVersionNum.ReadOnly = true;
            this.uTextVersionNum.Size = new System.Drawing.Size(56, 21);
            this.uTextVersionNum.TabIndex = 56;
            // 
            // uTextStdNumber
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance48;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(116, 76);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(84, 21);
            this.uTextStdNumber.TabIndex = 56;
            // 
            // uTextDeviceChgDocCode
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeviceChgDocCode.Appearance = appearance19;
            this.uTextDeviceChgDocCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeviceChgDocCode.Location = new System.Drawing.Point(416, 52);
            this.uTextDeviceChgDocCode.Name = "uTextDeviceChgDocCode";
            this.uTextDeviceChgDocCode.ReadOnly = true;
            this.uTextDeviceChgDocCode.Size = new System.Drawing.Size(108, 21);
            this.uTextDeviceChgDocCode.TabIndex = 6;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(296, 52);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(116, 20);
            this.uLabel1.TabIndex = 55;
            this.uLabel1.Text = "품종";
            // 
            // uComboProcess
            // 
            appearance40.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcess.Appearance = appearance40;
            this.uComboProcess.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcess.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboProcess.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboProcess.Location = new System.Drawing.Point(116, 28);
            this.uComboProcess.MaxLength = 50;
            this.uComboProcess.Name = "uComboProcess";
            this.uComboProcess.Size = new System.Drawing.Size(108, 19);
            this.uComboProcess.TabIndex = 5;
            this.uComboProcess.Text = "ultraComboEditor1";
            // 
            // uLabelProcess
            // 
            this.uLabelProcess.Location = new System.Drawing.Point(12, 28);
            this.uLabelProcess.Name = "uLabelProcess";
            this.uLabelProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcess.TabIndex = 53;
            this.uLabelProcess.Text = "공정";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance38;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 38;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(268, 12);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquip.TabIndex = 39;
            this.uLabelSearchEquip.Text = "설비코드";
            // 
            // uTextSearchEquipCode
            // 
            appearance49.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance49.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance49;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(372, 12);
            this.uTextSearchEquipCode.MaxLength = 20;
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(108, 21);
            this.uTextSearchEquipCode.TabIndex = 2;
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uTextSearchEquipName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance18;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uTextSearchEquipName.Location = new System.Drawing.Point(484, 12);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(108, 21);
            this.uTextSearchEquipName.TabIndex = 55;
            // 
            // uGridDeviceChgResultH
            // 
            this.uGridDeviceChgResultH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDeviceChgResultH.DisplayLayout.Appearance = appearance51;
            this.uGridDeviceChgResultH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDeviceChgResultH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance52.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance52.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgResultH.DisplayLayout.GroupByBox.Appearance = appearance52;
            appearance53.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgResultH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance53;
            this.uGridDeviceChgResultH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance54.BackColor2 = System.Drawing.SystemColors.Control;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgResultH.DisplayLayout.GroupByBox.PromptAppearance = appearance54;
            this.uGridDeviceChgResultH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDeviceChgResultH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDeviceChgResultH.DisplayLayout.Override.ActiveCellAppearance = appearance55;
            appearance56.BackColor = System.Drawing.SystemColors.Highlight;
            appearance56.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDeviceChgResultH.DisplayLayout.Override.ActiveRowAppearance = appearance56;
            this.uGridDeviceChgResultH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDeviceChgResultH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgResultH.DisplayLayout.Override.CardAreaAppearance = appearance57;
            appearance58.BorderColor = System.Drawing.Color.Silver;
            appearance58.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDeviceChgResultH.DisplayLayout.Override.CellAppearance = appearance58;
            this.uGridDeviceChgResultH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDeviceChgResultH.DisplayLayout.Override.CellPadding = 0;
            appearance59.BackColor = System.Drawing.SystemColors.Control;
            appearance59.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance59.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance59.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgResultH.DisplayLayout.Override.GroupByRowAppearance = appearance59;
            appearance60.TextHAlignAsString = "Left";
            this.uGridDeviceChgResultH.DisplayLayout.Override.HeaderAppearance = appearance60;
            this.uGridDeviceChgResultH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDeviceChgResultH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.Color.Silver;
            this.uGridDeviceChgResultH.DisplayLayout.Override.RowAppearance = appearance61;
            this.uGridDeviceChgResultH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDeviceChgResultH.DisplayLayout.Override.TemplateAddRowAppearance = appearance62;
            this.uGridDeviceChgResultH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDeviceChgResultH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDeviceChgResultH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDeviceChgResultH.Location = new System.Drawing.Point(0, 80);
            this.uGridDeviceChgResultH.Name = "uGridDeviceChgResultH";
            this.uGridDeviceChgResultH.Size = new System.Drawing.Size(1070, 760);
            this.uGridDeviceChgResultH.TabIndex = 6;
            this.uGridDeviceChgResultH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDeviceChgResultH_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 7;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // frmEQUZ0004
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridDeviceChgResultH);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0004";
            this.Load += new System.EventHandler(this.frmEQUZ0004_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0004_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0004_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTSStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSysProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeviceChgUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatRepairChg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCCSFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgResultD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeviceChgUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDeviceChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWorkGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeviceChgDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgResultH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeviceChgDocCode;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboWorkGubun;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDeviceChgDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeviceChgUserName;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDeviceChgResultD;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionCCSFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMatRepairChg;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSTSStock;
        private Infragistics.Win.Misc.UltraLabel uLabelSTSStock;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipLevel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSysProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLocation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSuperEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSysGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSysGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSysType;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.Misc.UltraLabel uLabelSysProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelLocation;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSuperSys;
        private Infragistics.Win.Misc.UltraLabel uLabelSysName;
        private Infragistics.Win.Misc.UltraLabel uLabelSysCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeviceChgUserID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDeviceChgResultH;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNum;
    }
}