﻿namespace QRPEQU.UI
{
    partial class frmEQU0017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0017));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uTextYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.uMonthView = new Infragistics.Win.UltraWinSchedule.UltraMonthViewSingle();
            this.uCalendarInfo = new Infragistics.Win.UltraWinSchedule.UltraCalendarInfo(this.components);
            this.uCalendarLook = new Infragistics.Win.UltraWinSchedule.UltraCalendarLook(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uMonthView)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uTextUserName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance3;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(692, 12);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 6;
            // 
            // uTextUserID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.Appearance = appearance2;
            this.uTextUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance5;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton1);
            this.uTextUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextUserID.Location = new System.Drawing.Point(588, 12);
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 19);
            this.uTextUserID.TabIndex = 5;
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(484, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "ultraLabel3";
            // 
            // uComboMonth
            // 
            this.uComboMonth.Location = new System.Drawing.Point(316, 12);
            this.uComboMonth.Name = "uComboMonth";
            this.uComboMonth.Size = new System.Drawing.Size(130, 21);
            this.uComboMonth.TabIndex = 3;
            this.uComboMonth.Text = "ultraComboEditor1";
            // 
            // uLabelMonth
            // 
            this.uLabelMonth.Location = new System.Drawing.Point(212, 12);
            this.uLabelMonth.Name = "uLabelMonth";
            this.uLabelMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelMonth.TabIndex = 2;
            this.uLabelMonth.Text = "ultraLabel2";
            // 
            // uTextYear
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextYear.Appearance = appearance4;
            this.uTextYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextYear.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uTextYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextYear.Location = new System.Drawing.Point(116, 12);
            this.uTextYear.Name = "uTextYear";
            this.uTextYear.Size = new System.Drawing.Size(68, 19);
            this.uTextYear.TabIndex = 1;
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(12, 12);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 0;
            this.uLabelYear.Text = "ultraLabel1";
            // 
            // uMonthView
            // 
            this.uMonthView.Location = new System.Drawing.Point(0, 80);
            this.uMonthView.Name = "uMonthView";
            this.uMonthView.Size = new System.Drawing.Size(1070, 694);
            this.uMonthView.TabIndex = 3;
            this.uMonthView.MoreActivityIndicatorClicked += new Infragistics.Win.UltraWinSchedule.MoreActivityIndicatorClickedEventHandler(this.uMonthView_MoreActivityIndicatorClicked);
            // 
            // uCalendarInfo
            // 
            this.uCalendarInfo.DataBindingsForAppointments.BindingContextControl = this;
            this.uCalendarInfo.DataBindingsForOwners.BindingContextControl = this;
            this.uCalendarInfo.BeforeDisplayAppointmentDialog += new Infragistics.Win.UltraWinSchedule.DisplayAppointmentDialogEventHandler(this.uCalendarInfo_BeforeDisplayAppointmentDialog);
            this.uCalendarInfo.AfterSelectedNotesChange += new System.EventHandler(this.uCalendarInfo_AfterSelectedNotesChange);
            // 
            // frmEQU0017
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uMonthView);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0017";
            this.Load += new System.EventHandler(this.frmEQU0017_Load);
            this.Activated += new System.EventHandler(this.frmEQU0017_Activated);
            this.Resize += new System.EventHandler(this.frmEQU0017_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uMonthView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextYear;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
        private Infragistics.Win.UltraWinSchedule.UltraMonthViewSingle uMonthView;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarInfo uCalendarInfo;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarLook uCalendarLook;
    }
}