﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 전산SHEET관리                                         */
/* 프로그램ID   : frmEQUZ0042.cs                                        */
/* 프로그램명   : CLEANER RUBBER 교체 주기                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-04-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0042 : Form,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0042()
        {
            InitializeComponent();
        }

        private void frmEQUZ0040_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.MdiParent, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0040_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0040_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGroupBox();
            InitButton();
            InitGrid();

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화

        /// <summary>
        /// 사용자 권한
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("CLEANER RUBBER 교체 주기", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelEquipCode, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grb = new WinGroupBox();
                grb.mfSetGroupBox(this.uGroupEquipList, GroupBoxType.LIST, "설비리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grb.mfSetGroupBox(this.uGroupContents, GroupBoxType.LIST, "교체내용", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grb.mfSetGroupBox(this.uGroupHistory, GroupBoxType.LIST, "교체이력정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                this.uGroupEquipList.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupEquipList.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupContents.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupContents.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupHistory.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupHistory.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //Resource 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSaveEquip, "설비등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
                btn.mfSetButton(this.uButtonSave, "완료", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonDel, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDelContents, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region 설비리스트
                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, false,
                                      Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                                      , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                                      , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼 설정
                for (int i = 1; i < 13; i++)
                {
                    string strKey = "M" + i.ToString();
                    string strValue = i.ToString() + "월";
                    grd.mfSetGridColumn(this.uGridEquipList, 0, strKey, strValue, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }

                //해당그리드의 선택한 줄이나 셀의 속성을 비활성화한다.
                this.uGridEquipList.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridEquipList.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                #endregion

                #region Contents

                // 일반설정
                grd.mfInitGeneralGrid(this.uGridContents, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column  
                grd.mfSetGridColumn(this.uGridContents, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridContents, 0, "DocCode", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridContents, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridContents, 0, "WriteDate", "날짜", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", DateTime.Now.ToString("yyyy-MM-dd"));

                grd.mfSetGridColumn(this.uGridContents, 0, "Legend", "범례", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 5
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                grd.mfSetGridColumn(this.uGridContents, 0, "Contents", "내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 500, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                //DropDown
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtCom = clsCommonCode.mfReadCommonCode("C0067", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridContents, 0, "Legend", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);


                #endregion

                #region History
                // 일반설정
                grd.mfInitGeneralGrid(this.uGridHistory, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column  
                grd.mfSetGridColumn(this.uGridHistory, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridHistory, 0, "DocCode", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", DateTime.Now.ToString("yyyy-MM-dd"));

                grd.mfSetGridColumn(this.uGridHistory, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "WriteDate", "날짜", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "Legend", "범례", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "Contents", "내용", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 500, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumnValueList(this.uGridHistory, 0, "Legend", Infragistics.Win.ValueListDisplayStyle.DisplayText, "ComCode", "ComCodeName", dtCom);

                #endregion


                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridContents.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridContents.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridHistory.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHistory.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                grd.mfAddRowGrid(this.uGridContents, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY2), "ASSY2");
                QRPEQU.BL.EQUSHT.ASSY2 clsASSY2 = new QRPEQU.BL.EQUSHT.ASSY2();
                brwChannel.mfCredentials(clsASSY2);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                DataTable dtDataInfo = clsASSY2.mfReadCleanerRubberH("E"); // 공백일 시 리스트


                this.uGridEquipList.DataSource = dtDataInfo;
                this.uGridEquipList.DataBind();
                /////////////

                
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtDataInfo.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);

                    //설비코드뒤에 있는 날짜와 현재날짜와 비교하여 같다면 셀 색변경
                    string strDate = "("+ DateTime.Now.Date.ToString("MM-dd") + ")";

                    for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                    {
                        for (int j = 0; j < this.uGridEquipList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            if (!this.uGridEquipList.Rows[i].Cells[j].Value.ToString().Equals(string.Empty)
                                && this.uGridEquipList.Rows[i].Cells[j].Value.ToString().Contains(strDate)
                                )
                            {
                                this.uGridEquipList.Rows[i].Cells[j].Appearance.BackColor = Color.Salmon;
                                break;
                            }
                        }
                    }
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {

        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipList.Rows.Count <= 0 && this.uGridContents.Rows.Count <= 0 && this.uGridHistory.Rows.Count <= 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                        , "확인창", "출력정보확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                if(this.uGridEquipList.Rows.Count > 0 )
                    grd.mfDownLoadGridToExcel(this.uGridEquipList);
                if (this.uGridContents.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridContents);
                if (this.uGridHistory.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridHistory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        { 
        }

        public void mfDelete()
        {
        }

        public void mfPrint()
        { }

        #endregion

        #region Event

        /// <summary>
        /// 셀편집시 편집이미지 생성
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridContents_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;
            WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridContents, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }

        /// <summary>
        /// 설비리스트에서 설비 더블클릭시 교체내용등록, 교체내용 이력을 알 수 있다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquipList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Value.ToString().Equals(string.Empty))
                    return;

                this.uTextEquipCode.Clear();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string[] strTemp = new string[2];

                strTemp = e.Cell.Value.ToString().Split(' ');
                this.uTextEquipCode.Text = strTemp[0];

                SearchHistory(this.uTextEquipCode.Text,m_resSys.GetString("SYS_LANG"));

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 교체내용을 저장한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uTextEquipCode.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "검색 된 설비를 더블 클릭 해주세요.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uGridContents.Rows.Count > 0)
                    this.uGridContents.ActiveCell = this.uGridContents.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY2), "ASSY2");
                QRPEQU.BL.EQUSHT.ASSY2 clsASSY2 = new QRPEQU.BL.EQUSHT.ASSY2();
                brwChannel.mfCredentials(clsASSY2);

                DataTable dtDataInfo = clsASSY2.mfSetDataInfo_CleanerD();


                for (int i = 0; i < this.uGridContents.Rows.Count; i++)
                {
                    if (this.uGridContents.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (!this.uGridContents.Rows[i].Hidden)
                        {
                            if (this.uGridContents.Rows[i].GetCellValue("Legend").ToString().Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "확인창", "필수입력사항확인", "범례를 선택해주세요.",
                                                                Infragistics.Win.HAlign.Right);
                                this.uGridContents.ActiveCell = this.uGridContents.Rows[i].Cells["Legend"];
                                this.uGridContents.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            DataRow drRow = dtDataInfo.NewRow();

                            drRow["DocCode"] = this.uGridContents.Rows[i].GetCellValue("DocCode");
                            drRow["EquipCode"] = this.uTextEquipCode.Text;
                            drRow["WriteDate"] = this.uGridContents.Rows[i].GetCellValue("WriteDate");
                            drRow["Legend"] = this.uGridContents.Rows[i].GetCellValue("Legend");
                            drRow["Contents"] = this.uGridContents.Rows[i].GetCellValue("Contents");

                            dtDataInfo.Rows.Add(drRow);
                        }
                    }
                }


                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;

                }


                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = clsASSY2.mfSaveCleanerRubberD(dtDataInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        ClearGrid();
                        SearchHistory(this.uTextEquipCode.Text,m_resSys.GetString("SYS_LANG"));
                    }
                    else
                    {
                        string strMsg = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = "입력한 정보를 저장하지 못했습니다.";
                        else
                            strMsg = ErrRtn.ErrMessage;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", strMsg,
                                                     Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 교체내용 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDelContents_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGridHistory.Rows.Count > 0)
                    this.uGridHistory.ActiveCell = this.uGridHistory.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "삭제 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY2), "ASSY2");
                QRPEQU.BL.EQUSHT.ASSY2 clsASSY2 = new QRPEQU.BL.EQUSHT.ASSY2();
                brwChannel.mfCredentials(clsASSY2);

                DataTable dtDataInfo = clsASSY2.mfSetDelDataInfo_Cleaner();

                for (int i = 0; i < this.uGridHistory.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridHistory.Rows[i].GetCellValue("Check"))  //선택되어 있고 문서번호가 존재하는 정보
                        && !this.uGridHistory.Rows[i].GetCellValue("DocCode").ToString().Equals(string.Empty))
                    {

                        DataRow drRow = dtDataInfo.NewRow();

                        drRow["DocCode"] = this.uGridHistory.Rows[i].GetCellValue("DocCode");
                        drRow["EquipCode"] = this.uGridHistory.Rows[i].GetCellValue("EquipCode");

                        dtDataInfo.Rows.Add(drRow);
                    }
                }


                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "삭제 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;

                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = clsASSY2.mfDeleteCleanerRubberD(dtDataInfo);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);

                        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        SearchHistory(this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    else
                    {
                        string strMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = "선택한 정보를 삭제하지 못했습니다.";
                        else
                            strMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", strMes,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비등록팝업창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSaveEquip_Click(object sender, EventArgs e)
        {
            try
            {
                frmEQUZ0042_P1 frmEquip = new frmEQUZ0042_P1();
                frmEquip.ShowDialog();

                mfSearch();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        /// <summary>
        /// 행삭제 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridContents.Rows.Count; i++)
                {
                    if(!this.uGridContents.Rows[i].Hidden
                        && Convert.ToBoolean(this.uGridContents.Rows[i].GetCellValue("Check")))
                    {
                        this.uGridContents.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Method

        /// <summary>
        /// 이력조회(상세조회)
        /// </summary>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        private void SearchHistory(string strEquipCode,string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY2), "ASSY2");
                QRPEQU.BL.EQUSHT.ASSY2 clsASSY2 = new QRPEQU.BL.EQUSHT.ASSY2();
                brwChannel.mfCredentials(clsASSY2);

                DataTable dtDataInfo = clsASSY2.mfReadCleanerRubberD(strEquipCode, strLang);

                this.uGridHistory.DataSource = dtDataInfo;
                this.uGridHistory.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        /// <summary>
        /// 그리드 줄삭제
        /// </summary>
        private void ClearGrid()
        {
            try
            {
                //this.uTextEquipCode.Clear();

                if (this.uGridContents.Rows.Count > 0)
                {
                    this.uGridContents.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridContents.Rows.All);
                    this.uGridContents.DeleteSelectedRows(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion



    }
}
