﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQUZ0022_P3.cs                                     */
/* 프로그램명   : 품종교체 팝업창                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-12-23                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0022_P3_S : Form
    {
        #region 전역변수

        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        DataTable dtRequest;
        DataTable dtAccept;
        DataTable dtMESInfo;

        private string strPlantCode;
        private string strEquipCode;
        private string strRepairReqCode;
        private string strSaveChk;

        public string SaveChk
        {
            get { return strSaveChk; }
            set { strSaveChk = value; }
        }
        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }
        public string EquipCode
        {
            get { return strEquipCode; }
            set { strEquipCode = value; }
        }
        public string RepairReqCode
        {
            get { return strRepairReqCode; }
            set { strRepairReqCode = value; }
        }

        #endregion

        public frmEQUZ0022_P3_S()
        {
            InitializeComponent();
        }

        public frmEQUZ0022_P3_S(DataTable _dtRequest,DataTable _dtAccept,DataTable _dtMESInfo)
        {
            InitializeComponent();
            dtRequest = _dtRequest;
            dtAccept = _dtAccept;
            dtMESInfo = _dtMESInfo;
        }

        private void frmEQUZ0022_P3_Load(object sender, EventArgs e)
        {
            if (strRepairReqCode == null)
            {
                strRepairReqCode = "";
            }

            InitTitle();
            InitLabel();
            InitGroupBox();
            InitButton();
            InitGrid();
            SearchDisplay();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("품종교체", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelUser, "작업자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStopType, "품종교체코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPackageTypeB, "PKGType", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPackageB, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProductionB, "생산코드", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelPackageTypeA, "PKGType", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPackageA, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProductionA, "생산코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRecipe, "Recipe", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSeq, "차수구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEMC, "EMC자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSerial, "금형SERIAL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelComment, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRows, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonOK, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
                btn.mfSetButton(this.uButtonCancel, "취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonChgCancel, "품종교체의뢰취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                WinGroupBox wgrb = new WinGroupBox();

                wgrb.mfSetGroupBox(this.uGroupEquip, GroupBoxType.INFO, "품종교체설비", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wgrb.mfSetGroupBox(this.uGroupBeforeProduct, GroupBoxType.INFO, "이전진행제품", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                   , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                   , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wgrb.mfSetGroupBox(this.uGroupAfterProduct, GroupBoxType.INFO, "변경진행진행제품", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                   , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                   , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wgrb.mfSetGroupBox(this.uGroupRecipe, GroupBoxType.INFO, "조회결과", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                   , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                   , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wgrb.mfSetGroupBox(this.uGroupDurable, GroupBoxType.INFO, "치공구교체(재고)", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                   , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                   , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wgrb.mfSetGroupBox(this.uGroupDurableLot, GroupBoxType.INFO, "치공구교체(신규)", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                   , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                   , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupEquip.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupEquip.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBeforeProduct.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBeforeProduct.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupAfterProduct.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupAfterProduct.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupRecipe.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupRecipe.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupDurable.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupDurable.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupDurableLot.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupDurableLot.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                #region Recipe
                //기본설정
                grd.mfInitGeneralGrid(this.uGridRecipe, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridRecipe, 0, "DESCRIPTION", "설명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRecipe, 0, "MODEL", "설비모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRecipe, 0, "PACKAGETYPE", "PKGType", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRecipe, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRecipe, 0, "RECIPENAME", "RecipeName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 220, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRecipe, 0, "BONDINGSPEC", "BondingSpec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                uGridRecipe.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridRecipe.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 수리출고 구성품
                // 수리출고구성품 Grid
                // 일반설정
                grd.mfInitGeneralGrid(this.uGridDurableMat, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                grd.mfSetGridColumn(this.uGridDurableMat, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                grd.mfSetGridColumn(this.uGridDurableMat, 0, "RepairGICode", "수리출고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "RepairGIGubunCode", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "INN");


                grd.mfSetGridColumn(this.uGridDurableMat, 0, "CurDurableMatCode", "기존치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "CurDurableMatName", "기존치공구명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 40, false, false, 5
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "CurLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGridDurableMat, 0, "ChgDurableInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "ChgDurableMatCode", "교체치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "ChgDurableMatName", "교체치공구명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "ChgLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "Qty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "ChgQty", "교체수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "CancelFlag", "수리취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //this.uGridDurableMat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                this.uGridDurableMat.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.None;

                #region DropDown

                
                //--수리출고구분--//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtRepairGICode = clsCommonCode.mfReadCommonCode("C0016", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridDurableMat, 0, "RepairGIGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtRepairGICode);

                //-----금형치공구창고정보-----//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo("2110", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridDurableMat, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGridDurableMat, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                #endregion

                //빈줄추가
                grd.mfAddRowGrid(this.uGridDurableMat, 0);


                uGridDurableMat.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDurableMat.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 치공구교체신규

                //기본설정
                grd.mfInitGeneralGrid(this.uGridDurableLot, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                grd.mfSetGridColumn(this.uGridDurableLot, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDurableLot, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 40
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableLot, 0, "Qty", "Qty", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "1");

                grd.mfSetGridColumn(this.uGridDurableLot, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                uGridDurableLot.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDurableLot.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                grd.mfAddRowGrid(this.uGridDurableLot, 0);

                this.uGridDurableLot.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.None;
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트 & 그리드 정보 뿌려줌
        /// </summary>
        private void SearchDisplay()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //설비수리정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsRepairReq);

                //설비수리등록 정보조회(MES수신) 
                DataTable dtEquip = clsRepairReq.mfReadEquipRepair_MESDB_Detail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //정보가 있을 경우 각 컨트롤에 뿌려준다.
                if (dtEquip.Rows.Count > 0)
                {
                    this.uTextPackageTypeB.Text = dtEquip.Rows[0]["PackageGroupCodeB"].ToString();
                    this.uTextPackageB.Text = dtEquip.Rows[0]["PackageB"].ToString();
                    this.uTextPackageBB.Text = dtEquip.Rows[0]["CUSTOMERPRODUCTSPECB"].ToString();
                    this.uTextProductionB.Text = dtEquip.Rows[0]["PRODUCTSPECNAME"].ToString();


                    this.uTextPackageTypeA.Text = dtEquip.Rows[0]["PackageGroupCode"].ToString();
                    this.uTextPackageA.Text = dtEquip.Rows[0]["PackageA"].ToString();
                    this.uTextPackageA.Tag = dtEquip.Rows[0]["ModelName"].ToString();
                    this.uTextPackageAA.Text = dtEquip.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                    this.uTextProductionA.Text = dtEquip.Rows[0]["REQUESTPRODUCTSPECNAME"].ToString();

                    
                    //해당 설비의 BOM콤보 추가
                    SearchDurableBOM();

                    //Recipe정보 조회
                    DataTable dtRecipe = clsRepairReq.mfReadRepairReq_MDMRecipePOP(strPlantCode, strEquipCode, dtEquip.Rows[0]["REQUESTPRODUCTSPECNAME"].ToString());

                    if (dtRecipe.Rows.Count == 0)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000112", "M000113", Infragistics.Win.HAlign.Right);
                    }
                    if (dtRecipe.Rows.Count == 1)
                        this.uTextRecipe.Text = dtRecipe.Rows[0]["RECIPENAME"].ToString();

                    this.uGridRecipe.DataSource = dtRecipe;
                    this.uGridRecipe.DataBind();


                }
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Event

        //저장버튼 클릭
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region 필수 입력확인 정보확인
                //-------------------------------필수 입력확인 , 정보 확인---------------------------------//
                //if (this.uGridRecipe.Rows.Count == 0)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "입력사항 확인", "레시피정보가 없으므로 저장이 불가합니다.", Infragistics.Win.HAlign.Right);

                //    return;
                //}

                //작업자를 입력해주세요.
                if (this.uTextUser.Text.Equals(string.Empty) || this.uTextUserName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M001003", Infragistics.Win.HAlign.Right);

                    this.uTextUser.Focus();
                    return;

                }
                ////if (this.uTextStopTypeCode.Text.Equals(string.Empty) || this.uTextStopTypeName.Text.Equals(string.Empty))
                ////{
                ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "확인창", "필수입력사항 확인", "품종교체코드를 입력해주세요.", Infragistics.Win.HAlign.Right);

                ////    this.uTextStopTypeCode.Focus();
                ////    return;
                ////}
                //if (this.uTextRecipe.Text.Equals(string.Empty))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "필수입력사항 확인", "Recipe를 선택해주세요.", Infragistics.Win.HAlign.Right);

                //    return;
                //}

                ////if (this.uTextSeq.Value.ToString().Equals(string.Empty))
                ////{
                ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "확인창", "필수입력사항 확인", "차수구분을 입력해주세요.", Infragistics.Win.HAlign.Right);
                ////    this.uTextSeq.Focus();

                ////    return;
                ////}
                // 해당 설비가 CCS의뢰 상태인 경우 메세지 출력 Return
                if (CheckCCS())
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);

                    return;
                }
                #endregion

                #region 컬럼설정

                #region 설비수리품종교체

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairDeviceChg), "RepairDeviceChg");
                QRPEQU.BL.EQUREP.RepairDeviceChg clsRepairDeviceChg = new QRPEQU.BL.EQUREP.RepairDeviceChg();
                brwChannel.mfCredentials(clsRepairDeviceChg);

                DataTable dtRepairDeviceChg = clsRepairDeviceChg.mfDataSet();

                #endregion

                #region 치공구

                //치공구교체정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                //---헤더값을 넣을 데이터 테이블---//
                DataTable dtDurableRepairH = clsDurableMatRepairH.mfDataSetInfo();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsDurableMatRepairD);

                //--상세정보를 넣을 데이터 테이블--//
                DataTable dtDurableRepairD = clsDurableMatRepairD.mfsetDataInfo();
                //--행삭제된 정보를 넣을 데이터테이블--//
                //DataTable dtDurableRepairDel = clsDurableMatRepairD.mfsetDataInfo();
                //dtDurableRepairD.Columns.Add("UnitCode", typeof(string));
                dtDurableRepairD.Columns.Add("UnitName", typeof(string));
                dtDurableRepairD.Columns.Add("Qty", typeof(string));
                dtDurableRepairD.Columns.Add("ChgDurableMatName", typeof(string));
                dtDurableRepairD.Columns.Add("ItemGubunCode", typeof(string));


                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);
                #endregion

                #region 신규LotNo

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairLotHistory), "RepairLotHistory");
                QRPEQU.BL.EQUREP.RepairLotHistory clsRepairLotHistory = new QRPEQU.BL.EQUREP.RepairLotHistory();
                brwChannel.mfCredentials(clsRepairLotHistory);

                DataTable dtDurableLot = clsRepairLotHistory.mfSetDataInfo();

                #endregion

                #endregion

                //레시피정보 삽입
                if (dtMESInfo.Rows.Count > 0)
                    dtMESInfo.Rows[0]["MACHINERECIPE"] = this.uTextRecipe.Text;
                    dtMESInfo.Rows[0]["COMMENT"] = this.uTextComment.Text;

                DataTable dtMESDurableMat = new DataTable();
                dtMESDurableMat.Columns.Add("TOOLID", typeof(string));
                dtMESDurableMat.Columns.Add("TOOLSPECID", typeof(string));

                string strLang = m_resSys.GetString("SYS_LANG");

                #region 설비수리 품종교체정보저장
                // 설비수리 품종교체정보저장
                DataRow drRepair = dtRepairDeviceChg.NewRow();

                drRepair["PlantCode"] = strPlantCode;
                drRepair["RepairReqCode"] = strRepairReqCode;
                drRepair["Seq"] = "0";
                drRepair["WorkerID"] = this.uTextUser.Text;
                drRepair["WorkDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                drRepair["WorkTime"] = DateTime.Now.ToString("HH:mm:ss");
                drRepair["DownCode"] = this.uTextStopTypeCode.Text;
                drRepair["DownCodeName"] = this.uTextStopTypeName.Text;
                drRepair["PreProductCode"] = this.uTextProductionB.Text;
                drRepair["ProductCode"] = this.uTextProductionA.Text;
                drRepair["MACHINERECIPENAME"] = this.uTextRecipe.Text;
                drRepair["VersionNum"] = this.uTextSeq.Value;
                drRepair["EMCEquipCode"] = this.uTextEMC.Text;
                drRepair["MoldSerial"] = this.uTextSerial.Text;
                drRepair["Comment"] = this.uTextComment.Text;

                dtRepairDeviceChg.Rows.Add(drRepair);
                #endregion

                #region 치공구 교체 정보 저장
                if (this.uGridDurableMat.Rows.Count > 0)
                {

                    #region 상세그리드 저장

                    if (this.uGridDurableMat.Rows.Count > 0)
                    {
                        this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                        {
                            //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.

                            ////조회된 정보에서 수리취소가 false인데 편집이미지가 있으면 없앤다(수리출고구분 수정가능일 시 다른방법을..)
                            //if (this.uGridDurableMat.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                            //    && Convert.ToBoolean(this.uGridDurableMat.Rows[i].Cells["CancelFlag"].Value) == false && this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image != null)
                            //{
                            //    this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image = null;
                            //}

                            //if (this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image != null)
                            //{
                                if (this.uGridDurableMat.Rows[i].Hidden == false)
                                {
                                    if (this.uGridDurableMat.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() != "")
                                    {
                                        #region 상세 필수입력사항 확인
                                        string strRowNum = (i + 1).ToString();

                                        if (!this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) 
                                            && this.uGridDurableMat.Rows[i].Cells["InputQty"].Value.ToString().Equals("0"))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000519",strLang), Infragistics.Win.HAlign.Right);

                                            //PerFormAction
                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["InputQty"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        if (!this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty)
                                            && this.uGridDurableMat.Rows[i].Cells["InputQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridDurableMat.Rows[i].Cells["InputQty"].Tag != null 
                                            && Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["InputQty"].Value) > Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["InputQty"].Tag)
                                            )
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000539",strLang), Infragistics.Win.HAlign.Right);

                                            //PerFormAction
                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["InputQty"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000730",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000552",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;

                                        }
                                        if (this.uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000452",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }
                                        if (this.uGridDurableMat.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridDurableMat.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000453",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["ChgQty"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridDurableMat.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["Qty"].Value) < Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000882",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000454",strLang), Infragistics.Win.HAlign.Right);

                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["ChgQty"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        #region 구성품 수량체크

                                        

                                        //구성품 교체혹은 투입시
                                        //if (!Convert.ToBoolean(this.uGridDurableMat.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                        //{

                                        //교체 일시 BOM정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        if (!this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            
                                            DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                        this.uGridDurableMat.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));



                                            //BOM 정보와 비교하여 정보가 없거나 수량이 부족할 경우 콤보를 다시 뿌려줌
                                            if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                                    , strRowNum + msg.GetMessge_Text("M000497",strLang), Infragistics.Win.HAlign.Right);

                                                #region DropDown

                                                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                                                //--해당설비의 BOM 이없을 경우 리턴 --
                                                if (dtDurable.Rows.Count == 0)
                                                {
                                                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M000715", "M001251", Infragistics.Win.HAlign.Right);

                                                    return;
                                                }
                                                //콤보 그리드 리스트 클러어
                                                this.uGridDurableMat.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                                                //콤보그리드 컬럼설정
                                                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                                                string strText = "";

                                                if (strLang.Equals("KOR"))
                                                    strText = "구성품코드,구성품명,LotNo,수량,단위";
                                                else if (strLang.Equals("CHN"))
                                                    strText = "构成品条码,构成品名,LotNo,数量,单位";
                                                else if (strLang.Equals("ENG"))
                                                    strText = "DurableMatCode,DurableMatName,LotNo,Qty,UnitName";

                                                //--그리드에 추가 --
                                                WinGrid wGrid = new WinGrid();
                                                wGrid.mfSetGridColumnValueGridList(this.uGridDurableMat, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "DurableMatCode", "DurableMatName", dtDurable);
                                                #endregion

                                                this.uGridDurableMat.Rows[i].Cells["InputQty"].Value = 0;
                                                this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;

                                                this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["CurDurableMatName"];
                                                this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }

                                        }
                                        //Stock의 재고정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                this.uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                this.uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //재고정보와 비교하여 정보가 없거나 수량이 부족 할 경우 콤보를 다시뿌려줌
                                        if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                ,msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000310",strLang)
                                                                , strRowNum + msg.GetMessge_Text("M000487",strLang), Infragistics.Win.HAlign.Right);

                                            ChgDurbleMat(this.uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                            this.uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                            this.uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                            this.uGridDurableMat.Rows[i].Cells["Qty"].Value = 0;
                                            this.uGridDurableMat.Rows[i].Cells["ChgQty"].Value = 0;
                                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }

                                        //}
                                        //// 수리취소 할 경우
                                        //else
                                        //{
                                        //    DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                        //                                                                this.uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                        //                                                            this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                        //                                                            this.uGridDurableMat.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    //BOM정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "확인창", "수리취소 정보확인", strRowNum + "번째 열의 기존구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //    //재고정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["InputQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "확인창", "수리취소 정보확인", strRowNum + "번째 열의 교체구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //}

                                        #endregion

                                        #endregion

                                        DataRow drRepairD = dtDurableRepairD.NewRow();

                                        //if (uGridDurableMat.Rows[i].Cells["PlantCode"].Value.ToString() != string.Empty)
                                        //{
                                        //    drRepairD["PlantCode"] = uGridDurableMat.Rows[i].Cells["PlantCode"].Value;
                                        //    drRepairD["RepairGICode"] = uGridDurableMat.Rows[i].Cells["RepairGICode"].Value;
                                        //    drRepairD["Seq"] = uGridDurableMat.Rows[i].Cells["Seq"].Value;
                                        //    drRepairD["CancelFlag"] = Convert.ToBoolean(uGridDurableMat.Rows[i].Cells["CancelFlag"].Value) == false ? "F" : "T";
                                        //}
                                        //else
                                        //{

                                        //}

                                        drRepairD["PlantCode"] = strPlantCode;
                                        drRepairD["RepairGICode"] = "";
                                        drRepairD["Seq"] = 0;
                                        drRepairD["CancelFlag"] = "F";
                                        drRepairD["RepairGIGubunCode"] = uGridDurableMat.Rows[i].Cells["RepairGIGubunCode"].Value;
                                        drRepairD["CurDurableMatCode"] = uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value;
                                        drRepairD["CurLotNo"] = uGridDurableMat.Rows[i].Cells["CurLotNo"].Value;
                                        drRepairD["CurQty"] = uGridDurableMat.Rows[i].Cells["InputQty"].Value;

                                        if (!uGridDurableMat.Rows[i].GetCellValue("ChgDurableInventoryCode").ToString().Equals(string.Empty))
                                        {
                                            drRepairD["ChgDurableInventoryCode"] = uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"].Value;
                                        }
                                        else
                                        {
                                            drRepairD["ChgDurableInventoryCode"] = uGridDurableMat.Rows[i].Cells["ChgDurableInventoryName"].Value;
                                        }

                                        drRepairD["ChgDurableMatCode"] = uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value;
                                        drRepairD["ChgLotNo"] = uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value;
                                        drRepairD["UnitCode"] = uGridDurableMat.Rows[i].Cells["UnitCode"].Value;
                                        drRepairD["ChgQty"] = uGridDurableMat.Rows[i].Cells["ChgQty"].Value;
                                        drRepairD["EtcDesc"] = uGridDurableMat.Rows[i].Cells["EtcDesc"].Value;

                                        dtDurableRepairD.Rows.Add(drRepairD);

                                        //MES로 전송될 치공구 교체정보
                                        drRepairD = dtMESDurableMat.NewRow();
                                        drRepairD["TOOLSPECID"] = uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value;
                                        drRepairD["TOOLID"] = uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value;
                                        dtMESDurableMat.Rows.Add(drRepairD);

                                    }

                                }
                            //}
                        }
                    }
                    #endregion

                    if (dtDurableRepairD.Rows.Count > 0)
                    {
                        #region 치공구교체헤더저장

                        DataRow drRepairH = dtDurableRepairH.NewRow();

                        drRepairH["PlantCode"] = strPlantCode;
                        drRepairH["RepairGICode"] = "";
                        drRepairH["GITypeCode"] = "RE";
                        drRepairH["EquipCode"] = strEquipCode;

                        if (RepairReqCode != null)
                            drRepairH["RepairReqCode"] = RepairReqCode;
                        else
                            drRepairH["RepairReqCode"] = "";

                        drRepairH["RepairGIReqDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        drRepairH["RepairGIReqID"] = this.uTextUser.Text;
                        drRepairH["EtcDesc"] = this.uTextComment.Text;

                        dtDurableRepairH.Rows.Add(drRepairH);

                        #endregion
                    }

                   
                }
                #endregion

                #region 신규Lot정보저장

                if (this.uGridDurableLot.Rows.Count > 0)
                {
                    DataRow drLot;
                    string strModelName = this.uTextPackageA.Tag == null ? "" : this.uTextPackageA.Tag.ToString();
                    string strUserID = this.uTextUser.Text;
                    //셀의 위치를 맨처음맨앞으로 이동시킨다.
                    this.uGridDurableLot.ActiveCell = this.uGridDurableLot.Rows[0].Cells[0];

                    //해당줄이 숨김상태가아니고 필수입력사항을 다입력하였을 경우 신규LotNo정보를 저장한다.
                    for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                    {
                        if (!this.uGridDurableLot.Rows[i].Hidden)
                        {
                            if (this.uGridDurableLot.Rows[i].Cells["LotNo"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001228", "M000075", Infragistics.Win.HAlign.Right);

                                this.uGridDurableLot.ActiveCell = this.uGridDurableLot.Rows[i].Cells["LotNo"];
                                this.uGridDurableLot.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            drLot = dtDurableLot.NewRow();

                            drLot["LotNo"] = this.uGridDurableLot.Rows[i].Cells["LotNo"].Value;
                            drLot["EquipCode"] = strEquipCode;
                            drLot["Package"] = this.uTextPackageA.Text;
                            drLot["ModelName"] = strModelName;
                            drLot["EtcDesc"] = this.uGridDurableLot.Rows[i].Cells["EtcDesc"].Value;
                            drLot["Qty"] = this.uGridDurableLot.Rows[i].Cells["Qty"].Value;
                            drLot["WriteID"] = strUserID;

                            dtDurableLot.Rows.Add(drLot);
                        }
                    }
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                //--------POPUP창 열기--------//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.Cursor = Cursors.WaitCursor;

                //처리 로직//
                TransErrRtn ErrRtn = new TransErrRtn();
                string strErrRtn = clsRepairDeviceChg.mfSaveRepairDeviceChg(dtRepairDeviceChg, dtDurableRepairH, dtDurableRepairD,dtDurableLot,dtRequest,dtAccept,dtMESInfo,dtMESDurableMat, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"),this.Name);

                ///---Decoding---//
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //--커서변경--//
                this.Cursor = Cursors.Default;

                //-------POPUP창 닫기-------//
                m_ProgressPopup.mfCloseProgressPopup(this);

                //----처리결과에 따른 메세지박스---//
                System.Windows.Forms.DialogResult result;
                string strMessage = "";
                if (ErrRtn.ErrNum == 0)
                {
                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMessage = ErrRtn.InterfaceResultMessage;
                    }
                    else
                        strMessage = msg.GetMessge_Text("M000930",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                 , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                 , msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMessage
                                                 , Infragistics.Win.HAlign.Right);

                    if (ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        strSaveChk = "OK";
                        this.Close();

                    }
                }
                else
                {
                    if(ErrRtn.ErrMessage.Equals(string.Empty))
                        strMessage = msg.GetMessge_Text("M000953",strLang);
                    else
                        strMessage = ErrRtn.ErrMessage;
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                  ,Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                  ,msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMessage
                                                  ,Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //취소버튼
        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //행삭제 버튼
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridDurableMat.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridDurableMat.Rows[i].Cells["Check"].Value))
                            this.uGridDurableMat.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //품종교체 취소버튼
        private void uButtonChgCancel_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                //system reourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                // 해당 설비가 CCS의뢰 상태인 경우 메세지 출력 Return
                if (CheckCCS())
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000723", "M001519", Infragistics.Win.HAlign.Right);

                    return;
                }

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtCancel = new DataTable();

                dtCancel.Columns.Add("RepairReqCode");
                dtCancel.Columns.Add("PlantCode");
                dtCancel.Columns.Add("RepairReqID");
                dtCancel.Columns.Add("AutoAccept");

                DataRow drCancel = dtCancel.NewRow();

                drCancel["RepairReqCode"] = strRepairReqCode;
                drCancel["PlantCode"] = strPlantCode;
                drCancel["RepairReqID"] = m_resSys.GetString("SYS_USERID");
                if(dtRequest.Rows.Count > 0)
                    drCancel["AutoAccept"] = dtRequest.Rows[0]["AutoAccept"];
                else
                    drCancel["AutoAccept"] = "N";

                dtCancel.Rows.Add(drCancel);

                if(dtMESInfo.Rows.Count == 0)
                {
                    drCancel = dtMESInfo.NewRow();
                    drCancel["EQPID"] = strEquipCode;
                    drCancel["EQPSTATE"] = "MDC";
                    drCancel["PRODUCTSPECID"] = this.uTextProductionA.Text;
                    dtMESInfo.Rows.Add(drCancel);
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001211", "M001210",
                                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");
                this.Cursor = Cursors.WaitCursor;

                TransErrRtn ErrRtn = new TransErrRtn();
                string strErrRtn = clsRepairReq.mfDeleteEquipRepair_Receipt(dtCancel, dtMESInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), this.Name);

                //Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //--커서변경--//
                this.Cursor = Cursors.Default;

                //-------POPUP창 닫기-------//
                m_ProgressPopup.mfCloseProgressPopup(this);

                //----처리결과에 따른 메세지박스---//
                System.Windows.Forms.DialogResult result;
                string strLang = m_resSys.GetString("SYS_LANG");

                if (ErrRtn.ErrNum == 0)
                {
                    string strMessage = "";

                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMessage = msg.GetMessge_Text("M000958",strLang);
                        else
                            strMessage = ErrRtn.InterfaceResultMessage;
                    }
                    else
                        strMessage = msg.GetMessge_Text("M000933",strLang);

                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001177",strLang), strMessage,
                                                Infragistics.Win.HAlign.Right);

                    if (ErrRtn.InterfaceResultCode.Equals("0"))
                        this.Close();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001177", "M000958",
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Text

        //품종교체코드 EditorButtonClick 시 팝업창이나옴
        private void uTextStopTypeCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmEQUZ0022_P2 frmReapir = new frmEQUZ0022_P2();
                frmReapir.PlantCode = strPlantCode;
                frmReapir.EquipCode = strEquipCode;
                frmReapir.SuperCode = "D";
                frmReapir.ShowDialog();


                this.uTextStopTypeCode.Text = frmReapir.RepairCode;
                this.uTextStopTypeName.Text = frmReapir.RepairName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //레시피그리드 리스트에서 선택된 레시피가 텍스트에 등록이 된다.
        private void uGridRecipe_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {

                this.uTextRecipe.Text = e.Row.Cells["RECIPENAME"].Value.ToString();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //사용자ID변경시 사용자명 클리어
        private void uTextUser_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextUserName.Text.Equals(string.Empty))
                    this.uTextUserName.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        //사용자팝업창을 띄운다.
        private void uTextUser_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //Syetem ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500,500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001230", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                    return;
                }

                //유저정보 팝업창을 호출한다.
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextUser.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        //사용자정보직접입력
        private void uTextUser_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyData.Equals(Keys.Back) || e.KeyData.Equals(Keys.Delete)) && !this.uTextUserName.Text.Equals(string.Empty))
                    this.uTextUserName.Clear();

                //공백이아니고 키데이터가 엔터 일 시 
                if (e.KeyData.Equals(Keys.Enter) && !this.uTextUser.Text.Equals(string.Empty))
                {
                    //Syetem ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001230", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                        return;
                    }


                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, this.uTextUser.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M001116", "M000883", Infragistics.Win.HAlign.Right);
                        if (!this.uTextUserName.Text.Equals(string.Empty))
                            this.uTextUserName.Clear();
                    }
                    

                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        #endregion

        #endregion

        #region 치공구 그리드

        //셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        private void uGridDurableMat_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableMat, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }

                //--체크박스가 수정불가 일시 false로 되돌린다 
                if (strColumn == "Check")
                {
                    if (e.Cell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    {
                        e.Cell.Value = false;
                        return;
                    }
                }
                ////-- 수리취소 체크 박스를 체크 해제하면 편집이미지 X --//
                //if (strColumn == "CancelFlag")
                //{
                //    if (e.Cell.Value.ToString() == "False")
                //    {
                //        e.Cell.Row.RowSelectorAppearance.Image = null;
                //        return;
                //    }
                //}

                #region 수리출고 구분 셀업데이트시 창고 교체량 교체구성품코드 셀의 헤더 색이 필수로 변한다
                if (strColumn == "RepairGIGubunCode")
                {
                    if (e.Cell.Value.ToString() != "")
                    {
                        //-- 헤더 색변화 --//
                        this.uGridDurableMat.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridDurableMat.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridDurableMat.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Red;
                    }
                    else
                    {
                        this.uGridDurableMat.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridDurableMat.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridDurableMat.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Black;
                    }
                }
                #endregion

                #region 수량 비교
                //수량을 입력한 후 해당설비구성품의 수량과 비교함 (입력한 수량보다 설비구성품의수량이 크면 안됨)
                if (strColumn.Equals("InputQty") && !e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Value.ToString().Equals("0"))
                {

                    //현재 입력한교체수량
                    int intChgQty = 0;
                    int intQty = 0;
                    for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                    {
                        if (this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image != null
                            && this.uGridDurableMat.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            && this.uGridDurableMat.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                        {
                            if (
                                e.Cell.Row.Cells["CurDurableMatCode"].Value.Equals(this.uGridDurableMat.Rows[i].Cells["CurDurableMatCode"].Value))
                            {

                                if (intQty.Equals(0))
                                {
                                    intQty = Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag);
                                }

                                intChgQty = intChgQty + Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["InputQty"].Value);

                            }
                        }
                    }

                    if (intChgQty > intQty)
                    {

                        string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["CurLotNo"].Tag;
                        string strLang = m_resSys.GetString("SYS_LANG");


                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000617",strLang)
                                               , msg.GetMessge_Text("M000659",strLang) + strQty + msg.GetMessge_Text("M000010",strLang), Infragistics.Win.HAlign.Right);

                        e.Cell.Value = 0;

                        return;
                    }

                    //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                    if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000730", "M000903", Infragistics.Win.HAlign.Right);
                        //수량되돌림
                        e.Cell.Value = e.Cell.Tag;
                        //PerFormAction
                        this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[e.Cell.Row.Index].Cells["InputQty"];
                        this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }

                }
                #endregion

                #region 교체수량비교

                if (strColumn.Equals("ChgQty"))
                {
                    if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if ((!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) 
                            && (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag)))
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000730", "M000890", Infragistics.Win.HAlign.Right);

                            //교체하는 금형치공구 수량적용
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag))
                            {
                                e.Cell.Value = 0;//e.Cell.Row.Cells["InputQty"].Tag;
                            }
                            else
                            {
                                e.Cell.Value = 0;// e.Cell.Row.Cells["Qty"].Value;
                            }

                            //PerFormAction
                            this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[e.Cell.Row.Index].Cells["ChgQty"];
                            this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        #region 중복구성품재고수량체크

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                        for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                        {
                            if (this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridDurableMat.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridDurableMat.Rows[i].Hidden.Equals(false)
                                && this.uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(string.Empty)
                                )
                            {
                                if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(this.uGridDurableMat.Rows[i].Cells["ChgDurableMatCode"].Value) &&
                                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(this.uGridDurableMat.Rows[i].Cells["ChgDurableInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridDurableMat.Rows[i].Cells["ChgQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value.ToString();
                            string strLang = m_resSys.GetString("SYS_LANG");

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000302",strLang)
                                                   , msg.GetMessge_Text("M000891",strLang) + strQty + msg.GetMessge_Text("M000010",strLang)
                                                   , Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }


                        #endregion
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //수리출고구성품그리드에서 창고 콤보를 선택하기 전 수리출고구분 체크
        private void uGridDurableMat_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //---창고 컬럼일때 발생--//
                if (e.Cell.Column.Key == "ChgDurableInventoryCode")
                {
                    //--- 수리출고구분이 "" 일경우 메세지 박스 ---//
                    if (e.Cell.Row.Cells["RepairGIGubunCode"].Value.ToString() == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000882", "M000743", Infragistics.Win.HAlign.Right);

                        this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[e.Cell.Row.Index].Cells["RepairGIGubunCode"];
                        this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                }

                //////교체구성품명 콤보그리드
                ////if (e.Cell.Column.Key.Equals("ChgDurableMatName"))
                ////{
                ////    //구성품코드가 공백일경우
                ////    if (e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                ////    {
                ////        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                    , "확인창", "입력사항 확인", "구성품을 선택해주세요.", Infragistics.Win.HAlign.Right);

                ////        this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"];
                ////        this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                ////        return;
                ////    }
                ////}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //-- 그리드 컬럼중 리스트 컬럼의 아이템을 선택하였을 때 발생함 --//
        private void uGridDurableMat_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPBrowser brwChannel = new QRPBrowser();
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;
                int intSelectRow;
                ////공장 정보저장
                //string strPlantCode = this.uTextPlantCode.Text;
                //string strEquipCode = this.uComboEquipCode.Value.ToString();

                //선택한 리스트의 Key값과 Value값 저장(컬럼그리드콤보가 없으면 셀그리드 콤보 정보로
                string strKey = "";
                string strValue = "";

                if (e.Cell.Column.ValueList != null)
                {
                    strKey = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }


                #region 구성품명 의 리스트 선택시 선택한 이름과 코드를 각 셀에 넣는다

                if (strColumn == "CurDurableMatName")
                {

                    //공백일 경우
                    if (strKey.Equals(string.Empty))
                    {
                        //수량,구성품코드 공백처리
                        e.Cell.Row.Cells["InputQty"].Value = 0;
                        e.Cell.Row.Cells["CurDurableMatCode"].Value = "";

                    }
                    else
                    {
                        intSelectRow = e.Cell.Column.ValueList.SelectedItemIndex;

                        //설비구성품정보 BL 호출

                        //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        //QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        //brwChannel.mfCredentials(clsEquipDurableBOM);

                        //DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOM_DurableMatInputQty(strPlantCode, this.uTextEquipCode.Text, strKey, m_resSys.GetString("SYS_LANG"));

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        brwChannel.mfCredentials(clsEquipDurableBOM);

                        DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                        e.Cell.Row.Cells["CurDurableMatCode"].Value = strKey;

                        //LotNo가 없으면 수량을 직접입력할수있고(100개중 50개만 교체대상이 될수도있음) LotNo가 있으면 할수없다.(LotNo가있는 구성품은 무조건1개라서)
                        if (dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                        {
                            //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            this.uGridDurableMat.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            e.Cell.Row.Cells["InputQty"].Tag = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["CurLotNo"].Value = "";
                        }
                        else
                        {
                            //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridDurableMat.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            e.Cell.Row.Cells["InputQty"].Tag = 1;
                            e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["CurLotNo"].Value = dtDurableMatInfo.Rows[intSelectRow]["LotNo"];
                        }

                        //단위저장
                        e.Cell.Row.Cells["CurLotNo"].Tag = dtDurableMatInfo.Rows[intSelectRow]["UnitCode"];

                        //Lot정보가 있는 구성품을 선택하였을 경우 리스트내에 똑같은 구성품이 올수없다.
                        for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                this.uGridDurableMat.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString()) &&
                                !dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                !this.uGridDurableMat.Rows[i].Appearance.BackColor.Equals(Color.Yellow) &&
                                this.uGridDurableMat.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000068", "M000853", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["InputQty"].Value = 0;
                                e.Cell.Row.Cells["InputQty"].Tag = 0;
                                e.Cell.Row.Cells["CurLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["CurLotNo"].Tag = string.Empty;
                                e.Cell.Row.Cells["CurDurableMatCode"].Value = string.Empty;
                                e.Cell.Row.Cells["CurDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                return;
                            }


                        }

                        //창고 콤보가 선택되어있다면,
                        if (!e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString().Equals(string.Empty))
                        {
                            if (!e.Cell.Row.Cells["CurLotNo"].Value.Equals(e.Cell.Row.Cells["ChgLotNo"].Value))
                            {
                                ChgDurbleMat(e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(), e.Cell.Row.Index);
                                e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["Qty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                            else
                            {
                                if (e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty) || Convert.ToInt32(e.Cell.Row.Cells["ChgQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value))
                                    e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                        }

                    }

                }
                #endregion

                #region 교체구성품창고의 구성품코드 리스트를 가져온다
                //선택한 교체구성품창고의 구성품코드리스트를 가져온다.
                if (strColumn.Equals("ChgDurableInventoryCode"))
                {
                    if (!strKey.Equals(string.Empty))
                    {
                        ChgDurbleMat(strKey, e.Cell.Row.Index);
                    }

                }
                #endregion

                #region 교체구성품코드 선택시 가용량 Lot정보 삽입

                if (strColumn.Equals("ChgDurableMatName"))
                {
                    if (!strKey.Equals(string.Empty))
                    {

                        //금형치공구정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                        brwChannel.mfCredentials(clsDurableStock);

                        //금형치공구정보 Detail매서드 호출
                        DataTable dtStock = new DataTable();


                        //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                        string strLotChk = "";

                        if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                            strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                        //dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(),
                        //                                                        this.uTextPackageA.Text,
                        //                                                        this.uTextPackageA.Tag == null ? "" : this.uTextPackageA.Tag.ToString(),
                        //                                                        strLotChk, m_resSys.GetString("SYS_LANG"));

                        dtStock = clsDurableStock.mfReadDMMDurableStock_Combo_SerialChk(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(), strLotChk, m_resSys.GetString("SYS_LANG"));
                        



                        if (dtStock.Rows.Count > 0)
                        {
                            intSelectRow = e.Cell.ValueList.SelectedItemIndex;

                            ////구성품끼리의 단위가 다를경우 경고
                            //if (!e.Cell.Row.Cells["CurLotNo"].Tag.Equals(dtStock.Rows[intSelectRow]["UnitCode"]))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                       , "확인창", "구성품 단위 확인", "기존구성품의 단위와 교체구성품의 단위가 다릅니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                            //    return;
                            //}
                            //그리드콤보에 선택한 정보를 삽입
                            e.Cell.Row.Cells["ChgLotNo"].Value = dtStock.Rows[intSelectRow]["LotNo"];
                            e.Cell.Row.Cells["Qty"].Value = dtStock.Rows[intSelectRow]["Qty"];
                            e.Cell.Row.Cells["ChgDurableMatCode"].Value = strKey;
                            //e.Cell.Row.Cells["UnitCode"].Value = dtStock.Rows[intSelectRow]["UnitCode"];

                            //Lot정보가 있을경우 교체수량1개 수정불가로 변경
                            if (!dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgQty"].Value = 1;

                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                            }
                            else
                            {
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            }

                            //선택한 정보가 그리드내에 있으면 다시입력
                            for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    uGridDurableMat.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtStock.Rows[intSelectRow]["LotNo"].ToString()) &&
                                    !dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                    this.uGridDurableMat.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit &&
                                   !this.uGridDurableMat.Rows[i].Appearance.BackColor.Equals(Color.Yellow))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000068", "M000853", Infragistics.Win.HAlign.Right);

                                    e.Cell.Row.Cells["Qty"].Value = 0;
                                    e.Cell.Row.Cells["ChgQty"].Value = 0;

                                    e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                    return;
                                }
                            }
                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 치공구 구성품

        /// <summary>
        /// 해당 설비의 치공구BOM정보
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        private string SearchDurableBOM()
        {
            string strRtn = "";
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);

                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));


                //--해당설비의 BOM 이없을 경우 리턴 --
                if (dtDurable.Rows.Count == 0)
                {
                    this.uGridDurableMat.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridDurableMat.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    strRtn = "No";
                    return strRtn;
                }

                if(this.uGridDurableMat.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation.Equals(Infragistics.Win.UltraWinGrid.Activation.NoEdit))
                    this.uGridDurableMat.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                if(this.uGridDurableMat.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation.Equals(Infragistics.Win.UltraWinGrid.Activation.NoEdit))
                    this.uGridDurableMat.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                //콤보 그리드 리스트 클러어
                this.uGridDurableMat.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                //콤보그리드 컬럼설정
                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                string strLang = m_resSys.GetString("SYS_LANG");

                string strText = "";

                if (strLang.Equals("KOR"))
                    strText = "구성품코드,구성품명,LotNo,수량,단위";
                else if (strLang.Equals("CHN"))
                    strText = "构成品条码,构成品名,LotNo,数量,单位";
                else if (strLang.Equals("ENG"))
                    strText = "DurableMatCode,DurableMatName,LotNo,Qty,UnitName";
                //--그리드에 추가 --
                WinGrid wGrid = new WinGrid();
                wGrid.mfSetGridColumnValueGridList(this.uGridDurableMat, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                    , "DurableMatCode", "DurableMatName", dtDurable);

                return strRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtn;
            }
            finally
            { }

        }

        /// <summary>
        /// 교체구성품그리드콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInven">창고정보</param>
        /// <param name="e">Infragistics.Win.UltraWinGrid.CellEventArgs</param>
        private void ChgDurbleMat(string strInven, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                string strLotChk = "";

                if (!this.uGridDurableMat.Rows[intIndex].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    strLotChk = this.uGridDurableMat.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";
                
                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //금형치공구창고에 해당하는 금형치공구 재고 조회
                //DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, strInven, this.uTextPackageA.Text, 
                //                                                                this.uTextPackageA.Tag == null ? "" : this.uTextPackageA.Tag.ToString(),
                //                                                                strLotChk, m_resSys.GetString("SYS_LANG"));
                DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo_SerialChk(strPlantCode, strInven, strLotChk, m_resSys.GetString("SYS_LANG"));

                //금형치공구정보에 검색조건 적용
                WinGrid wGrid = new WinGrid();

                //                this.uGridDurableMat.Rows[intIndex].Cells["ChgDurableMatName"].Column.Layout.ValueLists.Clear();
                string strDropDownValue = "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName";
                string strLang = m_resSys.GetString("SYS_LANG");

                string strDropDownText = "";

                if (strLang.Equals("KOR"))
                    strDropDownText = "치공구코드,치공구,LOTNO,수량,단위코드,단위";
                else if (strLang.Equals("CHN"))
                    strDropDownText = "治工具仓库编号,治工具,LOTNO,数量,单位编号,单位";
                else if (strLang.Equals("ENG"))
                    strDropDownText = "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName";
                //콤보그리드 초기화.
                //e.Cell.Column.Band.Layout.ValueLists.Clear();

                wGrid.mfSetGridCellValueGridList(this.uGridDurableMat, 0, intIndex, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                   , strDropDownValue, strDropDownText, "DurableMatCode", "DurableMatName", dtStock);

                if (dtStock.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion



        /// <summary>
        /// CCS 의뢰상태여부 검색 (의뢰상태면 요청,접수,결과등록 불가처리)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <returns>true : CCS 의뢰상태</returns>
        private bool CheckCCS()
        {
            try
            {
                bool bolChk = false;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.RepairDIS), "RepairDIS");
                QRPEQU.BL.EQUDIS.RepairDIS clsEquip = new QRPEQU.BL.EQUDIS.RepairDIS();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadRepair_MESDB(strPlantCode, "", "", "", strEquipCode, "", "", m_resSys.GetString("SYS_LANG"));

                //CCS 의뢰상태인 경우 요청 불가처리 2012-10-06
                if (dtEquip.Rows.Count > 0 && dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))
                    bolChk = true;


                return bolChk;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }




        






    }
}
