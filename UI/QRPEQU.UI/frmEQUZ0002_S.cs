﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmEQUZ0002.cs                                        */
/* 프로그램명   : 설비인증의뢰요청                                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-06-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPUI;
using QRPSYS;
using QRPCOM.QRPGLO;
using System.Threading;
using System.Resources;
using System.EnterpriseServices;
using System.Collections;
using System.IO;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0002_S : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmEQUZ0002_S()
        {
            InitializeComponent();
        }
        private void frmEQUZ0002_Activated(object sender, System.EventArgs e)
        {
            //검색툴
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmEQUZ0002_Load(object sender, System.EventArgs e)
        {

            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitButton();
            InitGrid();
            InitGrouBox();
            InitComboBox();
            InitValue();
 
            this.uGroupBoxContentsArea.Expanded = false;
            this.uComboPlant.ReadOnly = true;
        }

        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정 
                titleArea.mfSetLabelText("설비인증의뢰요청", m_rssSys.GetString("SYS_FONTNAME"), 12);

                //기본값설정
                uTextCertiReqID.Text = "";
                uTextSetupChargeID.Text = "";

                this.ultraTabControl1.Tabs[2].Visible = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        private void InitValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uTextAdmitStatusCode.Text = "";
                this.uTextCertiReqID.Text = "";
                this.uTextCertiReqName.Text = "";
                this.uTextCertiResultCode.Text = "";
                this.uTextDocCode.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextEquipReasonEtcDesc.Text = "";
                this.uTextMTReasonEtcDesc.Text = "";
                this.uTextObject.Text = "";
                this.uTextSetupChargeID.Text = "";
                this.uTextSetupChargeName.Text = "";
                this.uTextSubject.Text = "";

                this.uComboEquipCertiReqTypeCode.Value = "";
                this.uComboEquipReasonCode.Value = "";
                this.uComboMoveDeptCode.Value = "";
                this.uComboMTReasonCode.Value = "";
                this.uComboOriginDeptCode.Value = "";
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboProcess.Value = "";
                this.uComboSetupDeptCode.Value = "";
                this.uComboUseDeptCode.Value = "";
                this.uComboPKG.Value = "";
                this.uTextModelName.Text = "";
                this.uTextSerialNo.Text = "";
                this.uTextVendorCode.Text = "";

                this.uDateCeriReqDate.Value = DateTime.Now;
                this.uDateSetupCompleteDate.Value = DateTime.Now;

                this.uTextEtcDesc.Clear();
                //this.richTextEditor2.Clear();

                this.uCheckEquipStandardFlag.Checked = false;
                this.uCheckTechStandardFlag.Checked = false;
                this.uCheckWorkStandardFlag.Checked = false;
                this.uCheckGWFlag.Tag = string.Empty;
                this.uComboSearchEquipCode.Appearance.BackColor = Color.White;

                while (this.uGridSetup.Rows.Count > 0)
                {
                    this.uGridSetup.Rows[0].Delete(false);
                }
                while (this.uGridMoldTool.Rows.Count > 0)
                {
                    this.uGridMoldTool.Rows[0].Delete(false);
                }
                while (this.uGridSetupCondition.Rows.Count > 0)
                {
                    this.uGridSetupCondition.Rows[0].Delete(false);
                }
                while (this.uGridCertItem.Rows.Count > 0)
                {
                    this.uGridCertItem.Rows[0].Delete(false);
                }
                while (this.uGridCertFile.Rows.Count > 0)
                {
                    this.uGridCertFile.Rows[0].Delete(false);
                }
                while (this.uGridCertP.Rows.Count > 0)
                {
                    this.uGridCertP.Rows[0].Delete(false);
                }
                uCheckGWTFlag.Checked = false;
                uCheckGWFlag.Checked = false;
                uCheckGWResultState.Checked = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel2, "설비인증의뢰유형", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCompleteSetup, "Setup완료일", m_rssSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelAdminNum, "관리번호", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipmentCode, "설비코드", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipmentName, "설비명", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcess, "공정", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTitle, "제목", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPurpose, "목적", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSetupDepartment, "Setup부서", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSetupComplete, "Setup완료일", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSetupInCharge, "Setup담당자", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelPKG, "적용 PKG", m_rssSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.ultraLabel4, "사용부서", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel5, "인증의뢰일자", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel6, "인증의뢰자", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel7, "승인상태", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel8, "판정결과", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel9, "설비인증의뢰유형", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel10, "관련표준", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel14, "가동조건", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel15, "설비인증항목 및 평가결과", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel16, "첨부문서", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel17, "표준담당자", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelNote, "비고", m_rssSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelMeasuringMachineReason, "계측기 증감사유", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelReasonEquipment, "설비 증감사유", m_rssSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDepartment, "원부서", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMoveDepartment, "이전부서", m_rssSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelReason, "특이사항(사유)", m_rssSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow3, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow4, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow5, "행삭제", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDownload, "다운로드", m_rssSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode UCC = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(UCC);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                DataTable dtEquipCertiReqTypeCode = UCC.mfReadUserCommonCode("EQU", "U0002", m_resSys.GetString("SYS_LANG"));
                DataTable dtMTReason = UCC.mfReadUserCommonCode("EQU", "U0003", m_resSys.GetString("SYS_LANG"));
                DataTable dtEquipReason = UCC.mfReadUserCommonCode("EQU", "U0004", m_resSys.GetString("SYS_LANG"));


                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboSearchEquipCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtEquipCertiReqTypeCode);

                wCombo.mfSetComboEditor(this.uComboEquipCertiReqTypeCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtEquipCertiReqTypeCode);

                wCombo.mfSetComboEditor(this.uComboMTReasonCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtMTReason);

                wCombo.mfSetComboEditor(this.uComboEquipReasonCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtEquipReason);

                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(this.uComboPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPKG, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "Package", "ComboName", dtPackage);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_rssSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region Header Grid
                //설비인증조회
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridSysRequest, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridSysRequest, 0, "DocCode", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "ProcessName", "공정명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "SetupFinishDate", "Setup완료일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "SetupChargeID", "Setup담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 15
                     , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "SetupChargeName", "Setup담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 15
                     , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "SetupDeptCode", "Setup부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 15
                     , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "DeptName", "사용부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "CertiReqDate", "인증의뢰일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "CertiReqID", "인증의뢰자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "CertiReqName", "인증의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "AdmitStatusCode", "승인상태", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "Subject", "제목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "Object", "목적", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EquipCertiReqTypeCode", "설비점검의뢰코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "CertiResultCode", "인증결과코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EquipStandardFlag", "EquipSandardFlag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "WorkStandardFlag", "EquipSandardFlag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "TechStandardFlag", "EquipSandardFlag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "MTReasonCode", "계측기증감사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EquipReasonCode", "설비증감사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "MTReasonEtcDesc", "계측기증감사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EquipReasonEtcDesc", "계측기증감사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "OriginDeptCode", "원부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "MoveDeptCode", "이전부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "IncDecEtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSysRequest, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 290, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--폰트설정
                this.uGridSysRequest.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSysRequest.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //--빈줄추가
                grd.mfAddRowGrid(this.uGridSysRequest, 0);
                #endregion

                #region 부속설비
                //설비인증조회
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridSubSEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridSubSEquip, 0, "EquipCode", "부속설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubSEquip, 0, "EquipName", "부속설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubSEquip, 0, "ModelName", "Model명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubSEquip, 0, "VendorCode", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubSEquip, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region SetUp Grid
                //Setup현황
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridSetup, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridSetup, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSetup, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Right,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridSetup, 0, "SetupDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridSetup, 0, "ProgressDesc", "진행현황", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //금형치공구현황
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridMoldTool, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridMoldTool, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Right,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");


                grd.mfSetGridColumn(this.uGridMoldTool, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "Standard", "규격", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                //폰트설정
                uGridMoldTool.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMoldTool.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridSetup.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSetup.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(this.uGridMoldTool, 0);
                grd.mfAddRowGrid(this.uGridSetup, 0);
                #endregion

                #region 가동조건
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridSetupCondition, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridSetupCondition, 0, "Check", "", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSetupCondition, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, true, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");


                grd.mfSetGridColumn(this.uGridSetupCondition, 0, "SpecStandard", "항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, true, false, 1000
                    , Infragistics.Win.HAlign.Left,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", ""); //Spec기준 / 장비사양

                grd.mfSetGridColumn(this.uGridSetupCondition, 0, "Parameter", "Parameter", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 1000
                    , Infragistics.Win.HAlign.Left,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSetupCondition, 0, "WorkCondition", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", ""); //가동조건

                grd.mfSetGridColumn(this.uGridSetupCondition, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--폰트설정
                this.uGridSetupCondition.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSetupCondition.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //--빈줄추가
                grd.mfAddRowGrid(this.uGridSetupCondition, 0);
                #endregion

                #region 설비인증항목및평가결과
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridCertItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridCertItem, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridCertItem, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridCertItem, 0, "CertiItem", "인증항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 1000, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCertItem, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 6, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnn,nnn", "0");

                grd.mfSetGridColumn(this.uGridCertItem, 0, "JudgeStandard", "판정기준", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1000, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCertItem, 0, "EvalResultCode", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 1000, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCertItem, 0, "JudgeResultCode", "판정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 5, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //--폰트설정
                this.uGridCertItem.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCertItem.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //--빈줄추가
                grd.mfAddRowGrid(this.uGridCertItem, 0);
                #endregion

                #region Binding
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode UCC = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(UCC);

                ////판정결과 콤보박스만들기
                DataTable dtJudgeResult = UCC.mfReadCommonCode("C0022", m_rssSys.GetString("SYS_LANG"));

                ////그리드에 추가
                grd.mfSetGridColumnValueList(this.uGridCertItem, 0, "JudgeResultCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtJudgeResult);
                #endregion

                #region 첨부문서
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridCertFile, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridCertFile, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridCertFile, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridCertFile, 0, "Subject", "문서제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 1000, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCertFile, 0, "FileName", "파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, true, false, 1000, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridCertFile, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 500, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--폰트설정
                this.uGridCertFile.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCertFile.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //--빈줄추가
                grd.mfAddRowGrid(this.uGridCertFile, 0);
                #endregion

                #region 표준담당자
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridCertP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));
                //--컬럼설정
                grd.mfSetGridColumn(this.uGridCertP, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridCertP, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, true, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridCertP, 0, "StandardChargeID", "표준담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridCertP, 0, "StandardChargeName", "표준담당자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCertP, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCertP, 0, "Email", "E-mail", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--폰트설정
                this.uGridCertP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCertP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //--빈줄추가
                grd.mfAddRowGrid(this.uGridCertP, 0);
                #endregion

                #region 설비인증평가
                //설비인증평가
                grd.mfInitGeneralGrid(this.uGrid5, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_rssSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGrid5, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid5, 0, "InspectGroupCode", "InspectGroupCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "InspectTypeCode", "InspectTypeCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "InspectItemCode", "InspectItemCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "CertiItem", "인증항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                //////grd.mfSetGridColumn(this.uGrid5, 0, "JudgeStandard", "판정기준", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1000
                //////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGrid5, 0, "UpperSpec", "규격상한", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid5, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid5, 0, "EvalResultCode", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid5, 0, "EvalResultCode", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 1000
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "JudgeResultCode", "판정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //QRPSYS.BL.SYSPGM.CommonCode UCC = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChannel.mfCredentials(UCC);

                ////판정결과 콤보박스만들기
                DataTable dtJudgeResultQC = UCC.mfReadCommonCode("C0022", m_rssSys.GetString("SYS_LANG"));

                ////그리드에 추가
                grd.mfSetGridColumnValueList(this.uGrid5, 0, "JudgeResultCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtJudgeResultQC);
                #endregion
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGrouBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBoxSetup, GroupBoxType.LIST, "Setup현황", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBoxTool, GroupBoxType.LIST, "금형치공구현황", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxSetup.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxSetup.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxTool.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBoxTool.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //UserID
        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;

            }
            finally
            {
            }
        }

        private DataTable GetUserInfo(string strPlantCode, string strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
                dtUser.Dispose();
            }
        }

        private void SetDiswritable()
        {
            try
            {
                this.uComboEquipCertiReqTypeCode.ReadOnly = true;
                this.uComboEquipCertiReqTypeCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboEquipReasonCode.ReadOnly = true;
                this.uComboEquipReasonCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboMoveDeptCode.ReadOnly = true;
                this.uComboMoveDeptCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboMTReasonCode.ReadOnly = true;
                this.uComboMTReasonCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboOriginDeptCode.ReadOnly = true;
                this.uComboOriginDeptCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                this.uComboProcess.ReadOnly = true;
                this.uComboProcess.Appearance.BackColor = Color.Gainsboro;
                this.uComboUseDeptCode.ReadOnly = true;
                this.uComboUseDeptCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboSetupDeptCode.ReadOnly = true;
                this.uComboSetupDeptCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboPKG.ReadOnly = true;
                this.uComboPKG.Appearance.BackColor = Color.Gainsboro;

                this.uDateCeriReqDate.ReadOnly = true;
                this.uDateCeriReqDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateSetupCompleteDate.ReadOnly = true;
                this.uDateSetupCompleteDate.Appearance.BackColor = Color.Gainsboro;

                
                this.uCheckEquipStandardFlag.Enabled = false;
                this.uCheckTechStandardFlag.Enabled = false;
                this.uCheckWorkStandardFlag.Enabled = false;
                this.uCheckGWTFlag.Enabled = false;

                this.uTextAdmitStatusCode.ReadOnly = true;
                this.uTextAdmitStatusCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextCertiReqID.ReadOnly = true;
                this.uTextCertiReqID.Appearance.BackColor = Color.Gainsboro;
                this.uTextCertiReqName.ReadOnly = true;
                this.uTextCertiReqName.Appearance.BackColor = Color.Gainsboro;
                this.uTextCertiResultCode.ReadOnly = true;
                this.uTextCertiResultCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextDocCode.ReadOnly = true;
                this.uTextDocCode.Appearance.BackColor = Color.Gainsboro;

                this.uTextEquipCode.ReadOnly = true;
                this.uTextEquipCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextEquipName.ReadOnly = true;
                this.uTextEquipName.Appearance.BackColor = Color.Gainsboro;
                this.uTextEquipReasonEtcDesc.ReadOnly = true;
                this.uTextEquipReasonEtcDesc.Appearance.BackColor = Color.Gainsboro;
                this.uTextMTReasonEtcDesc.ReadOnly = true;
                this.uTextMTReasonEtcDesc.Appearance.BackColor = Color.Gainsboro;
                this.uTextObject.ReadOnly = true;
                this.uTextObject.Appearance.BackColor = Color.Gainsboro;
                this.uTextSetupChargeID.ReadOnly = true;
                this.uTextSetupChargeID.Appearance.BackColor = Color.Gainsboro;
                this.uTextSetupChargeName.ReadOnly = true;
                this.uTextSetupChargeName.Appearance.BackColor = Color.Gainsboro;
                this.uTextSubject.ReadOnly = true;

                this.uTextEtcDesc.Enabled = false;
                this.uTextEtcDesc.BackColor = Color.Gainsboro;

                for (int i = 0; i < this.uGridSetup.Rows.Count; i++)
                {
                    this.uGridSetup.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridSetup.Rows[i].Cells["SetupDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridSetup.Rows[i].Cells["ProgressDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                
                for (int i = 0; i < this.uGridMoldTool.Rows.Count; i++)
                {
                    this.uGridMoldTool.Rows[i].Cells["DurableMatCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridMoldTool.Rows[i].Cells["DurableMatName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridMoldTool.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGridSetupCondition.Rows.Count; i++)
                {
                    this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridSetupCondition.Rows[i].Cells["Parameter"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridSetupCondition.Rows[i].Cells["WorkCondition"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridSetupCondition.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                {
                    this.uGridCertItem.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertItem.Rows[i].Cells["CertiItem"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertItem.Rows[i].Cells["SampleSize"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertItem.Rows[i].Cells["JudgeStandard"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertItem.Rows[i].Cells["EvalResultCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertItem.Rows[i].Cells["JudgeResultCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                {
                    this.uGridCertFile.Rows[i].Cells["Subject"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertFile.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertFile.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGridCertP.Rows.Count; i++)
                {
                    this.uGridCertP.Rows[i].Cells["StandardChargeID"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertP.Rows[i].Cells["StandardChargeName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertP.Rows[i].Cells["DeptName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridCertP.Rows[i].Cells["Email"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void SetWritable()
        {
            try
            {
                this.uComboEquipCertiReqTypeCode.ReadOnly = false;
                this.uComboEquipCertiReqTypeCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboEquipReasonCode.ReadOnly = false;
                this.uComboEquipReasonCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboMoveDeptCode.ReadOnly = false;
                this.uComboMoveDeptCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboMTReasonCode.ReadOnly = false;
                this.uComboMTReasonCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboOriginDeptCode.ReadOnly = false;
                this.uComboOriginDeptCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboPlant.ReadOnly = false;
                //this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uComboProcess.ReadOnly = false;
                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;
                this.uComboUseDeptCode.ReadOnly = false;
                this.uComboUseDeptCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboSetupDeptCode.ReadOnly = false;
                this.uComboSetupDeptCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboPKG.ReadOnly = false;
                this.uComboPKG.Appearance.BackColor = Color.White;

                this.uDateCeriReqDate.ReadOnly = false;
                this.uDateCeriReqDate.Appearance.BackColor = Color.PowderBlue;
                this.uDateSetupCompleteDate.ReadOnly = false;
                this.uDateSetupCompleteDate.Appearance.BackColor = Color.PowderBlue;

                
                this.uCheckEquipStandardFlag.Enabled = true;
                this.uCheckTechStandardFlag.Enabled = true;
                this.uCheckWorkStandardFlag.Enabled = true;
                this.uCheckGWTFlag.Enabled = true;

                this.uTextCertiReqID.ReadOnly = false;
                this.uTextCertiReqID.Appearance.BackColor = Color.PowderBlue;

                this.uTextEquipCode.ReadOnly = true;
                this.uTextEquipCode.Appearance.BackColor = Color.PowderBlue;

                this.uTextEquipReasonEtcDesc.ReadOnly = false;
                this.uTextEquipReasonEtcDesc.Appearance.BackColor = Color.White;
                this.uTextMTReasonEtcDesc.ReadOnly = false;
                this.uTextMTReasonEtcDesc.Appearance.BackColor = Color.White;
                this.uTextObject.ReadOnly = false;
                this.uTextObject.Appearance.BackColor = Color.White;
                this.uTextSetupChargeID.ReadOnly = false;
                this.uTextSetupChargeID.Appearance.BackColor = Color.PowderBlue;
                this.uTextSubject.ReadOnly = false;

                this.uTextEtcDesc.Enabled = true;
                this.uTextEtcDesc.BackColor = Color.White;

                for (int i = 0; i < this.uGridSetup.Rows.Count; i++)
                {
                    this.uGridSetup.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridSetup.Rows[i].Cells["SetupDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridSetup.Rows[i].Cells["ProgressDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGridMoldTool.Rows.Count; i++)
                {
                    this.uGridMoldTool.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridMoldTool.Rows[i].Cells["DurableMatCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridMoldTool.Rows[i].Cells["DurableMatName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridMoldTool.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGridSetupCondition.Rows.Count; i++)
                {
                    this.uGridSetupCondition.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridSetupCondition.Rows[i].Cells["Parameter"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridSetupCondition.Rows[i].Cells["WorkCondition"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridSetupCondition.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                {
                    this.uGridCertItem.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertItem.Rows[i].Cells["CertiItem"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertItem.Rows[i].Cells["SampleSize"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertItem.Rows[i].Cells["JudgeStandard"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertItem.Rows[i].Cells["EvalResultCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertItem.Rows[i].Cells["JudgeResultCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                {
                    this.uGridCertFile.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertFile.Rows[i].Cells["Subject"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertFile.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertFile.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGridCertP.Rows.Count; i++)
                {

                    this.uGridCertP.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertP.Rows[i].Cells["StandardChargeName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertP.Rows[i].Cells["DeptName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridCertP.Rows[i].Cells["Email"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                InitValue();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                QRPEQU.BL.EQUCCS.EQUEquipCertiH CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                brwChannel.mfCredentials(CertiH);

                //Parameter
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipCode = this.uComboSearchEquipCode.Value.ToString();
                string strSetupCompleteDateFrom = this.uDateSearchSetupCompleteDateFrom.DateTime.Date.ToString("yyyy-MM-dd");
                string strSetupCompleteDateTo = this.uDateSearchSetupCompleteDateTo.DateTime.Date.ToString("yyyy-MM-dd");

                //Search
                DataTable dtCertiH = CertiH.mfReadEQUEquipCertiH(strPlantCode, strSetupCompleteDateFrom
                                                                    , strSetupCompleteDateTo, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //Grid DataBinding
                this.uGridSysRequest.DataSource = dtCertiH;
                this.uGridSysRequest.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }




                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSysRequest.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
        public void mfSave()
        {
            titleArea.Focus();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                this.uTextSubject.Text = this.uTextEquipName.Text + "  " + this.uComboEquipCertiReqTypeCode.SelectedText;

                #region 필수확인

                #region Header 확인
                #region 승인요청, 승인완료 검사
                if (this.uCheckGWFlag.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000762", Infragistics.Win.HAlign.Right);

                    return;
                }
                #endregion
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uComboPlant.DropDown();
                    return;
                }
                if (this.uTextEquipCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000721", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextEquipCode.Focus();
                    return;
                }
                if (this.uComboProcess.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000293", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uComboProcess.DropDown();
                    return;
                }
                if (this.uComboSetupDeptCode.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000119", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uComboSetupDeptCode.DropDown();
                    return;
                }
                if (this.uDateSetupCompleteDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000826", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uDateSetupCompleteDate.DropDown();
                    return;
                }
                if (this.uTextSetupChargeID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000118", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextSetupChargeID.Focus();
                    return;
                }
                if (this.uComboUseDeptCode.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000618", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uComboUseDeptCode.DropDown();
                    return;
                }
                if (this.uDateCeriReqDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000858", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uDateCeriReqDate.DropDown();
                    return;
                }
                if (this.uTextCertiReqID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000859", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextCertiReqID.Focus();
                    return;
                }
                if (this.uComboEquipCertiReqTypeCode.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000709", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uComboEquipCertiReqTypeCode.DropDown();
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                #endregion
                //uGridSetup uGridMoldTool uGridSetupCondition uGridCertItem uGridCertFile uGridCertP
                if (this.uGridSetup.Rows.Count == 0 && this.uGridMoldTool.Rows.Count == 0 && this.uGridSetupCondition.Rows.Count == 0
                    && this.uGridCertItem.Rows.Count == 0 && this.uGridCertFile.Rows.Count == 0 && this.uGridCertP.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001228", "M000860", Infragistics.Win.HAlign.Center);


                    return;
                }
                #region Item 확인

                WinGrid wGrid = new WinGrid();
                for (int i = 0; i < uGridSetup.Rows.Count; i++)
                {
                    if (wGrid.mfCheckCellDataInRow(uGridSetup, 0, i))
                        uGridSetup.Rows[i].Delete(false);
                }
                for (int i = 0; i < uGridSetup.Rows.Count; i++)
                {
                    if (uGridSetup.Rows[i].Hidden == false)
                    {
                        if (uGridSetup.Rows[i].Cells["SetupDate"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000864", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridSetup.Rows[i].Cells["SetupDate"].Selected = true;
                            return;
                        }
                        if (uGridSetup.Rows[i].Cells["ProgressDesc"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001127", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridSetup.Rows[i].Cells["ProgressDesc"].Selected = true;
                            return;
                        }
                    }
                }

                for (int i = 0; i < uGridMoldTool.Rows.Count; i++)
                {
                    if (wGrid.mfCheckCellDataInRow(uGridMoldTool, 0, i))
                        uGridMoldTool.Rows[i].Delete(false);
                }
                for (int i = 0; i < uGridMoldTool.Rows.Count; i++)
                {
                    if (uGridMoldTool.Rows[i].Hidden == false)
                    {
                        if (uGridMoldTool.Rows[i].Cells["DurableMatCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000339", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridMoldTool.Rows[i].Cells["DurableMatCode"].Selected = true;
                            return;
                        }
                    }
                }

                for (int i = 0; i < uGridSetupCondition.Rows.Count; i++)
                {
                    if (wGrid.mfCheckCellDataInRow(uGridSetupCondition, 0, i))
                        uGridSetupCondition.Rows[i].Delete(false);
                }
                for (int i = 0; i < uGridSetupCondition.Rows.Count; i++)
                {
                    if (uGridSetupCondition.Rows[i].Hidden == false)
                    {
                        if (uGridSetupCondition.Rows[i].Cells["SpecStandard"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000123", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridSetupCondition.Rows[i].Cells["SpecStandard"].Selected = true;
                            return;
                        }
                        if (uGridSetupCondition.Rows[i].Cells["Parameter"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000101", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridSetupCondition.Rows[i].Cells["Parameter"].Selected = true;
                            return;
                        }
                    }
                }

                for (int i = 0; i < uGridCertItem.Rows.Count; i++)
                {
                    if (wGrid.mfCheckCellDataInRow(uGridCertItem, 0, i))
                        uGridCertItem.Rows[i].Delete(false);
                }
                for (int i = 0; i < uGridCertItem.Rows.Count; i++)
                {
                    if (this.uGridCertItem.Rows[i].Hidden == false)
                    {
                        if (uGridCertItem.Rows[i].Cells["CertiItem"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000038", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridCertItem.Rows[i].Cells["CertiItem"].Selected = true;
                            return;
                        }
                    }
                }

                for (int i = 0; i < uGridCertP.Rows.Count; i++)
                {
                    if (wGrid.mfCheckCellDataInRow(uGridCertP, 0, i))
                        uGridCertP.Rows[i].Delete(false);
                }
                for (int i = 0; i < uGridCertP.Rows.Count; i++)
                {
                    if (uGridCertP.Rows[i].Hidden == false)
                    {
                        if (uGridCertP.Rows[i].Cells["StandardChargeID"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001200", Infragistics.Win.HAlign.Center);

                            //Focus
                            uGridCertP.Rows[i].Cells["StandardChargeID"].Selected = true;
                            return;
                        }
                    }
                }

                for (int i = 0; i < uGridCertFile.Rows.Count; i++)
                {
                    if (wGrid.mfCheckCellDataInRow(uGridCertFile, 0, i))
                        uGridCertFile.Rows[i].Delete(false);
                }
                for (int i = 0; i < uGridCertFile.Rows.Count; i++)
                {
                    if (this.uGridCertFile.Rows.Count > 0)
                    {
                        if (uGridCertFile.Rows[i].Hidden == false)
                        {
                            if (uGridCertFile.Rows[i].Cells["Subject"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000406", Infragistics.Win.HAlign.Center);

                                //Focus
                                uGridCertFile.Rows[i].Cells["Subject"].Selected = true;
                                return;
                            }
                            if (uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001187", Infragistics.Win.HAlign.Center);

                                //Focus
                                uGridCertFile.Rows[i].Cells["FileName"].Selected = true;
                                return;
                            }
                            else if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo fl = new FileInfo(uGridCertFile.Rows[i].Cells["FileName"].Value.ToString());

                                if (!fl.Exists)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001188", Infragistics.Win.HAlign.Center);

                                    //Focus
                                    uGridCertFile.Rows[i].Cells["FileName"].Selected = true;
                                    return;
                                }
                            }
                        }
                    }
                }
                #endregion

                int checkSetup = 0;
                int checkSetupCondition = 0;
                int checkCertiItem = 0;
                for (int i = 0; i < this.uGridSetup.Rows.Count; i++)
                {
                    if (this.uGridSetup.Rows[i].Hidden == false)
                    {
                        checkSetup++;
                    }
                }
                for (int i = 0; i < this.uGridSetupCondition.Rows.Count; i++)
                {
                    if (this.uGridSetupCondition.Rows[i].Hidden == false)
                    {
                        checkSetupCondition++;
                    }
                }
                for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                {
                    if (this.uGridCertItem.Rows[i].Hidden == false)
                    {
                        checkCertiItem++;
                    }
                }
                if (checkSetup == 0 || checkSetupCondition == 0 || checkCertiItem == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000765"
                                    , Infragistics.Win.HAlign.Center);

                    return;
                }
                if (this.uGridSetupCondition.Rows.Count > 0)
                    this.uGridSetupCondition.ActiveCell = this.uGridSetupCondition.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridSetupCondition.Rows.Count; i++)
                {
                    if (this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001228", "M000123", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Selected = true;
                        return;
                    }
                    else if (this.uGridSetupCondition.Rows[i].Cells["Parameter"].Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001228", "M000101", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uGridSetupCondition.Rows[i].Cells["Parameter"].Selected = true;
                        return;
                    }
                }

                if (this.uGridCertItem.Rows.Count > 0)
                    this.uGridCertItem.ActiveCell = this.uGridCertItem.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                {
                    if (this.uGridCertItem.Rows[i].Cells["CertiItem"].Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001228", "M000861", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uGridCertItem.Rows[i].Cells["CertiItem"].Selected = true;
                        return;
                    }
                }

                if (this.uGridCertP.Rows.Count > 0)
                    this.uGridCertP.ActiveCell = this.uGridCertP.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridCertP.Rows.Count; i++)
                {
                    
                    if (this.uGridCertP.Rows[i].Cells["StandardChargeID"].Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001228", "M001200", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uGridCertP.Rows[i].Cells["StandardChargeID"].Selected = true;
                        return;
                    }
                }

                if (this.uGridCertFile.Rows.Count > 0)
                    this.uGridCertFile.ActiveCell = this.uGridCertFile.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                {
                    if (this.uGridCertFile.Rows[i].Cells["Subject"].Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001228", "M000406", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uGridCertFile.Rows[i].Cells["Subject"].Selected = true;
                        return;
                    }
                    else if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString() == "FileName")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001228", "M001145", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uGridCertFile.Rows[i].Cells["FileName"].Selected = true;
                        return;
                    }
                }

                #endregion

                DialogResult DResult = new DialogResult();
                if (uCheckGWTFlag.Checked)
                {
                    if(uCheckGWTFlag.Enabled.Equals(false))
                    DResult = (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                         Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "M001264", "M000235", "M000233",
                         Infragistics.Win.HAlign.Right));
                    else
                        DResult = (msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, 500, 500,
                         Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "M001264", "M000235", "M000234",
                         Infragistics.Win.HAlign.Right));
                }
                else
                {
                    DResult = (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                         Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "M001264", "M001053", "M000936",
                         Infragistics.Win.HAlign.Right));
                }

                //// 결재선 팝업창
                QRPCOM.UI.frmCOM0013 frm = new QRPCOM.UI.frmCOM0013();
                DialogResult _dr = new DialogResult();

                if ( (uCheckGWTFlag.Checked && (DResult == DialogResult.Yes || DResult == DialogResult.No)) ||
                    (!uCheckGWTFlag.Checked && DResult == DialogResult.Yes) )
                {
                    #region 결재선 팝업창 호출
                    if (uCheckGWTFlag.Checked && (DResult == DialogResult.Yes || DResult == DialogResult.OK))
                    {
                        //결재선 팝업창 호출
                        frm.WriteID = uTextCertiReqID.Text;
                        frm.WriteName = uTextCertiReqName.Text;

                        _dr = frm.ShowDialog();
                        if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001228", "M001030", Infragistics.Win.HAlign.Center);

                            return;
                        }
                    }
                    #endregion


                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    #region Channel 설정
                    QRPBrowser brwChannel = new QRPBrowser();
                    //FileServer 연결
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0003");

                    //Header
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiH clsCertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                    brwChannel.mfCredentials(clsCertiH);
                    //Item
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItem), "EQUEquipCertiItem");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiItem clsItem = new QRPEQU.BL.EQUCCS.EQUEquipCertiItem();
                    brwChannel.mfCredentials(clsItem);
                    //ItemQC
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC), "EQUEquipCertiItemQC");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC clsItemQC = new QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC();
                    brwChannel.mfCredentials(clsItemQC);
                    //EquipSetup
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipSetup), "EQUEquipSetup");
                    QRPEQU.BL.EQUCCS.EQUEquipSetup clsSetup = new QRPEQU.BL.EQUCCS.EQUEquipSetup();
                    brwChannel.mfCredentials(clsSetup);
                    //EquipCertiP
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiP), "EQUEquipCertiP");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiP clsCertiP = new QRPEQU.BL.EQUCCS.EQUEquipCertiP();
                    brwChannel.mfCredentials(clsCertiP);
                    //SetupDuralmat
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupDurableMat), "EQUSetupDurableMat");
                    QRPEQU.BL.EQUCCS.EQUSetupDurableMat clsDurable = new QRPEQU.BL.EQUCCS.EQUSetupDurableMat();
                    brwChannel.mfCredentials(clsDurable);
                    //SetupCondition
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupCondition), "EQUSetupCondition");
                    QRPEQU.BL.EQUCCS.EQUSetupCondition clsCondition = new QRPEQU.BL.EQUCCS.EQUSetupCondition();
                    brwChannel.mfCredentials(clsCondition);
                    //File
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiFile), "EQUEquipCertiFile");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiFile clsFile = new QRPEQU.BL.EQUCCS.EQUEquipCertiFile();
                    brwChannel.mfCredentials(clsFile);
                    #endregion

                    #region DataTable
                    //DataTable
                    DataTable dtHeader = new DataTable();
                    DataTable dtSaveItem = new DataTable();
                    DataTable dtDelItem = new DataTable();
                    DataTable dtSaveItemQC = new DataTable();
                    DataTable dtSaveEquipSetup = new DataTable();
                    DataTable dtDelEquipSetup = new DataTable();
                    DataTable dtSaveCertiP = new DataTable();
                    DataTable dtDelCertiP = new DataTable();
                    DataTable dtSaveDurable = new DataTable();
                    DataTable dtDelDurable = new DataTable();
                    DataTable dtSaveCondition = new DataTable();
                    DataTable dtDelCondition = new DataTable();
                    DataTable dtSaveFile = new DataTable();
                    DataTable dtDelFile = new DataTable();
                    DataRow row;
                    #endregion

                    #region Header
                    
                    //Header Row
                    dtHeader = clsCertiH.mfSetDataInfo();

                    //리포트 작성을 위한 설비 작업 기술 표준 컬럼 설정
                    //dtHeader.Columns.Add("EquipStandard", typeof(string));
                    //dtHeader.Columns.Add("WorkStandard", typeof(string));
                    //dtHeader.Columns.Add("TechStandard", typeof(string)); 
                    dtHeader.Columns.Add("ReProcName", typeof(string)); //공정명
                    dtHeader.Columns.Add("CertiReqName", typeof(string)); //의뢰자

                    row = dtHeader.NewRow();
                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                    row["DocCode"] = this.uTextDocCode.Text;
                    row["EquipCode"] = this.uTextEquipCode.Text;
                    row["EquipName"] = this.uTextEquipName.Text;
                    row["ProcessCode"] = this.uComboProcess.Value.ToString();
                    row["ReProcName"] = this.uComboProcess.Text;
                    row["Package"] = this.uComboPKG.Value.ToString();
                    row["Subject"] = this.uTextSubject.Text;
                    row["Object"] = this.uTextObject.Text;
                    row["SetupChargeID"] = this.uTextSetupChargeID.Text;
                    
                    row["SetupDeptCode"] = this.uComboSetupDeptCode.Value.ToString();
                    row["SetupFinishDate"] = Convert.ToDateTime(this.uDateSetupCompleteDate.Value).ToString("yyyy-MM-dd");
                    row["UseDeptCode"] = this.uComboUseDeptCode.Value.ToString();
                    row["CertiReqDate"] = Convert.ToDateTime(this.uDateCeriReqDate.Value).ToString("yyyy-MM-dd");
                    row["CertiReqID"] = this.uTextCertiReqID.Text;
                    row["CertiReqName"] = "(Dept)" + this.uComboUseDeptCode.Text + "(Name)" + this.uTextCertiReqName.Text;
                    row["EquipCertiReqTypeCode"] = this.uComboEquipCertiReqTypeCode.Value.ToString();
                    row["EquipCertiReqTypeName"] = this.uComboEquipCertiReqTypeCode.SelectedText.ToString();

                    row["GWTFlag"] = uCheckGWTFlag.Checked ? DResult == DialogResult.Yes ? "T" : "F" : "F";
                    if (this.uCheckEquipStandardFlag.Checked == true)
                    {
                        row["EquipStandardFlag"] = "T";
                        //row["EquipStandard"] = "O";
                    }
                    else
                    {
                        row["EquipStandardFlag"] = "F";
                        
                    }
                    if (this.uCheckWorkStandardFlag.Checked == true)
                    {
                        row["WorkStandardFlag"] = "T";
                        //row["WorkStandard"] = "O";
                    }
                    else
                    {
                        row["WorkStandardFlag"] = "F";
                        
                    }
                    if (this.uCheckTechStandardFlag.Checked == true)
                    {
                        row["TechStandardFlag"] = "T";
                        //row["TechStandard"] = "O";
                    }
                    else
                    {
                        row["TechStandardFlag"] = "F";
                        
                    }
                    if (DResult == DialogResult.Yes && uCheckGWTFlag.Checked)
                    {
                        row["AdmitStatusCode"] = "AR";
                    }
                    else
                    {
                        row["AdmitStatusCode"] = "WR";
                    }

                    row["CertiResultCode"] = this.uTextCertiResultCode.Text;
                    row["EtcDesc"] = this.uTextEtcDesc.Text.Trim();
                    row["ModelName"] = uTextModelName.Text;
                    row["SerialNo"] = uTextSerialNo.Text;
                    row["VendorCode"] = uTextVendorCode.Text;

                    row["EquipCertiReqTypeName"] = uComboEquipCertiReqTypeCode.Text; // dtHeader.Rows[0]["EquipCertiReqTypeName"].ToString();
                    row["Object"] = uTextObject.Text; //dtHeader.Rows[0]["Object"].ToString();

                    dtHeader.Rows.Add(row);
                    #endregion

                    #region Setup
                    dtSaveEquipSetup = clsSetup.mfSetDataInfo();
                    dtDelEquipSetup = clsSetup.mfSetDataInfo();

                    if(this.uGridSetup.Rows.Count > 0)
                        this.uGridSetup.ActiveCell = this.uGridSetup.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridSetup.Rows.Count; i++)
                    {
                        if (this.uGridSetup.Rows[i].Hidden == false)
                        {
                            row = dtSaveEquipSetup.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = this.uGridSetup.Rows[i].RowSelectorNumber;
                            row["SetupDate"] = Convert.ToDateTime(this.uGridSetup.Rows[i].Cells["SetupDate"].Value).ToString("yyyy-MM-dd");
                            row["ProgressDesc"] = this.uGridSetup.Rows[i].Cells["ProgressDesc"].Value.ToString();

                            dtSaveEquipSetup.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelEquipSetup.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;

                            dtDelEquipSetup.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region SetupDurablemat
                    dtSaveDurable = clsDurable.mfSetDataInfo();
                    dtDelDurable = clsDurable.mfSetDataInfo();

                    if(this.uGridMoldTool.Rows.Count > 0)
                        this.uGridMoldTool.ActiveCell = this.uGridMoldTool.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridMoldTool.Rows.Count; i++)
                    {
                        
                        if (this.uGridMoldTool.Rows[i].Hidden == false)
                        {
                            row = dtSaveDurable.NewRow();
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = this.uGridMoldTool.Rows[i].RowSelectorNumber;
                            row["DurableMatCode"] = this.uGridMoldTool.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["EtcDesc"] = this.uGridMoldTool.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveDurable.Rows.Add(row);

                        }
                        else
                        {
                            row = dtDelDurable.NewRow();
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            dtDelDurable.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region SetupCondition
                    dtSaveCondition = clsCondition.mfSetDataInfo();
                    dtDelCondition = clsCondition.mfSetDataInfo();

                    if(this.uGridSetupCondition.Rows.Count > 0)
                        this.uGridSetupCondition.ActiveCell = this.uGridSetupCondition.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridSetupCondition.Rows.Count; i++)
                    {
                        
                        ////if (this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////, "확인창", "필수입력사항 확인", "SPEC기준/장비사양을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    //Focus
                        ////    this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Selected = true;
                        ////    return;
                        ////}
                        ////else if (this.uGridSetupCondition.Rows[i].Cells["Parameter"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////, "확인창", "필수입력사항 확인", "Parameter를 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    //Focus
                        ////    this.uGridSetupCondition.Rows[i].Cells["Parameter"].Selected = true;
                        ////    return;
                        ////}
                        //else if (this.uGridSetupCondition.Rows[i].Hidden == false)
                        if (this.uGridSetupCondition.Rows[i].Hidden == false)
                        {
                            row = dtSaveCondition.NewRow();
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = this.uGridSetupCondition.Rows[i].RowSelectorNumber;
                            row["SpecStandard"] = this.uGridSetupCondition.Rows[i].Cells["SpecStandard"].Value.ToString();
                            row["Parameter"] = this.uGridSetupCondition.Rows[i].Cells["Parameter"].Value.ToString();
                            row["WorkCondition"] = this.uGridSetupCondition.Rows[i].Cells["WorkCondition"].Value.ToString();
                            row["EtcDesc"] = this.uGridSetupCondition.Rows[i].Cells["EtcDesc"].Value.ToString();
                            dtSaveCondition.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelCondition.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;

                            dtDelCondition.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region EquipCertiItem
                    dtSaveItem = clsItem.mfSetDataInfo();
                    dtDelItem = clsItem.mfSetDataInfo();

                    if(this.uGridCertItem.Rows.Count > 0 )
                        this.uGridCertItem.ActiveCell = this.uGridCertItem.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                    {
                        
                        ////if (this.uGridCertItem.Rows[i].Cells["CertiItem"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////, "확인창", "필수입력사항 확인", "인증항목을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    //Focus
                        ////    this.uGridCertItem.Rows[i].Cells["CertiItem"].Selected = true;
                        ////    return;
                        ////}
                        ////else if (this.uGridCertItem.Rows[i].Hidden == false)
                        if (this.uGridCertItem.Rows[i].Hidden == false)
                        {
                            row = dtSaveItem.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = this.uGridCertItem.Rows[i].RowSelectorNumber;
                            row["CertiItem"] = this.uGridCertItem.Rows[i].Cells["CertiItem"].Value.ToString();
                            row["SampleSize"] = this.uGridCertItem.Rows[i].Cells["SampleSize"].Value.ToString();
                            row["JudgeStandard"] = this.uGridCertItem.Rows[i].Cells["JudgeStandard"].Value.ToString();
                            row["EvalResultCode"] = this.uGridCertItem.Rows[i].Cells["EvalResultCode"].Value.ToString();
                            row["JudgeResultCode"] = this.uGridCertItem.Rows[i].Cells["JudgeResultCode"].Value.ToString();

                            dtSaveItem.Rows.Add(row);
                        }

                        else
                        {
                            row = dtDelItem.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;

                            dtDelItem.Rows.Add(row);
                        }
                    }
                    if (this.uGrid5.Rows.Count > 0)
                    {
                        dtSaveItemQC = clsItemQC.mfSetDataInfo();
                        for (int i = 0; i < this.uGrid5.Rows.Count; i++)
                        {
                            row = dtSaveItemQC.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = this.uGrid5.Rows[i].RowSelectorNumber;
                            row["CertiItem"] = this.uGrid5.Rows[i].Cells["CertiItem"].Value.ToString();
                            row["SampleSize"] = this.uGrid5.Rows[i].Cells["SampleSize"].Value.ToString();
                            row["EvalResultCode"] = this.uGrid5.Rows[i].Cells["EvalResultCode"].Value.ToString();
                            row["JudgeResultCode"] = this.uGrid5.Rows[i].Cells["JudgeResultCode"].Value.ToString();
                            row["UpperSpec"] = this.uGrid5.Rows[i].Cells["UpperSpec"].Value.ToString();
                            row["LowerSpec"] = this.uGrid5.Rows[i].Cells["LowerSpec"].Value.ToString();

                            dtSaveItemQC.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region EquipCertiP
                    dtSaveCertiP = clsCertiP.mfSetDataInfo();
                    dtDelCertiP = clsCertiP.mfSetDataInfo();

                    if(this.uGridCertP.Rows.Count > 0)
                        this.uGridCertP.ActiveCell = this.uGridCertP.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridCertP.Rows.Count; i++)
                    {
                        //////if (this.uGridCertP.Rows[i].Cells["StandardChargeID"].Text == "")
                        //////{
                        //////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //////, "확인창", "필수입력사항 확인", "표준담당자ID를 입력해주세요", Infragistics.Win.HAlign.Center);

                        //////    //Focus
                        //////    this.uGridCertP.Rows[i].Cells["StandardChargeID"].Selected = true;
                        //////    return;
                        //////}
                        //////else if (this.uGridCertP.Rows[i].Hidden == false)
                        if (this.uGridCertP.Rows[i].Hidden == false)
                        {
                            row = dtSaveCertiP.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["StandardChargeID"] = this.uGridCertP.Rows[i].Cells["StandardChargeID"].Value.ToString();

                            dtSaveCertiP.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelCertiP.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;

                            dtDelCertiP.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region ActiveReportUpDownLoad

                    if (this.uCheckGWTFlag.Checked.Equals(true) && this.uCheckGWResultState.Checked.Equals(false) && DResult == DialogResult.Yes)
                    {
                        //DataTable _dtHeader = dtHeader.Copy();
                        //DataTable _dtSetup = dtSaveEquipSetup.Copy();
                        //DataTable _dtCondition = dtSaveCondition.Copy();
                        //DataTable _dtItem = dtSaveItem.Copy();

                        //ActiveReport(_dtHeader, _dtSetup, _dtCondition, _dtItem);

                        #region 리포트 첨부문서 그리드에 추가

                        //////리포트가 저장되있는 경로
                        ////string strBrowserPath = Application.ExecutablePath;
                        ////int intPos = strBrowserPath.LastIndexOf(@"\");
                        ////string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";

                        ////this.uGridCertFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                        //////기존 리포트 정보 확인
                        ////int intCnt = this.uGridCertFile.Rows.Count;
                        ////string strFileChk = "";

                        //////기존리포트 정보가 있을 경우 갱신을 한다.
                        ////for (int i = 0; i < intCnt; i++)
                        ////{
                        ////    if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains("-Setup.pdf") || this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains(m_strQRPBrowserPath + "Setup.pdf"))
                        ////    {
                        ////        this.uGridCertFile.Rows[i].Cells["FileName"].Value = m_strQRPBrowserPath + "Setup.pdf";
                        ////        strFileChk = "ok";
                        ////    }
                        ////    if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains("-Req.pdf") || this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains(m_strQRPBrowserPath + "Req.pdf"))
                        ////    {
                        ////        this.uGridCertFile.Rows[i].Cells["FileName"].Value = m_strQRPBrowserPath + "Req.pdf";
                        ////        strFileChk = "ok";
                        ////    }
                        ////}

                        //////기존정보가 없는 경우 리포트를 기존첨부문서 + 하여 올린다.
                        ////if (strFileChk.Equals(string.Empty))
                        ////{
                        ////    for (int i = 0; i < 2; i++)
                        ////    {
                        ////        //첨부문서 그리드에 리포트목록을 추가 시킨다.
                        ////        string strSubject = "";
                        ////        string strFile = "";
                        ////        uGridCertFile.DisplayLayout.Bands[0].AddNew();

                        ////        if (i.Equals(0))
                        ////        {

                        ////            strSubject = "Setup보고서";
                        ////            strFile = "Setup.pdf";
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["Seq"].Value = "0";
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["Subject"].Value = strSubject;
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["FileName"].Value = m_strQRPBrowserPath + strFile;
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["EtcDesc"].Value = "";
                        ////        }
                        ////        else
                        ////        {
                        ////            strSubject = "설비인증/의뢰통보서";
                        ////            strFile = "Req.pdf";
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["Seq"].Value = "0";
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["Subject"].Value = strSubject;
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["FileName"].Value = m_strQRPBrowserPath + strFile;
                        ////            this.uGridCertFile.Rows[this.uGridCertFile.Rows.Count - 1].Cells["EtcDesc"].Value = "";
                        ////        }

                        ////    }
                        ////}
                        ////uGridCertFile.DisplayLayout.Bands[0].AddNew();

                        #endregion


                    }

                    #endregion

                    #region File

                    dtSaveFile = clsFile.mfSetDataInfo();
                    dtDelFile = clsFile.mfSetDataInfo();
                    //Seq가 없는 경우 Seq할당(가장 높은 번호 + 1)
                    for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                    {
                        if (this.uGridCertFile.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            if (this.uGridCertFile.Rows[i].RowSelectorNumber.Equals(1))
                            {
                                this.uGridCertFile.Rows[i].Cells["Seq"].Value = "1";
                            }
                            else
                            {
                                if (!this.uGridCertFile.Rows[i].Cells["Subject"].Value.ToString().Equals(string.Empty))
                                    this.uGridCertFile.Rows[i].Cells["Seq"].Value = Convert.ToString(Convert.ToInt32(this.uGridCertFile.Rows[i - 1].Cells["Seq"].Value.ToString()) + 1);
                            }

                        }
                    }
                    //파일 이름 저장용
                    String[] strFileName = new String[this.uGridCertFile.Rows.Count];

                    for (int i = 0; i < uGridCertFile.Rows.Count; i++)
                    {
                        if (uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty) &&
                            uGridCertFile.Rows[i].Cells["Subject"].Value.ToString().Equals(string.Empty) &&
                            uGridCertFile.Rows[i].Cells["EtcDesc"].Value.ToString().Equals(string.Empty) &&
                            uGridCertFile.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            //uGridCertFile.Rows[i].Delete();
                            //i = 0;
                            uGridCertFile.Rows[i].Hidden = true;
                        }
                    }


                    for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                    {
                        if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString());
                            strFileName[i] = fileDoc.Name;
                        }
                        else
                        {
                            strFileName[i] = this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString();
                        }


                        ////this.uGridCertFile.ActiveCell = this.uGridCertFile.Rows[0].Cells[0];
                        ////if (this.uGridCertFile.Rows[i].Cells["Subject"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////, "확인창", "필수입력사항 확인", "문서제목을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    //Focus
                        ////    this.uGridCertFile.Rows[i].Cells["Subject"].Selected = true;
                        ////    return;
                        ////}
                        ////else if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString() == "FileName")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////, "확인창", "필수입력사항 확인", "첨부파일을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    //Focus
                        ////    this.uGridCertFile.Rows[i].Cells["FileName"].Selected = true;
                        ////    return;
                        ////}
                        ////else if (this.uGridCertFile.Rows[i].Hidden == false)
                        if (this.uGridCertFile.Rows[i].Hidden == false)
                        {
                            row = dtSaveFile.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = Convert.ToInt32(this.uGridCertFile.Rows[i].Cells["Seq"].Value.ToString());
                            row["Subject"] = this.uGridCertFile.Rows[i].Cells["Subject"].Value.ToString();
                            row["FileName"] = strFileName[i];
                            row["EtcDesc"] = this.uGridCertFile.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveFile.Rows.Add(row);

                        }
                        else
                        {
                            //삭제
                            row = dtDelFile.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DocCode"] = this.uTextDocCode.Text;
                            row["Seq"] = Convert.ToInt32(this.uGridCertFile.Rows[i].Cells["Seq"].Value.ToString());

                            dtDelFile.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region BL
                    String strMsg = clsCertiH.mfSaveEQUEquipCertiH(dtHeader, dtSaveFile, dtDelFile, dtSaveItem, dtSaveItemQC, dtDelItem, dtSaveCertiP, dtDelCertiP
                                                                        , dtSaveEquipSetup, dtDelEquipSetup, dtSaveCondition, dtDelCondition, dtSaveDurable,dtDelDurable
                                                                        , m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strMsg);

                    //this.MdiParent.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);

                    System.Windows.Forms.DialogResult result;

                    String strRtnDocCode;

                    if (this.uTextDocCode.Text == "")
                    {   
                        strRtnDocCode = ErrRtn.mfGetReturnValue(0);
                        this.uTextDocCode.Text = strRtnDocCode;
                    }
                    else
                    {
                        strRtnDocCode = this.uTextDocCode.Text;
                    }

                    #endregion

                    //this.MdiParent.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);

                    //Thread.Sleep(500);

                    bool FileUploadCheck = false;

                    #region DB저장 성공시 서버에 첨부파일 저장
                    if (ErrRtn.ErrNum == 0 && !dtSaveFile.Rows.Count.Equals(0))
                    {
                        dtHeader.Rows[0]["DocCode"] = strRtnDocCode;
                        if (this.uCheckGWTFlag.Checked.Equals(true) && this.uCheckGWResultState.Checked.Equals(false) && DResult == DialogResult.Yes)
                        {
                            ////DataTable _dtHeader = dtHeader.Copy(); //헤더
                            ////DataTable _dtSetup = dtSaveEquipSetup.Copy();
                            ////DataTable _dtCondition = dtSaveCondition.Copy();
                            ////DataTable _dtItem = dtSaveItem.Copy();

                            ////ActiveReport(_dtHeader, _dtSetup, _dtCondition, _dtItem);
                        }

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrAtt = new ArrayList();
                        String strfName = this.uComboPlant.Value.ToString() + "-" + strRtnDocCode;

                        for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                        {
                            if (this.uGridCertFile.Rows[i].Hidden == false)
                            {
                                if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                                {
                                    FileInfo fileDoc = new FileInfo(this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString());
                                    String strUploadFile = fileDoc.DirectoryName + "\\" + strfName + "-" + this.uGridCertFile.Rows[i].Cells["Seq"].Value.ToString() + "-" + fileDoc.Name;

                                    //같은 이름의 파일 있을경우 삭제
                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);

                                    //파일 카피
                                    File.Copy(this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString(), strUploadFile, true);
                                    arrAtt.Add(strUploadFile);

                                }
                            }
                        }

                        //추가된 파일들 업로드
                        fileAtt.mfInitSetSystemFileInfo(arrAtt, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                 dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                 dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                 dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                 dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        if (arrAtt.Count != 0)
                        {
                            FileUploadCheck = fileAtt.mfFileUpload_NonAssync();
                            //fileAtt.ShowDialog();
                        }
                        else if (arrAtt.Count == 0)
                        {
                            FileUploadCheck = true;
                        }


                    }
                    else
                    {
                        FileUploadCheck = true;
                    }
                    #endregion

                    //Thread.Sleep(500);

                    //t1 = m_ProgressPopup.mfStartThread();
                    //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    //this.MdiParent.Cursor = Cursors.WaitCursor;

                    #region 완료

                    if (!FileUploadCheck)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001135", "M001037", "M001263",
                                               Infragistics.Win.HAlign.Right);
                        return;
                    }

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        if (uCheckGWTFlag.Checked && DResult == DialogResult.Yes)
                        {
                            #region Brains - ( 그룹웨어 ) 인터페이스
                            //// 테스트 코드 -- 업로드시 주석 처리
                            ////frm.dtSendLine.Rows[0]["CD_USERKEY"] = "tica100"; // 기안자
                            ////frm.dtSendLine.Rows[1]["CD_USERKEY"] = "tica100"; // 결재자

                            dtHeader.Rows[0]["DocCode"] = strRtnDocCode;
                            DataTable dtRtn = clsCertiH.mfEquipGRWApproval(dtHeader, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                            //DataTable dtRtn = mfEquipGRWApproval(dtHeader, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));


                            if (dtRtn == null) // 그룹웨어 실패
                            {

                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000327",
                                                       Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else if (dtRtn.Rows.Count.Equals(0))
                            {
                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000327",
                                                       Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else if (!dtRtn.Rows[0]["CD_CODE"].ToString().Equals("00")) // 그룹웨어 실패
                            {
                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000327",
                                                       Infragistics.Win.HAlign.Right);
                                return;
                            }

                            #endregion
                        }
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001135", "M001037", "M000930",
                                               Infragistics.Win.HAlign.Right);

                        InitValue();
                        InitGrid();
                        if (this.uGroupBoxContentsArea.Expanded == true)
                        {
                            this.uGroupBoxContentsArea.Expanded = false;
                        }
                        mfSearch();
                    }
                    else
                    {
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        string strMessg = "";
                        string strLang = m_resSys.GetString("SYS_LANG");
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMessg = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMessg = ErrRtn.ErrMessage;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                      , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , msg.GetMessge_Text("M001135", strLang)
                                                      , msg.GetMessge_Text("M001037", strLang), strMessg
                                                      , Infragistics.Win.HAlign.Right);
                    }
                    #endregion
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001030", Infragistics.Win.HAlign.Center);

                    return;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        #region 테스트용 사용금지
        ////////////////public DataTable mfEquipGRWApproval(DataTable dtHeader, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        ////////////////{
        ////////////////    QRPBrowser brwChannel = new QRPBrowser();
        ////////////////    brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
        ////////////////    QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        ////////////////    brwChannel.mfCredentials(grw);

        ////////////////    DataTable dtRtn = new DataTable();

        ////////////////    DataTable dtFileInfo = grw.mfSetFileInfoDataTable();

        ////////////////    try
        ////////////////    {
        ////////////////        //Legacykey
        ////////////////        string strPlantCode = dtHeader.Rows[0]["PlantCode"].ToString();
        ////////////////        string strDocCode = dtHeader.Rows[0]["DocCode"].ToString();
        ////////////////        string strLegacyKey = strPlantCode + "||" + strDocCode;
        ////////////////        string strEquipCode = dtHeader.Rows[0]["EquipCode"].ToString();
        ////////////////        string strEquipName = dtHeader.Rows[0]["EquipName"].ToString();
        ////////////////        string strModelName = dtHeader.Rows[0]["ModelName"].ToString();
        ////////////////        string strSerialNo = dtHeader.Rows[0]["SerialNo"].ToString();
        ////////////////        string strVendorCode = dtHeader.Rows[0]["VendorCode"].ToString();
        ////////////////        string strReason = dtHeader.Rows[0]["EquipCertiReqTypeName"].ToString();
        ////////////////        string strPurpose = dtHeader.Rows[0]["Object"].ToString();

        ////////////////        string strFilePath = "EQUEquipCertiFile\\";
        ////////////////        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiFile), "EQUEquipCertiFile");
        ////////////////        QRPEQU.BL.EQUCCS.EQUEquipCertiFile file = new QRPEQU.BL.EQUCCS.EQUEquipCertiFile();
        ////////////////        brwChannel.mfCredentials(file);

        ////////////////        DataTable dtfile = file.mfReadCertiFile(strPlantCode, strDocCode, "KOR");

        ////////////////        #region CreateDataTable

        ////////////////        //FileInfo
        ////////////////        foreach (DataRow dr in dtfile.Rows)
        ////////////////        {
        ////////////////            DataRow _dr = dtFileInfo.NewRow();

        ////////////////            _dr["PlantCode"] = strPlantCode;
        ////////////////            _dr["NM_FILE"] = dr["FileName"].ToString();
        ////////////////            _dr["NM_LOCATION"] = strFilePath;
        ////////////////            if(!_dr["NM_FILE"].ToString().Equals(string.Empty))
        ////////////////                dtFileInfo.Rows.Add(_dr);
        ////////////////        }
        ////////////////        #endregion

        ////////////////        dtRtn = grw.Equip(strLegacyKey, dtSendLine, dtCcLine, strEquipCode, strEquipName, strModelName, strVendorCode,
        ////////////////            strSerialNo, strReason, strPurpose, dtFormInfo, dtFileInfo, strUserIP, strUserID);
        ////////////////    }
        ////////////////    catch (Exception ex)
        ////////////////    {
        ////////////////        return dtRtn;
        ////////////////        throw (ex);
        ////////////////    }


        ////////////////    return dtRtn;
        ////////////////}
        #endregion

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000633", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if(this.uComboPlant.ReadOnly == true)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000763", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if(this.uComboPlant.Value.ToString() == "")
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000643", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {
                    //BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiH CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                    brwChannel.mfCredentials(CertiH);

                    DataTable dtH = CertiH.mfSetDataInfo();
                    DataRow row = dtH.NewRow();
                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                    row["DocCode"] = this.uTextDocCode.Text;

                    dtH.Rows.Add(row);

                    if (dtH.Rows.Count > 0)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000650", "M000675",
                                                Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = CertiH.mfDeleteEquipCertiH(dtH);

                            //Decoding
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                            //End Logic
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            DialogResult DResult = new DialogResult();

                            if (ErrRtn.ErrNum == 0)
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M000638", "M000926",
                                                   Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                InitValue();
                                SetWritable();
                                InitGrid();
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M000638", "M000923",
                                                   Infragistics.Win.HAlign.Right);
                            }
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                InitValue();
                SetWritable();

                QRPBrowser toolButton = new QRPBrowser();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    
                    uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    //uTextInvenCode.Text = "";
                    //uTextInvenName.Text = "";
                    //uTextInvenNameCh.Text = "";
                    //uTextInvenNameEn.Text = "";
                    //uComboFlag.Value = "T";
                    //uComboInvenType.Value = "";
                    //uComboPlant.Value = "";

                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                WinGrid grd = new WinGrid();

                if (this.uGridSysRequest.Rows.Count == 0 && 
                    ((this.uGridCertFile.Rows.Count == 0 && this.uGridCertItem.Rows.Count == 0 && this.uGridCertP.Rows.Count == 0 && this.uGridSetup.Rows.Count == 0 && this.uGridSetupCondition.Rows.Count ==0) 
                    || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                if(this.uGridSysRequest.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridSysRequest);

                if (this.uGridCertFile.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridCertFile);

                if (this.uGridCertItem.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridCertItem);

                if (this.uGridCertP.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridCertP);

                if (this.uGridSetup.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridSetup);

                if (this.uGridSetupCondition.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridSetupCondition);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            
        }
        #endregion

        #region 이벤트

        //펼치거나 닫을 때 일어나는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridSysRequest.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridSysRequest.Height = 720;
                    for (int i = 0; i < uGridSysRequest.Rows.Count; i++)
                    {
                        uGridSysRequest.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 그리드 셀수정시 편집이미지이벤트

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridSysRequest_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridSysRequest, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridSetup, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridCertItem, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridCertFile, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid4_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridCertP, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }
        private void uGridSetup_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridSetup, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }

        private void uGridMoldTool_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridMoldTool, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }

        #endregion

        #region 그리드 이벤트
        private void uGridMoldTool_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0007 frmMold = new frmPOP0007();
                    frmMold.PlantCode = uComboPlant.Value.ToString();
                    frmMold.ShowDialog();

                    this.uGridMoldTool.ActiveRow.Cells["DurableMatCode"].Value = frmMold.DurableMatCode;
                    this.uGridMoldTool.ActiveRow.Cells["DurableMatName"].Value = frmMold.DurableMatName;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 헤더 더블클릭시 헤더상세,상세정보를 조회하여 DisPlay함
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridSysRequest_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItem), "EQUEquipCertiItem");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupCondition), "EQUSetupCondition");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupDurableMat), "EQUSetupDurableMat");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipSetup), "EQUEquipSetup");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiP), "EQUEquipCertiP");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiFile), "EQUEquipCertiFile");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC), "EQUEquipCertiItemQC");

                QRPEQU.BL.EQUCCS.EQUEquipCertiH Header = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                QRPEQU.BL.EQUCCS.EQUEquipCertiItem Item = new QRPEQU.BL.EQUCCS.EQUEquipCertiItem();
                QRPEQU.BL.EQUCCS.EQUSetupCondition Condition = new QRPEQU.BL.EQUCCS.EQUSetupCondition();
                QRPEQU.BL.EQUCCS.EQUSetupDurableMat DMat = new QRPEQU.BL.EQUCCS.EQUSetupDurableMat();
                QRPEQU.BL.EQUCCS.EQUEquipSetup Setup = new QRPEQU.BL.EQUCCS.EQUEquipSetup();
                QRPEQU.BL.EQUCCS.EQUEquipCertiP CertiP = new QRPEQU.BL.EQUCCS.EQUEquipCertiP();
                QRPEQU.BL.EQUCCS.EQUEquipCertiFile CFile = new QRPEQU.BL.EQUCCS.EQUEquipCertiFile();
                QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC ItemQc = new QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC();

                brwChannel.mfCredentials(Header);
                brwChannel.mfCredentials(Item);
                brwChannel.mfCredentials(Condition);
                brwChannel.mfCredentials(DMat);
                brwChannel.mfCredentials(Setup);
                brwChannel.mfCredentials(CertiP);
                brwChannel.mfCredentials(CFile);
                brwChannel.mfCredentials(ItemQc);

                DataTable dtHeader = Header.mfReadEquipCertiH_Detail(e.Row.Cells["PlantCode"].Value.ToString(), e.Row.Cells["DocCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                this.uComboPlant.Value = dtHeader.Rows[0]["PlantCode"].ToString(); 
                this.uTextDocCode.Text = dtHeader.Rows[0]["DocCode"].ToString(); 
                this.uTextEquipCode.Text = dtHeader.Rows[0]["EquipCode"].ToString(); 
                this.uTextEquipName.Text = dtHeader.Rows[0]["EquipName"].ToString(); 
                this.uComboPKG.Value = dtHeader.Rows[0]["Package"].ToString();
                this.uComboProcess.Value = dtHeader.Rows[0]["ProcessCode"].ToString(); 
                this.uTextSubject.Text = dtHeader.Rows[0]["Subject"].ToString(); 
                this.uComboSetupDeptCode.Text = dtHeader.Rows[0]["SetupDeptCode"].ToString(); 
                this.uDateSetupCompleteDate.Value = dtHeader.Rows[0]["SetupFinishDate"].ToString(); 
                this.uTextSetupChargeID.Text = dtHeader.Rows[0]["SetupChargeID"].ToString(); 
                this.uTextSetupChargeName.Text = dtHeader.Rows[0]["SetupChargeName"].ToString(); 
                this.uTextObject.Text = dtHeader.Rows[0]["Object"].ToString(); 
                this.uComboUseDeptCode.Value = dtHeader.Rows[0]["UseDeptCode"].ToString(); 
                this.uDateCeriReqDate.Value = dtHeader.Rows[0]["CertiReqDate"].ToString(); 
                this.uTextCertiReqID.Text = dtHeader.Rows[0]["CertiReqID"].ToString(); 
                this.uTextCertiReqName.Text = dtHeader.Rows[0]["CertiReqName"].ToString(); 
                this.uTextAdmitStatusCode.Text = dtHeader.Rows[0]["AdmitStatusCode"].ToString(); 
                this.uTextCertiResultCode.Text = dtHeader.Rows[0]["CertiResultCode"].ToString(); 
                this.uComboEquipCertiReqTypeCode.Value = dtHeader.Rows[0]["EquipCertiReqTypeCode"].ToString(); 
                this.uTextModelName.Text = dtHeader.Rows[0]["ModelName"].ToString();
                this.uTextSerialNo.Text = dtHeader.Rows[0]["SerialNo"].ToString();
                this.uTextVendorCode.Text = dtHeader.Rows[0]["VendorCode"].ToString();


                this.uCheckEquipStandardFlag.Checked = dtHeader.Rows[0]["EquipStandardFlag"].ToString().Equals("T") ? true : false;

                
                this.uCheckWorkStandardFlag.Checked = dtHeader.Rows[0]["WorkStandardFlag"].ToString().Equals("T") ? true : false;

                this.uCheckTechStandardFlag.Checked = dtHeader.Rows[0]["TechStandardFlag"].ToString().Equals("T") ? true : false;

                this.uComboMTReasonCode.Value = dtHeader.Rows[0]["MTReasonCode"].ToString(); 
                this.uComboEquipReasonCode.Value = dtHeader.Rows[0]["EquipReasonCode"].ToString(); 
                this.uTextMTReasonEtcDesc.Text = dtHeader.Rows[0]["MTReasonEtcDesc"].ToString(); 
                this.uTextEquipReasonEtcDesc.Text = dtHeader.Rows[0]["EquipReasonEtcDesc"].ToString(); 
                this.uComboOriginDeptCode.Value = dtHeader.Rows[0]["OriginDeptCode"].ToString(); 
                this.uComboMoveDeptCode.Value = dtHeader.Rows[0]["MoveDeptCode"].ToString(); 
                //this.richTextEditor2.SetDocumentText(dtHeader.Rows[0]["IncDecEtcDesc"].ToString());
                this.uTextEtcDesc.Text = dtHeader.Rows[0]["EtcDesc"].ToString();
                this.uCheckGWFlag.Tag = dtHeader.Rows[0]["AdmitStatus"];

                if (dtHeader.Rows[0]["AdmitStatus"].ToString().Equals("AR") || dtHeader.Rows[0]["AdmitStatus"].ToString().Equals("FN"))
                {
                    SetDiswritable();
                    uCheckGWTFlag.Checked = true;
                }
                else
                {
                    SetWritable();
                    uCheckGWTFlag.Checked = false;
                }

                if (dtHeader.Rows[0]["AdmitStatus"].ToString().Equals("RE"))
                    uCheckGWFlag.Checked = false;
                else
                    uCheckGWFlag.Checked = dtHeader.Rows[0]["GWTFlag"].ToString().Equals("T") ? true : false;

                uCheckGWResultState.Checked = dtHeader.Rows[0]["GWResultState"].ToString().Equals("T") ? true : false;

                this.uGroupBoxContentsArea.Expanded = true;

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDocCode = this.uTextDocCode.Text;
                ///////////설비인증항목 및 평가결과(CertiItem)
                //Search
                DataTable dtItem = Item.mfReadCertiItem(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                //DataBinding
                this.uGridCertItem.DataSource = dtItem;
                this.uGridCertItem.DataBind();

                //평가결과의 공백을 제거
                for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                {
                    String strTrim = this.uGridCertItem.Rows[i].Cells["EvalResultCode"].Value.ToString().Trim();

                    this.uGridCertItem.Rows[i].Cells["EvalResultCode"].Value = strTrim;
                }

                if (dtHeader.Rows.Count != 0)
                {
                    string strEquipCode = dtHeader.Rows[0]["EquipCode"].ToString();
                    DataTable dtSubEquip = Header.mfReadEQUEquipCertiSubEquip(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    this.uGridSubSEquip.DataSource = dtSubEquip;
                    this.uGridSubSEquip.DataBind();

                    if (this.uGridSubSEquip.Rows.Count != 0)
                    {

                    }
                }

                //////////가동조건(SetupCondition)
                DataTable dtCondition = Condition.mfReadSetupCondition(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                //DataBinding
                this.uGridSetupCondition.DataSource = dtCondition;
                this.uGridSetupCondition.DataBind();

                ///////금형치공구현황(DMat)
                DataTable dtDmat = DMat.mfReadDurableMat(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                this.uGridMoldTool.DataSource = dtDmat;
                this.uGridMoldTool.DataBind();

                /////////설비 Set현황(Setup)
                DataTable dtSetup = Setup.mfReadEquipSetup(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                this.uGridSetup.DataSource = dtSetup;
                this.uGridSetup.DataBind();

                ////////표준 담당자(CertiP)
                DataTable dtCertiP = CertiP.mfReadCertiP(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                this.uGridCertP.DataSource = dtCertiP;
                this.uGridCertP.DataBind();

                ////////첨부파일(CFile)
                DataTable dtCFile = CFile.mfReadCertiFile(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                this.uGridCertFile.DataSource = dtCFile;
                this.uGridCertFile.DataBind();
                
                //검사항목
                DataTable dtItemQc = ItemQc.mfReadCertiItemQC(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));

                this.uGrid5.DataSource = dtItemQc;
                this.uGrid5.DataBind();

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSysRequest.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001097", "M001102", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uGroupBoxContentsArea.Expanded == false)
                    this.uGroupBoxContentsArea.Expanded = true;

                ultraTabControl1.Tabs[0].Selected = true;
                QRPBrowser toolButton = new QRPBrowser();

                if (uCheckGWFlag.Checked)
                {
                    toolButton.mfActiveToolBar(this.ParentForm, true, false, true, true, false, true);
                }
                else
                {
                    toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true);
                }

                if (dtHeader.Rows[0]["AdmitStatus"].ToString().Equals("RE"))
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000330", "M000414", Infragistics.Win.HAlign.Right);

                if (this.uCheckGWTFlag.Checked && !uCheckGWFlag.Checked)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000330", "M000329", Infragistics.Win.HAlign.Right);
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
        }

        private void uGrid3_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "AllFile (*.*)|*.*"; //excel files (*.xlsx)|*.xlsx";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFile = openFile.FileName;
                        this.uGridCertFile.ActiveRow.Cells["FileName"].Value = strFile;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridMoldTool_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (e.KeyCode == Keys.Enter)
                {


                    //uGridMoldTool.Rows[uGridMoldTool.ActiveRow.Index].Cells["DurableMatCode"]
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupDurableMat), "EQUSetupDurableMat");
                    QRPEQU.BL.EQUCCS.EQUSetupDurableMat DMat = new QRPEQU.BL.EQUCCS.EQUSetupDurableMat();
                    brwChannel.mfCredentials(DMat);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strDurableMatCode = uGridMoldTool.ActiveRow.Cells["DurableMatCode"].Value.ToString();

                    DataTable dtDMat = DMat.mfReadDurable(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                    DialogResult DResult = new DialogResult();
                    // 조회 결과가 없을시 메세지창 띄움
                    WinMessageBox msg = new WinMessageBox();
                    if (dtDMat.Rows.Count == 0)
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        uGridMoldTool.Rows[uGridMoldTool.ActiveRow.Index].Cells["DurableMatName"].Value = dtDMat.Rows[0]["DurableMatName"].ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid4_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = uComboPlant.Value.ToString();
                    frmUser.ShowDialog();

                    this.uGridCertP.ActiveRow.Cells["StandardChargeID"].Value = frmUser.UserID;
                    this.uGridCertP.ActiveRow.Cells["StandardChargeName"].Value = frmUser.UserName;
                    this.uGridCertP.ActiveRow.Cells["Email"].Value = frmUser.EMail;
                    this.uGridCertP.ActiveRow.Cells["DeptName"].Value = frmUser.DeptName;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 셀 더블클릭시 파일 다운로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridCertFile_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (e.Cell.Column.Key.Equals("FileName"))
                {
                    if (e.Cell.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M001149",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (e.Cell.Value.ToString().Contains(":\\"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0003");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        arrFile.Add(e.Cell.Value.ToString());
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.ShowDialog();
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        #endregion

        #region TextEvent

        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0005 frmEquip = new frmPOP0005();
                    frmEquip.PlantCode = uComboPlant.Value.ToString();
                    frmEquip.ShowDialog();

                    this.uTextEquipCode.Text = frmEquip.EquipCode;
                    uTextModelName.Text = frmEquip.ModelName;
                    uTextSerialNo.Text = frmEquip.SerialNo;
                    uTextVendorCode.Text = frmEquip.VendorCode;
                    this.uTextEquipName.Text = frmEquip.EquipName;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiH clsH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                    brwChannel.mfCredentials(clsH);

                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strEquipCode = frmEquip.EquipCode;

                    DataTable dtSubEquip = clsH.mfReadEQUEquipCertiSubEquip(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    this.uGridSubSEquip.DataSource = dtSubEquip;
                    this.uGridSubSEquip.DataBind();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSetupChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = uComboPlant.Value.ToString();
                    frmUser.ShowDialog();

                    this.uTextSetupChargeID.Text = frmUser.UserID;
                    this.uTextSetupChargeName.Text = frmUser.UserName;
                    this.uComboSetupDeptCode.Value = frmUser.DeptCode;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 인증의뢰자 팝업창조회 의뢰자가 해당하는 부서 -> 사용부서에 삽입
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextCertiReqID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266.",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = uComboPlant.Value.ToString();
                    frmUser.ShowDialog();

                    this.uTextCertiReqID.Text = frmUser.UserID;
                    this.uTextCertiReqName.Text = frmUser.UserName;
                    this.uComboUseDeptCode.Value = frmUser.DeptCode;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비코드변동시 설비정보클리어
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextEquipName.Text.Equals(string.Empty))
                {
                    this.uTextEquipName.Text = "";
                    this.uTextModelName.Text = "";
                    this.uTextSerialNo.Text = "";
                    this.uTextVendorCode.Text = "";
                    if (this.uGridSubSEquip.Rows.Count > 0)
                    {
                        while (this.uGridSubSEquip.Rows.Count > 0)
                        {
                            this.uGridSubSEquip.Rows[0].Delete(false);
                        }
                    }

                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비코드 직접입력조회 및 부속설비조회, 바인드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData.Equals(Keys.Enter))
                {
                    if (this.uTextEquipCode.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult DResult = new DialogResult();

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEqu = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEqu);

                        String strPlantCode = this.uComboPlant.Value.ToString();
                        String strEquipCode = this.uTextEquipCode.Text;
                        string strLang = m_resSys.GetString("SYS_LANG");

                        DataTable dtEquipDiscard = clsEqu.mfReadEquipInfoDetail(strPlantCode, strEquipCode, strLang);
                        if (dtEquipDiscard.Rows.Count > 0)
                        {
                            if (dtEquipDiscard.Rows[0]["DiscardFlag"].ToString().Equals("T"))
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information,m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                                                    , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000199", strLang), this.uTextEquipCode.Text + msg.GetMessge_Text("M000353", strLang), Infragistics.Win.HAlign.Right);

                                this.uTextEquipName.Text = "";
                                this.uTextModelName.Text = "";
                                this.uTextSerialNo.Text = "";
                                this.uTextVendorCode.Text = "";
                                return;
                            }
                            this.uTextEquipName.Text = dtEquipDiscard.Rows[0]["EquipName"].ToString() ;
                            this.uTextModelName.Text = dtEquipDiscard.Rows[0]["ModelName"].ToString();
                            this.uTextSerialNo.Text = dtEquipDiscard.Rows[0]["SerialNo"].ToString();
                            this.uTextVendorCode.Text = dtEquipDiscard.Rows[0]["VendorCode"].ToString();

                            
                            brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                            QRPEQU.BL.EQUCCS.EQUEquipCertiH clsH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                            brwChannel.mfCredentials(clsH);

                            DataTable dtSubEquip = clsH.mfReadEQUEquipCertiSubEquip(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                            this.uGridSubSEquip.DataSource = dtSubEquip;
                            this.uGridSubSEquip.DataBind();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                                                    , "M001135", "M000199", "M000897", Infragistics.Win.HAlign.Right);

                            this.uTextEquipName.Clear();
                            this.uTextModelName.Clear();
                            this.uTextSerialNo.Clear();
                            this.uTextVendorCode.Clear();
                            return;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Setup담당자 직접입력조회, 담당자가 해당하는 부서 -> Setup부서에 삽입
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSetupChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (!this.uTextSetupChargeID.Text.Equals(string.Empty))
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        string strUserID = this.uTextSetupChargeID.Text;

                        DataTable dtUserInfo = GetUserInfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUserInfo.Rows.Count > 0)
                        {
                            this.uComboSetupDeptCode.Value = dtUserInfo.Rows[0]["DeptCode"].ToString();
                            this.uTextSetupChargeID.Text = dtUserInfo.Rows[0]["UserID"].ToString();
                            this.uTextSetupChargeName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
                else if(e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextSetupChargeName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 인증의뢰자 직업입력조회, 의뢰자가 해당하는 부서 -> 사용부서에 삽입
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextCertiReqID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                   
                    if (!this.uTextSetupChargeID.Text.Equals(string.Empty))
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        string strUserID = this.uTextCertiReqID.Text;

                        DataTable dtUserInfo = GetUserInfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUserInfo.Rows.Count > 0)
                        {
                            this.uComboUseDeptCode.Value = dtUserInfo.Rows[0]["DeptCode"].ToString();
                            this.uTextCertiReqID.Text = dtUserInfo.Rows[0]["UserID"].ToString();
                            this.uTextCertiReqName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextCertiReqName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 행삭제 이벤트

        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    for (int i = 0; i < this.uGridSetupCondition.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridSetupCondition.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridSetupCondition.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow3_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    for (int i = 0; i < this.uGridCertItem.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCertItem.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridCertItem.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow4_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCertFile.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridCertFile.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow5_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    for (int i = 0; i < this.uGridCertP.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCertP.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridCertP.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    for (int i = 0; i < this.uGridSetup.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridSetup.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridSetup.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlant.ReadOnly == false)
                {
                    for (int i = 0; i < this.uGridMoldTool.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridMoldTool.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridMoldTool.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinComboEditor wComboLType = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                DataTable dtDeptCode = new DataTable();
                DataTable dtPackage = new DataTable();

                String strPlantCode = this.uComboPlant.Value.ToString();

                this.uComboProcess.Items.Clear();
                this.uComboSetupDeptCode.Items.Clear();
                this.uComboUseDeptCode.Items.Clear();
                this.uComboPKG.Items.Clear();

                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;

                strPlantCode = this.uComboPlant.Value.ToString();


                // Bl 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);
                dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept deptCode = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(deptCode);
                dtDeptCode = deptCode.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                
                wCombo.mfSetComboEditor(this.uComboProcess, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);

                wCombo.mfSetComboEditor(this.uComboSetupDeptCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "DeptCode", "DeptName", dtDeptCode);

                wCombo.mfSetComboEditor(this.uComboUseDeptCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "DeptCode", "DeptName", dtDeptCode);

                wCombo.mfSetComboEditor(this.uComboOriginDeptCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "DeptCode", "DeptName", dtDeptCode);

                wCombo.mfSetComboEditor(this.uComboMoveDeptCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "DeptCode", "DeptName", dtDeptCode);

                dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPKG, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false,"", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "Package", "ComboName", dtPackage);

                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonDownload_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0003");


                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridCertFile.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(uGridCertFile.Rows[i].Cells["Check"].Value) == true)
                        {
                            if (this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                            {
                                arrFile.Add(this.uGridCertFile.Rows[i].Cells["FileName"].Value.ToString());
                            }
                        }
                    }
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }


        #endregion

        /// <summary>
        /// ActiveReport를 그린다.
        /// </summary>
        /// <param name="dtCertiH">설비인증헤더</param>
        /// <param name="dtSetup">Setup정보</param>
        /// <param name="dtSetupCondition">가동 조건</param>
        /// <param name="dtCertiItem">설비 인증 항목 및 평가 결과</param>
        private void ActiveReport(DataTable dtCertiH,DataTable dtSetup,DataTable dtSetupCondition,DataTable dtCertiItem)
        {
            frmEQUZ0010_Report rptExport = null;
            try
            {

                //리포트 폼으로 필요정보를 보내준다.
                rptExport = new frmEQUZ0010_Report(dtCertiH, dtSetup, dtSetupCondition, dtCertiItem,SubEquip());
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                rptExport.Dispose();
            }
        }

        /// <summary>
        /// 부속설비정보 담기
        /// </summary>
        /// <returns></returns>
        private DataTable SubEquip()
        {
            DataTable dtSubEquip = new DataTable();

            try
            {

                dtSubEquip.Columns.Add("EquipCode", typeof(string));
                dtSubEquip.Columns.Add("EquipName", typeof(string));
                dtSubEquip.Columns.Add("ModelName", typeof(string));
                dtSubEquip.Columns.Add("VendorCode", typeof(string));
                dtSubEquip.Columns.Add("SerialNo", typeof(string));
                dtSubEquip.Columns.Add("Package", typeof(string));
                dtSubEquip.Columns.Add("ReProcName", typeof(string)); 

                //리포트 작성을 위한 설비 작업 기술 표준 컬럼 설정
                dtSubEquip.Columns.Add("EquipStandard", typeof(string));
                dtSubEquip.Columns.Add("WorkStandard", typeof(string));
                dtSubEquip.Columns.Add("TechStandard", typeof(string));
                dtSubEquip.Columns.Add("Object", typeof(string));
                dtSubEquip.Columns.Add("EtcDesc", typeof(string)); //비고

                //Main 설비 저장
                DataRow drRow = dtSubEquip.NewRow();

                drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["EquipName"] = this.uTextEquipName.Text;
                drRow["ModelName"] = uTextModelName.Text;
                drRow["SerialNo"] = uTextSerialNo.Text;
                drRow["VendorCode"] = uTextVendorCode.Text;
                drRow["Package"] = this.uComboPKG.Value == null ? "" : this.uComboPKG.Value;
                drRow["ReProcName"] = this.uComboProcess.Text;

                if (this.uCheckEquipStandardFlag.Checked == true)
                    drRow["EquipStandard"] = "O";

                if (this.uCheckWorkStandardFlag.Checked == true)
                    drRow["WorkStandard"] = "O";

                if (this.uCheckTechStandardFlag.Checked == true)
                    drRow["TechStandard"] = "O";

                drRow["Object"] = this.uTextObject.Text;
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                dtSubEquip.Rows.Add(drRow);

                // 서브설비저장
                for (int i = 0; i < this.uGridSubSEquip.Rows.Count; i++)
                {
                    if (!this.uGridSubSEquip.Rows[i].GetCellValue("EquipCode").ToString().Equals(this.uTextEquipCode.Text))
                    {
                        drRow = dtSubEquip.NewRow();

                        drRow["EquipCode"] = this.uGridSubSEquip.Rows[i].GetCellValue("EquipCode");
                        drRow["EquipName"] = this.uGridSubSEquip.Rows[i].GetCellValue("EquipName");
                        drRow["ModelName"] = this.uGridSubSEquip.Rows[i].GetCellValue("ModelName");
                        drRow["SerialNo"] = this.uGridSubSEquip.Rows[i].GetCellValue("SerialNo");
                        drRow["VendorCode"] = this.uGridSubSEquip.Rows[i].GetCellValue("VendorCode");
                        drRow["Package"] = this.uComboPKG.Value == null ? "" : this.uComboPKG.Value;
                        drRow["ReProcName"] = this.uComboProcess.Text;
                        drRow["EtcDesc"] = this.uTextEtcDesc.Text;

                        dtSubEquip.Rows.Add(drRow);
                    }

                }

                return dtSubEquip;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSubEquip;
            }
            finally
            {
                dtSubEquip.Dispose();
            }
        }

        private void uGridSetupCondition_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            try
            {
 
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// PACKAGE 선택시 공정검사규격서의 검사항목 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboPKG_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (!this.uComboProcess.Value.ToString().Equals(string.Empty)
                    && !this.uComboPlant.Value.ToString().Equals(string.Empty)
                    && !this.uComboPKG.Value.ToString().Equals(string.Empty))
                {
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strProcessCode = this.uComboProcess.Value.ToString();
                    string strPackage = this.uComboPKG.Value.ToString();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiH clsH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                    brwChannel.mfCredentials(clsH);

                    DataTable dtSpecD = clsH.mfReadISOProcInspectD(strPlantCode, strPackage, strProcessCode, m_resSys.GetString("SYS_LANG"));

                    this.uGrid5.DataSource = dtSpecD;
                    this.uGrid5.DataBind();
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void frmEQUZ0002_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}



