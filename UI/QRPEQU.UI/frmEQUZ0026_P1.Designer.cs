﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0026_P1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0026_P1));
            this.uGridPMList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTaget = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTaget = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAll = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPMAll = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWait = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPMWait = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uTextToDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkerID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTaget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkerID)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridPMList
            // 
            this.uGridPMList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMList.DisplayLayout.Appearance = appearance1;
            this.uGridPMList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPMList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPMList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMList.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMList.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPMList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMList.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPMList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPMList.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPMList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPMList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMList.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPMList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMList.Location = new System.Drawing.Point(0, 60);
            this.uGridPMList.Name = "uGridPMList";
            this.uGridPMList.Size = new System.Drawing.Size(677, 312);
            this.uGridPMList.TabIndex = 0;
            this.uGridPMList.Text = "ultraGrid1";
            this.uGridPMList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridPMList_DoubleClickCell);
            // 
            // uLabelDate
            // 
            this.uLabelDate.Location = new System.Drawing.Point(228, 12);
            this.uLabelDate.Name = "uLabelDate";
            this.uLabelDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelDate.TabIndex = 1;
            this.uLabelDate.Text = "ultraLabel1";
            // 
            // uTextDate
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDate.Appearance = appearance18;
            this.uTextDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDate.Location = new System.Drawing.Point(332, 12);
            this.uTextDate.Name = "uTextDate";
            this.uTextDate.ReadOnly = true;
            this.uTextDate.Size = new System.Drawing.Size(85, 21);
            this.uTextDate.TabIndex = 2;
            // 
            // uLabelTaget
            // 
            this.uLabelTaget.Location = new System.Drawing.Point(12, 12);
            this.uLabelTaget.Name = "uLabelTaget";
            this.uLabelTaget.Size = new System.Drawing.Size(100, 20);
            this.uLabelTaget.TabIndex = 1;
            this.uLabelTaget.Text = "ultraLabel1";
            // 
            // uTextTaget
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTaget.Appearance = appearance15;
            this.uTextTaget.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTaget.Location = new System.Drawing.Point(116, 12);
            this.uTextTaget.Name = "uTextTaget";
            this.uTextTaget.ReadOnly = true;
            this.uTextTaget.Size = new System.Drawing.Size(100, 21);
            this.uTextTaget.TabIndex = 2;
            // 
            // uLabelAll
            // 
            this.uLabelAll.Location = new System.Drawing.Point(12, 36);
            this.uLabelAll.Name = "uLabelAll";
            this.uLabelAll.Size = new System.Drawing.Size(100, 20);
            this.uLabelAll.TabIndex = 1;
            this.uLabelAll.Text = "ultraLabel1";
            // 
            // uTextPMAll
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMAll.Appearance = appearance19;
            this.uTextPMAll.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMAll.Location = new System.Drawing.Point(116, 36);
            this.uTextPMAll.Name = "uTextPMAll";
            this.uTextPMAll.ReadOnly = true;
            this.uTextPMAll.Size = new System.Drawing.Size(100, 21);
            this.uTextPMAll.TabIndex = 2;
            // 
            // uLabelWait
            // 
            this.uLabelWait.Location = new System.Drawing.Point(228, 36);
            this.uLabelWait.Name = "uLabelWait";
            this.uLabelWait.Size = new System.Drawing.Size(100, 20);
            this.uLabelWait.TabIndex = 1;
            this.uLabelWait.Text = "ultraLabel1";
            // 
            // uTextPMWait
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMWait.Appearance = appearance17;
            this.uTextPMWait.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMWait.Location = new System.Drawing.Point(332, 36);
            this.uTextPMWait.Name = "uTextPMWait";
            this.uTextPMWait.ReadOnly = true;
            this.uTextPMWait.Size = new System.Drawing.Size(100, 21);
            this.uTextPMWait.TabIndex = 2;
            // 
            // uLabelPer
            // 
            this.uLabelPer.Location = new System.Drawing.Point(444, 36);
            this.uLabelPer.Name = "uLabelPer";
            this.uLabelPer.Size = new System.Drawing.Size(100, 20);
            this.uLabelPer.TabIndex = 1;
            this.uLabelPer.Text = "ultraLabel1";
            // 
            // uTextPer
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPer.Appearance = appearance13;
            this.uTextPer.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPer.Location = new System.Drawing.Point(548, 36);
            this.uTextPer.Name = "uTextPer";
            this.uTextPer.ReadOnly = true;
            this.uTextPer.Size = new System.Drawing.Size(100, 21);
            this.uTextPer.TabIndex = 2;
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(584, 376);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 3;
            this.uButtonOK.Text = "ultraButton1";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uTextToDate
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextToDate.Appearance = appearance14;
            this.uTextToDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextToDate.Location = new System.Drawing.Point(436, 12);
            this.uTextToDate.Name = "uTextToDate";
            this.uTextToDate.ReadOnly = true;
            this.uTextToDate.Size = new System.Drawing.Size(85, 21);
            this.uTextToDate.TabIndex = 2;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(416, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "~";
            // 
            // uLabelUserID
            // 
            this.uLabelUserID.Location = new System.Drawing.Point(12, 380);
            this.uLabelUserID.Name = "uLabelUserID";
            this.uLabelUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelUserID.TabIndex = 1;
            this.uLabelUserID.Text = "ultraLabel1";
            // 
            // uTextWorkerID
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkerID.Appearance = appearance16;
            this.uTextWorkerID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkerID.Location = new System.Drawing.Point(116, 380);
            this.uTextWorkerID.Name = "uTextWorkerID";
            this.uTextWorkerID.ReadOnly = true;
            this.uTextWorkerID.Size = new System.Drawing.Size(404, 21);
            this.uTextWorkerID.TabIndex = 2;
            // 
            // frmEQUZ0026_P1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(680, 407);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uTextPer);
            this.Controls.Add(this.uTextPMWait);
            this.Controls.Add(this.uTextWorkerID);
            this.Controls.Add(this.uTextPMAll);
            this.Controls.Add(this.uTextTaget);
            this.Controls.Add(this.uTextToDate);
            this.Controls.Add(this.uTextDate);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.uLabelPer);
            this.Controls.Add(this.uLabelWait);
            this.Controls.Add(this.uLabelUserID);
            this.Controls.Add(this.uLabelAll);
            this.Controls.Add(this.uLabelTaget);
            this.Controls.Add(this.uLabelDate);
            this.Controls.Add(this.uGridPMList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0026_P1";
            this.Text = "PM상세조회";
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTaget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkerID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMList;
        private Infragistics.Win.Misc.UltraLabel uLabelDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDate;
        private Infragistics.Win.Misc.UltraLabel uLabelTaget;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTaget;
        private Infragistics.Win.Misc.UltraLabel uLabelAll;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMAll;
        private Infragistics.Win.Misc.UltraLabel uLabelWait;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMWait;
        private Infragistics.Win.Misc.UltraLabel uLabelPer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPer;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabelUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkerID;
    }
}