﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmEQUZ0002.cs                                        */
/* 프로그램명   : 설비인증의뢰요청리포트                                */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-23                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.VisualBasic;
using System.Collections;
using System.IO;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0010_Report : Form
    {
        
        public frmEQUZ0010_Report()
        {
            InitializeComponent();
        }

        public frmEQUZ0010_Report(DataTable dtCertiH, DataTable dtSetup, DataTable dtSetupCondition, DataTable dtCertiItem,DataTable dtMainSubEquip)
        {
            
            InitializeComponent();

            InitReport(dtCertiH, dtSetup, dtSetupCondition, dtCertiItem, dtMainSubEquip);
        }

        private void InitReport(DataTable dtCertiH, DataTable dtSetup, DataTable dtSetupCondition, DataTable dtCertiItem, DataTable dtMainSubEquip)
        {
            try
            {
                #region Viewer 초기화
                rptViewDoc.Width = this.Width;
                rptViewDoc.Height = this.Height;
                rptViewDoc.Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                #endregion

                #region 리포트 그리기

                //설비인증요청 헤더 정보조회 매서드 실행

                DataTable dtCondi = dtSetupCondition.Copy();

                //Setup보고서로 리포트 그리기
                rptEQUZ0010_Main rptSetUpMain = new rptEQUZ0010_Main(dtSetup, dtSetupCondition, dtCertiItem, dtMainSubEquip);
                rptSetUpMain.DataSource = dtCertiH;

                rptSetUpMain.Run();
                rptViewDoc.Document = rptSetUpMain.Document;

                SaveReport_Export("S");

                //뷰어에있는 Document 초기화
                rptViewDoc.Document = null;

                dtCertiItem.Clear(); //설비인증의뢰 / 통보서의 인증항목은 품질에서 입력하는 사항이라 클리어하여 컬럼만보냄

                //설비인증요청보고서 그리기
                rptEQUZ0002_Main rptEquipCerti = new rptEQUZ0002_Main(dtCondi, dtCertiItem, dtMainSubEquip);
                rptEquipCerti.DataSource = dtCertiH;

                rptEquipCerti.Run();
                rptViewDoc.Document = rptEquipCerti.Document;

                SaveReport_Export("E");

                #endregion

            }
            catch (Exception ex)
            {
            }
            finally
            { }
        }


       

        /// <summary>
        /// 리포트 PDF로 변환하여 지정된 경로에 저장시킨다.
        /// </summary>
        private void SaveReport_Export(string strGubun)
        {
            try
            {
                //
                object export = null;
                export = new PdfExport();

                //Report디렉토리지정
                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";

                //디렉토리 유무확인
                DirectoryInfo di = new DirectoryInfo(m_strQRPBrowserPath);

                //지정된경로에 폴더가 없을 경우 폴더생성
                if (di.Exists.Equals(false))
                {
                    di.Create();
                }

                if (strGubun.Equals("S"))
                {
                    //파일여부 체크 존재한다면 
                    if (File.Exists(m_strQRPBrowserPath + "Setup.pdf"))
                    {
                        File.Delete(m_strQRPBrowserPath + "Setup.pdf");
                    }
                    //((PdfExport)export).Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Setup.pdf");

                    PdfExport pd = new PdfExport();
                    pd.Security.Permissions = PdfPermissions.AllowModifyAnnotations;
                    pd.Security.Permissions = PdfPermissions.AllowAssembly;
                    pd.Security.Permissions = PdfPermissions.AllowCopy;
                    pd.Security.Permissions = PdfPermissions.AllowModifyContents;
                    pd.Security.Permissions = PdfPermissions.AllowAccessibleReaders;
                    
                    pd.Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Setup.pdf");
                    pd.Dispose();

                }
                else
                {
                    //파일여부 체크 존재한다면 
                    if (File.Exists(m_strQRPBrowserPath  + "Req.pdf"))
                    {
                        File.Delete(m_strQRPBrowserPath  + "Req.pdf");
                    }
                    //((PdfExport)export).Export(this.rptViewDoc.Document, m_strQRPBrowserPath  + "Req.pdf");

                    PdfExport pd = new PdfExport();
                    pd.Security.Permissions = PdfPermissions.AllowModifyAnnotations;
                    pd.Security.Permissions = PdfPermissions.AllowAssembly;
                    pd.Security.Permissions = PdfPermissions.AllowCopy;
                    pd.Security.Permissions = PdfPermissions.AllowModifyContents;
                    pd.Security.Permissions = PdfPermissions.AllowAccessibleReaders;
                    pd.Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Req.pdf");
                    pd.Dispose();
                }

                //엑셀변환
                //export = new XlsExport();
                //((XlsExport)export).Export(this.rptViewDoc.Document, "C:\\Users\\Jong\\Desktop\\버림\\넴.xls");

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }


        //private void InitReport(string strPlantCode,string strDocCode, string strLang)
        //{
        //    try
        //    {
        //        #region Viewer 초기화
        //        rptViewDoc.Width = this.Width;
        //        rptViewDoc.Height = this.Height;
        //        rptViewDoc.Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        //        #endregion

        //        #region 리포트 그리기
        //        //설비인증요청 정보 BL호출
        //        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
        //        QRPEQU.BL.EQUCCS.EQUEquipCertiH clsCertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
        //        brwChannel.mfCredentials(clsCertiH);

        //        //설비인증요청 헤더 정보조회 매서드 실행
        //        DataTable dtCertiH = clsCertiH.mfReadEquipCertiH_Detail(strPlantCode, strDocCode, strLang);

        //        //Setup보고서로 리포트 그리기
        //        rptEQUZ0010_Main rptSetUpMain = new rptEQUZ0010_Main();
        //        rptSetUpMain.DataSource = dtCertiH;

        //        rptSetUpMain.Run();
        //        rptViewDoc.Document = rptSetUpMain.Document;

        //        SaveReport_Export("S", strDocCode);

        //        //뷰어에있는 Document 초기화
        //        rptViewDoc.Document = null;

        //        //설비인증요청보고서 그리기
        //        rptEQUZ0002_Main rptEquipCerti = new rptEQUZ0002_Main();
        //        rptEquipCerti.DataSource = dtCertiH;

        //        rptEquipCerti.Run();
        //        rptViewDoc.Document = rptEquipCerti.Document;

        //        SaveReport_Export("E", strDocCode);

        //        #endregion

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    { }
        //}

    }
}
