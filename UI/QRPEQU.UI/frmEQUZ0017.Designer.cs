﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0017));
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextChgChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextChgChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelChgChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelChgDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSPTransferD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextChgChargeID);
            this.uGroupBox1.Controls.Add(this.uTextChgChargeName);
            this.uGroupBox1.Controls.Add(this.uDateChgDate);
            this.uGroupBox1.Controls.Add(this.uLabelChgChargeID);
            this.uGroupBox1.Controls.Add(this.uLabelChgDate);
            this.uGroupBox1.Controls.Add(this.uGridSPTransferD);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBox1.TabIndex = 10;
            // 
            // uTextChgChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextChgChargeID.Appearance = appearance15;
            this.uTextChgChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextChgChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance20.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance20;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextChgChargeID.ButtonsRight.Add(editorButton1);
            this.uTextChgChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextChgChargeID.Location = new System.Drawing.Point(400, 28);
            this.uTextChgChargeID.MaxLength = 20;
            this.uTextChgChargeID.Name = "uTextChgChargeID";
            this.uTextChgChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextChgChargeID.TabIndex = 98;
            this.uTextChgChargeID.ValueChanged += new System.EventHandler(this.uTextChgChargeID_ValueChanged);
            this.uTextChgChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextChgChargeID_KeyDown);
            this.uTextChgChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextChgChargeID_EditorButtonClick);
            // 
            // uTextChgChargeName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChgChargeName.Appearance = appearance21;
            this.uTextChgChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChgChargeName.Location = new System.Drawing.Point(504, 28);
            this.uTextChgChargeName.Name = "uTextChgChargeName";
            this.uTextChgChargeName.ReadOnly = true;
            this.uTextChgChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextChgChargeName.TabIndex = 97;
            // 
            // uDateChgDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateChgDate.Appearance = appearance16;
            this.uDateChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateChgDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateChgDate.Location = new System.Drawing.Point(116, 28);
            this.uDateChgDate.Name = "uDateChgDate";
            this.uDateChgDate.Size = new System.Drawing.Size(100, 19);
            this.uDateChgDate.TabIndex = 35;
            // 
            // uLabelChgChargeID
            // 
            this.uLabelChgChargeID.Location = new System.Drawing.Point(296, 28);
            this.uLabelChgChargeID.Name = "uLabelChgChargeID";
            this.uLabelChgChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelChgChargeID.TabIndex = 34;
            this.uLabelChgChargeID.Text = "ultraLabel3";
            // 
            // uLabelChgDate
            // 
            this.uLabelChgDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelChgDate.Name = "uLabelChgDate";
            this.uLabelChgDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelChgDate.TabIndex = 33;
            this.uLabelChgDate.Text = "ultraLabel4";
            // 
            // uGridSPTransferD
            // 
            this.uGridSPTransferD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSPTransferD.DisplayLayout.Appearance = appearance9;
            this.uGridSPTransferD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSPTransferD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPTransferD.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridSPTransferD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSPTransferD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSPTransferD.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSPTransferD.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGridSPTransferD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSPTransferD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSPTransferD.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridSPTransferD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSPTransferD.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPTransferD.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance18.TextHAlignAsString = "Left";
            this.uGridSPTransferD.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridSPTransferD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSPTransferD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridSPTransferD.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridSPTransferD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSPTransferD.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridSPTransferD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSPTransferD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSPTransferD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSPTransferD.Location = new System.Drawing.Point(12, 32);
            this.uGridSPTransferD.Name = "uGridSPTransferD";
            this.uGridSPTransferD.Size = new System.Drawing.Size(1040, 708);
            this.uGridSPTransferD.TabIndex = 38;
            this.uGridSPTransferD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // ultraTextEditor1
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor1.Appearance = appearance22;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance23.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance23;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor1.ButtonsRight.Add(editorButton2);
            this.ultraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor1.Location = new System.Drawing.Point(360, 12);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(108, 19);
            this.ultraTextEditor1.TabIndex = 96;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 9;
            // 
            // uTextEquipCode
            // 
            this.uTextEquipCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance35;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton3);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(400, 12);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(116, 19);
            this.uTextEquipCode.TabIndex = 100;
            this.uTextEquipCode.ValueChanged += new System.EventHandler(this.uTextEquipCode_ValueChanged);
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uTextEquipName
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance67;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(520, 12);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(116, 21);
            this.uTextEquipName.TabIndex = 99;
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(296, 12);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquip.TabIndex = 2;
            this.uLabelEquip.Text = "ultraLabel2";
            // 
            // uComboPlant
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance2;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 8;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmEQUZ0017
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0017";
            this.Load += new System.EventHandler(this.frmEQUZ0017_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0017_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0017_FormClosing);
            this.Resize += new System.EventHandler(this.frmEQUZ0017_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPTransferD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSPTransferD;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateChgDate;
        private Infragistics.Win.Misc.UltraLabel uLabelChgChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelChgDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChgChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChgChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;

    }
}
