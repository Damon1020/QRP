﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0030
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0030));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionSelect = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipReq = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipReq)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uOptionSelect);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel6);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uTextSearchEquipName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance16;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Location = new System.Drawing.Point(764, 56);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipName.TabIndex = 38;
            // 
            // uTextSearchEquip
            // 
            appearance15.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquip.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquip.Location = new System.Drawing.Point(660, 56);
            this.uTextSearchEquip.MaxLength = 20;
            this.uTextSearchEquip.Name = "uTextSearchEquip";
            this.uTextSearchEquip.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquip.TabIndex = 38;
            this.uTextSearchEquip.ValueChanged += new System.EventHandler(this.uTextSearchEquip_ValueChanged);
            this.uTextSearchEquip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquip_KeyDown);
            this.uTextSearchEquip.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquip_EditorButtonClick);
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(556, 56);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquip.TabIndex = 37;
            // 
            // uOptionSelect
            // 
            this.uOptionSelect.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSelect.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "Request";
            valueListItem1.DisplayText = "의뢰일";
            valueListItem2.DataValue = "Receipt";
            valueListItem2.DisplayText = "접수일";
            valueListItem3.DataValue = "Repair";
            valueListItem3.DisplayText = "완료일";
            this.uOptionSelect.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.uOptionSelect.Location = new System.Drawing.Point(332, 12);
            this.uOptionSelect.Name = "uOptionSelect";
            this.uOptionSelect.Size = new System.Drawing.Size(256, 16);
            this.uOptionSelect.TabIndex = 36;
            this.uOptionSelect.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(660, 32);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipGroup.TabIndex = 35;
            // 
            // uComboSearchEquipLargeType
            // 
            this.uComboSearchEquipLargeType.Location = new System.Drawing.Point(388, 56);
            this.uComboSearchEquipLargeType.MaxLength = 50;
            this.uComboSearchEquipLargeType.Name = "uComboSearchEquipLargeType";
            this.uComboSearchEquipLargeType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLargeType.TabIndex = 34;
            this.uComboSearchEquipLargeType.ValueChanged += new System.EventHandler(this.uComboSearchEquType_ValueChanged);
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 56);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboProcessGroup.TabIndex = 33;
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(388, 32);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLoc.TabIndex = 32;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(116, 32);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchStation.TabIndex = 31;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 56);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 27;
            // 
            // uLabelEquipLoc
            // 
            this.uLabelEquipLoc.Location = new System.Drawing.Point(284, 32);
            this.uLabelEquipLoc.Name = "uLabelEquipLoc";
            this.uLabelEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLoc.TabIndex = 26;
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(284, 56);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 28;
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(556, 32);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroup.TabIndex = 30;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 29;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(12, 32);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 29;
            // 
            // uDateTo
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTo.Appearance = appearance18;
            this.uDateTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTo.Location = new System.Drawing.Point(228, 8);
            this.uDateTo.Name = "uDateTo";
            this.uDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateTo.TabIndex = 15;
            // 
            // uDateFrom
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFrom.Appearance = appearance17;
            this.uDateFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFrom.Location = new System.Drawing.Point(116, 8);
            this.uDateFrom.Name = "uDateFrom";
            this.uDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateFrom.TabIndex = 15;
            // 
            // ultraLabel6
            // 
            appearance2.FontData.BoldAsString = "True";
            this.ultraLabel6.Appearance = appearance2;
            this.ultraLabel6.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel6.TabIndex = 14;
            this.ultraLabel6.Text = "~";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1032, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(28, 21);
            this.uComboSearchPlant.TabIndex = 13;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1020, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(8, 20);
            this.uLabelSearchPlant.TabIndex = 12;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridEquipReq
            // 
            this.uGridEquipReq.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipReq.DisplayLayout.Appearance = appearance3;
            this.uGridEquipReq.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipReq.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipReq.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipReq.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.uGridEquipReq.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipReq.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGridEquipReq.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipReq.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipReq.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipReq.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridEquipReq.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipReq.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipReq.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipReq.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridEquipReq.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipReq.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipReq.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.uGridEquipReq.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridEquipReq.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipReq.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipReq.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridEquipReq.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipReq.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGridEquipReq.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipReq.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipReq.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipReq.Location = new System.Drawing.Point(0, 120);
            this.uGridEquipReq.Name = "uGridEquipReq";
            this.uGridEquipReq.Size = new System.Drawing.Size(1070, 704);
            this.uGridEquipReq.TabIndex = 5;
            // 
            // frmEQUZ0030
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridEquipReq);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0030";
            this.Load += new System.EventHandler(this.frmEQUZ0030_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0030_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipReq)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipReq;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSelect;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
    }
}