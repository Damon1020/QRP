﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 수리관리                                              */
/* 프로그램ID   : frmEQU0010.cs                                         */
/* 프로그램명   : 수리결과등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-06-30                                            */
/* 수정이력     : 2011-09-02 : 이종민 수정                              */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPEQU.UI
{
    public partial class frmEQU0010 : Form, IToolbar
    {
        // 다국어지원을 위한 전역변수

        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
        private Boolean m_bolDeleteCheck = false;
        private DataTable m_dtPMResult = new DataTable();
        private string m_strPlantCode;

        public frmEQU0010()
        {
            InitializeComponent();
        }

        #region Initialize Control

        private void frmEQU0010_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true,false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQU0010_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Title 제목
            titleArea.mfSetLabelText("수리결과등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "수리요청정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "수리접수정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "수리결과정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                // ExpandableGroupBox 접은상태로

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabel2, "수리요청번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchDate, "수리요청일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelRequestDate, "수리요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestUser, "수리요청자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFailDay, "고장발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFailTime, "고장발생시간", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProduct, "제품", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPKGType, "PKG Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestReason, "요청사유", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelAcceptDate, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAcceptUser, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStage, "Stage", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnusual, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelRepairDay, "수리일시", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRepairUser, "수리자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRepairResult, "수리결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStopType, "설비정지유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProblem, "문제점 및 조치내용", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                dtPlant.Dispose();
                clsPlant.Dispose();

                for (int i = 0; i < uComboSearchPlant.Items.Count; i++)
                {
                    uComboSearchPlant.SelectedIndex = i;
                    if (uComboSearchPlant.Value.ToString().Equals(m_resSys.GetString("SYS_PLANTCODE")))
                    {
                        break;
                    }
                    else
                    {
                        uComboSearchPlant.SelectedIndex = 0;
                    }
                }

                uDateSearchFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                uDateSearchToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridRepairResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                    , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true, Infragistics.Win.DefaultableBoolean.False
                    , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairReqCode", "수리요청번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairReqDate", "수리요청일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "UserName", "수리요청자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "BreakDownDate", "고장발생일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "BreakDownTime", "고장발생시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairReqReason", "요청사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "ReceiptDate", "접수일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "ReceiptName", "접수자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairStartDate", "수리시작일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairStartTime", "수리시작시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairEndDate", "수리완료일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairEndTime", "수리완료시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairEndName", "수리자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairResult", "수리결과", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRepairResult, 0, "RepairDesc", "수리특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 빈행추가
                //wGrid.mfAddRowGrid(this.uGrid, 0);
                //this.uGridRepairReq.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                // Set FontSize
                this.uGridRepairResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridRepairResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Component Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                uDateSearchFromDate.Value = DateTime.Now;
                uDateSearchToDate.Value = DateTime.Now;
                uComboSearchArea.Value = "";
                uComboSearchEquipProcess.Value = "";
                //uComboSearchPlant.Value = "";
                uComboSearchStation.Value = "";

                uTextReceiptDate.Text = "";
                uTextReceiptID.Text = "";
                uTextReceiptName.Text = "";
                uTextBreakDownDate.Text = "";
                uTextBreakDownTime.Text = "";
                uTextPKGType.Text = "";
                uTextRepairDesc.Text = "";
                uTextProductCode.Text = "";
                uTextProductName.Text = "";
                uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");
                uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");
                uTextRepairReqDate.Text = "";
                uTextRepairReqReason.Text = "";
                uTextRepairReqID.Text = "";
                uTextRepairReqName.Text = "";
                uTextStageDesc.Text = "";
                uTextReceiptDesc.Text = "";

                uCheckCCSRequest.CheckedValue = false;
                uCheckUrgentFlag.CheckedValue = false;

                uDateRepairStartDate.Value = DateTime.Now;
                uDateRepairStartTime.ResetDateTime();
                uDateRepairEndTime.ResetDateTime();

                m_dtPMResult.Clear();
                //m_dtPMResult.Dispose();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Common Event
        public void mfSearch()
        {
            //System ResouceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            WinMessageBox msg = new WinMessageBox();

            try
            {
                if (uComboSearchPlant.Value.ToString() == string.Empty)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "필수입력사항 확인", "공장을 선택하세요.", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.Focus();
                    return;
                } 

                if (Convert.ToDateTime(this.uDateSearchFromDate.Value).Date > Convert.ToDateTime(this.uDateSearchToDate.Value).Date)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검색날짜 확인", "검색시작일이 종료일보다 많습니다.", Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }

                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(uDateSearchFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(uDateSearchToDate.Value).ToString("yyyy-MM-dd");
                string strArea = uComboSearchArea.Value.ToString();
                string strStation = uComboSearchStation.Value.ToString();
                string strEquipProc = uComboSearchEquipProcess.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipRepairResult(strPlantCode, strFromDate, strToDate, strArea, strStation, strEquipProc, m_resSys.GetString("SYS_LANG"));

                /////////////
                ///////////// 
                uGridRepairResult.Refresh();
                uGridRepairResult.DataSource = dtEquip;
                uGridRepairResult.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                if (dtEquip.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                                Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            titleArea.Focus();
            if (uGroupBoxContentsArea.Expanded == false)
                return;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            #region Check Indispensable
            if (uTextRepairReqCode.Text.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "결과를 입력할 수리접수건을 선택하세요", Infragistics.Win.HAlign.Right);
                uGridRepairResult.Focus();
                return;
            }
            if (uDateRepairStartDate.Value.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "수리 시작일자를 입력하세요", Infragistics.Win.HAlign.Right);
                uDateRepairStartDate.Focus();
                return;
            }
            if (uDateRepairEndDate.Value.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "수리 시작일자를 입력하세요", Infragistics.Win.HAlign.Right);
                uDateRepairEndDate.Focus();
                return;
            }
            if (uTextRepairUserID.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "수리 담당자를 입력하세요", Infragistics.Win.HAlign.Right);
                uTextRepairUserID.Focus();
                return;
            }
            if (uTextRepairUserName.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "수리 담당자를 입력하세요", Infragistics.Win.HAlign.Right);
                uTextRepairUserName.Focus();
                return;
            }
            if (uComboRepairResult.Value.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "수리결과를 입력하세요", Infragistics.Win.HAlign.Right);
                uComboRepairResult.Focus();
                return;
            }
            //콤보박스 선택값 Validation Check//////////
            QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
            if (!check.mfCheckValidValueBeforSave(this)) return;
            ///////////////////////////////////////////

            #endregion

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            "확인창", "저장확인", "입력한 수리결과를 저장하시겠습니까?",
            Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                string strRepairReqCode = uTextRepairReqCode.Text.ToString();
                string strRepairStartDate = Convert.ToDateTime(uDateRepairStartDate.Value).ToString("yyyy-MM-dd");
                string strRepairStartTime = Convert.ToDateTime(uDateRepairStartTime.Value).ToString("HH:mm:ss");
                string strRepairEndDate = Convert.ToDateTime(uDateRepairEndDate.Value).ToString("yyyy-MM-dd");
                string strRepairEndTime = Convert.ToDateTime(uDateRepairEndTime.Value).ToString("HH:mm:ss");
                string strRepairUserID = uTextRepairUserID.Text;
                string strRepairResultCode = uComboRepairResult.Value.ToString();
                string strEquipDownCode = this.uTextFaultTypeCode.Text;
                string strEquipDownCodeName = this.uTextFaultTypeName.Text;
                string strCCSReqFlag = uCheckCCSRequest.Checked == true ? "T" : "F";
                string strRepairDesc = uTextRepairDesc.Text;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);

                string strRtn = clsEquipReq.mfSaveEquipRepairResult(
                                                                        strRepairReqCode, strRepairStartDate, strRepairStartTime,
                                                                        strRepairEndDate, strRepairEndTime, strRepairUserID,
                                                                        strRepairResultCode, strEquipDownCode, strEquipDownCodeName, strCCSReqFlag,
                                                                        strRepairDesc, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID")
                                                                    );

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                // 저장 끝

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    m_bolDeleteCheck = false;
                    InitGroupBox();
                    mfClear();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            if (!m_bolDeleteCheck)
                return;

            if (!uGroupBoxContentsArea.Expanded)
                return;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            #region Check Indispensable
            if (uTextRepairReqCode.Text.ToString() == string.Empty)
            {
                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "결과를 취소할 수리접수건을 선택하세요", Infragistics.Win.HAlign.Right);
                uGridRepairResult.Focus();
                return;
            }
            #endregion

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "취소확인", "입력한 수리결과를 취소하시겠습니까?",
                                    Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");

                string strRepairReqCode = uTextRepairReqCode.Text.ToString();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsEquipReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsEquipReq);
                
                string strRtn = clsEquipReq.mfDeleteEquipRepairResult(strRepairReqCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                // 저장 끝

                m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "취소처리결과", "수리결과를 취소했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    m_bolDeleteCheck = false;
                    InitGroupBox();
                    mfClear();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "취소처리결과", "수리결과를 취소하지 못했습니다.",
                                                 Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitValue();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                //Systme ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridRepairResult.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridRepairResult.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridRepairResult);

                /////////////


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region UI Event

        // GroupBox 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridRepairResult.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridRepairResult.Height = 760;

                    for (int i = 0; i < uGridRepairResult.Rows.Count; i++)
                    {
                        uGridRepairResult.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 공장콤보 변경시 다른 콤보박스 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinComboEditor wCombo = new WinComboEditor();

            try
            {
                if (uComboSearchPlant.Value.ToString() != string.Empty)
                {

                    //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    //Thread t1 = m_ProgressPopup.mfStartThread();
                    //m_ProgressPopup.mfOpenProgressPopup(this, "조회중...");
                    //this.MdiParent.Cursor = Cursors.WaitCursor;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);

                    DataTable dtArea = clsArea.mfReadAreaCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboSearchArea.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "AreaCode", "AreaName", dtArea);

                    dtArea.Dispose();
                    clsArea.Dispose();

                    brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboSearchStation.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "StationCode", "StationName", dtStation);

                    dtStation.Dispose();
                    clsStation.Dispose();

                    brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                    QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                    brwChannel.mfCredentials(clsEquipProcGubun);

                    DataTable dtEquipProcGugun = clsEquipProcGubun.mfReadProGubunCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboSearchEquipProcess.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboSearchEquipProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "EquipProcGubunCode", "EquipProcGubunName", dtEquipProcGugun);

                    dtEquipProcGugun.Dispose();
                    clsEquipProcGubun.Dispose();

                    brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                    QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                    brwChannel.mfCredentials(clsPMResult);

                    DataTable dtPMResult = clsPMResult.mfReadPMResultTypeCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    m_dtPMResult = clsPMResult.mfReadPMResultTypeCombo(uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    uComboRepairResult.ValueList.Reset();
                    wCombo.mfSetComboEditor(this.uComboRepairResult, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "PMResultCode", "PMResultName", dtPMResult);

                    dtPMResult.Dispose();
                    clsPMResult.Dispose();

                    //this.MdiParent.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    uComboSearchArea.ValueList.Reset();
                    uComboSearchEquipProcess.ValueList.Reset();
                    uComboSearchStation.ValueList.Reset();
                    uComboRepairResult.ValueList.Reset();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 설비정지유형 콤보 변경 이벤트 -- 미정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboStopType_ValueChanged(object sender, EventArgs e)
        {

        }

        private void mfClear()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                foreach (Control ctrl in uGroupBoxContentsArea.Controls)
                {
                    if (ctrl.GetType() == typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor))
                    {
                        ctrl.Text = string.Empty;
                    }
                }
                uTextRepairUserID.Text = m_resSys.GetString("SYS_USERID");
                uTextRepairUserName.Text = m_resSys.GetString("SYS_USERNAME");

                uDateRepairStartDate.Value = DateTime.Now;
                uDateRepairStartTime.ResetDateTime();
                uDateRepairEndTime.ResetDateTime();

                uCheckCCSRequest.Checked = false;
                uCheckHeadFlag.Checked = false;
                uCheckUrgentFlag.Checked = false;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        #endregion

        #region Grid Event

        private void uGridRepairResult_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            WinMessageBox msg = new WinMessageBox();

            try
            {
                string strRepairReqCode = e.Cell.Row.GetCellValue("RepairReqCode").ToString();
                if (!uGroupBoxContentsArea.Expanded)
                {
                    uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                if (strRepairReqCode != string.Empty)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                    //BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquip = clsEquip.mfReadEquipRepairResult_Detail(strRepairReqCode, m_resSys.GetString("SYS_LANG"));
                    //

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (dtEquip != null && dtEquip.Rows.Count != 0)
                    {
                        m_strPlantCode = dtEquip.Rows[0]["PlantCode"].ToString();

                        #region Request
                        uTextRepairReqCode.Text = dtEquip.Rows[0]["RepairReqCode"].ToString();
                        uTextRepairReqDate.Text = dtEquip.Rows[0]["RepairReqDate"].ToString();
                        uTextBreakDownDate.Text = dtEquip.Rows[0]["BreakDownDate"].ToString();
                        uTextProductCode.Text = dtEquip.Rows[0]["ProductCode"].ToString();
                        uTextProductName.Text = dtEquip.Rows[0]["ProductName"].ToString();
                        uTextRepairReqReason.Text = dtEquip.Rows[0]["RepairReqReason"].ToString();
                        uTextPKGType.Text = dtEquip.Rows[0]["PKGType"].ToString();
                        uCheckUrgentFlag.Checked = Convert.ToBoolean(dtEquip.Rows[0]["UrgentFlag"].ToString());
                        uTextBreakDownTime.Text = dtEquip.Rows[0]["BreakDownTime"].ToString();
                        uTextRepairReqID.Text = dtEquip.Rows[0]["RepairReqID"].ToString();
                        uTextRepairReqName.Text = dtEquip.Rows[0]["RepairReqName"].ToString();
                        #endregion

                        #region PMResultType
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                        QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                        brwChannel.mfCredentials(clsPMResult);

                        m_dtPMResult = new DataTable();
                        m_dtPMResult = clsPMResult.mfReadMASPMResultType(m_strPlantCode, m_resSys.GetString("SYS_LANG"));
                        uComboRepairResult.ValueList.Reset();
                        WinComboEditor wCombo = new WinComboEditor();

                        wCombo.mfSetComboEditor(this.uComboRepairResult, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                            , "PMResultCode", "PMResultName", m_dtPMResult);

                        ////m_dtPMResult.Clear();
                        //m_dtPMResult = new DataTable();
                        //m_dtPMResult = clsPMResult.mfReadMASPMResultType(uComboRepairResult.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                        #endregion

                        #region Receipt
                        uTextReceiptDate.Text = dtEquip.Rows[0]["ReceiptDate"].ToString();
                        uCheckHeadFlag.Checked = Convert.ToBoolean(dtEquip.Rows[0]["HeadFlag"].ToString());
                        uTextReceiptDesc.Text = dtEquip.Rows[0]["ReceiptDesc"].ToString();
                        uTextStageDesc.Text = dtEquip.Rows[0]["StageDesc"].ToString();
                        uTextReceiptID.Text = dtEquip.Rows[0]["ReceiptID"].ToString();
                        uTextReceiptName.Text = dtEquip.Rows[0]["ReceiptName"].ToString();
                        #endregion

                        #region Repair
                        if (dtEquip.Rows[0]["RepairStartDate"].ToString() != string.Empty)
                            uDateRepairStartDate.Value = dtEquip.Rows[0]["RepairStartDate"].ToString();
                        else
                            uDateRepairStartDate.Value = DateTime.Now;

                        if (dtEquip.Rows[0]["RepairStartTime"].ToString() != string.Empty)
                            uDateRepairStartTime.Value = dtEquip.Rows[0]["RepairStartTime"].ToString();
                        else
                            uDateRepairStartTime.Value = DateTime.Now;

                        if (dtEquip.Rows[0]["RepairEndDate"].ToString() != string.Empty)
                            uDateRepairEndDate.Value = dtEquip.Rows[0]["RepairEndDate"].ToString();
                        else
                            uDateRepairEndDate.Value = DateTime.Now;

                        if (dtEquip.Rows[0]["RepairEndTime"].ToString() != string.Empty)
                            uDateRepairEndTime.Value = dtEquip.Rows[0]["RepairEndTime"].ToString();
                        else
                            uDateRepairEndTime.Value = DateTime.Now;

                        if (dtEquip.Rows[0]["RepairID"].ToString() != string.Empty)
                        {
                            uTextRepairUserID.Text = dtEquip.Rows[0]["RepairID"].ToString();
                            uTextRepairUserName.Text = dtEquip.Rows[0]["RepairName"].ToString();
                        }

                        if (dtEquip.Rows[0]["RepairResultCode"].ToString() != string.Empty)
                        {
                            Boolean bolComboCheck = false;
                            m_bolDeleteCheck = true;
                            for (int i = 0; i < uComboRepairResult.Items.Count; i++)
                            {
                                if (dtEquip.Rows[0]["RepairResultCode"].ToString() == uComboRepairResult.Items[i].DataValue.ToString())
                                {
                                    uComboRepairResult.SelectedIndex = i;
                                    bolComboCheck = true;
                                }
                            }

                            if (!bolComboCheck)
                            {
                                uComboRepairResult.Items.Add(dtEquip.Rows[0]["RepairResultCode"].ToString(), dtEquip.Rows[0]["RepairResultName"].ToString());
                                uComboRepairResult.SelectedText = dtEquip.Rows[0]["RepairResultName"].ToString();
                            }
                        }
                        else
                        {
                            uComboRepairResult.SelectedIndex = 0;
                        }

                        //if (dtEquip.Rows[0]["EquipStopTypeCode"].ToString() != string.Empty)
                        //{
                        //    Boolean bolComboCheck = false;
                        //    for (int i = 0; i < uComboStopType.Items.Count; i++)
                        //    {
                        //        if (dtEquip.Rows[0]["EquipStopTypeCode"].ToString() == uComboStopType.Items[i].DataValue.ToString())
                        //        {
                        //            uComboStopType.SelectedIndex = i;
                        //            bolComboCheck = true;
                        //        }
                        //    }

                        //    if (!bolComboCheck)
                        //    {
                        //        uComboStopType.Items.Add(dtEquip.Rows[0]["EquipStopTypeCode"].ToString(), dtEquip.Rows[0]["EquipStopTypeName"].ToString());
                        //        uComboStopType.SelectedText = dtEquip.Rows[0]["EquipStopTypeName"].ToString();
                        //    }

                        //}
                        //else
                        //{
                        //    uComboStopType.SelectedIndex = 0;
                        //}

                        uCheckCCSRequest.Checked = Convert.ToBoolean(dtEquip.Rows[0]["CCSReqFlag"].ToString());
                        if(dtEquip.Rows[0]["RepairDesc"].ToString() != string.Empty)
                            uTextRepairDesc.Text = dtEquip.Rows[0]["RepairDesc"].ToString();
                        else
                            uTextRepairDesc.Text = string.Empty;

                        //설비정지유형
                        //this.uTextFaultTypeCode.Text = dtEquip.Rows[0]["FaultTypeCode"].ToString();
                        //this.uTextFaultTypeName.Text = dtEquip.Rows[0]["FaultTypeName"].ToString();

                        #endregion
                    }

                    dtEquip.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //스핀버튼을 누르면 초가늘어남
        private void uDateTime_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼
                Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                DateTime dateTemp = (DateTime)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    ed.Value = dateTemp.AddMinutes(1);
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && dateTemp > DateTime.MinValue)
                {
                    ed.Value = dateTemp.AddMinutes(-1);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        #endregion

        private void uComboRepairResult_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (uComboRepairResult.Items.Count == 0)
                    return;

                if (uComboSearchPlant.Value.ToString().Equals(string.Empty))
                    return;

                if (uComboRepairResult.Value.ToString().Equals(string.Empty))
                {
                    uCheckChangeFlag.Checked = false;
                    return;
                }

                string strRepairResultCode = uComboRepairResult.Value.ToString();
                for (int i = 0; i < m_dtPMResult.Rows.Count; i++)
                {
                    if (m_dtPMResult.Rows[i]["PMResultCode"].ToString() == strRepairResultCode)
                    {
                        uCheckChangeFlag.CheckedValue = m_dtPMResult.Rows[i]["ChangeFlag"].ToString() == "T" ? true : false;

                        if (uCheckChangeFlag.Checked)
                            break;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
    }
}
