﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 전산SHEET관리                                         */
/* 프로그램ID   : frmEQUZ0034.cs                                        */
/* 프로그램명   : Laser Full Power Check Sheet                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-03-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;


namespace QRPEQU.UI
{

    public partial class frmEQUZ0034 : Form, IToolbar
    {
        //리소를 호출을위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmEQUZ0034()
        {
            InitializeComponent();
        }

        private void frmEQUZ0034_Activated(object sender, EventArgs e)
        {
            QRPBrowser Toolbar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            Toolbar.mfActiveToolBar(this.MdiParent, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0034_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmEQUZ0034_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitGrid();
            InitLabel();
            InitCombo();

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }


        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("Laser Full Power Check", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchDate, "검색일", m_resSys.GetString("SYS_FONTNAME"), true, true);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_FONTNAME"));

                WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"),
                    "", "", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                this.uGridLaserFull.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;

                grd.mfInitGeneralGrid(this.uGridLaserFull, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false", 0, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "DocCode", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "InspectDate", "점검일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "", 2, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "InspectTime", "점검시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Time, "", "hh:mm:ss", "", 3, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup40Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "40Khz", "40Khz", 4, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup60Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "60Khz", "60Khz", 6, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup80Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "80Khz", "80Khz", 8, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup100Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "100Khz", "100Khz", 10, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup120Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "120Khz", "120Khz", 12, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup140Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "140Khz", "140Khz", 14, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup160Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "160Khz", "160Khz", 16, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup180Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "180Khz", "180Khz", 18, 0, 2, 2, false);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup200Khz = grd.mfSetGridGroup(this.uGridLaserFull, 0, "200Khz", "200Khz", 20, 0, 2, 2, false);


                grd.mfSetGridColumn(this.uGridLaserFull, 0, "40UV", "8(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup40Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "40Meter", "5.9(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup40Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "60UV", "6.5(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup60Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "60Meter", "4.8(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup60Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "80UV", "5(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup80Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "80Meter", "3.7(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup80Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "100UV", "4(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup100Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "100Meter", "2.9(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup100Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "120UV", "3.2(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup120Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "120Meter", "2.3(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup120Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "140UV", "2.6(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup140Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "140Meter", "1.9(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup140Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "160UV", "2.2(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup160Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "160Meter", "1.6(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup160Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "180UV", "2(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup180Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "180Meter", "1.4(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup180Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "200UV", "2(UV)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup200Khz);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "200Meter", "1.4(Meter)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup200Khz);


                grd.mfSetGridColumn(this.uGridLaserFull, 0, "InspectUserID", "점검자서명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 22, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGridLaserFull, 0, "UseLaserTime", "Laser사용시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Time, "", "hh:mm:ss", "", 23, 0, 1, 2, null);


                //헤더,셀 FontSize 설정
                this.uGridLaserFull.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridLaserFull.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridLaserFull.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //한줄추가
                grd.mfAddRowGrid(this.uGridLaserFull, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Toolbar

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                #region 필수입력사항
                if (this.uDateFrom.Value == null || this.uDateFrom.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인","검색시작일을 선택해주세요.",Infragistics.Win.HAlign.Right);
                    this.uDateFrom.DropDown();
                    return;
                }
                if (this.uDateTo.Value == null || this.uDateTo.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "검색종료일을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uDateTo.DropDown();
                    return;
                }
                if (this.uDateFrom.DateTime.Date > this.uDateTo.DateTime.Date)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "검색시작일이 종료일보다 큽니다.", Infragistics.Win.HAlign.Right);
                    this.uDateFrom.DropDown();
                    return;
                }

                #endregion


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY1), "ASSY1");
                QRPEQU.BL.EQUSHT.ASSY1 clsASSY1 = new QRPEQU.BL.EQUSHT.ASSY1();
                brwChannel.mfCredentials(clsASSY1);

                DataTable dtDataInfo = clsASSY1.mfReadLaserPowerCheck(this.uDateFrom.DateTime.Date.ToString("yyyy-MM-dd"), this.uDateTo.DateTime.Date.ToString("yyyy-MM-dd"), m_resSys.GetString("SYS_LANG"));
          
                this.uGridLaserFull.DataSource = dtDataInfo;
                this.uGridLaserFull.DataBind();
                /////////////


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGridLaserFull.Rows.Count > 0)
                    this.uGridLaserFull.ActiveCell = this.uGridLaserFull.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY1), "ASSY1");
                QRPEQU.BL.EQUSHT.ASSY1 clsASSY1 = new QRPEQU.BL.EQUSHT.ASSY1();
                brwChannel.mfCredentials(clsASSY1);

                DataTable dtDataInfo = clsASSY1.mfSetDataInfo_Laser();

                for (int i = 0; i < this.uGridLaserFull.Rows.Count; i++)
                {
                    if (this.uGridLaserFull.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (this.uGridLaserFull.Rows[i].Cells["InspectUserID"].Value.ToString().Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "확인창", "필수입력사항확인", "점검자를 입력해주세요.",
                                   Infragistics.Win.HAlign.Right);

                            this.uGridLaserFull.ActiveCell = this.uGridLaserFull.Rows[i].Cells["InspectUserID"];
                            this.uGridLaserFull.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        DataRow drRow = dtDataInfo.NewRow();

                        drRow["DocCode"] = this.uGridLaserFull.Rows[i].GetCellValue("DocCode").ToString();
                        
                        //점검일
                        if(this.uGridLaserFull.Rows[i].GetCellValue("InspectDate").ToString().Equals(string.Empty))
                            drRow["InspectDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        else
                            drRow["InspectDate"] = this.uGridLaserFull.Rows[i].GetCellValue("InspectDate");

                        //점검시간
                        if(this.uGridLaserFull.Rows[i].GetCellValue("InspectTime").ToString().Equals(string.Empty))
                            drRow["InspectTime"] = DateTime.Now.ToString("HH:mm:ss");
                        else
                            drRow["InspectTime"] = this.uGridLaserFull.Rows[i].GetCellValue("InspectTime");

                        drRow["40UV"] = this.uGridLaserFull.Rows[i].GetCellValue("40UV");
                        drRow["40Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("40Meter");
                        drRow["60UV"] = this.uGridLaserFull.Rows[i].GetCellValue("60UV");
                        drRow["60Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("60Meter");
                        drRow["80UV"] = this.uGridLaserFull.Rows[i].GetCellValue("80UV");
                        drRow["80Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("80Meter");
                        drRow["100UV"] = this.uGridLaserFull.Rows[i].GetCellValue("100UV");
                        drRow["100Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("100Meter");
                        drRow["120UV"] = this.uGridLaserFull.Rows[i].GetCellValue("120UV");
                        drRow["120Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("120Meter");
                        drRow["140UV"] = this.uGridLaserFull.Rows[i].GetCellValue("140UV");
                        drRow["140Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("140Meter");
                        drRow["160UV"] = this.uGridLaserFull.Rows[i].GetCellValue("160UV");
                        drRow["160Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("160Meter");
                        drRow["180UV"] = this.uGridLaserFull.Rows[i].GetCellValue("180UV");
                        drRow["180Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("180Meter");
                        drRow["200UV"] = this.uGridLaserFull.Rows[i].GetCellValue("200UV");
                        drRow["200Meter"] = this.uGridLaserFull.Rows[i].GetCellValue("200Meter");
                        drRow["InspectUserID"] = this.uGridLaserFull.Rows[i].GetCellValue("InspectUserID");
                        drRow["UseLaserTime"] = this.uGridLaserFull.Rows[i].GetCellValue("UseLaserTime");

                        dtDataInfo.Rows.Add(drRow);

                    }
                }

                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "저장 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = clsASSY1.mfSaveLaserPowerCheck(dtDataInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                   
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMes = "";
                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = "입력한 정보를 저장하지 못했습니다.";
                        else
                            strMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", strMes,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGridLaserFull.Rows.Count > 0)
                    this.uGridLaserFull.ActiveCell = this.uGridLaserFull.Rows[0].Cells[0];
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "삭제 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSHT.ASSY1), "ASSY1");
                QRPEQU.BL.EQUSHT.ASSY1 clsASSY1 = new QRPEQU.BL.EQUSHT.ASSY1();
                brwChannel.mfCredentials(clsASSY1);

                DataTable dtDataInfo = clsASSY1.mfSetDelDataInfo_Laser();


                for (int i = 0; i < this.uGridLaserFull.Rows.Count; i++)
                {
                    if (this.uGridLaserFull.Rows[i].RowSelectorAppearance != null)
                    {
                        if (!this.uGridLaserFull.Rows[i].Cells["DocCode"].Value.ToString().Equals(string.Empty)) //문서번호가 있는경우
                        {
                            if (Convert.ToBoolean(this.uGridLaserFull.Rows[i].Cells["Check"].Value))
                            {
                                DataRow drRow = dtDataInfo.NewRow();

                                drRow["DocCode"] = this.uGridLaserFull.Rows[i].Cells["DocCode"].Value;

                                dtDataInfo.Rows.Add(drRow);
                            }
                        }
                    }
                }


                if (dtDataInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "삭제 할 정보가 없습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsASSY1.mfDeleteLaserPowerCheck(dtDataInfo);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                   
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMes = "";
                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = "선택한 정보를 삭제하지 못했습니다.";
                        else
                            strMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", strMes,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridLaserFull.Rows.Count <= 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                        , "확인창", "출력정보확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridLaserFull);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
        }

        public void mfCreate()
        {
        }

        #endregion


        /// <summary>
        /// 그리드 셀편집시 줄머리에 편집이미지 삽입
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridLaserFull_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridLaserFull, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 점검일,점검시간 수정불가
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridLaserFull_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            e.Row.Cells["InspectDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            e.Row.Cells["InspectTime"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }



      

    }
}
