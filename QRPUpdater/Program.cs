﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace QRPUpdater
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string strExtArg = "";
            string[] strSep = { "|" };

            //Argument 문자열로 저장
            foreach (string strArg in args)
            {
                strExtArg = strExtArg + strArg;
            }

            //Argument가 없는 경우
            string strPreArg = "QRP";
            if (strExtArg != "")
            {
                string[] arrExtArg = strExtArg.Split(strSep, StringSplitOptions.None);
                strExtArg = "";
                for (int i = 1; i < arrExtArg.Count(); i++)
                {
                    strExtArg = strExtArg + arrExtArg[i] + "|";
                }
            }
            strExtArg = strPreArg + "|" + strExtArg;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmQRPUpdater(strExtArg));
        }
    }
}
