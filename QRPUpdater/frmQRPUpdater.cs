﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRP                                                   */
/* 모듈(분류)명 : QRP Updater                                           */
/* 프로그램ID   : frmQRPUpdater.cs                                      */
/* 프로그램명   : QRP System관련 화일 다운로드                          */
/* 작성자       : 류근철                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Collections;
using System.IO;

using System.EnterpriseServices;
using System.Resources;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Configuration;

using System.Diagnostics;

//using QRPCOM.QRPGLO;
using QRPSYS.BL.SYSPGM;

namespace QRPUpdater
{
    public partial class frmQRPUpdater : Form
    {
        private string m_strWebURL;             //화일을 처기하기 위한 Web URI
        private string m_strUserID;             //화일서버 폴더를 접근할 수 있는 사용자ID
        private string m_strPassword;           //화일서버 폴더를 접근할 수 있는 사용자암호
        private string m_strExePath;            //실행프로그램 Client경로

        private ArrayList m_arrSourceFolder = new ArrayList();     //다운로드받을 IIS 가상디렉토리의 Sub폴더
        private ArrayList m_arrDownloadDLLFile = new ArrayList();  //다운로드받을 DLL화일

        private int m_intDownloadIndex;         //현재 Upload화일 Index

        private string m_strQRPBrowserPath;     //QRPBrowser 실행프로그램 경로
        private string m_strExtenalArg;         //외부 Argument

        public frmQRPUpdater()
        {
            InitializeComponent();
        }

        public frmQRPUpdater(string strExtArg)
        {
            InitializeComponent();
            m_strExtenalArg = strExtArg;
        }

        private string ConvertDateTimeToString(string strOldDate)
        {
            try
            {
                string strNewDate = string.Empty;

                if (strOldDate.Contains("오전") || strOldDate.Contains("오후"))
                {
                    string[] sepSpace = { "   ", " ", "  " };
                    string[] sepTime = { ":" };
                    string[] arrDate = strOldDate.Split(sepSpace, StringSplitOptions.None);
                    string[] arrTime = arrDate[2].Split(sepTime, StringSplitOptions.None);

                    if (arrDate[1].Equals("오후") && !arrTime[0].Equals("12"))
                        arrTime[0] = (Convert.ToInt32(arrTime[0]) + 12).ToString();
                    else if (arrDate[1].Equals("오전") && arrTime[0].Equals("12"))
                        arrTime[0] = "0";

                    string strNewTime = string.Empty;
                    for (int i = 0; i < 2; i++)
                        strNewTime = strNewTime + arrTime[i] + ":";

                    strNewTime = strNewTime.Substring(0, strNewTime.Length - 1);

                    //strNewDate = arrDate[0] + " " + arrTime[0] + ":" + arrTime[1] + ":" + arrTime[2];
                    strNewDate = arrDate[0] + " " + strNewTime;
                }
                else
                {
                    strNewDate = strOldDate;
                }

                return strNewDate;
            }
            catch (System.Exception ex)
            {
                return "Error Exception";
            }
            finally
            {
            }
        }

        private void frmQRPUpdater_Load(object sender, EventArgs e)
        {
            try
            {
                //System.Threading.Thread.Sleep(10000);
                #region QRPUpadter이외의 Process 종료처리
                //string strPath = Environment.CurrentDirectory.ToString();
                string strPath = Application.StartupPath.ToString();
                DirectoryInfo Di = new DirectoryInfo(strPath);
                FileInfo[] arrFi = Di.GetFiles("*.exe");

                Process me = Process.GetCurrentProcess();
                foreach (FileInfo F in arrFi)
                {
                    if (me.ProcessName.Replace(".vshost", "") != F.Name.Replace(".exe", "").Replace(".vshost", ""))
                    {
                        Process[] clsPro = Process.GetProcessesByName(F.Name.Replace(".exe", ""));
                        foreach (Process Pro in clsPro)
                        {
                            if (Pro.ProcessName == F.Name.Replace(".exe", ""))
                            {
                                Pro.Kill();
                                break;
                            }
                        }
                        break;
                    }
                }
                #endregion

                //MessageBox.Show(m_strExtenalArg);

                //로그인 정보 전역변수 설정 ==> 공장코드 가지고 오도록 설정
                //mGlobalVal.UserIP = mGlobalVal.mfGetUserIP();
                //mGlobalVal.UserID = "admin";
                //mGlobalVal.Lang = "KOR";
                //mGlobalVal.SystemFontName = "돋움체";

                //MessageBox.Show("a1");

                //QRPGlobal SysRes = new QRPGlobal();
                ResourceSet m_resSys = new ResourceSet(strPath + "\\SysResources.resources");

                //MessageBox.Show("a2");

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                //MessageBox.Show("a3");

                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "QRPBrowser.exe";

                //MessageBox.Show("a3");

                //실행프로그램 Client경로 지정
                m_strExePath = Application.ExecutablePath;
                intPos = m_strExePath.LastIndexOf(@"\");
                m_strExePath = m_strExePath.Substring(0, intPos + 1);

                //QRP SYSTEM 화일서버 정보를 가져옴.////////////
                //m_strWebURL = "http://10.60.24.173/QRP_STS_FileSvr/QRPDLL/";
                //m_strUserID = "administrator";
                //m_strPassword = "jinjujin";

                //화일서버 연결정보 가져오기
                ////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                ////brwChannel.mfCredentials(clsSysAccess);

                mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S01");

                //화일서버 연결정보가 없으면 메세지 띄우고 끝
                if (dtSysAccess.Rows.Count <= 0)
                {
                    //MessageBox.Show("Don't Save Path Information for System File Download!");
                    RunQRPBrowser();
                }
                else
                {
                    m_strWebURL = dtSysAccess.Rows[0]["SystemAddressPath"].ToString();
                    m_strUserID = dtSysAccess.Rows[0]["AccessID"].ToString();
                    m_strPassword = dtSysAccess.Rows[0]["AccessPassword"].ToString();

                    //////////////////////////////////////////////////////////////////////
                    // QRP System 화일서버 경로 Parsing                     //2012.09.12//
                    //////////////////////////////////////////////////////////////////////
                    string[] strSeparator = { "/", "//" };
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    string strCur_ServerPath = config.AppSettings.Settings["RemoteServer"].Value.ToString();
                    string[] strArr_ServerPath = strCur_ServerPath.Split(strSeparator, StringSplitOptions.RemoveEmptyEntries);
                    string[] strArr_SvrFilePath = m_strWebURL.Split(strSeparator, StringSplitOptions.RemoveEmptyEntries);
                    m_strWebURL = "http://" + strArr_ServerPath[1] + "/" + strArr_SvrFilePath[2] + "/" + strArr_SvrFilePath[3] + "/";

                    ///////////////////////////////////////////////

                    string strDownloadFile;
                    string[] sepFolder = { "/\">" };           //화일정보중에서 Sub폴더정보를 Parsing하기 위한 구분자
                    string[] sepDetailFolder = { "</A>" };     //Sub폴더정보중에서 상세폴더정보를 Parsing하기 위한 구분자
                    string[] sepFile = { "<A HREF=" };         //화일정보중에서 화일명을 Parsing하기 위한 구분자
                    string[] sepLine = { "<br>" };         //화일정보중에서 화일명을 Parsing하기 위한 구분자
                    string[] sepSpace = { "   " };             //화일정보중에서 시간정보를 Parsing하기 위한 구분자
                    string[] sepFileLast = { "\">" };
                    Uri uri;

                    //권한설정
                    NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);

                    //WebClient로 QRPDLL폴더의 화일정보를 가지고 온다.
                    WebClient wclient = new WebClient();
                    wclient.Encoding = System.Text.Encoding.UTF8;
                    wclient.Credentials = cred;
                    uri = new Uri(m_strWebURL);
                    strDownloadFile = wclient.DownloadString(uri);

                    //가상디렉토리의 Sub폴더를 Parsing함.
                    string[] arrFullInfo = strDownloadFile.Split(sepLine, StringSplitOptions.None);
                    for (int i = 2; i < arrFullInfo.Count() - 1; i++)
                    {
                        string[] arrFolderInfo = arrFullInfo[i].Substring(2).Split(sepFolder, StringSplitOptions.None);
                        //폴더인 경우
                        if (arrFolderInfo.Count() > 1)
                        {
                            arrFolderInfo = arrFolderInfo[1].Split(sepDetailFolder, StringSplitOptions.None);
                            m_arrSourceFolder.Add(arrFolderInfo[0]);
                        }
                        //화일인 경우
                        else
                        {
                            string strFileFullInfo = arrFolderInfo[0];
                            //최종수정일 Parsing
                            string[] arrSourceFileLine = strFileFullInfo.Split(sepFile, StringSplitOptions.None);
                            string[] arrSourceFileDate = arrSourceFileLine[0].Split(sepSpace, StringSplitOptions.None);
                            //DateTime datSourceFileDate = Convert.ToDateTime(arrSourceFileDate[0]);
                            //DateTime datSourceFileDate = Convert.ToDateTime(Convert.ToDateTime(arrSourceFileDate[0]).ToString("yyyy-MM-dd HH:mm:ss"));

                            string datSourceFileDate = ConvertDateTimeToString(arrSourceFileDate[0]);
                            long datSourceFileLenth = Convert.ToInt64(arrSourceFileDate[arrSourceFileDate.Length - 1]);
                            //화일명 Parsing
                            //string[] sepFolderFile = { m_arrSourceFolder[i].ToString() + "/" };
                            string[] arrSourceFile = arrSourceFileLine[1].Split(sepFileLast, StringSplitOptions.None);
                            arrSourceFile = arrSourceFile[1].Split(sepDetailFolder, StringSplitOptions.None);
                            string strSourceFileName = arrSourceFile[0];

                            //Local에 화일이 없거나 Local에 있는 Dll화일의 최종수정일과 비교하여 틀리면 Download받음.
                            string strClientFile = m_strExePath + strSourceFileName;
                            FileInfo strClientFileInfo = new FileInfo(strClientFile);
                            
                            //QRPUpdater.exe는 제외한다 => 자기 자신이기 때문에 실행중 업데이트 못받음 : QRPBrowse에서 실행받도록 한다.
                            if (strSourceFileName != "QRPUpdater.exe")
                            {
                                if (strClientFileInfo.Exists == false || strClientFileInfo.LastWriteTime < Convert.ToDateTime(datSourceFileDate) || !strClientFileInfo.Length.Equals(datSourceFileLenth))
                                    m_arrDownloadDLLFile.Add(strSourceFileName);
                            }                            
                        }
                    }

                    for (int i = 0; i < m_arrSourceFolder.Count; i++)
                    {
                        WebClient wclientSub = new WebClient();
                        wclientSub.Encoding = System.Text.Encoding.UTF8;
                        wclientSub.Credentials = cred;
                        Uri uriSub = new Uri(m_strWebURL + m_arrSourceFolder[i] + "/");
                        strDownloadFile = wclientSub.DownloadString(uriSub);

                        string[] arrFileFullInfo = strDownloadFile.Split(sepLine, StringSplitOptions.None);

                        for (int j = 2; j < arrFileFullInfo.Count() - 1; j++)
                        {
                            string strFileFullInfo = arrFileFullInfo[j];
                            //최종수정일 Parsing
                            string[] arrSourceFileLine = strFileFullInfo.Split(sepFile, StringSplitOptions.None);
                            string[] arrSourceFileDate = arrSourceFileLine[0].Split(sepSpace, StringSplitOptions.None);
                            DateTime datSourceFileDate = Convert.ToDateTime(ConvertDateTimeToString(arrSourceFileDate[0]));
                            long datSourceFileLenth = Convert.ToInt64(arrSourceFileDate[arrSourceFileDate.Length-1]);
                            //화일명 Parsing
                            string[] sepFolderFile = { m_arrSourceFolder[i].ToString() + "/" };
                            string[] arrSourceFile = arrSourceFileLine[1].Split(sepFolderFile, StringSplitOptions.None);
                            arrSourceFile = arrSourceFile[1].Split(sepFileLast, StringSplitOptions.None);
                            string strSourceFileName = arrSourceFile[0];
                            
                            //Tag안에 한글은 깨져서 Tag밖에 것을 클라이언트 화일명으로 함.
                            string[] arrClientFileName = arrSourceFile[1].Split(sepDetailFolder, StringSplitOptions.None);
                            string strClientFileName = arrClientFileName[0];

                            //Local에 화일이 없거나 Local에 있는 Dll화일의 최종수정일과 비교하여 틀리면 Download받음.
                            string strClientFile = m_strExePath + m_arrSourceFolder[i] + "\\" + strClientFileName; // strSourceFileName;
                            FileInfo strClientFileInfo = new FileInfo(strClientFile);
                            if (strClientFileInfo.Exists == false || strClientFileInfo.LastWriteTime < datSourceFileDate || !strClientFileInfo.Length.Equals(datSourceFileLenth))
                            {
                                //m_arrDownloadDLLFile.Add(m_arrSourceFolder[i] + "/" + strSourceFileName);
                                m_arrDownloadDLLFile.Add(m_arrSourceFolder[i] + "/" + strClientFileName);
                                //SYSPGM.dll, QRPCOM.dll 및 QRPDB.dll 인 경우 Root인 경우도 Down받도록 처리
                                ////////if (strSourceFileName.ToUpper() == "QRPSYS.BL.SYSPGM.DLL" || strSourceFileName.ToUpper() == "QRPCOM.DLL" || strSourceFileName.ToUpper() == "QRPDB.DLL")
                                ////////{
                                ////////    //m_arrDownloadDLLFile.Add(strSourceFileName);
                                ////////    m_arrDownloadDLLFile.Add(strClientFileName);
                                ////////}
                            }
                        }
                    }

                    if (m_arrDownloadDLLFile.Count > 0)
                    {
                        //총Download받을 화일 갯수
                        uTextTotalFileCount.Text = m_arrDownloadDLLFile.Count.ToString();

                        //Upload화일 총진행율을 보여줄 ProgressBar를 설정함.
                        uProgressTotalProg.Minimum = 0;
                        uProgressTotalProg.Maximum = 100 * Convert.ToInt32(uTextTotalFileCount.Text);

                        m_intDownloadIndex = 0;
                        Download_DLLFile();
                    }
                    else
                    {
                        //MessageBox.Show("Don't Exist Download File");
                        RunQRPBrowser();
                    }
                }
                m_resSys.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.InnerException.ToString() + "\n" + ex.Source.ToString());
            }
            finally
            {
            }
        }

        #region 채널생성
        /// <summary>
        /// .NET Remoting을 위한 채널 및 Proxy 생성
        /// </summary>
        /// <param name="TypeName"></param>
        /// <param name="strClassName"></param>
        public void mfRegisterChannel(Type TypeName, string strClassName)
        {
            string RemoteServer = "";
            try
            {
                WellKnownClientTypeEntry[] entry = RemotingConfiguration.GetRegisteredWellKnownClientTypes();
                foreach (WellKnownClientTypeEntry E in entry)
                {
                    if (E.TypeName.ToString() == TypeName.FullName.ToString())
                        return;
                }

                //채널 생성
                bool bolMakeChannel = false;
                foreach (IChannel chn in ChannelServices.RegisteredChannels)
                {
                    if (chn.ChannelName == "http")
                    {
                        bolMakeChannel = true;
                        break;
                    }
                }
                if (bolMakeChannel == false)
                {
                    HttpChannel channel = new HttpChannel();
                    ChannelServices.RegisterChannel(channel, true);
                }

                //RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString();
                //app.config에 서버경로, 언어, 폰트를 저장한다.
                //////////////////////////////////////////////////////////////////////
                // 2012.09.12 변경 (Browser 에서의 App.Config 변경내용 반영이 안되어 아래코드로 수정)
                //////////////////////////////////////////////////////////////////////
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                RemoteServer = config.AppSettings.Settings["RemoteServer"].Value.ToString();

                //RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off; //신규추가
                RemotingConfiguration.RegisterWellKnownClientType(TypeName, RemoteServer + strClassName + ".rem");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// .NET Remoting 인증 설정
        /// </summary>
        /// <param name="obj"></param>
        public void mfCredentials(object obj)
        {
            try
            {
                //char[] sep = { ';' };
                //string[] cre = ConfigurationManager.AppSettings["Credentials"].ToString().Split(sep);
                IDictionary Props = ChannelServices.GetChannelSinkProperties(obj);
                //Props["credentials"] = new NetworkCredential(cre[0], cre[1]);
                Props["credentials"] = CredentialCache.DefaultCredentials;
                Props["preauthenticate"] = true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }
        #endregion

        private void RunQRPBrowser()
        {
            try
            {
                //System.Diagnostics.Process.Start(m_strQRPBrowserPath, m_strExtenalArg);
                Process _p = new Process();
                ProcessStartInfo _sp = new ProcessStartInfo();
                _sp.FileName = m_strQRPBrowserPath;
                _sp.Arguments = m_strExtenalArg;

                _p.StartInfo = _sp;
                _p.Start();

                Application.Exit();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 화일을 WebClient를 통해 Download함
        /// </summary>
        private void Download_DLLFile()
        {
            try
            {
                //권한설정
                NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);

                WebClient wclient = new WebClient();
                wclient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadFileProgress);
                wclient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
                wclient.Encoding = System.Text.Encoding.UTF8;
                wclient.Credentials = cred;
                Uri uri = new Uri(m_strWebURL + m_arrDownloadDLLFile[m_intDownloadIndex]);
                
                string strDownloadPath = "";
                if (m_arrDownloadDLLFile[m_intDownloadIndex].ToString().Contains("/"))
                {
                    strDownloadPath = m_strExePath + m_arrDownloadDLLFile[m_intDownloadIndex].ToString().Replace("/", "\\");
                    //폴더가 없는 경우 폴더 생성
                    string[] sep = { "/" };
                    string[] arrDownloadFolder = m_arrDownloadDLLFile[m_intDownloadIndex].ToString().Split(sep, StringSplitOptions.None);
                    string strDownloadFolder = m_strExePath + arrDownloadFolder[0] + "\\";
                    if (!Directory.Exists(strDownloadFolder))
                        Directory.CreateDirectory(strDownloadFolder);
                }
                else
                    strDownloadPath = m_strExePath + m_arrDownloadDLLFile[m_intDownloadIndex].ToString();

                uTextFileCount.Text = (m_intDownloadIndex + 1).ToString();

                //Download중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                uProgressCurProg.Minimum = 0;
                uProgressCurProg.Maximum = 100; //Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);

                //Download중인 화일명을 보여준다.
                uTextFileName.Text = m_arrDownloadDLLFile[m_intDownloadIndex].ToString();

                //Download시작
                wclient.DownloadFileAsync(uri, strDownloadPath);

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 Download하는 동안 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DownloadFileProgress(Object sender, DownloadProgressChangedEventArgs e)
        {
            try
            {
                //현재 화일의 진행율을 보여줌 
                uProgressCurProg.Value = e.ProgressPercentage;

                //전체 진행율을 보여줌
                uProgressTotalProg.Value = (m_intDownloadIndex+1) * 100 + e.ProgressPercentage;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 Download를 완료할때 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DownloadFileCompleted(Object sender, AsyncCompletedEventArgs e)
        {
            System.Threading.AutoResetEvent waiter = (System.Threading.AutoResetEvent)e.UserState; ;
            try
            {
                m_intDownloadIndex++;
                //Upload 화일이 남아있는 경우
                if (m_intDownloadIndex < m_arrDownloadDLLFile.Count)
                {
                    //권한설정
                    NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);

                    WebClient wclient = new WebClient();
                    wclient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadFileProgress);
                    wclient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
                    wclient.Encoding = System.Text.Encoding.UTF8;
                    wclient.Credentials = cred;
                    Uri uri = new Uri(m_strWebURL + m_arrDownloadDLLFile[m_intDownloadIndex]);

                    string strDownloadPath = "";
                    if (m_arrDownloadDLLFile[m_intDownloadIndex].ToString().Contains("/"))
                    {
                        strDownloadPath = m_strExePath + m_arrDownloadDLLFile[m_intDownloadIndex].ToString().Replace("/", "\\");
                        //폴더가 없는 경우 폴더 생성
                        string[] sep = { "/" };
                        string[] arrDownloadFolder = m_arrDownloadDLLFile[m_intDownloadIndex].ToString().Split(sep, StringSplitOptions.None);
                        string strDownloadFolder = m_strExePath + arrDownloadFolder[0] + "\\";
                        if (!Directory.Exists(strDownloadFolder))
                            Directory.CreateDirectory(strDownloadFolder);
                    }
                    else
                        strDownloadPath = m_strExePath + m_arrDownloadDLLFile[m_intDownloadIndex].ToString();

                    uTextFileCount.Text = (m_intDownloadIndex + 1).ToString();

                    //Download중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                    uProgressCurProg.Minimum = 0;
                    uProgressCurProg.Maximum = 100; //Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);

                    //Download중인 화일명을 보여준다.
                    uTextFileName.Text = m_arrDownloadDLLFile[m_intDownloadIndex].ToString();

                    //Download시작
                    wclient.DownloadFileAsync(uri, strDownloadPath);

                }
                else
                {
                    MessageBox.Show("File Download Complete!");
                    RunQRPBrowser();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                // If this thread throws an exception, make sure that
                // you let the main application thread resume.
                //waiter.Set();
            }
        }
    }
}
