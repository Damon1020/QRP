﻿namespace QRPUpdater
{
    partial class frmQRPUpdater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQRPUpdater));
            this.uProgressTotalProg = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.uProgressCurProg = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.uTextFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTotalFileCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFileCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotalFileCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // uProgressTotalProg
            // 
            this.uProgressTotalProg.Location = new System.Drawing.Point(204, 160);
            this.uProgressTotalProg.Name = "uProgressTotalProg";
            this.uProgressTotalProg.Size = new System.Drawing.Size(188, 20);
            this.uProgressTotalProg.TabIndex = 0;
            this.uProgressTotalProg.Text = "[Formatted]";
            // 
            // uProgressCurProg
            // 
            this.uProgressCurProg.Location = new System.Drawing.Point(204, 124);
            this.uProgressCurProg.Name = "uProgressCurProg";
            this.uProgressCurProg.Size = new System.Drawing.Size(188, 20);
            this.uProgressCurProg.TabIndex = 1;
            this.uProgressCurProg.Text = "[Formatted]";
            // 
            // uTextFileName
            // 
            appearance3.TextHAlignAsString = "Left";
            this.uTextFileName.Appearance = appearance3;
            this.uTextFileName.Location = new System.Drawing.Point(224, 84);
            this.uTextFileName.Name = "uTextFileName";
            this.uTextFileName.ReadOnly = true;
            this.uTextFileName.Size = new System.Drawing.Size(168, 21);
            this.uTextFileName.TabIndex = 2;
            // 
            // uTextTotalFileCount
            // 
            appearance4.TextHAlignAsString = "Right";
            this.uTextTotalFileCount.Appearance = appearance4;
            this.uTextTotalFileCount.Location = new System.Drawing.Point(360, 60);
            this.uTextTotalFileCount.Name = "uTextTotalFileCount";
            this.uTextTotalFileCount.ReadOnly = true;
            this.uTextTotalFileCount.Size = new System.Drawing.Size(32, 21);
            this.uTextTotalFileCount.TabIndex = 3;
            // 
            // uTextFileCount
            // 
            appearance1.TextHAlignAsString = "Right";
            this.uTextFileCount.Appearance = appearance1;
            this.uTextFileCount.Location = new System.Drawing.Point(316, 60);
            this.uTextFileCount.Name = "uTextFileCount";
            this.uTextFileCount.ReadOnly = true;
            this.uTextFileCount.Size = new System.Drawing.Size(32, 21);
            this.uTextFileCount.TabIndex = 4;
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.White;
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(348, 64);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 16);
            this.ultraLabel1.TabIndex = 5;
            this.ultraLabel1.Text = "/";
            // 
            // picLogo
            // 
            this.picLogo.Image = global::QRPUpdater.Properties.Resources.STS_Logo;
            this.picLogo.Location = new System.Drawing.Point(256, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(144, 32);
            this.picLogo.TabIndex = 6;
            this.picLogo.TabStop = false;
            // 
            // frmQRPUpdater
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::QRPUpdater.Properties.Resources.download;
            this.ClientSize = new System.Drawing.Size(400, 201);
            this.ControlBox = false;
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.uTextFileCount);
            this.Controls.Add(this.uTextTotalFileCount);
            this.Controls.Add(this.uTextFileName);
            this.Controls.Add(this.uProgressCurProg);
            this.Controls.Add(this.uProgressTotalProg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmQRPUpdater";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QRP Updater";
            this.Load += new System.EventHandler(this.frmQRPUpdater_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotalFileCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar uProgressTotalProg;
        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar uProgressCurProg;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFileName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTotalFileCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFileCount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.PictureBox picLogo;
    }
}