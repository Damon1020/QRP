﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using QRPCOM.QRPGLO;
using System.Web.Script.Services;
using System.Diagnostics;
using System.IO;

[WebService(Namespace = "http://STS.QRPWebService/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
[System.Web.Script.Services.ScriptService]
public class QRPApproval : System.Web.Services.WebService
{
    public QRPApproval()
    {

        //디자인된 구성 요소를 사용하는 경우 다음 줄의 주석 처리를 제거합니다. 
        //InitializeComponent(); 

    }

    #region Const
    // WebService 입/출력 시 xml Attribute에 필요한 항목
    private const string m_strResult_CD_CODE = "CD_CODE";
    private const string m_strResult_CD_STATUS = "CD_STATUS";
    private const string m_strResult_NO_EMPLOYEE = "NO_EMPLOYEE";
    private const string m_strResult_DS_POSITION = "DS_POSITION";
    private const string m_strResult_CD_LEGACYKEY = "CD_LEGACYKEY";

    // 입력 Return 용 xml string
    private const string m_strResult = "<RESULT></RESULT>";

    #endregion

    #region Util
    private string AddCDATA(string strReturn)
    {
        strReturn = "<![CDATA[" + strReturn + "]]>";
        return strReturn;
    }

    //개발시 작동// 구동시 권한 에러남 // 권한주는방식 다해봐도 안댐 // 한번더 시도할것임 // 결국 실패 안씀
    private void mfSaveWindowEvent(string strLegacyKey, string strDocNo, string strFmpf, string strType)
    {
        //string sSource = "QRPWebService.EventLog";
        //string sLog = "Application";

        //if (!EventLog.SourceExists(sSource))
        //    EventLog.CreateEventSource(sSource, sLog);

        //string msg = "WebServiceType : " + strType; 
        //msg = msg + "LegacyKey : " + strLegacyKey + Environment.NewLine;
        //msg = msg + "DocNo : " + strDocNo + Environment.NewLine;
        //msg = msg + "FmpfKey : " + strFmpf;

        //EventLog.WriteEntry(sSource, msg, EventLogEntryType.Information, 0);
    }

    #endregion

    private XmlNode mfSetApprovalDocument(string strLegacyKey, string strDOCNO, string strFmpf, string strCD_USERKEY)
    {
        //mfSaveWindowEvent(strLegacyKey, strDOCNO, strFmpf, "[ 결재문서 승인 ]");

        string strxml = "<RESULT>" + Environment.NewLine + Environment.NewLine + "</RESULT>";
        XmlDocument xmlResult = new XmlDocument();
        xmlResult.LoadXml(strxml);
        XmlNode _xmlNode = xmlResult.SelectSingleNode("RESULT");

        string strDS_POSITION = "mfSetApprovalDocument()";

        string strErrRtn = string.Empty;
        try
        {
            #region Check 입력 변수 확인 -- 하나라도 들어오지 않으면 에러 리턴 -- CD_CODE = "01"
            if (strLegacyKey.Equals(string.Empty) || strDOCNO.Equals(string.Empty) || strFmpf.Equals(string.Empty) || strCD_USERKEY.Equals(string.Empty))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "01";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
                return _xmlNode;
            }
            #endregion

            #region Action -- 입력받은 변수로 QRPDB에 저장 -- 프로시저 내부에서 strFmpf 값에 따라 테이블 선택 --
            QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
            strErrRtn = clsUser.mfSaveGRWApproval(strLegacyKey, strDOCNO, strFmpf, strCD_USERKEY);
            #endregion

            TransErrRtn ErrRtn = new TransErrRtn();
            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            #region Return -- 결과값 리턴 -- 성공시 CD_CODE = "00" / 에러 CD_CODE = "99"
            if (ErrRtn.ErrNum.Equals(0) && ErrRtn.ErrMessage.Equals(string.Empty))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "00";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "True";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("저장성공");
                return _xmlNode;
            }
            else
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "99";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);
                if (ErrRtn.ErrMessage.Equals(string.Empty))
                    _xmlNode.InnerText = AddCDATA("저장실패");
                else
                    _xmlNode.InnerText = AddCDATA(ErrRtn.ErrMessage);
                return _xmlNode;
            }
            #endregion
        }
        catch (Exception ex)
        {

            throw (ex);
        }
        finally
        {

        }
    }

    private XmlNode mfSetRejectDocument(string strLegacyKey, string strDOCNO, string strFmpf, string strCD_USERKEY)
    {
        //mfSaveWindowEvent(strLegacyKey, strDOCNO, strFmpf, "[ 결재문서 반려 ]");

        string strxml = "<RESULT>" + Environment.NewLine + Environment.NewLine + "</RESULT>";
        XmlDocument xmlResult = new XmlDocument();
        xmlResult.LoadXml(strxml);
        XmlNode _xmlNode = xmlResult.SelectSingleNode("RESULT");

        string strDS_POSITION = "mfSetRejectDocument()";

        string strErrRtn = string.Empty;
        try
        {
            #region Check 입력 변수 확인 -- 하나라도 들어오지 않으면 에러 리턴 -- CD_CODE = "01"
            if (strLegacyKey.Equals(string.Empty) || strDOCNO.Equals(string.Empty) || strFmpf.Equals(string.Empty) || strCD_USERKEY.Equals(string.Empty))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "01";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
                return _xmlNode;
            }
            #endregion

            #region Action -- 입력받은 변수로 QRPDB에 저장 -- 프로시저 내부에서 strFmpf 값에 따라 테이블 선택 --
            QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
            strErrRtn = clsUser.mfSaveGRWReject(strLegacyKey, strDOCNO, strFmpf, strCD_USERKEY);
            #endregion

            TransErrRtn ErrRtn = new TransErrRtn();
            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            #region Return -- 결과값 리턴 -- 성공시 CD_CODE = "00" / 에러 CD_CODE = "99"
            if (ErrRtn.ErrNum.Equals(0) && ErrRtn.ErrMessage.Equals(string.Empty))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "00";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "True";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("저장성공");
                return _xmlNode;
            }
            else
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "99";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                if (ErrRtn.ErrMessage.Equals(string.Empty))
                    _xmlNode.InnerText = AddCDATA("저장실패");
                else
                    _xmlNode.InnerText = AddCDATA(ErrRtn.ErrMessage);

                return _xmlNode;
            }
            #endregion
        }
        catch (Exception ex)
        {

            throw (ex);
        }
        finally
        {

        }
    }

    /// <summary>
    /// [ 결재문서 승인/반려 ]
    /// </summary>
    /// <param name="xmlRESULT"></param>
    /// <returns>xml node 리턴
    ///          CD_CODE : 성공여부 [ 0/1/99 ]
    ///          CD_STATUS : 성공여부 [ STRING ]
    ///          DS_POSITION : Method 명
    ///          CD_LEGACYKEY : 입력받은 LegacyKey</returns>
    [WebMethod(Description = "[ 결재문서 승인/반려 ]", MessageName = "mfSetDocument")]
    public XmlNode mfSetDocument(XmlDocument xmlRESULT)
    {
        string strxml = "<RESULT>" + Environment.NewLine + Environment.NewLine + "</RESULT>";
        XmlDocument xmlRtn = new XmlDocument();
        xmlRtn.LoadXml(strxml);
        XmlNode _xmlNode = xmlRtn.SelectSingleNode("RESULT");
        string strDS_POSITION = "mfSetDocument()";

        File.AppendAllText(@"d:\WebLog.txt",Environment.NewLine + DateTime.Now.ToString("yyyyMMddHHmmssfffff") + "\t" + xmlRESULT.OuterXml.ToString() + Environment.NewLine);

        #region 변수 만들기
        string strLegacyKey = string.Empty;
        string strDOCNO = string.Empty;
        string strFmpf = string.Empty;
        string strSetFlag = string.Empty;
        string strUSERKEY = string.Empty;

        try
        {
            strLegacyKey = xmlRESULT.SelectSingleNode("RESULT/strLegacyKey").InnerText;
            strDOCNO = xmlRESULT.SelectSingleNode("RESULT/strDocNo").InnerText;
            strFmpf = xmlRESULT.SelectSingleNode("RESULT/strFmpf").InnerText;
            strSetFlag = xmlRESULT.SelectSingleNode("RESULT/strSetFlag").InnerText;
            strUSERKEY = xmlRESULT.SelectSingleNode("RESULT/strUserKey").InnerText;
        }
        catch
        {
            XmlAttribute _xmlAtt = xmlRtn.CreateAttribute(m_strResult_CD_CODE);
            _xmlAtt.Value = "08";
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlAtt = xmlRtn.CreateAttribute(m_strResult_CD_STATUS);
            _xmlAtt.Value = "False";
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlAtt = xmlRtn.CreateAttribute(m_strResult_DS_POSITION);
            _xmlAtt.Value = strDS_POSITION;
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
            return _xmlNode;
        }

        if (strLegacyKey.Equals(string.Empty) ||
            strDOCNO.Equals(string.Empty) ||
            strFmpf.Equals(string.Empty) ||
            strSetFlag.Equals(string.Empty) ||
            strUSERKEY.Equals(string.Empty))
        {
            XmlAttribute _xmlAtt = xmlRtn.CreateAttribute(m_strResult_CD_CODE);
            _xmlAtt.Value = "07";
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlAtt = xmlRtn.CreateAttribute(m_strResult_CD_STATUS);
            _xmlAtt.Value = "False";
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlAtt = xmlRtn.CreateAttribute(m_strResult_DS_POSITION);
            _xmlAtt.Value = strDS_POSITION;
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
            return _xmlNode;
        }

        #endregion

        // 결재 승인
        if (strSetFlag.Equals("Y") || strSetFlag.Equals("T"))
        {
            XmlNode xml = mfSetApprovalDocument(strLegacyKey, strDOCNO, strFmpf, strUSERKEY);
            return xml;
        }
        // 결재 반려
        else if (strSetFlag.Equals("N") || strSetFlag.Equals("F"))
        {
            XmlNode xml = mfSetRejectDocument(strLegacyKey, strDOCNO, strFmpf, strUSERKEY);
            return xml;
        }
        // 이도 저도 아님
        else
        {
            XmlAttribute _xmlAtt = xmlRtn.CreateAttribute(m_strResult_CD_CODE);
            _xmlAtt.Value = "01";
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlAtt = xmlRtn.CreateAttribute(m_strResult_CD_STATUS);
            _xmlAtt.Value = "False";
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlAtt = xmlRtn.CreateAttribute(m_strResult_DS_POSITION);
            _xmlAtt.Value = strDS_POSITION;
            _xmlNode.Attributes.Append(_xmlAtt);

            _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
            return _xmlNode;


        }
    }
}
