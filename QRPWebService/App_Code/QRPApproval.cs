﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using QRPCOM.QRPGLO;


[WebService(Namespace = "http://STS.QRPWebService/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
// [System.Web.Script.Services.ScriptService]
public class QRPApproval : System.Web.Services.WebService
{
    public QRPApproval()
    {

        //디자인된 구성 요소를 사용하는 경우 다음 줄의 주석 처리를 제거합니다. 
        //InitializeComponent(); 
        
    }

    #region Const
    private const string m_strFmpf_ERP = "WF_STS_ERP02";
    private const string m_strFmpf_CreateISO = "WF_STS_FORM50_V0";
    private const string m_strFmpf_CancelISO = "WF_STS_FORM83";
    private const string m_strFmpf_Normal = "WF_STS_FORM101";

    private const string m_strResult_CD_CODE = "CD_CODE";
    private const string m_strResult_CD_STATUS = "CD_STATUS";
    private const string m_strResult_NO_EMPLOYEE = "NO_EMPLOYEE";
    private const string m_strResult_DS_POSITION = "DS_POSITION";
    private const string m_strResult_CD_LEGACYKEY = "CD_LEGACYKEY";

    private const string m_strResult = "<RESULT></RESULT>";
    
    #endregion

    #region Util
    private string AddCDATA(string strReturn)
    {
        strReturn = "<![CDATA[" + strReturn + "]]>";
        return strReturn;
    }
    #endregion

    [WebMethod(Description="[ 결재문서 승인 ]")]
    public XmlNode mfSetApprovalDocument(string strLegacyKey, string strDOCNO, string strFmpf, string strComment)
    {
        XmlDocument xmlResult = new XmlDocument();
        xmlResult.LoadXml(m_strResult);
        XmlNode _xmlNode = xmlResult.SelectSingleNode("RESULT");

        string strDS_POSITION = "mfSetApprovalDocument()";

        string strErrRtn = string.Empty;
        try
        {
            #region Check
            if (strLegacyKey.Equals(string.Empty) || strDOCNO.Equals(string.Empty) || strFmpf.Equals(string.Empty))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "01";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
                return _xmlNode;
            }
            #endregion

            #region Action
            QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
            strErrRtn = clsUser.mfSaveGRWApproval(strLegacyKey, strDOCNO, strFmpf, strComment);
            #endregion
            
            TransErrRtn ErrRtn = new TransErrRtn();
            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            #region Return
            if (ErrRtn.ErrNum.Equals("0"))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "00";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "True";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("저장성공");
                return _xmlNode;
            }
            else
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = ErrRtn.ErrNum.ToString("##");
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA(ErrRtn.ErrMessage);
                return _xmlNode;
            }
            #endregion
        }
        catch (Exception ex)
        {

            throw(ex);
        }
        finally
        {

        }  
    }

    [WebMethod(Description="[ 결재문서 반려 ]")]
    public XmlNode mfSetRejectDocument(string strLegacyKey, string strDOCNO, string strFmpf, string strComment)
    {
        XmlDocument xmlResult = new XmlDocument();
        xmlResult.LoadXml(m_strResult);
        XmlNode _xmlNode = xmlResult.SelectSingleNode("RESULT");

        string strDS_POSITION = "mfSetRejectDocument()";

        string strErrRtn = string.Empty;
        try
        {
            #region Check
            if (strLegacyKey.Equals(string.Empty) || strDOCNO.Equals(string.Empty) || strFmpf.Equals(string.Empty))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "01";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("결재 양식 FORM PREFIX 코드 값을 입력하십시오. ");
                return _xmlNode;
            }
            #endregion

            #region Action
            QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
            strErrRtn = clsUser.mfSaveGRWReject(strLegacyKey, strDOCNO, strFmpf, strComment);
            #endregion

            TransErrRtn ErrRtn = new TransErrRtn();
            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            #region Return
            if (ErrRtn.ErrNum.Equals("0"))
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = "00";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "True";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA("저장성공");
                return _xmlNode;
            }
            else
            {
                XmlAttribute _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_CODE);
                _xmlAtt.Value = ErrRtn.ErrNum.ToString("##");
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_STATUS);
                _xmlAtt.Value = "False";
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_CD_LEGACYKEY);
                _xmlAtt.Value = strLegacyKey;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlAtt = xmlResult.CreateAttribute(m_strResult_DS_POSITION);
                _xmlAtt.Value = strDS_POSITION;
                _xmlNode.Attributes.Append(_xmlAtt);

                _xmlNode.InnerText = AddCDATA(ErrRtn.ErrMessage);
                return _xmlNode;
            }
            #endregion
        }
        catch (Exception ex)
        {

            throw(ex);
        }
        finally
        {

        }  
    }
}
