﻿using System;
using System.Windows.Forms;
using System.Xml;
using System.Data;
using QRPGRW.BL.GRWUSR;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using System.EnterpriseServices;
using System.Threading;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPGRW.IF
{
    /// <summary>
    /// 그룹웨어 - Brains - 인터페이스용 클래스
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("QRPGRW")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class QRPGRW : ServicedComponent
    {
        #region const
        private const string m_strFmpf_CreateISO = "WF_STS_JEGEJUNG";
        private const string m_strFmpf_CancelISO = "WF_STS_PEJI";
        private const string m_strFmpf_Normal = "WF_STS_QRPGIAN";
        private const string m_strFmpf_EquipDiscard = "WF_STS_EQPDEL";
        private const string m_strFmpf_Material = "WF_STS_MATJEGUM";
        private const string m_strFmpf_EquipPM = "WF_STS_EQPCHECK_INFO";

        private const string strSystemKey = "AP47ST36-PR1F-6X9Q-C5X9-3PLUJX3D5XTM8D";

        private const string m_xmlSendApvLine = "xmlSendApvLine";
        private const string m_attSendApvLine_APV = "APV";
        private const string m_attSendApvLine_CD_KIND = "CD_KIND";
        private const string m_attSendApvLine_CD_COMPANY = "CD_COMPANY";
        private const string m_attSendApvLine_CD_USERKEY = "CD_USERKEY";

        private const string m_xmlCcLine = "xmlCcLine";
        private const string m_attCcLine_APV = "APV";
        private const string m_attCcLine_CD_KIND = "CD_KIND";
        private const string m_attCcLine_CD_COMPANY = "CD_COMPANY";
        private const string m_attCcLine_CD_USERKEY = "CD_USERKEY";

        private const string m_xmlReceiveApvLine = "";

        private const string m_xmlFormInfo = "<FormInfo>\n" +
                                        "\t<UserType></UserType>\n" +
                                        "\t<SaveTerm></SaveTerm>\n" +
                                        "\t<DocLevel></DocLevel>\n" +
                                        "\t<DocClassId></DocClassId>\n" +
                                        "\t<DocClassName></DocClassName>\n" +
                                        "\t<DocNo></DocNo>\n" +
                                        "\t<EntName></EntName>\n" +
                                    "</FormInfo>";

        private const string m_strFormInfo_UserType = "UserType";
        private const string m_strFormInfo_SaveTerm = "SaveTerm";
        private const string m_strFormInfo_DocLevel = "DocLevel";
        private const string m_strFormInfo_DocClassId = "DocClassId";
        private const string m_strFormInfo_DocClassName = "DocClassName";
        private const string m_strFormInfo_DocNo = "DocNo";
        private const string m_strFormInfo_EntName = "EntName";

        private const string m_strResult_CD_CODE = "CD_CODE";
        private const string m_strResult_CD_STATUS = "CD_STATUS";
        private const string m_strResult_CD_LEGACYKEY = "CD_LEGACYKEY";
        private const string m_strResult_NO_EMPLOYEE = "NO_EMPLOYEE";
        private const string m_strResult_DS_POSITION = "DS_POSITION";
        private const string m_strResult_MSG = "MSG";

        private const string m_strISODOCSubject = "표준 제 /개정 신청서";
        private const string m_strISODOCCancelSubject = "표준 폐지 신청서";
        private const string m_strMaterialSubject = "자재 재검토 의뢰서 : 특채정보";
        private const string m_strEquipSubject = "설비인증의뢰";
        private const string m_strEquipDiscardSubject = "설비폐기";
        private const string m_strEquipPM = "설비점검정보등록";

        private const string m_strBrainsFileSvrPath = "";

        private enum SendApvLine_CD_KIND
        {
            SI,     // : Send Initiator 기안자
            SA,     // : Send Approver  일반결재
            SS,     // : Send Serial    순차합의
            SP,     // : Send Parallel  병렬합의
            RI      // : 담당자         담당자
        }
        private enum CcLine_CD_KIND
        {
            CP, // : Cc Person
            CD  // : Cc Department
        }

        private const string m_strFile = "<FileInfo></FileInfo>";
        private const string m_strFile_Legacy = "LegacyInfo";
        private const string m_strFile_Legacy_AttCompany = "CD_COMPANY";
        private const string m_strFile_Legacy_AttACTION = "CD_ACTION";
        private const string m_strFile_Legacy_AttLINK = "IS_LINK";
        private const string m_strFile_Files = "Files";
        private const string m_strFile_Files_File = "File";
        private const string m_strFile_Files_File_AttNM = "NM_FILE";
        private const string m_strFile_Files_File_AttLOCATION = "NM_LOCATION";
        private const string m_strFile_SendUri_Test = @"\\gwdev.mybokwang.com";
        private const string m_strFile_SendUri = @"\\gwfile1.mybokwang.com";
        //private const string m_strFile_ID = @"FileAccess_StsQrp@mybokwang.com";
        //private const string m_strFile_Pwd = @"fileaccessqrp!@#";
        //private const string m_strFile_SendUriFolder = @"\WebSvcAttachTemp250";
        private const string m_strFile_ID = @"FileAccess_PstsQrp@mybokwang.com";
        private const string m_strFile_Pwd = @"fileaccessqrp!@#";
        private const string m_strFile_SendUriFolder = @"WebSvcAttachTemp251";
        private const string m_strFile_SendUriFolder_Test = @"\WebSvcAttachTemp250";


        #region PSTS 사용
        //PSTS SystemKey
        private const string strPSTSSystemKey = "PS763128-T2BS-6FQ3-S519-QE5OJPDP5SIDKR";
        private const string strPSTSGRWKey = "6B365D45-217F-4B8A-977F-EC0697E6AF2BBK";

        private const string m_xmlGetUserInfo = "<GetUser>\n" + "\t<strCdKind>A</strCdKind>\n" +
                                                    "\t<strCdSearchType>!1!</strCdSearchType>\n" +
                                                    "\t<strCdCompany>!2!</strCdCompany>\n" +
                                                    "\t<strCdUserKey>!3!</strCdUserKey>\n" +
                                                    "\t<strCdSystemKey>PS763128-T2BS-6FQ3-S519-QE5OJPDP5SIDKR</strCdSystemKey>\n" +
                                                "</GetUser>\n";
        #endregion

        #endregion

        #region Util
        private DataTable ResultSetDraftForm()
        {
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add(m_strResult_CD_CODE, typeof(string));
            dtReturn.Columns.Add(m_strResult_CD_STATUS, typeof(string));
            dtReturn.Columns.Add(m_strResult_CD_LEGACYKEY, typeof(string));
            dtReturn.Columns.Add(m_strResult_NO_EMPLOYEE, typeof(string));
            dtReturn.Columns.Add(m_strResult_DS_POSITION, typeof(string));
            dtReturn.Columns.Add(m_strResult_MSG, typeof(string));

            return dtReturn;
        }

        private string RemoveCDATA(string strReturn)
        {
            strReturn.Replace("<![CDATA[", "").Replace("]]>", "");
            return strReturn;
        }

        private string ConvertToHtml(string strCode)
        {
            strCode = strCode.Replace("\r\n", "<br/>");
            strCode = strCode.Replace("\n", "<br/>");
            return strCode;
        }

        private string SaveSetDraftForm(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strFmpf, string strUserIP, string strUserID)
        {

            System.IO.StreamWriter log = new System.IO.StreamWriter("D:/" + "SaveSetDraftForm.txt", true);
            try
            {
                //TransErrRtn ErrRtn = new TransErrRtn();

                string strErrRtn = string.Empty;
                GRWUser clsUser = new GRWUser();
                strErrRtn = clsUser.mfSaveSetDraftForm(strLegacyKey, dtSendLine, dtCcLine, dtFormInfo, strFmpf, strUserIP, strUserID);

                log.WriteLine("strErrRtn"+strErrRtn);

                //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                string[] arrErrSep = { "<Err>" };
                string rtn = strErrRtn.Split(arrErrSep, StringSplitOptions.None)[0].ToString();

                log.WriteLine("rtn:"+rtn);

                return rtn;

            }
            catch (Exception ex)
            {
                log.WriteLine("ex99");
                return "99";
                throw(ex);
            }
            finally
            {
                log.Close(); 
            }  
        }
        private void SaveSetDraftForm_F(string strLegacyKey, string strFmpf, string strUserIP, string strUsreID)
        {
            try
            {
                //TransErrRtn ErrRtn = new TransErrRtn();

                string strErrRtn = string.Empty;
                GRWUser clsUser = new GRWUser();
                strErrRtn = clsUser.mfSaveSetDraftForm_F(strLegacyKey, strFmpf, strUserIP, strUsreID);
                string[] arrErrSep = { "<Err>" };
                string rtn = strErrRtn.Split(arrErrSep, StringSplitOptions.None)[0].ToString();
                //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
            }
            catch (Exception ex)
            {
                string sSource = "QRPGRW.IF.EventLog";
                string sLog = "Application";

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);

                EventLog.WriteEntry(sSource, ex.Message.ToString(), EventLogEntryType.Error, 55);
                throw (ex);
            }
            finally
            {
            }  
        }

        private DataTable mfSetSendLineDataTable()
        {
            DataTable _Rtn = new DataTable();
            _Rtn.Columns.Add("CD_KIND", typeof(string));
            _Rtn.Columns.Add("CD_COMPANY", typeof(string));
            _Rtn.Columns.Add("CD_USERKEY", typeof(string));
            _Rtn.Columns.Add("UserID", typeof(string));
            return _Rtn;
        }
        private DataTable mfSetCcLineDataDable()
        {
            DataTable _Rtn = new DataTable();
            _Rtn.Columns.Add("CD_KIND", typeof(string));
            _Rtn.Columns.Add("CD_COMPANY", typeof(string));
            _Rtn.Columns.Add("CD_USERKEY", typeof(string));
            _Rtn.Columns.Add("UserID", typeof(string));
            return _Rtn;
        }
        private DataTable mfSetFormInfoDataTable()
        {
            DataTable _Rtn = new DataTable();
            _Rtn.Columns.Add("UserType", typeof(string));
            _Rtn.Columns.Add("SaveTerm", typeof(string));
            _Rtn.Columns.Add("DocLevel", typeof(string));
            _Rtn.Columns.Add("DocClassId", typeof(string));
            _Rtn.Columns.Add("DocClassName", typeof(string));
            _Rtn.Columns.Add("DocNo", typeof(string));
            _Rtn.Columns.Add("EntName", typeof(string));
            return _Rtn;
        }
        /// <summary>
        /// Brains 전숑용 파일정보 DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetFileInfoDataTable()
        {
            DataTable _Rtn = new DataTable();
            _Rtn.Columns.Add("PlantCode", typeof(string));
            _Rtn.Columns.Add(m_strFile_Files_File_AttNM, typeof(string));
            _Rtn.Columns.Add(m_strFile_Files_File_AttLOCATION, typeof(string));

            return _Rtn;
        }

        private string MoveFiles(DataTable dtFileInfo)
        {
            System.IO.StreamWriter log = new System.IO.StreamWriter("D:/" + "MoveFiles.txt", true);
            log.WriteLine("--begin--");
            //File.AppendAllText : 에러 확인용 filetransfer.txt에 메세지 기록
            try
            {
                int j = 0;
                int i = 99;

                while (!i.Equals(0)) // 5번까지 시도
                {
                    //i = Connect(m_strFile_SendUri_Test, m_strFile_ID, m_strFile_Pwd, null); // 테스트
                    i = Connect(m_strFile_SendUri, m_strFile_ID, m_strFile_Pwd, null); // 실제 사용

                    log.WriteLine("i:"+i);

                    j += 1;

                    log.WriteLine("j:"+j);

                    if (j.Equals(5))
                    {
                        log.WriteLine("j.Equals(5)--break");
                        break;
                    }
                }

                if (!i.Equals(0)) // 연결 실패
                {
                    //File.AppendAllText(@"d:\QRP_STS_Dll\filetransfer.txt", "연결실패");
                    return i.ToString();
                }
                else // 파일 전송
                {
                    log.WriteLine("dtFileInfo.Rows.Count:"+dtFileInfo.Rows.Count);
                    foreach (DataRow dr in dtFileInfo.Rows)
                    {
                        log.WriteLine("--foreach---dtFileInfo-");
                        string[] strLoc;
                        //File.AppendAllText(@"d:\QRP_STS_Dll\filetransfer.txt", "D:\\QRP_STS_FileSvr\\UploadFile\\" + dr[m_strFile_Files_File_AttLOCATION].ToString() + "\t" + dr[m_strFile_Files_File_AttNM].ToString() + Environment.NewLine);


                        strLoc = Directory.GetFiles("D:\\QRP_PSTS_FileSvr\\" + dr[m_strFile_Files_File_AttLOCATION].ToString(), dr[m_strFile_Files_File_AttNM].ToString());
                        //bool blcheck = File.Exists(strLoc[0]);

                        while (strLoc.Length <= 0)
                        {
                            Thread.Sleep(1000);
                            strLoc = Directory.GetFiles("D:\\QRP_PSTS_FileSvr\\\\" + dr[m_strFile_Files_File_AttLOCATION].ToString(), dr[m_strFile_Files_File_AttNM].ToString());
                        }

                        if (strLoc.Length > 0)
                        {
                            try
                            {
                                File.Copy(strLoc[0], m_strFile_SendUri + m_strFile_SendUriFolder + @"\" + dr[m_strFile_Files_File_AttNM], true); // 실제 사용
                                //File.AppendAllText(@"d:\QRP_STS_Dll\filetransfer.txt", strLoc[0] + Environment.NewLine + m_strFile_SendUri + m_strFile_SendUriFolder + @"\" + dr[m_strFile_Files_File_AttNM] + Environment.NewLine);
                            }
                            catch (Exception ex)
                            {
                                return "99";
                                throw(ex);
                            }
                        }
                        else // WebClient 파일서버에서 다운받는당
                        {
                            //일단 보류
                            //File.AppendAllText(@"d:\QRP_STS_Dll\filetransfer.txt", "파일없음");
                            return "88";
                        }
                    }
                    log.WriteLine("--return 00--");
                    return "00";
                }
            }
            catch (Exception ex)
            {
                log.WriteLine("--ex-99-:"+ex.ToString()+"--ex-99-");
                //File.AppendAllText(@"d:\QRP_STS_Dll\filetransfer.txt", "연결실패");
                return "99";
                throw(ex);
            }
            finally
            {
                //DisConnect(m_strFile_SendUri_Test); // 테스트용
                DisConnect(m_strFile_SendUri); // 실제 사용
                log.Close();
            }
        }

        #region Unc 연결 -- 사용금지 --
        private const int CONNECT_UPDATE_PROFILE = 0x1;
        private const int NO_ERROR = 0;

        /// <summary>
        /// Brains 파일 전송용 구조체 - 사용금지 -
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct NETRESOURCE
        {
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public uint dwScope;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public uint dwType;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public uint dwDisplayType;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public uint dwUsage;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public string lpLocalName;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public string lpRemoteName;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public string lpComment;
            /// <summary>
            /// - 사용금지 -
            /// </summary>
            public string lpProvider;
        }
        [DllImport("mpr.dll", CharSet = CharSet.Auto)]
        internal static extern int WNetUseConnection(
                    IntPtr hwndOwner,
                    [MarshalAs(UnmanagedType.Struct)] ref NETRESOURCE lpNetResource,
                    string lpPassword,
                    string lpUserID,
                    uint dwFlags,
                    StringBuilder lpAccessName,
                    ref int lpBufferSize,
                    out uint lpResult);

        [DllImport("mpr.dll", CharSet = CharSet.Auto)]
        internal static extern int WNetCancelConnection2(string lpName, Int32 dwFlags, bool bForce);

        /// <summary>
        /// 공유 폴더에 대한 네트워크 연결을 만든다.        
        /// 그 드라이브 이름은 StringBuilder 를 통해 값이 반환된다.
        /// </summary>
        /// <param name="remoteAccessUri">원격 공유폴더 경로</param>
        /// <param name="remoteUserId">원격 사용자 아이디</param>
        /// <param name="remotePassword">원격 비밀번호</param>
        /// <param name="localDriveName">사용가능한 드라이브 명, NULL일때 시스템이 자동으로 설정한다.</param>
        /// <returns>
        /// 0    : 성공 (0 이 아닌 값은 오류가 발생했음을 알리는 오류 코드)
        /// 85   : 네트드라이버를 설정할때, 네트워크 드라이버가 이미사용중일 때(시스템에 자동으로 생성되게 하면 localDriveName에 NULL를 입력 sb로 반환한다.)
        /// 234  : capacity 값은 공유 폴더의 경로를 담을 수 있도록 충분히 주어야 한다. 그렇지 않으면 오류 코드 234를 반환할 것이다.
        /// 1203 : 공유폴더경로 오류
        /// 1326 : 사용자/암호가 일치 하지 않는다.
        /// </returns>
        static private int Connect(string remoteAccessUri, string remoteUserId, string remotePassword, string localDriveName)
        {
            int capacity = 64;
            uint resultFlags = 0;
            // flags 가 0x80 이 아닌 값이 사용되면 StringBuilder는 일반적으로 공유 폴더의 UNC 이름을 반환한다. 
            uint flags = 0;
            System.Text.StringBuilder sb = new System.Text.StringBuilder(capacity);

            NETRESOURCE ns = new NETRESOURCE();
            ns.dwType = 1;           // 공유 디스크
            ns.lpLocalName = localDriveName;   // 로컬 드라이브 지정하지 않음
            ns.lpRemoteName = remoteAccessUri;
            ns.lpProvider = null;
            int result = WNetUseConnection(IntPtr.Zero,
                ref ns,
                remotePassword,
                remoteUserId,
                flags,
                sb,
                ref capacity,
                out resultFlags);
            return result;
        }

        static private int DisConnect(string remoteAccessUri)
        {
            int result = WNetCancelConnection2(remoteAccessUri, CONNECT_UPDATE_PROFILE, true);
            if (result != NO_ERROR)
            {
                //handle error
            }
            else
            {

            }

            return result;
        }
        #endregion

        /// <summary>
        /// ERP 연동 기안지 
        /// </summary>
        /// <param name="strLegacyKey"></param>
        /// <param name="dtSendLine"></param>
        /// <param name="dtCcLine"></param>
        /// <param name="strSubject"></param>
        /// <param name="strContentElement"></param>
        /// <param name="strComment"></param>
        /// <param name="dtFormInfo"></param>
        /// <param name="dtFileInfo"></param>
        /// <param name="strFmpf"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private DataTable SetDraftForm(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine, string strSubject, string strContentElement, string strComment, DataTable dtFormInfo, DataTable dtFileInfo, string strFmpf, string strUserIP, string strUserID)
        {
            System.IO.StreamWriter log = new System.IO.StreamWriter("D:/" + "materialSpecial.txt", true);

            DataTable dtResult = ResultSetDraftForm();
            try
            {
                #region QRP 저장
                string strRtn = SaveSetDraftForm(strLegacyKey, dtSendLine, dtCcLine, dtFormInfo, strFmpf, strUserIP, strUserID);
                

                log.WriteLine("-------begin-------");
                log.WriteLine(strRtn);


                if (!strRtn.Equals("0") && !strRtn.Equals("00"))
                {
                    dtResult = ResultSetDraftForm();
                    DataRow dr = dtResult.NewRow();
                    dr[m_strResult_CD_CODE] = "66";
                    dr[m_strResult_CD_STATUS] = "False";
                    dr[m_strResult_CD_LEGACYKEY] = strLegacyKey;
                    dr[m_strResult_NO_EMPLOYEE] = string.Empty;
                    dr[m_strResult_DS_POSITION] = "QRPError";
                    dtResult.Rows.Add(dr);
                    return dtResult;
                }
                #endregion

                #region File 보내기
                string _rtn = MoveFiles(dtFileInfo);
                log.WriteLine("_rtn:" + _rtn);
                #endregion

                if (_rtn.Equals("00"))
                {
                    //Test.LegacyApproval ERP = new global::QRPGRW.IF.Test.LegacyApproval(); // 테스트용
                    Brains.LegacyApproval ERP = new global::QRPGRW.IF.Brains.LegacyApproval(); // 실제 사용
                    log.WriteLine("--1--");
                    XmlDocument _xml = new XmlDocument();
                    XmlAttribute _att;

                    #region SendApvLine
                    string strSendLine = string.Empty;
                    foreach (DataRow dr in dtSendLine.Rows)
                    {
                        XmlNode _child = _xml.CreateElement(m_attSendApvLine_APV);

                        if (dr.Table.Columns.Contains(m_attSendApvLine_CD_KIND))
                        {
                            _att = _xml.CreateAttribute(m_attSendApvLine_CD_KIND);
                            _att.Value = dr[m_attSendApvLine_CD_KIND].ToString();
                            _child.Attributes.Append(_att);
                        }
                        if (dr.Table.Columns.Contains(m_attSendApvLine_CD_COMPANY))
                        {
                            _att = _xml.CreateAttribute(m_attSendApvLine_CD_COMPANY);
                            _att.Value = dr[m_attSendApvLine_CD_COMPANY].ToString();
                            _child.Attributes.Append(_att);
                        }
                        if (dr.Table.Columns.Contains(m_attSendApvLine_CD_USERKEY))
                        {
                            _att = _xml.CreateAttribute(m_attSendApvLine_CD_USERKEY);
                            _att.Value = dr[m_attSendApvLine_CD_USERKEY].ToString();
                            _child.Attributes.Append(_att);
                        }
                        strSendLine = strSendLine + _child.OuterXml.ToString();
                        //_xml.AppendChild(_child);
                    }

                    //string strSendLine = _xml.OuterXml.ToString();
                    #endregion
                    log.WriteLine("--2--");
                    #region CcLine

                    _xml = new XmlDocument();
                    //_param = _xml.CreateElement("param");
                    //_att = _xml.CreateAttribute("name");
                    //_att.Value = m_xmlCcLine;
                    //_param.Attributes.Append(_att);
                    string strCcLine = string.Empty;
                    foreach (DataRow dr in dtCcLine.Rows)
                    {
                        XmlNode _child = _xml.CreateElement(m_attCcLine_APV);

                        if (dr.Table.Columns.Contains(m_attCcLine_CD_KIND))
                        {
                            _att = _xml.CreateAttribute(m_attCcLine_CD_KIND);
                            _att.Value = dr[m_attCcLine_CD_KIND].ToString();
                            _child.Attributes.Append(_att);
                        }
                        if (dr.Table.Columns.Contains(m_attCcLine_CD_COMPANY))
                        {
                            _att = _xml.CreateAttribute(m_attCcLine_CD_COMPANY);
                            _att.Value = dr[m_attCcLine_CD_COMPANY].ToString();
                            _child.Attributes.Append(_att);
                        }
                        if (dr.Table.Columns.Contains(m_attCcLine_CD_USERKEY))
                        {
                            _att = _xml.CreateAttribute(m_attCcLine_CD_USERKEY);
                            _att.Value = dr[m_attCcLine_CD_USERKEY].ToString();
                            _child.Attributes.Append(_att);
                        }
                        strCcLine = strCcLine + _child.OuterXml.ToString();
                        //_xml.AppendChild(_child);
                    }

                    //string strCcLine = _xml.OuterXml.ToString();
                    #endregion
                    log.WriteLine("--3--");
                    #region FormInfo
                    _xml.LoadXml(m_xmlFormInfo);
                    //요청에 따른 LegacyKey 형태 변경(그룹웨어에서 전자결제에러시, 이부분 삭제)
                    //string strReplaceLegacy = strLegacyKey.Replace("||", "-");

                    foreach (DataRow dr in dtFormInfo.Rows)
                    {
                        string str = string.Empty;
                        if (dr.Table.Columns.Contains(m_strFormInfo_UserType))
                        {
                            str = "FormInfo/" + m_strFormInfo_UserType;// +"/";
                            _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_UserType].ToString();
                        }
                        if (dr.Table.Columns.Contains(m_strFormInfo_SaveTerm))
                        {
                            str = "FormInfo/" + m_strFormInfo_SaveTerm;// +"/";
                            _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_SaveTerm].ToString();
                        }
                        if (dr.Table.Columns.Contains(m_strFormInfo_DocLevel))
                        {
                            str = "FormInfo/" + m_strFormInfo_DocLevel;// +"/";
                            _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_DocLevel].ToString();
                        }
                        if (dr.Table.Columns.Contains(m_strFormInfo_DocClassId))
                        {
                            str = "FormInfo/" + m_strFormInfo_DocClassId;// +"/";
                            _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_DocClassId].ToString();
                        }
                        if (dr.Table.Columns.Contains(m_strFormInfo_DocClassName))
                        {
                            str = "FormInfo/" + m_strFormInfo_DocClassName;// +"/";
                            _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_DocClassName].ToString();
                        }
                        if (dr.Table.Columns.Contains(m_strFormInfo_DocNo))
                        {
                            str = "FormInfo/" + m_strFormInfo_DocNo;// +"/";
                            _xml.SelectSingleNode(str).InnerText = strLegacyKey;
                        }
                        if (dr.Table.Columns.Contains(m_strFormInfo_EntName))
                        {
                            str = "FormInfo/" + m_strFormInfo_EntName;// +"/";
                            _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_EntName].ToString();
                        }
                    }

                    string strFormInfo = _xml.OuterXml.ToString();
                    #endregion
                    log.WriteLine("--4--");
                    #region FileInfo
                    _xml.LoadXml(m_strFile);
                    XmlNode _file = _xml.CreateElement(m_strFile_Legacy);

                    XmlAttribute _fileatt = _xml.CreateAttribute(m_strFile_Legacy_AttCompany);
                    _fileatt.Value = dtSendLine.Rows[0][m_strFile_Legacy_AttCompany].ToString();
                    _file.Attributes.Append(_fileatt);

                    _fileatt = _xml.CreateAttribute(m_strFile_Legacy_AttACTION);
                    _fileatt.Value = "MOVE";
                    _file.Attributes.Append(_fileatt);

                    _fileatt = _xml.CreateAttribute(m_strFile_Legacy_AttLINK);
                    _fileatt.Value = "FALSE";
                    _file.Attributes.Append(_fileatt);

                    _xml.SelectSingleNode("FileInfo").AppendChild(_file);

                    _file = _xml.CreateElement(m_strFile_Files);
                    _xml.SelectSingleNode("FileInfo").AppendChild(_file);

                    foreach (DataRow dr in dtFileInfo.Rows)
                    {
                        XmlNode _child = _xml.CreateElement(m_strFile_Files_File);

                        if (dr.Table.Columns.Contains(m_strFile_Files_File_AttNM))
                        {
                            _fileatt = _xml.CreateAttribute(m_strFile_Files_File_AttNM);
                            _fileatt.Value = dr[m_strFile_Files_File_AttNM].ToString();
                            _child.Attributes.Append(_fileatt);
                        }
                        if (dr.Table.Columns.Contains(m_strFile_Files_File_AttLOCATION))
                        {
                            _fileatt = _xml.CreateAttribute(m_strFile_Files_File_AttLOCATION);
                            //_fileatt.Value = string.Empty;
                            _fileatt.Value = dr[m_strFile_Files_File_AttLOCATION].ToString();
                            _child.Attributes.Append(_fileatt);
                        }
                        _xml.SelectSingleNode("FileInfo/Files").AppendChild(_child);
                    }

                    string strfileinfo = _xml.OuterXml.ToString();

                    if (dtFileInfo.Rows.Count.Equals(0))
                        strfileinfo = string.Empty;
                    #endregion
                    log.WriteLine("--5--");
                    #region Result

                    XmlNode _return = ERP.SetDraftForm(strLegacyKey, strFmpf, strSendLine, m_xmlReceiveApvLine, strCcLine,
                        strSubject, strContentElement, strComment, strFormInfo, strfileinfo, strPSTSGRWKey);
                    log.WriteLine("--6--");
                    //로그
                    WriteTempLog("LegacyCode : " + strLegacyKey);
                    WriteTempLog("Fmpf : " + strFmpf);
                    WriteTempLog("strSendLine : " + strSendLine);
                    WriteTempLog("m_xmlReceiveApvLine : " + m_xmlReceiveApvLine);
                    WriteTempLog("strCcLine : " + strCcLine);
                    WriteTempLog("strSubject : " + strSubject);
                    WriteTempLog("strContentElement : " + strContentElement);
                    WriteTempLog("strComment : " + strComment);
                    WriteTempLog("strFormInfo : " + strFormInfo);
                    WriteTempLog("strfileinfo : " + strfileinfo);
                    WriteTempLog("strPSTSGRWKey : " +strPSTSGRWKey);

                    dtResult = ResultSetDraftForm();
                    DataRow drResult = dtResult.NewRow();
                    if (_return.OuterXml.ToString().Contains(m_strResult_CD_CODE))
                    {
                        drResult[m_strResult_CD_CODE] = _return.Attributes[m_strResult_CD_CODE].Value.ToString();
                    }
                    if (_return.OuterXml.ToString().Contains(m_strResult_CD_STATUS))
                    {
                        drResult[m_strResult_CD_STATUS] = _return.Attributes[m_strResult_CD_STATUS].Value.ToString();
                    }
                    if (_return.OuterXml.ToString().Contains(m_strResult_CD_LEGACYKEY))
                    {
                        drResult[m_strResult_CD_LEGACYKEY] = _return.Attributes[m_strResult_CD_LEGACYKEY].Value.ToString();
                    }
                    if (_return.OuterXml.ToString().Contains(m_strResult_NO_EMPLOYEE))
                    {
                        drResult[m_strResult_NO_EMPLOYEE] = _return.Attributes[m_strResult_NO_EMPLOYEE].Value.ToString();
                    }
                    if (_return.OuterXml.ToString().Contains(m_strResult_DS_POSITION))
                    {
                        drResult[m_strResult_DS_POSITION] = _return.Attributes[m_strResult_DS_POSITION].Value.ToString();
                    }

                    drResult[m_strResult_MSG] = RemoveCDATA(_return.InnerText.ToString());

                    dtResult.Rows.Add(drResult);
                    log.WriteLine("--7--");
                    #endregion

                    if (!_return.Attributes[m_strResult_CD_CODE].Value.ToString().Equals("00") || !Convert.ToBoolean(_return.Attributes[m_strResult_CD_STATUS].Value.ToString()))
                    {
                        #region QRP 'F' 처리
                        SaveSetDraftForm_F(strLegacyKey, strFmpf, strUserIP, strUserID);

                        WriteTempLog("m_strResult_CD_CODE : " + _return.Attributes[m_strResult_CD_CODE].Value.ToString());
                        WriteTempLog("m_strResult_CD_STATUS : " +_return.Attributes[m_strResult_CD_STATUS].Value.ToString());
                        #endregion

                        dtResult = ResultSetDraftForm();
                        DataRow dr = dtResult.NewRow();
                        dr[m_strResult_CD_CODE] = "77";
                        dr[m_strResult_CD_STATUS] = "False";
                        dr[m_strResult_CD_LEGACYKEY] = strLegacyKey;
                        dr[m_strResult_NO_EMPLOYEE] = string.Empty;
                        dr[m_strResult_DS_POSITION] = "QRPError";
                        dr[m_strResult_MSG] = RemoveCDATA(_return.InnerText.ToString());
                        dtResult.Rows.Add(dr);

                        log.WriteLine("--8--");
                    }
                }
                else
                {
                    #region QRP 'F' 처리
                    SaveSetDraftForm_F(strLegacyKey, strFmpf, strUserIP, strUserID);
                    #endregion

                    dtResult = ResultSetDraftForm();
                    DataRow dr = dtResult.NewRow();
                    dr[m_strResult_CD_CODE] = "88";
                    dr[m_strResult_CD_STATUS] = "False";
                    dr[m_strResult_CD_LEGACYKEY] = strLegacyKey;
                    dr[m_strResult_NO_EMPLOYEE] = string.Empty;
                    dr[m_strResult_DS_POSITION] = "QRPError";
                    dr[m_strResult_MSG] = RemoveCDATA("파일전송에러");
                    dtResult.Rows.Add(dr);

                    log.WriteLine("--9--");
                }
                log.WriteLine("dtResult:" + dtResult.ToString());
                return dtResult;
            }
            catch (Exception ex)
            {
                log.WriteLine("--10--");

                #region QRP 'F' 처리
                SaveSetDraftForm_F(strLegacyKey, strFmpf, strUserIP, strUserID);
                #endregion

                dtResult = ResultSetDraftForm();
                DataRow dr = dtResult.NewRow();
                dr[m_strResult_CD_CODE] = "99";
                dr[m_strResult_CD_STATUS] = "False";
                dr[m_strResult_CD_LEGACYKEY] = strLegacyKey;
                dr[m_strResult_NO_EMPLOYEE] = string.Empty;
                dr[m_strResult_DS_POSITION] = "QRPError";
                dr[m_strResult_MSG] = ex.Message.ToString() + "\t" + ex.StackTrace.ToString();
                dtResult.Rows.Add(dr);
                log.WriteLine("--ex.Message.ToString() \t ex.StackTrace.ToString()--:" + ex.Message.ToString() + "\t" + ex.StackTrace.ToString());
                return dtResult;
                throw (ex);
            }
            finally
            {
                log.WriteLine("-------end-------");
                log.Close();
            }
        }

        #endregion

        // WF_STS_JEGEJUNG
        #region 표준문서 제/개정
        /// <summary>
        /// 표준문서 제정
        /// </summary>
        /// <param name="strLegacyKey">레거시 시스템을 구분하는 구분키 : PlantCode + "||" + StdNumber + "||" + VersionNum
        /// 해당 건에 대한 리턴인지 확인할 수 있는 Transaction Key</param>
        /// <param name="dtSendLine"> 주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : ERP 아이디
        ///                          UserID     : QRP 아이디
        ///                          </param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : ERP 아이디
        ///                        UserID     : QRP 아이디</param>
        /// <param name="strDOCNo">표준문서 번호</param>
        /// <param name="strDOCSubject">표준문서 제목</param>
        /// <param name="strDOCUser">기안자</param>
        /// <param name="strComment">비고</param>
        /// <param name="dtFileInfo"> 그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치
        /// </param>
        /// <param name="dtFormInfo">
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}
        /// </param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable CreateISODOC(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            string strDOCNo, string strDOCSubject, string strDOCUser, string strComment, DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContent = ChangeContentString_ISODOC(ConvertToHtml(strDOCNo), ConvertToHtml(strDOCSubject), ConvertToHtml(strDOCUser), "", "", "");

            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strISODOCSubject, strContent, ConvertToHtml(strComment), dtFormInfo, dtFileInfo, m_strFmpf_CreateISO, strUserIP, strUserID);
            return dtRtn ;
        }

        /// <summary>
        /// 표준문서 제정
        /// </summary>
        /// <param name="strLegacyKey">레거시 시스템을 구분하는 구분키 : PlantCode + "||" + StdNumber + "||" + VersionNum
        /// 해당 건에 대한 리턴인지 확인할 수 있는 Transaction Key</param>
        /// <param name="dtSendLine"> 주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : Brains 아이디
        ///                          UserID     : QRP ID
        ///                          </param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : Brains 아이디
        ///                        UserID     : QRP ID</param>
        /// <param name="strDOCNo">표준문서 번호</param>
        /// <param name="strDOCSubject">표준문서 제목</param>
        /// <param name="strDOCUser">기안자</param>
        /// <param name="strComment">비고</param>
        /// <param name="strDOCChange">변경사항</param>
        /// <param name="strDOCFrom">변경 전</param>
        /// <param name="strDOCTo">변경 후</param>
        /// <param name="dtFileInfo"> 그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치
        /// </param>
        /// <param name="dtFormInfo">
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}
        /// </param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable ModifyISIDOC(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            string strDOCNo, string strDOCSubject, string strDOCUser, string strComment, string strDOCChange, string strDOCFrom, string strDOCTo, DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContent = ChangeContentString_ISODOC(ConvertToHtml(strDOCNo), ConvertToHtml(strDOCSubject), ConvertToHtml(strDOCUser), ConvertToHtml(strDOCChange), ConvertToHtml(strDOCFrom), ConvertToHtml(strDOCTo));

            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strISODOCSubject, strContent, ConvertToHtml(strComment), dtFormInfo, dtFileInfo, m_strFmpf_CreateISO, strUserIP, strUserID);
            return dtRtn;
        }

        private string ChangeContentString_ISODOC(string strDOCNO, string strDOCSubject, string strDOCUser, string strDOCChange, string strDOCFrom, string strDOCTo)
        {
            string strReturnString = string.Empty;
            //요청에 의해 문서상에 표시돼는 표준번호 형식 변경
            string strStdNumber = strDOCNO.Replace("||", "-");

            XmlDocument xml = new XmlDocument();
            xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

            XmlNode node = xml.SelectSingleNode("configuration/LegacyHtml/ISODOC");

            node.InnerText = node.InnerText.Replace(@"<P style=""FONT-FAMILY: "">표준 번호</P></TD>", @"<P style=""FONT-FAMILY: "">" + (strStdNumber == string.Empty ? @"&nbsp;" : strStdNumber) + "</P></TD>");
            node.InnerText = node.InnerText.Replace(@"<P style=""FONT-FAMILY: "">표준 제목</P></TD>", @"<P style=""FONT-FAMILY: "">" + (strDOCSubject == string.Empty ? @"&nbsp;" : strDOCSubject) + "</P></TD>");
            node.InnerText = node.InnerText.Replace(@"<P style=""FONT-FAMILY: "">기안자</P></TD>", @"<P style=""FONT-FAMILY: "">" + (strDOCUser == string.Empty ? @"&nbsp;" : strDOCUser) + "</P></TD>");
            node.InnerText = node.InnerText.Replace(@"<P style=""FONT-FAMILY: "">변경항목</P></TD>", @"<P style=""FONT-FAMILY: "">" + (strDOCChange == string.Empty ? "신규등록" : strDOCChange) + "</P></TD>");
            node.InnerText = node.InnerText.Replace(@"<P style=""FONT-FAMILY: "">FROM (변경전)</P></TD>", @"<P style=""FONT-FAMILY: "">" + (strDOCFrom == string.Empty ? "신규등록" : strDOCFrom) + "</P></TD>");
            node.InnerText = node.InnerText.Replace(@"<P style=""FONT-FAMILY: "">TO (변경후)</P></TD>", @"<P style=""FONT-FAMILY: "">" + (strDOCTo == string.Empty ? "신규등록" : strDOCTo) + "</P></TD>");
            strReturnString = node.InnerText;
            return strReturnString;
        }
        #endregion

        // WF_STS_PEJI
        #region 표준문서 폐기
        /// <summary>
        /// 표준문서 폐기
        /// </summary>
        /// <param name="strLegacyKey">레거시 시스템을 구분하는 구분키 : PlantCode + "||" + StdNumber + "||" + VersionNum
        /// 해당 건에 대한 리턴인지 확인할 수 있는 Transaction Key</param>
        /// <param name="dtSendLine"> 주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : Brains 아이디
        ///                          UserID     : QRP ID
        ///                          </param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : Brains 아이디
        ///                        UserID      : QRP ID</param>
        /// <param name="strDOCNo">표준문서 번호</param>
        /// <param name="strDOCSubject">표준문서 제목</param>
        /// <param name="strDOCReason">폐기 사유</param>
        /// <param name="strDOCUser">기안자</param>
        /// <param name="strComment">비고</param>
        /// <param name="dtFormInfo">
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}
        /// </param>
        /// <param name="dtFileInfo"> 그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치
        /// </param>        
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable CancelISODOC(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            string strDOCNo, string strDOCSubject, string strDOCReason, string strDOCUser, string strComment, DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContent = ChangeContentString_CanceISODOC(ConvertToHtml(strDOCNo), ConvertToHtml(strDOCSubject), ConvertToHtml(strDOCReason), ConvertToHtml(strDOCUser));
            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strISODOCCancelSubject, strContent, ConvertToHtml(strComment), dtFormInfo, dtFileInfo, m_strFmpf_CancelISO, strUserIP, strUserID);
            return dtRtn;
        }

        private string ChangeContentString_CanceISODOC(string strDOCNO, string strDOCSubject, string strDOCReason, string strDOCUser)
        {
            string strReturnString = string.Empty;
            //요청에 의해 문서상에 표시돼는 표준번호 형식 변경
            string strStdNumber = strDOCNO.Replace("||", "-");

            XmlDocument xml = new XmlDocument();
            xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

            XmlNode node = xml.SelectSingleNode("configuration/LegacyHtml/ISODOCCancel");

            node.InnerText = node.InnerText.Replace(@"<P>폐기 대상 표준 번호</P>", @"<P align=left>" + (strStdNumber == string.Empty ? @"&nbsp;" : strStdNumber) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P>폐기 대상 표준 제목</P>", @"<P align=left>" + (strDOCSubject == string.Empty ? @"&nbsp;" : strDOCSubject) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P>폐기 사유</P>", @"<P align=left>" + (strDOCReason == string.Empty ? @"&nbsp;" : strDOCReason) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P>작성부서/작성자</P>", @"<P align=left>" + (strDOCUser == string.Empty ? @"&nbsp;" : strDOCUser) + "</P>");
            strReturnString = node.InnerText;
            return strReturnString;
        }
        #endregion

        // 신규 - WF_STS_MATJEGUM
        #region 원자재 특채
        /// <summary>
        /// 원자재 특채
        /// </summary>
        /// <param name="strLegacyKey">레거시 시스템을 구분하는 구분키 : PlantCode + "||" + StdNumber
        /// 해당 건에 대한 리턴인지 확인할 수 있는 Transaction Key</param>
        /// <param name="dtSendLine"> 주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : Brains 아이디
        ///                          UserID     : QRP ID
        ///                          </param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : Brains 아이디
        ///                        UserID     : QRP ID</param>
        /// <param name="dtContent">본문</param>
        /// <param name="dtShip">Ship정보</param>
        /// <param name="strComment">비고</param>
        /// <param name="dtFormInfo">
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}
        /// </param>
        /// <param name="dtFileInfo"> 그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치
        /// </param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable Material(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            DataTable dtContent, DataTable dtShip, DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContent = ChangeContentString_Material(dtContent, dtShip, dtSendLine);

            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strMaterialSubject, strContent, string.Empty, dtFormInfo, dtFileInfo, m_strFmpf_Material, strUserIP, strUserID);
            return dtRtn;
        }

        private string ChangeContentString_Material(DataTable dtContent, DataTable dtShip, DataTable dtSendLine)
        {
            string strReturnString = string.Empty;

            string strHeader = string.Empty;
            string strShip = string.Empty;
            string strBody = string.Empty;

            XmlDocument xml = new XmlDocument();
            xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

            #region Header
            XmlNode nodeHeader = xml.SelectSingleNode("configuration/LegacyHtml/Material/HEADER");
            strHeader = nodeHeader.InnerText;
            #endregion

            #region Ship
            XmlNode nodeShip = xml.SelectSingleNode("configuration/LegacyHtml/Material/SHIP/TABLE");



            for (int i = 0; i < dtShip.Rows.Count; i++)
            {
                XmlNode nodeTR = xml.CreateElement("TR");
                XmlNode nodeTD = xml.CreateElement("TD");
                XmlNode nodeP = xml.CreateElement("P");

                XmlAttribute atthieght = xml.CreateAttribute("height");
                atthieght.Value = "21";
                XmlAttribute attalign = xml.CreateAttribute("align");
                attalign.Value = "center";

                // 자재코드
                nodeP.Attributes.Append(attalign);
                nodeP.InnerText = dtShip.Rows[i]["MaterialCode"].ToString();
                nodeTD.Attributes.Append(atthieght);
                nodeTD.AppendChild(nodeP);
                nodeTR.AppendChild(nodeTD);

                XmlNode nodeTD1 = xml.CreateElement("TD");
                XmlNode nodeP1 = xml.CreateElement("P");

                XmlAttribute atthieght1 = xml.CreateAttribute("height");
                atthieght1.Value = "21";
                XmlAttribute attalign1 = xml.CreateAttribute("align");
                attalign1.Value = "center";

                // 자재명
                nodeP1.Attributes.Append(attalign1);
                nodeP1.InnerText = dtShip.Rows[i]["MaterialName"].ToString();
                nodeTD1.Attributes.Append(atthieght1);
                nodeTD1.AppendChild(nodeP1);
                nodeTR.AppendChild(nodeTD1);

                XmlNode nodeTD2 = xml.CreateElement("TD");
                XmlNode nodeP2 = xml.CreateElement("P");

                XmlAttribute atthieght2 = xml.CreateAttribute("height");
                atthieght2.Value = "21";
                XmlAttribute attalign2 = xml.CreateAttribute("align");
                attalign2.Value = "center";

                // 입고일
                nodeTD2 = xml.CreateElement("TD");
                nodeP2.Attributes.Append(attalign2);
                nodeP2.InnerText = dtShip.Rows[i]["GRDate"].ToString();
                nodeTD2.Attributes.Append(atthieght2);
                nodeTD2.AppendChild(nodeP2);
                nodeTR.AppendChild(nodeTD2);

                XmlNode nodeTD3 = xml.CreateElement("TD");
                XmlNode nodeP3 = xml.CreateElement("P");

                XmlAttribute atthieght3 = xml.CreateAttribute("height");
                atthieght3.Value = "21";
                XmlAttribute attalign3 = xml.CreateAttribute("align");
                attalign3.Value = "center";

                // 수량
                nodeTD3 = xml.CreateElement("TD");
                nodeP3.Attributes.Append(attalign3);
                nodeP3.InnerText = Convert.ToDouble(dtShip.Rows[i]["GRQty"].ToString()).ToString("###,###,###");
                nodeTD3.Attributes.Append(atthieght3);
                nodeTD3.AppendChild(nodeP3);
                nodeTR.AppendChild(nodeTD3);

                XmlNode nodeTD4 = xml.CreateElement("TD");
                XmlNode nodeP4 = xml.CreateElement("P");

                XmlAttribute atthieght4 = xml.CreateAttribute("height");
                atthieght4.Value = "21";
                XmlAttribute attalign4 = xml.CreateAttribute("align");
                attalign4.Value = "center";

                // 협력업체
                nodeTD4 = xml.CreateElement("TD");
                nodeP4.Attributes.Append(attalign4);
                nodeP4.InnerText = dtShip.Rows[i]["VendorName"].ToString();
                nodeTD4.Attributes.Append(atthieght4);
                nodeTD4.AppendChild(nodeP4);
                nodeTR.AppendChild(nodeTD4);

                XmlNode nodeTD5 = xml.CreateElement("TD");
                XmlNode nodeP5 = xml.CreateElement("P");

                XmlAttribute atthieght5 = xml.CreateAttribute("height");
                atthieght5.Value = "21";
                XmlAttribute attalign5 = xml.CreateAttribute("align");
                attalign5.Value = "center";

                // 특채 LOT
                nodeTD5 = xml.CreateElement("TD");
                nodeP5.Attributes.Append(attalign5);
                nodeP5.InnerText = dtShip.Rows[i]["LotNo"].ToString();
                nodeTD5.Attributes.Append(atthieght5);
                nodeTD5.AppendChild(nodeP5);
                nodeTR.AppendChild(nodeTD5);

                XmlNode nodeTD6 = xml.CreateElement("TD");
                XmlNode nodeP6 = xml.CreateElement("P");

                XmlAttribute atthieght6 = xml.CreateAttribute("height");
                atthieght6.Value = "21";
                XmlAttribute attalign6 = xml.CreateAttribute("align");
                attalign6.Value = "center";

                // 비고
                nodeTD6 = xml.CreateElement("TD");
                nodeP6.Attributes.Append(attalign6);
                nodeP6.InnerText = dtShip.Rows[i]["EtcDesc"].ToString();
                nodeTD6.Attributes.Append(atthieght6);
                nodeTD6.AppendChild(nodeP6);
                nodeTR.AppendChild(nodeTD6);

                nodeShip.AppendChild(nodeTR);
            }

            if (dtShip.Rows.Count.Equals(0))
            {
                strShip = string.Empty;
            }
            else
            {
                strShip = nodeShip.OuterXml.ToString();
            }
            #endregion

            #region Body
            XmlNode nodBody = xml.SelectSingleNode("configuration/LegacyHtml/Material/BODY");

            nodBody.InnerText = nodBody.InnerText.Replace("!1!", (dtContent.Rows[0]["ReqObjectDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ReqObjectDesc"].ToString()));

            nodBody.InnerText = nodBody.InnerText.Replace("!2!", (dtContent.Rows[0]["MeetingDate1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingDate1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!3!", (dtContent.Rows[0]["MeetingPlace1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingPlace1"].ToString()));

            nodBody.InnerText = nodBody.InnerText.Replace("!4!", (dtContent.Rows[0]["DistPurchaseDeptName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseDeptName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!5!", (dtContent.Rows[0]["DistPurchaseUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseUserName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!6!", (dtContent.Rows[0]["DistProductDeptName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductDeptName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!7!", (dtContent.Rows[0]["DistProductUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductUserName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!8!", (dtContent.Rows[0]["DistDevelopDeptName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopDeptName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!9!", (dtContent.Rows[0]["DistDevelopUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopUserName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!10", (dtContent.Rows[0]["DistEquipDeptName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipDeptName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!11", (dtContent.Rows[0]["DistEquipUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipUserName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!12", (dtContent.Rows[0]["DistQualityDeptName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityDeptName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!13", (dtContent.Rows[0]["DistQualityUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityUserName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!14", (dtContent.Rows[0]["DistEtcDeptName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEtcDeptName1"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!15", (dtContent.Rows[0]["DistEtcUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEtcUserName1"].ToString()));

            nodBody.InnerText = nodBody.InnerText.Replace("!16", (dtContent.Rows[0]["MRBDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBDesc"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!17", (dtContent.Rows[0]["MRBUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBUserName"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!18", (dtContent.Rows[0]["MRBDeliveryDate"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBDeliveryDate"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!19", (dtContent.Rows[0]["MRBEtcDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBEtcDesc"].ToString()));

            nodBody.InnerText = nodBody.InnerText.Replace("!20", (dtContent.Rows[0]["MeetingDate2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingDate2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!21", (dtContent.Rows[0]["MeetingPlace2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingPlace2"].ToString()));

            nodBody.InnerText = nodBody.InnerText.Replace("!22", (dtContent.Rows[0]["DistPurchaseDeptName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseDeptName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!23", (dtContent.Rows[0]["DistPurchaseUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseUserName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!24", (dtContent.Rows[0]["DistProductDeptName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductDeptName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!25", (dtContent.Rows[0]["DistProductUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductUserName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!26", (dtContent.Rows[0]["DistDevelopDeptName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopDeptName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!27", (dtContent.Rows[0]["DistDevelopUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopUserName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!28", (dtContent.Rows[0]["DistEquipDeptName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipDeptName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!29", (dtContent.Rows[0]["DistEquipUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipUserName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!30", (dtContent.Rows[0]["DistQualityDeptName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityDeptName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!31", (dtContent.Rows[0]["DistQualityUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityUserName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!32", (dtContent.Rows[0]["DistEtcDeptName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEtcDeptName2"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!33", (dtContent.Rows[0]["DistEtcUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEtcUserName2"].ToString()));

            nodBody.InnerText = nodBody.InnerText.Replace("!34", (dtContent.Rows[0]["ResultDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultDesc"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!35", (dtContent.Rows[0]["ResultUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultUserName"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!36", (dtContent.Rows[0]["ResultDeliveryDate"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultDeliveryDate"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!37", (dtContent.Rows[0]["ResultEtcDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultEtcDesc"].ToString()));

            DataRow[] drSend = dtSendLine.Select("CD_KIND = 'SP'");

            if (drSend.Length > 0)
            {
                switch (drSend.Length)
                {
                    case 1:
                        nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!40", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!41", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!42", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!43", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!44", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!45", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!46", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!47", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!48", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!49", "&nbsp;");
                        break;
                    case 2:
                        nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!40", (drSend[1]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!41", (drSend[1]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!42", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!43", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!44", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!45", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!46", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!47", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!48", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!49", "&nbsp;");
                        break;
                    case 3:
                        nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!40", (drSend[1]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!41", (drSend[1]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!42", (drSend[2]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!43", (drSend[2]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!44", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!45", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!46", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!47", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!48", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!49", "&nbsp;");
                        break;
                    case 4:
                        nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!40", (drSend[1]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!41", (drSend[1]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!42", (drSend[2]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!43", (drSend[2]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!44", (drSend[3]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!45", (drSend[3]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!46", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!47", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!48", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!49", "&nbsp;");
                        break;
                    case 5:
                        nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!40", (drSend[1]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!41", (drSend[1]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!42", (drSend[2]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!43", (drSend[2]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!44", (drSend[3]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!45", (drSend[3]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!46", (drSend[4]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[4]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!47", (drSend[4]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[4]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!48", "&nbsp;");
                        nodBody.InnerText = nodBody.InnerText.Replace("!49", "&nbsp;");
                        break;
                    case 6:
                        nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!40", (drSend[1]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!41", (drSend[1]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!42", (drSend[2]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!43", (drSend[2]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!44", (drSend[3]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!45", (drSend[3]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!46", (drSend[4]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[4]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!47", (drSend[4]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[4]["UserName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!48", (drSend[5]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[5]["DeptName"].ToString()));
                        nodBody.InnerText = nodBody.InnerText.Replace("!49", (drSend[5]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[5]["UserName"].ToString()));
                        break;
                }

                if (drSend.Length > 6)
                {
                    nodBody.InnerText = nodBody.InnerText.Replace("!38", (drSend[0]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["DeptName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!39", (drSend[0]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[0]["UserName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!40", (drSend[1]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["DeptName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!41", (drSend[1]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[1]["UserName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!42", (drSend[2]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["DeptName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!43", (drSend[2]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[2]["UserName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!44", (drSend[3]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["DeptName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!45", (drSend[3]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[3]["UserName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!46", (drSend[4]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[4]["DeptName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!47", (drSend[4]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[4]["UserName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!48", (drSend[5]["DeptName"].ToString() == string.Empty ? @"&nbsp;" : drSend[5]["DeptName"].ToString()));
                    nodBody.InnerText = nodBody.InnerText.Replace("!49", (drSend[5]["UserName"].ToString() == string.Empty ? @"&nbsp;" : drSend[5]["UserName"].ToString()));
                }

            }
            else
            {
                nodBody.InnerText = nodBody.InnerText.Replace("!38", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!39", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!40", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!41", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!42", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!43", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!44", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!45", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!46", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!47", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!48", "&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!49", "&nbsp;");
            }

            nodBody.InnerText = nodBody.InnerText.Replace("!50", (dtContent.Rows[0]["AgreeDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreeDesc"].ToString()));

            if (dtContent.Rows[0]["InspectResult"].ToString().Equals("T") || dtContent.Rows[0]["InspectResult"].ToString().Equals("S"))
            {
                nodBody.InnerText = nodBody.InnerText.Replace("!51", "O");
                nodBody.InnerText = nodBody.InnerText.Replace("!52", @"&nbsp;");
            }
            else
            {
                nodBody.InnerText = nodBody.InnerText.Replace("!51", @"&nbsp;");
                nodBody.InnerText = nodBody.InnerText.Replace("!52", "O");
            }

            nodBody.InnerText = nodBody.InnerText.Replace("!53", (dtContent.Rows[0]["DistPurchaseDeptName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseDeptName3"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!54", (dtContent.Rows[0]["DistProductDeptName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductDeptName3"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!55", (dtContent.Rows[0]["DistDevelopDeptName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopDeptName3"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!56", (dtContent.Rows[0]["DistEquipDeptName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipDeptName3"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!57", (dtContent.Rows[0]["DistQualityDeptName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityDeptName3"].ToString()));
            nodBody.InnerText = nodBody.InnerText.Replace("!58", (dtContent.Rows[0]["DistEtcDeptName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEtcDeptName3"].ToString()));

            strBody = nodBody.InnerText;

            #endregion

            strReturnString = strHeader + strShip + strBody;
            return strReturnString;
        }
        #endregion

        // 일반 기안지 - WF_STS_QRPGIAN
        #region 설비인증의뢰
        /// <summary>
        /// 설비인증의뢰
        /// </summary>
        /// <param name="strLegacyKey">레거시 시스템을 구분하는 구분키 : PlantCode + "||" + DocCode
        /// 해당 건에 대한 리턴인지 확인할 수 있는 Transaction Key</param>
        /// <param name="dtSendLine"> 주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : ERP 아이디
        ///                          UserID     : QRP ID
        ///                          </param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : ERP 아이디
        ///                        UserID     : QRP ID</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strEquipName">설비명</param>
        /// <param name="strModelName">모델명</param>
        /// <param name="strMaker">메이커</param>
        /// <param name="strSerialNo">시리얼번호</param>
        /// <param name="strReason">사유</param>
        /// <param name="strPurpose">목적</param>
        /// <param name="dtFormInfo">
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}
        /// </param>
        /// <param name="dtFileInfo"> 그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치
        /// </param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable Equip(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            string strEquipCode, string strEquipName, string strModelName, string strMaker, string strSerialNo, string strReason, string strPurpose, 
            DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContents = ChangeContentString_Equip(strEquipCode, strEquipName, strModelName, strMaker, strSerialNo, strReason, strPurpose);

            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strEquipSubject, strContents, string.Empty, dtFormInfo, dtFileInfo, m_strFmpf_Normal, strUserIP, strUserID);
            return dtRtn;
        }

        private string ChangeContentString_Equip(string strEquipCode, string strEquipName, string strModelName, string strMaker,
            string strSerialNo, string strReason, string strPurpose)
        {
            string strReturnString = string.Empty;

            XmlDocument xml = new XmlDocument();
            xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

            XmlNode node = xml.SelectSingleNode("configuration/LegacyHtml/Equip");

            node.InnerText = node.InnerText.Replace(@"<P align=left>설비코드</P>", @"<P align=left>" + (strEquipCode == string.Empty ? @"&nbsp;" : strEquipCode) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>설비명</P>", @"<P align=left>" + (strEquipName == string.Empty ? @"&nbsp;" : strEquipName) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>모델명</P>", @"<P align=left>" + (strModelName == string.Empty ? @"&nbsp;" : strModelName) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>메이커</P>", @"<P align=left>" + (strMaker == string.Empty ? @"&nbsp;" : strMaker) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>시리얼번호</P>", @"<P align=left>" + (strSerialNo == string.Empty ? @"&nbsp;" : strSerialNo) + "</P>");
            node.InnerText = node.InnerText.Replace(@"&nbsp;사유", @"&nbsp;" + (strReason == string.Empty ? @"&nbsp;" : strReason));
            node.InnerText = node.InnerText.Replace(@"&nbsp;목적", @"&nbsp;" + (strPurpose == string.Empty ? @"&nbsp;" : strPurpose));
            strReturnString = node.InnerText;
            return strReturnString;
        }
        #endregion

        // 신규 - WF_STS_EQPDEL
        #region 설비 폐기 신청
        /// <summary>
        /// 설비 폐기 신청서
        /// </summary>
        /// <param name="strLegacyKey">레거시 시스템을 구분하는 구분키 : PlantCode + "||" + EquipCode
        /// 해당 건에 대한 리턴인지 확인할 수 있는 Transaction Key</param>
        /// <param name="dtSendLine"> 주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : ERP 아이디
        ///                          UserID     : QRP ID
        ///                          </param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : ERP 아이디
        ///                        UserID     : QRP ID</param>
        /// <param name="strStationName">스테이션명</param>
        /// <param name="strGRDate">입고일</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strSerialNo">시리얼번호</param>
        /// <param name="strPurchasePrice">설비 도입가</param>
        /// <param name="strRemainPrice">설비 잔존가</param>
        /// <param name="strReason">폐기 사유</param>
        /// <param name="dtFormInfo">
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}
        /// </param>
        /// <param name="dtFileInfo"> 그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치
        /// </param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable EquipDiscard(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            string strStationName, string strGRDate, string strEquipCode, string strSerialNo, string strPurchasePrice, string strRemainPrice, string strReason,
            DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContents = ChangeContentString_EquipDiscard(strStationName, strGRDate, strEquipCode, strSerialNo, strPurchasePrice, strRemainPrice, strReason);

            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strEquipDiscardSubject, strContents, string.Empty, dtFormInfo, dtFileInfo, m_strFmpf_EquipDiscard, strUserIP, strUserID);
            return dtRtn;
        }

        private string ChangeContentString_EquipDiscard(string strStationName, string strGRDate, string strEquipCode, string strSerialNo, string strPurchasePrice, string strRemainPrice, string strReason)
        {
            string strReturnString = string.Empty;

            XmlDocument xml = new XmlDocument();
            xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

            XmlNode node = xml.SelectSingleNode("configuration/LegacyHtml/EquipDiscard");

            node.InnerText = node.InnerText.Replace(@"<P align=left>부서</P>", @"<P align=left>" + (strStationName == string.Empty ? @"&nbsp;" : strStationName) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>입고일</P>", @"<P align=left>" + (strGRDate == string.Empty ? @"&nbsp;" : strGRDate) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>MODEL</P>", @"<P align=left>" + (strEquipCode == string.Empty ? @"&nbsp;" : strEquipCode) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>S/N</P>", @"<P align=left>" + (strSerialNo == string.Empty ? @"&nbsp;" : strSerialNo) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>도입가</P>", @"<P align=left>" + (strPurchasePrice == string.Empty ? @"&nbsp;" : Convert.ToDouble(strPurchasePrice).ToString("###,###")) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>잔존가</P>", @"<P align=left>" + (strRemainPrice == string.Empty ? @"&nbsp;" : Convert.ToDouble(strRemainPrice).ToString("###,###")) + "</P>");
            node.InnerText = node.InnerText.Replace(@"<P align=left>사유</P>", @"<P align=left>" + (strReason == string.Empty ? @"&nbsp;" : strReason) + "</P>");
            strReturnString = node.InnerText;
            return strReturnString;
        }
        #endregion

        /// <summary>
        /// 설비점검정보등록
        /// </summary>
        /// <param name="strLegacyKey">레거시키 PlantCode||StdNumber||VersionNum</param>
        /// <param name="dtSendLine">주 / 신청부서 결재선      (	최소한 기안자 1명, 결재자 1명이 있어야 합니다.)
        ///                          CD_KIND  : 결재구분 : / SI : 기안자 / SA : 신청부서 결재 / SS : 신청부서 순차합의 / SP : 신청부서 병렬합의 
        ///                          CD_COMPANY : 회사코드
        ///                          CD_USERKEY : ERP 아이디
        ///                          UserID     : QRP ID</param>
        /// <param name="dtCcLine">통보 결재선
        ///                        CD_KIND    : 결재구분 : / CP : 개인 통보 / CD : 부서통보
        ///                        CD_COMPANY : 회사코드
        ///                        CD_USERKEY : ERP 아이디
        ///                        UserID     : QRP ID</param>
        /// <param name="strPlantName">공장코드</param>
        /// <param name="strStationName">Station이름</param>
        /// <param name="strEquipLocName">위치</param>
        /// <param name="strEquipLTypeName">설비대분류</param>
        /// <param name="strEquipMTypeName">설비중분류</param>
        /// <param name="strEquipGroupName">설비그룹명</param>
        /// <param name="strRevisionReason">개정사유</param>
        /// <param name="dtFromItem">이전 개정의 점검 상세 정보</param>
        /// <param name="dtToItem">점검 상세정보 개정값</param>
        /// <param name="dtFormInfo">        
        ///    결재 연동문서의 부가 정보입니다.
        ///    필수 입력 사항은 아니며 필요시 해당 하는 노드를 
        ///    적어서 보내주시면 해당 항목이 입력 처리 됩니다.
        ///     DataTable Column :
        ///     UserType {결재선 사용자 구분 / USER(ERP ID) : U / EMPLOYEE(사번) : E}
        ///     SaveTerm {문서보존기간 : Default / 5(5년) / 1(1년), 3(3년), 5(5년), 10(10년), 99(영구)}
        ///     DocLevel {문서관리에 할당된 일반문서/보안문서의 숫자코드값}
        ///     DocClassId {문서관리의 문서폴더의 ID 숫자코드값 ex)510}
        ///     DocClassName {일반문서/보안문서 구분}
        ///     DocNo {문서번호 : ex) 정보화사무국-2010-01} -- 필수
        ///     EntName {사업부 이름 : (주)보광}</param>
        /// <param name="dtFileInfo">그룹웨어 전송용 파일정보 DataTable
        ///                             PlantCode   :   공장코드
        ///                             NM_FILE     :   파일명
        ///                             NM_LOCATION :   파일위치</param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable EquipPM(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
            string strPlantName, string strStationName,string strEquipLocName,string strEquipLTypeName,string strEquipMTypeName,
            string strEquipGroupName, string strRevisionReason, DataTable dtFromItem, DataTable dtToItem, DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        {
            string strContents = ChangContentString_EquipPM(strPlantName, strStationName, strEquipLocName, strEquipLTypeName, strEquipMTypeName
                , strEquipGroupName, strRevisionReason, dtFromItem, dtToItem);

            //첨부파일 데이터테이블 
            DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, m_strEquipPM, strContents, string.Empty, dtFormInfo, dtFileInfo, m_strFmpf_EquipPM, strUserIP, strUserID);
            return dtRtn;
        }

        private string ChangContentString_EquipPM(string strPlantName, string strStationName, string strEquipLocName, string strEquipLTypeName, string strEquipMTypeName, string strEquipGroupName, string strRevisionReason, DataTable dtFromItem, DataTable dtToItem)
        {
            string strReturnString = string.Empty;

            string strHeader = string.Empty;
            string strReason = string.Empty;
            string strFrom = string.Empty;
            string strFromItem = string.Empty;
            string strTo = string.Empty;
            string strToItem = string.Empty;

            XmlDocument xml = new XmlDocument();
            xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

            XmlNode nodeHeader = xml.SelectSingleNode("configuration/LegacyHtml/EquipPMH/Header");
            XmlNode nodeReason = xml.SelectSingleNode("configuration/LegacyHtml/EquipPMH/REASON");
            //공장, Station, EquipGroup
            nodeHeader.InnerText = nodeHeader.InnerText.Replace("!1!", strPlantName == string.Empty ? @"&nbsp" : strPlantName);
            nodeHeader.InnerText = nodeHeader.InnerText.Replace("!2!", strStationName == string.Empty ? @"nbsp" : strStationName);
            nodeHeader.InnerText = nodeHeader.InnerText.Replace("!3!", strEquipLocName == string.Empty ? @"nbsp" : strEquipLocName);
            nodeHeader.InnerText = nodeHeader.InnerText.Replace("!4!", strEquipLTypeName == string.Empty ? @"nbsp" : strEquipLTypeName);
            nodeHeader.InnerText = nodeHeader.InnerText.Replace("!5!", strEquipMTypeName == string.Empty ? @"nbsp" : strEquipMTypeName);
            nodeHeader.InnerText = nodeHeader.InnerText.Replace("!6!", strEquipGroupName == string.Empty ? @"nbsp" : strEquipGroupName);
            strHeader = nodeHeader.InnerText;
            //점검 요청 사유
            nodeReason.InnerText = nodeReason.InnerText.Replace("!7!", strRevisionReason == string.Empty ? @"&nbsp" : strRevisionReason);
            strReason = nodeReason.InnerText;
            //변경전
            XmlNode nodeFrom = xml.SelectSingleNode("configuration/LegacyHtml/EquipPMH/FROM/TABLE");
            strFrom = nodeFrom.InnerText;

            XmlNode NodeFromTR = xml.CreateElement("TR");
            XmlNode NodeFromTD = xml.CreateElement("TD");
            XmlNode NodeFromP = xml.CreateElement("P");

            XmlAttribute attheightFrom = xml.CreateAttribute("height");
            attheightFrom.Value = "21";
            XmlAttribute attalignFrom = xml.CreateAttribute("align");
            attalignFrom.Value = "left";
            strFrom = nodeFrom.OuterXml.ToString();

            //변경전 Item
            XmlNode nodeFromItem = xml.SelectSingleNode("configuration/LegacyHtml/EquipPMH/FROMITEM/TABLE");
            strFromItem = nodeFromItem.InnerText;

            XmlNode nodeFromItemTR = xml.CreateElement("TR");
            XmlNode nodeFromItemTD = xml.CreateElement("TD");
            XmlNode nodeFromItemP = xml.CreateElement("P");

            XmlAttribute attheightFromItem = xml.CreateAttribute("height");
            attheightFromItem.Value = "21";
            XmlAttribute attalinFromItem = xml.CreateAttribute("align");
            attalinFromItem.Value = "center";

            strFromItem = nodeFromItem.OuterXml.ToString();

            for (int i = 0; i < dtFromItem.Rows.Count; i++)
            {
                //순번
                XmlNode nodeTR = xml.CreateElement("TR");
                XmlNode nodeTDseq = xml.CreateElement("TD");
                XmlNode nodePseq = xml.CreateElement("P");

                XmlAttribute attheightItemseq = xml.CreateAttribute("height");
                attheightItemseq.Value = "21";
                XmlAttribute attalignItemseq = xml.CreateAttribute("align");
                attalignItemseq.Value = "center";

                nodePseq.Attributes.Append(attalignItemseq);
                nodePseq.InnerText = Convert.ToString(i + 1);
                nodeTDseq.Attributes.Append(attheightItemseq);
                nodeTDseq.AppendChild(nodePseq);
                nodeTR.AppendChild(nodeTDseq);

                //점검항목
                XmlNode nodeTD = xml.CreateElement("TD");
                XmlNode nodeP = xml.CreateElement("P");

                XmlAttribute attheightItem = xml.CreateAttribute("height");
                attheightItem.Value = "21";
                XmlAttribute attalignItem = xml.CreateAttribute("align");
                attalignItem.Value = "center";

                nodeP.Attributes.Append(attalignItem);
                nodeP.InnerText = dtFromItem.Rows[i]["PMInspectName"].ToString();
                nodeTD.Attributes.Append(attheightItem);
                nodeTD.AppendChild(nodeP);
                nodeTR.AppendChild(nodeTD);

                //기준
                XmlNode nodeTD1 = xml.CreateElement("TD");
                XmlNode nodeP1 = xml.CreateElement("P");

                XmlAttribute attheightItem1 = xml.CreateAttribute("height");
                attheightItem1.Value = "21";
                XmlAttribute attalignItem1 = xml.CreateAttribute("align");
                attalignItem1.Value = "left";

                nodeP1.Attributes.Append(attalignItem1);
                nodeP1.InnerText = dtFromItem.Rows[i]["PMInspectCriteria"].ToString();
                nodeTD1.Attributes.Append(attheightItem1);
                nodeTD1.AppendChild(nodeP1);
                nodeTR.AppendChild(nodeTD1);

                //점검주기
                XmlNode nodeTD2 = xml.CreateElement("TD");
                XmlNode nodeP2 = xml.CreateElement("P");

                XmlAttribute attheightItem2 = xml.CreateAttribute("height");
                attheightItem2.Value = "21";
                XmlAttribute attalignItem2 = xml.CreateAttribute("align");
                attalignItem2.Value = "center";

                nodeP2.Attributes.Append(attalignItem2);
                nodeP2.InnerText = dtFromItem.Rows[i]["PMPeriodCode"].ToString();
                nodeTD2.Attributes.Append(attheightItem2);
                nodeTD2.AppendChild(nodeP2);
                nodeTR.AppendChild(nodeTD2);

                //점검방법
                XmlNode nodeTD3 = xml.CreateElement("TD");
                XmlNode nodeP3 = xml.CreateElement("P");

                XmlAttribute attheightItem3 = xml.CreateAttribute("height");
                attheightItem3.Value = "21";
                XmlAttribute attalignItem3 = xml.CreateAttribute("align");
                attalignItem3.Value = "center";

                nodeP3.Attributes.Append(attalignItem3);
                nodeP3.InnerText = dtFromItem.Rows[i]["PMMethod"].ToString();
                nodeTD3.Attributes.Append(attheightItem3);
                nodeTD3.AppendChild(nodeP3);
                nodeTR.AppendChild(nodeTD3);

                //단위
                XmlNode nodeTD4 = xml.CreateElement("TD");
                XmlNode nodeP4 = xml.CreateElement("P");

                XmlAttribute attheightItem4 = xml.CreateAttribute("height");
                attheightItem4.Value = "21";
                XmlAttribute attalignItem4 = xml.CreateAttribute("align");
                attalignItem4.Value = "center";

                nodeP4.Attributes.Append(attalignItem4);
                nodeP4.InnerText = dtFromItem.Rows[i]["UnitDesc"].ToString();
                nodeTD4.Attributes.Append(attheightItem4);
                nodeTD4.AppendChild(nodeP4);
                nodeTR.AppendChild(nodeTD4);

                nodeFromItem.AppendChild(nodeTR);
            }
            //변경후
            XmlNode nodeTo = xml.SelectSingleNode("configuration/LegacyHtml/EquipPMH/TO/TABLE");
            strTo = nodeTo.InnerText;

            XmlNode NodeToTR = xml.CreateElement("TR");
            XmlNode NodeToTD = xml.CreateElement("TD");
            XmlNode NodeToP = xml.CreateElement("P");

            XmlAttribute attheightTo = xml.CreateAttribute("height");
            attheightTo.Value = "21";
            XmlAttribute attalignTo = xml.CreateAttribute("align");
            attalignTo.Value = "left";

            strTo = nodeTo.OuterXml.ToString();

            //변경후 항목
            XmlNode nodeToItem = xml.SelectSingleNode("configuration/LegacyHtml/EquipPMH/TOITEM/TABLE");
            strFromItem = nodeFromItem.InnerText;

            XmlNode nodeToItemTR = xml.CreateElement("TR");
            XmlNode nodeToItemTD = xml.CreateElement("TD");
            XmlNode nodeToItemP = xml.CreateElement("P");

            XmlAttribute attheightToItem = xml.CreateAttribute("height");
            attheightFromItem.Value = "21";
            XmlAttribute attalinToItem = xml.CreateAttribute("align");
            attalinFromItem.Value = "center";

            for (int i = 0; i < dtToItem.Rows.Count; i++)
            {
                //점검항목
                XmlNode nodeTR = xml.CreateElement("TR");
                XmlNode nodeTDseq = xml.CreateElement("TD");
                XmlNode nodePseq = xml.CreateElement("P");

                XmlAttribute attheightItemseq = xml.CreateAttribute("height");
                attheightItemseq.Value = "21";
                XmlAttribute attalignItemseq = xml.CreateAttribute("align");
                attalignItemseq.Value = "center";

                nodePseq.Attributes.Append(attalignItemseq);
                nodePseq.InnerText = Convert.ToString(i + 1);
                nodeTDseq.Attributes.Append(attheightItemseq);
                nodeTDseq.AppendChild(nodePseq);
                nodeTR.AppendChild(nodeTDseq);
                XmlNode nodeTD = xml.CreateElement("TD");
                XmlNode nodeP = xml.CreateElement("P");

                XmlAttribute attheightItem = xml.CreateAttribute("height");
                attheightItem.Value = "21";
                XmlAttribute attalignItem = xml.CreateAttribute("align");
                attalignItem.Value = "center";

                nodeP.Attributes.Append(attalignItem);
                nodeP.InnerText = dtToItem.Rows[i]["PMInspectName"].ToString();
                nodeTD.Attributes.Append(attheightItem);
                nodeTD.AppendChild(nodeP);
                nodeTR.AppendChild(nodeTD);

                //기준
                XmlNode nodeTD1 = xml.CreateElement("TD");
                XmlNode nodeP1 = xml.CreateElement("P");

                XmlAttribute attheightItem1 = xml.CreateAttribute("height");
                attheightItem1.Value = "21";
                XmlAttribute attalignItem1 = xml.CreateAttribute("align");
                attalignItem1.Value = "left";

                nodeP1.Attributes.Append(attalignItem1);
                nodeP1.InnerText = dtToItem.Rows[i]["PMInspectCriteria"].ToString();
                nodeTD1.Attributes.Append(attheightItem1);
                nodeTD1.AppendChild(nodeP1);
                nodeTR.AppendChild(nodeTD1);

                //점검주기
                XmlNode nodeTD2 = xml.CreateElement("TD");
                XmlNode nodeP2 = xml.CreateElement("P");

                XmlAttribute attheightItem2 = xml.CreateAttribute("height");
                attheightItem2.Value = "21";
                XmlAttribute attalignItem2 = xml.CreateAttribute("align");
                attalignItem2.Value = "center";

                nodeP2.Attributes.Append(attalignItem2);
                nodeP2.InnerText = dtToItem.Rows[i]["PMPeriodCode"].ToString();
                nodeTD2.Attributes.Append(attheightItem2);
                nodeTD2.AppendChild(nodeP2);
                nodeTR.AppendChild(nodeTD2);

                //점검방법
                XmlNode nodeTD3 = xml.CreateElement("TD");
                XmlNode nodeP3 = xml.CreateElement("P");

                XmlAttribute attheightItem3 = xml.CreateAttribute("height");
                attheightItem3.Value = "21";
                XmlAttribute attalignItem3 = xml.CreateAttribute("align");
                attalignItem3.Value = "center";

                nodeP3.Attributes.Append(attalignItem3);
                nodeP3.InnerText = dtToItem.Rows[i]["PMMethod"].ToString();
                nodeTD3.Attributes.Append(attheightItem3);
                nodeTD3.AppendChild(nodeP3);
                nodeTR.AppendChild(nodeTD3);

                //단위
                XmlNode nodeTD4 = xml.CreateElement("TD");
                XmlNode nodeP4 = xml.CreateElement("P");

                XmlAttribute attheightItem4 = xml.CreateAttribute("height");
                attheightItem4.Value = "21";
                XmlAttribute attalignItem4 = xml.CreateAttribute("align");
                attalignItem4.Value = "center";

                nodeP4.Attributes.Append(attalignItem4);
                nodeP4.InnerText = dtToItem.Rows[i]["UnitDesc"].ToString();
                nodeTD4.Attributes.Append(attheightItem4);
                nodeTD4.AppendChild(nodeP4);
                nodeTR.AppendChild(nodeTD4);

                nodeToItem.AppendChild(nodeTR);
            }

            strFromItem = nodeFromItem.OuterXml.ToString();
            strToItem = nodeToItem.OuterXml.ToString();

            //전체 문서 리턴
            strReturnString = strHeader + strReason + strFrom + strFromItem + "<p>" + strTo + strToItem;
            return strReturnString;
        }

        /// <summary>
        /// PSTS 사용자정보 검색 XML 만들기
        /// </summary>
        /// <param name="strSearchType">시스템 쿼리 종류 A : 전체, D : 부서, U : ID, E : 사번</param>
        /// <param name="strCompany">회사코드 PSTS는 251</param>
        /// <param name="strUserKey">부서코드 or 사용자ID or 사용자사번
        ///                         strSearchType의 값에 따라 변경
        ///                         A : 전체반환이므로 공백
        ///                         D : 부서코드
        ///                         U : ID
        ///                         E : 사번</param>
        /// <returns></returns>
        private string GetPSTSUserInfo(string strSearchType, string strCompany, string strUserKey)
        {
            string strRtnString = string.Empty;
            XmlDocument xml = new XmlDocument();

            XmlNode node = xml.SelectSingleNode(m_xmlGetUserInfo);
            //사용자 검색을 위한 파라미터를 XML Node에 넣는다
            node.InnerText = node.InnerText.Replace("!1!", strSearchType == string.Empty ? @"&nbsp" : strSearchType);
            node.InnerText = node.InnerText.Replace("!2!", strCompany == string.Empty ? @"&nbsp" : strCompany);
            node.InnerText = node.InnerText.Replace("!3!", strUserKey == string.Empty ? @"nbsp" : strUserKey);

            strRtnString = node.OuterXml;

            return strRtnString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCdKind"></param>
        /// <param name="strCdSearchType"></param>
        /// <param name="strCdCompany"></param>
        /// <param name="strCDUserKey"></param>
        /// <returns></returns>
        public DataTable dtUserInfo(string strCdKind, string strCdSearchType, string strCdCompany, string strCDUserKey)
        {
            IF.com.bokwang.ssocore.SyncAgent SyncAgent = new global::QRPGRW.IF.com.bokwang.ssocore.SyncAgent();

            DataTable dtRtn = new DataTable();

            dtRtn = SyncAgent.GetUserInfoByDataTable(strCdKind, strCdSearchType, strCdCompany, strCDUserKey, strPSTSSystemKey);

            return dtRtn;
        }





        private void WriteTempLog(string contents)
        {
            string strFileName = "D:\\Navigator.txt";
            System.IO.FileStream oFS = null;
            System.IO.StreamWriter oSW = null;
            string strLogContents = "";


            try
            {
                //파일스트림 객체 초기화
                oFS = new System.IO.FileStream(
                    //파일 이름 지정
                  strFileName,
                    //파일이 있으면 열고, 없으면 만든다
                  System.IO.FileMode.OpenOrCreate,
                    //파일을 읽기,쓰기 모드로 연다
                  System.IO.FileAccess.ReadWrite);


                //스트림라이터 객체 초기화
                oSW = new System.IO.StreamWriter(oFS);


                //마지막 부분을 찾는다.
                oSW.BaseStream.Seek(0, System.IO.SeekOrigin.End);


                strLogContents = contents;

                oSW.Write(strLogContents + Environment.NewLine);


                //반드시 flush를 해야, 메모리에 있는 내용을 파일에 기록한다.
                //flush하지 않으면 파일을 잠그기 때문에 다른 프로세스가 이 파일에 접근할 수 없다
                oSW.Flush();
                oSW.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oSW = null;
                oFS = null;
            }
        }





    }
}
