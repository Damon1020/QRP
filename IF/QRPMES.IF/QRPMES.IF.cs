﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRP-MES Interface Module                              */
/* 프로그램ID   : QRPMES.IF                                             */
/* 작성자       : 이종민                                                */
/* 작성일자     : 2011-09-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;

using System.Net;
using System.Xml;
using System.Data;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Configuration;
using System.Collections;

using TIBCO.Rendezvous;



namespace QRPMES.IF
{

    /// <summary>
    /// Client에서 MES와 I/F (Message IF) 하기위한 클래스
    /// IF Method 추가시 위의 class 말고 아래에 있는Tibrv class에 추가
    /// </summary>
    public partial class Tibrv : IDisposable
    {
        private static Dispatcher dispatcher;
        private static TIBCO.Rendezvous.Queue queue;
        private static Transport transport;
        private static bool isRun = false;
        private const string xmlCode = "<message><header><sourcesubject></sourcesubject><targetsubject></targetsubject><messagename></messagename><transactionid></transactionid><locale></locale></header><body></body><return><returncode></returncode><returnmessage></returnmessage></return></message>";
        private string m_Debug = "F";
        private bool disposed = false;

        /// <summary>
        /// Non-Debugging, Fixed-DEV MessageIF Connection Info
        /// </summary>
        public Tibrv()
        {
            Info.InitInfo();
            Initialize();
        }

        /// <summary>
        /// Debugging Class Fixed-DEV MessageIF Connection Info
        /// </summary>
        /// <param name="strDebug">object</param>
        public Tibrv(string strDebug)
        {
            Info.InitInfo();
            Initialize();
            m_Debug = "T";
        }

        /// <summary>
        /// Debugging, Set SYSSystemAccessInfo-Connetion Info
        /// </summary>
        /// <param name="strDebug">object</param>
        /// <param name="dtConnect">SYSSystemAccessInfo DataTable</param>
        public Tibrv(string strDebug, DataTable dtConnect)
        {
            Info.InitInfo(dtConnect);
            Initialize();
            m_Debug = "T";
        }

        /// <summary>
        /// Non-Debugging, Set SYSSystemAccessInfo-Connetion Info
        /// </summary>
        /// <param name="dtConnect">SYSSystemAccessInfo DataTable</param>
        public Tibrv(DataTable dtConnect)
        {
            Info.InitInfo(dtConnect);
            Initialize();
        }
        
        private void Initialize()
        {
            try
            {
                if (isRun)
                {
                    Terminate();
                }

                TIBCO.Rendezvous.Environment.Open();
                transport = new NetTransport(Info.Service, Info.Network, Info.Daemon); //"", Info.Daemon); // Info.Network, Info.Daemon);
                queue = new TIBCO.Rendezvous.Queue();
                dispatcher = new Dispatcher(queue);

                //if ((!Info.SourceSubject.Contains("*")) && (!string.IsNullOrEmpty(Info.SourceSubject)))
                //{
                //    listner = new Listener(queue, transport, Info.SourceSubject, null);
                //    listner.MessageReceived += new MessageReceivedEventHandler(OnMessageReceived);
                //}

                isRun = true;
            }
            catch (RendezvousException ex)
            {
                Console.Error.WriteLine(ex.StackTrace);
            }
        }

        private void Terminate()
        {
            if (!isRun) return;

            try
            {
                dispatcher.Destroy();
                queue.Destroy();
                transport.Destroy();

                TIBCO.Rendezvous.Environment.Close();
            }
            catch (Exception ex)
            {
                throw ex;            }
            finally
            {
                isRun = false;
            }
        }

        private void Send(string sendMessage)
        {
            Send(Info.TargetSubject, sendMessage);
        }

        private void Send(string TargetSubect, string sendMessage)
        {
            // Create the message   
            Message message = new Message();
            message.SendSubject = TargetSubect;

            try
            {
                message.AddField(Info.FieldName, sendMessage, 0);

                transport.Send(message);
            }
            catch (RendezvousException ex)
            {
                if (ex.Status == Status.InvalidTransport || ex.Status == Status.DaemonNotConnected)
                {
                    Initialize();
                }

                //Console.Error.WriteLine(ex.StackTrace);
                //throw ex;
                return;
            }
        }

        private string SendRequest(string sendMessage)
        {
            // Create the message
            Message message = new Message();
            message.SendSubject = Info.TargetSubject;
            
            try
            {
                message.AddField(Info.FieldName, sendMessage, 0);

                message = transport.SendRequest(message, Double.Parse(Info.TimeOut));
            }
            catch (RendezvousException ex)
            {
                if (ex.Status == Status.InvalidTransport || ex.Status == Status.DaemonNotConnected)
                {
                    Initialize();
                }
                //Console.Error.WriteLine(ex.StackTrace);
                return "";
            }

            if (message != null)
                return message.GetField(Info.FieldName).Value.ToString();
            else
                return "";
        }

        private string mfCreateTibrvMessage(string strMessageName, DataTable dtBody)
        {
            XmlDocument _xml = new XmlDocument();

            _xml.LoadXml(xmlCode);

            XmlNodeList _headder = _xml.SelectNodes("message/header/*");

            mfSetHeader(ref _headder, strMessageName);

            XmlNode _body = _xml.SelectSingleNode("message/body");
            for (int i = 0; i < dtBody.Rows.Count; i++)
            {
                XmlElement _item = _xml.CreateElement(dtBody.Rows[i]["ItemName"].ToString());
                _item.InnerText = dtBody.Rows[i]["ItemValue"].ToString();

                _body.AppendChild(_item);
            }

            return _xml.OuterXml.ToString();
        }
        private string mfCreateTibrvMessage(string strMessageName, DataTable dtBody, string strItemName, string strItemType, DataTable dtItem)
        {
            XmlDocument _xml = new XmlDocument();

            _xml.LoadXml(xmlCode);

            XmlNodeList _headder = _xml.SelectNodes("message/header/*");

            mfSetHeader(ref _headder, strMessageName);

            XmlNode _body = _xml.SelectSingleNode("message/body");
            for (int i = 0; i < dtBody.Rows.Count; i++)
            {
                XmlElement _item = _xml.CreateElement(dtBody.Rows[i]["ItemName"].ToString());
                if (_item.Name.Equals(strItemName))
                {
                    _body.AppendChild(_item);
                    //XmlElement _itemChild = _xml.CreateElement(strItemName);
                    //_item.AppendChild(_itemChild);


                    for (int j = 0; j < dtItem.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType);
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem.Columns[k].ColumnName);
                            _SecondChild.InnerText = dtItem.Rows[j][k].ToString();
                            _child.AppendChild(_SecondChild);
                        }
                    }
                }
                else
                {
                    _item.InnerText = dtBody.Rows[i]["ItemValue"].ToString();
                    _body.AppendChild(_item);
                }
            }

            return _xml.OuterXml.ToString();
        }
        private string mfCreateTibrvMessage(string strMessageName, DataTable dtBody, string strItemName, string strItemType, DataTable dtItem, string strItemName2, string strItemType2, DataTable dtItem2)
        {
            XmlDocument _xml = new XmlDocument();

            _xml.LoadXml(xmlCode);

            XmlNodeList _headder = _xml.SelectNodes("message/header/*");

            mfSetHeader(ref _headder, strMessageName);

            XmlNode _body = _xml.SelectSingleNode("message/body");
            for (int i = 0; i < dtBody.Rows.Count; i++)
            {
                XmlElement _item = _xml.CreateElement(dtBody.Rows[i]["ItemName"].ToString());
                if (_item.Name.Equals(strItemName))
                {
                    _body.AppendChild(_item);
                    //XmlElement _itemChild = _xml.CreateElement(strItemName);
                    //_item.AppendChild(_itemChild);


                    for (int j = 0; j < dtItem.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType); // + j.ToString());
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem.Columns[k].ColumnName);
                            _SecondChild.InnerText = dtItem.Rows[j][k].ToString();
                            _child.AppendChild(_SecondChild);
                        }
                    }
                }
                else if(_item.Name.Equals(strItemName2))
                {
                    _body.AppendChild(_item);
                    //XmlElement _itemChild = _xml.CreateElement(strItemName);
                    //_item.AppendChild(_itemChild);


                    for (int j = 0; j < dtItem2.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType2); // + j.ToString());
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem2.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem2.Columns[k].ColumnName);
                            _SecondChild.InnerText = dtItem2.Rows[j][k].ToString();
                            _child.AppendChild(_SecondChild);
                        }
                    }
                }
                else
                {
                    _item.InnerText = dtBody.Rows[i]["ItemValue"].ToString();
                    _body.AppendChild(_item);
                }
            }

            return _xml.OuterXml.ToString();
        }

        private string mfCreateTibrvMessage(string strMessageName, DataTable dtBody
                                        , string strItemName, string strItemType, DataTable dtItem
                                        , string strItemName2, string strItemType2, DataTable dtItem2
                                        , string strItemName3, string strItemType3, DataTable dtItem3)
        {
            //XML
            XmlDocument _xml = new XmlDocument();

            //전역변수에 저장된 Xml코드 Load
            _xml.LoadXml(xmlCode);

            XmlNodeList _headder = _xml.SelectNodes("message/header/*");

            mfSetHeader(ref _headder, strMessageName);

            XmlNode _body = _xml.SelectSingleNode("message/body");
            for (int i = 0; i < dtBody.Rows.Count; i++)
            {
                XmlElement _item = _xml.CreateElement(dtBody.Rows[i]["ItemName"].ToString());
                if (_item.Name.Equals(strItemName))
                {
                    _body.AppendChild(_item);
                    //XmlElement _itemChild = _xml.CreateElement(strItemName);
                    //_item.AppendChild(_itemChild);


                    for (int j = 0; j < dtItem.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType); // + j.ToString());
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem.Columns[k].ColumnName);
                            _SecondChild.InnerText = dtItem.Rows[j][k].ToString();
                            _child.AppendChild(_SecondChild);
                        }
                    }
                }
                else if (_item.Name.Equals(strItemName2))
                {
                    _body.AppendChild(_item);
                    //XmlElement _itemChild = _xml.CreateElement(strItemName);
                    //_item.AppendChild(_itemChild);


                    for (int j = 0; j < dtItem2.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType2); // + j.ToString());
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem2.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem2.Columns[k].ColumnName);
                            _SecondChild.InnerText = dtItem2.Rows[j][k].ToString();
                            _child.AppendChild(_SecondChild);
                        }
                    }
                }
                else if (_item.Name.Equals(strItemName3))
                {
                    _body.AppendChild(_item);
                    //XmlElement _itemChild = _xml.CreateElement(strItemName);
                    //_item.AppendChild(_itemChild);


                    for (int j = 0; j < dtItem3.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType3); // + j.ToString());
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem3.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem3.Columns[k].ColumnName);
                            _SecondChild.InnerText = dtItem3.Rows[j][k].ToString();
                            _child.AppendChild(_SecondChild);
                        }
                    }
                }
                else
                {
                    _item.InnerText = dtBody.Rows[i]["ItemValue"].ToString();
                    _body.AppendChild(_item);
                }
            }

            return _xml.OuterXml.ToString();
        }

        private string mfCreateTibrvMessage_Child(string strMessageName, DataTable dtBody, string strItemName, string strItemType, DataTable dtItem, string strItemName1, string strItemType1, DataTable dtItem1)
        {
            XmlDocument _xml = new XmlDocument();

            _xml.LoadXml(xmlCode);

            XmlNodeList _headder = _xml.SelectNodes("message/header/*");

            mfSetHeader(ref _headder, strMessageName);

            XmlNode _body = _xml.SelectSingleNode("message/body");
            for (int i = 0; i < dtBody.Rows.Count; i++)
            {
                XmlElement _item = _xml.CreateElement(dtBody.Rows[i]["ItemName"].ToString());
                if (_item.Name.Equals(strItemName))
                {
                    _body.AppendChild(_item);

                    for (int j = 0; j < dtItem.Rows.Count; j++)
                    {
                        XmlElement _child = _xml.CreateElement(strItemType);
                        _item.AppendChild(_child);

                        for (int k = 0; k < dtItem.Columns.Count; k++)
                        {
                            XmlElement _SecondChild = _xml.CreateElement(dtItem.Columns[k].ColumnName);
                            if (_SecondChild.Name.Equals(strItemName1))
                            {
                                _child.AppendChild(_SecondChild);

                                for (int a = 0; a < dtItem1.Rows.Count; a++)
                                {
                                    XmlElement _child1 = _xml.CreateElement(strItemType1);
                                    _SecondChild.AppendChild(_child1);

                                    for (int b = 0; b < dtItem1.Columns.Count; b++)
                                    {
                                        XmlElement _SecondChild1 = _xml.CreateElement(dtItem1.Columns[b].ColumnName);
                                        _SecondChild1.InnerText = dtItem1.Rows[a][b].ToString();
                                        _child1.AppendChild(_SecondChild1);
                                    }
                                }
                            }
                            else
                            {
                                _SecondChild.InnerText = dtItem.Rows[j][k].ToString();
                                _child.AppendChild(_SecondChild);
                            }
                        }
                    }
                }
                else
                {
                    _item.InnerText = dtBody.Rows[i]["ItemValue"].ToString();
                    _body.AppendChild(_item);
                }
            }

            return _xml.OuterXml.ToString();
        }

        private void mfSetHeader(ref XmlNodeList _Header, string strMessageName)
        {
            foreach (XmlElement _node in _Header)
            {
                switch (_node.Name)
                {
                    case "sourcesubject":
                        //_node.InnerText = "QRP.10.60.24.163";
                        _node.InnerText = Info.SourceSubject;
                        break;
                    case "targetsubject":
                        //_node.InnerText = "AIM.DEV.CNX.DQ";
                        _node.InnerText = Info.TargetSubject;
                        break;
                    case "messagename":
                        _node.InnerText = strMessageName;
                        break;
                    case "transactionid":
                        _node.InnerText = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                        break;
                    case "locale":
                        _node.InnerText = "kr";
                        break;
                }
            }
        }

        private DataTable mfDataTableInfo()
        {
            DataTable _dt = new DataTable();
            _dt.Columns.Add("ItemName", typeof(string));
            _dt.Columns.Add("ItemValue", typeof(string));

            return _dt;
        }
        private DataTable mfDataTableInfo_Item()
        {
            DataTable _dt = new DataTable();
            _dt.Columns.Add("PName", typeof(string));
            _dt.Columns.Add("ItemName", typeof(string));
            _dt.Columns.Add("ItemValue", typeof(string));

            return _dt;
        }

        /// <summary>
        /// Replay Message Check ( MessageName, TransactionID )
        /// </summary>
        /// <param name="strSendMessage">string sendmessage</param>
        /// <param name="strReplyMessage">string replymessage</param>
        /// <returns>if Same then retern true else false</returns>
        private Boolean mfCheckReply(string strSendMessage, string strReplyMessage)
        {
            XmlDocument xmlSend = new XmlDocument();
            xmlSend.LoadXml(strSendMessage);
            XmlDocument xmlReply = new XmlDocument();
            xmlReply.LoadXml(strReplyMessage);

            //XmlNode nodeSendMessageName = xmlSend.SelectSingleNode("message/header/messagename");
            //XmlNode nodeReplyMessageName = xmlReply.SelectSingleNode("message/header/messagename");
            XmlNode nodeSendTransactionID = xmlSend.SelectSingleNode("message/header/transactionid");
            XmlNode nodeReplyTransactionID = xmlReply.SelectSingleNode("message/header/transactionid");

            //if (!nodeSendMessageName.InnerText.ToString().Equals(nodeReplyMessageName.InnerText.ToString()))
            //{
            //    return false;
            //}
            //else
            //{
                if (!nodeSendTransactionID.InnerText.ToString().Equals(nodeReplyTransactionID.InnerText.ToString()))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            //}
        }

        private void mfSaveMEFIFLog(string strFormName, string strSendMessage, string strReplyMessage, string strUserIP, string strUserID)
        {
            try
            {
                //QRPJOB.BL.BATJOB.MESIF clsMESID = new QRPJOB.BL.BATJOB.MESIF();
                ////if (m_Debug.Equals("F"))
                ////{
                ////    mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.MESIF), "MESIF");
                ////    clsMESID = new QRPJOB.BL.BATJOB.MESIF();
                ////    mfCredentials(clsMESID);
                ////}
                ////else
                ////{
                ////    clsMESID = new QRPJOB.BL.BATJOB.MESIF();
                ////}

                XmlDocument _Send = new XmlDocument();
                _Send.LoadXml(strSendMessage);
                XmlDocument _Reply = new XmlDocument();
                _Reply.LoadXml(strReplyMessage);

                string strReturnCode = _Reply.SelectSingleNode("message/return/returncode").InnerText;
                string strReturnMessage = _Reply.SelectSingleNode("message/return/returnmessage").InnerText;
                string strsend = _Reply.SelectSingleNode("message").OuterXml.ToString();

                //clsMESID.mfSaveMESIFErrorLog(strFormName, strSendMessage, strsend, strReturnCode, strReturnMessage, strUserIP, strUserID);
                mfSaveMESIFErrorLog(strFormName, strSendMessage, strsend, strReturnCode, strReturnMessage, strUserIP, strUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// .NET Remoting을 위한 채널 및 Proxy 생성
        /// </summary>
        /// <param name="TypeName"></param>
        /// <param name="strClassName"></param>
        private void mfRegisterChannel(Type TypeName, string strClassName)
        {
            try
            {
                WellKnownClientTypeEntry[] entry = RemotingConfiguration.GetRegisteredWellKnownClientTypes();
                foreach (WellKnownClientTypeEntry E in entry)
                {
                    if (E.TypeName.ToString() == TypeName.FullName.ToString())
                        return;
                }

                //채널 생성
                bool bolMakeChannel = false;
                foreach (IChannel chn in ChannelServices.RegisteredChannels)
                {
                    if (chn.ChannelName == "http")
                    {
                        bolMakeChannel = true;
                        break;
                    }
                }
                if (bolMakeChannel == false)
                {
                    HttpChannel channel = new HttpChannel();
                    ChannelServices.RegisterChannel(channel, true);
                }
                
                //서버활성화방식(Server Activated Object : SAO - WellKnwon 객체)에서 Proxy 객체 생성
                string RemoteServer = "http://12.230.85.137/QRP_PSTS_DEV/";
                //RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off; //신규추가
                RemotingConfiguration.RegisterWellKnownClientType(TypeName, RemoteServer + strClassName + ".rem");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// .NET Remoting 인증 설정
        /// </summary>
        /// <param name="obj"></param>
        private void mfCredentials(object obj)
        {
            try
            {
                //char[] sep = { ';' };
                //string[] cre = ConfigurationManager.AppSettings["Credentials"].ToString().Split(sep);
                IDictionary Props = ChannelServices.GetChannelSinkProperties(obj);
                //Props["credentials"] = new NetworkCredential(cre[0], cre[1]);
                Props["credentials"] = CredentialCache.DefaultCredentials;
                Props["preauthenticate"] = true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                // Dispose of resources held by this instance.
                // (This process is not shown here.)
                Terminate();

                // Set the sentinel.
                disposed = true;

                // Suppress finalization of this disposed instance.
                if (disposing)
                {
                    GC.SuppressFinalize(this);
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        // Disposable types implement a finalizer.
        ~Tibrv()
        {
            Dispose(false);
        }
    }

    static class Info
    {
        private static string strService;
        private static string strNetWork;
        private static string strDaemon;
        private static string strSourceSubject;
        private static string strTargeSubject;
        private static IPHostEntry ip;

        /// <summary>
        /// TIBrv Connection Info : Service, Network, Daemon, Subject
        /// </summary>
        public static string Service
        {
            get { return strService; }
            set { strService = value; }
        }

        public static string Network
        {
            get { return strNetWork; }
            set { strNetWork = value; }
        }

        public static string Daemon
        {
            get { return strDaemon; }
            set { strDaemon = value; }
        }

        public static string SourceSubject
        {
            get { return strSourceSubject; }
            set { strSourceSubject = value; }
        }

        public static string TargetSubject
        {
            get { return strTargeSubject; }
            set { strTargeSubject = value; }
        }

        public static string FieldName = "";
        public static string TimeOut = "";

        /// <summary>
        /// Initialze Config
        /// </summary>
        public static void InitInfo(DataTable dtConnect)
        {
            ////// Live서버
            ////ip = Dns.GetHostEntry(Dns.GetHostName());
            ////FieldName = "xmlData";
            ////TimeOut = "50";
            ////Service = dtConnect.Rows[0]["EtcAccessInfo2"].ToString();
            ////Network = dtConnect.Rows[0]["EtcAccessInfo3"].ToString();
            ////Daemon = dtConnect.Rows[0]["SystemAddressPath"].ToString(); //"tcp:10.60.60.62:7500"; // "tcp:" + ip.AddressList[0].ToString() + ":8800"; //
            ////SourceSubject = "QRP." + ip.AddressList[0].ToString();
            ////TargetSubject = dtConnect.Rows[0]["EtcAccessInfo1"].ToString();  //"STS.PRD.CNM.DQ";
            
            ////// 테스트서버
            ////ip = Dns.GetHostEntry(Dns.GetHostName());
            ////FieldName = "xmlData";
            ////TimeOut = "50";
            ////Service = "11200";
            ////Network = ";225.13.13.2";
            ////Daemon = dtConnect.Rows[0]["SystemAddressPath"].ToString();
            ////SourceSubject = "QRP." + ip.AddressList[0].ToString();
            ////TargetSubject = dtConnect.Rows[0]["EtcAccessInfo1"].ToString();

            // Live 서버
            ip = Dns.GetHostEntry(Dns.GetHostName());
            FieldName = "xmlData";
            TimeOut = "100";
            Service = "7777";
            Network = ";239.77.77.77";
            //Daemon = "tcp:10.61.61.51.:7500";   //MES Main IP
            Daemon = "tcp:10.61.61.55.:7500";     //중간 Skip 서버
            SourceSubject = "QRP." + ip.AddressList[0].ToString();
            TargetSubject = "PSTS.PRD.CNM.DQ";

            //// Test 서버
            //ip = Dns.GetHostEntry(Dns.GetHostName());
            //FieldName = "xmlData";
            //TimeOut = "50";
            //Service = "7777";
            //Network = ";239.77.77.77";
            //Daemon = "tcp:10.61.61.57.:7500";
            //SourceSubject = "QRP." + ip.AddressList[0].ToString();
            //TargetSubject = "PSTS.DEV.CNM.DQ";
        }

        public static void InitInfo()
        {
            ////// Live 서버
            ip = Dns.GetHostEntry(Dns.GetHostName());
            FieldName = "xmlData";
            TimeOut = "100";
            Service = "7777";
            Network = ";239.77.77.77";
            //Daemon = "tcp:10.61.61.51.:7500";   //MES Main IP
            Daemon = "tcp:10.61.61.55.:7500";     //중간 Skip 서버
            SourceSubject = "QRP." + ip.AddressList[0].ToString();
            TargetSubject = "PSTS.PRD.CNM.DQ";

            //// Test 서버
            //ip = Dns.GetHostEntry(Dns.GetHostName());
            //FieldName = "xmlData";
            //TimeOut = "50";
            //Service = "7777";
            //Network = ";239.77.77.77";
            //Daemon = "tcp:10.61.61.57.:7500";
            //SourceSubject = "QRP." + ip.AddressList[0].ToString();
            //TargetSubject = "PSTS.DEV.CNM.DQ";
        }
    }

    public partial class Tibrv
    {
        /// <summary>
        /// Lot 정보 요청
        /// </summary>
        /// <param name="strLotID">Lot ID</param>
        /// <returns> Column[0] LOTID
        ///           Column[1] PRODUCTSPECNAME as ProductCode
        ///           Column[2] PRODUCTSPECDESC as ProductName                   
        ///           Column[3] CUSTOMPRODUCTSPECNAME as Customer ProductCode
        ///           Column[4] OPERID as ProcessCode
        ///           Column[5] OPERDESC as ProcessName
        ///           Column[6] EQPID as EquipCode
        ///           Column[7] EQPDESC as EquipName
        ///           Column[8] QTY
        ///           Column[9] PACKAGE
        ///           Column[10] WORKOPERID
        ///           Colimn[11] LOTPROCESSSTATE
        ///           Colimn[12] LOTHOLDSTATE
        ///           Colimn[13] LASTEVENTUSER
        ///           Column[14] returncode
        ///           Column[15] returnmessage</returns>
        public DataTable LOT_INFO_REQ(string strLotID)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("LOTID", typeof(string));
            dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
            dtRtn.Columns.Add("PRODUCTSPECDESC", typeof(string));
            dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
            dtRtn.Columns.Add("OPERID", typeof(string));
            dtRtn.Columns.Add("OPERDESC", typeof(string));
            dtRtn.Columns.Add("EQPID", typeof(string));
            dtRtn.Columns.Add("EQPDESC", typeof(string));
            dtRtn.Columns.Add("QTY", typeof(string));
            dtRtn.Columns.Add("PACKAGE", typeof(string));
            dtRtn.Columns.Add("WORKOPERID", typeof(string));
            dtRtn.Columns.Add("LOTPROCESSSTATE", typeof(string));
            dtRtn.Columns.Add("LOTHOLDSTATE", typeof(string));
            dtRtn.Columns.Add("LASTEVENTUSER", typeof(string));
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));

            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtLot = mfDataTableInfo();
                DataRow drLot = dtLot.NewRow();
                drLot["ItemName"] = "LOTID";
                drLot["ItemValue"] = strLotID;
                dtLot.Rows.Add(drLot);

                string strSendMessage = mfCreateTibrvMessage("LOT_INFO_REQ", dtLot);
                #endregion

                //if ((!Info.SourceSubject.Contains("*")) && (!string.IsNullOrEmpty(Info.SourceSubject)))
                //{
                //    listner = new Listener(queue, transport, Info.SourceSubject, null);
                //    listner.MessageReceived += new MessageReceivedEventHandler(LOT_INFO_R);
                //}

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        foreach (XmlNode _node in xmlRtn.SelectNodes("message/body/*"))
                        {
                            if (drRtn.Table.Columns.Contains(_node.Name.ToString()))
                            {
                                drRtn[_node.Name.ToString()] = _node.InnerText;
                            }
                        }
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText;
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText;
                        dtRtn.Rows.Add(drRtn);

                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("LOTID", typeof(string));
                        dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
                        dtRtn.Columns.Add("PRODUCTSPECDESC", typeof(string));
                        dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
                        dtRtn.Columns.Add("OPERID", typeof(string));
                        dtRtn.Columns.Add("OPERDESC", typeof(string));
                        dtRtn.Columns.Add("EQPID", typeof(string));
                        dtRtn.Columns.Add("EQPDESC", typeof(string));
                        dtRtn.Columns.Add("QTY", typeof(string));
                        dtRtn.Columns.Add("PACKAGE", typeof(string));
                        dtRtn.Columns.Add("WORKOPERID", typeof(string));
                        dtRtn.Columns.Add("LOTPROCESSSTATE", typeof(string));
                        dtRtn.Columns.Add("LOTHOLDSTATE", typeof(string));
                        dtRtn.Columns.Add("LASTEVENTUSER", typeof(string));
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("LOTID", typeof(string));
                    dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
                    dtRtn.Columns.Add("PRODUCTSPECDESC", typeof(string));
                    dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
                    dtRtn.Columns.Add("OPERID", typeof(string));
                    dtRtn.Columns.Add("OPERDESC", typeof(string));
                    dtRtn.Columns.Add("EQPID", typeof(string));
                    dtRtn.Columns.Add("EQPDESC", typeof(string));
                    dtRtn.Columns.Add("QTY", typeof(string));
                    dtRtn.Columns.Add("PACKAGE", typeof(string));
                    dtRtn.Columns.Add("WORKOPERID", typeof(string));
                    dtRtn.Columns.Add("LOTPROCESSSTATE", typeof(string));
                    dtRtn.Columns.Add("LOTHOLDSTATE", typeof(string));
                    dtRtn.Columns.Add("LASTEVENTUSER", typeof(string));
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("LOTID", typeof(string));
                dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
                dtRtn.Columns.Add("PRODUCTSPECDESC", typeof(string));
                dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
                dtRtn.Columns.Add("OPERID", typeof(string));
                dtRtn.Columns.Add("OPERDESC", typeof(string));
                dtRtn.Columns.Add("EQPID", typeof(string));
                dtRtn.Columns.Add("EQPDESC", typeof(string));
                dtRtn.Columns.Add("QTY", typeof(string));
                dtRtn.Columns.Add("PACKAGE", typeof(string));
                dtRtn.Columns.Add("WORKOPERID", typeof(string));
                dtRtn.Columns.Add("LOTPROCESSSTATE", typeof(string));
                dtRtn.Columns.Add("LOTHOLDSTATE", typeof(string));
                dtRtn.Columns.Add("LASTEVENTUSER", typeof(string));
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// 원자재 이상발생정보
        /// </summary>
        /// <param name="strFormName">form name</param>              
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strCOMMENT">주석</param>
        /// <param name="strHOLDCODE">Hold Code</param>
        /// <param name="dtCONSUMABLELIST">Column : CONSUMABLEID (자재 Lot ID) </param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable CON_HOLD4QC_REQ(string strFormName, string strUSERID, string strCOMMENT, string strHOLDCODE, DataTable dtCONSUMABLELIST, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();
                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);
                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);
                drCON = dtCON.NewRow();
                drCON["ItemName"] = "HOLDCODE";
                drCON["ItemValue"] = strHOLDCODE;
                dtCON.Rows.Add(drCON);
                drCON = dtCON.NewRow();
                drCON["ItemName"] = "CONSUMABLELIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("CON_HOLD4QC_REQ", dtCON, "CONSUMABLELIST", "CONSUMABLE", dtCONSUMABLELIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if(!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// 원자재 특채등록
        /// </summary>
        /// <param name="strFormName">form name</param>              
        /// <param name="strUSERID">작업자 ID</param>
        /// <param name="strCOMMENT">주석</param>
        /// <param name="dtCONSUMABLELIST">Column : CONSUMABLEID    자재 LOTID
        ///                                       : HOLDCODE        Hold Code
        ///                                       : ACTIONTYPE      조치결과(RELEASE, MRP, SCRAP)</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable CON_RELEASE4QC_REQ(string strFormName, string strUSERID, string strCOMMENT, DataTable dtCONSUMABLELIST, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "CONSUMABLELIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("CON_RELEASE4QC_REQ", dtCON, "CONSUMABLELIST", "CONSUMABLE", dtCONSUMABLELIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if(!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if  (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// LOT 원인 예상 공정 작업 진행 설비정보 요청
        /// </summary>
        /// <param name="strLOTID">Lot ID</param>
        /// <param name="strOPERID">예상공정 ID</param>
        /// <returns>EQPID      설비ID
        ///          USERID     작업자 ID
        ///          USERNAME     작업자명
        ///          returncode
        ///          returnmessage</returns>
        public DataTable LOT_PROCESSED_EQP_REQ(string strLOTID, string strOPERID)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("EQPID", typeof(string));
            dtRtn.Columns.Add("USERID", typeof(string));
            dtRtn.Columns.Add("USERNAME", typeof(string));
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtLot = mfDataTableInfo();
                DataRow drLot = dtLot.NewRow();
                drLot["ItemName"] = "LOTID";
                drLot["ItemValue"] = strLOTID;
                dtLot.Rows.Add(drLot);

                drLot = dtLot.NewRow();
                drLot["ItemName"] = "OPERID";
                drLot["ItemValue"] = strOPERID;
                dtLot.Rows.Add(drLot);

                string strSendMessage = mfCreateTibrvMessage("LOT_PROCESSED_EQP_REQ", dtLot);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if(!strRtn.Equals(string.Empty))
                {
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        #region Create Return DataTable via Reply Message
                        if (!strRtn.Equals(string.Empty))
                        {
                            XmlDocument xmlRtn = new XmlDocument();
                            xmlRtn.LoadXml(strRtn);

                            XmlNodeList xmlNode = xmlRtn.SelectNodes("message/body/EQPLIST");

                            string strreturncode = xmlRtn.SelectSingleNode("message/return/returncode").InnerText;
                            string strreturnmessage = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText;


                            
                            if (!xmlNode.Count.Equals(0))
                            {
                                for (int i = 0; i < xmlNode[0].ChildNodes.Count; i++)
                                {
                                    DataRow drRtn = dtRtn.NewRow();
                                    for (int j = 0; j < xmlNode[0].ChildNodes[i].ChildNodes.Count; j++)
                                    {
                                        if (drRtn.Table.Columns.Contains(xmlNode[0].ChildNodes[i].ChildNodes[j].Name))
                                        {
                                            drRtn[xmlNode[0].ChildNodes[i].ChildNodes[j].Name] = xmlNode[0].ChildNodes[i].ChildNodes[j].InnerText;
                                        }
                                    }
                                    drRtn["returncode"] = strreturncode;
                                    drRtn["returnmessage"] = strreturnmessage;
                                    dtRtn.Rows.Add(drRtn);
                                }
                            }



                            
                            return dtRtn;
                        }
                        else
                        {
                            dtRtn = new DataTable();
                            dtRtn.Columns.Add("EQPID", typeof(string));
                            dtRtn.Columns.Add("USERID", typeof(string));
                            dtRtn.Columns.Add("USERNAME", typeof(string));
                            return dtRtn;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Return
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("EQPID", typeof(string));
                        dtRtn.Columns.Add("USERID", typeof(string));
                        dtRtn.Columns.Add("USERNAME", typeof(string));
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                        #endregion
                    }
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("EQPID", typeof(string));
                    dtRtn.Columns.Add("USERID", typeof(string));
                    dtRtn.Columns.Add("USERNAME", typeof(string));
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("EQPID", typeof(string));
                dtRtn.Columns.Add("USERID", typeof(string));
                dtRtn.Columns.Add("USERNAME", typeof(string));
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// Lot 홀드 요청
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strUSERID">작업자 ID</param>
        /// <param name="strCOMMENT">주석</param>
        /// <param name="strHOLDCODE">Hold Code</param>
        /// <param name="dtLOTLIST">ColumnName : LOTID</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_HOLD4QC_REQ(string strFormName, string strUSERID, string strCOMMENT, string strHOLDCODE, DataTable dtLOTLIST, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "HOLDCODE";
                drCON["ItemValue"] = strHOLDCODE;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_HOLD4QC_REQ", dtCON, "LOTLIST", "LOT", dtLOTLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);
                
                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }
        
        /// <summary>
        /// 공정 이상발생 Lot 관련 Affect Lot(설비의 전후 작업 진행 Lot)
        /// </summary>
        /// <param name="strLOTID">LOT ID</param>
        /// <param name="strOPERID">공정 ID</param>
        /// <param name="strEQPID">Affect 설비 ID</param>
        /// <returns> 첫번째 Row FrontLot
        ///             LOTID                       LOTID
        ///             PRODUCTSPECNAME             제품 ID
        ///             CUSTOMPRODUCTSPECNAME       고객사 제품ID
        ///             OPERID                      공정ID
        ///             EQPID                       설비ID
        ///             QTY                         수량
        ///             EVENTTIME                   작업시간 (YYYYMMDDHHMISS)
        ///             USERID                      작업자ID
        ///             returncode
        ///             returnmessage
        ///           두번째 Row BackLot
        ///             LOTID                       LOTID
        ///             PRODUCTSPECNAME             제품 ID
        ///             CUSTOMPRODUCTSPECNAME       고객사 제품ID
        ///             OPERID                      공정ID
        ///             EQPID                       설비ID
        ///             QTY                         수량
        ///             EVENTTIME                   작업시간 (YYYYMMDDHHMISS)
        ///             USERID                      작업자ID
        ///             EVENTNAME                   현재상태                2012-11-16 추가 권종구
        ///             PROCESSOPERATIONNAME        현재공정                2012-11-16 추가 권종구
        ///             returncode
        ///             returnmessage
        /// </returns>
        public DataTable LOT_AFFECT_REQ(string strLOTID, string strOPERID, string strEQPID)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("LOTTYPE", typeof(string));
            dtRtn.Columns.Add("LOTID", typeof(string));
            dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
            dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
            dtRtn.Columns.Add("OPERID", typeof(string));
            dtRtn.Columns.Add("EQPID", typeof(string));
            dtRtn.Columns.Add("QTY", typeof(string));
            dtRtn.Columns.Add("EVENTTIME", typeof(string));
            dtRtn.Columns.Add("USERID", typeof(string));
            dtRtn.Columns.Add("EVENTNAME", typeof(string));             // 2012-11-16 추가 Lot 현재 상태
            dtRtn.Columns.Add("PROCESSOPERATIONNAME", typeof(string));  // 2012-11-16 추가 Lot 현재 공정
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion
   
            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "OPERID";
                drCON["ItemValue"] = strOPERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEQPID;
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_AFFECT_REQ", dtCON);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        XmlNodeList xmlNode = xmlRtn.SelectNodes("message/body/FRONTLOT");

                        string returncode = xmlRtn.SelectSingleNode("message/return/returncode").InnerText;
                        string returnmessage = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText;

                        #region 2012-10-30  전, 후 Lot 5개씩 검색
                        DataRow drRtn = dtRtn.NewRow();
                        //DataRow drRtn;
                        if (xmlNode.Count != 0)
                        {
                            int intCheck = 0;
                            for (int i = 0; i < xmlNode[0].ChildNodes.Count; i++)
                            {
                                if (xmlNode[0].ChildNodes[i].Name.Equals("LOTID"))
                                {
                                    if (!drRtn["LOTID"].Equals(string.Empty) && drRtn["LOTID"] != DBNull.Value)
                                        dtRtn.Rows.Add(drRtn);

                                    // LotID 면 새로운 행추가
                                    drRtn = dtRtn.NewRow();
                                }
                                if (drRtn.Table.Columns.Contains(xmlNode[0].ChildNodes[i].Name))
                                {
                                    drRtn[xmlNode[0].ChildNodes[i].Name] = xmlNode[0].ChildNodes[i].InnerText;
                                    intCheck++;
                                }

                                drRtn["LOTTYPE"] = "B";
                                drRtn["returncode"] = returncode;
                                drRtn["returnmessage"] = returnmessage;
                            }
                            if (intCheck != 0)
                                dtRtn.Rows.Add(drRtn);
                        }
                        else
                        {
                            drRtn = dtRtn.NewRow();
                            drRtn["LOTTYPE"] = "B";
                            drRtn["returncode"] = returncode;
                            drRtn["returnmessage"] = returnmessage;
                            dtRtn.Rows.Add(drRtn);
                        }

                        xmlNode = xmlRtn.SelectNodes("message/body/BACKLOT");


                        if (xmlNode.Count != 0)
                        {
                            int intCheck = 0;
                            for (int i = 0; i < xmlNode[0].ChildNodes.Count; i++)
                            {
                                if (xmlNode[0].ChildNodes[i].Name.Equals("LOTID"))
                                {
                                    if (!drRtn["LOTID"].Equals(string.Empty) && drRtn["LOTID"] != DBNull.Value && i != 0)
                                        dtRtn.Rows.Add(drRtn);

                                    // LotID 면 새로운 행추가
                                    drRtn = dtRtn.NewRow();
                                }
                                if (drRtn.Table.Columns.Contains(xmlNode[0].ChildNodes[i].Name))
                                {
                                    drRtn[xmlNode[0].ChildNodes[i].Name] = xmlNode[0].ChildNodes[i].InnerText;
                                    intCheck++;
                                }

                                drRtn["LOTTYPE"] = "A";
                                drRtn["returncode"] = returncode;
                                drRtn["returnmessage"] = returnmessage;
                            }
                            if (intCheck != 0)
                                dtRtn.Rows.Add(drRtn);

                        }
                        else
                        {
                            drRtn = dtRtn.NewRow();
                            drRtn["LOTTYPE"] = "A";
                            drRtn["returncode"] = returncode;
                            drRtn["returnmessage"] = returnmessage;
                            dtRtn.Rows.Add(drRtn);
                        }
                        #endregion

                        #region 주석 2012-10-30
                        //DataRow drRtn = dtRtn.NewRow();
                        //if (!xmlNode.Count.Equals(0))
                        //{
                        //    for (int i = 0; i < xmlNode[0].ChildNodes.Count; i++)
                        //    {
                        //        if (drRtn.Table.Columns.Contains(xmlNode[0].ChildNodes[i].Name))
                        //        {
                        //            drRtn[xmlNode[0].ChildNodes[i].Name] = xmlNode[0].ChildNodes[i].InnerText;
                        //        }
                        //    }
                        //}
                        //drRtn["LOTTYPE"] = "B";
                        //drRtn["returncode"] = returncode;
                        //drRtn["returnmessage"] = returnmessage;
                        //dtRtn.Rows.Add(drRtn);

                        //xmlNode = xmlRtn.SelectNodes("message/body/BACKLOT");

                        //drRtn = dtRtn.NewRow();
                        //if (!xmlNode.Count.Equals(0))
                        //{

                        //    for (int i = 0; i < xmlNode[0].ChildNodes.Count; i++)
                        //    {
                        //        if (drRtn.Table.Columns.Contains(xmlNode[0].ChildNodes[i].Name))
                        //        {
                        //            drRtn[xmlNode[0].ChildNodes[i].Name] = xmlNode[0].ChildNodes[i].InnerText;
                        //        }
                        //    }
                        //}
                        //drRtn["LOTTYPE"] = "A";
                        //drRtn["returncode"] = returncode;
                        //drRtn["returnmessage"] = returnmessage;
                        //dtRtn.Rows.Add(drRtn);
                        
                        //return dtRtn;
                        #endregion

                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("LOTTYPE", typeof(string));
                        dtRtn.Columns.Add("LOTID", typeof(string));
                        dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
                        dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
                        dtRtn.Columns.Add("OPERID", typeof(string));
                        dtRtn.Columns.Add("EQPID", typeof(string));
                        dtRtn.Columns.Add("QTY", typeof(string));
                        dtRtn.Columns.Add("EVENTTIME", typeof(string));
                        dtRtn.Columns.Add("USERID", typeof(string));
                        dtRtn.Columns.Add("EVENTNAME", typeof(string));             // 2012-11-16 추가 Lot 현재 상태
                        dtRtn.Columns.Add("PROCESSOPERATIONNAME", typeof(string));  // 2012-11-16 추가 Lot 현재 공정
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("LOTTYPE", typeof(string));
                    dtRtn.Columns.Add("LOTID", typeof(string));
                    dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
                    dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
                    dtRtn.Columns.Add("OPERID", typeof(string));
                    dtRtn.Columns.Add("EQPID", typeof(string));
                    dtRtn.Columns.Add("QTY", typeof(string));
                    dtRtn.Columns.Add("EVENTTIME", typeof(string));
                    dtRtn.Columns.Add("USERID", typeof(string));
                    dtRtn.Columns.Add("EVENTNAME", typeof(string));             // 2012-11-16 추가 Lot 현재 상태
                    dtRtn.Columns.Add("PROCESSOPERATIONNAME", typeof(string));  // 2012-11-16 추가 Lot 현재 공정
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("LOTTYPE", typeof(string));
                dtRtn.Columns.Add("LOTID", typeof(string));
                dtRtn.Columns.Add("PRODUCTSPECNAME", typeof(string));
                dtRtn.Columns.Add("CUSTOMPRODUCTSPECNAME", typeof(string));
                dtRtn.Columns.Add("OPERID", typeof(string));
                dtRtn.Columns.Add("EQPID", typeof(string));
                dtRtn.Columns.Add("QTY", typeof(string));
                dtRtn.Columns.Add("EVENTTIME", typeof(string));
                dtRtn.Columns.Add("USERID", typeof(string));
                dtRtn.Columns.Add("EVENTNAME", typeof(string));             // 2012-11-16 추가 Lot 현재 상태
                dtRtn.Columns.Add("PROCESSOPERATIONNAME", typeof(string));  // 2012-11-16 추가 Lot 현재 공정
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strCOMMENT">주석</param>
        /// <param name="dtLOTLIST">LOTID
        ///                       HOLDCODE</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_RELEASE4QC_REQ(string strFormName, string strUSERID, string strCOMMENT, DataTable dtLOTLIST, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_RELEASE4QC_REQ", dtCON, "LOTLIST", "LOT", dtLOTLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// 저수율 검토 결과 요청
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strOPERID">ProcessCode</param>
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strEQPID">설비코드</param>
        /// <param name="strSCRAPFLAG">Scrap여부 Y|N</param>
        /// <param name="dtSCRAPLIST">SCRAPCODE
        ///                           SCRAPQTY</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOW_YIELD_RESULT_REQ(string strFormName, string strLOTID, string strOPERID, string strUSERID, string strEQPID, string strSCRAPFLAG, DataTable dtSCRAPLIST, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "OPERID";
                drCON["ItemValue"] = strOPERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEQPID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPFLAG";
                drCON["ItemValue"] = strSCRAPFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOW_YIELD_RESULT_REQ", dtCON, "SCRAPLIST", "SCRAP", dtSCRAPLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// 설비 인증 완료
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strEQPID">설비코드</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable EQP_CONFIRM_REQ(string strFormName, string strUSERID, string strEQPID, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtLot = mfDataTableInfo();
                DataRow drLot = dtLot.NewRow();
                drLot["ItemName"] = "USERID";
                drLot["ItemValue"] = strUSERID;
                dtLot.Rows.Add(drLot);

                drLot = dtLot.NewRow();
                drLot["ItemName"] = "EQPID";
                drLot["ItemValue"] = strEQPID;
                dtLot.Rows.Add(drLot);

                string strSendMessage = mfCreateTibrvMessage("EQP_CONFIRM_REQ", dtLot);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// Lot을 검사 작업 시작 요청
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">작업자 ID</param>
        /// <param name="dtLOTLIST">Column[0] : LOTID as LotNo
        ///                         Column[1] : EQPID as EquipCode
        ///                         Column[2] : RECIPEID as Recipe id (not use '*')
        ///                         Column[3] : AREAID as Area Code
        ///                         Column[4] : IQASAMPLINGFLAG as IQCSampling 여부
        ///                         Column[5] : COMMENT as 비고</param>
        /// <param name="strQCFLAG">Y</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_TKIN4QC_REQ(string strFormName, string strFACTORYID, string strUSERID, DataTable dtLOTLIST, string strQCFLAG, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "FACTORYID";
                drCON["ItemValue"] = strFACTORYID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "QCFLAG";
                drCON["ItemValue"] = strQCFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_TKIN4QC_REQ", dtCON, "LOTLIST", "LOT", dtLOTLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }        
        }

        /// <summary>
        /// Lot 검사 작업 완료
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strEQPID">EquipCode</param>
        /// <param name="strRECIPEID">RecipeCode</param>
        /// <param name="strAREAID">AreaCode</param>
        /// <param name="strSPLITFLAG">Split여부('N' 고정)</param>
        /// <param name="strSCRAPFLAG">Scrap여부(Y|N)</param>
        /// <param name="dtSCRAPLIST">Column[0] : SCRAPCODE
        ///                           Column[1] : SCRAPQTY
        ///                           Scrap여부가 N인경우 컬럼만 만들어서 빈 DataTable 전달</param>
        /// <param name="strREPAIRFLAG">Repair 여부 (Y|N)</param>
        /// <param name="strRWFLAG">Rework 여부 (Y|N)</param>
        /// <param name="strCONSUMEFLAG">자재소모여부 (Y|N)</param>
        /// <param name="strCOMMENT">비고</param>
        /// <param name="strQCFLAG">'Y'</param>
        /// <param name="strSAMPLECOUNT">검사 Sampling 개수</param>
        /// <param name="strQUALEFLAG">판정 결과 (OK|NG)</param>
        /// <param name="dtREASONLIST">Column[0] : REASONCODE 불량 코드
        ///                            Column[1] : REASONQTY 불량 갯수
        ///                            Column[2] : CAUSEEQPID 불량 원인 설비</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_TKOUT4QC_REQ(string strFormName, string  strFACTORYID, string strUSERID, string strLOTID,
                                          string strEQPID, string strRECIPEID, string strAREAID,
                                          string strSPLITFLAG, string strSCRAPFLAG, DataTable dtSCRAPLIST,
                                          string strREPAIRFLAG, string strRWFLAG, string strCONSUMEFLAG,
                                          string strCOMMENT, string strQCFLAG, string strSAMPLECOUNT,
                                          string strQUALEFLAG, DataTable dtREASONLIST,
                                          string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "FACTORYID";
                drCON["ItemValue"] = strFACTORYID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEQPID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RECIPEID";
                drCON["ItemValue"] = strRECIPEID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "AREAID";
                drCON["ItemValue"] = strAREAID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SPLITFLAG";
                drCON["ItemValue"] = strSPLITFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPFLAG";
                drCON["ItemValue"] = strSCRAPFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "REPAIRFLAG";
                drCON["ItemValue"] = strREPAIRFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RWFLAG";
                drCON["ItemValue"] = strRWFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "CONSUMEFLAG";
                drCON["ItemValue"] = strCONSUMEFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "QCFLAG";
                drCON["ItemValue"] = strQCFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SAMPLECOUNT";
                drCON["ItemValue"] = strSAMPLECOUNT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "QUALEFLAG";
                drCON["ItemValue"] = strQUALEFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "REASONLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_TKOUT4QC_REQ", dtCON, "SCRAPLIST", "SCRAP", dtSCRAPLIST, "REASONLIST", "REASON", dtREASONLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch(Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// Lot 검사 작업 완료
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strEQPID">EquipCode</param>
        /// <param name="strRECIPEID">RecipeCode</param>
        /// <param name="strAREAID">AreaCode</param>
        /// <param name="strSPLITFLAG">Split여부('N' 고정)</param>
        /// <param name="strSCRAPFLAG">Scrap여부(Y|N)</param>
        /// <param name="dtSCRAPLIST">Column[0] : SCRAPCODE
        ///                           Column[1] : SCRAPQTY
        ///                           Scrap여부가 N인경우 컬럼만 만들어서 빈 DataTable 전달</param>
        /// <param name="strREPAIRFLAG">Repair 여부 (Y|N)</param>
        /// <param name="strRWFLAG">Rework 여부 (Y|N)</param>
        /// <param name="strCONSUMEFLAG">자재소모여부 (Y|N)</param>
        /// <param name="strCOMMENT">비고</param>
        /// <param name="strQCFLAG">'Y'</param>
        /// <param name="dtINSPECTITEMLIST">Column[0] : INSPECTID 검사항목
        ///                                 Column[1] : SAMPLECOUNT 검사 Sampling 개수
        ///                                 Column[2] : QUALEFLAG 판정결과 (OK|NG)</param>
        /// <param name="dtREASONLIST">Column[0] : REASONCODE 불량 코드
        ///                            Column[1] : REASONQTY 불량 갯수
        ///                            Column[2] : CAUSEEQPID 불량 원인 설비</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_TKOUT4QC_REQ(string strFormName, string strFACTORYID, string strUSERID, string strLOTID,
                                          string strEQPID, string strRECIPEID, string strAREAID,
                                          string strSPLITFLAG, string strSCRAPFLAG, DataTable dtSCRAPLIST,
                                          string strREPAIRFLAG, string strRWFLAG, string strCONSUMEFLAG,
                                          string strCOMMENT, string strQCFLAG, DataTable dtINSPECTITEMLIST,
                                          DataTable dtREASONLIST,
                                          string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "FACTORYID";
                drCON["ItemValue"] = strFACTORYID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEQPID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RECIPEID";
                drCON["ItemValue"] = strRECIPEID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "AREAID";
                drCON["ItemValue"] = strAREAID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SPLITFLAG";
                drCON["ItemValue"] = strSPLITFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPFLAG";
                drCON["ItemValue"] = strSCRAPFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "REPAIRFLAG";
                drCON["ItemValue"] = strREPAIRFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RWFLAG";
                drCON["ItemValue"] = strRWFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "CONSUMEFLAG";
                drCON["ItemValue"] = strCONSUMEFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "QCFLAG";
                drCON["ItemValue"] = strQCFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "INSPECTITEMLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "REASONLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);




                string strSendMessage = mfCreateTibrvMessage("LOT_TKOUT4QC_REQ", dtCON
                                                        , "SCRAPLIST", "SCRAP", dtSCRAPLIST
                                                        , "REASONLIST", "REASON", dtREASONLIST
                                                        , "INSPECTITEMLIST", "INSPECTITEM", dtINSPECTITEMLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }



        /// <summary>
        /// Lot 검사 작업 완료
        /// </summary>
        /// <param name="strFormName">form name</param>      
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">작업자ID</param>
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strEQPID">EquipCode</param>
        /// <param name="strRECIPEID">RecipeCode</param>
        /// <param name="strAREAID">AreaCode</param>
        /// <param name="strSPLITFLAG">Split여부('N' 고정)</param>
        /// <param name="strSCRAPFLAG">Scrap여부(Y|N)</param>
        /// <param name="dtSCRAPLIST">Column[0] : SCRAPCODE
        ///                           Column[1] : SCRAPQTY
        ///                           Scrap여부가 N인경우 컬럼만 만들어서 빈 DataTable 전달</param>
        /// <param name="strREPAIRFLAG">Repair 여부 (Y|N)</param>
        /// <param name="strRWFLAG">Rework 여부 (Y|N)</param>
        /// <param name="strCONSUMEFLAG">자재소모여부 (Y|N)</param>
        /// <param name="strCOMMENT">비고</param>
        /// <param name="strQCFLAG">'Y'</param>
        /// <param name="dtINSPECTITEMLIST">Column[0] : INSPECTID 검사항목
        ///                                 Column[1] : SAMPLECOUNT 검사 Sampling 개수
        ///                                 Column[2] : QUALEFLAG 판정결과 (OK|NG)</param>
        /// <param name="dtREASONLIST">Column[0] : REASONCODE 불량 코드
        ///                            Column[1] : REASONQTY 불량 갯수
        ///                            Column[2] : CAUSEEQPID 불량 원인 설비</param>
        ///                            
        /// <param name="strTKOUTFLAG">TrackIn Flag</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_TKOUT4QC_REQ(string strFormName, string strFACTORYID, string strUSERID, string strLOTID,
                                          string strEQPID, string strRECIPEID, string strAREAID,
                                          string strSPLITFLAG, string strSCRAPFLAG, DataTable dtSCRAPLIST,
                                          string strREPAIRFLAG, string strRWFLAG, string strCONSUMEFLAG,
                                          string strCOMMENT, string strQCFLAG, DataTable dtINSPECTITEMLIST,
                                          DataTable dtREASONLIST,string strTKOUTFLAG,
                                          string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "FACTORYID";
                drCON["ItemValue"] = strFACTORYID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEQPID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RECIPEID";
                drCON["ItemValue"] = strRECIPEID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "AREAID";
                drCON["ItemValue"] = strAREAID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SPLITFLAG";
                drCON["ItemValue"] = strSPLITFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPFLAG";
                drCON["ItemValue"] = strSCRAPFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "SCRAPLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "REPAIRFLAG";
                drCON["ItemValue"] = strREPAIRFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RWFLAG";
                drCON["ItemValue"] = strRWFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "CONSUMEFLAG";
                drCON["ItemValue"] = strCONSUMEFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "QCFLAG";
                drCON["ItemValue"] = strQCFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "INSPECTITEMLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "REASONLIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "TKOUTFLAG";
                drCON["ItemValue"] = strTKOUTFLAG;
                dtCON.Rows.Add(drCON);


                string strSendMessage = mfCreateTibrvMessage("LOT_TKOUT4QC_REQ", dtCON
                                                        , "SCRAPLIST", "SCRAP", dtSCRAPLIST
                                                        , "REASONLIST", "REASON", dtREASONLIST
                                                        , "INSPECTITEMLIST", "INSPECTITEM", dtINSPECTITEMLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }



        /// <summary>
        /// SPCN 알람 
        /// </summary>
        /// <param name="strFormName">Form Nmae</param>
        /// <param name="strUSERID">작업자 ID</param>
        /// <param name="strALARMID">"*"</param>
        /// <param name="strALARMTEXT">"*"</param>
        /// <param name="strEQPID">EquipCode</param>
        /// <param name="strCompleteFlag">Y|N (Y:조치완료, N:발생)</param>
        /// <param name="strUserIP">User IP</param>
        public void SPCN_ALARM(string strFormName, string strUSERID, string strALARMID, string strALARMTEXT, string strEQPID, string strCompleteFlag, string strUserIP)
        {
            #region DataTable
            //DataTable dtRtn = new DataTable();
            //dtRtn.Columns.Add("returncode", typeof(string));
            //dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtLot = mfDataTableInfo();
                DataRow drLot = dtLot.NewRow();
                drLot["ItemName"] = "USERID";
                drLot["ItemValue"] = strUSERID;
                dtLot.Rows.Add(drLot);

                drLot = dtLot.NewRow();
                drLot["ItemName"] = "ALARMID";
                drLot["ItemValue"] = strALARMID;
                dtLot.Rows.Add(drLot);

                drLot = dtLot.NewRow();
                drLot["ItemName"] = "ALARMTEXT";
                drLot["ItemValue"] = strALARMTEXT;
                dtLot.Rows.Add(drLot);

                drLot = dtLot.NewRow();
                drLot["ItemName"] = "EQPID";
                drLot["ItemValue"] = strEQPID;
                dtLot.Rows.Add(drLot);

                drLot = dtLot.NewRow();
                drLot["ItemName"] = "COMPLETEFLAG";
                drLot["ItemValue"] = strCompleteFlag;
                dtLot.Rows.Add(drLot);

                string strSendMessage = mfCreateTibrvMessage("SPCN_ALARM", dtLot);
                #endregion

                //string strRtn = SendRequest(strSendMessage);
                Send(strSendMessage);

                //if (!strRtn.Equals(string.Empty))
                //    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                //if (!strRtn.Equals(string.Empty))
                //{
                //    #region Create Return DataTable via Reply Message
                //    if (mfCheckReply(strSendMessage, strRtn))
                //    {
                //        XmlDocument xmlRtn = new XmlDocument();
                //        xmlRtn.LoadXml(strRtn);

                //        DataRow drRtn = dtRtn.NewRow();
                //        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                //        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                //        dtRtn.Rows.Add(drRtn);
                //        return dtRtn;
                //    }
                //    else
                //    {
                //        dtRtn = new DataTable();
                //        dtRtn.Columns.Add("returncode", typeof(string));
                //        dtRtn.Columns.Add("returnmessage", typeof(string));
                //        return dtRtn;
                //    }
                //    #endregion
            //    }
            //    else
            //    {
            //        #region Return
            //        dtRtn = new DataTable();
            //        dtRtn.Columns.Add("returncode", typeof(string));
            //        dtRtn.Columns.Add("returnmessage", typeof(string));
            //        return dtRtn;
            //        #endregion
            //    }
            }
            catch(Exception ex)
            {
                #region Return
                throw(ex);
                //dtRtn = new DataTable();
                //dtRtn.Columns.Add("returncode", typeof(string));
                //dtRtn.Columns.Add("returnmessage", typeof(string));
                //DataRow dr = dtRtn.NewRow();
                //dr["returncode"] = ex.Message.ToString();
                //dr["returnmessage"] = ex.StackTrace.ToString();
                //dtRtn.Rows.Add(dr);
                //return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }


        /// <summary>
        /// CCS 의뢰 접수 완료 요청  
        /// </summary>
        /// <param name="strFormName">Form Name</param>
        /// <param name="strUSERID">작업자 ID</param>
        /// <param name="strEQPID">EquipCode</param>
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable CCS_ACCEPT_REQ(string strFormName, string strUSERID, string strEQPID, string strLOTID, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEQPID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("CCS_ACCEPT_REQ", dtCON);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if(!strRtn.Equals(string.Empty)) 
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// 설비상태변경요청
        /// </summary>
        /// <param name="strFormName">폼명</param>
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">작업자사번</param>
        /// <param name="strAUTOACCEPT">자동접수처리여부(Y | N)</param>
        /// <param name="strCANCELFLAG">의뢰취소여부(Y | N)</param>
        /// <param name="dtSTATELIST"><para>설비상태변경List</para>
        ///                         <para>Column["EQPID"] : 설비ID</para>
        ///                         <para>Column["EQPSTATE"] : 설비상태(DOWN | MDC(품종교체시))</para>
        ///                         <para>Column["PRODUCTSPECID"] : 품종 교체시 제품코드</para>
        ///                         <para>Column["MACHINERECIPE"] : 설비 Recipe</para>
        ///                         <para>Column["REASONCODE"] : Down이나 품종교체코드</para>
        ///                         <para>Column["EMERGENCYFLAG"] : 긴급처리여부 (Y | N)</para>
        ///                         <para>Column["COMMENT"] : Comment</para>
        ///                         <para>Column["TOOLLIST"] : 공백</para></param>
        ///<param name="dtTOOLLIST"><para>치공구장착정보</para>
        ///                         <para>Column["TOOLID"] : 치공구 LOT ID</para>
        ///                         <para>Column["TOOLSPECID"] : 치공구 Part 정보</para></param>
        ///<param name="strLogInUserID">로그인사용자아이디</param>
        ///<param name="strLogInUserIP">로그인사용자아이피</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable EQP_CHANGESTATE_REQ(string strFormName, string strFACTORYID, string strUSERID, string strAUTOACCEPT, string strCANCELFLAG, DataTable dtSTATELIST, DataTable dtTOOLLIST
                                            , string strLogInUserID, string strLogInUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "FACTORYID";
                drCON["ItemValue"] = strFACTORYID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "AUTOACCEPT";
                drCON["ItemValue"] = strAUTOACCEPT;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "CANCELFLAG";
                drCON["ItemValue"] = strCANCELFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "STATELIST";
                drCON["ItemValue"] = "";
                dtCON.Rows.Add(drCON);

                //string strSendMessage = mfCreateTibrvMessage("EQP_CHANGESTATE_REQ", dtCON, "STATELIST", "STATE", dtSTATELIST);
                string strSendMessage = mfCreateTibrvMessage_Child("EQP_CHANGESTATE_REQ", dtCON, "STATELIST", "STATE", dtSTATELIST, "TOOLLIST", "TOOL", dtTOOLLIST);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strLogInUserIP, strLogInUserID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        
        /// <summary>
        /// WAFER 수입검사여부 확인
        /// </summary>
        /// <param name="strFormName">화면ID</param>
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">검사자ID</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strBGFlag">B/G여부(Y/N)</param>
        /// <param name="strComment">비고</param>
        /// <param name="strLogInUserID">로그인사용자ID</param>
        /// <param name="strLogInUserIP">로그인사용자IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_WAFERBG4QC_REQ(string strFormName, string strFACTORYID, string strUSERID, string strLotNo, string strBGFlag, string strComment
                                            , string strLogInUserID, string strLogInUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                ////drCON["ItemName"] = "FACTORYID";
                ////drCON["ItemValue"] = strFACTORYID;
                ////dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLotNo;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "BGFLAG";
                drCON["ItemValue"] = strBGFlag;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strComment;
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_WAFERBG4QC_REQ", dtCON);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strLogInUserIP, strLogInUserID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }

        /// <summary>
        /// CCS의뢰대상 장비 Lockin/UnLocking
        /// </summary>
        /// <param name="strFormName">화면ID</param>
        /// <param name="strFACTORYID">공장코드</param>
        /// <param name="strUSERID">검사자ID</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strDownCode">다운코드</param>
        /// <param name="strLockingFlag">Locking 여부(Y/N)</param>
        /// <param name="strComment">비고</param>
        /// <param name="strLogInUserID">로그인사용자ID</param>
        /// <param name="strLogInUserIP">로그인사용자IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable EQP_DOWN4QC(string strFormName, string strFACTORYID, string strUSERID, string strLotNo
                                            , string strEquipCode, string strDownCode, string strLockingFlag, string strComment
                                            , string strLogInUserID, string strLogInUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                ////drCON["ItemName"] = "FACTORYID";
                ////drCON["ItemValue"] = strFACTORYID;
                ////dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLotNo;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "EQPID";
                drCON["ItemValue"] = strEquipCode;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "DOWNCODE";
                drCON["ItemValue"] = strDownCode;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOCKINGFLAG";
                drCON["ItemValue"] = strLockingFlag;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strComment;
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("EQP_DOWN4QC", dtCON);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (strRtn == null)
                    mfWindowsLogcheck("EQP_DOWN4QC : Return Message Is NULL" +
                                    "\nLotNo : " + strLotNo +
                                    "\nEquipCode : " + strEquipCode +
                                    "\nLockingFlag : " + strLockingFlag +
                                    "\nLoginUserID : " + strLogInUserID +
                                    "\nLoginUserIP : " + strLogInUserIP);
                else
                    mfWindowsLogcheck("EQP_DOWN4QC : Return Message" +
                                    "\nLotNo : " + strLotNo +
                                    "\nEquipCode : " + strEquipCode +
                                    "\nLockingFlag : " + strLockingFlag +
                                    "\nLoginUserID : " + strLogInUserID +
                                    "\nLoginUserIP : " + strLogInUserIP +
                                    "\nSend\n" + strSendMessage + 
                                    "\nReply\n" + strRtn);

                ////if (!strRtn.Equals(string.Empty))
                ////    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strLogInUserIP, strLogInUserID);
                ////else
                ////    mfWindowsLogcheck("EQP_DOWN4QC : Return Message Is Empty" +
                ////                    "\nLotNo : " + strLotNo +
                ////                    "\nEquipCode : " + strEquipCode +
                ////                    "\nLockingFlag : " + strLockingFlag +
                ////                    "\nLoginUserID : " + strLogInUserID +
                ////                    "\nLoginUserIP : " + strLogInUserIP);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck("EQP_DOWN4QC : Exception Error" +
                                    "\nex.Data\n" + ex.Data.Values.ToString() +
                                    "\nex.GetType\n" + ex.GetType().ToString() +
                                    "\nex.InnerException.Message\n" + ex.InnerException.Message +
                                    "\nex.InnerException.Data\n" + ex.InnerException.Data.Values.ToString() +
                                    "\nex.InnerException.Source\n" + ex.InnerException.Source +
                                    "\nex.InnerException.StackTrace\n" + ex.InnerException.StackTrace +
                                    "\nex.Message\n" + ex.Message +
                                    "\nex.Source\n" + ex.Source +
                                    "\nex.StackTrace\n" + ex.StackTrace
                                    );
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }


        /// <summary>
        /// TEST 이상발생
        /// </summary>
        /// <param name="strFormName">Form Name</param>
        /// <param name="strUSERID">작업자 ID</param>
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strOPERID">Process</param>
        /// <param name="strRELEASEFLAG">LotRELEASE 처리 가능 여부</param>
        /// <param name="strCOMMENT">Comment</param>
        /// <param name="strUserIP">User IP</param>
        /// <returns>Column[0] returncode : if success '0' else errorcode
        ///          Column[1] returnmessage : errormessage</returns>
        public DataTable LOT_TESTHOLD4QC_REQ(string strFormName,string strUSERID, string strLOTID, string strOPERID,
                                            string strRELEASEFLAG, string strCOMMENT, string strUserIP)
        {
            #region DataTable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("returncode", typeof(string));
            dtRtn.Columns.Add("returnmessage", typeof(string));
            #endregion

            try
            {
                if (!isRun)
                {
                    Initialize();
                }

                #region XmlCreate
                DataTable dtCON = mfDataTableInfo();

                DataRow drCON = dtCON.NewRow();
                drCON["ItemName"] = "USERID";
                drCON["ItemValue"] = strUSERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "LOTID";
                drCON["ItemValue"] = strLOTID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "OPERID";
                drCON["ItemValue"] = strOPERID;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "RELEASEFLAG";
                drCON["ItemValue"] = strRELEASEFLAG;
                dtCON.Rows.Add(drCON);

                drCON = dtCON.NewRow();
                drCON["ItemName"] = "COMMENT";
                drCON["ItemValue"] = strCOMMENT;
                dtCON.Rows.Add(drCON);

                string strSendMessage = mfCreateTibrvMessage("LOT_TESTHOLD4QC_REQ", dtCON);
                #endregion

                string strRtn = SendRequest(strSendMessage);

                if (!strRtn.Equals(string.Empty))
                    mfSaveMEFIFLog(strFormName, strSendMessage, strRtn, strUserIP, strUSERID);

                if (!strRtn.Equals(string.Empty))
                {
                    #region Create Return DataTable via Reply Message
                    if (mfCheckReply(strSendMessage, strRtn))
                    {
                        XmlDocument xmlRtn = new XmlDocument();
                        xmlRtn.LoadXml(strRtn);

                        DataRow drRtn = dtRtn.NewRow();
                        drRtn["returncode"] = xmlRtn.SelectSingleNode("message/return/returncode").InnerText.ToString();
                        drRtn["returnmessage"] = xmlRtn.SelectSingleNode("message/return/returnmessage").InnerText.ToString();
                        dtRtn.Rows.Add(drRtn);
                        return dtRtn;
                    }
                    else
                    {
                        dtRtn = new DataTable();
                        dtRtn.Columns.Add("returncode", typeof(string));
                        dtRtn.Columns.Add("returnmessage", typeof(string));
                        return dtRtn;
                    }
                    #endregion
                }
                else
                {
                    #region Return
                    dtRtn = new DataTable();
                    dtRtn.Columns.Add("returncode", typeof(string));
                    dtRtn.Columns.Add("returnmessage", typeof(string));
                    return dtRtn;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Return
                dtRtn = new DataTable();
                dtRtn.Columns.Add("returncode", typeof(string));
                dtRtn.Columns.Add("returnmessage", typeof(string));
                DataRow dr = dtRtn.NewRow();
                dr["returncode"] = ex.Message.ToString();
                dr["returnmessage"] = ex.StackTrace.ToString();
                dtRtn.Rows.Add(dr);
                return dtRtn;
                #endregion
            }
            finally
            {
                Terminate();
            }
        }


        [System.EnterpriseServices.AutoComplete(false)]
        private string mfSaveMESIFErrorLog(string strFormName, string xmlSendMessage, string xmlReplyMessage, string strReturnCode, string strReturnMessage, string strUserIP, string strUserID)
        {
            QRPDB.SQLS sql = new QRPDB.SQLS();

            QRPDB.TransErrRtn ErrRtn = new QRPDB.TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            try
            {
                System.Data.SqlClient.SqlTransaction trans = sql.SqlCon.BeginTransaction();
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strFormName", ParameterDirection.Input, SqlDbType.VarChar, strFormName, 50);
                sql.mfAddParamDataRow(dtParameter, "@i_xmlSendMessage", ParameterDirection.Input, SqlDbType.VarChar, xmlSendMessage, 8000);
                sql.mfAddParamDataRow(dtParameter, "@i_xmlReplyMessage", ParameterDirection.Input, SqlDbType.VarChar, xmlReplyMessage, 8000);
                sql.mfAddParamDataRow(dtParameter, "@i_strReturnCode", ParameterDirection.Input, SqlDbType.VarChar, strReturnCode, 50);
                sql.mfAddParamDataRow(dtParameter, "@i_strReturnMessage", ParameterDirection.Input, SqlDbType.VarChar, strReturnMessage, 50);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMESIFErrorLog", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if(ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        private void mfWindowsLogcheck(string End)
        {
            //System.Windows.Forms.MessageBox.Show(ex.Message.ToString(), string.Empty, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly);
            string sSource = "QRPMES.IF.EventLog";
            string sLog = "Application";

            if (!System.Diagnostics.EventLog.SourceExists(sSource))
                System.Diagnostics.EventLog.CreateEventSource(sSource, sLog);

            System.Diagnostics.EventLog.WriteEntry(sSource, End, System.Diagnostics.EventLogEntryType.SuccessAudit, 0);
        }
    }
}
